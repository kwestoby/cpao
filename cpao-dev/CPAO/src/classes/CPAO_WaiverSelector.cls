public with sharing class CPAO_WaiverSelector {
	
	public static List<CPAO_Waiver__c> getContinuousContactWaivers(Set<Id> contactIds) {
		return [SELECT Id, CPAO_Type__c, CPAO_Sub_Type__c, CPAO_Contact__c, CPAO_Continuous_Waiver_Exemption__c,
				 RecordType.Name, CPAO_Status__c
				FROM CPAO_Waiver__c 
				WHERE CPAO_Contact__c IN :contactIds AND CPAO_Continuous_Waiver_Exemption__c = true];
	}

	public static List<CPAO_Waiver__c> getContactWaivers(Set<Id> contactIds) {
		return [SELECT Id, CPAO_Type__c, CPAO_Sub_Type__c, CPAO_Contact__c, CPAO_Continuous_Waiver_Exemption__c,
				 RecordType.Name, CPAO_Status__c
				FROM CPAO_Waiver__c 
				WHERE CPAO_Contact__c IN :contactIds];
	}
}