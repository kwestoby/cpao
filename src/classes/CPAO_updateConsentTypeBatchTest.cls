@isTest
public class CPAO_updateConsentTypeBatchTest {

	public static List<Contact> communicationPreferences;
	public static List<contact>contacts;

	static void dataInitialize(String consentType,Date consentDate){

		List<Account>accounts = CPAO_TestFactory.createAccounts(1);
		insert accounts; 
		contacts = CPAO_TestFactory.createContacts(accounts[0],1);
		insert contacts;
		communicationPreferences=CPAO_TestFactory.createCommPreferenceImplied(1,consentType,contacts,consentDate);
		update(communicationPreferences);

	}

    // This test changes the consent type of a communication preference from implied to
    // unknown because the implied consent expiry date is at least 181 days before today
	@isTest static void changeConsentTypeToUnknown() {
		dataInitialize('Implied', Date.newInstance(2017, 6, 9));
		Test.startTest();
		CPAO_updateConsentTypeBatch batchJob = new CPAO_updateConsentTypeBatch();
		database.executebatch(batchJob);
		Test.stopTest();
		List<Contact> result = CPAO_CommunicationPreferenceSelector.getCommunicationPreferences();
		for ( Integer i=0; i<result.size();i++ ){
			System.assert(result[i].id == contacts[i].Id );
			System.assert(result[i].CPAO_Consent_type__c== 'Unknown');
		}
	
	}

	// This test doesnt changes the consent type of a communication preference  to
    // unknown because the consent type is not implied
	@isTest static void noChangeOnConsentType() {
		dataInitialize('Express Yes',Date.newInstance(2017, 6, 9));
		Test.startTest();
		CPAO_updateConsentTypeBatch batchJob = new CPAO_updateConsentTypeBatch();
		database.executebatch(batchJob);
		Test.stopTest();
		List<Contact> result = CPAO_CommunicationPreferenceSelector.getCommunicationPreferences();
		for ( Integer i=0; i<result.size();i++ ){
			System.assert(result[i].id == contacts[i].Id );
			System.assert(result[i].CPAO_Consent_type__c== 'Express Yes');
		
		}
	
	}

	// This test doesnt changes the consent type of a communication preference  to
    // unknown because the consent type is  implied but the consent type expiray date
    // is still within the range of 181 days past the consent date
	@isTest static void noChangeOnImpliedConsentType() {
		dataInitialize('Implied',Date.newInstance(2017, 11, 9));
		Test.startTest();
		CPAO_updateConsentTypeBatch batchJob = new CPAO_updateConsentTypeBatch();
		database.executebatch(batchJob);
		Test.stopTest();
		List<Contact> result = CPAO_CommunicationPreferenceSelector.getCommunicationPreferences();
		for ( Integer i=0; i<result.size();i++ ){
			System.assert(result[i].id == contacts[i].Id );
			System.assert(result[i].CPAO_Consent_type__c== 'Implied');
		
		}
	
	}
















/*   //DEPRECATED as we stopped using Communication preferences object
	// Commented for tracking purposes 

	public static List<CPAO_Communications_Preferences__c> communicationPreferences;
	public static List<contact>contacts;

	static void dataInitialize(String consentType,Date consentDate){

		List<Account>accounts = CPAO_TestFactory.createAccounts(1);
		insert accounts; 
		contacts = CPAO_TestFactory.createContacts(accounts[0],1);
		insert contacts;
		communicationPreferences=CPAO_TestFactory.createCommPreferenceImplied(1,consentType,contacts,consentDate);
		insert(communicationPreferences);
	}

    // This test changes the consent type of a communication preference from implied to
    // unknown because the implied consent expiry date is at least 181 days before today
	@isTest static void changeConsentTypeToUnknown() {
		dataInitialize('Implied', Date.newInstance(2017, 6, 9));
		Test.startTest();
		CPAO_updateConsentTypeBatch batchJob = new CPAO_updateConsentTypeBatch();
		database.executebatch(batchJob);
		Test.stopTest();
		List<CPAO_Communications_Preferences__c> result = CPAO_CommunicationPreferenceSelector.getCommunicationPreferences();
		for ( Integer i=0; i<result.size();i++ ){
			System.assert(result[i].Associated_Contact__c == contacts[i].Id );
			System.assert(result[i].CPAO_Consent_type__c== 'Unknown');
		}
	
	}

	// This test doesnt changes the consent type of a communication preference  to
    // unknown because the consent type is not implied
	@isTest static void noChangeOnConsentType() {
		dataInitialize('Express Yes',Date.newInstance(2017, 6, 9));
		Test.startTest();
		CPAO_updateConsentTypeBatch batchJob = new CPAO_updateConsentTypeBatch();
		database.executebatch(batchJob);
		Test.stopTest();
		List<CPAO_Communications_Preferences__c> result = CPAO_CommunicationPreferenceSelector.getCommunicationPreferences();
		for ( Integer i=0; i<result.size();i++ ){
			System.assert(result[i].Associated_Contact__c == contacts[i].Id );
			System.assert(result[i].CPAO_Consent_type__c== 'Express Yes');
		
		}
	
	}

	// This test doesnt changes the consent type of a communication preference  to
    // unknown because the consent type is  implied but the consent type expiray date
    // is still within the range of 181 days past the consent date
	@isTest static void noChangeOnImpliedConsentType() {
		dataInitialize('Implied',Date.newInstance(2017, 11, 9));
		Test.startTest();
		CPAO_updateConsentTypeBatch batchJob = new CPAO_updateConsentTypeBatch();
		database.executebatch(batchJob);
		Test.stopTest();
		List<CPAO_Communications_Preferences__c> result = CPAO_CommunicationPreferenceSelector.getCommunicationPreferences();
		for ( Integer i=0; i<result.size();i++ ){
			System.assert(result[i].Associated_Contact__c == contacts[i].Id );
			System.assert(result[i].CPAO_Consent_type__c== 'Implied');
		
		}
	
	}*/
}