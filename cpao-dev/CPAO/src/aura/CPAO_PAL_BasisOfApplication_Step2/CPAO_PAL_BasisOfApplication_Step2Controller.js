({
    
    clickOnNextButton : function(component,event,helper){
        var componentNameInEvent=event.getParam("componentName");
        if(componentNameInEvent==component.get('v.nameOfThisComponent')){
            if(event.getParam("nextStepName")=='Exit'){ 
                component.set('v.shouldSaveAndQuit',true);
            }
            var validForm = component.find('PALForm').reduce(function (validSoFar, inputCmp) {
                var role_in_PA=  component.get('v.currentPAL.CPAO_Role_In_Public_Accounting__c');  
                var basis_of_app=   component.get('v.currentPAL.CPAO_Basis_Of_Application__c');
                if(role_in_PA==undefined){
                    component.set('v.role_in_PA_Required', true);
                }else{
                    component.set('v.role_in_PA_Required', false);
                }
                if(basis_of_app==undefined){
                    component.set('v.basis_of_app_required',true);
                }else {
                     component.set('v.basis_of_app_required',false);
                    
                }
                
                inputCmp.showHelpMessageIfInvalid();
                return validSoFar && inputCmp.get('v.validity').valid &&
                    (role_in_PA!=undefined) && (basis_of_app!=undefined);
            }, true);
            
            if(validForm){
                component.set('v.role_in_PA_Required', false);
                component.set('v.basis_of_app_required',false);
                
                var effectiveDate= component.get('v.currentPAL').CPAO_Effective_Date__c;
                var formattedDate=effectiveDate.substring(0,10);
                component.set('v.currentPAL.CPAO_Effective_Date__c',formattedDate);
                var showSpinnerEvent = component.getEvent("showSpinner");
                showSpinnerEvent.fire();
                console.log(JSON.stringify(component.get('v.currentPAL')));
                helper.upsertPAL(component);
                
            }
        }
    },
    
     
})