({
	methodFailure : function(component, event, helper, body, header) {
		var applicationHasChanged = component.getEvent("applicationHasChanged");
        applicationHasChanged.setParams({"nextButtonClickeable" : false, 
    									"modalBody" : body,
    									"modalHeader" : header} );
        applicationHasChanged.fire();
	},
})