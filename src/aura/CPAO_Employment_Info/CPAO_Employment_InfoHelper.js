({
    showToast : function(title, message, type) {
   		console.log('start of showToast method');
	  	var toastEvent = $A.get("e.force:showToast");
	  	console.log('before setparams');
	  	toastEvent.setParams({
	        "title": title,
	        "message": message,
	        "type" : type
	  	});
	  	console.log('before toast fire');
	  	toastEvent.fire();
	  	console.log('after toast fire');
	},

	confirmRetirementUnemployment : function(component, event, helper, type) {
		console.log('retirement method');
		var allRelationships = component.get("v.listOfRelationships");
	    for (var i = 0; i < allRelationships.length; i++) {
	    	console.log('allRelationships[i]:');
	    	console.log(allRelationships[i]);

	        if(allRelationships[i].CPAO_End_Date__c == null || allRelationships[i].CPAO_End_Date__c == undefined){
	        	component.set("v.confirmationMessage"," Error:Please mark an end date for all employment entries");
	          helper.showToast("Error", "please mark an end date for all employment entries", "error");
	          return;
	        }
	    }
	    var action = component.get("c.retireUnemployEmployee");
	    action.setParams({
	    	"employmentType": type
	      	}); 

	    action.setCallback(this, function(response){

	        var state = response.getState();
	        console.log('state'+state);
	        if (state === "SUCCESS") {
	        	component.set("v.endDateModalOpen", false);
	        	if(type == 'Retired'){
	        		if(response.getReturnValue() == 'Success'){
			            component.set("v.confirmationMessage", "Successfully changed status");
			        } else {
			            component.set("v.confirmationMessage", "Error updating status, please contact administrator");
			        }
	        	} else if(type == 'Unemployed'){
	        		if(response.getReturnValue() == 'Success'){
			            component.set("v.confirmationMessage", "Successfully changed status");
			        } else {
			            component.set("v.confirmationMessage", "Error updating status, please contact administrator");
			        }
	        	}
	        	
	        }else{
	        	 console.log('there was an error');
	        	  var errors=response.getError();
                      var message = errors[0];
                      
                      console.log(message);
	        }
	      });
	       $A.enqueueAction(action);
	},
	orderListOfEmployer : function(component, event, helper) {
     //logic for sorting records by primary employer and then by start date desc 
     		//console.log('order start with '+ JSON.stringify(component.get("v.listOfRelationships")));

            var listOfEmployer=component.get("v.listOfRelationships");
            var sortedListOfEmployer=[];//sorted List by stsorted List by start date DESC excluding the primary employerart date DESC excluding the primary employer
            var sortedListOfEmployerWithPrimary=[];//sorted List by start date DESC including the primary employer that should remain on the top no matter its start date

            for(var i = 0; i < listOfEmployer.length ; i++){

                if(listOfEmployer[i].CPAO_Primary_Employer__c=='Yes'){
                    sortedListOfEmployerWithPrimary.push(listOfEmployer[i]);
                }else{
                    sortedListOfEmployer.push(listOfEmployer[i]);
                }

            }
            /*
            console.log('sortedListOfEmployer');
             for(var i = 0; i < sortedListOfEmployer.length ; i++){

               console.log(JSON.stringify(sortedListOfEmployer[i]));

            }

             console.log('sortedListOfEmployerWithPrimary');
             for(var i = 0; i < sortedListOfEmployerWithPrimary.length ; i++){

                 console.log(JSON.stringify(sortedListOfEmployerWithPrimary[i]));

            }*/




            //adding the primary 
            for(var i = 0; i < sortedListOfEmployer.length; i++){
                sortedListOfEmployerWithPrimary.push(sortedListOfEmployer[i]);
            }

            component.set('v.listOfRelationships',sortedListOfEmployerWithPrimary);
                 		//console.log('order ends with '+ JSON.stringify(component.get("v.listOfRelationships")));

    },

   
})