({
	doInit : function(component, event, helper) {
		var apexAction = component.get('c.getWaivers');
        apexAction.setCallback(this, function(response) {
        	var state = response.getState();
            console.log('state');
            console.log(state);
        	if(state === "SUCCESS"){
                var result = response.getReturnValue();
                if(result.hasAMDWaiver == true && result.hasCPDWaiver == true){
                	component.set("v.hasAMDWaiver", true);
                    component.set("v.hasCPDWaiver", true);
                    component.set("v.pathNumber", 1);
                } else if(result.hasAMDWaiver != true && result.hasCPDWaiver == true){
                	component.set("v.hasAMDWaiver", false);
                    component.set("v.hasCPDWaiver", true);
                    component.set("v.pathNumber", 2);
                } else if(result.hasAMDWaiver == true && result.hasCPDWaiver != true){
                    component.set("v.hasAMDWaiver", true);
                    component.set("v.hasCPDWaiver", false);
                    component.set("v.pathNumber", 3);
                } else if(result.hasAMDWaiver != true && result.hasCPDWaiver != true){
                    component.set("v.hasAMDWaiver", false);
                    component.set("v.hasCPDWaiver", false);
                    component.set("v.pathNumber", 4);
                } 

                if(result.livesInCanada == false){
                    component.set("v.livesInCanada", false);
                }

                if(result.unemployed3TimesAlready == true){
                    component.set("v.unemployed3TimesAlready", true);
                }

                component.set("v.communityHomePage", result.communityURL);
                component.set("v.waiverExplanations", result.waiverExplanations);
                component.set("v.waiverExplanation", result.waiverExplanations[result.currentAMDCPDApplication.CPAO_AMD_Waiver__c]);

                
                var app = result.currentAMDCPDApplication;
                
                if(app.CPAO_Not_a_Resident_of_Canada__c == 'Yes' && app.CPAO_Not_Registered_for_GST_HST__c == 'Yes' &&
                app.CPAO_Not_Present_in_Canada__c == 'Yes' && app.CPAO_Do_not_Carry_Business_in_Canada__c == 'Yes'){
                    component.set("v.showZeroTaxAttestation", true);
                } else {
                    component.set("v.showZeroTaxAttestation", false);
                }
                
                component.set("v.application", app);
                var serviceTypes = [];
                for (var i = 0; i<result.serviceTypes.length; i++) { 
                    serviceTypes.push(result.serviceTypes[i]);
                }

                component.set("v.serviceTypes", serviceTypes);
                component.set("v.applicationServiceTypes", result.applicationServiceTypes);
                component.set("v.capacityAccount",result.capacityAccount);
                //component.set("v.registrantTypes", result.registrantTypes);
                console.log('result.capacityAccount');
                console.log(result.capacityAccount);
                
                var values = [];
                var result = result.registrantTypes;
                for(var key in result){
                    values.push({
                        class:'optionClass',
                        label:key,
                        value:result[key]});         
                }
                component.set("v.registrantTypes",values);

                
            }
                

        });
        $A.enqueueAction(apexAction);
	},

    handleNext : function(component,event,helper){
        var getselectedStep = component.get("v.selectedStep");
        var pathNumber = component.get("v.pathNumber");
        if(getselectedStep == "step1"){
            component.set("v.selectedStep", "step2");
            component.set("v.canBack", true);
            
        } else if(getselectedStep == "step3"){
            var reviewLoadMethod = component.find('extendedProfile');
            reviewLoadMethod.loadMethod();
            component.set("v.selectedStep", "step4");
        } else {
            helper.showSpinner(component);
            var nextButtonClick = $A.get("e.c:CPAO_Next_Button_Has_Been_Pressed");
            nextButtonClick.setParams({
                "componentName" : getselectedStep,
                "nextStepName" : "Next"});
            nextButtonClick.fire();
            
        }

    },

    saveAndExit : function(component,event,helper){
        var getselectedStep = component.get("v.selectedStep");
        if(getselectedStep == "step1" || getselectedStep == "step3"){
            var homepageURL = component.get("v.communityHomePage");
            window.location.href = homepageURL;
            return;
        } else {
            helper.showSpinner(component);
            var nextButtonClick = $A.get("e.c:CPAO_Next_Button_Has_Been_Pressed");
            nextButtonClick.setParams({
                "componentName" : getselectedStep,
                "nextStepName" : "Exit"});
            nextButtonClick.fire();
        }
    },

    applyResponseFromChildComponent : function(component,event,helper){
        var getselectedStep = component.get("v.selectedStep");
        var pathNumber = component.get("v.pathNumber");
        helper.hideSpinner(component);
        var isNextOk = event.getParam("nextButtonClickeable");
        if(isNextOk == false){
            var modalBody = event.getParam("modalBody");
            var modalHeader = event.getParam("modalHeader");
            if(modalBody == undefined || modalBody == ''){
                return;
            }
            component.set("v.isOpen", true);
            component.set("v.modalBody", modalBody);
            component.set("v.modalHeader", modalHeader);
            return;
        }
        var action = event.getParam("action");
        if(action == "Exit"){
            var homepageURL = component.get("v.communityHomePage");
            window.location.href = homepageURL;
            return;
        }
        var toReviewPage = false;
        if(getselectedStep == "step2"){
            component.set("v.selectedStep", "step3");
            component.set("v.canBack", true);
        } else if(getselectedStep == "step4"){
            if(pathNumber == 2){
                component.set("v.selectedStep", "step6");
            } else {
                component.set("v.selectedStep", "step5");
            }
        } else if(getselectedStep == "step5"){
            if(pathNumber == 3){
                component.set("v.selectedStep", "step7");
                toReviewPage = true;
            } else {
                component.set("v.selectedStep", "step6");
            }
        } else if(getselectedStep == "step6"){
            component.set("v.selectedStep", "step7");
            toReviewPage = true;
        } else if(getselectedStep == "step7"){
            console.log('you made it to the end');
        }
        if(toReviewPage == true){
            var reviewLoadMethod = component.find('reviewPage');
            reviewLoadMethod.loadMethod();
        }
    },

    handlePrev : function(component,event,helper){
        var getselectedStep = component.get("v.selectedStep");
        var pathNumber = component.get("v.pathNumber");
        if(getselectedStep == "step2"){
            component.set("v.selectedStep", "step1");
            component.set("v.canBack", false);
        } else if(getselectedStep == "step3"){
            component.set("v.selectedStep", "step2");
            if(pathNumber == 1){
                component.set("v.canBack", false);
            }
        } else if(getselectedStep == "step4"){
            component.set("v.selectedStep", "step3");
        } else if(getselectedStep == "step5"){
            component.set("v.selectedStep", "step4");        
        } else if(getselectedStep == "step6"){
            if(pathNumber == 2){
                component.set("v.selectedStep", "step4");
            } else {
                component.set("v.selectedStep", "step5");
            }
        } else if(getselectedStep == "step7"){
            if(pathNumber == 3){
                component.set("v.selectedStep", "step5");
            } else {
                component.set("v.selectedStep", "step6");
            }
        }
    },
    closeModel: function(component, event, helper) {
      // for Hide/Close Model,set the "isOpen" attribute to "Fasle"  
      component.set("v.isOpen", false);
    },
})