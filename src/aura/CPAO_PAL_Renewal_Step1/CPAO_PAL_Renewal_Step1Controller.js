({
	 regulations_Redirect : function(component, event, helper) {
        url_to_redirect='https://www.cpaontario.ca/stewardship-of-the-profession/governance/act-bylaws-and-regulations';
        window.open(url_to_redirect,'_blank');
    },
    
    prescribedForm_Redirect : function(component, event, helper) {
        url_to_redirect='https://media.cpaontario.ca/cpa-Members/pdfs/Form-9-1H.PDF';
        window.open(url_to_redirect,'_blank');
    },
})