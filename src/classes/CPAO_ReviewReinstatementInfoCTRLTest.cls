@isTest
public with sharing class CPAO_ReviewReinstatementInfoCTRLTest {
	public static List<Account> factoryAccounts;
	public static List<Contact> factoryContacts;
	public static User user;

	public static void dataInitialize(Integer numberOfAccounts, Integer numberOfContacts){
		CPAO_TestFactory.automationControlSetup();
		factoryAccounts = CPAO_TestFactory.createAccounts(numberOfAccounts);
		insert factoryAccounts;
		factoryContacts= CPAO_TestFactory.createContacts(factoryAccounts[0],numberOfContacts);
		insert factoryContacts;

		user = CPAO_TestFactory.createCommunityUser(factoryContacts[0].Id);
        insert user;

	}

	@isTest
	public static void standardTest() {
		dataInitialize(1,1);
		RecordType reinstatementRecordType = [SELECT Id FROM RecordType WHERE Name = :CPAO_ApplicationHandler.REINSTATEMENT AND SobjectType = 'CPAO_Application__c'][0];
		CPAO_Application__c application = new CPAO_Application__c(
			CPAO_Country__c = 'Australia',
			CPAO_Accounting_Body__c = 'CPA Australia',
			CPAO_Conditions__c = 'test',
			RecordTypeId = reinstatementRecordType.Id,
			CPAO_Status__c = 'Open');
		CPAO_Account_Contact_Relationship__c relationship = CPAO_TestFactory.createAccountContactRelationships(factoryAccounts[0], factoryContacts[0], 1)[0];
		insert relationship;
		CPAO_Declaration__c declaration = CPAO_TestFactory.createDeclarations(1, factoryContacts[0])[0];


		
		
		system.runAs(user) {
			application.CPAO_Legal_Given_Name_s__c = 'test';
			insert application;
			
			declaration.RecordTypeId = [SELECT Id FROM RecordType WHERE Name = :CPAO_DeclarationSelector.GOOD_CHARACTER AND SobjectType = 'CPAO_Declaration__c'][0].Id;
			declaration.CPAO_Application__c = application.Id;
			insert declaration;

			CPAO_ReviewReinstatementInfoCTRL testObject = new CPAO_ReviewReinstatementInfoCTRL();
			testObject.attested = true;
			testObject.save();
			testObject.previousStep();
	        
	    }
	}
}