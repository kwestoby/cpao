@isTest
private class CPAO_Application_reinstatement_EmailTest {
    public static List <Contact> Con = new List<Contact>();  
    public static List<Account> factoryAccounts;
    public static List<Contact> factoryContacts;
    public static List<CPAO_Application__c> Applications;
    public static CPAO_Application__c application1;
    
    public static void dataInitialize(Integer numberOfAccounts, Integer numberOfContacts){
    CPAO_TestFactory.automationControlSetup();
    factoryAccounts = CPAO_TestFactory.createAccounts(numberOfAccounts);
    insert factoryAccounts;
    factoryContacts= CPAO_TestFactory.createContacts(factoryAccounts[0],numberOfContacts);
    insert factoryContacts;
  }
    @isTest
    static void InsertApplication() {
    dataInitialize(1,1);
    RecordType amdCPDRecordType = [SELECT Id FROM RecordType WHERE DeveloperName = 'CPAO_Reinstatement_Application' AND SobjectType = 'CPAO_Application__c'];
    application1 = new CPAO_Application__c();
    application1.RecordTypeId = amdCPDRecordType.Id;
    application1.CPAO_Contact__c=factoryContacts[0].Id;
    application1.CPAO_Status__c = 'Completed';
    application1.CPAO_Recommendation__c= 'Approve';
    application1.CPAO_Recommendation_Notes__c= 'Test Rec';
    insert application1;
      CPAO_Licence__c Lic = new CPAO_Licence__c();
      Lic.CPAO_Issued_to__c=factoryContacts[0].Id;
      Lic.CPAO_Effective_Date__c= Date.today();
      Lic.CPAO_Related_Application__c=application1.id;
      Lic.CPAO_Status__c='Active';
      Lic.CPAO_Licence_Type__c='Public Accounting Licence';
      Insert Lic;
      System.debug('licence :1 :2'+Lic.CPAO_Status__c +Lic.CPAO_Licence_Type__c);
      System.AssertEquals('Public Accounting Licence',Lic.CPAO_Licence_Type__c);
    Applications = new List<CPAO_Application__c>();
    Applications.add(application1);
      
      Test.startTest();
            CPAO_Application_reinstatement_Email.Find_PAL_License(Applications);
      Test.stopTest();
      
    }
        
}