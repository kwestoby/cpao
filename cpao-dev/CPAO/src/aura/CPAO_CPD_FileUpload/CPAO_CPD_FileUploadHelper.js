({
    updateAPF : function(component, helper, isQuit) {
        // var action = component.get("c.updateFirmStructure");
        // action.setParams({
        //     // some stuff here
        // });
        // action.setCallback(this, function(response){
        //     var state = response.getState();
        //     var result = response.getReturnValue();
        //     var err = response.getError();
        //     if(state === "SUCCESS") {
                
        //         if(isQuit) {
        //             helper.fireNextEvent(component, "Quit");
        //         } else {
        //             helper.fireNextEvent(component, "Next");
        //         }
        //     }
        // });
        // // $A.enqueueAction(action);
        if(isQuit) {
            helper.fireNextEvent(component, "Quit");
        } else {
            helper.fireNextEvent(component, "Next");
        }
    },

    fireNextEvent : function(component, nextStep) {
        var nextButtonClick = $A.get("e.c:CPAO_Next_Button_Has_Been_Pressed");
        nextButtonClick.setParams({
            "componentName" : "CPAO_CPD_Application",
            "nextStepName" : nextStep});
        nextButtonClick.fire();
    },

    firePrevEvent : function(component) {
        var prevButtonClick = $A.get("e.c:CPAO_Prev_Button_Has_Been_Pressed");
        prevButtonClick.setParams({
            "componentName" : "CPAO_CPD_Application",
            "nextStepName" : "Prev"});
        prevButtonClick.fire();
    },
})