public class CPAO_ReceiptItemUpdateHelper{

    public static void updateItemReceipts(Map<Id, OrderApi__Item__c>  itemsObjs){
        
        try{
            List<OrderApi__Business_Group__c> businessObjList = new List<OrderApi__Business_Group__c>();
            Set<OrderApi__Business_Group__c> businessObjSet = new Set<OrderApi__Business_Group__c>();

            Map<Id,OrderApi__Business_Group__c> businessMap = New Map<Id,OrderApi__Business_Group__c>([Select Id, name from OrderApi__Business_Group__c]);

            for(OrderApi__Item__c orderitem: itemsObjs.values()){
                OrderApi__Business_Group__c businessGroupObj = businessMap.get(orderitem.OrderApi__Business_Group__c);
                businessGroupObj.OrderApi_Item_Name__c = orderitem.Name;
                businessObjSet.add(businessGroupObj);
            }
        
            if(businessObjSet.size() > 0){
                businessObjList.addALL(businessObjSet);
                update businessObjList;
            }
        }catch (Exception e) {
        
        }
    }
}