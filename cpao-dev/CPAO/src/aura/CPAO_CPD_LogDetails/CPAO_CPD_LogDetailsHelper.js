({
    updateAPF : function(component, helper, isQuit) {
        // var action = component.get("c.updateFirmStructure");
        // action.setParams({
        //     // some stuff here
        // });
        // action.setCallback(this, function(response){
        //     var state = response.getState();
        //     var result = response.getReturnValue();
        //     var err = response.getError();
        //     if(state === "SUCCESS") {
                
        //         if(isQuit) {
        //             helper.fireNextEvent(component, "Quit");
        //         } else {
        //             helper.fireNextEvent(component, "Next");
        //         }
        //     }
        // });
        // // $A.enqueueAction(action);
        
    },

    navigateNext : function(component, isQuit) {
        if(isQuit) {
            helper.fireNextEvent(component, "Quit");
        } else {
            helper.fireNextEvent(component, "Next");
        }
    },

    fireNextEvent : function(component, nextStep) {
        var nextButtonClick = $A.get("e.c:CPAO_Next_Button_Has_Been_Pressed");
        nextButtonClick.setParams({
            "componentName" : "CPAO_CPD_Application",
            "nextStepName" : nextStep});
        nextButtonClick.fire();
    },

    firePrevEvent : function(component) {
        var prevButtonClick = $A.get("e.c:CPAO_Prev_Button_Has_Been_Pressed");
        prevButtonClick.setParams({
            "componentName" : "CPAO_CPD_Application",
            "nextStepName" : "Prev"});
        prevButtonClick.fire();
    },

    handleNewLogSave : function(component, helper) {
        var logFields = component.find("requiredLogField");
        var isValid = true;
        for (var i = logFields.length - 1; i >= 0; i--) {
            var field = logFields[i];
            if(!field.get("v.value")) {
                isValid = false;
                break;
            }
        }
        if(!isValid) {
            component.set("v.errorMsg", "All fields are required");
        }
        else {
            helper.saveLog(component, helper);
        }
    },

    saveLog : function(component, helper) {
        // Do server saving stuff here
        var log = component.get("v.newLog");
        var logs = component.get("v.logs");

        logs.push(log);
        component.set("v.logs", logs);
    },

    getClickedIndex : function(component, event) {
        return event.currentTarget.dataset.index;
    },

    deleteLogs : function(component, helper, logs) {
        var action = component.get("c.deleteLogs");
        var logsJSON = JSON.stringify(logs);
        action.setParams({
            logsJSON : logsJSON
        });
        action.setCallback(this, function(response){
            var state = response.getState();
            var result = response.getReturnValue();
            var err = response.getError();
            if(state === "SUCCESS") {
                // refresh our list of logs
                component.set("v.logs", result);
            }
        });
        $A.enqueueAction(action);
    }
})