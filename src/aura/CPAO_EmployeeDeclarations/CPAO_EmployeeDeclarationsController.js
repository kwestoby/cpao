({
	doInit : function(component, event, helper) {
		console.log('first line of dec doinit');
		var apexAction = component.get('c.getDeclarations');
        apexAction.setCallback(this, function(response) {
        	console.log('fist line of callback');
        	var state = response.getState();
        	console.log(state);
        	if(state === "SUCCESS"){
                var eventList = response.getReturnValue();
                var openDeclarations = [];
                console.log('eventList');
                console.log(eventList);
                if(eventList.length > 0){
                	for(var i = 0; i<eventList.length; i++){
	                	if(eventList[i].CPAO_Status__c == 'Completed'){
	                		openDeclarations.push(eventList[i]);
	                	}
	                	
	                }
	                if(openDeclarations.length != 0){
	                	component.set("v.openDecs", openDeclarations);
	                }
                } else {
                	component.set("v.noDeclarations", true);
                }
            }
        });
        $A.enqueueAction(apexAction);
	},
})