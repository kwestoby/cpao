<?xml version="1.0" encoding="UTF-8"?>
<CustomMetadata xmlns="http://soap.sforce.com/2006/04/metadata" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xmlns:xsd="http://www.w3.org/2001/XMLSchema">
    <label>Other</label>
    <protected>false</protected>
    <values>
        <field>CPAO_Breach_Instruction__c</field>
        <value xsi:type="xsd:string">Please call CPAO to learn more about how to close this infraction.</value>
    </values>
    <values>
        <field>CPAO_Breach_Type__c</field>
        <value xsi:type="xsd:string">Other</value>
    </values>
</CustomMetadata>
