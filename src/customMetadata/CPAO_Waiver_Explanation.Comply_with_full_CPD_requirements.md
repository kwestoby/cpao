<?xml version="1.0" encoding="UTF-8"?>
<CustomMetadata xmlns="http://soap.sforce.com/2006/04/metadata" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xmlns:xsd="http://www.w3.org/2001/XMLSchema">
    <label>Comply with full CPD requirements</label>
    <protected>false</protected>
    <values>
        <field>AMD_or_CPD__c</field>
        <value xsi:type="xsd:string">CPD</value>
    </values>
    <values>
        <field>CPAO_Explanation__c</field>
        <value xsi:type="xsd:string">You comply with the annual CPD requirements. You have completed the annual minimum 20 hours, of which at least 50% was verifiable</value>
    </values>
    <values>
        <field>CPAO_Percent_Off__c</field>
        <value xsi:nil="true"/>
    </values>
    <values>
        <field>Explanation_Third_Year__c</field>
        <value xsi:type="xsd:string">You comply with the annual CPD requirements. You have completed the annual minimum 20 hours, of which at least 50% was verifiable.  You comply with the overall three-year CPD cycle requirements of 120 hours, of which at least 50% was verifiable.</value>
    </values>
    <values>
        <field>Full_Name__c</field>
        <value xsi:type="xsd:string">Comply with full CPD requirements</value>
    </values>
</CustomMetadata>
