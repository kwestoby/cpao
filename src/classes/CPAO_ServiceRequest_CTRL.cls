public class CPAO_ServiceRequest_CTRL {
	public static User LOGGED_USER =  CPAO_UserSelector.getCurrentUser();

	@auraEnabled
	public static User componentInit (){
		return LOGGED_USER;
	}
}