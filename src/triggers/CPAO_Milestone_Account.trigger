trigger CPAO_Milestone_Account on Account (before insert, before update) {

    //CPAO_Automation_Control__c control = CPAO_Automation_Control__c.getOrgDefaults();
    //if(!control.Account__c){
    //    return;
    //}
    Map<Id, Account> accountToEvaluate = new Map<Id, Account >();
    if(Trigger.isBefore&& Trigger.isUpdate){
        for(Account accObj : Trigger.New){
            accountToEvaluate.put(accObj.Id,accObj);
        }
    }  
    
    CPAO_MilestoneAccountHelper.historyTracker(accountToEvaluate);
   
   
}