trigger CPAO_Application_Reinstatement on CPAO_Application__c (before insert, before update) {
    List<CPAO_Application__c> Application = new List<CPAO_Application__c>();
    if(Trigger.isBefore && Trigger.isUpdate){
        for(CPAO_Application__c newApp : Trigger.New){
            CPAO_Application__c oldApp=  Trigger.oldMap.get(newApp.Id);
            System.debug('Application ID:'+newApp.Id );
            if(newApp.CPAO_Application_Type__c == 'Reinstatement' && newApp.CPAO_Recommendation__c=='Approve' && newApp.CPAO_Status__c=='Completed')
                if(newApp.CPAO_Application_Type__c == 'Reinstatement' && newApp.CPAO_Decision__c=='Approve' && newApp.CPAO_Status__c=='Completed'){
                    CPAO_Application_reinstatement_Email.Find_PAL_License(Trigger.New);
                    Trigger.new[0].OwnerId = UserInfo.getUserId();
                }
                     
        }
    }      
}