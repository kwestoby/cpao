@isTest
public with sharing class CPAO_TestFactory {
	public static String ACTIVE_STATUS = 'Active';
	
	// Used for preventing duplicate test data entries
	private static Integer UNIQUE_NUMBER {
		get {
			UNIQUE_NUMBER = UNIQUE_NUMBER == null ? 0 : UNIQUE_NUMBER + 1;
			return UNIQUE_NUMBER;
		}
		set;
	}

	public static void automationControlSetup() {
		insert new CPAO_Automation_Control__c(
		                                      SetupOwnerId = UserInfo.getOrganizationId(),
		                                      Account__c = true,
		                                      Case__c = true,
		                                      Contact__c = true,
		                                      CPAO_Account_Contact_Relationship_c__c = true,
		                                      CPAO_Account_to_Account_Relationship_c__c = true,
		                                      CPAO_Application_c__c = true,
		                                      CPAO_Declaration_c__c = true,
		                                      CPAO_Infraction_c__c = true,
		                                      CPAO_Infraction_Type_c__c = true,
		                                      CPAO_Insurance_c__c = true,
		                                      CPAO_Note_c__c = true,
		                                      CPAO_PPR_Programs_c__c = true,
		                                      CPD_Declaration_c__c = true,
		                                      EventApi_Attendee_c__c = true,
		                                      EventApi_Venue_c__c = true,
		                                      OrderApi_Receipt_c__c = true,
		                                      OrderApi_Subscription_c__c = true,
		                                      OrderApi_Sales_Order_c__c = true,
		                                      Task__c = true,
		                                      OrderApi_Item_c__c = true);
	}
	public static List<Account> createAccounts(Integer amount) {
		List<Account> accounts = new List<Account> ();
		for (Integer i = 0; i<amount; i++) {
			Account newAccount = new Account(Name = 'Test' + i);
			newAccount.CPAO_Account_Type__c = 'Non-Firm';
			newAccount.CPAO_Primary_Industry__c = 'Construction';
			newAccount.CPAO_Virtual__c = 'Yes';
			newAccount.CPAO_Account_Status__c = 'Active';
			newAccount.BillingStreet = '114 Mariott';
			newAccount.BillingCity = 'Chateauguay';
			newAccount.BillingState = 'Quebec';
			newAccount.BillingPostalCode = 'H2M 2K8';
			newAccount.BillingCountry = 'Canada';
			newAccount.CPAO_Preferred_Mailing_Address__c = 'Business';
			newAccount.CPAO_Corporate_Structure__c = 'Partnership';
			newAccount.Type_of_Services__c = 'Accounting Services';
			accounts.add(newAccount);
		}
		return accounts;
	}

	public static List<Contact> createContacts(Account account, Integer amount) {
		List<Contact> contacts = new List<Contact> ();
		for (Integer i = 0; i<amount; i++) {
			contacts.add(new Contact(LastName = 'Test' + i, FirstName = 'test', AccountId = account.Id));
		}
		List<Contact> validContacts = addContactStudentInfo(contacts);
		return validContacts;
	}

	public static List<Account> createAccountsPrimaryContact(List<Account> accounts) {
		List<Contact> contacts = new List<Contact> ();
		for (Integer i = 0; i<accounts.size(); i++) {
			contacts.add(new Contact(LastName = 'Test' + i, FirstName = 'test', AccountId = accounts[i].Id));
		}
		List<Contact> validContacts = addContactStudentInfo(contacts);
		insert validContacts;
		for (Integer i = 0; i<accounts.size(); i++) {
			accounts[i].OrderApi__Primary_Contact__c = validContacts[i].id;
		}
		return accounts;
	}

	public static List<Contact> addContactStudentInfo(List<Contact> contacts) {
		for (Contact contact : contacts) {
			contact.CPAO_CPA_Contact_Type__c = 'Student';
			contact.CPAO_Preferred_Mailing_Address__c = 'Residential';
			contact.MailingStreet = '123 test ave';
			contact.CPAO_Preferred_Phone__c = 'Business Phone';
			contact.Phone = '1234567890';
			contact.CPAO_CPA_Status__c = 'Active';
			contact.MailingPostalCode = 'A9A 9A9';
			contact.MailingState = 'Ontario';
			contact.MailingCity = 'Toronto';
			contact.CPAO_Legal_Given__c = 'test';
			contact.CPAO_Legal_Surname__c = 'test';
			contact.CPAO_Consent_type__c = 'No Consent';
			contact.Email = 'test@test.com';
		}
		return contacts;
	}
	public static List<Contact> addMemberInfo(List<Contact> contacts) {
		for (Contact contact : contacts) {
			contact.CPAO_CPA_Contact_Type__c = 'Member';
			contact.CPAO_CPA_Status__c = 'Active';
		}
		return contacts;
	}


	// this method creates unused ids. One of those  ids name is not unique and is contained
	// in a range that is not yet activated
	public static List<CPAO_CPA_Id__c> createNotUniqueIdRecords(Integer amount) {
		List<CPAO_CPA_Id__c> idRecordsList = new List<CPAO_CPA_Id__c> ();
		for (Integer i = 0; i<amount; i++) {
			idRecordsList.add(new CPAO_CPA_Id__c(Id_Name__c = i * 100));
		}
		return idRecordsList;
	}

	// this method creates id with unique ids names that are not contained in any range
	// those ids are unused so ready to be assigned
	public static List<CPAO_CPA_Id__c> createUnusedIdRecords(Integer amount) {
		List<CPAO_CPA_Id__c> idRecordsList = new List<CPAO_CPA_Id__c> ();
		for (Integer i = 1; i<amount + 1; i++) {
			idRecordsList.add(new CPAO_CPA_Id__c(Id_Name__c = i * 100));
		}
		return idRecordsList;
	}

	// this method creates id with unique ids names that are not contained in any range
	// those ids are active so we cannot assign then
	public static List<CPAO_CPA_Id__c> createActiveIdRecords(Integer amount) {
		List<CPAO_CPA_Id__c> idRecordsList = new List<CPAO_CPA_Id__c> ();
		for (Integer i = 6; i<amount + 6; i++) {
			idRecordsList.add(new CPAO_CPA_Id__c(Id_Name__c = i * 100, Status__c = ACTIVE_STATUS));
		}
		return idRecordsList;
	}

	public static List<CPAO_CPA_Range_Details__c> createRange(Integer amount) {
		List<CPAO_CPA_Range_Details__c> rangeList = new List<CPAO_CPA_Range_Details__c> ();
		for (Integer i = 0; i<amount; i++) {
			CPAO_CPA_Range_Details__c rangeToAdd = new CPAO_CPA_Range_Details__c(Name = 'Range ' + i);
			rangeToAdd.Range_Start__c = i * (10);
			rangeToAdd.Range_End__c = i * (10) + 9;
			rangeList.add(rangeToAdd);
		}
		rangeList[0].Active__c = true;
		return rangeList;
	}

	public static List<CPAO_CPA_Range_Details__c> addNextRange(List<CPAO_CPA_Range_Details__c> actualRanges) {

		for (Integer i = 0; i<actualRanges.size(); i++) {
			if (i != actualRanges.size() - 1) {
				actualRanges[i].Next_Range_Id__c = actualRanges[i + 1].Name;
			}
		}
		return actualRanges;

	}

	public static CPA_Id_Warning_Email__c createEmailDetails() {

		CPA_Id_Warning_Email__c warning_email_details = new CPA_Id_Warning_Email__c();
		warning_email_details.Warning_Email_send_to_address__c = 'haliima_d@live.fr';
		warning_email_details.Warning_email_threshold__c = 2000;
		warning_email_details.CPA_Warning_Email_Subject__c = 'Number of unused CPAO Ids reached treshold';

		return warning_email_details;
	}

	public static List<CPAO_Account_Contact_Relationship__c> createAccountContactRelationships(
	                                                                                           Account account, Contact contact, Integer amount) {

		List<CPAO_Account_Contact_Relationship__c> relationships = new List<CPAO_Account_Contact_Relationship__c> ();
		for (Integer i = 0; i<amount; i++) {
			relationships.add(new CPAO_Account_Contact_Relationship__c(
			                                                           CPAO_Account__c = account.Id,
			                                                           CPAO_Contact__c = contact.Id,
			                                                           CPAO_Relationship_to_Account__c = 'Employee',
			                                                           CPAO_Text__c = 'employee',
			                                                           CPAO_Business_Unit__c = 'Assurance, Business Valuation',
			                                                           CPAO_Position_Category__c = 'Associate Partner',
			                                                           CPAO_Start_Date__c = Date.today()));
		}
		return relationships;
	}

	/*
	  //Deprecated 
	  //This method has been commented out because Communication Preferences Object is not used anymore
	  //The functionalities have been moved over to Contact Object. Therefore, the test methods have been
	  //modified accordingly
	  public static List<CPAO_Communications_Preferences__c> createCommPreferenceImplied( Integer amount, String consentType, List<Contact> contacts,Date consentDate){
	 
	  List<CPAO_Communications_Preferences__c> communicationPreferences = new List<CPAO_Communications_Preferences__c>();
	  for(Integer i=0; i<amount; i++){
	  communicationPreferences.add(new CPAO_Communications_Preferences__c(
	  Associated_Contact__c = contacts[i].Id,
	  CPAO_Consent_type__c = consentType,
	  CPAO_Consent_Date__c = consentDate,
	  CPAO_Source__c='Portal Usage'
	  )); // more than 181 days from now
	 
	  }
	  return communicationPreferences;
	  }*/
	public static List<Contact> createCommPreferenceImplied(Integer amount, String consentType, List<Contact> contacts, Date consentDate) {

		for (Contact contact : contacts) {
			contact.CPAO_Consent_type__c = consentType;
			contact.CPAO_Consent_Date__c = consentDate;
			contact.CPAO_Source__c = 'Portal Usage';
		}

		return contacts;
	}

	public static User createCommunityUser(Id contactId) {
		Id p = [select id from profile where name = 'Partner Community User'].id;
		return new User(
		                alias = 'test123',
		                email = 'test123@testmail.com',
		                emailencodingkey = 'UTF-8',
		                lastname = 'Testing',
		                languagelocalekey = 'en_US',
		                localesidkey = 'en_US',
		                profileid = p,
		                country = 'Canada',
		                IsActive = true,
		                ContactId = contactId,
		                timezonesidkey = 'America/Los_Angeles',
		                username = 'tester@testmail.com'+UNIQUE_NUMBER);
	}

	public static List<OrderApi__Subscription__c> createSubscriptions(Integer amount, Contact contact) {
		List<OrderApi__Subscription__c> subscriptions = new List<OrderApi__Subscription__c> ();
		for (Integer i = 0; i<amount; i++) {
			subscriptions.add(new OrderApi__Subscription__c(
			                                                OrderApi__Contact__c = contact.Id,
			                                                OrderApi__Current_Term_End_Date__c = Date.today().addDays(- 1),
			                                                OrderApi__Grace_Period_End_Date__c = Date.today().addDays(1),
			                                                CPAO_Effective_Date__c = Date.today(),
			                                                OrderApi__Expired_Date__c = Date.today()
			));
		}
		return subscriptions;
	}

	public static List<CPAO_Infraction__c> createInfractions(Integer amount, CPAO_Infraction_Type__c infractionType) {
		List<CPAO_Infraction__c> infractions = new List<CPAO_Infraction__c> ();
		for (Integer i = 0; i<amount; i++) {
			infractions.add(new CPAO_Infraction__c(CPAO_Infraction_Type__c = infractionType.Id));
		}
		return infractions;
	}

	public static List<CPAO_Infraction_Type__c> createInfractionTypes(Integer amount, String name) {
		List<CPAO_Infraction_Type__c> infractionTypes = new List<CPAO_Infraction_Type__c> ();
		for (Integer i = 0; i<amount; i++) {
			infractionTypes.add(new CPAO_Infraction_Type__c(Name = name));
		}
		return infractionTypes;
	}

	public static List<CPAO_Declaration__c> createDeclarations(Integer amount, Contact contact) {
		List<CPAO_Declaration__c> declarations = new List<CPAO_Declaration__c> ();
		for (Integer i = 0; i<amount; i++) {
			declarations.add(new CPAO_Declaration__c(CPAO_Contact__c = contact.Id));
		}
		return declarations;
	}

	public static List<EventApi__Event__c> createEvents(Integer amount) {
		List<EventApi__Event__c> returnedEvents = new List<EventApi__Event__c> ();
		EventApi__Event__c event = new EventApi__Event__c();
		EventApi__Event_Category__c eventCategory = new EventApi__Event_Category__c(Name = 'Default Lightning Event Category');
		for (Integer i = 0; i<amount; i++) {
			event.Name = 'UnitTestEvent' + i;
			event.EventApi__Registration_Style__c = 'Lightning Event';
			event.EventApi__Event_Key__c = '76abe256-bz6a-8';
			event.EventApi__Start_Date__c = date.newinstance(2018, 3, 17);
			event.EventApi__Start_Date_Time__c = DateTime.newInstance(2018, 3, 17, 7, 0, 0);
			event.EventApi__End_Date__c = date.newinstance(2018, 3, 17);
			event.EventApi__End_Date_Time__c = DateTime.newInstance(2018, 5, 29, 7, 0, 0);
			event.EventApi__Start_Time__c = '07:00';
			event.EventApi__End_Time__c = '07:00';
			event.EventApi__Event_Category__c = eventCategory.Id;
			returnedEvents.add(event);
		}
		return returnedEvents;
	}

	public static List<EventApi__Venue__c> createVenues(Integer amount, EventApi__Event__c event, Boolean primaryVenue) {
		List<EventApi__Venue__c> returnedVenues = new List<EventApi__Venue__c> ();
		EventApi__Venue__c venue = new EventApi__Venue__c();
		for (Integer i = 0; i<amount; i++) {
			venue.name = 'UnitTestVenue' + i;
			venue.EventApi__Is_Primary_Venue__c = primaryVenue;
			venue.EventApi__Event__c = event.Id;
			returnedVenues.add(venue);
		}

		return returnedVenues;
	}

	public static List<CPAO_Licence__c> createInactiveLicences(Integer amount, Contact contact, CPAO_Application__c application) {
		List<CPAO_Licence__c> returnedLicences = new List<CPAO_Licence__c> ();
		CPAO_Licence__c associatedLicence = new CPAO_Licence__c();
		for (Integer i = 0; i<amount; i++) {

			associatedLicence.CPAO_Related_Application__c = application.Id;
			associatedLicence.CPAO_Status__c = 'Active';
			associatedLicence.CPAO_Sub_Status__c = 'Original';
			associatedLicence.CPAO_Licence_Type__c = 'Public Accounting Licence';
			associatedLicence.CPAO_Issued_to__c = contact.Id;
			associatedLicence.CPAO_Effective_Date__c = date.newinstance(2018, 5, 7); //Date.today();
			associatedLicence.CPAO_Expiry_Date__c = Date.today();
			returnedLicences.add(associatedLicence);
		}

		return returnedLicences;
	}
	public static List<CPAO_Licence__c> createActiveLicences(Integer amount, Contact contact, CPAO_Application__c application) {
		List<CPAO_Licence__c> returnedLicences = new List<CPAO_Licence__c> ();
		CPAO_Licence__c associatedLicence = new CPAO_Licence__c();
		for (Integer i = 0; i<amount; i++) {

			associatedLicence.CPAO_Related_Application__c = application.Id;
			associatedLicence.CPAO_Status__c = 'Active';
			associatedLicence.CPAO_Sub_Status__c = 'Original';
			associatedLicence.CPAO_Licence_Type__c = 'Public Accounting Licence';
			associatedLicence.CPAO_Issued_to__c = contact.Id;
			associatedLicence.CPAO_Effective_Date__c = date.newinstance(2018, 5, 7); //Date.today();
			associatedLicence.CPAO_Expiry_Date__c = Date.newinstance(2028, 5, 7);
			returnedLicences.add(associatedLicence);
		}

		return returnedLicences;
	}


	public static List<CPAO_Application__c> createPALApplication(Integer amount, Contact contact) {
		List<RecordType> PalRecordType = CPAO_ApplicationSelector.getPALApplicationRecordType();
		CPAO_Application__c newApplication = new CPAO_Application__c();

		List<CPAO_Application__c> returnedApplications = new List<CPAO_Application__c> ();
		for (Integer i = 0; i<amount; i++) {
			newApplication.RecordTypeId = PalRecordType[0].Id;
			newApplication.CPAO_Contact__c = contact.Id;
			newApplication.CPAO_Status__c = 'Open';
			returnedApplications.add(newApplication);
		}
		return returnedApplications;
	}

	public static List<CPAO_Application__c> createReadmissionApplication(Integer amount, Contact contact) {
		Id readmissionRecordTypeId = CPAO_ApplicationHandler.getApplicationRecordTypeId(CPAO_ApplicationHandler.READMISSION_RECORDTYPENAME);
		List<CPAO_Application__c> returnedApplications = new List<CPAO_Application__c> ();
		for (Integer i = 0; i<amount; i++) {
			CPAO_Application__c newApplication = new CPAO_Application__c();
			newApplication.RecordTypeId = readmissionRecordTypeId;
			newApplication.CPAO_Status__c = 'Open';
			returnedApplications.add(newApplication);
		}
		return returnedApplications;
	}

	public static List<CPAO_Application__c> createAMDCPDpplication(Integer amount, Contact contact) {
		List<RecordType> AmdCpdRecordType = CPAO_ApplicationSelector.getAMDCPDApplicationRecordType();

		List<CPAO_Application__c> returnedApplications = new List<CPAO_Application__c> ();
		for (Integer i = 0; i<amount; i++) {
			CPAO_Application__c newApplication = new CPAO_Application__c();
			newApplication.RecordTypeId = AmdCpdRecordType[0].Id;
			newApplication.CPAO_Status__c = 'Submitted-Payment Deferred';
			newApplication.CPAO_Contact__c = contact.id;
			returnedApplications.add(newApplication);
		}
		return returnedApplications;
	}

	public static List<CPAO_Application__c> createAMDCPDpplication(Integer amount) {
		List<RecordType> AmdCpdRecordType = CPAO_ApplicationSelector.getAMDCPDApplicationRecordType();

		List<CPAO_Application__c> returnedApplications = new List<CPAO_Application__c> ();
		for (Integer i = 0; i<amount; i++) {
			CPAO_Application__c newApplication = new CPAO_Application__c();
			newApplication.RecordTypeId = AmdCpdRecordType[0].Id;
			newApplication.CPAO_Status__c = 'Submitted-Payment Deferred';
			returnedApplications.add(newApplication);
		}
		return returnedApplications;
	}


	public static List<OrderApi__Receipt__c> createReceipt(Integer amount, String receiptType, List<Account> listAccounts) {
		List<OrderApi__Receipt__c> listReceipt = new List<OrderApi__Receipt__c> ();
		for (integer i = 0; i<amount; i++) {
			OrderApi__Receipt__c rec = new OrderApi__Receipt__c(OrderApi__Account__c = listAccounts[i].id, OrderApi__Type__c = receiptType);
			listReceipt.add(rec);
		}
		return listReceipt;
	}


	public static User createUser(String userName,String profile) {
		Id p = [select id from profile where name = :profile].id;
		return new User(
		                alias = 'testcpao',
		                email = 'testCPAO@testmail.com',
		                emailencodingkey = 'UTF-8',
		                lastname = 'Testing',
		                languagelocalekey = 'en_US',
		                localesidkey = 'en_US',
		                profileid = p,
		                country = 'Canada',
		                IsActive = true,
		                timezonesidkey = 'America/Los_Angeles',
		                username = userName);
	}

	public static List<CPAO_Account_to_Account_Relationship__c> createAccountToAccountRelationships(Account parent, List<Account> children) {
		return createAccountToAccountRelationships(parent, children, null);
	}

	public static List<CPAO_Account_to_Account_Relationship__c> createAccountToAccountRelationships(Account parent, List<Account> children, String relationship) {
		List<CPAO_Account_to_Account_Relationship__c> relationships = new List<CPAO_Account_to_Account_Relationship__c>();
		for(Account child : children) {
			relationships.add(createAccountToAccountRelationship(parent.Id, child.Id, relationship));
		}
		return relationships;
	}

	public static CPAO_Account_to_Account_Relationship__c createAccountToAccountRelationship(Id parentId, Id childId) {
		return createAccountToAccountRelationship(parentId, childId, null);
	}

	public static CPAO_Account_to_Account_Relationship__c createAccountToAccountRelationship(Id parentId, Id childId, String relationship) {
		if(relationship == null) { 
			relationship = 'Related Business'; 
		}
		
		return new CPAO_Account_to_Account_Relationship__c(
			CPAO_Account_Name__c = parentId,
			CPAO_Associated_Account_Name__c = childId,
			CPAO_Start_Date__c = Date.today(),
			CPAO_End_Date__c = Date.today().addDays(7),
			CPAO_Relationship_to_Associated_Account__c = relationship
		);
	}




}