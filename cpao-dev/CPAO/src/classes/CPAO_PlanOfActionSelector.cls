public without sharing class CPAO_PlanOfActionSelector {

	public static List<Plan_of_Action_Items__c> getPOAsByApplicationId(Id applicationId) {

		return [SELECT Id, CPAO_Course_Name__c, CPOA_CPD_Hours__c, CPAO_CPD_Provider_or_Sponsoring__c, CPAO_End_Date__c, 
					CPAO_Research_Area__c, CPAO_Start_Date__c, CPAO_Verifiable__c, CPAO_CPD_Unverifiable_Hours__c, 
					CPAO_CPD_Verifiable_Hours__c, CPAO_Sum__c
				FROM Plan_of_Action_Items__c
				WHERE CPAO_Application__c = :applicationId
				ORDER BY CreatedDate];
	}
}