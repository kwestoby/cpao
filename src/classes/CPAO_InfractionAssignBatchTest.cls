@isTest
private class CPAO_InfractionAssignBatchTest {
	
	public static List<Account> accounts;
	public static List<Contact> contacts;
	public static List<OrderApi__Subscription__c> subscriptions;

	public static List<CPAO_Infraction_Type__c> infractionTypes;

	static void dataInitialize(Integer contactAmount){
		CPAO_TestFactory.automationControlSetup();
		accounts = CPAO_TestFactory.createAccounts(1);
		insert accounts;
		contacts = CPAO_TestFactory.createContacts(accounts[0], contactAmount);
		insert contacts;
		String AMD = CPAO_InfractionAssignBatch.AMD;
		String APF = CPAO_InfractionAssignBatch.APF;
		String PIF = CPAO_InfractionAssignBatch.PIF;
		OrderApi__Item_Class__c itemClass = new OrderApi__Item_Class__c(Name = CPAO_InfractionAssignBatch.AMD);
		insert itemClass;
		
		List<OrderApi__Item__c> itemList = new List<OrderApi__Item__c>();
		itemList.add(new OrderApi__Item__c(Name = AMD, OrderApi__Item_Class__c = itemClass.Id));
		itemList.add(new OrderApi__Item__c(Name = APF, OrderApi__Item_Class__c = itemClass.Id));
		itemList.add(new OrderApi__Item__c(Name = PIF, OrderApi__Item_Class__c = itemClass.Id));
		insert itemList;

		OrderApi__Subscription_Plan__c subPlan = CPAO_FontevaTestFactory.createSubPlan();
    	subPlan.Name = 'AMD';
    	insert subPlan;

		subscriptions = CPAO_TestFactory.createSubscriptions(3, contacts[0]);
		for(Integer i = 0; i<3; i++){
			subscriptions[i].OrderApi__Item__c = itemList[i].Id;
			subscriptions[i].OrderApi__Subscription_Plan__c = subPlan.Id;
		}
		insert subscriptions;

		infractionTypes = new List<CPAO_Infraction_Type__c>();
		infractionTypes.add(CPAO_TestFactory.createInfractionTypes(1, AMD)[0]);
		infractionTypes.add(CPAO_TestFactory.createInfractionTypes(1, APF)[0]);
		infractionTypes.add(CPAO_TestFactory.createInfractionTypes(1, PIF)[0]);

		insert infractionTypes;

	}

	@isTest
	static void itShould() {
		dataInitialize(1);
		Test.startTest();
		CPAO_InfractionAssignBatch b = new CPAO_InfractionAssignBatch();
		database.executebatch(b);
		Test.stopTest();
		subscriptions = [SELECT Id, CPAO_Infraction_Created__c FROM OrderApi__Subscription__c WHERE Id IN :subscriptions];
		for(Integer i = 0; i<3; i++){
			System.assert(subscriptions[i].CPAO_Infraction_Created__c == true);
		}
		List<CPAO_Infraction__c> infractionsCreatedFromBatch = [SELECT Id FROM CPAO_Infraction__c WHERE CPAO_Contact__c = :contacts[0].Id];
		System.assert(infractionsCreatedFromBatch.size() == 3);
	}
}