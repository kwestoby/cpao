public with sharing class CPAO_IDRecordController {
	public static String UNUSED_STATUS = 'Unused';

	public static String idAlreadyExists = 'The Id you are trying to use already exists';
	public static String noCurrentActiveRange = 'there is no active CPA Id range specified';
	public static String noNextActiveRange = 'there is no follow up CPA Id range';
	public static String nextActiveRangeNotSpecified = 'the next active range is not specified on the current range record';
	public static String rangeSizeError = 'the next range start is larger than its end, please reverse the values';
	public static String warningEmailError = 'No treshold or email adress were found ';
 	public static String errorEmail='The email failed to send:';
	//This method accepts calls from CPAO_CPAIdRecordAssigner which ask for a list of Id records in the "proper order". 
	//If enough Id Records exist, then those Ids are returned (line 24)

	//If there are not enough unused Id records, then the get new id method is called. 
	//Once the required Id records are created, they are returned to teh calling method

	public static List<CPAO_CPA_Id__c> getIds(Integer numberOfIdsNeeded) {

		List<CPAO_CPA_Id__c> availableIds = CPAO_CPAIdSelector.getUnusedIds();
		List<CPAO_CPA_Id__c> idsToReturn = new List<CPAO_CPA_Id__c>();

		//if(availableIds) TODO --------------------------
		//---------
		if(availableIds.size() >= numberOfIdsNeeded){

			for(Integer i = 0; i<numberOfIdsNeeded; i++){
				idsToReturn.add(availableIds[i]);
			}
			return idsToReturn;

		} else if(availableIds.size()>0){
			idsToReturn = availableIds.clone();
		}

		createNewIdRecords();
		//this list contains newly created ids and will be used later on to determine if number of newly
		//inserted IDs is enough or if there is a need to create more from another range
		List<CPAO_CPA_Id__c> availableIds2 = CPAO_CPAIdSelector.getUnusedIds();
		while(availableIds2.size()<numberOfIdsNeeded){
			createNewIdRecords();
			 availableIds2 = CPAO_CPAIdSelector.getUnusedIds();
		}
		for(CPAO_CPA_Id__c id:getNewIds(numberOfIdsNeeded - availableIds.size(), availableIds)){
			idsToReturn.add(id);
		}
		return idsToReturn;

	}

	//This method accepts calls from CPAO_CPAIdRecordAssigner which asks for Id records with specified values
	//If records with those values exist and are used then an error is thrown
	//if none of the Id values were used, then this method grabs the already created and unused Ids, 
	//inserts new ones as needed, and returns them to the calling class
	public static Map<Decimal, CPAO_CPA_Id__c> getIdsManual(Set<Decimal> neededIds) {
		List<CPAO_CPA_Id__c> existingIds = CPAO_CPAIdSelector.getExistingIdsFromSet(neededIds);
		Map<Decimal, CPAO_CPA_Id__c> idsToReturn = new Map<Decimal, CPAO_CPA_Id__c>();
		Boolean conflict = false;
		for(CPAO_CPA_Id__c idRecord:existingIds){
			if(idRecord.Status__c != UNUSED_STATUS){
				throw new CPAO_IDRecordController_Exception(idAlreadyExists); 
			} else {
				idsToReturn.put(idRecord.Id_Name__c, idRecord);
			}
		}

		for(Decimal neededId:neededIds){
			if(!idsToReturn.containsKey(neededId)){
				idsToReturn.put(neededId, new CPAO_CPA_Id__c(Id_Name__c = neededId, Status__c = UNUSED_STATUS));
			}			
		}
		return idsToReturn;
	}

	//if new ids are needed in a sequential manner, this method looks for the current active range of Ids.
	//this record should have the ID of the next range to use, which is then searched for. 
	//if there is none then throw error
	//mark the old range as inactive, mark the new range active, and then create Id records in that range.
	//additional logic is needed for situations where an Id was already created in the past as a result of manually being set
	public static void createNewIdRecords() {
		List<CPAO_CPA_Range_Details__c> rangesToUpdate = new List<CPAO_CPA_Range_Details__c>();
		List<CPAO_CPA_Range_Details__c> activeRange = CPAO_CPARangeDetailSelector.getActiveRange();
		if(activeRange.size()!=1){
			throw new CPAO_IDRecordController_Exception(noCurrentActiveRange); 
		}
		if(activeRange[0].Next_Range_Id__c == null){
			throw new CPAO_IDRecordController_Exception(nextActiveRangeNotSpecified);
		}
		
		List<CPAO_CPA_Id__c> existingIdInActiveRange = CPAO_CPAIdSelector.getExistingIdsInRange(activeRange[0].Range_Start__c,activeRange[0].Range_End__c);

		List<CPAO_CPA_Id__c> newIdsToinsert = new List<CPAO_CPA_Id__c>();

		//if(existingIdInActiveRange.size()==0){
		if(existingIdInActiveRange.size()<=activeRange[0].Range_End__c-activeRange[0].Range_Start__c){
			for(Integer j=0;j<=activeRange[0].Range_End__c-activeRange[0].Range_Start__c;j++){
				newIdsToinsert.add(new CPAO_CPA_Id__c(
					Id_Name__c = activeRange[0].Range_Start__c + j,
					Status__c = UNUSED_STATUS));
			}
			insert newIdsToinsert;
			return;

		}

		activeRange[0].Active__c = false;


		rangesToUpdate.add(activeRange[0]);

		List<CPAO_CPA_Range_Details__c> nextRange = CPAO_CPARangeDetailSelector.getNextRange(activeRange[0].Next_Range_Id__c);

		if(nextRange.size()!=1){
			throw new CPAO_IDRecordController_Exception(noNextActiveRange); 
		}
		if((nextRange[0].Range_Start__c-nextRange[0].Range_End__c) > 0){
			throw new CPAO_IDRecordController_Exception(rangeSizeError); 
		}
		nextRange[0].Active__c = true;
		rangesToUpdate.add(nextRange[0]);

		List<CPAO_CPA_Id__c> existingIdsInRange = CPAO_CPAIdSelector.getExistingIdsInRange(nextRange[0].Range_Start__c, nextRange[0].Range_End__c);
		Set<Decimal> existingIdSet = new Set<Decimal>();
		if(!existingIdsInRange.isEmpty()){

			for(CPAO_CPA_Id__c idRecord:existingIdsInRange){
				existingIdSet.add(idRecord.Id_Name__c);
			}
		}

		//List<CPAO_CPA_Id__c> newIdsToinsert = new List<CPAO_CPA_Id__c>();
		for(Integer i=0; i<(nextRange[0].Range_End__c-nextRange[0].Range_Start__c); i++){
			if(!existingIdSet.contains(nextRange[0].Range_Start__c + i)){
				newIdsToinsert.add(new CPAO_CPA_Id__c(
					Id_Name__c = nextRange[0].Range_Start__c + i,
					Status__c = UNUSED_STATUS));
			}
		}
		update rangesToUpdate;
		insert newIdsToinsert;
	}

	//If a new range of Ids was needed, this queries for the first few Ids in that range and returns them
	// TODO - this method should be refactored to be part of the create new ids method
	public static List<CPAO_CPA_Id__c> getNewIds(Integer numberOfNewIdsNeeded, List<CPAO_CPA_Id__c> availableIds) {
		if(availableIds.isEmpty()){
			return CPAO_CPAIdSelector.getUnusedIds();
		}
		Set<Decimal> currentIds = new Set<Decimal>();
		for(CPAO_CPA_Id__c existingId:availableIds){
			currentIds.add(existingId.Id_Name__c);
		}
		return CPAO_CPAIdSelector.getUnusedIdsExceptOldRange(currentIds);
	}

	@future
	public static void sendIdWarningEmail() {
		//get threshould number from custom setting,search number of existing unused Ids and then check if there is a next acrive range
		List<CPAO_CPA_Id__c> unusedIdList = CPAO_CPAIdSelector.getUnusedIds();
		List<CPA_Id_Warning_Email__c> warningEmailDetails = CPAO_CPAWarningEmailSelector.getCurrentEmailDetails();
		if(warningEmailDetails.size()==0){
			throw new CPAO_IDRecordController_Exception(warningEmailError); 
		}
		if(unusedIdList.size()>=warningEmailDetails[0].Warning_email_threshold__c){
			return;
		}else{
			List<CPAO_CPA_Range_Details__c> activeRange=CPAO_CPARangeDetailSelector.getActiveRange();
			if(!activeRange.isEmpty()){
				//throw new CPAO_IDRecordController_Exception(noCurrentActiveRange); 
				List<CPAO_CPA_Range_Details__c> nextRange =CPAO_CPARangeDetailSelector.getNextRange(activeRange[0].Next_Range_Id__c);
				if(nextRange.size()>0){
					return;
				}
			} else{
				system.debug('UNUSED IDS LESS THAN TRESHOLD');
					Messaging.SingleEmailMessage message = new Messaging.SingleEmailMessage();
					message.toAddresses = new String[] {warningEmailDetails[0].Warning_Email_send_to_address__c };
					message.subject = warningEmailDetails[0].CPA_Warning_Email_Subject__c;
					message.plainTextBody =warningEmailDetails[0].CPA_Warning_Email_Body__c;
					/*message.plainTextBody = 'HI.The number of unused CPAO Ids have reached the  treshold of '
					+warningEmailDetails[0].Warning_email_threshold__c+' ids. Please acquire more as there is only '+
					unusedIdList.size()+ ' ids remaining';*/
					Messaging.SingleEmailMessage[] messages =   new List<Messaging.SingleEmailMessage> {message};
					Messaging.SendEmailResult[] results = Messaging.sendEmail(messages);
					if (results[0].success) {
					    System.debug('The email was sent successfully.');
					} else {
						throw new CPAO_IDRecordController_Exception(errorEmail+results[0].errors[0].message);   
					}
				
			}			
		}
	}

	public class CPAO_IDRecordController_Exception extends Exception {}
}