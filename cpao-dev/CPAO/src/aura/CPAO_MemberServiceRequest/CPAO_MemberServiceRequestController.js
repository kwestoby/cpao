({
	doInit : function(component,event,helper){
        
        var action = component.get("c.componentInit");
        action.setCallback(this, function(response){
            var state = response.getState();
            if (state === "SUCCESS") {
               
                component.set('v.contact',response.getReturnValue());
                console.log(JSON.stringify(response.getReturnValue()));
         
                  
              }else{
                  console.log('error while doinit Main container');
                  var error= response.getError();
                  console.log(error[0]);
                  component.set('v.errorMsg', error[0].message);
              }
        });
        
        $A.enqueueAction(action);
        
        
    }
})