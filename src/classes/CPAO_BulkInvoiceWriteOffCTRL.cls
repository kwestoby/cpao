/**
 * @author Sachet Khanal (sakhanal@deloitte.com)
 * @version 1.0.0
 * @description Controller class used by lightning component for handling bulk invoice write offs.
 *
 *
 **/ 
public class CPAO_BulkInvoiceWriteOffCTRL {

	private static final String BATCH_CLASS = 'CPAO_BulkInvoiceWriteOffBatch';

	@AuraEnabled
	public static InvoiceQueryResponse getInvoices(String query) {
		//System.debug('InvoiceQueryResponse::getInvoices()');
		try {
			InvoiceQuery invQuery = (InvoiceQuery) Json.deserialize(query, InvoiceQuery.class);
			//System.debug(invQuery);
			ApexPages.StandardSetController invoiceStdSetCtrl = new ApexPages.StandardSetController(invQuery.getQueryLocator());
			invoiceStdSetCtrl.setPageSize(invQuery.pageSize);
			invoiceStdSetCtrl.setPageNumber(invQuery.pageNumber);
			InvoiceQueryResponse response = new InvoiceQueryResponse((List<OrderApi__Invoice__c>) invoiceStdSetCtrl.getRecords(), invoiceStdSetCtrl.getResultSize());
			return response;
		} catch(Exception ex) {
			throw new AuraHandledException('Error! ' + ex.getMessage());
		}
	}

	@AuraEnabled
	public static List<AsyncApexJob> getPendingJobs() {
		List<AsyncApexJob> jobs = [SELECT Id, JobItemsProcessed, TotalJobItems, Status, ExtendedStatus
		                           FROM AsyncApexJob
		                           WHERE ApexClass.Name = :BATCH_CLASS
		                           AND JobType = 'BatchApex'
		                           AND Status NOT IN('Completed', 'Aborted', 'Failed') 
								   ORDER BY CreatedDate DESC];
		return jobs;
	}

	@AuraEnabled
	public static void writeOffInvoices(List<Id> invoiceIDs) {
		CPAO_BulkInvoiceWriteOffBatch batchable = new CPAO_BulkInvoiceWriteOffBatch(invoiceIDs);
        Database.executeBatch(batchable);
	}

	public class InvoiceQueryResponse {
		@AuraEnabled public List<OrderApi__Invoice__c> invoices;
		@AuraEnabled public Integer totalRecords;

		public InvoiceQueryResponse(List<OrderApi__Invoice__c> invoices, Integer totalRecords) {
			this.invoices = invoices;
			this.totalRecords = totalRecords;
		}
	}

	public class InvoiceQuery {
		private String searchKey;
		private List<String> invoiceStatus;
		private List<String> memberStatus;
		private Date postedFrom;
		private Date postedTo;
		private String sortBy;
		private String sortOrder;
		private Integer pageNumber;
		private Integer pageSize;

		public Database.QueryLocator getQueryLocator() {
			String queryStr = 'SELECT Id, Name, OrderApi__Contact__c, OrderApi__Contact__r.Name, ' +
			'OrderApi__Account__c, OrderApi__Account__r.Name, OrderApi__Total__c, OrderApi__Balance_Due__c, OrderApi__Posted_Date__c ';
			queryStr += ' FROM OrderApi__Invoice__c WHERE OrderApi__Balance_Due__c>0';
			if (!String.isEmpty(searchKey)) {
				queryStr += ' AND (Name LIKE \'%' + String.escapeSingleQuotes(searchKey.trim()) + '%\'';
				queryStr += ' OR OrderApi__Contact__r.Name LIKE \'' + String.escapeSingleQuotes(searchKey.trim()) + '%\'';
				queryStr += ' OR OrderApi__Contact__r.FirstName LIKE \'' + String.escapeSingleQuotes(searchKey.trim()) + '%\'';
				queryStr += ' OR OrderApi__Contact__r.LastName LIKE \'' + String.escapeSingleQuotes(searchKey.trim()) + '%\'';
				queryStr += ')';
			}
			if (postedFrom != null) {
				queryStr += ' AND OrderApi__Posted_Date__c>=:postedFrom';
			}
			if (postedTo != null) {
				queryStr += ' AND OrderApi__Posted_Date__c<=:postedTo';
			}
			if (invoiceStatus != null && invoiceStatus.size() > 0) {
				queryStr += ' AND OrderApi__Status__c IN :invoiceStatus';
			}
			if (memberStatus != null && memberStatus.size() > 0) {
				queryStr += ' AND OrderApi__Contact__r.CPAO_CPA_Status__c IN :memberStatus';
			}
			queryStr += (!String.isEmpty(sortBy)) ? ' ORDER BY ' + String.escapeSingleQuotes(sortBy) : '';
			queryStr += (!String.isEmpty(sortBy) && !String.isEmpty(sortOrder)) ? ' ' + String.escapeSingleQuotes(sortOrder) : '';
			System.debug(queryStr);
			return Database.getQueryLocator(queryStr);
		}
	}
}