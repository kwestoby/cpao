({
	doInit : function(component, event, helper) {
		var apexAction = component.get('c.getCPDandPOAinfo');
        apexAction.setCallback(this, function(response) {
        	var state = response.getState();
        	if(state === "SUCCESS"){
                var result = response.getReturnValue();

                component.set("v.priorNoComply", result.priorNoComply);
                component.set("v.application", result.currentAMDCPDApplication);
                if(result.currentAMDCPDApplication.CPAO_Declaration__c != undefined || result.currentAMDCPDApplication.CPAO_Declaration__c != null){
                	var checkedComp = component.find(result.currentAMDCPDApplication.CPAO_Declaration__c).set("v.checked", true);
                	// checkedComp.checked = true;
                }
                var map = JSON.parse(result.currentAMDCPDApplication.CPAO_JSON_File_Map__c);
                if(map['fileNameExtraordinaryConsequences'] != undefined){
                    if(map['fileNameExtraordinaryConsequences']['Name'] != undefined){
                        component.set('v.fileNameExtraordinaryConsequences',map['fileNameExtraordinaryConsequences']['Name']);
                    }
                }   
                component.set("v.waiverExplanations", result.waiverExplanations);
                
				var poaS = [];
				for(var i=0;i<result.poaItems.length;i++) {
					poaS.push({poa:result.poaItems[i], error:""});
				}
                component.set("v.poaS", poaS);
                
            }
        });
        $A.enqueueAction(apexAction);
	},

	onRadioChange : function(component, event, helper) {
		var newVal = event.getSource().get('v.label');
		var app = component.get("v.application");
		app.CPAO_Declaration__c = newVal;
		component.set("v.application", app);

        var waiverValueMap = component.get("v.waiverExplanations");
        var newWaiverExplanation = waiverValueMap[newVal];
        component.set("v.waiverExplanation", newWaiverExplanation);
	},

	clickOnNextButton : function(component,event,helper){
    	var talkingToComponent = event.getParam("componentName");
    	if(talkingToComponent == 'step5'){
            var keyList = [];
            var filenameList = [];
            var base64DataList = [];
            var contentTypeList = [];
    		var app = component.get("v.application");
    		if(app.CPAO_Declaration__c == 'I do not comply'){
                var PreviousPOAs = component.get("v.priorNoComply");
                if(PreviousPOAs == true){
                    app.CPAO_Mulitple_POAs_in_Triennial_Period__c = true;
                } else {
                    var validForm = component.find('CPDFormNoComply').reduce(function (validSoFar, inputCmp) {
                    // Displays error messages for invalid fields
                    inputCmp.showHelpMessageIfInvalid();
                    return validSoFar && inputCmp.get('v.validity').valid;

                    }, true);
                    if(validForm == false){
                        helper.methodFailure(component, event, helper);
                        return;
                    }
					
					var poaS = component.get("v.poaS");
                    /*if(poaS.length == 0){
                        helper.methodFailure(component, event, helper);
                        return;
                    }*/

                    var sumVerifiedHours = 0;
                    var sumUnverifiedHours = 0;
                    
                    for(var i=0;i<poaS.length;i++) {
                        if(poaS[i].poa.CPAO_Verifiable__c == 'Yes'){
                            sumVerifiedHours += poaS[i].poa.CPOA_CPD_Hours__c;
                        } else {
                            sumUnverifiedHours += poaS[i].poa.CPOA_CPD_Hours__c;
                        }                        
                    }
                    var warning = true;
                    if(app.CPAO_Number_of_Unverifiable_Hours_Short__c == sumUnverifiedHours 
                    && app.CPAO_Number_of_Verifiable_Hours_Short__c == sumVerifiedHours){
                        warning = false;
                    }
                    if(warning == true){
                        component.set("v.warningModalOpen", true);
                        helper.methodFailure(component, event, helper);
                        return;
                    }
                }        			
    		}

    		if(app.CPAO_Declaration__c == 'Other extraordinary consequences'){
    			var log = component.find('CPDFormOther');

    			var validForm = component.find('CPDFormOther').reduce(function (validSoFar, inputCmp) {
		        // Displays error messages for invalid fields
		        inputCmp.showHelpMessageIfInvalid();
		        return validSoFar && inputCmp.get('v.validity').valid;
		    	}, true);
		    	if(validForm == false){
                    helper.methodFailure(component, event, helper);
		    		return;
		    	}
    		}
    		
    		if(app.CPAO_Declaration__c == 'Comply with other professional body requirements'){
    			var log = component.find('CPDFormOtherBody');
    			var validForm = component.find('CPDFormOtherBody').reduce(function (validSoFar, inputCmp) {
		        // Displays error messages for invalid fields
		        inputCmp.showHelpMessageIfInvalid();
		        return validSoFar && inputCmp.get('v.validity').valid;
		    	}, true);
		    	if(validForm == false){
                    helper.methodFailure(component, event, helper);
		    		return;
		    	}
    		}
            if(app.CPAO_Declaration__c == '' || app.CPAO_Declaration__c == undefined ){
                helper.methodFailure(component, event, helper, "please choose a value", "Error");
                return;
            }

            // UPLOADIND FUNCTIONALITY 
            if(app.CPAO_Declaration__c == 'Other extraordinary consequences'){
                
                var fileNameExtraordinaryConsequences = component.get("v.fileNameExtraordinaryConsequences");
                var fileContentsExtraordinaryConsequences = component.get('v.fileContentsExtraordinaryConsequences');
                var fileTypeExtraordinaryConsequences= component.get('v.fileTypeExtraordinaryConsequences');
                if(fileNameExtraordinaryConsequences == undefined){
                    helper.methodFailure(component, event, helper, "Please Upload Supporting Documentation For Your Extraordinary Consequences", "Error");
                    return;
                }else {
                    if(fileNameExtraordinaryConsequences != undefined && fileContentsExtraordinaryConsequences != null){
                        keyList.push("fileNameExtraordinaryConsequences");
                        filenameList.push(fileNameExtraordinaryConsequences);
                        base64DataList.push(encodeURIComponent(component.get('v.fileContentsExtraordinaryConsequences')));
                        contentTypeList.push(fileTypeExtraordinaryConsequences);
                        console.log(keyList);
                        console.log(filenameList);
                        console.log(contentTypeList);
                        console.log(base64DataList);
                    }                        
                } 
            }else{
               keyList=null;
               filenameList=null;
               base64DataList=null;
               contentTypeList=null;
            }
            //

    		var poaS = component.get("v.poaS");
			var poaList = []
			for(var i=0;i<poaS.length;i++) {
				poaS[i].error = "";
				poaList.push(poaS[i].poa);
				if(poaS[i].poa.CPAO_Start_Date__c>poaS[i].poa.CPAO_End_Date__c) {
					poaS[i].error += "<p>End Date cannot occur before Start Date.</p>";
				}
			}
			component.set("v.poaS",poaS);
    		poaList = JSON.stringify(poaList);
    		var apexAction = component.get('c.saveAppAndPOA');
	        apexAction.setParams({
                "keyList" : keyList,
                "fileNameList" : filenameList,
                "base64DataList" : base64DataList, 
                "contentTypeList" : contentTypeList,
		    	"currentAMDCPDApplication": app,
		    	"poaS": poaList
		    });
	        apexAction.setCallback(this, function(response) {
	        	var state = response.getState();
	        	if(state === "SUCCESS"){
	        		var result = response.getReturnValue();
	                if(result == 'Success'){
	                	var actionToTake = event.getParam("nextStepName");
                        var applicationHasChanged = component.getEvent("applicationHasChanged");
                        applicationHasChanged.setParams({
                            "nextButtonClickeable" : true,
                            "action" : actionToTake});
				        applicationHasChanged.fire();
	                } else {
                        helper.methodFailure(component, event, helper);
	                }                
	            } else {
                    console.log('error while submitting AMD');
                    var errors=response.getError();
                    var message = errors[0];
                    console.log(message);
                    helper.methodFailure(component, event, helper);
                }
	        });
	        $A.enqueueAction(apexAction);
    	}        
    },

    removeItem : function(component, event, helper) {
        var index = event.getSource().get("v.name");
        var poaS = component.get("v.poaS");
        var poa = poaS[index].poa;
        if (poa.Id == undefined || poa.Id == null) { 
            poaS.splice(index, 1);
            component.set("v.poaS", poaS);
            return;
        } else {
        	var apexAction = component.get("c.deletePOA");
            var recordId = poa.Id;
            apexAction.setParams({
                "poaId" : recordId
            });
            apexAction.setCallback(this, function(response) {
                var state = response.getState();
                if (state === "SUCCESS") {
                    var result =  response.getReturnValue();
                    if (result == 'Success') {
                        poaS.splice(index, 1);
                        component.set("v.poaS", poaS);
                    }
                }
            });

            $A.enqueueAction(apexAction);
        }

    },

	addLine : function(component, event, helper) {
		var poaSItem = component.get("v.poaS");
        poaSItem.push({poa:{},error:""});
        component.set("v.poaS", poaSItem);
	},

    closeModel: function(component, event, helper) {
        // for Hide/Close Model,set the "warningModalOpen" attribute to "Fasle"  
        component.set("v.warningModalOpen", false);
    },

    confirmStatusChange : function(component, event, helper) {
        component.set("v.warningModalOpen", false);
        helper.showSpinner(component);

        var app = component.get("v.application");
        var poaS = component.get("v.poaS");
        var poaList = [];
        for(var i=0;i<poaS.length;i++) {
            poaS[i].error = "";
            poaList.push(poaS[i].poa);
            if(poaS[i].poa.CPAO_Start_Date__c>poaS[i].poa.CPAO_End_Date__c) {
                poaS[i].error += "<p>End Date cannot occur before Start Date.</p>";
            }
        }
        component.set("v.poaS",poaS);
        poaList = JSON.stringify(poaList);
        var apexAction = component.get('c.saveAppAndPOA');
        apexAction.setParams({
            "keyList" : keyList,
            "fileNameList" : filenameList,
            "base64DataList" : base64DataList, 
            "contentTypeList" : contentTypeList,
            "currentAMDCPDApplication": app,
            "poaS": poaList
        });
        apexAction.setCallback(this, function(response) {
            var state = response.getState();
            if(state === "SUCCESS"){
                console.log('state');
                console.log(state);
                var result = response.getReturnValue();
                if(result == 'Success'){                    
                    console.log('result');
                    console.log(result);
                    helper.hideSpinner(component);
                    var actionToTake = '';
                    var applicationHasChanged = component.getEvent("applicationHasChanged");
                    applicationHasChanged.setParams({
                        "nextButtonClickeable" : true,
                        "action" : actionToTake});
                    applicationHasChanged.fire();
                } else {
                    helper.hideSpinner(component);
                    helper.methodFailure(component, event, helper);
                }                
            } else {
                helper.hideSpinner(component);
                helper.methodFailure(component, event, helper);
            }
            helper.hideSpinner(component);
        });
        $A.enqueueAction(apexAction);
    },
    handleFilesChange: function (component, event,helper) {

        var fileInput = event.getSource().get("v.files")[0];
        var fileFor = event.getSource().get("v.name");
        var fileInputString = 'v.fileInput' + fileFor;
        var fileNameString = 'v.fileName' + fileFor;
        var fileTypeString = 'v.fileType' + fileFor;
        var fileContentsString = 'v.fileContents' + fileFor;
        console.log(fileInputString);
        component.set(fileInputString,fileInput);
        component.set(fileNameString,fileInput.name);
        component.set(fileTypeString,fileInput.type);
        
        var fr = new FileReader();
        var self = this;
        
        fr.onload = function() {
            var fileContents = fr.result;
            var base64Mark = 'base64,';
            var dataStart = fileContents.indexOf(base64Mark) + base64Mark.length;
            fileContents = fileContents.substring(dataStart);
            component.set(fileContentsString,fileContents);
        };
        
        fr.readAsDataURL(fileInput);
    }

})