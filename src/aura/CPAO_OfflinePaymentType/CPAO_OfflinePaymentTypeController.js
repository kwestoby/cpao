({
	doInit : function(component, event, helper) {
		console.log(component);
		helper.clearMessage(component);
        var custompayObj = component.get('v.customPaymentTypeObj');
		var action = component.get("c.getOfflineInstructions");
		action.setCallback(this, function(response){
			var state = response.getState();
			if(state==="SUCCESS") {
				component.set("v.instructions", response.getReturnValue());
			} else {
				console.log("Error occured while getting offline instructions: "+response.getError());
			}
		});
		$A.enqueueAction(action);
	},
    submitForOfflinePayment : function(component, event, helper) {
		helper.clearMessage(component);
		var callbackUrl = component.get("v.customPaymentTypeObj.callbackUrl");
		var salesOrderId = callbackUrl.split("sales_order=")[1].split("&")[0];
		var epaymentId = callbackUrl.split("epayment=")[1].split("&")[0];
		var action = component.get("c.confirmOfflinePayment");
		if(salesOrderId!="") {
			action.setParams({paymentId: salesOrderId});
		} else if (epaymentId!=""){
			action.setParams({paymentId: epaymentId});
		}
		action.setCallback(this, function(response) {
			var state = response.getState();
			if(state==="SUCCESS") {
				helper.showMessage(component, "Confirmation", "confirm", "Confirmed for offline payment.");
			} else {
				helper.showMessage(component, "Error", "error", response.getError()[0].message);
			}
		});
		$A.enqueueAction(action);
	}
})