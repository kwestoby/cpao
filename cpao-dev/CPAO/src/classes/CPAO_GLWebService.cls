global class CPAO_GLWebService {

	@ReadOnly
	webservice static List<GLTxDetail> getGLTransactions() {
		CPAO_Integration_Settings__c intSettings = CPAO_Integration_Settings__c.getOrgDefaults();
		Decimal batchNumber = intSettings.CPAO_Transaction_Line_Batch_Number__c;
		updateTransactionLineBatchNumber();
		List<AggregateResult> agResults = [SELECT OrderApi__GL_Account__r.CPAO_GL_Code__c glcode, OrderApi__GL_Account__r.Name description,
		                                   SUM(OrderApi__Debit__c) debit, SUM(OrderApi__Credit__c) credit, OrderApi__Date__c txdate
		                                   FROM OrderApi__Transaction_Line__c
		                                   WHERE CPAO_Batch_Number__c = :batchNumber
		                                   AND OrderApi__GL_Account__c != null
		                                   GROUP BY OrderApi__GL_Account__r.CPAO_GL_Code__c, OrderApi__GL_Account__r.Name, OrderApi__Date__c];
		List<GLTxDetail> results = new List<GLTxDetail> ();
		for (AggregateResult ar : agResults) {
			GLTxDetail gtx = new GLTxDetail();
			gtx.gl_code = (String) ar.get('glcode');
			gtx.gl_description = (String) ar.get('description');
			gtx.tx_date = (Date) ar.get('txdate');
			gtx.amount = (Decimal) ar.get('debit');
			gtx.amount -= (Decimal) ar.get('credit');
			gtx.batch_number = batchNumber;
			results.add(gtx);
		}
		return results;
	}

	@Future
	public static void updateTransactionLineBatchNumber() {
		CPAO_Integration_Settings__c intSettings = CPAO_Integration_Settings__c.getOrgDefaults();
		intSettings.CPAO_Transaction_Line_Batch_Number__c += 1;
		update intSettings;
	}

	global class GLTxDetail {
		webservice String gl_code;
		webservice String gl_description;
		webservice Decimal batch_number;
		webservice Decimal amount;
		webservice Date tx_date;
	}
}