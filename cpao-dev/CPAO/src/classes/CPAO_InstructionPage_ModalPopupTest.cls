@isTest
public class CPAO_InstructionPage_ModalPopupTest {

@isTest
    public static void closePopup(){
        CPAO_InstructionPage_ModalPopup modalPopup = new CPAO_InstructionPage_ModalPopup();
        modalPopup.closePopup();
        System.assert(modalPopup.displayPopup == false);
        
    } 
    
@isTest
   public static void openPopup(){
        CPAO_InstructionPage_ModalPopup modalPopup = new CPAO_InstructionPage_ModalPopup();
        modalPopup.openPopup();
        System.assert(modalPopup.displayPopup == true);
    }
    
}