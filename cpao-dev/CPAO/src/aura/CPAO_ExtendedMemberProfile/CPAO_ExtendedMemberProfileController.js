({
	doAction : function(component, event, helper) {
		var app = component.get('v.application');

		if(app.CPAO_professional_services_in_Ontario__c != 'Yes'){
			return;
		}
		console.log('app.CPAO_Professional_Service_Work_Performed__c');
		console.log(app.CPAO_Professional_Service_Work_Performed__c);
		if(app.CPAO_Capacity_through_Firm__c != null || app.CPAO_Capacity_through_Firm__c != undefined){
			console.log(component.get("v.capacityAccount"));
			//component.set("v.selectedAccount", component.get("v.capacityAccount"));
		}

		var checkboxes = component.find('checkboxInput');
		var appServices = component.get('v.applicationServiceTypes');
		if(appServices.length > 0){
			for(var i = 0; i < checkboxes.length; i++){
				for(var j = 0; j < appServices.length; j++){
					if(appServices[j] == checkboxes[i].get("v.name")){
						checkboxes[i].set("v.checked", true);
					}
				}
			}
		}
			
	},
	clickOnNextButton : function(component,event,helper){
    	var talkingToComponent = event.getParam("componentName");
    	if(talkingToComponent == 'step4'){
            var app = component.get("v.application");
            if(app.CPAO_professional_services_in_Ontario__c != 'Yes' && app.CPAO_professional_services_in_Ontario__c != 'No'){
                helper.methodFailure(component, event, helper, 'Please specify if you are providing professional services in Ontario', 'Error');
                return;
            }
            if(app.CPAO_professional_services_in_Ontario__c == 'Yes'){
            	var services = [];
	            var servicesRaw = component.find('checkboxInput');
	            for(var i = 0; i < servicesRaw.length; i++){
	            	if(servicesRaw[i].get("v.checked") == true){
	            		services.push(servicesRaw[i].get("v.name"));
	            	}
	            }
	            console.log('services');
	            console.log(services);
	            if(!services.length > 0){
	            	helper.methodFailure(component, event, helper, 'Please select a service', 'Error');
	                return;
	            }
	            console.log('capacity fields');
	            console.log(component.get("v.selectedAccount"));
	            console.log(app.CPAO_Capacity_through_Non_firm__c);
	            if((component.get("v.selectedAccount") == undefined || component.get("v.selectedAccount") == null) &&
	            	(app.CPAO_Capacity_through_Non_firm__c == '' || app.CPAO_Capacity_through_Non_firm__c == undefined)){
	            	helper.methodFailure(component, event, helper, 'Please specify an entity where you provided the service(s)', 'Error');
	            	return;
	            }

	            if(app.CPAO_Registrant_Of__c == null || app.CPAO_Registrant_Of__c == undefined){
	                helper.methodFailure(component, event, helper, 'Please specify if you are a registrant', 'Error');
	                return;
	            }
            }
            
            var apexAction = component.get('c.extendedProfileSave');
	        apexAction.setParams({
		    	"currentAMDCPDApplication": app,
		    	"services": services
		    });
            console.log('marco test 4');
	        apexAction.setCallback(this, function(response) {
	        	var state = response.getState();
                console.log('marco test 5');
                console.log(state);
	        	if(state === "SUCCESS"){
	        		var result = response.getReturnValue();
                    console.log('marco test 6');
                    console.log(result);
	                if(result == 'Success'){
                        var actionToTake = event.getParam("nextStepName");
	                	var applicationHasChanged = component.getEvent("applicationHasChanged");
				        applicationHasChanged.setParams({
                            "nextButtonClickeable" : true,
                            "action" : actionToTake});
				        applicationHasChanged.fire();
	                } else {
                        helper.methodFailure(component, event, helper);    
                    }
	            } else {
                    helper.methodFailure(component, event, helper);
                }
	        });
	        $A.enqueueAction(apexAction);
    	}        
    },
})