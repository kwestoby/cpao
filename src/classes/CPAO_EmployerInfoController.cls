public without sharing class CPAO_EmployerInfoController {

	@AuraEnabled
	//This method returns informations about the current logged user in the portal
	public static User getLoggedUser(){
		return CPAO_UserSelector.getCurrentUser();
	}

	@AuraEnabled
	//This method returns all CPAO_Account_Contact_Relationship__c objects associated to 
	//the current logged user's contact record.
	// Those relationships refer to all employers a contact may have
	public static ResultWrapper getRelationships(){
		User loggedUser = getLoggedUser();
		ResultWrapper response = new ResultWrapper();

		response.relationships = 
			CPAO_AccountContactRelationSelector.getRelationshipsByContactIdAndRelationToAccount(loggedUser.ContactId, 'Employee','Active');
		response.canBeRetiredUnemployed = true;
		for(CPAO_Account_Contact_Relationship__c rel:response.relationships){
			if(rel.CPAO_End_Date__c == null){
				response.canBeRetiredUnemployed = false;
			}
		}
		response.contact = CPAO_ContactSelector.getContactToUpdate(new Set<Id>{loggedUser.ContactId})[0];
		response.employmentStatuses = new List<String>();
		Schema.DescribeFieldResult employmentStatuseValues = Contact.CPAO_Employment_Status__c.getDescribe();
		for(Schema.PicklistEntry ple:employmentStatuseValues.getPicklistValues()){
			if(ple.getLabel() != 'Unknown'){
				response.employmentStatuses.add(ple.getLabel());
			}
			
		}
		return response;
	}

	@AuraEnabled
	//This method retrieves all the possibles picklist values for this field: CPAO_Business_Unit__c in
	//CPAO_Account_Contact_Relationship__c object.
	//Those values will be populated in a form so that the user could choose from them
	public static  List<String>  getBusinessUnitOptions(){
		List<String> listOfOptions = new List<String>();
        Schema.DescribeFieldResult fieldResult = CPAO_Account_Contact_Relationship__c.CPAO_Business_Unit__c.getDescribe();
		List<Schema.PicklistEntry> pickListEntries = fieldResult.getPicklistValues();
		for (Schema.Picklistentry picklistEntry : pickListEntries){
			if(picklistEntry.isActive()){
				listOfOptions.add(pickListEntry.getLabel());
			}
		}
		return listOfOptions;
	}

	@AuraEnabled
	public static  List<String>  getPositionCategoryOptions(){
		List<String> listOfOptions = new List<String>();
        Schema.DescribeFieldResult fieldResult = CPAO_Account_Contact_Relationship__c.CPAO_Position_Category__c.getDescribe();
		List<Schema.PicklistEntry> pickListEntries = fieldResult.getPicklistValues();
		for (Schema.Picklistentry picklistEntry : pickListEntries){
			if(picklistEntry.isActive()){
				listOfOptions.add(pickListEntry.getLabel());
			}
			
		}
		return listOfOptions;
	}

	@AuraEnabled
	public static CPAO_Account_Contact_Relationship__c SaveAccountContactRelationships(CPAO_Account_Contact_Relationship__c relationship){
		
		upsert(relationship);
		return relationship;
	}

	@AuraEnabled
	public static Id getAccountLookup(String accountName){
		List<Account> existingAccounts = CPAO_AccountSelector.getAccountToUpdate(accountName);
		return existingAccounts[0].Id;
	}

	@AuraEnabled
	public static List<CPAO_Account_Contact_Relationship__c> isPrimaryEmployer(Id contactId){

	    List<CPAO_Account_Contact_Relationship__c> primaryEmployer = 
	    	CPAO_AccountContactRelationSelector.getPrimaryRelationshipsByContactId(new Set<Id>{contactId});
		return primaryEmployer;
	}

	@AuraEnabled
	public static List<CPAO_Account_Contact_Relationship__c> changePrimaryEmployer(Id relationshipId, Boolean doInsert){
		List<CPAO_Account_Contact_Relationship__c> primaryEmployer = 
			CPAO_AccountContactRelationSelector.getPrimaryRelationshipsById(relationshipId);
	    primaryEmployer[0].CPAO_Primary_Employer__c='No';
	    if(doInsert){
	    	update primaryEmployer;
	    }
		
		return primaryEmployer;
	}

	@AuraEnabled
	public static List<CPAO_Account_Contact_Relationship__c> updateEmployerAndChangePrimary(CPAO_Account_Contact_Relationship__c relationship){
		
		//ResultWrapper response = new ResultWrapper();
		//CPAO_Account_Contact_Relationship__c updatedRelationship= new CPAO_Account_Contact_Relationship__c();
		List<CPAO_Account_Contact_Relationship__c> returnedRelationship = new List<CPAO_Account_Contact_Relationship__c>();
		List<CPAO_Account_Contact_Relationship__c> primaryEmployer=isPrimaryEmployer(relationship.CPAO_Contact__c);
		if(primaryEmployer!=null){
			returnedRelationship=changePrimaryEmployer(primaryEmployer[0].Id, false);
			relationship.CPAO_Primary_Employer__c='Yes';
			returnedRelationship.add(relationship);
			upsert(returnedRelationship);
		}
		return returnedRelationship;
	}

	@AuraEnabled
	public static String retireUnemployEmployee(String employmentType){
		User loggedUser = getLoggedUser();
		//String relationshipToAccount='';
		List<CPAO_Account_Contact_Relationship__c> relationships = 
		CPAO_AccountContactRelationSelector.getPrimaryRelationshipsByContactId(new Set<Id>{loggedUser.ContactId});
		for(CPAO_Account_Contact_Relationship__c relationship:relationships){
			relationship.CPAO_Primary_Employer__c = 'No';
		}
		Contact currentContact = new Contact(Id = loggedUser.ContactId, CPAO_Employment_Status__c = employmentType);
		try {
			update relationships;		
			update currentContact;
		} catch(Exception e) {
			return e.getMessage();
		}
		//List<Account> retirementAccount = CPAO_AccountSelector.getAccountToUpdate(employmentType);
		//if(retirementAccount.size()>0){
		//	if(employmentType=='Unemployed'){
		//		relationshipToAccount='Unemployment';
		//	}else if(employmentType=='Retired'){
		//		relationshipToAccount='Retirement';
		//	}
		//	CPAO_Account_Contact_Relationship__c retireRelationship = new CPAO_Account_Contact_Relationship__c(
		//		CPAO_Contact__c = loggedUser.ContactId, 
		//		CPAO_Account__c = retirementAccount[0].Id,
		//		CPAO_Start_Date__c = Date.today(),
		//		CPAO_Primary_Employer__c = 'Yes',
		//		CPAO_Relationship_to_Account__c = relationshipToAccount);
		//	insert retireRelationship;
		//	return true;
		//}
		return 'Success';
	}

	

	@AuraEnabled
	public static  String  componentInit(){
		ResultWrapper response = new ResultWrapper();
		response.businessUnits = getBusinessUnitOptions();
		response.positionCategories = getPositionCategoryOptions();

	    return JSON.serialize(response);
	}

	@AuraEnabled
	public static  Contact  updateContact(Contact contactToUpdate){
		update contactToUpdate;
        return contactToUpdate;
	}

	@AuraEnabled
	public static NewEmploymentDoInitWrapper NewEmploymentDoInit(){
		NewEmploymentDoInitWrapper response = new NewEmploymentDoInitWrapper();
		response.businessUnits = getBusinessUnitOptions();
		response.positionCategories = getPositionCategoryOptions();
		response.loggedUser = getLoggedUser();
		response.newEmploymentWrapper = new NewEmploymentWrapper();

		List<String> userFieldsToGather = new List<String> { 'Countrycode', 'statecode' };
		Schema.SObjectType userSStype = Schema.getGlobalDescribe().get('User');
        Schema.DescribeSObjectResult userDescribe = userSStype.getDescribe();
		for (String fieldName : userFieldsToGather) {
            List<String> pickListValuesList = new List<String> ();

            for (Schema.PicklistEntry pickListVal : userDescribe.fields.getMap().get(fieldName).getDescribe().getPicklistValues()) {
                pickListValuesList.add(pickListVal.getLabel());
            }
            if (fieldName == 'Countrycode') {
                response.country = pickListValuesList;
            } else if (fieldName == 'statecode') {
                response.state = pickListValuesList;
            }
        }
	    return response;
	}

	@AuraEnabled
	public static String NewEmployerCreate(String JSONuserInputData){
		User loggedUser = getLoggedUser();
		String adminThatOwnsNewEmployers = Label.New_Employer_Owner_Id;
		NewEmploymentWrapper userInputData = (NewEmploymentWrapper)JSON.deserialize(JSONuserInputData, CPAO_EmployerInfoController.NewEmploymentWrapper.class);
		Account newAccount = new Account(
			Name = userInputData.employerName,
			BillingStreet = userInputData.employerAddress,
			BillingCity = userInputData.city,
			BillingState = userInputData.provinceTerritory,
			BillingPostalCode = userInputData.postalCode,
			BillingCountry = userInputData.country,
			Phone = userInputData.phone,
			Industry = userInputData.industry,
			CPAO_Account_Verified__c = 'Unverified',
			OwnerId = adminThatOwnsNewEmployers);

		try {
			insert newAccount;
		} catch(Exception e) {
			return e.getMessage();
		}

		CPAO_Account_Contact_Relationship__c relToInsert = new CPAO_Account_Contact_Relationship__c(
			CPAO_Account__c = newAccount.Id,
			CPAO_Contact__c = loggedUser.ContactId,
			CPAO_Text__c = userInputData.positionTitle,
			CPAO_Position_Category__c = userInputData.positionCategory,
			CPAO_Business_Unit__c = userInputData.businessUnit,
			CPAO_Primary_Employer__c = userInputData.isPrimaryEmployer,
			CPAO_Start_Date__c = Date.today(),
			OwnerId = adminThatOwnsNewEmployers);

		try {
			insert relToInsert;
		} catch(Exception e) {
			return e.getMessage();
		}
	    return 'Success';
	}

	public class ResultWrapper{

        @AuraEnabled public List<String> businessUnits{get;set;}
        @AuraEnabled public List<String> positionCategories {get;set;}
        @AuraEnabled public List<String> employmentStatuses{get;set;}
        @AuraEnabled public Boolean hasPrimaryEmployer {get;set;}
        @AuraEnabled public Boolean canBeRetiredUnemployed {get;set;}
        @AuraEnabled public List<CPAO_Account_Contact_Relationship__c> relationships {get;set;}
        @AuraEnabled public Contact contact {get;set;}
    }

    public class NewEmploymentDoInitWrapper{

        @AuraEnabled public User loggedUser{get;set;}
        @AuraEnabled public List<String> positionCategories {get;set;}
        @AuraEnabled public List<String> businessUnits{get;set;}
        @AuraEnabled public NewEmploymentWrapper newEmploymentWrapper {get;set;}
        @AuraEnabled public List<String> country { get; set; }
        @AuraEnabled public List<String> state { get; set; }
    }

    public class NewEmploymentWrapper{

        @AuraEnabled public String employerName{get;set;}
        @AuraEnabled public String employerAddress{get;set;}
        @AuraEnabled public String city{get;set;}
        @AuraEnabled public String provinceTerritory{get;set;}
        @AuraEnabled public String postalCode{get;set;}
        @AuraEnabled public String country{get;set;}
        @AuraEnabled public String phone{get;set;}
        @AuraEnabled public String industry{get;set;}
        @AuraEnabled public String isAccountingFirm{get;set;}
        @AuraEnabled public String positionTitle{get;set;}
        @AuraEnabled public String positionCategory{get;set;}
        @AuraEnabled public String businessUnit{get;set;}
        @AuraEnabled public String isPrimaryEmployer{get;set;}
    }
    
}