({
	doInit : function(component, event, helper) {
		var apexAction = component.get('c.handleNewIdButtonClicks');
		var recordId = component.get('v.recordId');
		apexAction.setParams({
            "recordId" : recordId,
        });
        apexAction.setCallback(this, function(response) {
        	var state = response.getState();
        	if(state === "SUCCESS"){
                var error = response.getReturnValue();
                if(error == null){
                    helper.showToast("Success", "New CPA Id was properly assigned", "success");
                    $A.get('e.force:refreshView').fire();
                    $A.get("e.force:closeQuickAction").fire();
                } else {
                    var error = response.getReturnValue();
                    helper.showToast("Error", error, "error");
                    $A.get('e.force:refreshView').fire();
                    $A.get("e.force:closeQuickAction").fire();
                }
            }
        });
        $A.enqueueAction(apexAction);
	}


})