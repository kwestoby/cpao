({
	loadMethod : function(component, event, helper) {
		var app = component.get("v.application");
        console.log('laodmethod for good character');

        var map = JSON.parse(app.CPAO_JSON_File_Map__c);
        if(map['fileNamecriminalExplanation'] != undefined){
            if(map['fileNamecriminalExplanation']['Name'] != undefined){
                component.set('v.fileNamecriminalExplanation',map['fileNamecriminalExplanation']['Name']);
            }
        }
        console.log('past 1');
        if(map['fileNamesuspensionExplanation'] != undefined){
            if(map['fileNamesuspensionExplanation']['Name'] != undefined){
                component.set('v.fileNamesuspensionExplanation',map['fileNamesuspensionExplanation']['Name']);
                console.log('should pass');
            }
        }
        if(map['fileNamedisciplineExplanation'] != undefined){
            if(map['fileNamedisciplineExplanation']['Name'] != undefined){
                component.set('v.fileNamedisciplineExplanation',map['fileNamedisciplineExplanation']['Name']);
            }
        }
        if(map['fileNameviolationExplanation'] != undefined){
            if(map['fileNameviolationExplanation']['Name'] != undefined){
                component.set('v.fileNameviolationExplanation',map['fileNameviolationExplanation']['Name']);
            }
        }
        if(map['fileNameexpulsionExplanation'] != undefined){
            if(map['fileNameexpulsionExplanation']['Name'] != undefined){
                component.set('v.fileNameexpulsionExplanation',map['fileNameexpulsionExplanation']['Name']);
            }
        }
        if(map['fileNamebankruptcyExplanation'] != undefined){
            if(map['fileNamebankruptcyExplanation']['Name'] != undefined){
                component.set('v.fileNamebankruptcyExplanation',map['fileNamebankruptcyExplanation']['Name']);
            }
        }
        console.log('done good char loadMethod');
	},

	clickOnNextButton : function(component,event,helper){
        var talkingToComponent = event.getParam("componentName");
        if(talkingToComponent == 'step4'){
            
        	var app = component.get("v.application");
        	var enterAll = false;
            var enterFile = false;
            var keyList = [];
            var filenameList = [];
            var base64DataList = [];
            var contentTypeList = [];

        	if(app.CPAO_Criminal_Charges_or_Convictions__c == undefined || app.CPAO_Susp_Rev_of_Professional_License__c == undefined ||
        	app.CPAO_Subject_to_disciplinary_decision__c == undefined || app.CPAO_Guilty_of_Security_Tax_Legislation__c == undefined ||
        	app.CPAO_Expelled_from_a_Professional_Org__c == undefined || app.CPAO_Made_assignment_in_bankruptcy__c == undefined){
        		enterAll = true;
        	} 

        	if(app.CPAO_Criminal_Charges_or_Convictions__c == 'Yes' ){
        		if(app.CPAO_Explanation_for_criminal_offence__c == undefined || !app.CPAO_Explanation_for_criminal_offence__c.replace(/\s/g, '').length){
                    enterAll = true;
                }
                var fileNamecriminalExplanation = component.get("v.fileNamecriminalExplanation");
                if(fileNamecriminalExplanation == undefined){
                    enterFile = true;
                } else {
                    if(component.get('v.fileContentscriminalExplanation') != undefined && component.get('v.fileContentscriminalExplanation') != null){
                        keyList.push("fileNamecriminalExplanation");
                        filenameList.push(fileNamecriminalExplanation);
                        base64DataList.push(encodeURIComponent(component.get('v.fileContentscriminalExplanation')));
                        contentTypeList.push(component.get("v.fileTypecriminalExplanation"));
                    }                        
                }
        	} 

        	if(app.CPAO_Susp_Rev_of_Professional_License__c == 'Yes'){
                if(app.CPAO_Explanation_for_suspension__c == undefined || !app.CPAO_Explanation_for_suspension__c.replace(/\s/g, '').length){
                    enterAll = true;
                }
                var fileNamesuspensionExplanation = component.get("v.fileNamesuspensionExplanation");
                if(fileNamesuspensionExplanation == undefined){
                    enterFile = true;
                } else {
                    if(component.get('v.fileContentssuspensionExplanation') != undefined && component.get('v.fileContentssuspensionExplanation') != null){
                        keyList.push("fileNamesuspensionExplanation");
                        filenameList.push(fileNamesuspensionExplanation);
                        base64DataList.push(encodeURIComponent(component.get('v.fileContentssuspensionExplanation')));
                        contentTypeList.push(component.get("v.fileTypesuspensionExplanation"));
                    }                    
                }
        	}

        	if(app.CPAO_Subject_to_disciplinary_decision__c == 'Yes'){
                if(app.CPAO_Explanation_for_discipline_decision__c == undefined || !app.CPAO_Explanation_for_discipline_decision__c.replace(/\s/g, '').length){
                    enterAll = true;
                }
                var fileNamedisciplineExplanation = component.get("v.fileNamedisciplineExplanation");
                if(fileNamedisciplineExplanation == undefined){
                    enterFile = true;
                } else {
                    if(component.get('v.fileContentsdisciplineExplanation') != undefined && component.get('v.fileContentsdisciplineExplanation') != null){
                        keyList.push("fileNamedisciplineExplanation");
                        filenameList.push(fileNamedisciplineExplanation);
                        base64DataList.push(encodeURIComponent(component.get('v.fileContentsdisciplineExplanation')));
                        contentTypeList.push(component.get("v.fileTypedisciplineExplanation"));
                    }                        
                }
        	}

        	if(app.CPAO_Guilty_of_Security_Tax_Legislation__c == 'Yes'){
                if(app.CPAO_Explanation_for_violation__c == undefined || !app.CPAO_Explanation_for_violation__c.replace(/\s/g, '').length){
                    enterAll = true;
                }
                var fileNameviolationExplanation = component.get("v.fileNameviolationExplanation");
                if(fileNameviolationExplanation == undefined){
                    enterFile = true;
                } else {
                    if(component.get('v.fileContentsviolationExplanation') != undefined && component.get('v.fileContentsviolationExplanation') != null){
                        keyList.push("fileNameviolationExplanation");
                        filenameList.push(fileNameviolationExplanation);
                        base64DataList.push(encodeURIComponent(component.get('v.fileContentsviolationExplanation')));
                        contentTypeList.push(component.get("v.fileTypeviolationExplanation"));
                    }                        
                }
        	}

        	if(app.CPAO_Expelled_from_a_Professional_Org__c == 'Yes'){
                if(app.CPAO_Explanation_for_expulsion__c == undefined || !app.CPAO_Explanation_for_expulsion__c.replace(/\s/g, '').length){
                    enterAll = true;
                }
                var fileNameexpulsionExplanation = component.get("v.fileNameexpulsionExplanation");
                if(fileNameexpulsionExplanation == undefined){
                    enterFile = true;
                } else {
                    if(component.get('v.fileContentsexpulsionExplanation') != undefined && component.get('v.fileContentsexpulsionExplanation') != null){
                        keyList.push("fileNameexpulsionExplanation");
                        filenameList.push(fileNameexpulsionExplanation);
                        base64DataList.push(encodeURIComponent(component.get('v.fileContentsexpulsionExplanation')));
                        contentTypeList.push(component.get("v.fileTypeexpulsionExplanation"));
                    }                        
                }
        	}

        	if(app.CPAO_Made_assignment_in_bankruptcy__c == 'Yes'){
                if(app.CPAO_Explanation_for_bankruptcy__c == undefined || !app.CPAO_Explanation_for_bankruptcy__c.replace(/\s/g, '').length){
                    enterAll = true;
                }        		
                var fileNamebankruptcyExplanation = component.get("v.fileNamebankruptcyExplanation");
                if(fileNamebankruptcyExplanation == undefined){
                    enterFile = true;
                } else {
                    if(component.get('v.fileContentsbankruptcyExplanation') != undefined && component.get('v.fileContentsbankruptcyExplanation') != null){
                        keyList.push("fileNamebankruptcyExplanation");
                        filenameList.push(fileNamebankruptcyExplanation);
                        base64DataList.push(encodeURIComponent(component.get('v.fileContentsbankruptcyExplanation')));
                        contentTypeList.push(component.get("v.fileTypebankruptcyExplanation"));
                    }                    
                }
        	}
            if(enterAll == true && enterFile == true){
                helper.methodFailure(component, event, helper, 'Please enter all required fields and attach all required documents', 'Error');
                return;
            }
        	if(enterAll == true){
        		helper.methodFailure(component, event, helper, 'Please enter all required fields', 'Error');
        		return;
        	}
            if(enterFile == true){
                helper.methodFailure(component, event, helper, 'Please attach all required documents', 'Error');
                return;
            }
            console.log('marco 1');
            console.log(keyList);
            // return;
            var apexAction = component.get('c.saveApplication');
            apexAction.setParams({
                "keyList" : keyList,
                "fileNameList" : filenameList,
                "base64DataList" : base64DataList, 
                "contentTypeList" : contentTypeList,
                "application": app
            });
            console.log('marco 2');
            apexAction.setCallback(this, function(response) {
                var state = response.getState();
                console.log('state');
                console.log(state);
                if(state === "SUCCESS"){
                    var result = response.getReturnValue();
                    console.log('result');
                    console.log(result);
                    if(result == 'Success'){
                        component.set('v.fileContentscriminalExplanation',null);
                        component.set('v.fileTypecriminalExplanation',null);

                        component.set('v.fileContentssuspensionExplanation',null);
                        component.set('v.fileTypesuspensionExplanation',null);

                        component.set('v.fileContentsdisciplineExplanation',null);
                        component.set('v.fileTypedisciplineExplanation',null);

                        component.set('v.fileContentsviolationExplanation',null);
                        component.set('v.fileTypeviolationExplanation',null);

                        component.set('v.fileContentsexpulsionExplanation',null);
                        component.set('v.fileTypeexpulsionExplanation',null);

                        component.set('v.fileContentsbankruptcyExplanation',null);
                        component.set('v.fileTypebankruptcyExplanation',null);
                        var actionToTake = event.getParam("nextStepName");
                        var applicationHasChanged = component.getEvent("applicationHasChanged");
                        applicationHasChanged.setParams({
                            "nextButtonClickeable" : true,
                            "action" : actionToTake});
                        applicationHasChanged.fire();
                    } else {
                        helper.methodFailure(component, event, helper, result, 'Error');    
                    }
                } else {
                    helper.methodFailure(component, event, helper, state, 'Error');
                }
            });
            $A.enqueueAction(apexAction);
        }
    },

    handleFilesChange: function (component, event,helper) {
        var fileInput = event.getSource().get("v.files")[0];
        var fileFor = event.getSource().get("v.name");
        var fileInputString = 'v.fileInput' + fileFor;
        var fileNameString = 'v.fileName' + fileFor;
        var fileTypeString = 'v.fileType' + fileFor;
        var fileContentsString = 'v.fileContents' + fileFor;
        console.log(fileInputString);

        component.set(fileInputString,fileInput);
        component.set(fileNameString,fileInput.name);
        component.set(fileTypeString,fileInput.type);
        
        var fr = new FileReader();
        var self = this;
        
        fr.onload = function() {
            var fileContents = fr.result;
            var base64Mark = 'base64,';
            var dataStart = fileContents.indexOf(base64Mark) + base64Mark.length;
            fileContents = fileContents.substring(dataStart);
            component.set(fileContentsString,fileContents);
        };
        
        fr.readAsDataURL(fileInput);
    },
})