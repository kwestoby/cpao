trigger CPAO_History_Tracker on CPAO_History_Tracker_Controller__c (before insert, after insert, before update, after update) {
     CPAO_Automation_Control__c control = CPAO_Automation_Control__c.getOrgDefaults();
    if(!control.CPAO_History_Tracker_Controller_c__c){
        return;
    }
 
    if(Trigger.isBefore && Trigger.isInsert){
        CPAO_HistoryTrackerControllerHelper.historyTrackerControllerValidation(Trigger.New);
    }
    if(Trigger.isBefore && Trigger.isUpdate){
          CPAO_HistoryTrackerControllerHelper.historyTrackerControllerValidation(Trigger.New);

    }

    if(Trigger.isAfter && Trigger.isInsert){
     
    }

    if(Trigger.isAfter && Trigger.isUpdate){
    
    }

    if(Trigger.isAfter && Trigger.isDelete){
      
    }

}