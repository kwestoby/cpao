trigger assignFirstApproval on OrderApi__Receipt__c (before update) {

    CPAO_Automation_Control__c control = CPAO_Automation_Control__c.getOrgDefaults();
    if(!control.OrderApi_Receipt_c__c){
        return;
    }
    
    Map<Id, OrderApi__Receipt__c> receiptToEvaluate = new Map<Id, OrderApi__Receipt__c >();
    if(Trigger.isBefore&& Trigger.isUpdate){
        for(OrderApi__Receipt__c receiptObj : Trigger.New){
                if(receiptObj.OrderApi__Type__c == 'Refund')
                    receiptToEvaluate.put(receiptObj.Id,receiptObj);
            }
    }       
    CPAO_FirstApproverRequestHelper.updateOrderReceipts(receiptToEvaluate);
}