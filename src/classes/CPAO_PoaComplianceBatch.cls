global class CPAO_PoaComplianceBatch implements Database.Batchable<sObject> {
    //searches for contacts with CPA Contact Type = member and who have no continuous amd waiver    
    global Database.QueryLocator start(Database.BatchableContext BC) {
        String query='SELECT Id, CPAO_Compliance_Due_Date__c FROM CPAO_Application__c WHERE CPAO_Compliance_Declaration__c = false AND ';
        query+= 'CPAO_Record_Keeping_Declaration__c = false AND CPAO_Status__c = \'Completed\' AND ';
        query+= 'RecordType.Name = \'AMD/CPD\'';
        return Database.getQueryLocator(query);
    }

    global void execute(Database.BatchableContext BC, List<CPAO_Application__c> applications) {
        List<CPAO_Application__c> warningList = new List<CPAO_Application__c>();
        List<CPAO_Application__c> infractionList = new List<CPAO_Application__c>();
        for(CPAO_Application__c app:applications){
            Date today = Date.today();
            Date complianceDueDate = app.CPAO_Compliance_Due_Date__c;
            if(complianceDueDate.addDays(-10) == today){
                warningList.add(app);
            }
            if(complianceDueDate == today){
                infractionList.add(app);
            }
        }
        if(!warningList.isEmpty()){
            sendWarningEmails(warningList);
        }
        if(!infractionList.isEmpty()){
            sendInfractionEmails(infractionList);
            List<CPAO_Infraction_Type__c> infractionType = CPAO_InfractionSelector.getInfractionType('CPD Plan of Action Compliance (CPD POA)');
            if(infractionType.size() != 1){
                return;
            }
            List<CPAO_Infraction__c> infractionsToCreate = new List<CPAO_Infraction__c>();
            for(CPAO_Application__c app:infractionList){
                infractionsToCreate.add(new CPAO_Infraction__c(
                CPAO_Infraction_Type__c = infractionType[0].Id,
                CPAO_Contact__c = app.CPAO_Contact__c));
            }
            if(!infractionsToCreate.isEmpty()){
                insert infractionsToCreate;
            }
        }
    }

    global void finish(Database.BatchableContext BC) {
        
    }

    public static void sendWarningEmails(List<CPAO_Application__c> warningList) {
        List<EmailTemplate> lstEmailTemplates = getTemplate('CPAO_CPD_POA_compliance_warning');
        if(lstEmailTemplates.size() != 1){
            return;
        }
        Messaging.SingleEmailMessage[] messages = new List<Messaging.SingleEmailMessage>();

        for(CPAO_Application__c app:warningList){
            Messaging.SingleEmailMessage mail = new Messaging.SingleEmailMessage();
            mail.setTemplateId(lstEmailTemplates[0].Id);
            mail.setSaveAsActivity(false);
            mail.setTargetObjectId(app.CPAO_Contact__c);

            mail.setWhatId(app.id);
            messages.add(mail);
        }           
        
        Messaging.SendEmailResult[] resultMail = Messaging.sendEmail(messages);
    }

    public static void sendInfractionEmails(List<CPAO_Application__c> infractionList) {
        List<EmailTemplate> lstEmailTemplates = getTemplate('CPAO_CPD_POA_compliance_past_due');
        if(lstEmailTemplates.size() != 1){
            return;
        }
        Messaging.SingleEmailMessage[] messages = new List<Messaging.SingleEmailMessage>();

        for(CPAO_Application__c app:infractionList){
            Messaging.SingleEmailMessage mail = new Messaging.SingleEmailMessage();
            mail.setTemplateId(lstEmailTemplates[0].Id);
            mail.setSaveAsActivity(false);
            mail.setTargetObjectId(app.CPAO_Contact__c);

            mail.setWhatId(app.id);
            messages.add(mail);
        }           
        
        Messaging.SendEmailResult[] resultMail = Messaging.sendEmail(messages);
    }
    public static List<EmailTemplate> getTemplate(String templateName) {
        return [SELECT Id, Body, Subject from EmailTemplate where DeveloperName = :templateName];
    }
}