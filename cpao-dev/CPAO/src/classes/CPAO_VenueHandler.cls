public with sharing class CPAO_VenueHandler {
	
	public static void modifyEventLocation(Map<Id, Id>  venuesAndEvents) {
				
			//Set of Ids to get the appropriate events to modify
			Set<Id> setOfIdsToModify = venuesAndEvents.keySet();

			//List of event we have to populate the location field
			List<EventApi__Event__c> eventsToModify=[SELECT Id, CPAO_Location__c FROM EventApi__Event__c 
		     WHERE Id IN:setOfIdsToModify];

		    for(EventApi__Event__c event:eventsToModify){
		    	event.CPAO_Location__c = venuesAndEvents.get(event.Id);
		    }
		    update eventsToModify;
			
	}

	public static void insertFilter(List<EventApi__Venue__c> venues) {
		
		//A map of ids where key = id of venue that is marked as primary and where
		//value = id of associated event.
		//This map will be used so we could update the related event location field to be 
		//a lookup to its primary venue
		Map<Id, Id> PrimaryVenuesAndRelatedEvents = new Map<Id, Id>();

		
		for(EventApi__Venue__c venue:venues){
			if(venue.EventApi__Is_Primary_Venue__c == True && venue.EventApi__Event__c!=null){
				PrimaryVenuesAndRelatedEvents.put(venue.EventApi__Event__c,venue.Id);
			}
		}
		if(PrimaryVenuesAndRelatedEvents.size()>0){
			modifyEventLocation(PrimaryVenuesAndRelatedEvents);
			
		}

	}

	public static void updateFilter(List<EventApi__Venue__c> newVenues,Map<Id,EventApi__Venue__c> oldVenues) {
		
		//A map of ids where key = id of venue that is marked as primary and where
		//value = id of associated event.
		//This map will be used so we could update the related event location field to be 
		//a lookup to its primary venue
		Map<Id, Id> PrimaryVenuesAndRelatedEvents = new Map<Id, Id>();

		
		for(EventApi__Venue__c venue:newVenues){
			if(venue.EventApi__Is_Primary_Venue__c == True && (!oldVenues.get(venue.Id).EventApi__Is_Primary_Venue__c)
			&& venue.EventApi__Event__c!=null ){
				PrimaryVenuesAndRelatedEvents.put(venue.EventApi__Event__c,venue.Id);
			}
		}
		if(PrimaryVenuesAndRelatedEvents.size()>0){
			modifyEventLocation(PrimaryVenuesAndRelatedEvents);
			
		}

	}
}