@isTest
private class CPAO_InfractionHandlerTest {

	public static List<Account> factoryAccounts;
	public static List<Contact> factoryContacts;
	public static List<CPAO_Infraction__c> infractions;
	public static CPAO_Infraction_Type__c infractionType;

	public static void dataInitialize(Integer numberOfAccounts, Integer numberOfContacts){
		CPAO_TestFactory.automationControlSetup();
		factoryAccounts = CPAO_TestFactory.createAccounts(numberOfAccounts);
		insert factoryAccounts;
		factoryContacts= CPAO_TestFactory.createContacts(factoryAccounts[0],numberOfContacts);
		insert factoryContacts;
		infractionType = CPAO_TestFactory.createInfractionTypes(1, 'test')[0];
		insert infractionType;

		infractions = CPAO_TestFactory.createInfractions((numberOfAccounts+numberOfContacts)*2, infractionType);

	}

	@isTest
	static void activeInserts() {
		dataInitialize(2,2);
		infractions[0].CPAO_Contact__c = factoryContacts[0].Id;
		infractions[1].CPAO_Contact__c = factoryContacts[1].Id;
		infractions[2].CPAO_Account__c = factoryAccounts[0].Id;
		infractions[3].CPAO_Account__c = factoryAccounts[1].Id;
		infractions[4].CPAO_Contact__c = factoryContacts[0].Id;
		infractions[5].CPAO_Contact__c = factoryContacts[1].Id;
		infractions[6].CPAO_Account__c = factoryAccounts[0].Id;
		infractions[7].CPAO_Account__c = factoryAccounts[1].Id;
		for(Integer i=0; i<infractions.size(); i++){
			if(i>3){
				infractions[i].CPAO_Status__c = CPAO_InfractionHandler.OPEN;
			} else {
				infractions[i].CPAO_Status__c = 'Closed';
			}
		}
		insert infractions;
		factoryAccounts = [SELECT Id, CPAO_Number_of_Active_Infractions__c FROM Account WHERE Id IN :factoryAccounts];
		factoryContacts = [SELECT Id, CPAO_Number_of_Active_Infractions__c FROM Contact WHERE Id IN :factoryContacts];
		for(Account account:factoryAccounts){
			System.assert(account.CPAO_Number_of_Active_Infractions__c == 2);
		}

		for(Contact contact:factoryContacts){
			System.assert(contact.CPAO_Number_of_Active_Infractions__c == 2);
		}
	}

	@isTest
	static void mixedUpdates() {
		dataInitialize(2,2);
		infractions[0].CPAO_Contact__c = factoryContacts[0].Id;
		infractions[1].CPAO_Contact__c = factoryContacts[1].Id;
		infractions[2].CPAO_Account__c = factoryAccounts[0].Id;
		infractions[3].CPAO_Account__c = factoryAccounts[1].Id;
		infractions[4].CPAO_Contact__c = factoryContacts[0].Id;
		infractions[5].CPAO_Contact__c = factoryContacts[1].Id;
		infractions[6].CPAO_Account__c = factoryAccounts[0].Id;
		infractions[7].CPAO_Account__c = factoryAccounts[1].Id;

		insert infractions;
		factoryAccounts = [SELECT Id, CPAO_Number_of_Active_Infractions__c FROM Account WHERE Id IN :factoryAccounts];
		factoryContacts = [SELECT Id, CPAO_Number_of_Active_Infractions__c FROM Contact WHERE Id IN :factoryContacts];
		for(Account account:factoryAccounts){
			System.assert(account.CPAO_Number_of_Active_Infractions__c == 2);
		}

		for(Contact contact:factoryContacts){
			System.assert(contact.CPAO_Number_of_Active_Infractions__c == 2);
		}
		for(Integer i=0; i<infractions.size(); i++){
			if(i>3){
				infractions[i].CPAO_Status__c = CPAO_InfractionHandler.OPEN;
			} else {
				infractions[i].CPAO_Status__c = 'Closed';
			}
		}
		update infractions;
		factoryAccounts = [SELECT Id, CPAO_Number_of_Active_Infractions__c FROM Account WHERE Id IN :factoryAccounts];
		factoryContacts = [SELECT Id, CPAO_Number_of_Active_Infractions__c FROM Contact WHERE Id IN :factoryContacts];
		for(Account account:factoryAccounts){
			System.assert(account.CPAO_Number_of_Active_Infractions__c == 1);
		}

		for(Contact contact:factoryContacts){
			System.assert(contact.CPAO_Number_of_Active_Infractions__c == 1);
		}
	}

	@isTest
	static void deletesInserts() {
		dataInitialize(2,2);
		infractions[0].CPAO_Contact__c = factoryContacts[0].Id;
		infractions[1].CPAO_Contact__c = factoryContacts[1].Id;
		infractions[2].CPAO_Account__c = factoryAccounts[0].Id;
		infractions[3].CPAO_Account__c = factoryAccounts[1].Id;
		infractions[4].CPAO_Contact__c = factoryContacts[0].Id;
		infractions[5].CPAO_Contact__c = factoryContacts[1].Id;
		infractions[6].CPAO_Account__c = factoryAccounts[0].Id;
		infractions[7].CPAO_Account__c = factoryAccounts[1].Id;
		for(Integer i=0; i<infractions.size(); i++){
			if(i>3){
				infractions[i].CPAO_Status__c = CPAO_InfractionHandler.OPEN;
			} else {
				infractions[i].CPAO_Status__c = 'Closed';
			}
		}
		insert infractions;
		factoryAccounts = [SELECT Id, CPAO_Number_of_Active_Infractions__c FROM Account WHERE Id IN :factoryAccounts];
		factoryContacts = [SELECT Id, CPAO_Number_of_Active_Infractions__c FROM Contact WHERE Id IN :factoryContacts];
		for(Account account:factoryAccounts){
			System.assert(account.CPAO_Number_of_Active_Infractions__c == 2);
		}

		for(Contact contact:factoryContacts){
			System.assert(contact.CPAO_Number_of_Active_Infractions__c == 2);
		}

		delete infractions;
	}

	@isTest
	static void leToSuspensionsAndRevocations() {
		dataInitialize(2,2);
		infractions[0].CPAO_Contact__c = factoryContacts[0].Id;
		infractions[0].CPAO_Led_to_Suspension__c = true;
		infractions[1].CPAO_Contact__c = factoryContacts[1].Id;
		infractions[1].CPAO_Led_to_Revocation_Deregistration__c = true;
		infractions[2].CPAO_Account__c = factoryAccounts[0].Id;
		infractions[3].CPAO_Account__c = factoryAccounts[1].Id;
		infractions[3].CPAO_Led_to_Suspension__c = true;
		infractions[3].CPAO_Led_to_Revocation_Deregistration__c = true;
		infractions[4].CPAO_Contact__c = factoryContacts[0].Id;
		infractions[5].CPAO_Contact__c = factoryContacts[1].Id;
		infractions[6].CPAO_Account__c = factoryAccounts[0].Id;
		infractions[7].CPAO_Account__c = factoryAccounts[1].Id;

		insert infractions;

		infractions[4].CPAO_Led_to_Suspension__c = true;
		infractions[5].CPAO_Led_to_Revocation_Deregistration__c = true;
		infractions[6].CPAO_Led_to_Suspension__c = true;
		infractions[7].CPAO_Led_to_Revocation_Deregistration__c = true;

		update infractions;
	}
}