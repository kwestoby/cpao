({
    doAction : function(component, event, helper) {
        var apexAction = component.get('c.loadAttestationInfo');
        apexAction.setCallback(this, function(response) {
            var state = response.getState();
            console.log('state');
            console.log(state);
            console.log("step7 load");
            if(state === "SUCCESS"){
                var result = response.getReturnValue();

                component.set("v.currentAMDCPDApplication", result);
                console.log("result");
                console.log(result);
            }
        });
        $A.enqueueAction(apexAction);
    },
	clickOnNextButton : function(component,event,helper){
        var talkingToComponent = event.getParam("componentName");
        if(talkingToComponent == 'step8'){
            console.log("step 7 start talking");
            
            var app = component.get("v.currentAMDCPDApplication");
            console.log("app");
            console.log(app);
            if(app.CPAO_Attestation__c != true){
                console.log("!!!!!didnt attest");
                helper.methodFailure(component, event, helper);
                return;
            }
            console.log("did attest");
            var apexAction = component.get('c.saveApplicationAttestation');
            var hasAMDWaiver = component.get("v.hasAMDWaiver");
            var hasCPDWaiver = component.get("v.hasCPDWaiver");
            apexAction.setParams({
                "currentAMDCPDApplication": app,
                "hasAMDWaiver": hasAMDWaiver,
                "hasCPDWaiver": hasCPDWaiver
            });
            apexAction.setCallback(this, function(response) {
                var state = response.getState();
                console.log("state");
                console.log(state);
                if(state === "SUCCESS"){
                    var result = response.getReturnValue();
                    console.log("result");
                    console.log(result);
                    if(result.status == 'Success'){

                        window.location.href = result.checkOutScreenURL;
                    } else {
                        helper.methodFailure(component, event, helper, result.msg, result.status);    
                    }
                } else {
                    helper.methodFailure(component, event, helper);
                }
            });
            $A.enqueueAction(apexAction);
        }        
    },
})