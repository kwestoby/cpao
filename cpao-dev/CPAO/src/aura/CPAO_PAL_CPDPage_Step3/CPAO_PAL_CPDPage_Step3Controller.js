({
    
    clickOnNextButton : function(component,event,helper){
        var componentNameInEvent=event.getParam("componentName");
        
        if(componentNameInEvent==component.get('v.nameOfThisComponent')){
            if(event.getParam("nextStepName")=='Exit'){ 
                component.set('v.shouldSaveAndQuit',true);
            }
            var validForm = component.find('PALForm').reduce(function (validSoFar, inputCmp) {
                var cpd_declaration=  component.get('v.currentPAL.CPAO_CPD_Declaration__c');  
                // Displays error messages for invalid fields
                if(cpd_declaration==undefined){
                    component.set('v.cpd_required',true);
                }else{
                    component.set('v.cpd_required',false);

                }
                
                inputCmp.showHelpMessageIfInvalid();
                return validSoFar && inputCmp.get('v.validity').valid && cpd_declaration!=undefined ;
            }, true);
            
            if(validForm){
                component.set('v.cpd_required',false);
                component.set('v.currentPAL',event.getParam('PALApplication'));
                
                console.log('click on next event fired  CPD ');
                var nextStep=event.getParam('nextStepName');
                //component.set('v.currentPAL.CPAO_Current_Step__c',nextStep);
                
                
                //console.log('cpd declaration is:  '+ component.get('v.currentPAL').CPAO_CPD_Declaration__c);
                var showSpinnerEvent = component.getEvent("showSpinner");
                showSpinnerEvent.fire();
                helper.upsertPAL(component);
            }
        }
        
        
    }
    
})