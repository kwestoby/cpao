public class CPAO_ReinstatementContainerCTRL {
    
    public static final String OPEN = 'Open';
    public static User LOGGED_USER = getLoggedUserInfo();
    
    public static  List<CPAO_Infraction__c>  getCurrentInfractions(){
         Set<Id> contactId=new Set<Id> ();
         contactId.add(LOGGED_USER.ContactId);
         return CPAO_InfractionSelector.getContactOpenInfractions(contactId);
    }
    
    public static User getLoggedUserInfo(){
        return CPAO_UserSelector.getCurrentUser();
    }
    
	@AuraEnabled
	public static ContainerWrapper getReinstatementInfo() {
		ContainerWrapper results = new ContainerWrapper();
		results.communityURL = System.Label.Community_URL;
        results.currentInfractions = getCurrentInfractions();

		List<CPAO_Application__c> currentReinstatementApp = 
				CPAO_ApplicationSelector.getUserApplicationsByRecordTypeNameAndStatus(OPEN, CPAO_ApplicationHandler.REINSTATEMENT);
		if(!currentReinstatementApp.isEmpty()){
			results.currentReinstatementApp = currentReinstatementApp[0];
		} else {
			return null;
		}

		return results;
	}
    
    public class ContainerWrapper{
		@AuraEnabled public CPAO_Application__c currentReinstatementApp{get;set;}
        @AuraEnabled public List<CPAO_Infraction__c> currentInfractions{get;set;}
        @AuraEnabled public String communityURL{get;set;}
    }
    
}