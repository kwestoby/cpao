({
	 clickOnNextButton : function(component,event,helper){
        var componentNameInEvent=event.getParam("componentName");
        if(componentNameInEvent==component.get('v.nameOfThisComponent')){

            if(event.getParam("nextStepName")=='Exit'){ 
                component.set('v.shouldSaveAndQuit',true);
            }
            //var country = component.get('v.currentPAL.CPAO_Country__c'); 
            //var accountingBody = component.get('v.currentPAL.CPAO_Accounting_Body__c'); 
            
            var metInfoProvided=component.get('v.metInfoProvided');
            var authorization = component.get('v.currentPAL.CPAO_Authorization_to_contact_Reg_Body__c'); 
            var validForm =false;
            var country;
            var accountingBody;
            if(component.get('v.metPracticeInspectionRequirements')==false){
                var validForm= component.find('PALForm').reduce(function (validSoFar, inputCmp) {
                	var practiceInspectionRequirement=  component.get('v.currentPAL.CPAO_Practice_Inspection_Requirement__c');  
                    // Displays error messages for invalid fields
                    if(practiceInspectionRequirement==undefined){
                        //$A.util.addClass(practiceInspectionRequirement_req, 'slds-show');
                        component.set('v.showPI_requirementError',true);
                    }else{
                       // $A.util.removeClass(practiceInspectionRequirement_req, 'slds-show');
                         component.set('v.showPI_requirementError',false);
                    }

                    if(practiceInspectionRequirement==metInfoProvided){
                        country = component.find("country").get("v.value");
                        accountingBody = component.find("accountingBody").get("v.value");
                        if(country==''){
                        	component.set('v.showCountryError',true);
                        
                        }else{
                           // $A.util.removeClass(practiceInspectionRequirement_req, 'slds-show');
                            component.set('v.currentPAL.CPAO_Country__c',country);
                            component.set('v.showCountryError',false);
                        }
                        if(accountingBody==''){
                        	component.set('v.showAccountingBodyError',true);

                        }else{
                            component.set('v.currentPAL.CPAO_Accounting_Body__c',accountingBody);
                            component.set('v.showAccountingBodyError',false);
                        }

                        if(authorization=='--'){
                        	component.set('v.showAuthorizationError',true);
                        }else{
                           // $A.util.removeClass(practiceInspectionRequirement_req, 'slds-show');
                             component.set('v.showAuthorizationError',false);
                        }
                    }
                    inputCmp.showHelpMessageIfInvalid();
                    return validSoFar && inputCmp.get('v.validity').valid && practiceInspectionRequirement!=undefined  && (practiceInspectionRequirement!=metInfoProvided||
                            ( practiceInspectionRequirement==metInfoProvided && country !='--'&& accountingBody!='' && authorization !=''));
                }, true);
            }
            if(validForm || component.get('v.metPracticeInspectionRequirements')==true){

	           if(component.get('v.metPracticeInspectionRequirements')){
	            	component.set('v.currentPAL.CPAO_Practice_Inspection_Requirement__c',component.get('v.metCPAORecords'));
	            }
	            if(component.get('v.currentPAL.CPAO_Practice_Inspection_Requirement__c') != metInfoProvided){
	                component.set('v.currentPAL.CPAO_Country__c',null);
	                component.set('v.currentPAL.CPAO_Accounting_Body__c',null);
	                component.set('v.currentPAL.CPAO_Date_of_Inspection__c',null);
	                component.set('v.currentPAL.CPAO_Authorization_to_contact_Reg_Body__c',null);
	            }
	            var showSpinnerEvent = component.getEvent("showSpinner");
	            showSpinnerEvent.fire();
	           helper.upsertPAL(component);
       		}

        }  	   
        
    }

})