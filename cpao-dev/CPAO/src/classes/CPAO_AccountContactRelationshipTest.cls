@isTest
public with sharing class CPAO_AccountContactRelationshipTest {
    
    public static List<Account> accounts;
    public static List<Contact> contacts;

    static void dataInitialize(Integer contactAmount){
         
        CPAO_TestFactory.automationControlSetup();
        accounts = CPAO_TestFactory.createAccounts(1);
        insert accounts;
        contacts = CPAO_TestFactory.createContacts(accounts[0], contactAmount);
        insert contacts;
        
    }

    static CPAO_Account_Contact_Relationship__c simpleRelationshipSetup(CPAO_Account_Contact_Relationship__c rel){
        rel.CPAO_Primary_Employer__c = 'Yes';
        rel.CPAO_Text__c = 'TestClass Function';
        rel.CPAO_Position_Category__c = 'COO';
        rel.CPAO_Business_Unit__c = 'Internal Audit';
        return rel;
    }
    
    @isTest
    static void relationshipsByContactIdAndStatus() {
        dataInitialize(1);
        CPAO_AccountContactRelationSelector.getRelationshipsByContactIdAndStatus(contacts[0].ID, 'Active');
        CPAO_AccountContactRelationSelector.getRelationshipsByDateAndAccountType(contacts[0].ID,'Ambassador',System.TODAY(), 'Non-Firm');
        CPAO_Account_Contact_Relationship__c rel = simpleRelationshipSetup(CPAO_TestFactory.createAccountContactRelationships(accounts[0], contacts[0], 1)[0]);
        insert rel;
        CPAO_AccountContactRelationSelector.getPrimaryRelationshipsById(rel.ID);
    }

    @isTest
    static void insertPrimaryRecord() {
        dataInitialize(1);
        CPAO_Account_Contact_Relationship__c rel = simpleRelationshipSetup(CPAO_TestFactory.createAccountContactRelationships(accounts[0], contacts[0], 1)[0]);
        insert rel;
        Contact contact = [SELECT Id, CPAO_Primary_Employer__c, Title FROM Contact WHERE Id = :rel.CPAO_Contact__c];
        System.assert(contact.CPAO_Primary_Employer__c == accounts[0].Id && contact.Title == 'TestClass Function');
    }

    @isTest
    static void insertPrimaryRecordThenMarkNoPrimary() {
        dataInitialize(1);
        CPAO_Account_Contact_Relationship__c rel = simpleRelationshipSetup(CPAO_TestFactory.createAccountContactRelationships(accounts[0], contacts[0], 1)[0]);
        insert rel;
        rel.CPAO_Primary_Employer__c = 'No';
        update rel;
        Contact contact = [SELECT Id, CPAO_Primary_Employer__c, Title FROM Contact WHERE Id = :rel.CPAO_Contact__c];
        System.assert(contact.CPAO_Primary_Employer__c == null && contact.Title == null);
    }

    @isTest
    static void insertRecordThenMarkAsPrimary() {
        dataInitialize(1);
        CPAO_Account_Contact_Relationship__c rel = simpleRelationshipSetup(CPAO_TestFactory.createAccountContactRelationships(accounts[0], contacts[0], 1)[0]);
        rel.CPAO_Primary_Employer__c = 'No';
        insert rel;
        rel.CPAO_Primary_Employer__c = 'Yes';
        update rel;
        Contact contact = [SELECT Id, CPAO_Primary_Employer__c, Title FROM Contact WHERE Id = :rel.CPAO_Contact__c];
        System.assert(contact.CPAO_Primary_Employer__c == accounts[0].Id && contact.Title == 'TestClass Function');
    }

    //@isTest
    /*static void insertRecordThenMarkAsPrimaryInBulk() {
        dataInitialize(250);
        List<CPAO_Account_Contact_Relationship__c> rels = new List<CPAO_Account_Contact_Relationship__c>();
        for(Contact contact:contacts){
            CPAO_Account_Contact_Relationship__c rel = simpleRelationshipSetup(CPAO_TestFactory.createAccountContactRelationships(accounts[0], contact, 1)[0]);
            rel.CPAO_Primary_Employer__c = 'No';
            rels.add(rel);
        }
        insert rels;
        for(CPAO_Account_Contact_Relationship__c rel:rels){
            rel.CPAO_Primary_Employer__c = 'Yes';
        }
        Test.startTest();
        update rels;
        Test.stopTest();
        Contact contact = [SELECT Id, CPAO_Primary_Employer__c, Title FROM Contact WHERE Id = :rels[0].CPAO_Contact__c];
        System.assert(contact.CPAO_Primary_Employer__c == accounts[0].Id && contact.Title == 'TestClass Function');
    }*/

    @isTest
    static void duplicateCatching() {
        dataInitialize(1);
        CPAO_Account_Contact_Relationship__c rel = simpleRelationshipSetup(CPAO_TestFactory.createAccountContactRelationships(accounts[0], contacts[0], 1)[0]);
        insert rel;

        CPAO_Account_Contact_Relationship__c rel2 = simpleRelationshipSetup(
            CPAO_TestFactory.createAccountContactRelationships(accounts[0], contacts[0], 1)[0]);
        try{
            insert rel2;
        } catch (Exception e){
            System.debug('error e: '+e.getMessage() );
            System.assert(e.getMessage().contains(CPAO_AccountContactRelationship.DUPLICATE_IN_NEW_RECORDS));
        }
    }
}