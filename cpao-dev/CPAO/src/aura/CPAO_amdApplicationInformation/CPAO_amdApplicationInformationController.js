({
	doInit : function(component, event, helper) {
		var provinces = ["", "Alberta", "British Columbia", "Manitoba", "New Brunswick", 
				"Newfoundland & Labrador", "Nova Scotia", "Northwest Territories", "Nunavut", 
				"Ontario", "Prince Edward Island", "Quebec", "Saskatchewan", "Yukon", "Bermuda"];

		component.set("v.provinces", provinces);

		var apexAction = component.get('c.getWaiverExplanations');
        apexAction.setCallback(this, function(response) {
        	var state = response.getState();
        	if(state === "SUCCESS"){
                var result = response.getReturnValue();
                
                component.set("v.waiverExplanations", result.waiverExplanations);
                component.set("v.application", result.application);

                var listOfWaivers = result.amdWaiverTypes;
                //listOfWaivers.unshift('');
                component.set("v.waiverTypes", listOfWaivers);                
            }
        });
        $A.enqueueAction(apexAction);
	},

    clickOnNextButton : function(component,event,helper){
    	var talkingToComponent = event.getParam("componentName");
    	if(talkingToComponent == 'step6'){
    		var apexAction = component.get('c.saveApplication');
            var app = component.get("v.application");
            console.log('marco test 1');
            var unemployed3TimesAlready = component.get("v.unemployed3TimesAlready");
            if(unemployed3TimesAlready == true && app.CPAO_AMD_Waiver__c == 'Financial Hardship/Unemployment'){
                helper.methodFailure(component, event, helper);
                return;
            }
            console.log('marco test 2');
            var validForm = component.find('baseForm').reduce(function (validSoFar, inputCmp) {
            // Displays error messages for invalid fields
            inputCmp.showHelpMessageIfInvalid();
            return validSoFar && inputCmp.get('v.validity').valid;
            }, true);
            if(validForm == false){
                helper.methodFailure(component, event, helper);
                return;
            }

            if(app.CPAO_Applied_for_Deferral__c == undefined || app.CPAO_Applied_for_Deferral__c == ''){
                helper.methodFailure(component, event, helper, 'Please fill in all required fields', 'Error');
                return;
            }
            console.log('marco test 3');
            var livesInCanada = component.get("v.livesInCanada");

            if(livesInCanada == false){
                if(app.CPAO_Not_a_Resident_of_Canada__c == undefined || app.CPAO_Not_Registered_for_GST_HST__c == undefined || 
                app.CPAO_Not_Present_in_Canada__c == undefined || app.CPAO_Do_not_Carry_Business_in_Canada__c == undefined){
                    helper.methodFailure(component, event, helper, 'Please fill in all required fields', 'Error');
                    return;
                }
            }
	        apexAction.setParams({
		    	"currentAMDCPDApplication": app
		    });
            console.log('marco test 4');
	        apexAction.setCallback(this, function(response) {
	        	var state = response.getState();
                console.log('marco test 5');
                console.log(state);
	        	if(state === "SUCCESS"){
	        		var result = response.getReturnValue();
                    console.log('marco test 6');
                    console.log(result);
	                if(result == 'Success'){
                        var actionToTake = event.getParam("nextStepName");
	                	var applicationHasChanged = component.getEvent("applicationHasChanged");
				        applicationHasChanged.setParams({
                            "nextButtonClickeable" : true,
                            "action" : actionToTake});
				        applicationHasChanged.fire();
	                } else {
                        helper.methodFailure(component, event, helper);    
                    }
	            } else {
                    helper.methodFailure(component, event, helper);
                }
	        });
	        $A.enqueueAction(apexAction);
    	}        
    },

    changeExplanation : function(component,event,helper){
    	var waiverValue = component.get("v.application.CPAO_AMD_Waiver__c");
    	var waiverValueMap = component.get("v.waiverExplanations");
    	var newWaiverExplanation = waiverValueMap[waiverValue];
    	component.set("v.waiverExplanation", newWaiverExplanation);
    },

    checkZeroTax : function(component,event,helper){
        var app = component.get("v.application");
        if(app.CPAO_Not_a_Resident_of_Canada__c == 'Yes' && app.CPAO_Not_Registered_for_GST_HST__c == 'Yes' &&
        app.CPAO_Not_Present_in_Canada__c == 'Yes' && app.CPAO_Do_not_Carry_Business_in_Canada__c == 'Yes'){
            component.set("v.showZeroTaxAttestation", true);
        } else {
            component.set("v.showZeroTaxAttestation", false);
            app.CPAO_Zero_Tax_Attestation__c = false;
            component.set("v.application", app);
        }
    }
})