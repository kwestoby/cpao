@isTest
private class CPAO_InsuranceInfoControllerTest
{
    public static List<Account> accounts;
    public static List<Contact> contacts;
    public static CPAO_Application__c application1;
    
    @isTest
    static void createInsuranceCoverage()
    {
        //CPAO_TestFactory.automationControlSetup();
        accounts = CPAO_TestFactory.createAccounts(1);
        insert accounts;
        
        contacts = CPAO_TestFactory.createContacts(accounts[0], 1);
        insert contacts;
        
        RecordType amdCPDRecordType = [SELECT Id FROM RecordType WHERE DeveloperName = 'CPAO_Reinstatement_Application' AND SobjectType = 'CPAO_Application__c'];
        application1 = new CPAO_Application__c();
        application1.RecordTypeId = amdCPDRecordType.Id;
        application1.CPAO_Contact__c=contacts[0].Id;
        application1.CPAO_Status__c = 'Completed';
        application1.CPAO_Recommendation__c= 'Approve';
        application1.CPAO_Recommendation_Notes__c= 'Test Rec';
        insert application1;
    
        string keystring = 'theyKEYrandomstringhereendswith\n';
        Test.startTest();
        CPAO_InsuranceInfoController.getLoggedUserInfo();
        CPAO_InsuranceInfoController.getCurrentInsurances();
        CPAO_InsuranceInfoController.getIdOfAccountsPrimary();
        CPAO_InsuranceInfoController.getCoverageTypesOptions();
        CPAO_InsuranceInfoController.getStatusOptions();
        CPAO_InsuranceInfoController.getInsuranceProviders();
        CPAO_InsuranceInfoController.componentInit();
        
        //CPAO_InsuranceInfoController.saveTheFile(accounts[0].ID, 'test file', keystring, 'test','test','test');
        //CPAO_FirmRegistration_CTRL.upsertFirmApproval('test', '8 Adelaide St W', 'Toronto', 'ON','M5H 1H1','123 123 1233','test@sample.com','Https://www.sample.org');
        //CPAO_InsuranceInfoController.loadReviewInfo();
        Test.stopTest();
    }
}