({
    doInit : function(component, event, helper) {
        helper.doInit(component, helper);
    },

    clickOnNextButton : function(component, event, helper) {
        let direction = event.getParam("nextStepName");
        helper.handleStep(component, helper, direction);
    },

    clickOnPrevButton : function(component, event, helper) {
        helper.handleStep(component, helper, "Prev");
    },
})