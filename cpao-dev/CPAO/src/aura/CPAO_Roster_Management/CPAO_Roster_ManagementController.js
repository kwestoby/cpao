({
	doInit : function(component, event, helper) {
		helper.init(component, event);
        var action = component.get("c.getUser");
        action.setCallback(this, function(response) {
            var state = response.getState();
            if (state === "SUCCESS") {
                var storeResponse = response.getReturnValue();
                component.set("v.userInfo", storeResponse);
            }
            
        });
        $A.enqueueAction(action);
	}, 
	
    toggle : function(component, event, helper) {
        var toggleFirm = component.find("selectFirm");
        $A.util.toggleClass(toggleFirm, "toggle");
       
        var theSelectedValue = component.get("v.selectedValue");
        if(theSelectedValue === 'APF') {
            var selectedAccounts = component.get("v.selectedAccounts");
            window.location = '/apex/CPAO_AnnualPractitionerFee?id='+selectedAccounts[0].acc.Id
            return;
        }

        /* Passing Value to Apex Controller till $A.enqueueAction(action) */
        component.set('v.instructionRead',true); // InstructionRead set to True after reading the instruction
        var action = component.get("c.queryInstructionScreen");
            action.setCallback(this, function(response){
            component.set("v.instructionScreen", response.getReturnValue());
        });
        $A.enqueueAction(action);   
    },
    selectPaymentType: function (component, event, helper) {
        helper.selectPaymentType(component, event);
    },
    
    selectedCheckedAccount: function (component, event, helper) {
    	helper.selectedCheckedAccount(component, event);
    }
    
})