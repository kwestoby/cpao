public with sharing class CPAO_InfractionWireframeCTRL {

    public static User LOGGED_USER = getLoggedUserInfo();
    public static  Map<Id,String>returnedIdsAndInfractionTypes = new Map<Id,String>();
    public static List<RecordType> REINSTATEMENT_APPLICATION_RECORD_TYPE=CPAO_ApplicationSelector.getReinstatementApplicationRecordType();

    public PageReference nextStep() {
        String stepId = Framework.PageUtils.getParam('id');
        joinApi.JoinUtils joinUtil;
        String redirectURL = '';
        if(!Test.isRunningTest()){
            joinUtil = new joinApi.JoinUtils(stepId);
            redirectURL = joinUtil.navStepsMap.get('Next');
        }
            
        JSONGenerator gen = JSON.createGenerator(true);
        gen.writeStartObject();
        gen.writeStringField('stepId', stepId);
        gen.writeStringField('redirectURL', redirectURL);
        gen.writeEndObject();
        if(!Test.isRunningTest()){
            joinUtil.setRedirectURLCookie(gen.getAsString());
        }
        
        return new PageReference(redirectURL);

    }

    public PageReference previousStep() {
        if(!Test.isRunningTest()){
            String stepId = Framework.PageUtils.getParam('id');
            joinApi.JoinUtils joinUtil = new joinApi.JoinUtils(stepId);
            return new PageReference(joinUtil.getPreviousStep('{}'));
        } else {
            return null;
        }
    }

    public PageReference cancelJP() {
        String stepId = Framework.PageUtils.getParam('id');
        if(!Test.isRunningTest()){
            joinApi.JoinUtils joinUtil = new joinApi.JoinUtils(stepId);
            joinUtil.deleteCookies();
        }
        
        //return new PageReference(joinUtil.navStepsMap.get('First'));
        return new PageReference('/');

    }
 
    @AuraEnabled
    public static User getLoggedUserInfo(){
        return CPAO_UserSelector.getCurrentUser();
    }

    @AuraEnabled
    public static  List<CPAO_Infraction__c>  getCurrentInfractions(){
         Set<Id> contactId=new Set<Id> ();
         contactId.add(LOGGED_USER.ContactId);
        

        
         return CPAO_InfractionSelector.getContactOpenInfractions(contactId);

    }

    @AuraEnabled
    public static  Map<Id,String>  getAllInfractionsType(){
         List<CPAO_Infraction_Type__c> infractionTypes= [SELECT Id, Name FROM CPAO_Infraction_Type__c];
         for(CPAO_Infraction_Type__c infractionType: infractionTypes){
            returnedIdsAndInfractionTypes.put(infractionType.Id,infractionType.Name);
         }
         return returnedIdsAndInfractionTypes;

    }

    @AuraEnabled
    public static  CPAO_Application__c  getReinstatementApplication(){
        List<CPAO_Application__c> reinstatementApplications = 
        CPAO_ApplicationSelector.getUserApplicationsByRecordTypeNameAndStatus('Open', 'CPAO_Reinstatement_Application');

        if(reinstatementApplications.size()>0){
            return reinstatementApplications[0];
        }else{
            //create a new reinstatement application because it has not been created in previous steps of join process
            CPAO_Application__c returnedApplication = new CPAO_Application__c();
            returnedApplication.CPAO_Contact__c=LOGGED_USER.ContactId;
            returnedApplication.CPAO_Status__c='Open';
            returnedApplication.RecordTypeId =REINSTATEMENT_APPLICATION_RECORD_TYPE[0].Id;

            insert(returnedApplication);
            return returnedApplication;
        
        }
    }
    

    
    @AuraEnabled
    public static  String  componentInit(){
        ResultWrapper response = new ResultWrapper();
        response.breachInstructions=CPAO_MetadataTypeSelector.getBreachInstructions();
        response.currentInfractions=getCurrentInfractions();
        response.infractionTypes=getAllInfractionsType();
        response.reinstatementApplication=getReinstatementApplication();
        Map<Id,CPAO_Infraction__c> returnedMap= new Map<Id,CPAO_Infraction__c>();
        for (CPAO_Infraction__c infraction:response.currentInfractions){
            returnedMap.put(infraction.Id,infraction);
        }
        response.currentInfractionsMap=returnedMap;
        response.infractionWireframeControllerReference= System.Label.CPAO_InfractionWireframeControllerReference;

        return JSON.serialize(response) ;
    }

    // @Author : Halima Dosso
    //This method links a specific infraction to a reinstatement application
    @AuraEnabled
    public static  CPAO_Infraction__c   linkInfractionToReinstatementApplication(Id infraction, Id  application){
        List<CPAO_Infraction__c> infractionsToModify=CPAO_InfractionSelector.getInfractionById(infraction);
        infractionsToModify[0].CPAO_Reinstatement_Application__c=application;
        update infractionsToModify[0];
        return infractionsToModify[0];
    }

    public class ResultWrapper{
        @AuraEnabled public List<CPAO_Infraction__c> currentInfractions{get;set;}
        @AuraEnabled public List<Breach_Instruction__mdt> breachInstructions{get;set;}
        @AuraEnabled public Map<Id,String> infractionTypes {get;set;}
        @AuraEnabled public CPAO_Application__c reinstatementApplication {get;set;}
        @AuraEnabled public Map<Id,CPAO_Infraction__c> currentInfractionsMap {get;set;}
        @AuraEnabled public String infractionWireframeControllerReference {get;set;}    }
}