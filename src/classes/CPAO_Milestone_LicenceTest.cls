@isTest
public class CPAO_Milestone_LicenceTest {
    
    public static List<Account> factoryAccounts;
    public static List<Contact> factoryContacts;
    public static  List<CPAO_Licence__c> factoryActiveLicences;
    public static Integer NUMBER_LICENCES = 1;
        
        @testSetup
        public static void setup(){
            factoryAccounts = CPAO_TestFactory.createAccounts(2);
            insert factoryAccounts;
            factoryContacts= CPAO_TestFactory.createContacts(factoryAccounts[0],2);
            insert factoryContacts;
            List<CPAO_Application__c> factoryApplications =  CPAO_TestFactory.createAMDCPDpplication(1, factoryContacts[0]);       
            insert factoryApplications; 
            factoryActiveLicences = CPAO_TestFactory.createActiveLicences(NUMBER_LICENCES,factoryContacts[0],factoryApplications[0]);
           System.debug('factoryActiveLicences' + factoryActiveLicences[0]);
            insert factoryActiveLicences;
        }
    
    @isTest
    public static void beforeUpdateTest(){
        factoryActiveLicences = [SELECT Id, Name FROM CPAO_Licence__c];
        test.startTest();
                System.debug('Test' + factoryAccounts);
        System.debug('Test' + factoryActiveLicences);
        for(CPAO_Licence__c licence : factoryActiveLicences){
            licence.CPAO_Status__c = 'Suspended';
        }
        update factoryActiveLicences;
        test.stopTest();
        Integer historyTrack = [SELECT COUNT(Id) FROM CPAO_MilestoneAndHistory__c].size();
        System.debug('Number of records:' + historyTrack);
        System.assert(NUMBER_LICENCES == historyTrack);
        
    }
    
}