<?xml version="1.0" encoding="UTF-8"?>
<CustomMetadata xmlns="http://soap.sforce.com/2006/04/metadata" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xmlns:xsd="http://www.w3.org/2001/XMLSchema">
    <label>Retired Exemption</label>
    <protected>false</protected>
    <values>
        <field>AMD_or_CPD__c</field>
        <value xsi:type="xsd:string">CPD</value>
    </values>
    <values>
        <field>CPAO_Explanation__c</field>
        <value xsi:type="xsd:string">You are retired, have ceased full-time practice, full-time employment or full-time business activity. The sum of your age and the total years of aggregate membership in CPA Ontario, CGA Ontario, CMA Ontario and one or more provincial bodies or an accounting body recognized by the Council (or Board), equals or exceeds “70” AND you do not hold a public accounting licence or provide any Reliance Services further to Regulation 4-5, Section 1.3.) If you provided other professional service(s), your gross annual income for providing professional services for which you were remunerated, was less than $25,000.</value>
    </values>
    <values>
        <field>CPAO_Percent_Off__c</field>
        <value xsi:nil="true"/>
    </values>
    <values>
        <field>Explanation_Third_Year__c</field>
        <value xsi:type="xsd:string">You are retired, have ceased full-time practice, full-time employment or full-time business activity. The sum of your age and the total years of aggregate membership in CPA Ontario, CGA Ontario, CMA Ontario and one or more provincial bodies or an accounting body recognized by the Council (or Board), equals or exceeds “70” AND you do not hold a public accounting licence or provide any Reliance Services further to Regulation 4-5, Section 1.3.) If you provided other professional service(s), your gross annual income for providing professional services for which you were remunerated, was less than $25,000.</value>
    </values>
    <values>
        <field>Full_Name__c</field>
        <value xsi:type="xsd:string">Retired Exemption</value>
    </values>
</CustomMetadata>
