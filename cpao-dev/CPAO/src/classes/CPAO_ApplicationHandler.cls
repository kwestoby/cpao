public without sharing class CPAO_ApplicationHandler {
    public static final String ADMIT = 'Admit';
    public static final String ADMIT_CONDITIONS = 'Admit with Conditions';
    public static final String MEMBER = 'Member';
    public static final String MEMBER_ACTIVE = 'Member Active';
    public static final String SUBMITTED = 'Submitted';
    public static final String COMPLETED = 'Completed';
    public static final String SUBMITTED_NOT_PAID='Submitted not paid';
    public static final String SUBMITTED_DEFERRED = 'Submitted-Payment Deferred';
    public static final String IN_PROGRESS = 'In Progress';
    public static final String REINSTATEMENT = 'Reinstatement';
    public static final String REINSTATE = 'Reinstate';  
    public static final String DENY = 'Deny';
    public static final String ADDITIONAL_INFO_REQUIRED = 'Additional Information is Required';
    public static final String AMD_CPD = 'AMD/CPD';
    public static final String AMD_CPD_VAL = 'AMD / CPD';
    public static final String CPD_ONLY = 'CPD Only';
    public static final String MRA_APPLICATION = 'MRA Transfer';
    public static final String PAL_APPLICATION = 'PAL Application';
    public static final String PAL_RENEWAL = 'PAL Renewal';
    public static final String COA_APPLICATION = 'COA Application';
    public static final String ACTIVE = 'Active';
    public static final String ORIGINAL = 'Original';
    public static final String RENEWED = 'Renewed';
    public static final String APPROVE = 'Approve';
    public static final String PUBLIC_ACCOUNTING_TYPE = 'Public Accounting Licence';
    public static final String CERTIFICATE_OF_AUTHORIZATION = 'Certificate Of Authorization';
    public static final String APPLICATION_WITHDRAWN = 'CPAO_Application_Withdrawn';
    public static final String CONFIRMATION_CPD = 'Confirmation_CPD';
    public static final String READMISSION_RECORDTYPENAME = 'Readmission';
    public static final String PAL_SUBSC_PLAN_NAME='Public Accounting Licence';
    public static final String EXTRA_ORD_CONSEQ ='Other extraordinary consequences';
    public static final String IN_EXEMPTION_REVIEW = 'In Exemption Review';
    public static boolean firstRunBefore = true;
    public static boolean firstRunAfter = true;


    //All the methods in this class act as filters and then send appropriate records to the 
    //CPAO_ApplicationServices or CPAO_ApplicationServiceUtils Class
    public static Id getApplicationRecordTypeId(String recordTypename) {
        return Schema.SObjectType.CPAO_Application__c.getRecordTypeInfosByName().get(recordTypename).getRecordTypeId();
    }
    public static void beforeInsertFilter(List<CPAO_Application__c> newApplications) {
        CPAO_ApplicationServices.stampUserContactOnApplication(newApplications);
        List<CPAO_Application__c> amdApplications = new List<CPAO_Application__c> ();
        Id amdCPDRecordTypeid = getApplicationRecordTypeId(AMD_CPD);
        for (CPAO_Application__c application : newApplications) {
            if (amdCPDRecordTypeid != null) {
                if (application.recordTypeId == amdCPDRecordTypeid) {
                    amdApplications.add(application);
                }
            }
        }
        if (!amdApplications.isEmpty()) {
            CPAO_ApplicationServices.amdANDcpdInitializer(amdApplications);
        }
        CPAO_ApplicationServiceUtils.checkDuplicateApplication(newApplications);
    }

    public static void beforeUpdateFilter(List<CPAO_Application__c> newApplications, Map<Id, CPAO_Application__c> oldMap) {

        Id reinstatementRecordTypeId = getApplicationRecordTypeId(REINSTATEMENT);
        Id readmissionRecordTypeId = getApplicationRecordTypeId(READMISSION_RECORDTYPENAME);
        Id PALRecordTypeId = getApplicationRecordTypeId(PAL_APPLICATION);
        Id amdCPDRecordTypeid = getApplicationRecordTypeId(AMD_CPD);
        Id COARecordTypeId = getApplicationRecordTypeId(COA_APPLICATION);

        List<CPAO_Application__c> inReviewApplications = new List<CPAO_Application__c> ();
        
        List<CPAO_Application__c> roReinstate = new List<CPAO_Application__c> ();
        List<CPAO_Application__c> roDeny = new List<CPAO_Application__c> ();
        List<CPAO_Application__c> registrarReinstated = new List<CPAO_Application__c> ();
        List<CPAO_Application__c> registrarDenied = new List<CPAO_Application__c> ();
        
        List<CPAO_Application__c> roReadmit = new List<CPAO_Application__c> ();
        List<CPAO_Application__c> registrarReadmit = new List<CPAO_Application__c> ();
        
        List<CPAO_Application__c> RO_Staff_applications = new List<CPAO_Application__c> ();
        List<CPAO_Application__c> poaAcceptedApplications = new List<CPAO_Application__c> ();
        List<CPAO_Application__c> poaDenyApplications = new List<CPAO_Application__c> ();
        List<CPAO_Application__c> poaMoreInfoApplications = new List<CPAO_Application__c> ();
        List<CPAO_Application__c> ReinstatementApps_for_email = new List<CPAO_Application__c> ();
        

        for (CPAO_Application__c application : newApplications) {
            //REINSTATEMENT APPLICATION
            if (reinstatementRecordTypeId != null) {
                if (application.RecordTypeId == reinstatementRecordTypeId) {
                    if(application.CPAO_Application_Type__c == 'Reinstatement' && application.CPAO_Decision__c=='Approve' && application.CPAO_Status__c=='Completed'){
                           {
                            ReinstatementApps_for_email.add(application);
                           }
                    }
                    
                    
                    
                    if (application.CPAO_Recommendation__c != oldMap.get(application.Id).CPAO_Recommendation__c) {
                        if (application.CPAO_Recommendation__c == REINSTATE) {
                            roReinstate.add(application);
                        }

                        if (application.CPAO_Recommendation__c == DENY) {
                            roDeny.add(application);
                        }
                    }

                    if (application.CPAO_Decision__c != oldMap.get(application.Id).CPAO_Decision__c) {
                        if (application.CPAO_Decision__c == REINSTATE) {
                            registrarReinstated.add(application);
                        }

                        if (application.CPAO_Decision__c == DENY) {
                            registrarDenied.add(application);
                        }
                    }
                }
            }
            
            //READMISSION APPLICATION
            if (readmissionRecordTypeId != null) {
                if (application.RecordTypeId == readmissionRecordTypeId) {
                    if (application.CPAO_Recommendation__c != oldMap.get(application.Id).CPAO_Recommendation__c) {
                        if (application.CPAO_Recommendation__c == APPROVE) {
                            roReadmit.add(application);
                        }

                        /*if (application.CPAO_Recommendation__c == DENY) {
                            roDeny.add(application);
                        }*/
                    }

                    if (application.CPAO_Decision__c != oldMap.get(application.Id).CPAO_Decision__c) {
                        if (application.CPAO_Decision__c == APPROVE && application.CPAO_Status__c == COMPLETED) {
                            registrarReadmit.add(application);
                        }

                        /*if (application.CPAO_Decision__c == DENY) {
                            registrarDenied.add(application);
                        }*/
                    }
                    
                    if (application.CPAO_Status__c != oldMap.get(application.Id).CPAO_Status__c) {
                        if (application.CPAO_Decision__c == APPROVE && application.CPAO_Status__c == COMPLETED) {
                            registrarReadmit.add(application);
                        }
                    }
                }
            }
            
            //PAL APPLICATION
            if (PALRecordTypeId != null && application.recordTypeId == PALRecordTypeId) {
                if (oldMap.get(application.Id).CPAO_Status__c != application.CPAO_Status__c && application.CPAO_Status__c == SUBMITTED) {
                    RO_Staff_applications.add(application);
                }
            }

            //COA APPLICATION
            if (COARecordTypeId != null && application.recordTypeId == COARecordTypeId) {

                if (oldMap.get(application.Id).CPAO_Status__c != application.CPAO_Status__c && application.CPAO_Status__c == SUBMITTED) {
                    RO_Staff_applications.add(application);
                }
            }

            //AMD APPLICATION           
            if (amdCPDRecordTypeid != null && application.recordTypeId == amdCPDRecordTypeid) {

                if (oldMap.get(application.Id).CPAO_Status__c != application.CPAO_Status__c && application.CPAO_Declaration__c == EXTRA_ORD_CONSEQ &&
                    (application.CPAO_Status__c == SUBMITTED_NOT_PAID || application.CPAO_Status__c == SUBMITTED_DEFERRED || application.CPAO_Status__c == SUBMITTED)) {
                    application.CPAO_Sub_Status__c = IN_EXEMPTION_REVIEW;
                    RO_Staff_applications.add(application);
                }

                if (application.recordTypeId == amdCPDRecordTypeid) {
                    if(application.CPAO_Status__c == 'Completed' && application.CPAO_Decision__c == 'Approve' 
                    && (oldMap.get(application.Id).CPAO_Status__c != application.CPAO_Status__c || 
                        oldMap.get(application.Id).CPAO_Decision__c != application.CPAO_Decision__c)){
                        poaAcceptedApplications.add(application);
                    }
                    if(application.CPAO_Status__c == 'Deny' && application.CPAO_Decision__c == 'Approve' 
                    && (oldMap.get(application.Id).CPAO_Status__c != application.CPAO_Status__c || 
                        oldMap.get(application.Id).CPAO_Decision__c != application.CPAO_Decision__c)){
                        poaDenyApplications.add(application);
                    }
                    if(application.CPAO_Decision__c == 'Additional Information Required' && 
                        oldMap.get(application.Id).CPAO_Decision__c != 'Additional Information Required'){
                        poaMoreInfoApplications.add(application);
                    }
                }
            
            }

            if (application.CPAO_Status__c == 'In Review' && application.CPAO_Status__c != oldMap.get(application.Id).CPAO_Status__c) {
                inReviewApplications.add(application);
            }
        }
        
        //READMISSION RECOMMENDATION - Looks for open infractions
        if (!roReadmit.isEmpty() && firstRunBefore) {
            firstRunBefore = false;
            CPAO_ApplicationServices.roReinstateApplications(roReadmit);
        }
        
        //READMISSION DECISION - Looks for open infractions
        if (!registrarReadmit.isEmpty() && firstRunBefore) {
            firstRunBefore = false;
            CPAO_ApplicationServices.registrarReadmitApplications(registrarReadmit);
        }
        
        //REINSTATEMENT RECOMMENDATION - Looks for open infractions 
        if (!roReinstate.isEmpty() && firstRunBefore) {
            firstRunBefore = false;
            CPAO_ApplicationServices.roReinstateApplications(roReinstate);
        }
        if (!roDeny.isEmpty() && firstRunBefore) {
            firstRunBefore = false;
            CPAO_ApplicationServiceUtils.sendToRegistrarQueue(roDeny);
        }
        
        //REINSTATEMENT DECISION
        if (!registrarReinstated.isEmpty() && firstRunBefore) {
            firstRunBefore = false;
            CPAO_ApplicationServices.registrarReinstateApplications(registrarReinstated);
        }
        if (!registrarDenied.isEmpty() && firstRunBefore) {
            firstRunBefore = false;
            CPAO_ApplicationServices.registrarDenyApplications(registrarDenied);
        }
        if (!inReviewApplications.isEmpty() && firstRunBefore) {
            firstRunBefore = false;
            CPAO_ApplicationServices.inReviewApplications(inReviewApplications);
        }

        if (!RO_Staff_applications.isEmpty() && firstRunAfter) {
            CPAO_ApplicationServiceUtils.sendToRO_rQueue(RO_Staff_applications);
        }
        
        if (!ReinstatementApps_for_email.isEmpty() ) {
            CPAO_ApplicationServices.Find_PAL_License(ReinstatementApps_for_email);
        }
        
        //POA Decision and Recomendation Actions
        if (!poaAcceptedApplications.isEmpty()) {
            CPAO_ApplicationServices.poaCompleted(poaAcceptedApplications);
        }

        if (!poaMoreInfoApplications.isEmpty()) {
            CPAO_ApplicationServices.poaStamp(poaMoreInfoApplications);
        }

        if (!poaDenyApplications.isEmpty()) {
            //CPAO_ApplicationServices.poaStamp(poaDenyApplications);
        }

    }
    public static void afterInsertFilter(List<CPAO_Application__c> newApplications) {


    }
    public static void afterUpdateFilter(List<CPAO_Application__c> newApplications, Map<Id, CPAO_Application__c> oldMap) {
        Id amdCPDRecordTypeid = getApplicationRecordTypeId(AMD_CPD);
        List<CPAO_Application__c> registrarAdmited = new List<CPAO_Application__c> ();
        List<CPAO_Application__c> reinstatementPaid = new List<CPAO_Application__c> ();
        List<CPAO_Application__c> amdApplications = new List<CPAO_Application__c> ();
        Id PALRecordTypeId = getApplicationRecordTypeId(PAL_APPLICATION);
        Id PALRenewalRecordTypeId = getApplicationRecordTypeId(PAL_RENEWAL);
        Id COARecordTypeId = getApplicationRecordTypeId(COA_APPLICATION);
        List<CPAO_Licence__c> licencesToAdd = new List<CPAO_Licence__c> ();
        List<CPAO_Licence__c> PAL_Renewal_LicencesToAdd = new List<CPAO_Licence__c> ();
        List<CPAO_Licence__c> PALlicencesToAdd = new List<CPAO_Licence__c> ();
        List<CPAO_Licence__c> COAlicencesToAdd = new List<CPAO_Licence__c> ();
        Set<Id> PAL_renewalContactIds= new Set<Id>(); // Subcriptions to be modified with employer is paying PALR set to "no"
        
        Set<Id> PAL_ContactIds= new Set<Id>();
        List<CPAO_Application__c> cpdEmails = new List<CPAO_Application__c> ();

        List<CPAO_Application__c> successfullwithdrawals = new List<CPAO_Application__c> ();

        Map<Id, Id> appsandRelatedLicences = new Map<Id, Id> ();

        for(CPAO_Application__c application : newApplications) {
            if(application.CPAO_Decision__c != null) {
                if(application.CPAO_Decision__c != oldMap.get(application.Id).CPAO_Decision__c) {
                    if(application.CPAO_Decision__c == ADMIT || application.CPAO_Decision__c == ADMIT_CONDITIONS) {
                        registrarAdmited.add(application);
                    }
                }
            }
        }

        for (CPAO_Application__c application : newApplications) {
            if(application.CPAO_Status__c == 'Withdrawn' && oldMap.get(application.Id).CPAO_Status__c != 'Withdrawal Requested') {
                successfullwithdrawals.add(application);
            }
        }

        if(!registrarAdmited.isEmpty() && firstRunAfter) {
            firstRunAfter = false;
            CPAO_ApplicationServices.registrarAdmitedApplications(registrarAdmited);
        }

        Id recordTypeId = getApplicationRecordTypeId(REINSTATEMENT);

        for(CPAO_Application__c application : newApplications) {
            if(recordTypeId != null) {
                if(application.RecordTypeId == recordTypeId) {
                    if(application.CPAO_Status__c == SUBMITTED &&
                        oldMap.get(application.Id).CPAO_Status__c != SUBMITTED && oldMap.get(application.Id).CPAO_Status__c != IN_PROGRESS) {
                        reinstatementPaid.add(application);
                    }
                }
            }

            if(amdCPDRecordTypeid != null){
                if (application.RecordTypeId == amdCPDRecordTypeid){
                    if(application.CPAO_Sub_Type__c == AMD_CPD_VAL){

                        if(application.CPAO_Applied_for_Deferral__c == 'Yes'){
                            if(application.CPAO_Status__c == SUBMITTED_DEFERRED && 
                            oldMap.get(application.Id).CPAO_Status__c != SUBMITTED_DEFERRED){
                                cpdEmails.add(application);
                            }

                        } else if(application.CPAO_Employer_is_paying_AMD__c == 'Yes'){
                            if(application.CPAO_Status__c == SUBMITTED && 
                            oldMap.get(application.Id).CPAO_Status__c != SUBMITTED){
                                cpdEmails.add(application);
                            }

                        } else if(application.CPAO_Applied_for_Deferral__c == 'No' && application.CPAO_Employer_is_paying_AMD__c == 'No'){
                            if(application.CPAO_Status__c == COMPLETED && oldMap.get(application.Id).CPAO_Status__c != COMPLETED){
                                cpdEmails.add(application);
                            }
                        }

                    } else if(application.CPAO_Sub_Type__c == CPD_ONLY){
                        if(application.CPAO_Status__c == COMPLETED && oldMap.get(application.Id).CPAO_Status__c != COMPLETED){
                            cpdEmails.add(application);
                        }
                    }
                }
            }

            if(PALRecordTypeId != null && application.recordTypeId == PALRecordTypeId) {


                if(oldMap.get(application.Id).CPAO_Decision__c != application.CPAO_Decision__c
                    && application.CPAO_Decision__c == APPROVE ) {
                    CPAO_Licence__c associatedLicence = new CPAO_Licence__c();
                    associatedLicence.CPAO_Related_Application__c = application.Id;
                    associatedLicence.CPAO_Status__c = ACTIVE;
                    associatedLicence.CPAO_Sub_Status__c = ORIGINAL;
                    associatedLicence.CPAO_Licence_Type__c = PUBLIC_ACCOUNTING_TYPE;
                    associatedLicence.CPAO_Issued_to__c = application.CPAO_Contact__c;
                    associatedLicence.CPAO_Effective_Date__c = Date.today();
                    associatedLicence.CPAO_Expiry_Date__c = CPAO_ApplicationServices.calculatePALExpiryDate(associatedLicence.CPAO_Effective_Date__c);
                    licencesToAdd.add(associatedLicence);
                    PALlicencesToAdd.add(associatedLicence);
                    PAL_ContactIds.add(application.CPAO_Contact__c);

                }

            }
            if(PALRenewalRecordTypeId != null && application.recordTypeId == PALRenewalRecordTypeId) {

                if(oldMap.get(application.Id).CPAO_Decision__c != application.CPAO_Decision__c && application.CPAO_Decision__c == APPROVE  
                     && application.CPAO_Status__c == COMPLETED) {

                    List<Application_Date__mdt> applicationDates= CPAO_MetadataTypeSelector.getApplicationDates();
                    if(!applicationDates.isEmpty()){
                        CPAO_Licence__c associatedLicence = new CPAO_Licence__c();
                        associatedLicence.CPAO_Related_Application__c = application.Id;
                        associatedLicence.CPAO_Status__c = ACTIVE;
                        associatedLicence.CPAO_Sub_Status__c = RENEWED;
                        associatedLicence.CPAO_Licence_Type__c = PUBLIC_ACCOUNTING_TYPE;
                        associatedLicence.CPAO_Issued_to__c = application.CPAO_Contact__c;
                        Date effectiveDate=applicationDates[0].CPAO_PAL_Renewal_Effective_Date__c;
                        Date expiryDate = applicationDates[0].CPAO_PAL_Renewal_Expiry_Date__c;
                        Integer currentYear = Date.today().year();
                        associatedLicence.CPAO_Effective_Date__c = date.newinstance(currentYear,effectiveDate.month(),effectiveDate.day());
                        associatedLicence.CPAO_Expiry_Date__c = date.newinstance(currentYear+1,expiryDate.month(),expiryDate.day());
                        licencesToAdd.add(associatedLicence);
                        PAL_Renewal_LicencesToAdd.add(associatedLicence);
                        PAL_renewalContactIds.add(application.CPAO_Contact__c);
                         
                    }

                }

            }
            

            if(COARecordTypeId != null && application.recordTypeId == COARecordTypeId) {



                if(oldMap.get(application.Id).CPAO_Decision__c != application.CPAO_Decision__c
                    && application.CPAO_Decision__c == APPROVE) {
                    CPAO_Licence__c associatedLicence = new CPAO_Licence__c();
                    associatedLicence.CPAO_Related_Application__c = application.Id;
                    associatedLicence.CPAO_Status__c = ACTIVE;
                    associatedLicence.CPAO_Sub_Status__c = ORIGINAL;
                    associatedLicence.CPAO_Licence_Type__c = CERTIFICATE_OF_AUTHORIZATION;
                    // add name of professional corporation 
                    associatedLicence.CPAO_Issued_to__c = application.CPAO_Contact__c;
                    associatedLicence.CPAO_Effective_Date__c = Date.today();
                    //associatedLicence.CPAO_Expiry_Date__c=CPAO_ApplicationServices.calculatePALExpiryDate(associatedLicence.CPAO_Effective_Date__c);
                    associatedLicence.CPAO_Name_of_Professional_Corporation__c = application.CPAO_COA_Firm_Name__c;
                    licencesToAdd.add(associatedLicence);
                    COAlicencesToAdd.add(associatedLicence);

                }

            }

        }

        if(!cpdEmails.isEmpty()) {
            CPAO_ApplicationServices.sendEmails(cpdEmails, CONFIRMATION_CPD);
        }

        if(!successfullwithdrawals.isEmpty()) {
            CPAO_ApplicationServices.sendEmails(successfullwithdrawals, APPLICATION_WITHDRAWN);
        }

        if(!reinstatementPaid.isEmpty() && firstRunAfter) {
            firstRunAfter = false;
            CPAO_ApplicationServices.reinstatementPaidApplications(reinstatementPaid);
        }


        if(!licencesToAdd.isEmpty() && firstRunAfter) {
            firstRunAfter = false;
            insert licencesToAdd;
            Set<Id> applicationsIdSet = new Set<Id> ();
            for(CPAO_Licence__c licence : licencesToAdd) {
                appsandRelatedLicences.put(licence.CPAO_Related_Application__c, licence.Id);
                applicationsIdSet.add(licence.CPAO_Related_Application__c);
            }
            List<CPAO_Application__c> appsToModify = CPAO_ApplicationSelector.getApplicationById(applicationsIdSet);
            if(!appsToModify.isEmpty()) {
                for (CPAO_Application__c application : appsToModify) {
                    application.CPAO_Related_Licence__c = appsandRelatedLicences.get(application.Id);
                }
                update appsToModify;

            }
            if(!PALlicencesToAdd.isEmpty()) {
                CPAO_ApplicationServices.sendApprovalEmail(PALlicencesToAdd, 'PAL');

            }
            if(!COAlicencesToAdd.isEmpty()) {
                CPAO_ApplicationServices.sendApprovalEmail(COAlicencesToAdd, 'COA');
            }

            if(!PAL_Renewal_LicencesToAdd.isEmpty()) {
                CPAO_ApplicationServices.sendApprovalEmail(PAL_Renewal_LicencesToAdd, 'PAL-Renewal');
            }

        }

        if(!PAL_renewalContactIds.isEmpty()){
            List<OrderApi__Subscription__c> PALSubscriptionsToUpdate = CPAO_FontevaObjectSelector.getContactsSubscriptions(PAL_SUBSC_PLAN_NAME,PAL_renewalContactIds);
            if(!PALSubscriptionsToUpdate.isEmpty()){
                for (OrderApi__Subscription__c individualSubscription : PALSubscriptionsToUpdate){
                    individualSubscription.CPAO_PALB_Approved__c=true;
                }
                update PALSubscriptionsToUpdate;
            }
        }

        if(!PAL_ContactIds.isEmpty()){ //THIS HAS TO BE MODIFIED ONCE WE DESIGN THE INITIAL SUBSCRIPTION CREATION
            
            List<OrderApi__Subscription__c> newSubscriptionsToAdd = new List<OrderApi__Subscription__c>();
            List<OrderApi__Subscription_Plan__c> PAL_SubsriptionPlan = CPAO_FontevaObjectSelector.getSubscriptionPlanInfo(PAL_SUBSC_PLAN_NAME);
            OrderApi__Item_Subscription_Plan__c palItem = CPAO_FontevaObjectSelector.getSubscriptionPlanItems(PAL_SUBSC_PLAN_NAME)[0];
            if(!PAL_SubsriptionPlan.isEmpty()){

                for(Id contactId:PAL_ContactIds){
                    OrderApi__Subscription__c PAL_subscription = new OrderApi__Subscription__c();
                    PAL_subscription.OrderApi__Contact__c=contactId;
                    PAL_subscription.OrderApi__Subscription_Plan__c=PAL_SubsriptionPlan[0].Id;
                    PAL_subscription.OrderApi__Status__c='Active';
                    PAL_subscription.CPAO_Effective_Date__c = Date.today();
                    PAL_subscription.OrderApi__Item__c=palItem.OrderApi__Item__r.Id;
                    PAL_subscription.OrderApi__Business_Group__c=palItem.OrderApi__Item__r.OrderApi__Business_Group__c;
                    
                    /*PAL_subscription
                    PAL_subscription*/
                    newSubscriptionsToAdd.add(PAL_subscription);
                }

                if(!newSubscriptionsToAdd.isEmpty()){
                    insert newSubscriptionsToAdd;
                }
            }
            
        } //END


    }
}