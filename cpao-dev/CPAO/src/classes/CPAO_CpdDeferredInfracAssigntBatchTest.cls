@isTest
private class CPAO_CpdDeferredInfracAssigntBatchTest {
	
	public static List<Account> factoryAccounts;
	public static List<Contact> factoryContacts;
	public static User currentUser;
	public static User user;
	public static RecordType amdCPDRecordType;
	public static OrderApi__Subscription_Plan__c subPlan;
	public static OrderApi__Subscription__c contactSubscription;

	public static void dataInitialize(Integer numberOfAccounts, Integer numberOfContacts){
		CPAO_TestFactory.automationControlSetup();
		factoryAccounts = CPAO_TestFactory.createAccounts(numberOfAccounts);
		insert factoryAccounts;
		factoryContacts = CPAO_TestFactory.createContacts(factoryAccounts[0],numberOfContacts);
		factoryContacts[0].MailingCountry = 'Canada';
		factoryContacts[0].MailingState = 'Ontario';
		factoryContacts[0].CPAO_CPA_Contact_Type__c = 'Member';
		insert factoryContacts;
		
		user = CPAO_TestFactory.createCommunityUser(factoryContacts[0].Id);
        insert user;

        currentUser = CPAO_UserSelector.getCurrentUser();
        amdCPDRecordType = [SELECT Id FROM RecordType 
							WHERE Name = :CPAO_ApplicationHandler.AMD_CPD 
							AND SobjectType = 'CPAO_Application__c'][0];
		insert (CPAO_TestFactory.createInfractionTypes(1, 'Continuing Professional Development (CPD)'));
    }

    public static void fontevaDataInitialize() {
    	subPlan = CPAO_FontevaTestFactory.createSubPlan();
    	subPlan.Name = 'AMD';
    	insert subPlan;
    	contactSubscription = new OrderApi__Subscription__c(
    		OrderApi__Subscription_Plan__c = subPlan.Id,
    		OrderApi__Contact__c = factoryContacts[0].Id,
    		CPAO_Effective_Date__c = Date.today(),
    		CPAO_Payment_Deferral_Due_Date__c = Date.today());
    	insert contactSubscription;
    	
    }

    @isTest
	public static void basicTest() {
		dataInitialize(1,1);
		fontevaDataInitialize();
		Test.startTest();
		CPAO_CpdDeferredInfracAssigntBatch c = new CPAO_CpdDeferredInfracAssigntBatch();
		database.executebatch(c);
		Test.stopTest();
	}
	
}