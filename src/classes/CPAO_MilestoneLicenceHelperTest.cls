/**
* @File Name    :   CPAO_MilestoneLicenceHelperTest
* @Description  :   Trigger Milestone Licence Test
* @Date Created :   2018-07-12
* @Author       :   Michaell Reis Gasparini, Deloitte, mreisg@deloitte.com
* @group        :   Class
* @Modification Log:
**************************************************************************************
* Ver       Date        Author              Modification
* 1.0       2018-07-12  Michaell Reis       Created the file/class
**/
@isTest
private class CPAO_MilestoneLicenceHelperTest {
	@testSetup
	static void dataInit(){

		CPAO_History_Tracker_Controller__c historytrackerController = new CPAO_History_Tracker_Controller__c();
		historytrackerController.CPAO_Fields_to_Copy__c ='CPAO_Licence_Status_Effective_Date__c,CPAO_Licence_Type__c';
		historytrackerController.CPAO_Trigger_Fields__c ='CPAO_Licence_Status_Effective_Date__c,CPAO_Licence_Type__c';
		historytrackerController.CPAO_ObjectType__c ='Licence';
		historytrackerController.CPAO_Active__c=true;

		Database.insert(historytrackerController);
		

		CPAO_TestFactory.automationControlSetup();
		
		List<Account> accounts = CPAO_TestFactory.createAccounts(1);
		accounts[0].CPAO_Account_Status__c = 'Active';
		accounts[0].CPAO_Account_Substatus__c = 'Registered';
		
		Database.insert(accounts);

		List<Contact> contacts = CPAO_TestFactory.createContacts(accounts[0],1);
		contacts[0].CPAO_CPA_Status__c  = 'Active';
		Database.insert(contacts);

		List<CPAO_Application__c> applications =CPAO_TestFactory.createPALApplication(1,contacts[0]);

		Database.insert(applications);

		List<CPAO_Licence__c> licences= CPAO_TestFactory.createActiveLicences(1,contacts[0],applications[0]);

		licences[0].CPAO_Licence_Status_Effective_Date__c =Date.today()-30;
		licences[0].CPAO_Licence_Type__c='Public Accounting Licence';	

		Database.insert(licences);

	}

	@isTest static void testChangeFields() {
		
		id recordTypeId = Schema.SObjectType.CPAO_MilestoneAndHistory__c.getRecordTypeInfosByName().get('License Status').getRecordTypeId();
		
		CPAO_Licence__c licence =[Select id, CPAO_Licence_Status_Effective_Date__c,CPAO_Licence_Type__c FROM CPAO_Licence__c LIMIT 1];
		Date oldDate =licence.CPAO_Licence_Status_Effective_Date__c;
		String oldStatus =(String)licence.CPAO_Licence_Type__c;
		licence.CPAO_Licence_Type__c='Certificate Of Authorization';
		
		Database.update(licence);
		

		CPAO_MilestoneAndHistory__c history =[Select id, CPAO_Licence__c,CPAO_Licence_Status_Effective_Date__c,CPAO_Licence_Type__c, RecordTypeId FROM CPAO_MilestoneAndHistory__c LIMIT 1]; 
		
		System.assertEquals(licence.Id,history.CPAO_Licence__c);
		System.assertEquals(recordTypeId ,history.RecordTypeId);
		System.assertEquals(oldDate,history.CPAO_Licence_Status_Effective_Date__c);
		System.assertEquals(oldStatus,history.CPAO_Licence_Type__c );
		
	}



}