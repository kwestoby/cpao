({
	refreshContacts : function(cmp, ev) {
    
    var new_wrapper= ev.getParam('Final_wrappers');  
    
    var oldWrapper = cmp.get('v.childAttribute'); 
    if(oldWrapper != null){
		for (var idx=0; idx<new_wrapper.length; idx++) {
            if(new_wrapper[idx].selected)
            	oldWrapper.push(new_wrapper[idx]);
        }
        
        cmp.set('v.childAttribute', oldWrapper);     

        }

    },
    
    /*  Button to remove the record
    RemoveFromtheContactList: function(cmp, event) {
        
        var oldWrapper = cmp.get('v.wrappers');
        alert('oldwrapper890'+JSON.stringify(oldWrapper));  

        for (var idx=0; idx<oldWrapper.length; idx++) {
            if (oldWrapper[idx].selected) {
	            oldWrapper.remove((oldWrapper[idx]));
            }
        }
        alert('RemoveOldWrapper'+JSON.stringify(oldWrapper));  
        cmp.set('v.wrappers',oldWrapper);
        
     },
     */
    
     /* Automatically remove 
     RemoveFromtheContactLists: function(cmp, event) {
        
        var childCheckboxes = cmp.find("DependentCheckbox");
        alert('childCheckboxes1234'+JSON.stringify(childCheckboxes));

        var oldWrapper = cmp.get('v.wrappers');
		alert('oldwrapper'+JSON.stringify(oldWrapper));  
         
        if (!Array.isArray(childCheckboxes)) {
   			childCheckboxes = [childCheckboxes];
  		}
  		for (var i = 0; i < childCheckboxes.length; i++) {
           if(childCheckboxes[i].get("v.value") == true){ 
          	    alert('childCheckboxes999'+JSON.stringify(childCheckboxes[i].get("v.value")));
				oldWrapper.remove((oldWrapper[i]));
            }
            
        }
        alert('RemoveOldWrapper'+JSON.stringify(oldWrapper));  
        cmp.set('v.wrappers',oldWrapper);
     },
     */
     checkAllCheckboxes: function(cmp,event){
    	var masterCheckboxes = cmp.find("masterCheckbox");
        var childCheckboxes = cmp.find("DependentCheckbox");
        var val = masterCheckboxes.get("v.value");

  		if (!Array.isArray(childCheckboxes)) {
   			childCheckboxes = [childCheckboxes];
  		}

  		for (var i = 0; i < childCheckboxes.length; i++) {
    		childCheckboxes[i].set("v.value", val);
  		}
       
	 },
    
     saveMarkedContactsaAndOpenPayment : function(cmp, ev) {
     	cmp.set('v.paymentCheckVar',true);
        var oldWrapper = cmp.get('v.wrappers');
        var ids=new Array();
        var action = cmp.get("c.saveMarkedContacts");

        for (var idx=0; idx<oldWrapper.length; idx++) {
            if (oldWrapper[idx].selected) {
				ids.push(oldWrapper[idx].con.Id);
            	cmp.set('v.wrappers',oldWrapper[idx]);
            }
        }
         
        var idListJSON=JSON.stringify(ids);
        action.setParams({
                   "idListJSONStr": idListJSON
        });
 
        action.setCallback(this, function(response) {
            var state = response.getState();

            if (state === "SUCCESS") {
				var cons=response.getReturnValue()
                cmp.set('v.contacts', cons);
            }
            else if (state === "ERROR") {
		        var errors = response.getError();
                alert('Error : ' + JSON.stringify(errors));
            }
        });
        $A.enqueueAction(action);
     }
    
})