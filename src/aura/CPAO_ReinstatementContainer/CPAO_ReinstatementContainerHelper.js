({
	showSpinner: function (component, event, helper) {
        var spinner = component.find("mySpinner");
        $A.util.removeClass(spinner, "slds-hide");
    },
    
    hideSpinner: function (component, event, helper) {
        var spinner = component.find("mySpinner");
        $A.util.addClass(spinner, "slds-hide");
    },
    methodFailure : function(component, event, helper, body, header) {
		var applicationHasChanged = component.getEvent("applicationHasChanged");
        applicationHasChanged.setParams({"nextButtonClickeable" : false, 
    									"modalBody" : body,
    									"modalHeader" : header} );
        applicationHasChanged.fire();
	}
})