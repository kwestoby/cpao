public  class CPAO_FileController {
	   @AuraEnabled

    public static Id saveTheFile(Id parentId, String fileName, String base64Data, String contentType,String fileTitle) { 
        base64Data = EncodingUtil.urlDecode(base64Data, 'UTF-8');
        
        ContentVersion a = new ContentVersion();
        a.FirstPublishLocationId = parentId;

        a.versionData = EncodingUtil.base64Decode(base64Data);
        a.title = fileName;

        a.pathOnClient = '/'+fileName;
        //  a.ContentType = contentType;
        
        insert a;
        
        return a.Id;
    }

      @AuraEnabled
    public static CPAO_Application__c  upsertPalApplication(CPAO_Application__c PALappToAdd) {

		return CPAO_PAL_Application_CTRL.upsertPalApplication( PALappToAdd);
	}
}