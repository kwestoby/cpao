public with sharing class CPAO_ApplicationServiceUtils {
	public static final String APPLICATION_HAS_INFRACTIONS = 'This Application has open Infractions that need to be closed before proceeding';
	public static final String APPLICATION_ALREADY_IN_PROGRESS = 'Application is already in progress.';
	public static final String APPLICATION_DUPLICATE = 'Duplicate application detected.';
	public static final String APPLICATION_DUPLICATE_PORTAL = 'Duplicate application detected, please contact CPA Ontario\'s Customer Service team for more information.';


	public static void sendToRegistrarQueue(List<CPAO_Application__c> applications) {
		List<Group> registrarQueue = CPAO_GroupSelector.getRegistrarQueue();
		if (registrarQueue.size() != 1) {
			return;
		}

		for (CPAO_Application__c application : applications) {
			application.OwnerId = registrarQueue[0].Id;
		}
	}

	public static void sendToRO_rQueue(List<CPAO_Application__c> applications) {
		List<Group> roQueue = CPAO_GroupSelector.getROStaffQueue();
		if (roQueue.size() != 1) {
			return;
		}

		for (CPAO_Application__c application : applications) {
			application.OwnerId = roQueue[0].Id;
		}
	}



	public static List<CPAO_Application__c> findOpenInfractions(List<CPAO_Application__c> applications) {
		Map<Id, CPAO_Application__c> contactIdsToApplications = new Map<Id, CPAO_Application__c> ();
		List<CPAO_Application__c> noInfractionApplications = new List<CPAO_Application__c> ();
		for (CPAO_Application__c application : applications) {
			contactIdsToApplications.put(application.CPAO_Contact__c, application);
		}
		Map<Id, CPAO_Infraction__c> contactIdToInfractionMap = new Map<Id, CPAO_Infraction__c> ();
		for (CPAO_Infraction__c openInfraction : CPAO_InfractionSelector.getContactOpenInfractions(contactIdsToApplications.keySet())) {
			contactIdToInfractionMap.put(openInfraction.CPAO_Contact__c, openInfraction);
		}

		for (CPAO_Application__c application : applications) {
			if (contactIdToInfractionMap.containsKey(application.CPAO_Contact__c)) {
				application.addError(APPLICATION_HAS_INFRACTIONS);
			} else {
				noInfractionApplications.add(application);
			}
		}


		return noInfractionApplications;
	}

	/**
	 * @description Check for existing application for contacts to find and add errors to duplicate.
	 * @param applications List<CPAO_Application__c> List of applications to check duplicated for.
	 **/
	public static void checkDuplicateApplication(List<CPAO_Application__c> applications) {
		Set<Id> contactIDs = new Set<ID> ();
		for (CPAO_Application__c app : applications) {
			if (!app.CPAO_Override_Duplicate__c && app.CPAO_Contact__c != null) {
				contactIDs.add(app.CPAO_Contact__c);
			}
		}
		if (contactIDs.size() <= 0) {
			return;
		}
		List<CPAO_Application__c> existingApps = [SELECT Id, CPAO_Contact__c, CPAO_Application_Type__c, CPAO_Status__c FROM CPAO_Application__c
		                                          WHERE CPAO_Contact__c IN :contactIDs
		                                          AND Id NOT IN :applications
		                                          AND CPAO_Status__c IN('Submitted', 'In Review', 'Completed')];
		Map<String, CPAO_Application__c> existingAppMap = new Map<String, CPAO_Application__c> ();
		for (CPAO_Application__c app : existingApps) {
			if (app.CPAO_Contact__c != null) {
				String concatAppID = app.CPAO_Contact__c + ':' + app.CPAO_Application_Type__c;
				existingAppMap.put(concatAppID, app);
			}
		}
		for (CPAO_Application__c app : applications) {
			String concatAppID = app.CPAO_Contact__c + ':' + app.CPAO_Application_Type__c;
			if (!app.CPAO_Override_Duplicate__c && existingAppMap.containsKey(concatAppID)) {
				String errorMsg = '';
				if (existingAppMap.get(concatAppID).CPAO_Status__c == 'Completed') {
					if (CPAO_UserSelector.getCurrentUser().ContactId != null) {
						errorMsg += APPLICATION_DUPLICATE_PORTAL;
					} else {
						errorMsg += APPLICATION_DUPLICATE;
					}
				} else {
					errorMsg += APPLICATION_ALREADY_IN_PROGRESS;
				}
				app.addError(errorMsg);
			}
		}
	}
    
  
}