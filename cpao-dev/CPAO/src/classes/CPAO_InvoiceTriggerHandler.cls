public class CPAO_InvoiceTriggerHandler {
	public static void afterUpdateFilter(List<OrderApi__Invoice__c> newInvoices, Map<Id, OrderApi__Invoice__c> oldMap) {
		List<OrderApi__Invoice__c> paidInvoices = new List<OrderApi__Invoice__c> ();
		for (OrderApi__Invoice__c invoice : newInvoices) {
			OrderApi__Invoice__c oldInvoice = oldMap.get(invoice.Id);
			if (invoice.OrderApi__Status__c=='Paid' && invoice.OrderApi__Status__c != oldInvoice.OrderApi__Status__c) {
				paidInvoices.add(invoice);
			}
		}
		if (paidInvoices.size() > 0) {
			CPAO_InvoiceService.handleSubscriptionRenewal(paidInvoices);
		}
	}
}