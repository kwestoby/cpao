public without sharing class CPAO_TaskHandler {
//without sharing - means that the code runs in admin mode rather than with the limitations of the logged in user
    
    private static final String FINANCE_QUEUE = 'CPAO_Finance_Queue';
    private static final String QUEUE = 'Queue';
    private static final String INVOICE_REQUEST = 'Other';
    private static final String TASK = 'Task';
    private static final String PAYMENT_STATUS_REQUEST = 'Invoice Requested';
    private static final String PAYMENT_STATUS_CREATED = 'Invoice Created';
    private static final String TASK_COMPLETED = 'Completed';
    
    public static void afterInsertFilter(List<Task> allTasks){
        List<Task> invoiceRequests = new List<Task>();
        
        for(Task task: allTasks){
            
            if(task.Type == INVOICE_REQUEST){
                invoiceRequests.add(task);
            }
        }
        
        if(!invoiceRequests.isEmpty()){
            addToFinanceQueue(invoiceRequests);
        }
        
    }
    
    public static void afterUpdateFilter(List<Task> allTasks, Map<Id, Task> oldMap){
        
        Id readmissionRecordTypeId = CPAO_ApplicationHandler.getApplicationRecordTypeId(CPAO_ApplicationHandler.READMISSION_RECORDTYPENAME);
        List<CPAO_Application__c> applicationsToMark = [select Id, CPAO_Payment_Status__c from CPAO_Application__c where RecordTypeId=:readmissionRecordTypeId];
        Map<Id,CPAO_Application__c> applicationMap = new Map<Id,CPAO_Application__c>();
        
        for(CPAO_Application__c app: applicationsToMark){
            applicationMap.put(app.Id, app);
        }
        
        for(Task task:allTasks){
            if(task.Status != oldMap.get(task.Id).Status){
                if(task.Status == TASK_COMPLETED){
                    if(applicationMap.containsKey(task.WhatId)){
                        CPAO_Application__c temp = applicationMap.get(task.WhatId);
                        temp.CPAO_Payment_Status__c = PAYMENT_STATUS_CREATED;
                    }
                }
            }
        }
        
        update applicationMap.values();
        
    }
    
    public static void addToFinanceQueue(List<Task> financeTasks){
        List<Group> financeQueue = [select Id from Group where Type =: QUEUE AND DeveloperNAME =: FINANCE_QUEUE limit 1];
        List<GroupMember> financeMembers = [select Id, UserOrGroupId from GroupMember where GroupId =: financeQueue[0].Id];
        
        Id readmissionRecordTypeId = CPAO_ApplicationHandler.getApplicationRecordTypeId(CPAO_ApplicationHandler.READMISSION_RECORDTYPENAME);
        List<CPAO_Application__c> applicationsToMark = [select Id, CPAO_Payment_Status__c from CPAO_Application__c where RecordTypeId=:readmissionRecordTypeId];
        Map<Id,CPAO_Application__c> applicationMap = new Map<Id,CPAO_Application__c>();
        
        for(CPAO_Application__c app: applicationsToMark){
            applicationMap.put(app.Id, app);
        }
        
        for(Task task: financeTasks){
            Integer rand = Math.round(Math.random()*(financeMembers.size()-1));
            task.OwnerId = financeMembers[rand].UserOrGroupId;
            
            if (applicationMap.containsKey(task.WhatId)){
                CPAO_Application__c temp = applicationMap.get(task.WhatId);
            	temp.CPAO_Payment_Status__c = PAYMENT_STATUS_REQUEST;
            }
            
        }
        
        update applicationMap.values();
    }
    
    

}