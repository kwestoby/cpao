({
    becomeLicensedPA_Redirect : function(component, event, helper) {
        var url_to_redirect='https://www.cpaontario.ca/cpa-members/obligations/public-accounting-licence';
        window.open(url_to_redirect,'_blank');
    },
    
    byLaws_Redirect : function(component, event, helper) {
        var url_to_redirect='https://www.cpaontario.ca/stewardship-of-the-profession/governance/act-bylaws-and-regulations';
        window.open(url_to_redirect,'_blank');
    },
    
    FAQ_Redirect : function(component, event, helper) {
        var url_to_redirect='https://www.cpaontario.ca/cpa-members/obligations/public-accounting-licence/pal-faqs';
        //window.location.href=url_to_redirect;
        window.open(url_to_redirect,'_blank');
    },
})