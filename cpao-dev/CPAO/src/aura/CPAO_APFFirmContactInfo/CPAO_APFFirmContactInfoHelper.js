({
    updateAPF : function(component, helper, isQuit) {
        var action = component.get("c.updateAccount");
        var account = component.get("v.account");
        var accountJSON = JSON.stringify(account);

        action.setParams({
            accountJSON : accountJSON
        });
        action.setCallback(this, function(response){
            var state = response.getState();
            var result = response.getReturnValue();
            var err = response.getError();
            if(state === "SUCCESS") {
                component.set("v.account", result);
                component.set("v.isEdit", false);
                
                helper.handleNext(component, helper, isQuit);
            }
        });
        $A.enqueueAction(action);
    },

    handleLookupChange : function(component, helper, attr) {
        var struct = helper.getLookupStructure();
        var lookup = struct[attr];

        var account = component.get("v.account");
        account[lookup.field] = component.get("v."+lookup.attr+".val");
        component.set("v.account", account); 
    },

    handleNext: function(component, helper, isQuit) {
        if(isQuit) {
            helper.fireNextEvent(component, "Quit");
        } else {
            helper.fireNextEvent(component, "Next");
        }
    },

    fireNextEvent : function(component, nextStep) {
        var nextButtonClick = $A.get("e.c:CPAO_Next_Button_Has_Been_Pressed");
        nextButtonClick.setParams({
            "componentName" : "CPAO_AnnualPractitionerFee",
            "nextStepName" : nextStep});
        nextButtonClick.fire();
    },

    firePrevEvent : function(component) {
        var prevButtonClick = $A.get("e.c:CPAO_Prev_Button_Has_Been_Pressed");
        prevButtonClick.setParams({
            "componentName" : "CPAO_AnnualPractitionerFee",
            "nextStepName" : "Prev"});
        prevButtonClick.fire();
    },

    getLookupStructure: function() {
        var struct = {
            localOfficer         : { name: 'CPAO_LSO__r.Name',                            field: 'CPAO_LSO__c',                            attr: 'localOfficer' },
            inspectionClient     : { name: 'CPAO_Practice_Inspection_Contact__r.Name',    field: 'CPAO_Practice_Inspection_Contact__c',    attr: 'inspectionClient' },
            disciplineContact    : { name: 'CPAO_Discipline_Contact__r.Name',             field: 'CPAO_Discipline_Contact__c',             attr: 'disciplineContact' },
            disciplineContactAlt : { name: 'CPAO_Discipline_Contact_Alternative__r.Name', field: 'CPAO_Discipline_Contact_Alternative__c', attr: 'disciplineContactAlt' },
        };
        return struct;
    },
})