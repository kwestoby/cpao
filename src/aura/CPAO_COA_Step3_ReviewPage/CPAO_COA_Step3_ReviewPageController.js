({
    doAction : function(component, event, helper) {
        
        var apexAction = component.get('c.loadReviewInfo');
        apexAction.setCallback(this, function(response) {
            var state = response.getState();
            console.log('state');
            console.log(state);
            if(state === "SUCCESS"){

                var result = response.getReturnValue();
                if(result!=null){
                    component.set("v.accountInfo", result.professionalCoorporation);
                    component.set("v.currentCOA", result.currentCOAApplication);
                    var accountInfo=component.get("v.accountInfo");
                    if(accountInfo.BillingStreet!=null && accountInfo.BillingCity!=null && accountInfo.BillingState!=null && accountInfo.BillingCountry!=null
                       && accountInfo.BillingPostalCode!=null ){
                        component.set('v.haveBusinessAdress',true);
                    }
                    
                    if(accountInfo.ShippingStreet!=null && accountInfo.ShippingCity!=null && accountInfo.ShippingState!=null && accountInfo.ShippingCountry!=null
                       && accountInfo.ShippingPostalCode!=null ){
                        component.set('v.haveOtherAdress',true);
                    }
                }
            }
        });
        $A.enqueueAction(apexAction);
    },
    clickOnNextButton : function(component,event,helper){
        
        var componentNameInEvent=event.getParam("componentName");
        
        
        if(componentNameInEvent==component.get('v.nameOfThisComponent')){
            var validForm = component.find('COAForm').reduce(function (validSoFar, inputCmp) {
                
                inputCmp.showHelpMessageIfInvalid();
                return validSoFar && inputCmp.get('v.validity').valid ;
            }, true);
            
            if(validForm){
                component.set('v.currentCOA.CPAO_Status__c','Submitted');
                component.set('v.currentCOA.CPAO_Submission_Date__c',new Date());
                var showSpinnerEvent = component.getEvent("showSpinner");
                showSpinnerEvent.fire();
                helper.upsertCOA(component);
            }
            
        }
    }
})