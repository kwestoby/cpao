public with sharing class CPAO_BadgeSelector {

	public static List<OrderApi__Badge_Type__c> getBadgeType(String badgeTypeName) {
		return[SELECT Id, Name FROM OrderApi__Badge_Type__c WHERE Name = : badgeTypeName];
	}

	public static List<OrderApi__Badge__c> getBadges(Set<Id> contactIds) {
		return[SELECT Id, OrderApi__Badge_Type__c, OrderApi__Contact__c FROM OrderApi__Badge__c WHERE OrderApi__Contact__c IN : contactIds];
	}

	public static List<OrderApi__Badge__c> getBadges(Set<Id> contactIds, Id badgeTypeId) {
		return[SELECT Id, OrderApi__Contact__c, OrderApi__Badge_Type__c FROM OrderApi__Badge__c
		WHERE OrderApi__Contact__c IN : contactIds AND OrderApi__Badge_Type__c = : badgeTypeId];
	}

	public static List<OrderApi__Badge__c> getActiveBadges(Set<Id> contactIds) {
		return[SELECT Id, OrderApi__Badge_Type__c, OrderApi__Badge_Type__r.Name, OrderApi__Contact__c FROM OrderApi__Badge__c WHERE OrderApi__Contact__c IN : contactIds AND OrderApi__Is_Active__c = true];
	}

}