({
	clickOnNextButton : function(component, event, helper) {
       var talkingToComponent = event.getParam("componentName");
        if(talkingToComponent == 'step1'){
            
            helper.methodFailure(component, event, helper,"Please resolve the following outstanding infractions in order to continue with your Reinstatement Application","Infractions");
            // use step 1 to differentiate Modal Close
            
            //if press ok on modal, then it goes to the next page.
        }
	}
})