/**
 * @author Sachet Khaanl (sakhanal@deloitt.com)
 * @version 1.0.0
 * @description This class is used for querying Instruction records.
 *
 *
 **/ 
public without sharing class CPAO_InstructionSelector {
	public static final String MRA_APP = 'MRA Application';
	public static final String PT_APP = 'PT Application';
	public static final String PAL_APP = 'PAL Application';
	public static final String OFFLINE_PAYMENT = 'Offline Payment';

	public static CPAO_Instruction__c getInstructions(String applicableTo) {
		return[SELECT Id, CPAO_Detail__c FROM CPAO_Instruction__c WHERE CPAO_Applicable_To__c = : applicableTo LIMIT 1];
	}
}