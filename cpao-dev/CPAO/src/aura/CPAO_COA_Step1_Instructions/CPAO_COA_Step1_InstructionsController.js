({
    
    COA_eligibility_Redirect : function(component, event, helper) {
        url_to_redirect='https://www.cpaontario.ca/cpa-members/public-practice/setting-up-a-practice/professional-corporations';
        window.open(url_to_redirect,'_blank');
    },
    
    byLaws_Redirect : function(component, event, helper) {
        url_to_redirect='https://www.cpaontario.ca/stewardship-of-the-profession/governance/act-bylaws-and-regulations';
        window.open(url_to_redirect,'_blank');
    },
    
    FAQ_Redirect : function(component, event, helper) {
        url_to_redirect='https://www.cpaontario.ca/cpa-members/public-practice/setting-up-a-practice/professional-corporations/faqs-about-professional-corporations';
        //window.location.href=url_to_redirect;
        window.open(url_to_redirect,'_blank');
    },
    
    clickOnNextButton : function(component,event,helper){
        
        var componentNameInEvent=event.getParam("componentName");
        if(componentNameInEvent==component.get('v.nameOfThisComponent')){
            if(event.getParam("nextStepName")=='Exit'){ 
                component.set('v.shouldSaveAndQuit',true);
            }
            var lso_account_req1=component.find('lso_account_req1');
            var firmName=component.get('v.currentCOA.CPAO_COA_Firm_Name__c');
            if(firmName==undefined ||firmName=='--Please select an accounting body--'){
                $A.util.addClass(lso_account_req1, 'slds-show');
            }else{
                $A.util.removeClass(lso_account_req1, 'slds-show');
                var showSpinnerEvent = component.getEvent("showSpinner");
                showSpinnerEvent.fire();
                helper.upsertCOA(component);
                
            }         
            
        }
        
    }
    
})