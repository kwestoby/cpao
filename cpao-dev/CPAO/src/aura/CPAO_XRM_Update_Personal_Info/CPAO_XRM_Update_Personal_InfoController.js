({
	doInit : function(component, event, helper) {
		var apexAction = component.get('c.loadReviewInfo');
        apexAction.setCallback(this, function(response) {
            var state = response.getState();
            if(state === "SUCCESS"){
                var result = response.getReturnValue();
                component.set("v.userContact", result.userContact);
            }
        });
        $A.enqueueAction(apexAction);
	},
    clickOnNextButton : function(component,event,helper){
        var talkingToComponent = event.getParam("componentName");
        if(talkingToComponent == 'step2'){
            component.set("v.nextStepName", event.getParam("nextStepName"));
			return;
        }        
    },
	handleSaveSuccess : function(component, event, helper) {
		console.log("handleSaveSuccess()");
		var actionToTake = component.get("v.nextStepName");
		console.log(actionToTake);
		var applicationHasChanged = component.getEvent("applicationHasChanged");
		applicationHasChanged.setParams({
			"nextButtonClickeable" : true,
			"action" : actionToTake
		});
		applicationHasChanged.fire();
	}
})