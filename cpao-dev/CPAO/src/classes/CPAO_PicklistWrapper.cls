public class CPAO_PicklistWrapper {
	@AuraEnabled public String label;
    @AuraEnabled public String value;

    public CPAO_PicklistWrapper(String value, String label) {
        this.value = value;
        this.label = label;
    }

    public CPAO_PicklistWrapper(Schema.PicklistEntry entry) {
        this.value = entry.getValue();
        this.label = entry.getLabel(); 
    }
}