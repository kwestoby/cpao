({
	 upsertPAL : function(component) {
        console.log( 'returned application: '+JSON.stringify(component.get('v.currentPAL')));
        var action = component.get("c.upsertPalApplication");		
        action.setParams({ "PALRenewalToAdd" :component.get('v.currentPAL') });
        action.setCallback(this, function(response){
            console.log('callback');
            var state = response.getState();
            if (state === "SUCCESS") {
                console.log('PAL added');
                if(component.get('v.shouldSaveAndQuit')){
                    window.location.href='/CPAO_ApplicationDashboard';
                    
                }else{
                    component.set('v.currentPAL',response.getReturnValue());
                    var applicationHasChanged = component.getEvent("applicationHasChanged");
                    applicationHasChanged.setParams({
                        "currentApplication": component.get("v.currentPAL"),
                        "nextButtonClickeable": "true"
                    });
                    applicationHasChanged.fire();
                }
                
            }else{
                console.log('error while adding Pal renewal');
                 var errors=response.getError();
                var message = errors[0];
                
                console.log(message);
                
            }
        });
        
        $A.enqueueAction(action);
        
    },
    upload: function(component, file, fileContents) {
        /* var validForm = component.find('uploadField').reduce(function (validSoFar, inputCmp) {
        inputCmp.showHelpMessageIfInvalid();
        return validSoFar && inputCmp.get('v.validity').valid }, true);

      if(validForm){*/
         var uploadName=component.get('v.currentPAL.CPAO_Special_Consideration_File_Name__c');
         var oldUploadId=component.get('v.currentPAL.CPAO_Special_Consideration_File_Id__c');
         var oldFileId;
         var newFileId;
         var currentUpload=component.get('v.SpecialConsiderationName');
         
         
         
         if(component.get('v.fileInput')==null || component.get('v.currentPAL.CPAO_Prof_Experience_Spec_Consideration__c')!=component.get('v.doNotConsider')){
             //alert('nothing new to upload,, keep old file');
             if(oldUploadId!=null){
                this.removeSpecialConsiderationFile(component);

             }else{
                component.set('v.currentPAL.CPAO_Special_Consideration_File_Name__c',null);
                this.upsertPAL(component);

             }
            
         }else{
             var idFileToReplace=null;
             if(oldUploadId!=null){
                idFileToReplace=oldUploadId;
             }

            var action = component.get("c.saveTheFile"); 
                 action.setParams({
                     parentId: component.get("v.currentPAL.Id"),
                     fileName: component.get("v.fileName"),
                     base64Data: encodeURIComponent(fileContents), 
                     contentType: component.get("v.fileType"),
                     fileTitle: component.get('v.fileTitle'),
                     idFileToReplace:idFileToReplace
            });
             //alert('notnull, thats a new file');
             /*if(oldUploadId!=null){
                 var action = component.get("c.saveTheFile"); 
                 action.setParams({
                     parentId: component.get("v.currentPAL.Id"),
                     fileName: component.get("v.fileName"),
                     base64Data: encodeURIComponent(fileContents), 
                     contentType: component.get("v.fileType"),
                     fileTitle: component.get('v.fileTitle'),
                     idFileToReplace:oldUploadId
                 });
             }else{
                 var action = component.get("c.saveTheFile"); 
                 action.setParams({
                     parentId: component.get("v.currentPAL.Id"),
                     fileName: component.get("v.fileName"),
                     base64Data: encodeURIComponent(fileContents), 
                     contentType: component.get("v.fileType"),
                     fileTitle: component.get('v.fileTitle'),
                     idFileToReplace:null
                 });
             }*/
             
             
             action.setCallback(this, function(response) {
                 var state = response.getState();
                 if (state === "SUCCESS") {
                     var attachId = response.getReturnValue().fileId;
                     component.set('v.currentPAL.CPAO_Special_Consideration_File_Id__c',attachId);
                     var fileName=response.getReturnValue().fileName;
                     component.set('v.currentPAL.CPAO_Special_Consideration_File_Name__c',fileName);
                     //  alert(componen);
                     
                     this.upsertPAL(component);
                     /*var returnUploadIdEvent = component.getEvent("returnUploadId");
                    returnUploadIdEvent.setParams({
                      "IdOfUploadedFile":response.getReturnValue()
                    });
                   returnUploadIdEvent.fire();
                   alert('fired');*/
                      
                  }else{
                      console.log('error while adding attachment'+ component.get("v.fileName") +  component.get("v.fileTitle"));
                      var errors=response.getError();
                      var message = errors[0];
                      
                      console.log(message);
                      
                  }
                
            });
           
           
           $A.enqueueAction(action); 
       }
         //  }
     },
      removeSpecialConsiderationFile:function(component){
        // alert('acmethod');
        var oldUploadId=component.get('v.currentPAL.CPAO_Special_Consideration_File_Id__c');
        var listOfIds=[];
        listOfIds.push(oldUploadId);
        
        var serverAction = component.get("c.deletePALApplicationFiles");   
        serverAction.setParams({ "filesIdToDelete" : listOfIds});
        serverAction.setCallback(this, function(response){
            console.log('callback');
            var state = response.getState();
            // alert(state);
            if (state === "SUCCESS") {
                component.set('v.currentPAL.CPAO_Special_Consideration_File_Name__c',null);
                component.set('v.currentPAL.CPAO_Special_Consideration_File_Id__c',null);
        
                this.upsertPAL(component);
            }else{
                console.log('error while deleting special Consideration file');
                
            }
        });
        
        $A.enqueueAction(serverAction);
    }
})