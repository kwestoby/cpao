global class CPAO_CpdInfractionAssignmentBatch implements Database.Batchable<sObject> {
	
	//querying for contact records who do not have deferrals for the amd subscription renewal. Completion is recorded on the application record
	global Database.QueryLocator start(Database.BatchableContext BC) {
		Integer currentYear = date.today().year();
		String amdCpdRecordTypeName = CPAO_ApplicationHandler.AMD_CPD;
		String query = 'SELECT Id FROM Contact WHERE CPAO_CPA_Contact_Type__c = \'Member\' AND CPAO_CPD_Continuous_Exemption__c = null ';
        query = query + 'AND  Id NOT IN (SELECT CPAO_Contact__c FROM CPAO_Application__c WHERE RecordType.Name = :amdCpdRecordTypeName AND ';
        query = query + 'CPAO_Status__c = \'Completed\' AND CALENDAR_YEAR(CreatedDate) = :currentYear) AND Id IN (SELECT OrderApi__Contact__c ';
        query = query + 'FROM OrderApi__Subscription__c WHERE OrderApi__Subscription_Plan__r.Name = \'AMD\' AND CPAO_Payment_Deferral_Due_Date__c ';
        query = query + '= null)';
        return Database.getQueryLocator(query);
	}

   	global void execute(Database.BatchableContext BC, List<Contact> contacts) {
		if(contacts.isEmpty()){
			return;
		}
		List<CPAO_Infraction_Type__c> cpdInfractionType = [SELECT id, Name FROM CPAO_Infraction_Type__c WHERE Name = 'Continuing Professional Development (CPD)'];
		if(cpdInfractionType.size() != 1){
    		return;
    	}
    	List<CPAO_Infraction__c> infractionsToCreate = new List<CPAO_Infraction__c>();
    	for(Contact contact:contacts){
    		infractionsToCreate.add(new CPAO_Infraction__c(
    			CPAO_Infraction_Type__c = cpdInfractionType[0].Id,
                CPAO_Contact__c = contact.Id));
    	}
    	if(!infractionsToCreate.isEmpty()){
    		insert infractionsToCreate;
    	}
	}
	
	global void finish(Database.BatchableContext BC) {
		
	}
	
}