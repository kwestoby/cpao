@isTest
private class CPAO_WrapperContactTest {

	@isTest
	private static void testWrapperContact() {
		CPAO_WrapperContact wc = new CPAO_WrapperContact();
		wc.con = new Contact();
		wc.selected = true;
		wc.statusOfCompletion = 'Test status';
		wc.orderOfSubscription = new OrderApi__Subscription__c();
		System.assertEquals(wc.selected, true);
	}
}