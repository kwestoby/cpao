public class CPAO_NewApplicationCTRL {

    public final String PAL_APP = 'Public Accounting Licence Application';
    public final String MRA = 'New MRA Registration';
    public final String REINSTATEMENT = 'Reinstate Membership';
    public final String COA_APP = 'Certificate of Authorization Application';

    public String selectedAppType { get; set; }
    public List<SelectOption> availableApplicationTypes { get; set; }
    public User currentUser;
    public boolean displayPopup {get; set;}
    
    public void closePopup(){
        displayPopup = false;
    }
    
    public void openPopup(){
        displayPopup = true;
    }

    public void loadAppTypeOptions() {
        availableApplicationTypes = new List<SelectOption> ();
        List<OrderApi__Badge__c> badges = new List<OrderApi__Badge__c> ();
        if (currentUser.ContactId != null) {
            badges = CPAO_BadgeSelector.getActiveBadges(new Set<Id> { currentUser.ContactId });
        }
        availableApplicationTypes.add(new SelectOption(REINSTATEMENT, REINSTATEMENT));
        availableApplicationTypes.add(new SelectOption(MRA, MRA));
        for (OrderApi__Badge__c badge : badges) {
            if (badge.OrderApi__Badge_Type__r.Name == 'PAL') {
                availableApplicationTypes.add(new SelectOption(PAL_APP, PAL_APP));
            }
            if (badge.OrderApi__Badge_Type__r.Name == 'LSO') {
                availableApplicationTypes.add(new SelectOption(COA_APP, COA_APP));
            }
        }
        selectedAppType = availableApplicationTypes[0].getValue();
    }

    public PageReference cancel() {
        return Page.CPAO_ApplicationDashboard;
    }

    public PageReference startApplication() {
        CPAO_Application__c application;
        if (selectedAppType.equals(REINSTATEMENT)) {
            List<CPAO_Application__c> currentReinstatementAppliation = CPAO_ApplicationSelector.getUserApplicationsByRecordTypeNameAndStatus('Open', CPAO_ApplicationHandler.REINSTATEMENT);
            if (currentReinstatementAppliation.size() > 0) {
                application = currentReinstatementAppliation[0];
            } else {
                Id reinstatementrecordTypeId = Schema.SObjectType.CPAO_Application__c.getRecordTypeInfosByName().get(CPAO_ApplicationHandler.REINSTATEMENT).getRecordTypeId();
                application = new CPAO_Application__c(
                                                      CPAO_Status__c = 'Open',
                                                      CPAO_Contact__c = currentUser.ContactId,
                                                      RecordTypeId = reinstatementrecordTypeId
                );
            }
        } else if (selectedAppType.equals(MRA)) {
            List<CPAO_Application__c> currentMRAAppliation = CPAO_ApplicationSelector.getUserApplicationsByRecordTypeNameAndStatus('Open', CPAO_ApplicationHandler.MRA_APPLICATION);
            if (currentMRAAppliation.size() > 0) {
                application = currentMRAAppliation[0];
            } else {
                Id MRARecordTypeId = Schema.SObjectType.CPAO_Application__c.getRecordTypeInfosByName().get(CPAO_ApplicationHandler.MRA_APPLICATION).getRecordTypeId();
                application = new CPAO_Application__c(
                                                      CPAO_Status__c = 'Open',
                                                      CPAO_Contact__c = currentUser.ContactId,
                                                      RecordTypeId = MRARecordTypeId
                );
            }
        } else if (selectedAppType.equals(COA_APP)) {
            List<CPAO_Application__c> currentPalApps = CPAO_ApplicationSelector.getUserApplicationsByRecordTypeNameAndStatus('Open', CPAO_ApplicationHandler.COA_APPLICATION);
            if (currentPalApps.size() > 0) {
                application = currentPalApps[0];
            } else {
                Id COARecordTypeId = Schema.SObjectType.CPAO_Application__c.getRecordTypeInfosByName().get(CPAO_ApplicationHandler.COA_APPLICATION).getRecordTypeId();
                application = new CPAO_Application__c(
                                                      CPAO_Contact__c = currentUser.ContactId,
                                                      RecordTypeId = COARecordTypeId,
                                                      CPAO_Status__c = 'Open',
                                                      CPAO_Current_Step__c = 'step1'
                );
            }
        } else if (selectedAppType.equals(PAL_APP)) {
            List<CPAO_Application__c> currentPalApps = CPAO_ApplicationSelector.getUserApplicationsByRecordTypeNameAndStatus('Open', CPAO_ApplicationHandler.PAL_APPLICATION);
            if (currentPalApps.size() > 0) {
                application = currentPalApps[0];
            } else {
                Id PALRecordTypeId = Schema.SObjectType.CPAO_Application__c.getRecordTypeInfosByName().get(CPAO_ApplicationHandler.PAL_APPLICATION).getRecordTypeId();
                application = new CPAO_Application__c(
                                                      CPAO_Contact__c = currentUser.ContactId,
                                                      RecordTypeId = PALRecordTypeId,
                                                      CPAO_Status__c = 'Open',
                                                      CPAO_Current_Step__c = 'step1'
                );
            }
        }
        if (application != null) {
            try {
                upsert application;
                application = [SELECT Id, CPAO_Form_URL__c FROM CPAO_Application__c WHERE Id = :application.Id];
                return new PageReference(application.CPAO_Form_URL__c);
            } catch(DmlException ex) {
                System.debug(ex.getMessage());
            }
        }
        return null;
    }


    public CPAO_NewApplicationCTRL() {
        currentUser = CPAO_UserSelector.getCurrentUser();
        loadAppTypeOptions();
    }
}