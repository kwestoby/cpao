({
	// @Author : Halima Dosso
	// This function runs once the component CPAO_CurrentEmployer is fully loaded on the page 
	// It calls an apex method @componentInit from class CPAO_EmployerInfoController.
	// Based on the returned value, it sets two  attributes based on CPAO_Account_Contact_Relationship__c object:
	// 	- businessUnitOptions: the different values for the picklist field CPAO_Business_Unit__c.
	// 	- positionCategoryOptions: the different values for the picklist field CPAO_Position_Category__c
	// Those picklist values will be used in a form so the end user could choose from it 
	doInit : function(component, event, helper) {

		var initializeComponentAction = component.get("c.componentInit");
		initializeComponentAction.setCallback(this, function(response){
			var state = response.getState();
			if (state === "SUCCESS") {
				component.set("v.businessUnitOptions", JSON.parse(response.getReturnValue()).businessUnits);
				component.set("v.positionCategoryOptions", JSON.parse(response.getReturnValue()).positionCategories);
			}
		});
		$A.enqueueAction(initializeComponentAction);
	},
 
	// @Author : Halima Dosso
	// This function runs once the end user clicks on button "Update Employer" to update
	// a CPAO_Account_Contact_Relationship__c record  associated to his contact record.
	// Before performing the updates, the function verify the following:
	// 	- If the user is updating the only CPAO_Account_Contact_Relationship__c record associated 
	//	  to his contact record and the user is unchecking the primary employer checkbox,the system 
	//    shows an error message indicating that he has to set another primary employer before
	//    unchecking the current
	//  - Else if the user is updating a non primary employer to make it primary, this function calls a helper
	//	  method @updatePrimaryEmployer to update the record
	//	- Otherwise, if the user updates any field on a CPAO_Account_Contact_Relationship__c record excepted the primary
	//    employer checkbox,  this function calls a helper method @updateSecondaryEmployer to update the record
	updateEmployer : function(component, event, helper) {

		var primaryEmployerNewValue=component.find('primaryEmployerNew').get('v.checked');
		var primaryEmployerOldValue=component.get('v.accountContactRel.CPAO_Primary_Employer__c');
		var businessUnitValue =component.get("v.businessUnitSelectedValue");   
	    component.set("v.accountContactRel.CPAO_Business_Unit__c", businessUnitValue);
	    var positionCategoryValue=component.get("v.positionCategorySelectedValue");
	    component.set("v.accountContactRel.CPAO_Position_Category__c", positionCategoryValue);
	    var startDate = component.get('v.accountContactRel.CPAO_Start_Date__c');
	    var endDate = component.get('v.accountContactRel.CPAO_End_Date__c');
		if(primaryEmployerOldValue=='Yes' && (!primaryEmployerNewValue) ){
			component.set('v.severity','error');
			component.set("v.confirmationMessage","Before Removing This Employer as Your Primary Employer, You Must Add Another Primary Employer First.");		
		}else if(startDate>endDate){
			component.set('v.severity','error');
			var error = "Start Date Must Be Prior To End Date."// Start Date is: " + startDate;
			component.set("v.confirmationMessage", error);
		} else if(primaryEmployerOldValue=='No' && primaryEmployerNewValue){
			helper.updatePrimaryEmployer(component,event);
		
		}else{     
			helper.updateSecondaryEmployer(component,event);
		}
 
	},
	showEditableForm : function(component, event, helper) {
		component.set("v.showEditableForm",true);
		var editButton=component.find('editButton');
		$A.util.addClass(editButton, 'slds-hide');
		
	},

	hideEditableForm : function(component, event, helper) {
		component.set("v.showEditableForm",false);
		var editButton=component.find('editButton');
		$A.util.removeClass(editButton, 'slds-hide');
		
	}
	
})