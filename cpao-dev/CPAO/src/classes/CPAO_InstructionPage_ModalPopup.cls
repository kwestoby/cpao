public class CPAO_InstructionPage_ModalPopup {
    public boolean displayPopup {get; set;}
    
    public void closePopup(){
        displayPopup = false;
    }
    
    public void openPopup(){
        displayPopup = true;
    }
}