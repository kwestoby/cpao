@isTest
private class LookupTest {

	@TestSetup
	static void setupData() {
		Account acc = new Account(Name = 'Test Account',
		                          CPAO_Account_Type__c = 'Firm',
		                          CPAO_Account_Status__c = 'Active',
		                          BillingStreet = '123 Fake St',
		                          BillingCity = 'Toronto',
		                          BillingState = 'Ontario',
		                          BillingPostalCode = '12345',
		                          BillingCountry = 'Canada');
		insert acc;
		Contact con = new Contact(FirstName = 'Test',
		                          LastName = 'Contact',
		                          AccountId = acc.Id,
		                          CPAO_CPA_Contact_Type__c = 'Member',
		                          CPAO_CPA_Status__c = 'Active');
		insert con;
	}

	@isTest
	private static void testLookup() {
		Test.startTest();
		String result = Lookup.searchDB('Account', 'Name', 'Id', 1, 'CPAO_Account_Type__c', 'Firm', 'CPAO_Account_Status__c=\'Inactive\'');
		System.assertEquals('[]', result);
		result = Lookup.searchDB('Account', 'Name', 'Id', 1, 'CPAO_Account_Type__c', 'Firm', '');
		Account acc = [SELECT Id FROM Account LIMIT 1];
		System.assertEquals('[{"val":"'+acc.Id+'","text":"Test Account - 123 Fake St, Toronto, Ontario, 12345,Ontario, Canada","objName":"Account"}]', result);
		Test.stopTest();
	}
}