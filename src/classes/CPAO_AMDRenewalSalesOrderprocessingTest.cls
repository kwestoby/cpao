@isTest
public class CPAO_AMDRenewalSalesOrderprocessingTest {
	
	public static List<Account> factoryAccounts;
	public static List<Contact> factoryContacts;
	public static User currentUser;
	public static User user;
	public static RecordType amdCPDRecordType;
	public static OrderApi__Subscription_Plan__c subPlan;
	public static OrderApi__Subscription__c contactSubscription;
	public static OrderApi__Item__c amdOntarioFee;
	public static OrderApi__Item__c amdCanadaFee;
	public static OrderApi__Item__c amdLateFee;
	public static OrderApi__Item__c cpdLateFee;

	public static void dataInitialize(Integer numberOfAccounts, Integer numberOfContacts){
		CPAO_TestFactory.automationControlSetup();
		factoryAccounts = CPAO_TestFactory.createAccounts(numberOfAccounts);
		insert factoryAccounts;
		factoryContacts = CPAO_TestFactory.createContacts(factoryAccounts[0],numberOfContacts);
		factoryContacts[0].MailingCountry = 'Canada';
		factoryContacts[0].MailingState = 'Ontario';
		insert factoryContacts;
		
		user = CPAO_TestFactory.createCommunityUser(factoryContacts[0].Id);
        insert user;

        currentUser = CPAO_UserSelector.getCurrentUser();
        amdCPDRecordType = [SELECT Id FROM RecordType 
							WHERE Name = :CPAO_ApplicationHandler.AMD_CPD 
							AND SobjectType = 'CPAO_Application__c'][0];
    }
    public static OrderApi__Item__c createItem(String name, Id itemClassId) {
    	return new OrderApi__Item__c(
    		Name = name,
	        OrderApi__Price__c = 100.00,
	        OrderApi__Cost__c = 50.00,
	        OrderApi__Is_Active__c = true,
	        OrderApi__Item_Class__c = itemClassId);
    }
    public static void fontevaDataInitialize() {
    	subPlan = CPAO_FontevaTestFactory.createSubPlan();
    	subPlan.Name = 'AMD';
    	insert subPlan;
    	contactSubscription = new OrderApi__Subscription__c(
    		OrderApi__Subscription_Plan__c = subPlan.Id,
    		OrderApi__Contact__c = factoryContacts[0].Id,
    		CPAO_Effective_Date__c = Date.today());
    	insert contactSubscription;
    	OrderApi__Item_Class__c itemClass = CPAO_FontevaTestFactory.createItemClass();
    	OrderApi__Item_Class__c itemClassLateFee = CPAO_FontevaTestFactory.createItemClass();
    	itemClassLateFee.Name = CPAO_FontevaObjectSelector.LATE_FEE_CLASS;
    	OrderApi__Item_Class__c itemClassTax = CPAO_FontevaTestFactory.createItemClass();
    	itemClassTax.Name = 'tax class';
    	insert new List<OrderApi__Item_Class__c>{itemClass,itemClassLateFee, itemClassTax};
    	
    	OrderApi__Tax_Locale__c ontarioTaxLocale = new OrderApi__Tax_Locale__c(
    		Name = 'Provincial Tax - Ontario',
    		OrderApi__Tax_Locale_Field__c = 'OrderApi__Shipping_State__c',
    		OrderApi__Locale_Values_CSV__c = 'Ontario,ON,ONT,ontario,on,ont');
    	insert ontarioTaxLocale;

    	amdOntarioFee = createItem(CPAO_AMDRenewalSalesOrderprocessing.AMD_CANADA_ITEM, itemClass.Id);
    	amdCanadaFee = createItem(CPAO_AMDRenewalSalesOrderprocessing.AMD_ONTARIO_ITEM, itemClass.Id);
    	amdLateFee = createItem(CPAO_AMDRenewalSalesOrderprocessing.AMD_LATE_FEE, itemClassLateFee.Id);
    	cpdLateFee = createItem(CPAO_AMDRenewalSalesOrderprocessing.CPD_LATE_FEE, itemClassLateFee.Id);
    	OrderApi__Item__c taxItem = createItem('Ontario Tax', itemClassTax.Id);
    	taxItem.OrderApi__Is_Tax__c = true;
    	taxItem.OrderApi__Tax_Locale__c = ontarioTaxLocale.Id;
    	insert new List<OrderApi__Item__c>{amdOntarioFee,amdCanadaFee,amdLateFee,cpdLateFee,taxItem};


    	List<OrderApi__Item_Subscription_Plan__c> isps = new List<OrderApi__Item_Subscription_Plan__c>();
        isps.add(new OrderApi__Item_Subscription_Plan__c(
        	OrderApi__Is_Default__c=true,
	        OrderApi__Item__c=amdOntarioFee.Id,
	        OrderApi__Subscription_Plan__c=subPlan.Id));
        isps.add(new OrderApi__Item_Subscription_Plan__c(
        	OrderApi__Is_Default__c=true,
	        OrderApi__Item__c=amdCanadaFee.Id,
	        OrderApi__Subscription_Plan__c=subPlan.Id));
        insert isps;
    }

    @isTest
	public static void loadAttestationInfoTest() {
		dataInitialize(1,1);
		CPAO_Account_Contact_Relationship__c relationship = CPAO_TestFactory.createAccountContactRelationships(
			factoryAccounts[0], factoryContacts[0], 1)[0];
		relationship.CPAO_Primary_Employer__c = 'Yes';
		insert relationship;
		CPAO_Application__c application = new CPAO_Application__c();
		
		application.CPAO_Contact__c = factoryContacts[0].Id;
		application.CPAO_Status__c = 'Open';
		application.RecordTypeId = amdCPDRecordType.Id;
		insert application;
		system.runAs(user) {
			CPAO_AMDRenewalSalesOrderprocessing.ReviewPageWrapper result = CPAO_AMDRenewalSalesOrderprocessing.loadReviewInfo();
		}
	}

	@isTest
	public static void saveApplicationAttestationTest() {
		dataInitialize(1,1);
		fontevaDataInitialize();
		CPAO_Application__c application = new CPAO_Application__c();
		
		application.CPAO_Contact__c = factoryContacts[0].Id;
		application.CPAO_Status__c = 'Open';
		application.RecordTypeId = amdCPDRecordType.Id;
		application.CPAO_AMD_Location_Based_Reduction__c = 'Category 0100 – Member is paying full CPA Ontario and Full CPA Canada fees to CPAO';
		application.CPAO_AMD_Waiver__c = 'None';
		application.CPAO_AMD_Zero_Tax_Certificate__c = 'Yes';
		application.CPAO_Employer_is_paying_AMD__c = 'No';
		insert application;
		application = [SELECT CPAO_Sub_Type__c, CPAO_Status__c, CPAO_Contact__c, CPAO_Employer_is_paying_AMD__c, CPAO_Applied_for_Deferral__c, 
			CPAO_AMD_Waiver__c, CPAO_AMD_Location_Based_Reduction__c, CPAO_AMD_Zero_Tax_Certificate__c 
			FROM CPAO_Application__c WHERE Id = :application.Id];
		system.runAs(user) {
			CPAO_AMDRenewalSalesOrderprocessing.saveApplicationAttestation(application);
			application.CPAO_Employer_is_paying_AMD__c = 'Yes';
			CPAO_AMDRenewalSalesOrderprocessing.saveApplicationAttestation(application);
		}
	}
}