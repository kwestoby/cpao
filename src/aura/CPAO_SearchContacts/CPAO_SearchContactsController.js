({
	doInit : function(component, event, helper) {
		helper.init(component, event);
        var action = component.get("c.getUser");
        action.setCallback(this, function(response) {
            var state = response.getState();
            if (state === "SUCCESS") {
                var storeResponse = response.getReturnValue();
                component.set("v.userInfo", storeResponse);
            }
        });
        $A.enqueueAction(action);
	},
    
   	SearchJS : function(component, event, helper) {
        var searchKeyId=component.find("searchId"); 
        var searchKey=component.get('v.searchKeyword');
        if(searchKey == '' || searchKey == null)
        {
            //searchKeyId.set("v.errors",[{message: "Enter search Keyword" }]);
             $A.util.addClass(searchKeyId,'errorClassAdd');
            //$A.util.removeClass(searchKeyId,'errorClassRemove');
        }
        else{
             $A.util.addClass(searchKeyId, 'errorClassRemove');
             $A.util.removeClass(searchKeyId, 'errorClassAdd');
             helper.searchHelper(component,event);
        }
    },
    /* 
    AddtotheContactList : function(component, event, helper) {
 		helper.AddtotheContactList(component,event);
    }, */
   
  	display : function(component, event, helper) {
    helper.toggleHelper(component, event);
  	},

  	displayOut : function(component, event, helper) {
   	helper.toggleHelper(component, event);
  	},
    
    addSelectedCheckboxes : function(component, event, helper) {
    helper.addSelectedCheckboxes(component,event);
   }
})