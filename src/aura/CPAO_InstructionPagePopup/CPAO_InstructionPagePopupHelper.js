({
    showinstructionScreen : function(cmp, ev) {
		var action = cmp.get("c.queryInstructionScreen");
        action.setCallback(this, function(response){
        cmp.set("v.instructionScreen", response.getReturnValue());
        });
        $A.enqueueAction(action);   
    },
    
    getContacts : function(cmp, ev) {
    	//Passing Value to Apex Controller till $A.enqueueAction(action) 
        var cmpTarget = cmp.find('Modalbox');
		var cmpBack = cmp.find('Modalbackdrop');
		$A.util.removeClass(cmpBack,'slds-backdrop--open');
		$A.util.removeClass(cmpTarget, 'slds-fade-in-open');
       
        var selectedAccObj = cmp.get('v.selectedAcc'); 
        var selectedPay = cmp.get('v.SelectedPaymentType');
        var action = cmp.get("c.getContactNames");
        
        var ids=new Array();
        for (var idx=0; idx<selectedAccObj.length; idx++) {
            if (selectedAccObj[idx].selected) {
	            ids.push(selectedAccObj[idx].acc.Id);
            }
        }
        var idListJSON=JSON.stringify(ids);
        action.setParams({
        	"selectedRosterPaymentType": selectedPay,
            "selectedAccountIds": idListJSON
        });

        cmp.set('v.instructionRead',true); 
      
     	action.setCallback(this, function(response) {
     	var state = response.getState();
            
        if (state === "SUCCESS") {
			var cons=response.getReturnValue()

			var wrappers=new Array();
            for (var idx=0; idx<cons.length; idx++) {
            	var wrapper = { 'con' : cons[idx], 
					'selected' : false
				}; 
                wrappers.push(wrapper);
            }
        
            cmp.set('v.wrappers', wrappers);
        }
        else if (state === "ERROR") {
        	var errors=response.getError();
            var message = errors[0];
            console.log(message); 
            alert('Error : ' + JSON.stringify(errors));
        }
     });
     $A.enqueueAction(action);
       
    },
    
    refreshContacts : function(cmp, ev) {
    var new_wrapper= ev.getParam('Final_wrappers');   	
    var oldWrapper = cmp.get('v.wrappers'); 
   		if(oldWrapper != null){
			for (var idx=0; idx<new_wrapper.length; idx++) {
            	if(new_wrapper[idx].selected)
            		oldWrapper.push(new_wrapper[idx]);
        	}
			cmp.set('v.wrappers', oldWrapper);     
    	}

    },
    
    checkAllCheckboxes: function(cmp,event){
    	var masterCheckboxes = cmp.find("masterCheckbox");

        var childCheckboxes = cmp.find("DependentCheckbox");
        
        var wrapObj = cmp.find("wrapId");
        //var oldWrapper = cmp.set('v.wrappers',wrapObj);
        //alert('oldWrapper456 : ' + JSON.stringify(oldWrapper));  

        var val = masterCheckboxes.get("v.value");

  		if (!Array.isArray(childCheckboxes)) {
   			childCheckboxes = [childCheckboxes];
  		}

  		for (var i = 0; i < childCheckboxes.length; i++) {
    		childCheckboxes[i].set("v.value", val);
  		}
        
        var oldWrapper = cmp.get('v.wrappers');

        var wrappers=new Array();
        for (var idx=0; idx<oldWrapper.length; idx++) {
            if(oldWrapper[idx].con.Subscription_Status_of_Completion_AMD__c == undefined){
            //	alert('oldWrapper[idx].con.Subscription_Status_of_Completionffff'+oldWrapper[idx].con.Subscription_Status_of_Completion_AMD__c);
            }
            //alert('oldWrapper[idx].con.Subscription_Status_of_Completion_AMD__c'+oldWrapper[idx].con.Subscription_Status_of_Completion_AMD__c);
        	if(oldWrapper[idx].con.Subscription_Status_of_Completion_AMD__c == 'AMD' && oldWrapper[idx].con.Subscription_Status_of_Completion_AMD__c != ''){
				var wrapper = { 'con' : oldWrapper[idx].con.Id, 
								'selected' : false
				}; 
				wrappers.push(wrapper);
             }
        }

	 },
    
     saveMarkedContactsaAndOpenPayment : function(cmp, ev) {
     	cmp.set('v.paymentCheckVar',true);
        var oldWrapper = cmp.get('v.wrappers');

        var ids=new Array();
        var action = cmp.get("c.saveMarkedContacts");

        for (var idx=0; idx<oldWrapper.length; idx++) {
            if (oldWrapper[idx].selected) {
				ids.push(oldWrapper[idx].con.Id);
            	cmp.set('v.wrappers',oldWrapper[idx]);
            }
        }

        var idListJSON=JSON.stringify(ids);
        action.setParams({
        	"idListJSONStr": idListJSON
        });
 
        action.setCallback(this, function(response) {
            var state = response.getState();

            if (state === "SUCCESS") {
				var cons=response.getReturnValue()
                cmp.set('v.wrappers', cons);
            }
            else if (state === "ERROR") {
		        var errors = response.getError();
                alert('Error : ' + JSON.stringify(errors));
            }
        });
         
        var toggleContent = cmp.find("content");
        $A.util.toggleClass(toggleContent, "toggle");
        $A.enqueueAction(action);
     }
    
})