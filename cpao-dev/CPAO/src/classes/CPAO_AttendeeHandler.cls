public without sharing class CPAO_AttendeeHandler {
    
    public static void beforeInsertFilter(List<EventApi__Attendee__c> triggerNew) {
        List<EventApi__Attendee__c> properEventCancelations = new List<EventApi__Attendee__c>();
        for(EventApi__Attendee__c attendee:triggerNew){
            if(attendee.EventApi__Refund_Requested__c && attendee.CPAO_Can_Event_Be_Cancelled__c){
                properEventCancelations.add(attendee);
            }
        }
        if(!properEventCancelations.isEmpty()){
            cancelEventRegistration(properEventCancelations);
        }
    }

    public static void beforeUpdateFilter(List<EventApi__Attendee__c> triggerNew, Map<Id, EventApi__Attendee__c> oldMap) {
        List<EventApi__Attendee__c> properEventCancelations = new List<EventApi__Attendee__c>();
        for(EventApi__Attendee__c attendee:triggerNew){
            if(attendee.EventApi__Refund_Requested__c != oldMap.get(attendee.Id).EventApi__Refund_Requested__c){
                if(attendee.EventApi__Refund_Requested__c && attendee.CPAO_Can_Event_Be_Cancelled__c){
                    properEventCancelations.add(attendee);
                }
            }               
        }
        if(!properEventCancelations.isEmpty()){
            cancelEventRegistration(properEventCancelations);
        }
    }

    //cancels an event registration. this is done when the CPAO_Can_Event_Be_Cancelled__c field is set to true,
    // this field is true when the current time is before the event start date and before the number of hours before event
    //start date a refund can be granted according to the Event field CPAO_Hours_to_Cancel_Registration__c
    public static void cancelEventRegistration(List<EventApi__Attendee__c> attendeesToBeCanceled) {
        for(EventApi__Attendee__c attendee:attendeesToBeCanceled){
            attendee.EventApi__Status__c = 'Cancelled';
        }
    }
}