@isTest
private class CPAO_ReadmissionContainerCTRLTest {
	
	public static List<Account> factoryAccounts;
	public static List<Contact> factoryContacts;
	public static User currentUser;
	public static User user;
	public static RecordType amdCPDRecordType;

	public static void dataInitialize(Integer numberOfAccounts, Integer numberOfContacts){
		CPAO_TestFactory.automationControlSetup();
		factoryAccounts = CPAO_TestFactory.createAccounts(numberOfAccounts);
		insert factoryAccounts;
		factoryContacts = CPAO_TestFactory.createContacts(factoryAccounts[0],numberOfContacts);

		insert factoryContacts;
		
        
        
		user = CPAO_TestFactory.createCommunityUser(factoryContacts[0].Id);
        insert user;
        

        currentUser = CPAO_UserSelector.getCurrentUser();
        amdCPDRecordType = [SELECT Id FROM RecordType 
												WHERE Name = :CPAO_ApplicationHandler.READMISSION_RECORDTYPENAME 
												AND SobjectType = 'CPAO_Application__c'][0];
    }

    @isTest
	public static void basicTest() {
		dataInitialize(1,1);
		CPAO_Application__c application = new CPAO_Application__c();
		
		application.CPAO_Contact__c = factoryContacts[0].Id;
		application.CPAO_Status__c = 'Open';
		application.RecordTypeId = amdCPDRecordType.Id;
		application.CPAO_JSON_File_Map__c = '{}';
		insert application;
		application = [SELECT Id, CPAO_JSON_File_Map__c FROM CPAO_Application__c WHERE Id = :application.Id];
		List<String> keyList = new List<String>{'test'};
		List<String> fileNameList = new List<String>{'test'};
    	List<String> base64DataList = new List<String>{'1010101'};
    	List<String> contentTypeList = new List<String>{'html'};
    	CPAO_ReadmissionContainerCTRL.save5Year(keyList, fileNameList, base64DataList, contentTypeList, application);
    	CPAO_ReadmissionContainerCTRL.save5Year(keyList, fileNameList, base64DataList, contentTypeList, application);
    	CPAO_ReadmissionContainerCTRL.saveCpd('fileNamecriminalExplanation', '1010101', 'html', application);
		CPAO_ReadmissionContainerCTRL.saveCertificate('fileNamesuspensionExplanation', '1010101', 'html', application);
		CPAO_ReadmissionContainerCTRL.saveCpd('fileNamecriminalExplanation', '1010101', 'html', application);
		CPAO_ReadmissionContainerCTRL.saveCertificate('fileNamesuspensionExplanation', '1010101', 'html', application);
    	
	    OrderApi__Item_Class__c itemClass = CPAO_FontevaTestFactory.createItemClass();
		insert itemClass;
		insert (new OrderApi__Item__c(
    		Name = 'Readmission Fee',
	        OrderApi__Price__c = 100.00,
	        OrderApi__Cost__c = 50.00,
	        OrderApi__Is_Active__c = true,
	        OrderApi__Item_Class__c = itemClass.Id));

		system.runAs(user){
    		CPAO_ReadmissionContainerCTRL.getReadmissionInfo();
			CPAO_ReadmissionContainerCTRL.loadReviewInfo();
			CPAO_ReadmissionContainerCTRL.saveAttestation(application);
    	}

	}	
}