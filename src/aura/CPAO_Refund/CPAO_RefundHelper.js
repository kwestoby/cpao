({
    //Fetch account from Apex controller
    getReceiptList: function(component) {
        var action = component.get('c.getReceipts');
        
        //Set up the callback
        console.log("here1");
        var self = this;
        action.setCallback(this, function(actionResult) {
        	console.log("here2");
            console.log(actionResult.getReturnValue());
            component.set('v.receipts', actionResult.getReturnValue());
        });
        
        $A.enqueueAction(action);
    },
    createRefund: function(component, receiptID, reason){
        
        
        var action = component.get('c.createRefund');
        //action.setParams({receiptID});
        action.setParams({id: receiptID, reason: reason});
                               
		$A.enqueueAction(action);
     
        
        
    }
    
})