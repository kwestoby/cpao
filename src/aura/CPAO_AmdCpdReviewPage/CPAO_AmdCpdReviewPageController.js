({
	doAction : function(component, event, helper) {
		var apexAction = component.get('c.loadReviewInfo');
        apexAction.setCallback(this, function(response) {
        	var state = response.getState();
            console.log('state');
            console.log(state);
        	if(state === "SUCCESS"){
                var result = response.getReturnValue();
                component.set("v.userContact", result.userContact);
                component.set("v.contactEmploymentRecords", result.contactEmploymentRecords);
                component.set("v.contactEmploymentRecordsSize", result.contactEmploymentRecords.length);
                component.set("v.currentAMDCPDApplication", result.currentAMDCPDApplication);
                component.set("v.poaItemsSize", result.poaItems.length);
                component.set("v.poaItems", result.poaItems);
                component.set("v.canEmployerPay", result.canEmployerPay);
            }
        });
        $A.enqueueAction(apexAction);
	},

    clickOnNextButton : function(component,event,helper){
        var talkingToComponent = event.getParam("componentName");
        if(talkingToComponent == 'step7'){
            var apexAction = component.get('c.saveApplicationAttestation');
            var app = component.get("v.currentAMDCPDApplication");
            var canEmployerPay = component.get("v.canEmployerPay");
            if(canEmployerPay == true){
                if(app.CPAO_Employer_is_paying_AMD__c != 'Yes' && app.CPAO_Employer_is_paying_AMD__c != 'No'){
                    helper.methodFailure(component, event, helper, 'Please specify if your employer is paying your AMD fees', 'Error');
                    return;
                }

            }
            if(app.CPAO_Attestation__c != true){
                helper.methodFailure(component, event, helper, 'Please attest', 'Error');
                return;
            }
            var hasAMDWaiver = component.get("v.hasAMDWaiver");
            var hasCPDWaiver = component.get("v.hasCPDWaiver");
            apexAction.setParams({
                "currentAMDCPDApplication": app
            });
            apexAction.setCallback(this, function(response) {
                var state = response.getState();
                console.log('state');
                console.log(state);
                if(state === "SUCCESS"){
                    var result = response.getReturnValue();
                    console.log('result');
                        console.log(result);
                    if(result.status == 'Success'){

                        window.location.href = result.checkOutScreenURL;
                    } else {
                        helper.methodFailure(component, event, helper, result.msg, result.status);    
                    }
                } else {
                    helper.methodFailure(component, event, helper);
                }
            });
            $A.enqueueAction(apexAction);
        }        
    },
})