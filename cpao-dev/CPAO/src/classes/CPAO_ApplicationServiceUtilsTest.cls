@isTest
public class CPAO_ApplicationServiceUtilsTest {

	public static List<Account> factoryAccounts;
	public static List<Contact> factoryContacts;
	public static User currentUser;
	public static User user;

	public static void dataInitialize(Integer numberOfAccounts, Integer numberOfContacts){
		CPAO_TestFactory.automationControlSetup();
		factoryAccounts = CPAO_TestFactory.createAccounts(numberOfAccounts);
		insert factoryAccounts;
		factoryContacts = CPAO_TestFactory.createContacts(factoryAccounts[0],numberOfContacts);
		factoryContacts = CPAO_TestFactory.addContactStudentInfo(factoryContacts);

		insert factoryContacts;
		
		user = CPAO_TestFactory.createCommunityUser(factoryContacts[0].Id);
        insert user;

        currentUser = CPAO_UserSelector.getCurrentUser();
	}

	@isTest
	public static void standardTest() {
		dataInitialize(1,1);
		CPAO_Application__c application = CPAO_TestFactory.createAMDCPDpplication(1, factoryContacts[0])[0];
		insert application;
		CPAO_Application__c application2 = CPAO_TestFactory.createAMDCPDpplication(1, factoryContacts[0])[0];
		application2.CPAO_Status__c = 'Completed';
		insert application2;
		application = [SELECT Id, CPAO_Contact__c, CPAO_Application_Type__c, CPAO_Status__c FROM CPAO_Application__c WHERE Id = :application.Id];
		CPAO_ApplicationServiceUtils.sendToRegistrarQueue(new List<CPAO_Application__c>{application});
		CPAO_ApplicationServiceUtils.sendToRO_rQueue(new List<CPAO_Application__c>{application});
		CPAO_ApplicationServiceUtils.findOpenInfractions(new List<CPAO_Application__c>{application});
		CPAO_ApplicationServiceUtils.checkDuplicateApplication(new List<CPAO_Application__c>{application});
	}

}