public without sharing class CPAO_MyRegisteredEventsCTRL {

	@AuraEnabled
	public static List<EventApi__Attendee__c> getEvents(){
		User loggedUser = CPAO_UserSelector.getCurrentUser();
		return [SELECT Id, Name, EventApi__Event__r.EventApi__Start_Date__c, EventApi__Event__r.EventApi__End_Date__c, 
					EventApi__Event__r.Name, EventApi__Event__r.Id, EventApi__Event__r.EventApi__Status__c
				FROM EventApi__Attendee__c
				WHERE EventApi__Contact__c=:loggedUser.ContactId
				ORDER BY EventApi__Event__r.EventApi__Start_Date__c ASC];
	}
}