public with sharing class CPAO_ContactHandler {
    public static String INACTIVE_STATUS = 'Inactive';
    public static String SUSPENDED = 'Suspended';
    public static String REVOKED = 'Revoked';
    public static String RESIGNED = 'Resigned';
    public static String DEREGISTERED = 'Deregistered';
    public static String MEMBER = 'Member';
    /*public static String MEMBER_CONTACT_TYPE = 'Member';
public static String ACTIVE_STATUS = 'Member Active';
public static String SUSPENDED_STATUS = 'Member Suspended';*/
    public static String contactListSizeError = 'Contact list size error, please contact an administrator';
    public static String idCurrentlyExists = Label.idCurrentlyExists;
    
    public static void beforeInsertFilters(List<Contact> contacts) {
        List<Contact> filteredContacts = new List<Contact>();
        List<Contact> suspendedContacts = new List<Contact>();
        List<Contact> revokedDeregisteredContacts = new List<Contact>();
        for(Contact contact:contacts){
            if(contact.CPAO_CPA_Id__c != null){
                filteredContacts.add(contact);
            }
            if(contact.CPAO_CPA_Status__c == SUSPENDED){
                suspendedContacts.add(contact);
            }
            if(contact.CPAO_CPA_Status__c == REVOKED || contact.CPAO_CPA_Status__c == DEREGISTERED){
                revokedDeregisteredContacts.add(contact);
            }
        }
        if(!filteredContacts.isEmpty()){
            modifyCPAIdDisplay(filteredContacts);
        }

        if(!suspendedContacts.isEmpty()){
            CPAO_ContactServices.suspendAction(suspendedContacts);
        }
        if(!revokedDeregisteredContacts.isEmpty()){
            CPAO_ContactServices.revokeDeregisterAction(revokedDeregisteredContacts);
        }
    }
    public static void beforeUpdateFilters(List<Contact> contacts, Map<Id, Contact> oldMap) {
        List<Contact> filteredContacts = new List<Contact>();
        List<Contact> suspendedContacts = new List<Contact>();
        List<Contact> revokedDeregisteredContacts = new List<Contact>();
        for(Contact contact:contacts){
            if(oldMap.get(contact.Id).CPAO_CPA_Id__c != contact.CPAO_CPA_Id__c){
                if(contact.CPAO_CPA_Id__c != null){
                    filteredContacts.add(contact);
                }
            }
            if(contact.CPAO_CPA_Status__c == SUSPENDED && oldMap.get(contact.Id).CPAO_CPA_Status__c != SUSPENDED){
                suspendedContacts.add(contact);
            }
            if((contact.CPAO_CPA_Status__c == REVOKED && oldMap.get(contact.Id).CPAO_CPA_Status__c != REVOKED) || 
                (contact.CPAO_CPA_Status__c == DEREGISTERED && oldMap.get(contact.Id).CPAO_CPA_Status__c != DEREGISTERED)){
                revokedDeregisteredContacts.add(contact);
            }
        }
        if(!filteredContacts.isEmpty()){
            modifyCPAIdDisplay(filteredContacts);
        }
        if(!suspendedContacts.isEmpty()){
            CPAO_ContactServices.suspendAction(suspendedContacts);
        }
        if(!revokedDeregisteredContacts.isEmpty()){
            CPAO_ContactServices.revokeDeregisterAction(revokedDeregisteredContacts);
        }
    }
    //handles calls from insert trigger, filters and then calls methods where relevant
    public static void afterInsertFilters(List<Contact> contacts) {
        List<Contact> contactListCPAIdNotNull = new List<Contact>();
        List<Id> contactsEligibleToPAL = new List<Id>();
        List<Contact> contactListAccountLookupNotNull = new List<Contact>();

        List<Contact> readmissionContacts = new List<Contact>();
        List<Contact> reinstatementContacts = new List<Contact>();

        for(Contact contact:contacts){
            if(contact.CPAO_CPA_Id__c != null){
                contactListCPAIdNotNull.add(contact);
            }
            if(contact.AccountId != null){
                contactListAccountLookupNotNull.add(contact);
            }
            
            if(CPAO_ContactServices.contact_eligible_To_PAL_Badge(contact)){
                contactsEligibleToPAL.add(contact.Id);
            }

            if(contact.CPAO_CPA_Contact_Type__c == MEMBER && (contact.CPAO_CPA_Status__c == RESIGNED || contact.CPAO_CPA_Status__c == REVOKED)){
                readmissionContacts.add(contact);
            }
            
            if(contact.CPAO_CPA_Contact_Type__c == MEMBER && contact.CPAO_CPA_Status__c == SUSPENDED){
                
                reinstatementContacts.add(contact);
            }
            
        }
        if(!contactListCPAIdNotNull.isEmpty()){
            System.debug('contactListCPAIdNotNull: ' +contactListCPAIdNotNull);
            packageAndPassToIdClass(null, contactListCPAIdNotNull, null);
        }
        if(!contactListAccountLookupNotNull.isEmpty()){
            System.debug('contactListAccountLookupNotNull: ' +contactListAccountLookupNotNull);
            //createRelationshipRecords(contactListAccountLookupNotNull);
        }
        if(!contactsEligibleToPAL.isEmpty()){
            CPAO_ContactServices.assignBadgeToContacts(contactsEligibleToPAL,'PAL');
        }
        
        if(!readmissionContacts.isEmpty()){
            CPAO_ContactServices.createReadmissionApplications(readmissionContacts);
        }
        
        if(!reinstatementContacts.isEmpty()){
            CPAO_ContactServices.createReinstatementApplications(reinstatementContacts);
        }
        
    }
    
    //handles calls from update trigger, filters and then calls methods where relevant
    public static void afterUpdateFilters(List<Contact> contacts, Map<Id, Contact> oldMap) {
        List<Decimal> idsToBeInactivated = new List<Decimal>();
        List<Contact> manualAssignContacts = new List<Contact>();
        List<Id> contactsEligibleToPAL = new List<Id>();
        //List of contacts that need to have their PAL badge removed
        List<Id> PALContactsToRemove = new List<Id>(); 

        List<Contact> readmissionContacts = new List<Contact>();
        List<Contact> reinstatementContacts = new List<Contact>();
        List<Contact> cancelReinstatementContacts = new List<Contact>();
        
        for(Contact contact:contacts){
            if(oldMap.get(contact.Id).CPAO_CPA_Id__c != contact.CPAO_CPA_Id__c){
                if(contact.CPAO_CPA_Id__c != null){
                    manualAssignContacts.add(contact);
                }
                if(oldMap.get(contact.Id).CPAO_CPA_Id__c != null){
                    idsToBeInactivated.add(oldMap.get(contact.Id).CPAO_CPA_Id__c);
                }
                
            }
            //TO DO: Verifier que old values isnt the same
            /*if(contact.CPAO_CPA_Contact_Type__c==MEMBER_CONTACT_TYPE && (contact.CPAO_CPA_Status__c
==ACTIVE_STATUS	||contact.CPAO_CPA_Status__c==SUSPENDED_STATUS)){
contactsEligibleToPAL.add(contact.Id);
}

Boolean PAL_Badge_Eligible=(oldMap.get(contact.Id).CPAO_CPA_Contact_Type__c==MEMBER_CONTACT_TYPE && (oldMap.get(contact.Id).CPAO_CPA_Status__c
==ACTIVE_STATUS	||oldMap.get(contact.Id).CPAO_CPA_Status__c==SUSPENDED_STATUS));


if((PAL_Badge_Eligible) && (contact.CPAO_CPA_Contact_Type__c!=
MEMBER_CONTACT_TYPE ||( contact.CPAO_CPA_Status__c!=ACTIVE_STATUS && contact.CPAO_CPA_Status__c!=SUSPENDED_STATUS))){
system.debug(oldMap.get(contact.Id));
PALContactsToRemove.add(contact.Id);
}*/
            if(CPAO_ContactServices.contact_eligible_To_PAL_Badge(contact) &&
               (!CPAO_ContactServices.contact_eligible_To_PAL_Badge(oldMap.get(contact.Id)))){
                   contactsEligibleToPAL.add(contact.Id);
               }
            
            if(CPAO_ContactServices.contact_eligible_To_PAL_Badge(oldMap.get(contact.Id)) &&
               (!CPAO_ContactServices.contact_eligible_To_PAL_Badge(contact))){
                   PALContactsToRemove.add(contact.Id);
               }

            if(contact.CPAO_CPA_Contact_Type__c == MEMBER && (contact.CPAO_CPA_Status__c == RESIGNED || contact.CPAO_CPA_Status__c == REVOKED)
            && oldMap.get(contact.Id).CPAO_CPA_Status__c != RESIGNED && oldMap.get(contact.Id).CPAO_CPA_Status__c != REVOKED){
                readmissionContacts.add(contact);
            }
            
            if(contact.CPAO_CPA_Contact_Type__c == MEMBER){
                if(contact.CPAO_CPA_Status__c != oldMap.get(contact.Id).CPAO_CPA_Status__c){
                    if(contact.CPAO_CPA_Status__c == SUSPENDED){

                        reinstatementContacts.add(contact);
                        
                    }
            	}  
            }
            
            if(contact.CPAO_CPA_Contact_Type__c == MEMBER && oldMap.get(contact.Id).CPAO_CPA_Status__c == SUSPENDED){
                if(contact.CPAO_CPA_Status__c != oldMap.get(contact.Id).CPAO_CPA_Status__c){
                    if(contact.CPAO_CPA_Status__c != SUSPENDED){

                        cancelReinstatementContacts.add(contact);
                        
                    }
            	}  
            }
            
        }
        
        //TODO: Rajouter portion todelete documentation, datamodel
        
        
        if(!manualAssignContacts.isEmpty()){
            packageAndPassToIdClass(null, manualAssignContacts, idsToBeInactivated);
        }
        if(!contactsEligibleToPAL.isEmpty()){
            CPAO_ContactServices.assignBadgeToContacts(contactsEligibleToPAL,'PAL');
        }
        if(!PALContactsToRemove.isEmpty()){
            CPAO_ContactServices.removeBadgeFromContact(PALContactsToRemove,'PAL');
        }

        if(!readmissionContacts.isEmpty()){
            CPAO_ContactServices.createReadmissionApplications(readmissionContacts);
        }
        
        if(!reinstatementContacts.isEmpty()){
            CPAO_ContactServices.createReinstatementApplications(reinstatementContacts);
        }
        
        if(!cancelReinstatementContacts.isEmpty()){
            
            CPAO_ContactServices.cancelReinstatementApplications(cancelReinstatementContacts);
        }
    }
    /*
//handles calls from after delete trigger, filters and then calls methods where relevant
//
public static void afterDeleteFilters(List<Account> accounts) {
List<Id> primaryContactsToRemove= new List<Id>();
List<Id> LSOContactsToRemove= new List<Id>();

for(Account account:accounts){ 
if(account.OrderApi__Primary_Contact__c!=null){
primaryContactsToRemove.add(account.OrderApi__Primary_Contact__c);
}

if(account.CPAO_LSO__c!=null){
LSOContactsToRemove.add(account.CPAO_LSO__c);
}
}

if(!LSOContactsToRemove.isEmpty()){
CPAO_ContactServices.removeBadgeFromContact(LSOContactsToRemove,'LSO');
}
if(!primaryContactsToRemove.isEmpty()){
CPAO_ContactServices.removeBadgeFromContact(primaryContactsToRemove,'Primary Contact');
}
}*/
    
    
    public static void afterDeleteFilters(Map<Id, Contact> oldMap) {
        CPAO_ContactServices.removeAllBadgesFromContact( oldMap.keySet());
    }
    
    
    //this will handle new Id button clicks on the contact page
    public static void handleNewIdButtonClicks(Id contactId) {
        List<Contact> contact = [SELECT Id, CPAO_CPA_Id__c FROM Contact WHERE Id =:contactId];
        
        if(contact.size() != 1){
            throw new CPAO_ContactHandler_Exception(contactListSizeError);
        }
        
        List<Decimal> idsToRenderInactive = new List<Decimal>();
        if(contact[0].CPAO_CPA_Id__c != null){
            throw new CPAO_ContactHandler_Exception(idCurrentlyExists);
            //idsToRenderInactive.add(contact[0].CPAO_CPA_Id__c);
        }
        packageAndPassToIdClass(new List<Id>{contact[0].Id}, null, idsToRenderInactive);
    }
    
    //creates a relationship record if the account lookup is fileld in
    /*public static void createRelationshipRecords(List<Contact> contacts) {
System.debug('contacts: ' +contacts);
List<CPAO_Account_Contact_Relationship__c> relationshipsToInsert = new List<CPAO_Account_Contact_Relationship__c>();
for(Contact contact:contacts){
relationshipsToInsert.add(new CPAO_Account_Contact_Relationship__c(
CPAO_Account__c = contact.AccountId,
CPAO_Contact__c = contact.Id,
CPAO_Start_Date__c = Date.today(),
CPAO_Relationship_to_Account__c = 'Employee'));
}
if(!relationshipsToInsert.isEmpty()){
System.debug('relationshipsToInsert: ' +relationshipsToInsert);
insert relationshipsToInsert;
}
}*/
    //handles contact records which are to be sent to CPAO_CPAIdRecordAssigner for dealing with Id Record assignment
    public static void packageAndPassToIdClass(List<Id> autoAssignContacts, List<Contact> manualAssignContacts, List<Decimal> idsToRenderInactive) {
        
        if(CPAO_CPAIdRecordAssigner.hasrun){
            return;
        }
        CPAO_RecordIdAssignmentWrapper wrapper = new CPAO_RecordIdAssignmentWrapper('Contact');
        if(autoAssignContacts != null){
            for(Id contactId:autoAssignContacts){
                wrapper.idsAutomaticAssignment.add(contactId);
            }
        }
        if(manualAssignContacts != null){
            for(Contact contact:manualAssignContacts){
                wrapper.idsManualAssignment.put(contact.Id, contact.CPAO_CPA_Id__c);
            }
        }
        
        if(idsToRenderInactive != null){
            for(Decimal idRecord:idsToRenderInactive){
                wrapper.idRecordsToRenderInactive.add(idRecord);
            }
        }
        
        CPAO_CPAIdRecordAssigner.assignIds(wrapper);
    }
    
    public static void modifyCPAIdDisplay(List<Contact> contacts) {
        for(Contact contact:contacts){
            String cpaIdDisplay = String.valueOf(contact.CPAO_CPA_Id__c);
            //while(cpaIdDisplay.length()<9){
            //	cpaIdDisplay = '0' + cpaIdDisplay;
            //}
            contact.CPAO_CPA_Id_Display__c = 'C' + cpaIdDisplay;
        }
    }
    
    public class CPAO_ContactHandler_Exception extends Exception {}
}