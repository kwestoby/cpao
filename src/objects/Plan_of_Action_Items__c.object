<?xml version="1.0" encoding="UTF-8"?>
<CustomObject xmlns="http://soap.sforce.com/2006/04/metadata">
    <actionOverrides>
        <actionName>Accept</actionName>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>CancelEdit</actionName>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>Clone</actionName>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>Delete</actionName>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>Edit</actionName>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>List</actionName>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>New</actionName>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>SaveEdit</actionName>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>Tab</actionName>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>View</actionName>
        <type>Default</type>
    </actionOverrides>
    <allowInChatterGroups>true</allowInChatterGroups>
    <compactLayoutAssignment>SYSTEM</compactLayoutAssignment>
    <deploymentStatus>Deployed</deploymentStatus>
    <enableActivities>true</enableActivities>
    <enableBulkApi>true</enableBulkApi>
    <enableChangeDataCapture>false</enableChangeDataCapture>
    <enableFeeds>false</enableFeeds>
    <enableHistory>true</enableHistory>
    <enableReports>true</enableReports>
    <enableSearch>true</enableSearch>
    <enableSharing>true</enableSharing>
    <enableStreamingApi>true</enableStreamingApi>
    <externalSharingModel>ControlledByParent</externalSharingModel>
    <fields>
        <fullName>CPAO_Application__c</fullName>
        <externalId>false</externalId>
        <label>Application</label>
        <referenceTo>CPAO_Application__c</referenceTo>
        <relationshipName>Plan_of_Action_Items</relationshipName>
        <relationshipOrder>0</relationshipOrder>
        <reparentableMasterDetail>true</reparentableMasterDetail>
        <trackHistory>false</trackHistory>
        <trackTrending>false</trackTrending>
        <type>MasterDetail</type>
        <writeRequiresMasterRead>false</writeRequiresMasterRead>
    </fields>
    <fields>
        <fullName>CPAO_Audit__c</fullName>
        <deleteConstraint>SetNull</deleteConstraint>
        <description>Note: contact Dan</description>
        <externalId>false</externalId>
        <label>Audit</label>
        <referenceTo>CPAO_Audit__c</referenceTo>
        <relationshipLabel>Log Items</relationshipLabel>
        <relationshipName>Log_Items1</relationshipName>
        <required>false</required>
        <trackHistory>false</trackHistory>
        <trackTrending>false</trackTrending>
        <type>Lookup</type>
    </fields>
    <fields>
        <fullName>CPAO_CPD_Provider_or_Sponsoring__c</fullName>
        <externalId>false</externalId>
        <label>CPD Provider or Sponsoring organization</label>
        <length>200</length>
        <required>true</required>
        <trackHistory>false</trackHistory>
        <trackTrending>false</trackTrending>
        <type>Text</type>
        <unique>false</unique>
    </fields>
    <fields>
        <fullName>CPAO_CPD_Unverifiable_Hours__c</fullName>
        <externalId>false</externalId>
        <label>CPD Unverifiable Hours</label>
        <precision>18</precision>
        <required>false</required>
        <scale>0</scale>
        <trackHistory>false</trackHistory>
        <trackTrending>false</trackTrending>
        <type>Number</type>
        <unique>false</unique>
    </fields>
    <fields>
        <fullName>CPAO_CPD_Verifiable_Hours__c</fullName>
        <externalId>false</externalId>
        <label>CPD Verifiable Hours</label>
        <precision>18</precision>
        <required>false</required>
        <scale>0</scale>
        <trackHistory>false</trackHistory>
        <trackTrending>false</trackTrending>
        <type>Number</type>
        <unique>false</unique>
    </fields>
    <fields>
        <fullName>CPAO_Contact__c</fullName>
        <deleteConstraint>SetNull</deleteConstraint>
        <externalId>false</externalId>
        <label>Contact</label>
        <referenceTo>Contact</referenceTo>
        <relationshipName>Plan_of_Action_Items</relationshipName>
        <required>false</required>
        <trackHistory>false</trackHistory>
        <trackTrending>false</trackTrending>
        <type>Lookup</type>
    </fields>
    <fields>
        <fullName>CPAO_Course_Name__c</fullName>
        <externalId>false</externalId>
        <label>Course / Activity Name</label>
        <length>200</length>
        <required>true</required>
        <trackHistory>false</trackHistory>
        <trackTrending>false</trackTrending>
        <type>Text</type>
        <unique>false</unique>
    </fields>
    <fields>
        <fullName>CPAO_End_Date__c</fullName>
        <externalId>false</externalId>
        <label>End Date</label>
        <required>true</required>
        <trackHistory>false</trackHistory>
        <trackTrending>false</trackTrending>
        <type>Date</type>
    </fields>
    <fields>
        <fullName>CPAO_Legacy_System_ID__c</fullName>
        <description>This is legacy system Id. If data came from IMIS Bloor, it starts with BR, for York it is YR, for CGA it is CG and for CAMS it is CM.</description>
        <externalId>true</externalId>
        <inlineHelpText>This is legacy system Id. If data came from IMIS Bloor, it starts with BR, for York it is YR, for CGA it is CG and for CAMS it is CM.</inlineHelpText>
        <label>Legacy System ID</label>
        <length>50</length>
        <required>false</required>
        <trackHistory>false</trackHistory>
        <trackTrending>false</trackTrending>
        <type>Text</type>
        <unique>false</unique>
    </fields>
    <fields>
        <fullName>CPAO_Method__c</fullName>
        <externalId>false</externalId>
        <label>Method</label>
        <required>false</required>
        <trackHistory>false</trackHistory>
        <trackTrending>false</trackTrending>
        <type>Picklist</type>
        <valueSet>
            <restricted>true</restricted>
            <valueSetDefinition>
                <sorted>false</sorted>
                <value>
                    <fullName>Course/Conference/Seminars/Presentation</fullName>
                    <default>false</default>
                    <label>Course/Conference/Seminars/Presentation</label>
                </value>
                <value>
                    <fullName>Casual reading of professional journals/magazines not part of research</fullName>
                    <default>false</default>
                    <label>Casual reading of professional journals/magazines not part of research</label>
                </value>
                <value>
                    <fullName>Formal study leading to a degree/diploma</fullName>
                    <default>false</default>
                    <label>Formal study leading to a degree/diploma</label>
                </value>
                <value>
                    <fullName>Informal self-study of course materials/electronic media/device</fullName>
                    <default>false</default>
                    <label>Informal self-study of course materials/electronic media/device</label>
                </value>
                <value>
                    <fullName>On-the-job training for new software/systems/procedures/techniques/application</fullName>
                    <default>false</default>
                    <label>On-the-job training for new software/systems/procedures/techniques/application</label>
                </value>
                <value>
                    <fullName>Pre-professional re-examination/formal testing</fullName>
                    <default>false</default>
                    <label>Pre-professional re-examination/formal testing</label>
                </value>
                <value>
                    <fullName>Publication of professional articles/papers/academic work</fullName>
                    <default>false</default>
                    <label>Publication of professional articles/papers/academic work</label>
                </value>
                <value>
                    <fullName>Research/Projects resulting in presentations/report/documentation</fullName>
                    <default>false</default>
                    <label>Research/Projects resulting in presentations/report/documentation</label>
                </value>
                <value>
                    <fullName>Research/reading professional literature or journals for specific application in professional role</fullName>
                    <default>false</default>
                    <label>Research/reading professional literature or journals for specific application in professional role</label>
                </value>
                <value>
                    <fullName>Self-study leading to examination/designation</fullName>
                    <default>false</default>
                    <label>Self-study leading to examination/designation</label>
                </value>
                <value>
                    <fullName>Speaker in conference/briefing session/discussion group</fullName>
                    <default>false</default>
                    <label>Speaker in conference/briefing session/discussion group</label>
                </value>
                <value>
                    <fullName>Teaching a course/PD session in an area relevant to professional role</fullName>
                    <default>false</default>
                    <label>Teaching a course/PD session in an area relevant to professional role</label>
                </value>
                <value>
                    <fullName>Participation &amp; Work on technical committees</fullName>
                    <default>false</default>
                    <label>Participation &amp; Work on technical committees</label>
                </value>
                <value>
                    <fullName>Employer-Based In-House Training Session</fullName>
                    <default>false</default>
                    <label>Employer-Based In-House Training Session</label>
                </value>
                <value>
                    <fullName>Writing of technical articles/papers/books</fullName>
                    <default>false</default>
                    <label>Writing of technical articles/papers/books</label>
                </value>
                <value>
                    <fullName>Other</fullName>
                    <default>false</default>
                    <label>Other</label>
                </value>
            </valueSetDefinition>
        </valueSet>
    </fields>
    <fields>
        <fullName>CPAO_Research_Area__c</fullName>
        <externalId>false</externalId>
        <label>Research Area</label>
        <length>100</length>
        <required>true</required>
        <trackHistory>false</trackHistory>
        <trackTrending>false</trackTrending>
        <type>Text</type>
        <unique>false</unique>
    </fields>
    <fields>
        <fullName>CPAO_Start_Date__c</fullName>
        <externalId>false</externalId>
        <label>Start Date</label>
        <required>true</required>
        <trackHistory>false</trackHistory>
        <trackTrending>false</trackTrending>
        <type>Date</type>
    </fields>
    <fields>
        <fullName>CPAO_Type__c</fullName>
        <externalId>false</externalId>
        <label>Type</label>
        <required>false</required>
        <trackHistory>false</trackHistory>
        <trackTrending>false</trackTrending>
        <type>Picklist</type>
        <valueSet>
            <restricted>true</restricted>
            <valueSetDefinition>
                <sorted>false</sorted>
                <value>
                    <fullName>Plan of Action Item</fullName>
                    <default>false</default>
                    <label>Plan of Action Item</label>
                </value>
                <value>
                    <fullName>CPD Audit Log Item</fullName>
                    <default>false</default>
                    <label>CPD Audit Log Item</label>
                </value>
            </valueSetDefinition>
        </valueSet>
    </fields>
    <fields>
        <fullName>CPAO_Verifiable__c</fullName>
        <externalId>false</externalId>
        <label>Verifiable</label>
        <required>true</required>
        <trackHistory>false</trackHistory>
        <trackTrending>false</trackTrending>
        <type>Picklist</type>
        <valueSet>
            <restricted>true</restricted>
            <valueSetDefinition>
                <sorted>false</sorted>
                <value>
                    <fullName>Yes</fullName>
                    <default>false</default>
                    <label>Yes</label>
                </value>
                <value>
                    <fullName>No</fullName>
                    <default>false</default>
                    <label>No</label>
                </value>
            </valueSetDefinition>
        </valueSet>
    </fields>
    <fields>
        <fullName>CPOA_CPD_Hours__c</fullName>
        <externalId>false</externalId>
        <label>CPD Hours</label>
        <precision>18</precision>
        <required>true</required>
        <scale>0</scale>
        <trackHistory>false</trackHistory>
        <trackTrending>false</trackTrending>
        <type>Number</type>
        <unique>false</unique>
    </fields>
    <label>Log Item</label>
    <listViews>
        <fullName>All</fullName>
        <filterScope>Everything</filterScope>
        <label>All</label>
    </listViews>
    <nameField>
        <displayFormat>L-{0000}</displayFormat>
        <label>Plan of Action Items Name</label>
        <trackHistory>false</trackHistory>
        <type>AutoNumber</type>
    </nameField>
    <pluralLabel>Log Items</pluralLabel>
    <searchLayouts/>
    <sharingModel>ControlledByParent</sharingModel>
    <validationRules>
        <fullName>End_Date_Before_Start</fullName>
        <active>true</active>
        <description>End date cannot occur before the start date.</description>
        <errorConditionFormula>CPAO_End_Date__c &lt; CPAO_Start_Date__c</errorConditionFormula>
        <errorDisplayField>CPAO_End_Date__c</errorDisplayField>
        <errorMessage>End date cannot occur before the start date.</errorMessage>
    </validationRules>
    <visibility>Public</visibility>
</CustomObject>
