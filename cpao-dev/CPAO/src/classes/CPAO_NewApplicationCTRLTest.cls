@isTest
private class CPAO_NewApplicationCTRLTest {
	@testSetup
	static void dataInit(){
		
		
		CPAO_TestFactory.automationControlSetup();
		
		List<Account> accounts = CPAO_TestFactory.createAccounts(1);
		Database.insert(accounts);

		List<Contact> contacts = CPAO_TestFactory.createContacts(accounts[0],1);
		Database.insert(contacts);

		User user = CPAO_TestFactory.createCommunityUser(contacts[0].id);
		Database.insert(user);

		List<CPAO_Application__c> applications =CPAO_TestFactory.createPALApplication(1,contacts[0]);
		id recordTypeId = Schema.SObjectType.CPAO_Application__c.getRecordTypeInfosByName().get('MRA Application').getRecordTypeId();
		applications[0].RecordTypeId=recordTypeId;
		applications[0].CPAO_Status__c ='Open';
		applications[0].CPAO_Current_Step__c = 'step1';
		Database.insert(applications);
	}


	@isTest static void callPopup() {
		// Implement test code
		CPAO_NewApplicationCTRL controller = new CPAO_NewApplicationCTRL();
		controller.closePopup();
		controller.openPopup();
	}
	
	@isTest static void callLoad() {
		// Implement test code
		CPAO_NewApplicationCTRL controller = new CPAO_NewApplicationCTRL();
		controller.loadAppTypeOptions();
	}

	@isTest static void callCancel() {
		// Implement test code
		CPAO_NewApplicationCTRL controller = new CPAO_NewApplicationCTRL();
		controller.cancel();
	}	
	
	@isTest static void callStartApplication() {
		// Implement test code
		CPAO_NewApplicationCTRL controller = new CPAO_NewApplicationCTRL();
		controller.startApplication();



	}	


}