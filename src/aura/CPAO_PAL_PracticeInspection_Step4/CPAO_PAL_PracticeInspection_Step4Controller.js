({
    clickOnNextButton : function(component,event,helper){
        var componentNameInEvent=event.getParam("componentName");
        if(componentNameInEvent==component.get('v.nameOfThisComponent')){
            if(event.getParam("nextStepName")=='Exit'){ 
                component.set('v.shouldSaveAndQuit',true);
            }
            var practice_inspection_req= component.find('practice_inspection_req');
            var validForm = component.find('PALForm').reduce(function (validSoFar, inputCmp) {
                var CPAO_Practice_Inspection_Declaration__c=  component.get('v.currentPAL.CPAO_Practice_Inspection_Declaration__c');  
                // Displays error messages for invalid fields
                if(CPAO_Practice_Inspection_Declaration__c==undefined){
                    //  alert('undefined');
                    $A.util.addClass(practice_inspection_req, 'slds-show');
                }else{
                    // $A.util.addClass(error_role_in_PA, 'slds-hide');
                    $A.util.removeClass(practice_inspection_req, 'slds-show');
                    // alert('dfined');
                }
                
                inputCmp.showHelpMessageIfInvalid();
                return validSoFar && inputCmp.get('v.validity').valid &&
                    CPAO_Practice_Inspection_Declaration__c!=undefined;
            }, true);
            
            if(validForm){
                $A.util.removeClass(practice_inspection_req, 'slds-show');
                
                var nextStep=event.getParam('nextStepName');
                //component.set('v.currentPAL.CPAO_Current_Step__c',nextStep);
                
                component.set('v.currentPAL',event.getParam('PALApplication'));
                
                var inspectiondate1 = component.get('v.currentPAL').CPAO_Practice_Inspection_Date_1__c;
                if(inspectiondate1!=undefined){
                    var formattedDate1=inspectiondate1.substring(0,10);
                    component.set('v.currentPAL.CPAO_Practice_Inspection_Date_1__c',formattedDate1);
                }
                var inspectiondate2 = component.get('v.currentPAL').CPAO_Practice_Inspection_Date_2__c;
                if(inspectiondate2!=undefined){
                    var formattedDate2=inspectiondate2.substring(0,10);
                    component.set('v.currentPAL.CPAO_Practice_Inspection_Date_2__c',formattedDate2);
                }
                
                var inspectiondate3 = component.get('v.currentPAL').CPAO_Practice_Inspection_Date_3__c;
                if(inspectiondate3!=undefined){
                    var formattedDate3=inspectiondate3.substring(0,10);
                    component.set('v.currentPAL.CPAO_Practice_Inspection_Date_3__c',formattedDate3);
                }
                
                
                //Clear the conditionally required fields if they are not required anymore
                var declaration =component.get('v.currentPAL.CPAO_Practice_Inspection_Declaration__c');
                var NotYetInspected= component.get('v.NotYetInspected');
                var insideCanada=component.get('v.insideCanada');
                var outsideCanada=component.get('v.outsideCanada');
                
                if(declaration!=NotYetInspected){
                    component.set('v.currentPAL.CPAO_Firm_Professional_Corporation_Pi__c',null);
                }
                
                if(declaration!=NotYetInspected && declaration!=insideCanada&&declaration!=outsideCanada){
                    component.set('v.currentPAL.CPAO_Practice_Inspection_Date_1__c',null);
                    component.set('v.currentPAL.CPAO_Practicing_Office_Name_1__c',null);
                    component.set('v.currentPAL.CPAO_Practicing_Office_Address_1__c',null);
                    component.set('v.currentPAL.CPAO_Practice_Inspection_Date_2__c',null);
                    component.set('v.currentPAL.CPAO_Practicing_Office_Name_2__c',null);
                    component.set('v.currentPAL.CPAO_Practicing_Office_Address_2__c',null);
                    component.set('v.currentPAL.CPAO_Practice_Inspection_Date_3__c',null);
                    component.set('v.currentPAL.CPAO_Practicing_Office_Name_3__c',null);
                    component.set('v.currentPAL.CPAO_Practicing_Office_Address_3__c',null);
                    
                }
                
                
                
                
                // console.log( 'about to add application: '+JSON.stringify(component.get('v.currentPAL')));
                var showSpinnerEvent = component.getEvent("showSpinner");
                showSpinnerEvent.fire();
                helper.upsertPAL(component);
                
                //to do: delete values in date nd in firm name if choice isnt good    	
            }
        }  	   
        
    }
})