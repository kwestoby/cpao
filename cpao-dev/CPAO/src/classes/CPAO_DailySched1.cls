global class CPAO_DailySched1 implements Schedulable {
	global void execute(SchedulableContext sc) {
		//CPAO_PoaComplianceBatch poaComplianceBatch = new CPAO_PoaComplianceBatch();
        database.executebatch(new CPAO_PoaComplianceBatch());

        //CPAO_InfractionAssignBatch infractionAssignBatch = new CPAO_InfractionAssignBatch();
		database.executebatch(new CPAO_InfractionAssignBatch());

		//CPAO_Update_Licence_Status_Batch update_Licence_Status_Batch = new CPAO_Update_Licence_Status_Batch();
		database.executebatch(new CPAO_Update_Licence_Status_Batch());

		//CPAO_updateConsentTypeBatch updateConsentTypeBatch = new CPAO_updateConsentTypeBatch();
		database.executebatch(new CPAO_updateConsentTypeBatch());

		//CPAO_UnmarkPrimaryRelationBatch b = new CPAO_UnmarkPrimaryRelationBatch();
		database.executebatch(new CPAO_UnmarkPrimaryRelationBatch());
	}
}