public with sharing class CPAO_RosterManagement{
    
    public static Map<Id,Account> accountMap = new Map<Id,Account>();
    Public static List<Id> contactlistIds = new List<Id>();
    public static List<Id> ContactIds = new List<Id>();
    public static Set<Contact> confinalSet= new  Set<Contact>();
    
    /* Fetch Users */
    @AuraEnabled 
    public static user getUser(){
        User u = CPAO_UserSelector.getCurrentUser();
        return u;
    } 
    
    
    @AuraEnabled
    public static List<Account> getAccountNames(){
        User u = CPAO_UserSelector.getCurrentUser();
        return CPAO_AccountSelector.getContactSoAccounts(u.ContactId);
    }
    
    @AuraEnabled 
    public static List<Account> getAccountDetails(String idListJSONStr){
        System.debug('idListJSON = ' + idListJSONStr);
        Type idArrType=Type.forName('List<Id>');
        List<Id> ids=(List<Id>) JSON.deserialize(idListJSONStr, idArrType);
        return CPAO_AccountSelector.getAccountToUpdate(new Set<Id>(ids));
    }
    
    /*Roster Payment Type */
    @AuraEnabled
    public static List<CPAO_RosterPaymentType__mdt> queryAllPaymentTypes(){                                                 
        return CPAO_MetadataTypeSelector.getRosterPaymentType();
    }
    
    /*Roster Instruction Screen */
    @AuraEnabled
    public static CPAO_RosterInstructionScreen__mdt queryInstructionScreen(){
        return CPAO_MetadataTypeSelector.getRosterInstruction();
    }
    
    @AuraEnabled
    public static List<Contact> getContactNames(string selectedRosterPaymentType, string selectedAccountIds){
        List<Id> subContactIds = new List<Id>();
        List<Id> itemClassIds = new List<Id>();
        List<Id> contactAppIds = new List<Id>();
        List<Account> accIds= new List<Account>();

        //Integer currentYear = System.Today().year();
        //Date startDate= Date.newInstance(currentYear ,3, 1);
        //Date endDate= Date.newInstance(currentYear+1 ,4, 30);
        
        System.debug('selectedAccountsIds= ' + selectedAccountIds);
        
        Type idArrType=Type.forName('List<Id>');
        List<Id> ids=(List<Id>) JSON.deserialize(selectedAccountIds, idArrType);
        system.debug('idssssssssss'+ids);
        
        User u = CPAO_UserSelector.getCurrentUser();

        for(Account account:CPAO_AccountSelector.getContactSoAccountsWithIds(u.ContactId, ids)){
            accountMap.put(account.OrderApi__Primary_Contact__c, account);
        }
        
        system.debug(' accountMap'+accountMap);
        String payType = '%'+selectedRosterPaymentType+'%';
        system.debug('payType'+payType);
        
        for(CPAO_Application__c appContact : CPAO_ApplicationSelector.getAppsByStatusAndType('Submitted', payType)){
            contactAppIds.add(appContact.CPAO_Contact__c);
        }
        system.debug('contactAppIds'+contactAppIds);
            
        // for(Contact conObj : [Select name, Id, CPAO_Status_of_Completion__c,CPAO_CPA_Contact_Type__c, CPAO_CPA_Id_Display__c from Contact where Id =: contactAppIds]){
        //     system.debug('conObj'+conObj);
        //     contactlistIds.add(conObj.Id);
        // }
            
           
        //for(OrderApi__Item_Class__c itemClassObj : CPAO_FontevaObjectSelector.getPayTypeItemClass(payType)){
        //    itemClassIds.add(itemClassObj.Id);
        //}
        
        //for(OrderApi__Subscription__c orderSub : [Select Id, OrderApi__Contact__c,OrderApi__Activated_Date__c,OrderApi__Expired_Date__c, OrderApi__Status__c, name, OrderApi__Item_Class__c  from OrderApi__Subscription__c where OrderApi__Status__c =: 'Active' and OrderApi__Item_Class__c =: itemClassIds and OrderApi__Contact__c=: contactAppIds]){
        //    subContactIds.add(orderSub.OrderApi__Contact__c);
        //}
        //system.debug('subContactIds'+subContactIds);
        
        for(CPAO_Account_Contact_Relationship__c accConRelation : 
        CPAO_AccountContactRelationSelector.getPrimaryRelationshipsByContactId(new Set<Id>(contactAppIds))){
            //[Select CPAO_Account__c, CPAO_Contact__c, CPAO_Primary_Employer__c from CPAO_Account_Contact_Relationship__c 
            //where CPAO_Contact__c =: contactAppIds and CPAO_Primary_Employer__c =:'Yes'])
            Account acc = accountMap.get(accConRelation.CPAO_Contact__c);
            system.debug('accConRelation.CPAO_Contact__c'+accConRelation.CPAO_Contact__c);
            if(acc != null){
                ContactIds.add(accConRelation.CPAO_Contact__c);
                system.debug('accConRelationaccConRelation '+accConRelation );

            }
        }
        
        
        //List<Contact> conList = [Select Id,Subscription_Status_of_Completion_AMD__c,Subscription_Status_of_Completion_APF__c,
        //Subscription_Status_of_Completion_PAL__c, name, CPAO_CPA_Id_Display__c  from Contact where Id =: ContactIds];
        
        //system.debug('conList '+conList );
        return CPAO_ContactSelector.getContactToUpdate(new Set<Id>(ContactIds));
        
    }
    
    @AuraEnabled
    public static list<contact> searchmethod(string searchKeyword){
        system.debug('--enter --'+searchKeyword);
        String searchKey=searchKeyword + '%';
        system.debug('--enter =--'+searchKey);
        list<Id> conIds =new list<Id>();
        list<Id> contactmembersIds =new list<Id>();

        list<contact> ConList=[select id,name,CPAO_Primary_Employer__r.Name, Subscription_Status_of_Completion_AMD__c,Subscription_Status_of_Completion_APF__c,Subscription_Status_of_Completion_PAL__c, CPAO_CPA_Id_Display__c from contact where CPAO_CPA_Id_Display__c like: searchKey and CPAO_CPA_Contact_Type__c =:'Member'];
        for(contact con:ConList){
            system.debug('connnnnnn'+con.Name);
            conIds.add(con.Id);
        }
        
        for(CPAO_Account_Contact_Relationship__c accConRelation : [Select CPAO_Account__c, CPAO_Contact__c, CPAO_Primary_Employer__c from CPAO_Account_Contact_Relationship__c where CPAO_Contact__c =: conIds]){
            contactmembersIds.add(accConRelation.CPAO_Contact__c);
        }
        
        List<Contact> condisplay = [Select Id, name, CPAO_Primary_Employer__r.Name, Subscription_Status_of_Completion_AMD__c,Subscription_Status_of_Completion_APF__c,Subscription_Status_of_Completion_PAL__c, CPAO_CPA_Id_Display__c  from Contact where Id =: contactmembersIds];
        system.debug('condisplay '+condisplay );
            
        return condisplay;
    }
    
    public static List<Contact> cons = new List<Contact>();
       
    @AuraEnabled
    public static List<OrderApi__Receipt__c> savePayment (List<OrderApi__Receipt__c> acc){
        upsert acc;
        return acc;
    }
    

    @AuraEnabled 
    public static List<Contact> saveMarkedContacts(String idListJSONStr){
        Set<Contact> conSet = new Set<Contact>();
        System.debug('idListJSON = ' + idListJSONStr);
        Type idArrType=Type.forName('List<Id>');
        List<Contact> conList = new List<Contact>();
        List<Id> ids=(List<Id>) JSON.deserialize(idListJSONStr, idArrType);
        List<Contact> condetailsObj = [Select Id, name, Subscription_Status_of_Completion_AMD__c,Subscription_Status_of_Completion_APF__c,Subscription_Status_of_Completion_PAL__c,CPAO_CPA_Contact_Type__c, CPAO_CPA_Id_Display__c from Contact where id in: ids ];
        for(integer i=0; i<condetailsObj.size(); i++){
            conSet.add(condetailsObj[i]);
        }

        conList.addAll(conSet);
        system.debug('conListtttt'+conList);

        return conList;
    }
   
 
}