({
	doInit : function(component, event, helper) {
		console.log('doinit');
		var action = component.get("c.getUserBreaches");
	   	action.setCallback(this, function(response){
		      var state = response.getState();
		      if (state === "SUCCESS") {
		      	//console.log(JSON.parse(response.getReturnValue()));
		      	var currentInfractions=response.getReturnValue().currentInfractions;
                  var currentFiles=response.getReturnValue().currentFiles;
		      
                  console.log('currentFiles');
                  console.log(currentFiles);
		      	component.set('v.listOfInfractions',currentInfractions);
                component.set('v.filesName',currentFiles);
                  
		    	
		      }else{
		      	console.log('error while doinit');
		      }
	    });
	  
	    $A.enqueueAction(action);
	},
    doSave: function(component, event, helper){
        var infractions = component.get("v.listOfInfractions");
        var errors = [];
        var isError = false;
        
        for(var i=0;i<infractions.length; i++){
            var error = {};
            if(infractions[i].CPAO_Marked_as_Resolved__c == false){
                error.marked = "We noticed that you have an unresolved breach. Please confirm that you have resolved your breach(es) before proceeding to the next page."
                isError = true;
            }
            errors.push(error);
        }
        component.set('v.errors',errors);

        if(isError == true){
            helper.methodFailure(component, event, helper, "There seems to be an Error, please try again.", "Error");
          	return false;  
        } 
        
        var action=component.get("c.updateUserBreaches");
        var files = component.get("v.filesName");
        console.log(files);
        
        for(var i=0;i<files.length;i++){
            if(files[i] == undefined){
                files[i] = [];
                console.log('changed');
            }
        }
        
        action.setParams({ 
            infractionList: infractions,
            files: JSON.stringify(files)                            
        });
     
        
        //save
        //send infractions object to apex and update infractions
        $A.enqueueAction(action);

        //console.log(infractions);
        
        return true;
    },
    doExit: function(component, event, helper){
        var infractions = component.get("v.listOfInfractions");

        var action=component.get("c.updateUserBreaches");
        
        action.setParams({ infractionList: infractions});

        $A.enqueueAction(action);

        console.log(infractions);

    },
    handleFilesChange: function(component, event, helper){
        
        var fileNames = component.get('v.filesName');
        var key = event.getSource().get('v.id');
        
     	//list of files submitted for one infraction
        var fileList = event.getSource().get("v.files");
        
        var fileArray = [];
        
        
		//var self = this;
        
        for(var i=0; i<fileList.length;i++){
            
            (function(file){
                var newFile = {};
                newFile.title = file.name;
            	newFile.filetype = file.type;
                
                
                var fr = new FileReader();
                
                fr.onload = function(e){
                    var fileContents = e.target.result;
                    var base64Mark = 'base64,';
                    var dataStart = fileContents.indexOf(base64Mark) + base64Mark.length;
                    fileContents = fileContents.substring(dataStart);
                    newFile.filecontent =  encodeURIComponent(fileContents);
                    fileArray.push(newFile);
                    //console.log(i);
                    component.set('v.filesName',fileNames);

                }
                fr.readAsDataURL(file);
                
            })(fileList[i]);
            
        }
        
   
        fileNames[key] = fileArray;
        //console.log(fileNames[key]);

        
    }
})