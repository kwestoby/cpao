public without sharing class CPAO_EmploymentPageReinstatementCTRL {
    
    public CPAO_EmploymentPageReinstatementCTRL() {

    }

    public PageReference save(){
        String stepId = Framework.PageUtils.getParam('id');
        String redirectURL = '';
        joinApi.JoinUtils joinUtil;
        if(!Test.isRunningTest()){
            joinUtil = new joinApi.JoinUtils(stepId);
            redirectURL = joinUtil.navStepsMap.get('Next');
        } else {
            redirectURL = '/test';
        }
            

        List<CPAO_Application__c> currentReinstatementAppliation = 
            CPAO_ApplicationSelector.getUserApplicationsByRecordTypeNameAndStatus('Open', CPAO_ApplicationHandler.REINSTATEMENT);
        if(!currentReinstatementAppliation.isEmpty()){
            String declarationFieldGroupId = Label.Good_Character_Declaration_reinstatement_field_group;
            List<CPAO_Declaration__c> currentApplicationDec = 
                CPAO_DeclarationSelector.getApplicationDeclaration(currentReinstatementAppliation[0].Id, CPAO_DeclarationSelector.GOOD_CHARACTER);

            if(!currentApplicationDec.isEmpty() && declarationFieldGroupId != null){
                redirectURL = redirectURL + '&' + declarationFieldGroupId + '_SObjId=' + currentApplicationDec[0].Id;
            }
        }
        
        // to pass parameters to the next step; harcoded ids to test
        //redirectURL = redirectURL + '&a0O3B000000dZWVUA2_SObjId=a2T3B0000002MQCUA2';
        JSONGenerator gen = JSON.createGenerator(true);
        gen.writeStartObject();
        gen.writeStringField('stepId', stepId);
        gen.writeStringField('redirectURL', redirectURL);
        gen.writeEndObject();
        if(!Test.isRunningTest()){
            joinUtil.setRedirectURLCookie(gen.getAsString());
        }
        return new PageReference(redirectURL);

    }

    public PageReference previousStep() {
        if(Test.isRunningTest()){
            return null;
        }
        String stepId = Framework.PageUtils.getParam('id');
        joinApi.JoinUtils joinUtil = new joinApi.JoinUtils(stepId);
        return new PageReference(joinUtil.getPreviousStep('{}'));
    }

    public PageReference cancelJP() {
        String stepId = Framework.PageUtils.getParam('id');
        if(!Test.isRunningTest()){
            joinApi.JoinUtils joinUtil = new joinApi.JoinUtils(stepId);
            joinUtil.deleteCookies();
        }            
        return new PageReference('/');

    }
}