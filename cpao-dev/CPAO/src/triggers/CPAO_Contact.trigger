trigger CPAO_Contact on Contact (before insert, after insert, before update, after update) {

	CPAO_Automation_Control__c control = CPAO_Automation_Control__c.getOrgDefaults();
	if(!control.Contact__c){
		return;
	}
	
	if(Trigger.isBefore && Trigger.isInsert){
		CPAO_ContactHandler.beforeInsertFilters(Trigger.New);
      
	}
	if(Trigger.isBefore && Trigger.isUpdate){
		CPAO_ContactHandler.beforeUpdateFilters(Trigger.New, Trigger.oldMap);
        CPAO_MilestoneHelper.historyTracker(Trigger.New);
	}

	if(Trigger.isAfter && Trigger.isInsert){
		CPAO_ContactHandler.afterInsertFilters(Trigger.New);
	}
	if(Trigger.isAfter && Trigger.isUpdate){
		CPAO_ContactHandler.afterUpdateFilters(Trigger.New, Trigger.oldMap);
	}

	if(Trigger.isAfter && Trigger.isDelete){
        CPAO_ContactHandler.afterDeleteFilters(Trigger.oldMap);
    }
}