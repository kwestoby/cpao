@isTest
private class CPAO_SalesOrderHandlerTest {

	@TestSetup
	private static void setupData() {
		CPAO_TestFactory.automationControlSetup();
		Account acc = CPAO_TestFactory.createAccounts(1) [0];
		insert acc;
		Contact con = CPAO_TestFactory.createContacts(acc, 1) [0];
		insert con;
		CPAO_Application__c app = CPAO_TestFactory.createReadmissionApplication(1, con) [0];
		insert app;
	}

	@isTest
	private static void testAfterInsert() {
		Contact con = [SELECT Id FROM Contact LIMIT 1];
		CPAO_Application__c app = [SELECT Id, CPAO_Status__c FROM CPAO_Application__c LIMIT 1];
		System.assertNotEquals('Submitted', app.CPAO_Status__c);
		Test.startTest();
		OrderApi__Sales_Order__c sOrder = new OrderApi__Sales_Order__c(OrderApi__Entity__c = 'Contact',
		                                                               OrderApi__Contact__c = con.Id,
																	   CPAO_Application__c=app.Id,
		                                                               OrderApi__Posting_Entity__c = 'Receipt',
		                                                               OrderApi__Posted_Date__c = System.today() - 5,
		                                                               OrderApi__Posting_Status__c = 'Posted',
		                                                               OrderApi__Status__c = 'Closed');
		insert sOrder;
		Test.stopTest();
		app = [SELECT Id, CPAO_Status__c FROM CPAO_Application__c LIMIT 1];
		System.assertEquals('Submitted', app.CPAO_Status__c);
	}

	@isTest
	private static void testAfterUpdate() {
		Contact con = [SELECT Id FROM Contact LIMIT 1];
		CPAO_Application__c app = [SELECT Id, CPAO_Status__c FROM CPAO_Application__c LIMIT 1];
		System.assertNotEquals('Submitted', app.CPAO_Status__c);
		OrderApi__Sales_Order__c sOrder = new OrderApi__Sales_Order__c(OrderApi__Entity__c = 'Contact',
		                                                               OrderApi__Contact__c = con.Id,
																	   CPAO_Application__c=app.Id,
		                                                               OrderApi__Posting_Entity__c = 'Receipt',
		                                                               OrderApi__Posted_Date__c = System.today() - 5,
		                                                               OrderApi__Posting_Status__c = 'Posted',
		                                                               OrderApi__Status__c = 'Open');
		insert sOrder;
		Test.startTest();
		sOrder.OrderApi__Status__c = 'Closed';
		update sOrder;
		sOrder = [SELECT Id, OrderApi__Status__c FROM OrderApi__Sales_Order__c LIMIT 1];
		System.debug(sOrder);
		Test.stopTest();
		app = [SELECT Id, CPAO_Status__c FROM CPAO_Application__c LIMIT 1];
		System.assertEquals('Submitted', app.CPAO_Status__c);
	}
}