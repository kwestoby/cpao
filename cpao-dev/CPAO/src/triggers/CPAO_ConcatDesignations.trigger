//This Trigger will fire after insert, update, delete and undelete
Trigger CPAO_ConcatDesignations on CPAO_Designation_Award__c (after insert, after update, after delete){

    //If the event is insert, this list takes New Values or else it takes old values
    List<CPAO_Designation_Award__c> DesignationList = (Trigger.isInsert || Trigger.isUpdate) ? Trigger.new : Trigger.old;
    
    //To store associated Contact Ids
    Set<Id> DesignationconIds = new Set<Id>();

    //Loop through the Records to store the contact Id values 
    for (CPAO_Designation_Award__c Desig_Award : DesignationList) {
            DesignationconIds.add(Desig_Award.Contact__C);
    }
    
    CPAO_ConcatDesignations.concatDesignations(DesignationconIds, trigger.oldmap, trigger.newmap);

    /*//To store associated contact records
    List<contact> cont = new List<contact>();
    cont = [Select Id, Name, Designations__c, (Select Id, CPAO_Country__c, CPAO_Accounting_Designations_Awards__c from Designations_Awards__r) from contact where Id in :DesignationconIds];
    
    //To store all Fellow records
    Set<Id> setFellow = new Set<Id>();
    for(Contact con: cont){
        for(CPAO_Designation_Award__c s: con.Designations_Awards__r){
            if(s.CPAO_Accounting_Designations_Awards__c.equalsIgnoreCase('fellow') && s.CPAO_Country__c == 'Canada'){
                setFellow.add(con.Id);
            }
        }
    }
    
    //Loop through all the associated records to concatenate designations to contact record
    for(contact o : cont){
        o.Designations__c = '';
        if(o.Designations_Awards__r != null && o.Designations_Awards__r.size()>0){
            for(CPAO_Designation_Award__c s: o.Designations_Awards__r){
                if(s.CPAO_Country__c == 'Canada'){
                    if(!o.Designations__c.containsIgnoreCase(s.CPAO_Accounting_Designations_Awards__c)){ 
                        o.Designations__c += ((setFellow.contains(o.Id) && !s.CPAO_Accounting_Designations_Awards__c.equalsIgnoreCase('fellow')) ? 'F' + s.CPAO_Accounting_Designations_Awards__c + ';' : s.CPAO_Accounting_Designations_Awards__c + ';');
                    }
                }else
                {
                     if(o.Designations__c.contains(s.CPAO_Accounting_Designations_Awards__c) && Trigger.oldMap.get(s.Id).CPAO_Country__c != Trigger.newMap.get(s.Id).CPAO_Country__c){
                         o.Designations__c = o.Designations__c.remove(s.CPAO_Accounting_Designations_Awards__c);
                     }
                }    
            }    
        }
        //else
        //o.Designations__c = null; 
    }
    if(cont.size()>0)
    update cont;*/

}