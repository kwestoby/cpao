@isTest
public class CPAO_ContactServicesTest {
public static String MEMBER_CONTACT_TYPE = 'Member';
    public static String ACTIVE_STATUS = 'Member Active';
    public static String LSO_BADGE_TYPE='LSO';
    public static String APP_CANCELLED = 'Cancelled';
	public static List<Account> factoryAccounts;
  	public static List<Contact> factoryContacts;
    public static List<Account>  dataInitializeBadges(Integer numberOfAccounts,Integer numberOfContacts ){
		List<Account> accountWithoutPrimaryContact = CPAO_TestFactory.createAccounts(numberOfAccounts);
	  	List<Account> factoryAccounts=CPAO_TestFactory.createAccountsPrimaryContact(accountWithoutPrimaryContact);
		OrderApi__Badge_Type__c primaryContactBadgeType=new OrderApi__Badge_Type__c(Name='Primary Contact');
		insert primaryContactBadgeType;
       factoryContacts= CPAO_TestFactory.createContacts(factoryAccounts[0],numberOfContacts);
    	//insert factoryContacts;
		return factoryAccounts;
	}
    @isTest
    static void InsertApplication() {
    //dataInitialize(1,1);
    List<Account> accountsToInsert= dataInitializeBadges(1,1);
    list <OrderApi__Badge_Type__c> badgetype = [SELECT Id FROM OrderApi__Badge_Type__c WHERE Name = 'Primary Contact' LIMIT 1];
        
    OrderApi__Badge__c	 badge1 = new OrderApi__Badge__c();
 	badge1.OrderApi__Contact__c = factoryContacts[0].Id;
 	badge1.OrderApi__Badge_Type__c = 'a0k3B000000z7Hi';
    insert badge1;
    }
}