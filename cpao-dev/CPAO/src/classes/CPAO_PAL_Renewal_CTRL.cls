public without sharing class CPAO_PAL_Renewal_CTRL {

	public static User LOGGED_USER =  CPAO_UserSelector.getCurrentUser();
   // public static OrderApi__Subscription__c USER_PAL_SUBSCRIPTION = getPALSubscription();
	//public static OrderApi__Subscription_Plan__c PAL_SUBSC_PLAN = getPALSubscriptionPlan();
	public static string PAL_SUBSC_TYPE_NAME='Public Accounting Licence';
    public static string PAL_RENEWAL_RECORD_TYPE_NAME='CPAO_PAL_Renewal';
    public static List<RecordType> PAL_RENEWAL_RECORD_TYPE=CPAO_ApplicationSelector.getRecordType(PAL_RENEWAL_RECORD_TYPE_NAME);
    public static string  ACTIVE='Active';
    public static string OPEN='Open';
    public static string COMPLETED='Completed';
    public static string  EMPLOYEE='Employee';
    public static string  SHAREHOLDER='Shareholder';
    public static String FIRM='Firm';
    public static String PROFESSIONAL_COORPORATION='Professional Corporation';
    public static final String AMD_CPD_VAL = 'CPAO_AMD_CPD';
    public static final String COMPLY_WITH_FULL_CPD_REQ = 'Comply with full CPD requirements';
    public static final String COMPLY_WITH_OTHER_BODY_REQ = 'Comply with other professional body requirements';
    public static final String ACTIVE_LICENCE='Active Licence with Other Body';
    public static final String APPROVE ='Approve';
    public static final String PAL_APPLICATION = 'CPAO_PAL_Application';

 
/*
    @AuraEnabled
    public static OrderApi__Subscription__c getPALSubscription() {
        return CPAO_FontevaObjectSelector.getContactSubscription(PAL_SUBSC_TYPE_NAME, LOGGED_USER.ContactId)[0];
        //return [SELECT Id,OrderApi__Contact__c,OrderApi__Subscription_Plan__c 
        //	    FROM OrderApi__Subscription__c
        //	    WHERE OrderApi__Subscription_Plan__c='a1N3B000004o2dhUAA' AND OrderApi__Contact__c=:LOGGED_USER.ContactId ];
    }

    @AuraEnabled
    public static OrderApi__Subscription_Plan__c getPALSubscriptionPlan() {
        List<OrderApi__Subscription_Plan__c> pal_subscription_plan = 
        CPAO_FontevaObjectSelector.getSubscriptionPlan(PAL_SUBSC_TYPE_NAME); 
        return pal_subscription_plan[0];
    }
*/

	@AuraEnabled
    public static initWrapper getCurrentPALRenewalApplication() {
        initWrapper response= new initWrapper();
         List<CPAO_Application__c> palRenewalApplications= CPAO_ApplicationSelector.getUserApplicationsByRecordTypeNameAndStatus(OPEN,PAL_RENEWAL_RECORD_TYPE_NAME);
	    if(!palRenewalApplications.isEmpty()){
	    	response.currentRenewal= palRenewalApplications[0];
	    }else{
	    	CPAO_Application__c newPalRenewal = new CPAO_Application__c();
            newPalRenewal.CPAO_Contact__c = LOGGED_USER.ContactId;
            newPalRenewal.RecordTypeId = PAL_RENEWAL_RECORD_TYPE[0].Id;
            newPalRenewal.CPAO_Status__c = OPEN;
            newPalRenewal.CPAO_Current_Step__c = 'step1';
            insert newPalRenewal;
            response.currentRenewal = newPalRenewal;
        }
        response.associatedWithCPAOFirm=isAssociatedWithCPAOFirm();
        response.metCPD_requirements=metCPD_requirements();
        response.metPracticeInspectionRequirements=metPracticeInspectionRequirements();
        response.countryOptions=getCountryOptions();
        response.accountingBodyOptions=getAccountingBodiesOptions();
        response.needToMeetProfessionalRequirements=needToMeetProfessionalRequirements();
        return response;
	   
	}

    @AuraEnabled
    public static CPAO_Application__c upsertPalApplication(CPAO_Application__c PALRenewalToAdd) {
        
        upsert PALRenewalToAdd;
        return PALRenewalToAdd;
        
    }

    @AuraEnabled
    //This method retrieves all the possibles picklist values for this field: CPAO_Country__c in
    //CPAO_Application__c object.
    //Those values will be populated in a form so that the user could choose from them
    public static  List<String>  getCountryOptions(){
        List<String> listOfCountryOptions = new List<String>();
        Schema.DescribeFieldResult fieldResult = CPAO_Application__c.CPAO_Country__c.getDescribe();
        List<Schema.PicklistEntry> pickListEntries = fieldResult.getPicklistValues();
        for (Schema.Picklistentry picklistEntry : pickListEntries){
            if(picklistEntry.isActive()){
                listOfCountryOptions.add(pickListEntry.getLabel());
            }
        }
        return listOfCountryOptions;
    }

     @AuraEnabled
    //This method retrieves all the possibles picklist values for this field: CPAO_Accounting_Body__c in
    //CPAO_Application__c object.
    //Those values will be populated in a form so that the user could choose from them
    public static  List<String>  getAccountingBodiesOptions(){
        List<String> listOfBodiesOptions = new List<String>();
        Schema.DescribeFieldResult fieldResult = CPAO_Application__c.CPAO_Accounting_Body__c.getDescribe();
        List<Schema.PicklistEntry> pickListEntries = fieldResult.getPicklistValues();
        for (Schema.Picklistentry picklistEntry : pickListEntries){
            if(picklistEntry.isActive()){
                listOfBodiesOptions.add(pickListEntry.getLabel());
            }
        }
        return listOfBodiesOptions;
    }
    
    public static Boolean isAssociatedWithCPAOFirm() {

        Boolean returnedValue=false;
        List<CPAO_Account_Contact_Relationship__c> accountContactRelationships= CPAO_AccountContactRelationSelector.getRelationshipsByContactIdAndStatus(LOGGED_USER.ContactId,ACTIVE);
        for(CPAO_Account_Contact_Relationship__c relationship: accountContactRelationships){
            if(relationship.CPAO_Relationship_to_Account__c==EMPLOYEE && relationship.CPAO_Account__r.CPAO_Account_Type__c==FIRM && relationship.CPAO_Account__r.CPAO_Account_Status__c==ACTIVE ){
                returnedValue=true;
                return returnedValue;
            }else if(relationship.CPAO_Relationship_to_Account__c==SHAREHOLDER  && relationship.CPAO_Account__r.CPAO_Account_Type__c==FIRM && relationship.CPAO_Account__r.CPAO_Account_Status__c==ACTIVE 
                    && relationship.CPAO_Account__r.CPAO_Corporate_Structure__c== PROFESSIONAL_COORPORATION){
                returnedValue=true;
                return returnedValue;

            }else{
                returnedValue=false;
            }
        }
        return returnedValue;
    }

    public static  Boolean metCPD_requirements() {
        Integer currentYear = Date.today().year();
        Set<Integer>setOfYears=new Set<Integer>{currentYear-3,currentYear-2,currentYear-1}; 
        Boolean returnedValue=false;
        List<CPAO_Application__c> previousAMD_applications= CPAO_ApplicationSelector.getUserApplications(AMD_CPD_VAL,setOfYears,COMPLETED);
        if(previousAMD_applications.size()<3){
            return false;
        }
        for(CPAO_Application__c application: previousAMD_applications){
            if(application.CPAO_Declaration__c==COMPLY_WITH_FULL_CPD_REQ || application.CPAO_Declaration__c==COMPLY_WITH_OTHER_BODY_REQ){
                returnedValue=true;
            }else{
                return false;
            }
          
        }
       return returnedValue;
    }

    public static  Boolean needToMeetProfessionalRequirements() {
        Boolean returnedValue=false;
        Integer currentYear = Date.today().year()-5;
        Integer currentMonth= Date.today().month();
        Integer currentDay=  Date.today().day();           
        Date currentDateMinus5Years=date.newinstance(currentYear,currentMonth,currentDay);
        List<CPAO_Application__c> userPalApps=CPAO_ApplicationSelector.getApplicationByRecordTypeAndDecisionDate(PAL_APPLICATION,currentDateMinus5Years,ACTIVE_LICENCE);
        if(!userPalApps.isEmpty()){
            returnedValue=false;
        }else{
            returnedValue=true;
        }
        return returnedValue;
    }


    public static  Boolean metPracticeInspectionRequirements() {
        Boolean returnedValue=false;
        List<Contact> currentContact= CPAO_ContactSelector.getContactRecords(new Set<Id>{LOGGED_USER.ContactId});
        if(!currentContact.isEmpty() && currentContact[0].CPAO_Practice_Inspector__c){
            return true;
        }else{
            Integer currentYear = Date.today().year()-5;
            Integer currentMonth= Date.today().month();
            Integer currentDay=  Date.today().day();           
            Date currentDateMinus5Years=date.newinstance(currentYear,currentMonth,currentDay);
            List<CPAO_Account_Contact_Relationship__c> employersList= CPAO_AccountContactRelationSelector.getRelationshipsByDateAndAccountType(LOGGED_USER.ContactId,EMPLOYEE,currentDateMinus5Years, FIRM);
            if(!employersList.isEmpty()){
                Set<Id> employersId= new Set<Id>();
                Map<Id,List<CPAO_Practice_Inspection__c>> accountAndAssociatedInspections = new Map<Id,List<CPAO_Practice_Inspection__c>> ();
                for (CPAO_Account_Contact_Relationship__c employer:employersList ){
                    employersId.add(employer.CPAO_Account__c);
                    accountAndAssociatedInspections.put(employer.CPAO_Account__c, new List<CPAO_Practice_Inspection__c>());
                }
                List<CPAO_Practice_Inspection__c> employersPracticeInspections = CPAO_PracticeInspectionSelector.getInspectionByReportDate(employersId,currentDateMinus5Years);
                if(!employersPracticeInspections.isEmpty()){
                    for(CPAO_Practice_Inspection__c inspection:employersPracticeInspections){
                        accountAndAssociatedInspections.get(inspection.CPAO_ID__c).add(inspection);
                    }
                }
               for(Id employerId:employersId){
                    if( accountAndAssociatedInspections.get(employerId).size()>0){
                        returnedValue=true;
                    }else{
                        return false;
                    }
                }
               
            }
        } 
       return returnedValue;
    }

    @AuraEnabled
    public static CPAO_PAL_Application_CTRL.FileWrapper saveTheFile(Id parentId, String fileName, String base64Data, String contentType, String fileTitle, String idFileToReplace) {
        return CPAO_PAL_Application_CTRL.saveTheFile(parentId,fileName,base64Data,contentType,fileTitle,idFileToReplace) ;
    }

    @AuraEnabled
    public static Boolean deletePALApplicationFiles(List<Id> filesIdToDelete) {
        return CPAO_PAL_Application_CTRL.deletePALApplicationFiles(filesIdToDelete);
    }

     @AuraEnabled
    public static ReviewPageWrapper loadReviewInfo() {
        ReviewPageWrapper results = new ReviewPageWrapper();
       
        User currentUser = LOGGED_USER; // CPAO_UserSelector.getCurrentUser();
        results.userContact = CPAO_ContactSelector.getContactToUpdate(new Set<Id> { currentUser.ContactId }) [0];
        CPAO_PAL_Renewal_Screen__mdt renewalAttestation=CPAO_MetadataTypeSelector.getPALRenewalAttestation();
        if(renewalAttestation!=null){
           results.PAL_attestation=renewalAttestation.CPAO_PAL_Renewal_Attestation__c;

        }
        return results;
        
    }


    public class initWrapper {
        @AuraEnabled public CPAO_Application__c currentRenewal { get; set; }
        @AuraEnabled public Boolean associatedWithCPAOFirm { get; set; }
        @AuraEnabled public  Boolean metCPD_requirements { get; set; }
        @AuraEnabled public  Boolean metPracticeInspectionRequirements { get; set; }
        @AuraEnabled public Boolean needToMeetProfessionalRequirements { get; set; }
        @AuraEnabled public List<String> countryOptions { get; set; }
        @AuraEnabled public List<String> accountingBodyOptions { get; set; }   
    }

    public class ReviewPageWrapper {
    
    @AuraEnabled public Contact userContact { get; set; }
    @AuraEnabled public String PAL_attestation{ get; set; }
    }


}