<?xml version="1.0" encoding="UTF-8"?>
<CustomMetadata xmlns="http://soap.sforce.com/2006/04/metadata" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xmlns:xsd="http://www.w3.org/2001/XMLSchema">
    <label>Bank Transfer</label>
    <protected>false</protected>
    <values>
        <field>CPAO_Description__c</field>
        <value xsi:type="xsd:string">Payment must be received on or before the due date. If you are paying by bank transfer, please follow the instructions below:

1. Send payment to the Canadian Imperial Bank of Commerce, 2 Bloor St. West, Toronto, ON, Canada, M4W 3E2. Bank Transit number 00502, bank account number 48-00419.  The Swift Code for U.S. remittances is PNBPUS3NNYC ABA 026005092 and for worldwide remittances is CIBCCATT.

2. Please include your full name and CPA Ontario ID.

Note: If you are currently living in another country, there is an extra $56.50 processing fee ($50 plus $6.50 HST) that will apply for this option. Therefore, please ensure you have sufﬁcient funds to cover the full dues and processing charge.</value>
    </values>
    <values>
        <field>CPAO_Order__c</field>
        <value xsi:type="xsd:double">1.0</value>
    </values>
    <values>
        <field>CPAO_Type__c</field>
        <value xsi:type="xsd:string">Offline Payment</value>
    </values>
</CustomMetadata>
