public without sharing class CPAO_SalesOrderHandler {
	
	public static void afterInsertFilter(List<OrderApi__Sales_Order__c> newSalesOrders) {
		List<OrderApi__Sales_Order__c> paidSalesOrders = new List<OrderApi__Sales_Order__c>();
		for(OrderApi__Sales_Order__c so:newSalesOrders){
			if(so.OrderApi__Status__c == 'Closed' && so.CPAO_Application__c != null){
				paidSalesOrders.add(so);
			}
		}

		if(!paidSalesOrders.isEmpty()){
			applicationRecordType(paidSalesOrders);
		}

	}

	public static void afterUpdateFilter(List<OrderApi__Sales_Order__c> newSalesOrders, Map<Id, OrderApi__Sales_Order__c> oldMap) {
		List<OrderApi__Sales_Order__c> paidSalesOrders = new List<OrderApi__Sales_Order__c>();
		for(OrderApi__Sales_Order__c so:newSalesOrders){
			if(so.OrderApi__Status__c == 'Closed' && oldMap.get(so.Id).OrderApi__Status__c != 'Closed' && so.CPAO_Application__c != null){
				paidSalesOrders.add(so);
			}
		}

		if(!paidSalesOrders.isEmpty()){
			applicationRecordType(paidSalesOrders);
		}
	}

	public static void applicationRecordType(List<OrderApi__Sales_Order__c> newSalesOrders) {
		//Id readmissionRecortdTypeId = CPAO_ApplicationHandler.getApplicationRecordTypeId(CPAO_ApplicationHandler.READMISSION_RECORDTYPENAME);
		List<OrderApi__Sales_Order__c> salesOrdersWithReadmissionApps = 
			CPAO_FontevaObjectSelector.getSalesOrdersWithSpecifiedAppRecordTypes(newSalesOrders, 'Readmission');

		if(!salesOrdersWithReadmissionApps.isEmpty()){
			CPAO_SalesOrderService.markApplicationsSubmitted(salesOrdersWithReadmissionApps);
		}
	}
}