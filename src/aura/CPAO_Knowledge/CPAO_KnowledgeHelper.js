({
	getKnowledgeArticles: function (component) {
		var action = component.get("c.getKnowledgeArticles");
		action.setParams({
			query: JSON.stringify({
				searchKey: component.get("v.searchKey"),
				pageSize: component.get("v.pageSize"),
				pageNumber: component.get("v.pageNumber"),
				sortBy: component.get("v.sortBy"),
				sortOrder: component.get("v.sortOrder")
			})
		});
		action.setCallback(this, function(response) {
			var state = response.getState();
			if(state==="SUCCESS") {
				component.set("v.articles", response.getReturnValue().articles);
				component.set("v.totalRecords", response.getReturnValue().totalRecords);
			} else {
				console.log("Error: "+response.getError()[0].message);
			}
			component.set("v.isLoading", false);
		});
		$A.enqueueAction(action);
	}
})