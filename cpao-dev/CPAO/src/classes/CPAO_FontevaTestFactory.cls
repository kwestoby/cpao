@isTest
public class CPAO_FontevaTestFactory {
	
    static Boolean isInstalled = false;
    public static OrderApi__Subscription_Plan__c subPlan;

      public static void install() {
        if (!isInstalled) {
            Framework.Installer.install();
            PagesApi.PackageScripts.install();
            OrderApi.PackageScripts.install();
            EventApi.PackageScripts.install();
            //PackageScripts.install();
            isInstalled = true;
        }
    }

    public static OrderApi__Business_Group__c createBusinessGroup() {
        OrderApi__Business_Group__c bg = new OrderApi__Business_Group__c();
        bg.Name = 'Test Business Group';
        bg.OrderApi__Type__c = 'Department';
        bg.OrderApi__Business_Entity_Type__c = 'Corporation';
        bg.OrderApi__Merchant_Name__c = 'Test Merchant';
        bg.OrderApi__Merchant_Location__c = 'Test Merchant Location';
        return bg;
    }

    public static EventApi__Event_Category__c createEventCategory() {
        OrderApi__Business_Group__c bg = CPAO_FontevaTestFactory.createBusinessGroup();
        insert bg;
        EventApi__Event_Category__c cat = new EventApi__Event_Category__c();
        cat.EventApi__Business_Group__c = bg.Id;
        cat.Name = 'Test Event Category';
        return cat;
    }

    public static EventApi__Ticket_Block__c createTicketBlock() {
        EventApi__Ticket_Block__c ticketBlock = new EventApi__Ticket_Block__c();
        ticketBlock.EventApi__Number_Of_Available_Tickets__c = 25;
        ticketBlock.EventApi__Ticket_Block_Name__c = 'test';
        return ticketBlock;
    }

    public static EventApi__Event__c createEvent() {
        EventApi__Event_Category__c cat = CPAO_FontevaTestFactory.createEventCategory();
        insert cat;
        EventApi__Event__c ev = new EventApi__Event__c();
        ev.EventApi__Event_Category__c = cat.Id;
        ev.Name = 'Test Event';
        ev.EventApi__Event_Key__c = 'ABCD';
        ev.EventApi__Start_Date_Time__c = Datetime.now();
        ev.EventApi__Attendees__c = 10;
        ev.EventApi__Capacity__c = 100;
        ev.EventApi__Enable_Access_Permissions__c = false;
        ev.EventApi__Enable_Registration_Groups__c = true;
        ev.EventApi__Enable_Sponsors__c = true;
        ev.EventApi__Enable_Waitlist__c = true;
        ev.EventApi__Enable_Schedule_Items__c = true;
        ev.EventApi__End_Date_Time__c = Datetime.now().addDays(4);
        ev.EventApi__Start_Date__c = Date.today();
        ev.EventApi__Start_Time__c = '09:00 AM';
        ev.EventApi__End_Date__c = Date.today().addDays(4);
        ev.EventApi__End_Time__c = '09:00 AM';
        ev.EventApi__Is_Active__c = true;
        ev.EventApi__Is_Published__c = true;
        ev.EventApi__Registration_Instructions__c = 'Test Instructions';
        ev.EventApi__Registration_Style__c = 'Conference';
        ev.EventApi__Status__c = 'Planned';
        ev.EventApi__Ticket_Sales_Start_Date__c = Date.today();
        ev.EventApi__Time_Zone__c = '(GMT-10:00) America/Adak';
        ev.EventApi__Waitlist_Entries__c = 10;
        ev.EventApi__Business_Group__c = cat.EventApi__Business_Group__c;
        return ev;
    }

    public static EventApi__Ticket_Type__c createTicketType(EventApi__Event__c ev) {
        EventApi__Ticket_Type__c tt = new EventApi__Ticket_Type__c();
        tt.EventApi__Event__c = ev.Id;
        tt.Name = 'Test User';
        tt.EventApi__Price__c = 100;
        tt.EventApi__Is_Active__c = true;
        tt.EventApi__Is_Published__c = true;
        return tt;
    }

    public static EventApi__Section__c createSection() {
        EventApi__Ticket_Type__c tt = createTicketType();
        insert tt;
        EventApi__Section__c sectionObj = new EventApi__Section__c();
        sectionObj.Name = 'First Section';
        sectionObj.EventApi__Description__c = 'test';
        sectionObj.EventApi__Display_Order__c = 1;
        sectionObj.EventApi__Event__c = tt.EventApi__Event__c;
        return sectionObj;
    }

    public static EventApi__Attendee__c createAttendee() {
        EventApi__Ticket_Type__c tt = createTicketType();
        insert tt;
        EventApi__Attendee__c attendee = new EventApi__Attendee__c();
        attendee.EventApi__Event__c = tt.EventApi__Event__c;
        attendee.EventApi__Ticket_Type__c = tt.Id;
        attendee.EventApi__Full_Name__c = 'fullName';
        return attendee;
    }

    public static EventApi__Ticket_Type__c createTicketType() {
        EventApi__Event__c ev = CPAO_FontevaTestFactory.createEvent();
        insert ev;
        return CPAO_FontevaTestFactory.createTicketType(ev);
    }

    public static EventApi__Speaker__c createSpeaker() {
        EventApi__Event__c ev = CPAO_FontevaTestFactory.createEvent();
        insert ev;
        EventApi__Speaker__c speakerObj = new EventApi__Speaker__c();
        speakerObj.EventApi__Event__c = ev.Id;
        speakerObj.Name = 'Test User';
        speakerObj.EventApi__Email__c = 'test@stesd.com';
        speakerObj.EventApi__Bio__c = 'Test stest';
        //speakerObj.Facebook_URL__c = 'http://www.facebook.com/testProfile';
        //speakerObj.Twitter_URL__c = 'http://www.twitter.com/testProfile';
        //speakerObj.Linkedin_URL__c = 'http://www.linkedIn.com/testProfile';
        //speakerObj.Is_Featured__c = true;
        //speakerObj.EventApi__Is_Featured__c = false;
        return speakerObj;
    }

    public static Contact createContact() {
        Account acct = CPAO_FontevaTestFactory.createAccount();
        insert acct;
        Contact con = new Contact();
        con.FirstName = 'FirstName';
        con.LastName = 'LastName';
        con.Email = 'test@email.com';
        con.MailingStreet = 'Fairfax';
        con.MailingCity = 'Arlington';
        con.MailingState = 'VA';
        con.MailingPostalCode = '22203';
        con.MailingCountry = 'US';
        con.AccountId = acct.Id;
        return con;
    }

    public static Account createAccount() {
        Account acct = new Account();
        acct.Name = 'Test Account';
        return acct;
    }

    public static EventApi__Sponsor__c createSponsor() {
        EventApi__Sponsor_Package__c spckg = CPAO_FontevaTestFactory.createSponsorPackage();
        insert spckg;
        Contact con = CPAO_FontevaTestFactory.createContact();
        insert con;
        EventApi__Sponsor__c sponsorObj = new EventApi__Sponsor__c();
        sponsorObj.Name = 'TEST SPONSOR';
        sponsorObj.EventApi__Sponsor_Package__c = spckg.Id;
        sponsorObj.EventApi__Event__c = spckg.EventApi__Event__c;
        sponsorObj.EventApi__Account__c = con.AccountId;
        sponsorObj.EventApi__Contact__c = con.Id;
        //sponsorObj.Is_Featured__c = true;
        //sponsorObj.EventApi__Is_Featured__c = false;
        return sponsorObj;
    }

    public static EventApi__Sponsor_Package__c createSponsorPackage() {
        EventApi__Event__c ev = CPAO_FontevaTestFactory.createEvent();
        insert ev;
        EventApi__Sponsor_Package__c spckg = new EventApi__Sponsor_Package__c();
        spckg.Name = 'TEST SPONSOR PACKAGE';
        spckg.EventApi__Price__c = 5000;
        spckg.EventApi__Quantity_Available__c = 10;
        spckg.EventApi__Display_Name__c = 'TEST SPONSOR PACKAGE';
        spckg.EventApi__Line_Description__c = 'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Aenean euismod bibendum laoreet. Proin gravida dolor sit amet lacus accumsan et viverra justo commodo. Proin sodales pulvinar tempor';
        spckg.EventApi__Event__c = ev.Id;
        spckg.EventApi__Is_Active__c = true;
        spckg.EventApi__Display_Order__c = 1;
        return spckg;
    }

    public static OrderApi__Payment_Gateway__c createGateway() {
        OrderApi__Business_Group__c bg = CPAO_FontevaTestFactory.createBusinessGroup();
        insert bg;
        OrderApi__Payment_Gateway__c pg = new OrderApi__Payment_Gateway__c();
        pg.OrderApi__Business_Group__c = bg.Id;
        pg.OrderApi__Is_Enabled__c = true;
        pg.OrderApi__Token__c = 'EBVTQugrEH4PuvRLEkatnBttqrf';
        pg.OrderApi__Gateway_Type__c = 'Test';
        return pg;
    }

    public static PagesApi__Theme__c createTheme() {
        PagesApi__Theme__c theme = new PagesApi__Theme__c();
        theme.Name = 'testTheme';
        return theme;
    }

    public static OrderApi__Store__c createStore() {
        OrderApi__Payment_Gateway__c pg = CPAO_FontevaTestFactory.createGateway();
        insert pg;
        OrderApi__Store__c store = new OrderApi__Store__c();
        store.Name = 'Test';
        store.OrderApi__Business_Group__c = pg.OrderApi__Business_Group__c;
        store.OrderApi__Gateway__c = pg.Id;
        return store;
    }

    public static PagesApi__Site__c createSite() {

        PagesApi__Theme__c theme = CPAO_FontevaTestFactory.createTheme();
        insert theme;

        OrderApi__Store__c store = CPAO_FontevaTestFactory.createStore();
        insert store;

        PagesApi__Site__c siteObj = new PagesApi__Site__c();
        siteObj.Name = 'Test';
        siteObj.OrderApi__Business_Group__c = store.OrderApi__Business_Group__c;
        siteObj.PagesApi__Is_Published__c = true;
        siteObj.PagesApi__Site_Url__c = 'https://www.google.com';
        siteObj.OrderApi__Store__c = store.Id;
        siteObj.PagesApi__Theme__c = theme.Id;
        return siteObj;
    }

    // Helper method to create user instance. Insert is not done in this method
    public static User createUser(String licenseName,String roleName) {
        List<Profile> testProfiles = [SELECT Id FROM Profile WHERE UserLicense.Name = : licenseName LIMIT 1];
        if (testProfiles.size() != 1) {
            return null;
        }
        String usrName = System.now().format('yyyyMMddhhmmss') + '@testorg.com';
        User usr = new User();
        usr.Alias = 'test';
        usr.Email = usrName;
        usr.Username = usrName;
        usr.EmailEncodingKey = 'UTF-8';
        usr.LastName = 'last';
        usr.FirstName = 'first';
        usr.LanguageLocaleKey = 'en_US';
        usr.LocaleSidKey = 'en_US';
        usr.TimeZoneSidKey = 'America/Los_Angeles';
        if (roleName != null) {
            UserRole usrRole = [SELECT Id, Name, DeveloperName FROM UserRole WHERE Name = :roleName LIMIT 1 ];
            usr.UserRoleId = usrRole.Id;
        }
        usr.ProfileId = testProfiles[0].Id;
        System.assert(usr != null);
        return usr;
    }

    public static EventApi__Venue__c createVenue() {
        EventApi__Event__c ev = CPAO_FontevaTestFactory.createEvent();
        insert ev;
        EventApi__Venue__c venueObj = new EventApi__Venue__c();
        venueObj.EventApi__Event__c = ev.Id;
        venueObj.Name = 'Test Venue';
        venueObj.EventApi__Is_Primary_Venue__c = true;
        venueObj.EventApi__Description__c = 'Test Description';
        venueObj.EventApi__Street__c = '4201 Test Street Dr';
        return venueObj;
    }

    public static PagesApi__Form__c createForm() {

        PagesApi__Form__c testForm = new PagesApi__Form__c();
        testForm.Name = 'TEST FORM';
        testForm.PagesApi__User_Instructions__c = 'TEST INSTRUCTIONS';
        insert testForm;

        List<PagesApi__Field_Group__c> testFieldGroups = new List<PagesApi__Field_Group__c>();

        PagesApi__Field_Group__c fg = new PagesApi__Field_Group__c();
        fg.PagesApi__DML_Operation__c = 'Insert';
        fg.PagesApi__Form__c = testForm.Id;
        fg.PagesApi__Mapped_Object__c = 'account';
        fg.PagesApi__Order__c = 0;
        fg.PagesApi__Is_Multiple__c = false;
        fg.PagesApi__Instructions__c = 'Please fill in all required fields.';
        testFieldGroups.add(fg);

        insert testFieldGroups;

        List<PagesApi__Field__c> testFields = new List<PagesApi__Field__c>();

        PagesApi__Field__c f = new PagesApi__Field__c();
        f.PagesApi__Field_Group__c = fg.Id;
        f.PagesApi__Help_Text__c = 'Help Text';
        f.PagesApi__Form__c = testForm.Id;
        f.PagesApi__Is_Required__c = true;
        f.PagesApi__Is_Hidden__c = false;
        f.PagesApi__Order__c = 1;
        f.PagesApi__Type__c = 'Text';
        f.PagesApi__Mapped_Field__c = 'Name';

        testFields.add(f);

        PagesApi__Field__c f2 = new PagesApi__Field__c();
        f2.PagesApi__Field_Group__c = fg.Id;
        f2.PagesApi__Help_Text__c = 'Help Text';
        f2.PagesApi__Form__c = testForm.Id;
        f2.PagesApi__Is_Required__c = true;
        f2.PagesApi__Is_Hidden__c = false;
        f2.PagesApi__Order__c = 2;
        f2.PagesApi__Type__c = 'Picklist';
        f2.PagesApi__Options__c = 'First\nOption 1\nOption 2\nOption 3\nOption 4\nLast';
        f2.PagesApi__Mapped_Field__c = 'Type';

        testFields.add(f2);

        /**
         * PD-1813, added section header that has no mapping field
         */
        PagesApi__Field__c f3 = new PagesApi__Field__c();
        f3.PagesApi__Field_Group__c = fg.Id;
        f3.PagesApi__Help_Text__c = 'Section Header';
        f3.PagesApi__Form__c = testForm.Id;
        f3.PagesApi__Is_Required__c = false;
        f3.PagesApi__Is_Hidden__c = false;
        f3.PagesApi__Order__c = 3;
        f3.PagesApi__Type__c = 'Section Header';

        /**
         * PD-1855, enable hidden fields
         */
        PagesApi__Field__c f4 = new PagesApi__Field__c();
        f4.PagesApi__Field_Group__c = fg.Id;
        f4.PagesApi__Help_Text__c = 'Hidden Field';
        f4.PagesApi__Form__c = testForm.Id;
        f4.PagesApi__Is_Required__c = false;
        f4.PagesApi__Is_Hidden__c = true;
        f4.PagesApi__Order__c = 4;
        f4.PagesApi__Type__c = 'Text';
        f4.PagesApi__Mapped_Field__c = 'AccountNumber';

        testFields.add(f4);
        insert testFields;

        return testForm;

    }

    public static OrderApi__Item_Class__c createItemClass() {
        OrderApi__Item_Class__c ic = new OrderApi__Item_Class__c();
        ic.Name = 'Test Item Class Name';
        return ic;
    }

    public static OrderApi__Item__c createSubscriptionItem() {
        OrderApi__Item__c item=new OrderApi__Item__c();
        OrderApi__GL_Account__c gl=new OrderApi__GL_Account__c();
        OrderApi__Business_Group__c bg=new OrderApi__Business_Group__c();
        bg=CPAO_FontevaTestFactory.createBusinessGroup();
        insert bg;
        OrderApi__Item_Class__c itemClass=CPAO_FontevaTestFactory.createItemClass();
        itemClass.OrderApi__Is_Subscription__c = true;
        insert itemClass;
        subPlan= CPAO_FontevaTestFactory.createSubPlan();
        subPlan.OrderApi__Business_Group__c=bg.Id;
        insert subPlan;

        item.Name='dirty magazine';
        item.OrderApi__Price__c=100.00;
        item.OrderApi__Cost__c=50.00;
        item.OrderApi__Is_Active__c=false;
        item.OrderApi__Item_Class__c=itemClass.Id;
        item.OrderApi__Business_Group__c=bg.Id;
        item.OrderApi__Income_Account__c=gl.Id;
        item.OrderApi__Adjustment_Account__c=gl.Id;
        item.OrderApi__Discount_Account__c=gl.Id;
        item.OrderApi__Refund_Account__c=gl.Id;
        item.OrderApi__Business_Group__c=bg.Id;
        insert item;

        OrderApi__Item_Subscription_Plan__c isp=new OrderApi__Item_Subscription_Plan__c();
        isp.OrderApi__Is_Default__c=true;
        isp.OrderApi__Item__c=item.Id;
        isp.OrderApi__Subscription_Plan__c=subPlan.Id;
        insert isp;

        item.OrderApi__Is_Active__c=true;
        update item;

        return item;
    }

    public static OrderApi__Item__c createOrderApiItem() {
        OrderApi__Item__c itm = new OrderApi__Item__c();
        OrderApi__GL_Account__c gl = new OrderApi__GL_Account__c();
        OrderApi__Business_Group__c bg = new OrderApi__Business_Group__c();
        bg = CPAO_FontevaTestFactory.createBusinessGroup();
        insert bg;
        OrderApi__Item_Class__c itmClass = CPAO_FontevaTestFactory.createItemClass();
        insert itmClass;
        itm.Name = 'test item';
        itm.OrderApi__Price__c = 100.00;
        itm.OrderApi__Cost__c = 50.00;
        itm.OrderApi__Is_Active__c = true;
        itm.OrderApi__Item_Class__c = itmClass.Id;
        itm.OrderApi__Business_Group__c = bg.Id;
        itm.OrderApi__Income_Account__c = gl.Id;
        itm.OrderApi__Adjustment_Account__c = gl.Id;
        itm.OrderApi__Discount_Account__c = gl.Id;
        itm.OrderApi__Refund_Account__c = gl.Id;
        itm.OrderApi__Business_Group__c = bg.Id;
        insert itm;
        return itm;
    }

    public static EventApi__Event__c createEventApiEvent() {
        EventApi__Event__c ev = new EventApi__Event__c();
        ev.Name = 'Test Event';
        ev.EventApi__Event_Key__c = 'ABCD';
        ev.EventApi__Start_Date_Time__c = Datetime.now();
        ev.EventApi__Attendees__c = 10;
        ev.EventApi__Capacity__c = 100;
        ev.EventApi__Enable_Access_Permissions__c = false;
        ev.EventApi__Enable_Registration_Groups__c = true;
        ev.EventApi__Enable_Sponsors__c = true;
        ev.EventApi__Enable_Waitlist__c = true;
        ev.EventApi__Enable_Schedule_Items__c = true;
        ev.EventApi__End_Date_Time__c = Datetime.now().addDays(4);
        ev.EventApi__Is_Active__c = true;
        ev.EventApi__Is_Published__c = true;
        ev.EventApi__Registration_Instructions__c = 'Test Instructions';
        ev.EventApi__Registration_Style__c = 'Conference';
        ev.EventApi__Status__c = 'Planned';
        ev.EventApi__Ticket_Sales_Start_Date__c = Date.today();
        ev.EventApi__Start_Date__c = Date.today();
        ev.EventApi__Start_Time__c = '09:00 AM';
        ev.EventApi__End_Date__c = Date.today().addDays(4);
        ev.EventApi__End_Time__c = '09:00 AM';
        ev.EventApi__Waitlist_Entries__c = 10;
        insert ev;
        return ev;
    }

    public static OrderApi__Sales_Order_Line__c createSalesOrderLine(EventApi__Event__c ev) {
        OrderApi__Sales_Order__c so = new OrderApi__Sales_Order__c();
        OrderApi__Sales_Order_Line__c sol = new OrderApi__Sales_Order_Line__c();
        OrderApi__Item__c itm = new OrderApi__Item__c();
        insert so;
        itm = CPAO_FontevaTestFactory.createOrderApiItem();
        OrderApi__Business_Group__c bg = CPAO_FontevaTestFactory.createBusinessGroup();
        insert bg;
        sol.EventApi__Event__c = ev.Id;
        sol.OrderApi__Sales_Order__c = so.Id;
        sol.OrderApi__Item__c = itm.Id;
        sol.OrderApi__Business_Group__c = bg.Id;
        EventApi__Ticket_Type__c tt = CPAO_FontevaTestFactory.createTicketType(ev);
        insert tt;
        sol.EventApi__Ticket_Type__c = tt.Id;
        itm.EventApi__Ticket_Type__c = tt.Id;
        update itm;
        insert sol;
        return sol;
    }

    public static EventApi__Event_Status__c createEventStatus() {
        EventApi__Event__c ev = CPAO_FontevaTestFactory.createEvent();
        insert ev;
        EventApi__Event_Status__c statusObj = new EventApi__Event_Status__c();
        statusObj.Name = 'Planned';
        statusObj.EventApi__Is_Current_Status_For_Event__c = true;
        statusObj.EventApi__Event__c = ev.Id;
        statusObj.EventApi__Order__c = 1;
        return statusObj;
    }


    public static EventApi__Event_Page__c createEventPage() {
        EventApi__Event_Status__c statusObj = createEventStatus();
        insert statusObj;
        EventApi__Event_Page__c pageObj = new EventApi__Event_Page__c();
        pageObj.EventApi__Navigation_Label__c = 'test';
        pageObj.EventApi__Page_Name__c = 'test';
        insert pageObj;
        EventApi__Event_Status_Page__c statusPage = new EventApi__Event_Status_Page__c();
        statusPage.EventApi__Event_Status__c = statusObj.Id;
        statusPage.EventApi__Event_Page__c = pageObj.Id;
        statusPage.EventApi__Order__c = 1;
        statusPage.EventApi__Is_Published__c = true;
        insert statusPage;

        EventApi__Event_Page_Component__c pageComp = new EventApi__Event_Page_Component__c();
        pageComp.EventApi__Component_Name__c = 'test';
        pageComp.EventApi__Config__c = 'sdf';
        pageComp.EventApi__Order__c = 1;
        pageComp.EventApi__Event_Page__c = pageObj.Id;

        return pageObj;
    }

    public static OrderApi__Receipt__c createReceipt() {
        OrderApi__Receipt__c rec = new OrderApi__Receipt__c();
        Contact contactObj = CPAO_FontevaTestFactory.createContact();
        insert contactObj;
        rec.OrderApi__Contact__c = contactObj.Id;
        return rec;
    }

    public static OrderApi__Invoice__c createInvoice() {
        OrderApi__Invoice__c rec = new OrderApi__Invoice__c();
        Contact contactObj = CPAO_FontevaTestFactory.createContact();
        insert contactObj;
        rec.OrderApi__Contact__c = contactObj.Id;
        return rec;
    }

    public static EventApi__Schedule_Item__c createScheduleItem(EventApi__Event__c ev) {
        EventApi__Schedule_Item__c sch = new EventApi__Schedule_Item__c();
        sch.Name = 'Test Schedule1';
        sch.EventApi__Event__c = ev.Id;
        sch.EventApi__Price__c = 120;
        sch.EventApi__Is_Active__c = true;
        sch.EventApi__Quantity_Available__c = 120;
        sch.EventApi__Start_Date_Time__c = ev.EventApi__Start_Date_Time__c;
        sch.EventApi__End_Date_Time__c = ev.EventApi__End_Date_Time__c;
        sch.EventApi__Start_Date__c = ev.EventApi__Start_Date__c;
        sch.EventApi__End_Date__c = ev.EventApi__Start_Date__c;
        sch.EventApi__Session_Start_Time__c = '9:30 AM';
        sch.EventApi__Session_End_Time__c = '10:30 AM';
        return sch;
    }

    public static PagesApi__Form__c createFormWithSkipLogic() {

        PagesApi__Form__c testForm = new PagesApi__Form__c();
        testForm.Name = 'TEST FORM';
        testForm.PagesApi__User_Instructions__c = 'TEST INSTRUCTIONS';
        insert testForm;

        List<PagesApi__Field_Group__c> testFieldGroups = new List<PagesApi__Field_Group__c>();

        PagesApi__Field_Group__c fg = new PagesApi__Field_Group__c();
        fg.PagesApi__DML_Operation__c = 'Insert';
        fg.PagesApi__Form__c = testForm.Id;
        fg.PagesApi__Mapped_Object__c = 'account';
        fg.PagesApi__Order__c = 0;
        fg.PagesApi__Is_Multiple__c = false;
        fg.PagesApi__Instructions__c = 'Please fill in all required fields.';
        testFieldGroups.add(fg);

        insert testFieldGroups;

        List<PagesApi__Field__c> testFields = new List<PagesApi__Field__c>();

        PagesApi__Field__c f = new PagesApi__Field__c();
        f.PagesApi__Field_Group__c = fg.Id;
        f.PagesApi__Help_Text__c = 'Help Text';
        f.PagesApi__Form__c = testForm.Id;
        f.PagesApi__Is_Required__c = true;
        f.PagesApi__Is_Hidden__c = false;
        f.PagesApi__Order__c = 1;
        f.PagesApi__Type__c = 'Text';
        f.PagesApi__Mapped_Field__c = 'Name';
        insert f;

        PagesApi__Field__c f2 = new PagesApi__Field__c();
        f2.PagesApi__Field_Group__c = fg.Id;
        f2.PagesApi__Help_Text__c = 'Help Text';
        f2.PagesApi__Form__c = testForm.Id;
        f2.PagesApi__Is_Required__c = true;
        f2.PagesApi__Is_Hidden__c = false;
        f2.PagesApi__Order__c = 2;
        f2.PagesApi__Type__c = 'Picklist';
        f2.PagesApi__Options__c = 'First\nOption 1\nOption 2\nOption 3\nOption 4\nLast';
        f2.PagesApi__Mapped_Field__c = 'Type';
        insert f2;

        /**
         * PD-1813, added section header that has no mapping field
         */
        PagesApi__Field__c f3 = new PagesApi__Field__c();
        f3.PagesApi__Field_Group__c = fg.Id;
        f3.PagesApi__Help_Text__c = 'Section Header';
        f3.PagesApi__Form__c = testForm.Id;
        f3.PagesApi__Is_Required__c = false;
        f3.PagesApi__Is_Hidden__c = false;
        f3.PagesApi__Order__c = 3;
        f3.PagesApi__Type__c = 'Section Header';
        insert f3;
        /**
         * PD-1855, enable hidden fields
         */
        PagesApi__Field__c f4 = new PagesApi__Field__c();
        f4.PagesApi__Field_Group__c = fg.Id;
        f4.PagesApi__Help_Text__c = 'Hidden Field';
        f4.PagesApi__Form__c = testForm.Id;
        f4.PagesApi__Is_Required__c = false;
        f4.PagesApi__Is_Hidden__c = true;
        f4.PagesApi__Order__c = 4;
        f4.PagesApi__Type__c = 'Text';
        f4.PagesApi__Mapped_Field__c = 'AccountNumber';
        insert f4;

        PagesApi__Skip_Logic_Rule__c logic1 = new PagesApi__Skip_Logic_Rule__c();
        logic1.PagesApi__Field__c = f2.Id;
        logic1.PagesApi__Answer__c = 'First';
        logic1.PagesApi__Destination__c = f3.Id;
        insert logic1;

        return testForm;

    }

    public static OrderApi__Subscription_Plan__c createSubPlan() {
        OrderApi__Subscription_Plan__c subPlan = new OrderApi__Subscription_Plan__c();
        subPlan.Name='test subscription plan';
        subPlan.OrderApi__Type__c='Termed';
        subPlan.OrderApi__Initial_Term_Months__c=12;
        subPlan.OrderApi__Renewal_Term_Months__c=12;
        subPlan.OrderApi__Bill_Cycle__c='AUTO';
        subPlan.OrderApi__Auto_Renew_Option__c='Enabled';
        subPlan.OrderApi__Enable_Proration__c=false;
        subPlan.OrderApi__Is_Active__c=true;
        return subPlan;
    
    }

    public static OrderApi__Subscription__c createSubscription(){
        OrderApi__Item__c item=createSubscriptionItem();
        OrderApi__Business_Group__c businessGroup=createBusinessGroup();
        OrderApi__Subscription_Plan__c subscriptionPlan=createSubPlan();
        OrderApi__Subscription__c testSubscription = new OrderApi__Subscription__c();
        testSubscription.OrderApi__Business_Group__c = businessGroup.Id;
        testSubscription.OrderApi__Item__c=item.Id;
        testSubscription.OrderApi__Subscription_Plan__c=subscriptionPlan.Id;
        return testSubscription;
    }

    
}