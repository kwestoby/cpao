({
    doInit : function(component,event,helper){
        
        var action = component.get("c.getCurrentCOAApplication");
        action.setCallback(this, function(response){
            console.log(response);
            var state = response.getState();
            if (state === "SUCCESS") {

                var apexResponse=response.getReturnValue();
                
                component.set('v.currentCOA',apexResponse.COA_app);
                component.set('v.LSO_accounts',apexResponse.LSO_Accounts);
                
            }else{
                console.log('error while doinit Main container');
                var errors = response.getError();
                       console.log(errors[0]);
            }
            
        });
        
        $A.enqueueAction(action);
        
        
    },
    handleNext : function(component,event,helper){
        var coaApp= component.get('v.currentCOA');
        
        var getselectedStep = component.get("v.selectedStep");

        if(getselectedStep == "step0"){
            // component.set("v.selectedStep", "step2");
            helper.notifyClickOnNextButton("CPAO_COA_ChoiceOfFirm",coaApp,'step1');    
        }
        if(getselectedStep == "step1"){
            // component.set("v.selectedStep", "step2");
            helper.notifyClickOnNextButton("CPAO_COA_Step1_Instructions",coaApp,'step2');    
        }
        else if(getselectedStep == "step2"){
            helper.notifyClickOnNextButton("CPAO_COA_Step2_COA_Declaration",coaApp,'step3');
            
            //component.set("v.selectedStep", "step3");
        }else if(getselectedStep == "step3"){
                component.set("v.selectedStep", "step4");
        }
    },
    
    handlePrev : function(component,event,helper){
        var getselectedStep = component.get("v.selectedStep");
        if(getselectedStep == "step1"){
            component.set("v.selectedStep", "step0");
        }
        if(getselectedStep == "step2"){
            component.set("v.selectedStep", "step1");
        }
        else if(getselectedStep == "step3"){
            component.set("v.selectedStep", "step2");
        }
            else if(getselectedStep == "step4"){
                component.set("v.selectedStep", "step3");
            }
    },
    
    handleFinish : function(component,event,helper){
        var coaApp= component.get('v.currentCOA');
        helper.notifyClickOnNextButton("CPAO_COA_Step3_ReviewPage",coaApp,'');
    },
    applyResponseFromChildComponent : function(component,event,helper){
        if(event.getParam('nextButtonClickeable')){
            component.set('v.currentCOA',event.getParam('currentApplication'));
            console.log( 'returned application : '+JSON.stringify(component.get('v.currentCOA')));
            
            //  helper.formatJavaScriptDates(component);
            if(component.get("v.selectedStep")== "step0"){
                component.set("v.selectedStep", "step1");
                
            }else  if(component.get("v.selectedStep")== "step1"){
                component.set("v.selectedStep", "step2");
                
                // helper.verifyIfSkippingCPDPage(component,'Next');
            }else if(component.get("v.selectedStep")== "step2"){
                component.set("v.selectedStep", "step3");
                var reviewLoadMethod = component.find('reviewPageCOA');
            	reviewLoadMethod.loadMethod();
                
            }
            helper.hideSpinner(component);
            
            
        }
    },saveAndQuit: function(component,event,helper){
        var getselectedStep = component.get("v.selectedStep");
        var COAApp= component.get('v.currentCOA');
        if(getselectedStep == "step0"){
            helper.notifyClickOnNextButton("CPAO_COA_ChoiceOfFirm",COAApp,'Exit');
        }else if(getselectedStep == "step1"){
            helper.notifyClickOnNextButton("CPAO_COA_Step1_Instructions",COAApp,'Exit');
            
        }else if(getselectedStep == "step2"){
            helper.notifyClickOnNextButton("CPAO_COA_Step2_COA_Declaration",COAApp,'Exit');
            
        }
        
    },
    showSpinner: function (component, event, helper) {
        helper.showSpinner(component, event, helper);
    }
    
    
    
})