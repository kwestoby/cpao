@isTest
public with sharing class CPAO_ApplicationServicesTest{

	public static List<Account> factoryAccounts;
	public static List<Contact> factoryContacts;
	public static User currentUser;
	public static User user;

	public static void dataInitialize(Integer numberOfAccounts, Integer numberOfContacts){
		CPAO_TestFactory.automationControlSetup();
		factoryAccounts = CPAO_TestFactory.createAccounts(numberOfAccounts);
		insert factoryAccounts;
		factoryContacts = CPAO_TestFactory.createContacts(factoryAccounts[0],numberOfContacts);
		factoryContacts = CPAO_TestFactory.addContactStudentInfo(factoryContacts);

		insert factoryContacts;
		
		user = CPAO_TestFactory.createCommunityUser(factoryContacts[0].Id);
        insert user;

        currentUser = CPAO_UserSelector.getCurrentUser();
	}

	@isTest
	public static void standardTest() {
		dataInitialize(1,1);
		CPAO_Application__c application = CPAO_TestFactory.createAMDCPDpplication(1, factoryContacts[0])[0];
		insert application;
		CPAO_Application__c application2 = CPAO_TestFactory.createAMDCPDpplication(1, factoryContacts[0])[0];
		application2.CPAO_Status__c = 'Completed';
		insert application2;
		List<SDOC__SDTemplate__c> templatesToInsert = new List<SDOC__SDTemplate__c>();
		templatesToInsert.add(new SDOC__SDTemplate__c(
			Name = 'PAL Licence Attachment',
			SDOC__Base_Object__c = 'CPAO_Licence__c',
			SDOC__Template_Format__c = 'HTML',
			SDOC__Active__c = true));
		templatesToInsert.add(new SDOC__SDTemplate__c(
			Name = 'PAL Application Approved',
			SDOC__Base_Object__c = 'CPAO_Licence__c',
			SDOC__Template_Format__c = 'HTML',
			SDOC__Active__c = true));
		templatesToInsert.add(new SDOC__SDTemplate__c(
			Name = 'COA Licence Attachment',
			SDOC__Base_Object__c = 'CPAO_Licence__c',
			SDOC__Template_Format__c = 'HTML',
			SDOC__Active__c = true));
		templatesToInsert.add(new SDOC__SDTemplate__c(
			Name = 'COA Application Approved',
			SDOC__Base_Object__c = 'CPAO_Licence__c',
			SDOC__Template_Format__c = 'HTML',
			SDOC__Active__c = true));
		insert templatesToInsert;
		List<CPAO_Licence__c> licences = CPAO_TestFactory.createActiveLicences(2, factoryContacts[0], application);
		application = [SELECT Id, CPAO_Contact__c, CPAO_Application_Type__c, CPAO_Status__c FROM CPAO_Application__c WHERE Id = :application.Id];
		CPAO_ApplicationServices.stampUserContactOnApplication(new List<CPAO_Application__c>{application});
		CPAO_ApplicationServices.roReinstateApplications(new List<CPAO_Application__c>{application});
		CPAO_ApplicationServices.inReviewApplications(new List<CPAO_Application__c>{application});
		CPAO_ApplicationServices.registrarReinstateApplications(new List<CPAO_Application__c>{application});
		CPAO_ApplicationServices.registrarReadmitApplications(new List<CPAO_Application__c>{application});
		CPAO_ApplicationServices.registrarDenyApplications(new List<CPAO_Application__c>{application});
		CPAO_ApplicationServices.checkBreachesBeforeApplApprovoed(new List<CPAO_Application__c>{application});
		CPAO_ApplicationServices.reinstatementPaidApplications(new List<CPAO_Application__c>{application});

		CPAO_ApplicationServices.amdANDcpdInitializer(new List<CPAO_Application__c>{application});
		CPAO_ApplicationServices.registrarAdmitedApplications(new List<CPAO_Application__c>{application});

		CPAO_ApplicationServices.calculatePALExpiryDate(Date.today());

		CPAO_ApplicationServices.sendApprovalEmail(licences, 'PAL');
		CPAO_ApplicationServices.sendApprovalEmail(licences, 'COA');
		CPAO_ApplicationServices.sendApprovalEmail(licences, 'PAL-Renewal');

		CPAO_Infraction_Type__c infractionType = CPAO_TestFactory.createInfractionTypes(1, CPAO_InfractionAssignBatch.AMD)[0];
        insert infractionType;
		CPAO_Infraction__c infraction = CPAO_TestFactory.createInfractions(1, infractionType)[0];
        infraction.CPAO_Contact__c = factoryContacts[0].Id;
        insert infraction;
        CPAO_ApplicationServices.registrarReadmitApplications(new List<CPAO_Application__c>{application});
        CPAO_ApplicationServices.reinstatementPaidApplications(new List<CPAO_Application__c>{application});
        application.CPAO_Recommendation__c = 'Approve';
        CPAO_ApplicationServices.reinstatementPaidApplications(new List<CPAO_Application__c>{application});
        CPAO_ApplicationServices.checkBreachesBeforeApplApprovoed(new List<CPAO_Application__c>{application});
        



	}
}