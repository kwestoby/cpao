({
	doInit: function (component, event, helper) {
		helper.getKnowledgeArticles(component);
	},
	handleShowModal: function(component, event, helper) {
		var index = event.currentTarget.dataset.index;
		var article = component.get("v.articles")[index];
		var title = article.Title;
		var bodyContent = "";
		if(article.Summary!==undefined) {
			bodyContent += "<h2>Summary</h2><p>"+article.Summary+"</p>";
		}
		if(article.Question__c!==undefined) {
			bodyContent += "<h2>Question</h2><p>"+article.Question__c+"</p>";
		}
		if(article.Answer__c!==undefined) {
			bodyContent += "<h2>Answer</h2><p>"+article.Answer__c+"</p>";
		}
		component.find("knowledgeModal").show(title, bodyContent);
	},
	handleFilterChange: function(component, event, helper) {
        component.set("v.pageMessages", []);
        component.set("v.pageNumber",1);     
        helper.getKnowledgeArticles(component);
    },
	handleSort : function(component, event, helper) {
        var id = event.currentTarget.dataset.id;
        if(component.get("v.sortBy")===id) {
            if(component.get("v.sortOrder")=="ASC") {
                component.set("v.sortOrder", "DESC");
            } else {
                component.set("v.sortOrder", "ASC");
            }
        } else {
            component.set("v.sortBy", id);
            component.set("v.sortOrder", "ASC");
        }
        helper.getKnowledgeArticles(component);
    },
    pageSizeChanged: function(component, event, helper) {
        component.set("v.pageMessages", []);
        component.set("v.pageNumber",1);
        helper.getKnowledgeArticles(component);
    },
    first: function(component, event, helper) {
        if(component.get("v.pageNumber")>1) {
            component.set("v.pageNumber",1);
            helper.getKnowledgeArticles(component);
        }
    },
    previous: function(component, event, helper) {
        var currentPage = component.get("v.pageNumber");
        if(currentPage>1) {
            currentPage-=1;
            component.set("v.pageNumber",currentPage);
            helper.getKnowledgeArticles(component);
        }
    },
    next: function(component, event, helper) {
        var currentPage = component.get("v.pageNumber");
        var lastPageNumber = component.get("v.pages");
        if(currentPage<lastPageNumber) {
            currentPage+=1;
            component.set("v.pageNumber",currentPage);
            helper.getKnowledgeArticles(component);
        }
    },
    last: function(component, event, helper) {
        var currentPage = component.get("v.pageNumber");
        var lastPageNumber = component.get("v.pages");
        if(currentPage<lastPageNumber) {
            component.set("v.pageNumber", lastPageNumber);
            helper.getKnowledgeArticles(component);
        }
    }
})