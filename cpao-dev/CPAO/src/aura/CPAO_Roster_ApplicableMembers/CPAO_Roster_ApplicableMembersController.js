({
	doInit : function(component, event, helper) {
		helper.init(component, event);
        var action = component.get("c.getUser");
        action.setCallback(this, function(response) {
            var state = response.getState();
            if (state === "SUCCESS") {
                var storeResponse = response.getReturnValue();
                component.set("v.userInfo", storeResponse);
            }
        });
        $A.enqueueAction(action);
	},
	
    refreshContacts : function(component, event, helper) {
    	helper.refreshContacts(component, event);
    },

    /* RemoveFromtheContactList: function(component, event, helper) {
    	helper.RemoveFromtheContactList(component,event);
    },
   
	RemoveFromtheContactLists: function(component, event, helper) {
    	helper.RemoveFromtheContactLists(component,event);
    },
    */
    checkAllCheckboxes : function(component, event, helper) {
    	helper.checkAllCheckboxes(component,event);
    },
    
    saveMarkedContactsaAndOpenPayment : function(component, event, helper) {
		helper.saveMarkedContactsaAndOpenPayment(component, event);
	}
})