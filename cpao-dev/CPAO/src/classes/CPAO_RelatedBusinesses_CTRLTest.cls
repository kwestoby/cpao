@isTest
private class CPAO_RelatedBusinesses_CTRLTest {
    private static User currentUser;
    private static List<Account> factoryAccounts;
    private static List<Contact> factoryContacts;
    private static List<User> communityUsers;
    private static List<CPAO_Account_to_Account_Relationship__c> relationships;

    private static Map<Id, Contact> factoryContactMap;
    private static Map<Id, Account> factoryAccountMap;
    private static Account parentAccount;

    @testSetup
    public static void dataInitialize(){
        CPAO_TestFactory.automationControlSetup();
        
        List<Account> factoryAccounts = CPAO_TestFactory.createAccounts(4);
        insert factoryAccounts;

        List<CPAO_Account_to_Account_Relationship__c> relationships = 
            CPAO_TestFactory.createAccountToAccountRelationships(factoryAccounts[0], new List<Account>{factoryAccounts[1], factoryAccounts[2]});
        insert relationships;

        List<Contact> factoryContacts = CPAO_TestFactory.createContacts(factoryAccounts[0], 1);
        insert factoryContacts;

        communityUsers = new List<User>();
        communityUsers.add(CPAO_TestFactory.createCommunityUser(factoryContacts[0].Id));
        insert communityUsers;
    }

    private static void fillStaticData() {
        currentUser = CPAO_UserSelector.getCurrentUser();

        factoryAccounts = [SELECT Id FROM Account];
        factoryAccountMap = new Map<Id, Account>(factoryAccounts);
        System.assertEquals(4, factoryAccounts.size());

        relationships = [
            SELECT 
                CPAO_Account_Name__c,
                CPAO_Associated_Account_Name__c, 
                CPAO_Start_Date__c,
                CPAO_End_Date__c,
                CPAO_Relationship_to_Associated_Account__c,
                CPAO_Status__c
            FROM CPAO_Account_to_Account_Relationship__c
        ];
        System.assertEquals(2, relationships.size());

        parentAccount = factoryAccountMap.get(relationships[0].CPAO_Account_Name__c);
        System.assert(parentAccount != null);

        factoryContacts = [SELECT Id, CPAO_CPA_Status__c FROM Contact];
        factoryContactMap = new Map<Id, Contact>(factoryContacts);
        System.assertEquals(1, factoryContacts.size());


        communityUsers = [SELECT Id, ContactId FROM User WHERE ContactId IN: factoryContacts];
        System.assertEquals(factoryContacts.size(), communityUsers.size(), 'We should have used all the contacts to make community users');
    }

    @isTest
    private static void saveAccountTest() {
        fillStaticData();

        Account newAccount = CPAO_TestFactory.createAccounts(1)[0];
        newAccount.Name = 'Annual Practitioner Test Account';

        String newAccountJSON = JSON.serialize(newAccount);
        Boolean exceptionOccurred = false;
        String error;
        System.runAs(communityUsers[0]) {
            try {
                CPAO_RelatedBusinesses_CTRL.saveAccount(newAccountJSON);
            }
            catch(Exception ex) {
                exceptionOccurred = true;
                error = ex.getMessage() + '\n\n' + ex.getStackTraceString();
            }
        }
        List<Account> accounts = [SELECT Id FROM Account WHERE Name=:newAccount.Name];
        System.assert(!exceptionOccurred, error);
        System.assertEquals(1, accounts.size(), 'We should have created the account');
    }

    @isTest
    private static void saveChildrenTest1() {
        fillStaticData();
        Account newChild = factoryAccounts[3];
        
        CPAO_Account_to_Account_Relationship__c relationship = CPAO_TestFactory.createAccountToAccountRelationship(parentAccount.Id, newChild.Id);
        
        List<CPAO_RelatedBusinesses_CTRL.RelatedWrapper> wrappers = new List<CPAO_RelatedBusinesses_CTRL.RelatedWrapper>();
        wrappers.add(new CPAO_RelatedBusinesses_CTRL.RelatedWrapper(relationship));
        String wrappersJSON = JSON.serialize(wrappers);
        
        List<CPAO_RelatedBusinesses_CTRL.RelatedWrapper> children;
        System.runAs(communityUsers[0]) {
            children = CPAO_RelatedBusinesses_CTRL.saveChildren(wrappersJSON, parentAccount.Id);
        }
        System.assertEquals(relationships.size()+wrappers.size(), children.size(), 'We should have '+wrappers.size()+' more children');
    }

    @isTest
    private static void saveChildrenTest2() {
        fillStaticData();
        CPAO_Account_to_Account_Relationship__c relationship = relationships[0];

        relationship.CPAO_End_Date__c = relationship.CPAO_End_Date__c.addDays(12);
        
        List<CPAO_RelatedBusinesses_CTRL.RelatedWrapper> wrappers = new List<CPAO_RelatedBusinesses_CTRL.RelatedWrapper>();
        wrappers.add(new CPAO_RelatedBusinesses_CTRL.RelatedWrapper(relationship));
        String wrappersJSON = JSON.serialize(wrappers);
        
        List<CPAO_RelatedBusinesses_CTRL.RelatedWrapper> children;
        System.runAs(communityUsers[0]) {
            children = CPAO_RelatedBusinesses_CTRL.saveChildren(wrappersJSON, parentAccount.Id);
        }
        System.assertEquals(relationships.size(), children.size(), 'The number of children should not have changed');

        CPAO_Account_to_Account_Relationship__c updatedRelation = [SELECT CPAO_End_Date__c FROM CPAO_Account_to_Account_Relationship__c WHERE Id =: relationship.Id];
        System.assertEquals(relationship.CPAO_End_Date__c, updatedRelation.CPAO_End_Date__c, 'The end date should have been updated');
    }
    
    @isTest
    private static void getInitWrapperTest() {
        fillStaticData();

        CPAO_RelatedBusinesses_CTRL.InitWrapper init;
        System.runAs(communityUsers[0]) {
            init = CPAO_RelatedBusinesses_CTRL.getInitWrapper(parentAccount.Id);
        }
        System.assertNotEquals(null, init, 'Our Init wrapper should not be null');

        System.assertNotEquals(true, init.children.isEmpty(), 'We should have received children in the initialization');
        System.assertNotEquals(true, init.relationships.isEmpty(), 'We should have received relationships in the initialization');
        System.assertNotEquals(null, init.parent, 'We should have received parent in the initialization');
    }

    @isTest
    private static void getParentTest() {
        fillStaticData();

        Account parent;
        System.runAs(communityUsers[0]) {
            parent = CPAO_RelatedBusinesses_CTRL.getParent(parentAccount.Id);
        }
        System.assertEquals(parentAccount.Id, parent.Id, 'We should have received the same account');
    }

    @isTest
    private static void getChildrenTest() {
        fillStaticData();

        List<CPAO_RelatedBusinesses_CTRL.RelatedWrapper> children;
        System.runAs(communityUsers[0]) {
            children = CPAO_RelatedBusinesses_CTRL.getChildren(parentAccount.Id);
        }
        System.assertEquals(relationships.size(), children.size(), 'We should have received the same amount of children');
    }

    @isTest
    private static void getRelationshipPicklistTest() {
        List<Schema.PicklistEntry> entries = CPAO_Account_to_Account_Relationship__c.CPAO_Relationship_to_Associated_Account__c.getDescribe().getPicklistValues();
        List<CPAO_PicklistWrapper> picklistValues = CPAO_RelatedBusinesses_CTRL.getRelationshipPicklist();
        System.assertEquals(entries.size(), picklistValues.size(), 'We should have received the same amount of relationship picklist values');
    }

    @isTest
    private static void getAccountToAccountRelationsTest() {
        fillStaticData();

        List<CPAO_Account_to_Account_Relationship__c> children;
        System.runAs(communityUsers[0]) {
            children = CPAO_RelatedBusinesses_CTRL.getAccountToAccountRelations(parentAccount.Id);
        }
        System.assertEquals(relationships.size(), children.size(), 'We should have received the same amount of children');
    }
}