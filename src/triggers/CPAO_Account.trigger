trigger CPAO_Account on Account (before insert, after insert, before update, after update) {

    CPAO_Automation_Control__c control = CPAO_Automation_Control__c.getOrgDefaults();
    if(!control.Account__c){
        return;
    }
    
    if(Trigger.isBefore && Trigger.isInsert){
        CPAO_AccountHandler.beforeInsertFilters(Trigger.New);
    }
    if(Trigger.isBefore && Trigger.isUpdate){
        CPAO_AccountHandler.beforeUpdateFilters(Trigger.New, Trigger.oldMap);

    }

    if(Trigger.isAfter && Trigger.isInsert){
        CPAO_AccountHandler.afterInsertFilters(Trigger.New);
    }

    if(Trigger.isAfter && Trigger.isUpdate){
        CPAO_AccountHandler.afterUpdateFilters(Trigger.New, Trigger.oldMap);
    }

    if(Trigger.isAfter && Trigger.isDelete){
        CPAO_AccountHandler.afterDeleteFilters(Trigger.Old);
    }
}