<?xml version="1.0" encoding="UTF-8"?>
<CustomMetadata xmlns="http://soap.sforce.com/2006/04/metadata" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xmlns:xsd="http://www.w3.org/2001/XMLSchema">
    <label>Company or Personal Cheque</label>
    <protected>false</protected>
    <values>
        <field>CPAO_Description__c</field>
        <value xsi:type="xsd:string">Effective XXXX, we will no longer be accepting cheque as a payment option. We apologize for the inconvenience this may have caused you.</value>
    </values>
    <values>
        <field>CPAO_Order__c</field>
        <value xsi:type="xsd:double">2.0</value>
    </values>
    <values>
        <field>CPAO_Type__c</field>
        <value xsi:type="xsd:string">Offline Payment</value>
    </values>
</CustomMetadata>
