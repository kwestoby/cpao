trigger CPAO_Infraction on CPAO_Infraction__c (before insert, after insert, before update, after update, after delete) {

	CPAO_Automation_Control__c control = CPAO_Automation_Control__c.getOrgDefaults();
	if(!control.CPAO_Infraction_c__c){
		return;
	}
	
	if(Trigger.isBefore && Trigger.isInsert){
		CPAO_InfractionHandler.beforeInsertFilter(Trigger.New);
	}
	if(Trigger.isBefore && Trigger.isUpdate){
		CPAO_InfractionHandler.beforeUpdateFilter(Trigger.New, Trigger.oldMap);
	}

	if(Trigger.isAfter && Trigger.isInsert){
		CPAO_InfractionHandler.afterInsertFilter(Trigger.New);
	}
	if(Trigger.isAfter && Trigger.isUpdate){
		CPAO_InfractionHandler.afterUpdateFilter(Trigger.New, Trigger.oldMap);
	}
	if(Trigger.isAfter && Trigger.isDelete){
		CPAO_InfractionHandler.afterDeleteFilter(Trigger.oldMap);
	}
}