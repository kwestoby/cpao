public without sharing class CPAO_MetadataTypeSelector {
    public static List<Application_Date__mdt> getApplicationDates() {
        return [SELECT CPD_Exemption_Effective_Date__c, CPD_Exemption_Expiry_Date__c, Date_AMD_CPD_Appears_in_Portal__c, 
                Member_Pricing_Reset_Date__c, CPD_Infraction_Date__c, AMD_Late_Fee_Date__c, CPD_Late_Fee_Date__c, 
                AMD_Deferral_Date__c, AMD_Renewal_Date__c,CPAO_Date_PAL_Appears_On_Portal__c,CPAO_PAL_Renewal_Effective_Date__c,
                CPAO_PAL_Renewal_Expiry_Date__c,CPAO_Reset_Employer_is_paying_PALR__c
                FROM Application_Date__mdt WHERE DeveloperName = 'Base'];
    }



    public static List<CPAO_Waiver_Explanation__mdt> getWaiverExplanations() {
        return [SELECT Id, Full_Name__c, CPAO_Explanation__c, CPAO_Percent_Off__c FROM CPAO_Waiver_Explanation__mdt
                WHERE AMD_or_CPD__c = 'AMD'];
    }

    public static List<CPAO_Waiver_Explanation__mdt> getWaiverExplanation(String applicationWaiverType) {
        return [SELECT Id, Full_Name__c, CPAO_Explanation__c, CPAO_Percent_Off__c 
                FROM CPAO_Waiver_Explanation__mdt
                WHERE Full_Name__c = :applicationWaiverType AND AMD_or_CPD__c = 'AMD'];
    }
    
    public static List<Breach_Instruction__mdt> getBreachInstructions() {
        return [SELECT Id, CPAO_Breach_Type__c, CPAO_Breach_Instruction__c 
                FROM Breach_Instruction__mdt];
    }

    public static List<CPAO_Waiver_Explanation__mdt> getCpdWaiverExplanations() {
        return [SELECT Id, Full_Name__c, CPAO_Explanation__c, CPAO_Percent_Off__c, Explanation_Third_Year__c 
                FROM CPAO_Waiver_Explanation__mdt
                WHERE AMD_or_CPD__c = 'CPD'];
    }

    public static List<CPAO_Waiver_Explanation__mdt> getCpdWaiverExplanation(String applicationWaiverType) {
        return [SELECT Id, Full_Name__c, CPAO_Explanation__c, CPAO_Percent_Off__c 
                FROM CPAO_Waiver_Explanation__mdt
                WHERE Full_Name__c = :applicationWaiverType AND AMD_or_CPD__c = 'CPD'];
    }

    public static List<Triennial_Period__mdt> getTrienialPeriods() {
        return [SELECT Id, First_Year__c, Second_Year__c, Third_Year__c FROM Triennial_Period__mdt];
    }

    public static List<Annual_Membership_Due_Line__mdt> getAMDLines() {
        return [SELECT Id, Live_In_Canada_Bermuda__c, Paying_CPAC_Through_CPAO__c, Paying_Provincial_Through_Ontario__c, MasterLabel,
                AMD_Location_Based_Reduction__c  
                FROM Annual_Membership_Due_Line__mdt];
    }

    public static List<Annual_Membership_Due_Line__mdt> getAMDLine(String locationInfo) {
        return [SELECT Id, Live_In_Canada_Bermuda__c, Paying_CPAC_Through_CPAO__c, Paying_Provincial_Through_Ontario__c, MasterLabel,
                AMD_Location_Based_Reduction__c, CPA_Canada_Fee__c, CPA_Ontario_Fee__c
                FROM Annual_Membership_Due_Line__mdt
                WHERE AMD_Location_Based_Reduction__c = :locationInfo];
    }

    public static List<CPAO_SDOC_Templates_Used_In_Applications__mdt> getSDOCS_templatesIds(String developerName) {
        return [SELECT Id, CPAO_COA_Approval_SDOCS_Template_ID__c , CPAO_COA_Licence_Attachment_Template_Id__c,
                       CPAO_PAL_Approval_SDOCS_Template_ID__c,PAL_Licence_Attachment_SDOC_Template_Id__c,CPAO_PAL_Ren_Approval_SDOCS_Template_ID__c
                FROM CPAO_SDOC_Templates_Used_In_Applications__mdt
                WHERE DeveloperName =: developerName];
    }

    public static List<CPAO_RosterPaymentType__mdt> getRosterPaymentType() {
        return [SELECT CPAO_PaymentType__c FROM CPAO_RosterPaymentType__mdt];
    }

    public static CPAO_RosterInstructionScreen__mdt getRosterInstruction() {
        return [SELECT CPAO_RosterInstruction__c FROM CPAO_RosterInstructionScreen__mdt LIMIT 1];
    }

    public static CPAO_PAL_Application_Screen__mdt getPALAppAttestation() {
        return [SELECT CPAO_PAL_Application_Attestation__c FROM CPAO_PAL_Application_Screen__mdt 
                WHERE DeveloperName = 'Base'];
    }

    public static CPAO_PAL_Renewal_Screen__mdt getPALRenewalAttestation() {
        return [SELECT CPAO_PAL_Renewal_Attestation__c FROM CPAO_PAL_Renewal_Screen__mdt 
                WHERE DeveloperName = 'Base'];
    }
}