({
    doInit: function(component,event,helper){
        var escapedString="Quebec CPA Order/L'Ordre des CPA du Quebec";
        var CPA_Quebec="Quebec CPA Order/L Ordre des CPA du Quebec";
        var accountingBodies=component.get('v.options');
        //var unescapedString=accountingBodies[11];
        if(accountingBodies[12].label==CPA_Quebec){
            accountingBodies[12].label=escapedString;
        }
    },
    clickOnNextButton :  function(component,event,helper){
        //NICE TO HAVE: VALIDATE THAT REFERENCE 1 IS DIFERENT FROM REF 2

        var componentNameInEvent=event.getParam("componentName");
        var accountingBody1=component.get('v.currentPAL.CPAO_Membership_Body_Reference_1__c');
        var accountingBody2=component.get('v.currentPAL.CPAO_Membership_Body_Reference_2__c');
        var accountingBody1Valid=true;
        var accountingBody2Valid=true;
        if(componentNameInEvent==component.get('v.nameOfThisComponent')){
            if(event.getParam("nextStepName")=='Exit'){ 
                component.set('v.shouldSaveAndQuit',true);
            }
            var validForm = component.find('PALForm').reduce(function (validSoFar, inputCmp) {
                if(accountingBody1==undefined || accountingBody1=='--Please select an accounting body--'){
                     component.set('v.accounting_body1_required',true);
                    accountingBody1Valid=false;
                }else{
                     component.set('v.accounting_body1_required',true);
                }
                
                if(accountingBody2==undefined || accountingBody2=='--Please select an accounting body--'){
                   component.set('v.accounting_body2_required',true);
                    accountingBody2Valid=false;
                }else{
                    component.set('v.accounting_body2_required',false);
                }
                inputCmp.showHelpMessageIfInvalid();
                return validSoFar && inputCmp.get('v.validity').valid && accountingBody1Valid  && accountingBody2Valid}  , true);
            if(validForm){		
                
                 component.set('v.accounting_body1_required',true);
                component.set('v.accounting_body2_required',false);
                var nextStep=event.getParam('nextStepName');
                //  component.set('v.currentPAL.CPAO_Current_Step__c',nextStep);
                component.set('v.currentPAL',event.getParam('PALApplication'));
                var showSpinnerEvent = component.getEvent("showSpinner");
                showSpinnerEvent.fire();
                helper.upsertPAL(component);
            }		
            
        }
    }
})