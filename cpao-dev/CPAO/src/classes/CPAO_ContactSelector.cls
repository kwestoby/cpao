public without sharing class CPAO_ContactSelector {


	public static List<Contact> getContactToUpdate(Set<Id> contactIdsToCPAIdVal) {
		return[SELECT Id, Salutation, CPAO_Legal_Surname__c, CPAO_Legal_Given__c, DonorApi__Suffix__c, CPAO_Type__c,
		FirstName, LastName, Birthdate, Email, CPAO_CPA_Contact_Type__c, CPAO_CPA_Status__c, CPAO_CPA_Id_Display__c,
		CPAO_Gender__c, Phone, HomePhone, MobilePhone, CPAO_Alternate_Email__c, CPAO_Preferred_Mailing_Address__c,
		MailingAddress, MailingStreet, MailingCity, MailingState, MailingCountry, MailingPostalCode, OtherAddress,
		OtherStreet, OtherCity, OtherState, OtherCountry, OtherPostalCode, MailingStateCode, MailingCountryCode, OtherCountryCode,
		OtherStateCode, CPAO_Preferred_Phone__c, CPAO_CPD_Continuous_Exemption__c, CPAO_AMD_Continuous_Waiver__c,
		CPAO_Subscription_Status_of_Comple_AMD__c, CPAO_Subscription_Status_of_Comple_APF__c, CPAO_Subscription_Status_of_Comple_PAL__c,
		CPAO_Member_news_and_information__c, CPAO_Association_news__c, CPAO_Professional_development_learning__c,
		CPAO_Conferences__c, CPAO_Networking_events__c, CPAO_Volunteer_opportunities__c, CPAO_Member_and_student_benefits__c,
		CPAO_Exam_and_Course_information__c, CPAO_Consent_type__c, CPAO_Employment_Status__c
		FROM Contact
		WHERE Id IN : contactIdsToCPAIdVal];
	}

	public static List<Contact> getLSOContacts(Set<Id> contactsIds) {
		return[SELECT Id, CPAO_CPA_Contact_Type__c, CPAO_Type__c, CPAO_CPA_Status__c
		FROM Contact 
		WHERE Id IN : contactsIds AND CPAO_CPA_Contact_Type__c = 'Member' AND(CPAO_CPA_Status__c = 'Member Active' OR CPAO_CPA_Status__c = 'Active')];
	}

	public static List<Contact> getContactRecords(Set<Id> contactsIds) {
		return[SELECT Id, CPAO_CPA_Contact_Type__c, CPAO_Type__c, CPAO_CPA_Status__c, CPAO_Practice_Inspector__c
		FROM Contact
		WHERE Id IN : contactsIds];
	}
	public static List<Contact> getContactRecords(String type, String status) {
		return[SELECT Id, CPAO_CPA_Contact_Type__c, CPAO_Type__c, CPAO_CPA_Status__c
		FROM Contact
		WHERE CPAO_CPA_Contact_Type__c = : type AND CPAO_CPA_Status__c = : status];
	}

}