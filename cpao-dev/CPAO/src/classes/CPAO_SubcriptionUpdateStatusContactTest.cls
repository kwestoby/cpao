@isTest
private class CPAO_SubcriptionUpdateStatusContactTest {

	@TestSetup
	private static void setupData() {
		CPAO_TestFactory.automationControlSetup();
		Account acc = CPAO_TestFactory.createAccounts(1) [0];
		insert acc;
		Contact con = CPAO_TestFactory.createContacts(acc, 1) [0];
		insert con;
		List<OrderApi__Subscription_Plan__c> plans = new List<OrderApi__Subscription_Plan__c> ();
		plans.add(new OrderApi__Subscription_Plan__c(Name = 'AMD'));
		plans.add(new OrderApi__Subscription_Plan__c(Name = 'PAL'));
		plans.add(new OrderApi__Subscription_Plan__c(Name = 'APF'));
		insert plans;
		List<OrderApi__Subscription__c> subs = new List<OrderApi__Subscription__c> ();
		subs.add(new OrderApi__Subscription__c(OrderApi__Subscription_Plan__c = plans[0].Id,
		                                       OrderApi__Contact__c = con.Id,
		                                       OrderApi__Status__c = 'Pending',
		                                       CPAO_Effective_Date__c = Date.newInstance(2018, 1, 1),
		                                       OrderApi__Activated_Date__c = Date.newInstance(2018, 1, 2),
		                                       OrderApi__Expired_Date__c = Date.newInstance(2018, 5, 2)));
		subs.add(new OrderApi__Subscription__c(OrderApi__Subscription_Plan__c = plans[1].Id,
		                                       OrderApi__Contact__c = con.Id,
		                                       OrderApi__Status__c = 'Pending',
		                                       CPAO_Effective_Date__c = Date.newInstance(2018, 1, 1),
		                                       OrderApi__Activated_Date__c = Date.newInstance(2018, 1, 2),
		                                       OrderApi__Expired_Date__c = Date.newInstance(2018, 5, 2)));
		subs.add(new OrderApi__Subscription__c(OrderApi__Subscription_Plan__c = plans[2].Id,
		                                       OrderApi__Contact__c = con.Id,
		                                       OrderApi__Status__c = 'Pending',
		                                       CPAO_Effective_Date__c = Date.newInstance(2018, 1, 1),
		                                       OrderApi__Activated_Date__c = Date.newInstance(2018, 1, 2),
		                                       OrderApi__Expired_Date__c = Date.newInstance(2018, 5, 2)));
		subs.add(new OrderApi__Subscription__c(OrderApi__Subscription_Plan__c = plans[2].Id,
		                                       OrderApi__Contact__c = con.Id,
		                                       CPAO_Effective_Date__c = Date.newInstance(2011, 1, 1),
		                                       OrderApi__Status__c = 'Pending',
		                                       OrderApi__Activated_Date__c = Date.newInstance(2011, 1, 2),
		                                       OrderApi__Expired_Date__c = Date.newInstance(2011, 5, 2)));
		insert subs;
	}

	@isTest
	private static void testSubscriptionUpdateContactStatus() {
		Test.startTest();
		OrderApi__Subscription__c sub = [SELECT Id FROM OrderApi__Subscription__c WHERE OrderApi__Subscription_Plan__r.Name = 'APF' AND CPAO_Effective_Date__c=:Date.newInstance(2018,1,1)];
		sub.OrderApi__Status__c = 'Active';
		update sub;
		Contact con = [SELECT Id, CPAO_Subscription_Status_of_Comple_APF__c FROM Contact LIMIT 1];
		System.assertEquals('Paid', con.CPAO_Subscription_Status_of_Comple_APF__c);
		sub = [SELECT Id FROM OrderApi__Subscription__c WHERE OrderApi__Subscription_Plan__r.Name = 'APF' AND CPAO_Effective_Date__c=:Date.newInstance(2011,1,1)];
		sub.OrderApi__Status__c = 'Active';
		update sub;
		con = [SELECT Id, CPAO_Subscription_Status_of_Comple_APF__c FROM Contact LIMIT 1];
		System.assertEquals('Submission Outstanding', con.CPAO_Subscription_Status_of_Comple_APF__c);
		sub = [SELECT Id FROM OrderApi__Subscription__c WHERE OrderApi__Subscription_Plan__r.Name = 'AMD'];
		sub.OrderApi__Status__c = 'Active';
		update sub;
		con = [SELECT Id, CPAO_Subscription_Status_of_Comple_AMD__c FROM Contact LIMIT 1];
		System.assertEquals('Paid', con.CPAO_Subscription_Status_of_Comple_AMD__c);
		sub = [SELECT Id FROM OrderApi__Subscription__c WHERE OrderApi__Subscription_Plan__r.Name = 'PAL'];
		sub.OrderApi__Status__c = 'Active';
		update sub;
		con = [SELECT Id, CPAO_Subscription_Status_of_Comple_PAL__c FROM Contact LIMIT 1];
		System.assertEquals('Paid', con.CPAO_Subscription_Status_of_Comple_PAL__c);
		Test.stopTest();
	}
}