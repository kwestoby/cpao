({
	doAction : function(component, event, helper) {
		var app = component.get("v.application");
		console.log(app);
		console.log(app.CPAO_Revoked_for_5_years__c);
		if(!(!app.CPAO_Revoked_for_5_years__c)){
            var yesOrNoYear = app.CPAO_Revoked_for_5_years__c + 'Year';
        	var checkedComp = component.find(yesOrNoYear).set("v.checked", true);
        }

        var map = JSON.parse(app.CPAO_JSON_File_Map__c);
        if(map['courseDocument'] != undefined){
            if(map['courseDocument']['Name'] != undefined){
                component.set('v.fileName',map['courseDocument']['Name']);
            }
        }
        if(map['accountingBody1File'] != undefined){
            if(map['accountingBody1File']['Name'] != undefined){
                component.set('v.fileNamebodydoc1',map['accountingBody1File']['Name']);
            }
        }
        if(map['accountingBody2File'] != undefined){
            if(map['accountingBody2File']['Name'] != undefined){
                component.set('v.fileNamebodydoc2',map['accountingBody2File']['Name']);
            }
        }
        if(map['accountingBody3File'] != undefined){
            if(map['accountingBody3File']['Name'] != undefined){
                component.set('v.fileNamebodydoc3',map['accountingBody3File']['Name']);
            }
        }
        if(map['accountingBody4File'] != undefined){
            if(map['accountingBody4File']['Name'] != undefined){
                component.set('v.fileNamebodydoc4',map['accountingBody4File']['Name']);
            }
        }
	},

	onRadioChange : function(component, event, helper) {
		var newVal = event.getSource().get('v.value');
		var app = component.get("v.application");
		app.CPAO_Revoked_for_5_years__c = newVal;
		component.set("v.application", app);
	},

	clickOnNextButton : function(component,event,helper){
    	var talkingToComponent = event.getParam("componentName");
    	if(talkingToComponent == 'step7'){
    		var app = component.get("v.application");
            var keyList = [];
            var filenameList = [];
            var base64DataList = [];
            var contentTypeList = [];
    		if(app.CPAO_Revoked_for_5_years__c == undefined || app.CPAO_Revoked_for_5_years__c == null){
                helper.methodFailure(component, event, helper, 'Please choose  readmission time period', 'Error');
    			return;
    		}
    		if(app.CPAO_Completed_Courses_Required__c == 'Yes'){
    			var courseInfoInputed = false;
    			if(app.CPAO_Course_Type__c == undefined || app.CPAO_Course_Type__c == null){
    				courseInfoInputed = true;
    			}
    			if(app.CPAO_Course_Completion_Date__c == undefined || app.CPAO_Course_Completion_Date__c == null){
    				courseInfoInputed = true;
    			}
    			if(courseInfoInputed == true){
                    helper.methodFailure(component, event, helper, 'Please fill in course information', 'Error');
    				return;
    			}
    			var filename = component.get('v.fileName');
	    		if(filename == undefined || !filename.replace(/\s/g, '').length){
                    helper.methodFailure(component, event, helper, "please provide an attachment", "Error");
	    			return;
	    		}
                if(component.get('v.fileContents') != null || component.get('v.fileContents') != undefined){
                    keyList.push("courseDocument");
                    filenameList.push(component.get("v.fileName"));
                    base64DataList.push(encodeURIComponent(component.get('v.fileContents')));
                    contentTypeList.push(component.get("v.fileType"));
                }

    		} else if(app.CPAO_Completed_Courses_Required__c == 'No'){
                var fileNamebodydoc1 = component.get('v.fileNamebodydoc1');
                if(!app.CPAO_Name_of_Accounting_Body_1__c){
                    helper.methodFailure(component, event, helper, "At least one accounting body has to be filled in with the attachment", "Error");
                    return;
                }
                if(!app.CPAO_Name_of_Accounting_Body_1__c || !app.CPAO_Date_Membership_Awarded_1__c || 
                    !app.CPAO_Date_Membership_Ended_1__c || !fileNamebodydoc1){
                    helper.methodFailure(component, event, helper, "At least one accounting body has to be filled in with the attachment", "Error");
                    return;
                } else {
                    if(component.get('v.fileContentsbodydoc1') != null || component.get('v.fileContentsbodydoc1') != undefined){
                        keyList.push("accountingBody1File");
                        filenameList.push(fileNamebodydoc1);
                        base64DataList.push(encodeURIComponent(component.get('v.fileContentsbodydoc1')));
                        contentTypeList.push(component.get("v.fileTypebodydoc1"));
                    }
                }
                console.log('marco test 2');
                //console.log(!app.CPAO_Name_of_Accounting_Body_2__c.replace(/\s/g, '').length);
                if(!(!app.CPAO_Name_of_Accounting_Body_2__c)){
                    if(!app.CPAO_Name_of_Accounting_Body_2__c.replace(/\s/g, '').length){
                        var fileNamebodydoc2 = component.get('v.fileNamebodydoc2');
                        if( !app.CPAO_Date_Membership_Awarded_2__c || !app.CPAO_Date_Membership_Ended_2__c || !fileNamebodydoc2){
                            helper.methodFailure(component, event, helper, "Fill out all the information for accounting body 2", "Error");
                            return;
                        } else {
                            if(component.get('v.fileContentsbodydoc2') != null || component.get('v.fileContentsbodydoc2') != undefined){
                                keyList.push("accountingBody2File");
                                filenameList.push(fileNamebodydoc2);
                                base64DataList.push(encodeURIComponent(component.get('v.fileContentsbodydoc2')));
                                contentTypeList.push(component.get("v.fileTypebodydoc2"));
                            }                                
                        }
                    }
                }
                console.log('marco test 3');
                if(!(!app.CPAO_Name_of_Accounting_Body_3__c)){
                    if(!app.CPAO_Name_of_Accounting_Body_3__c.replace(/\s/g, '').length){
                        var fileNamebodydoc3 = component.get('v.fileNamebodydoc3');
                        if(!app.CPAO_Date_Membership_Awarded_3__c || !app.CPAO_Date_Membership_Ended_3__c || !fileNamebodydoc3){
                            helper.methodFailure(component, event, helper, "Fill out all the information for accounting body 3", "Error");
                            return;
                        } else {
                            if(component.get('v.fileContentsbodydoc3') != null || component.get('v.fileContentsbodydoc3') != undefined){
                                keyList.push("accountingBody3File");
                                filenameList.push(fileNamebodydoc3);
                                base64DataList.push(encodeURIComponent(component.get('v.fileContentsbodydoc3')));
                                contentTypeList.push(component.get("v.fileTypebodydoc3"));
                            }
                        }
                    }
                }
                if(!(!app.CPAO_Name_of_Accounting_Body_4__c)){
                    if(!app.CPAO_Name_of_Accounting_Body_4__c.replace(/\s/g, '').length){
                        var fileNamebodydoc4 = component.get('v.fileNamebodydoc4');
                        if(!app.CPAO_Date_Membership_Awarded_4__c || !app.CPAO_Date_Membership_Ended_4__c || !fileNamebodydoc4){
                            helper.methodFailure(component, event, helper, "Fill out all the information for accounting body 4", "Error");
                            return;
                        } else {
                            if(component.get('v.fileContentsbodydoc4') != null || component.get('v.fileContentsbodydoc4') != undefined){
                                keyList.push("accountingBody4File");
                                filenameList.push(fileNamebodydoc4);
                                base64DataList.push(encodeURIComponent(component.get('v.fileContentsbodydoc4')));
                                contentTypeList.push(component.get("v.fileTypebodydoc4"));
                            }
                        }
                    }
                }
                    
            }

            console.log('right before apex action');
    		var apexAction = component.get('c.save5Year');
	        apexAction.setParams({
	        	"keyList" : keyList,
                "fileNameList" : filenameList,
                "base64DataList" : base64DataList,
                "contentTypeList" : contentTypeList,
		    	"currentReadmissionApp": app
		    });
	        apexAction.setCallback(this, function(response) {
	        	var state = response.getState();
	        	console.log('state');
	        	console.log(state);
	        	if(state === "SUCCESS"){
	        		var result = response.getReturnValue();
	        		console.log('result');
	        		console.log(result);
	                if(result == 'Success'){
                        component.set('v.fileContents',null);
                        component.set('v.fileContentsbodydoc1',null);
                        component.set('v.fileContentsbodydoc2',null);
                        component.set('v.fileContentsbodydoc3',null);
                        component.set('v.fileContentsbodydoc4',null);
	                	var actionToTake = event.getParam("nextStepName");
                        var applicationHasChanged = component.getEvent("applicationHasChanged");
                        applicationHasChanged.setParams({
                            "nextButtonClickeable" : true,
                            "action" : actionToTake});
				        applicationHasChanged.fire();
	                } else {
                        helper.methodFailure(component, event, helper);
	                }                
	            } else {
                    helper.methodFailure(component, event, helper);
                }
	        });
	        $A.enqueueAction(apexAction);
    	}        
    },

    handleFilesChange: function (component, event,helper) {
    	console.log('file input start');
        var fileInput = event.getSource().get("v.files")[0];
        component.set('v.fileInput',fileInput);
        component.set('v.fileName',fileInput.name);
        component.set('v.SpecialConsiderationName',fileInput.name);
        component.set('v.fileType',fileInput.type);
        
        var fr = new FileReader();
        var self = this;
        
        fr.onload = function() {
            var fileContents = fr.result;
            var base64Mark = 'base64,';
            var dataStart = fileContents.indexOf(base64Mark) + base64Mark.length;
            fileContents = fileContents.substring(dataStart);
            component.set('v.fileContents',fileContents);
        };
        
        fr.readAsDataURL(fileInput);
    },

    handleFilesChangeVar: function (component, event,helper) {
        var fileInput = event.getSource().get("v.files")[0];
        var fileFor = event.getSource().get("v.name");
        var fileInputString = 'v.fileInput' + fileFor;
        var fileNameString = 'v.fileName' + fileFor;
        var fileTypeString = 'v.fileType' + fileFor;
        var fileContentsString = 'v.fileContents' + fileFor;
        component.set(fileInputString,fileInput);
        component.set(fileNameString,fileInput.name);
        component.set(fileTypeString,fileInput.type);
        
        var fr = new FileReader();
        var self = this;
        
        fr.onload = function() {
            var fileContents = fr.result;
            var base64Mark = 'base64,';
            var dataStart = fileContents.indexOf(base64Mark) + base64Mark.length;
            fileContents = fileContents.substring(dataStart);
            component.set(fileContentsString,fileContents);
        };
        
        fr.readAsDataURL(fileInput);
    },
})