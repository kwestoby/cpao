({
    
    clickOnNextButton : function(component,event,helper){
        var componentNameInEvent=event.getParam("componentName");
        
        if(componentNameInEvent==component.get('v.nameOfThisComponent')){
            if(event.getParam("nextStepName")=='Exit'){ 
                component.set('v.shouldSaveAndQuit',true);
            }
            var validForm = component.find('PALForm').reduce(function (validSoFar, inputCmp) {
                var cpd_declaration=  component.get('v.currentPAL.CPAO_CPD_Declaration__c');  
                var cpd_req= component.find('cpd_req');
                // Displays error messages for invalid fields
                if(cpd_declaration==undefined){
                    //  alert('undefined');
                    $A.util.addClass(cpd_req, 'slds-show');
                }else{
                    // $A.util.addClass(error_role_in_PA, 'slds-hide');
                    $A.util.removeClass(cpd_req, 'slds-show');
                    // alert('dfined');
                }
                
                inputCmp.showHelpMessageIfInvalid();
                return validSoFar && inputCmp.get('v.validity').valid && cpd_declaration!=undefined ;
            }, true);
            
            if(validForm){
                var cpd_req= component.find('cpd_req');
                $A.util.removeClass(cpd_req, 'slds-show');
                component.set('v.currentPAL',event.getParam('PALApplication'));
                
                console.log('click on next event fired  CPD ');
                var nextStep=event.getParam('nextStepName');
                //component.set('v.currentPAL.CPAO_Current_Step__c',nextStep);
                
                
                //console.log('cpd declaration is:  '+ component.get('v.currentPAL').CPAO_CPD_Declaration__c);
                var showSpinnerEvent = component.getEvent("showSpinner");
                showSpinnerEvent.fire();
                helper.upsertPAL(component);
            }
        }
        
        
    }
    
})