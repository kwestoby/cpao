@isTest
private class CPAO_FirstApproverRequestHelperTest{
    
    public static List<Account> accounts;
    public static List<Contact> contacts;
    public static User communityUser;
    
    public static void dataInitialize(Integer contactAmount) {
        accounts = CPAO_TestFactory.createAccounts(2);
        accounts[1].Name = 'Retirement';
        insert accounts;
        contacts = CPAO_TestFactory.createContacts(accounts[0], contactAmount);
        insert contacts;
        communityUser = CPAO_TestFactory.createCommunityUser(contacts[1].Id);
        insert communityUser;
        OrderApi__Business_Group__c businessGroup = new OrderApi__Business_Group__c(Name = 'Test BG');
        insert businessGroup;
        OrderApi__Item_Class__c itemClass = new OrderApi__Item_Class__c(Name = 'Test item class',
                                                                    OrderApi__Business_Group__c = businessGroup.Id);
        insert itemClass;
        OrderApi__Item__c item = new OrderApi__Item__c(Name = 'Test item',
                                                   OrderApi__Business_Group__c = businessGroup.Id,
                                                   OrderApi__Item_Class__c = itemClass.Id);
        insert item;
    }
    
    @isTest
    public static void basicTest() {
        dataInitialize(2);
        
    List<Group> assignQueue = new List<Group>();
    assignQueue.add(new Group(Type = 'Queue', NAME = 'CPAO Registrar Office (Registrar)'));
    insert assignQueue;
    
    OrderApi__Receipt__c  receipt = new OrderApi__Receipt__c ();
    receipt.OrderApi__Contact__c = contacts[0].Id;
    receipt.OrderApi__Account__c = accounts[0].Id;
    receipt.OrderApi__Type__c = 'Refund';
    insert receipt;

    }

 
}