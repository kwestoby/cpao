@isTest
private class LookupSObjectControllerTest {

	@isTest
	private static void testLookup() {
		Contact con = new Contact(FirstName = 'Test',
		                          LastName = 'Contact',
		                          CPAO_CPA_Contact_Type__c = 'Member',
		                          CPAO_CPA_Status__c = 'Active');
		insert con;
		Test.setFixedSearchResults(new List<String>{con.Id});
		Test.startTest();
		LookupSObjectController.Result[] results = LookupSObjectController.lookup('Test', 'Contact');
		Test.stopTest();
		System.assertEquals(1, results.size());
		System.assertEquals('Test Contact', results[0].SObjectLabel);
	}
}