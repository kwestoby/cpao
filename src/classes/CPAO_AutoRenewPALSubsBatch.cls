global class CPAO_AutoRenewPALSubsBatch implements Database.Batchable<sObject> {

	public static string PAL_SUBSC_TYPE_NAME='Public Accounting Licence';
	public static Set<Id> contactsIds= new Set<Id>();
	public static String MEMBER='Member';
	public static String PAL_RENEWAL_RECORD_TYPE_NAME='CPAO_PAL_Renewal';
	public static List<RecordType>  PAL_RENEWAL_RECORD_TYPE= CPAO_ApplicationSelector.getRecordType(PAL_RENEWAL_RECORD_TYPE_NAME);
	public static String OPEN='Open';
	public static String ACTIVE='Active';
	public static String PAL_LICENCE='Public Accounting Licence';

	global Database.QueryLocator start(Database.BatchableContext BC) {
		Integer currentYear = date.today().year(); 
	
		OrderApi__Subscription_Plan__c pal_subscription_plan = new OrderApi__Subscription_Plan__c();
 		List<OrderApi__Subscription_Plan__c> pal_subscription_plans = CPAO_FontevaObjectSelector.getSubscriptionPlan(PAL_SUBSC_TYPE_NAME);
 		if(!pal_subscription_plans.isEmpty()){
 			pal_subscription_plan=pal_subscription_plans[0];
 		}

		/*String query='SELECT Id,OrderApi__Contact__c,OrderApi__Subscription_Plan__c, OrderApi__Contact__r.CPAO_CPA_Contact_Type__c,OrderApi__Contact__c.CPAO_CPA_Status__c'+
		'FROM OrderApi__Subscription__c WHERE OrderApi__Subscription_Plan__c=:pal_subscription_plan.Id AND OrderApi__Contact__r.CPAO_CPA_Contact_Type__c =:MEMBER AND'+
		 ' OrderApi__Contact__c.CPAO_CPA_Status__c=:MEMBER_ACTIVE';
		System.debug(query);*/
		/*List<Contact> activeMembersWithActiveLicence=CPAO_ContactSelector.getContactRecords(MEMBER,ACTIVE);
		Set<Id>idActiveMembers=new Set<Id>();
		for(Contact contact:activeMembers){
			idActiveMembers.add(contact.Id);
		}*/
		List<CPAO_Licence__c>activeMembersWithActiveLicence=[
		SELECT Id, CPAO_Licence_Type__c,CPAO_Issued_to__c,CPAO_Status__c,CPAO_Issued_to__r.CPAO_CPA_Contact_Type__c,
			   CPAO_Issued_to__r.CPAO_CPA_Status__c
		FROM CPAO_Licence__c
		WHERE CPAO_Licence_Type__c=:PAL_LICENCE AND CPAO_Status__c = :ACTIVE AND CPAO_Issued_to__r.CPAO_CPA_Contact_Type__c=: MEMBER
			AND CPAO_Issued_to__r.CPAO_CPA_Status__c =:ACTIVE];
		Set<Id>idactiveMembersWithActiveLicence=new Set<Id>();
		for( CPAO_Licence__c licence:activeMembersWithActiveLicence){
			idactiveMembersWithActiveLicence.add(licence.CPAO_Issued_to__c);
		}
		return Database.getQueryLocator([
			SELECT Id, Name,OrderApi__Subscription_Plan__c,OrderApi__Contact__c
               	FROM OrderApi__Subscription__c
               	WHERE OrderApi__Subscription_Plan__c =:pal_subscription_plan.Id AND OrderApi__Contact__c IN :idactiveMembersWithActiveLicence

			]);
			/*return Database.getQueryLocator([
			SELECT Id, Name,CPAO_CPA_Contact_Type__c,CPAO_CPA_Status__c
           	FROM Contact
           	WHERE Id IN(SELECT CPAO_Issued_to__c,CPAO_Licence_Type__c,CPAO_Status__c FROM CPAO_Licence__c WHERE CPAO_Licence_Type__c=:PAL_LICENCE AND CPAO_Status__c=:ACTIVE)
               		  AND Id IN(OrderApi__Subscription_Plan__c,OrderApi__Contact__c	FROM OrderApi__Subscription__c WHERE OrderApi__Subscription_Plan__c=:pal_subscription_plan.Id)
               		  AND CPAO_CPA_Contact_Type__c=:MEMBER ANDCPAO_CPA_Status__c=:ACTIVE

			]);*/
			}

   	global void execute(Database.BatchableContext BC, List<OrderApi__Subscription__c> palSubcriptions) {
		if(palSubcriptions.isEmpty()){
			return;
		}
    	List<OrderApi__Renewal__c> renewalTerms = new List<OrderApi__Renewal__c>();
    	for(OrderApi__Subscription__c individualSubcription:palSubcriptions){
    		OrderApi__Renewal__c newTerm=new OrderApi__Renewal__c();
    		newTerm.OrderApi__Subscription__c=individualSubcription.Id;
    		newTerm.OrderApi__Is_Active__c=false;
    		renewalTerms.add(newTerm);
    	}
    	insert renewalTerms;
	}
	
	global void finish(Database.BatchableContext BC) {
		
	}

	public static Boolean eligibleForRenewal(OrderApi__Subscription__c individualSubcription){
		
		return true;

	}
	
}