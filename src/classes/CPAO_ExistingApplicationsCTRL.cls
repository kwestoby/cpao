public without sharing class CPAO_ExistingApplicationsCTRL {
	public static final String ACTIVE = 'Active';
	public static final String AMD_CPD = 'AMD/CPD';
	public static String PAL_RENEWAL_RECORD_TYPE_NAME='CPAO_PAL_Renewal';
	public static string PAL_SUBSC_TYPE_NAME='Public Accounting Licence';



	@AuraEnabled
	public static List<CPAO_Application__c> getApplications() {
		
		List<String> statuses = new List<String>{'Open','Submitted not paid','Additional Information Required','In Review','Completed','Withdrawn','Cancelled','Closed','Submitted', 'Withdrawal Requested'};
		return CPAO_ApplicationSelector.getAllNonAMDApplication(statuses);//getUserApplicationsWithStatuses(statuses);
	}

	@AuraEnabled
	public static List<CPAO_Application__c> getAllApplications(){
		return CPAO_ApplicationSelector.getAllUserApplications();
	}


	@AuraEnabled
	public static renewalPageWrapper getRenewals() {
		renewalPageWrapper results = new renewalPageWrapper();
		List<String> recordTypes = new List<String>{AMD_CPD};
		List<Application_Date__mdt> applicationDate = CPAO_MetadataTypeSelector.getApplicationDates();
		Integer currentYear = Integer.valueOf(Date.Today().year());
		Integer appearMonth = applicationDate[0].Date_AMD_CPD_Appears_in_Portal__c.Month();
		Integer appearDay = applicationDate[0].Date_AMD_CPD_Appears_in_Portal__c.Day();
		Date dateAppsAppearInPortal = Date.newInstance(currentYear, appearMonth, appearDay);
		List<CPAO_Application__c> applicationsToReturn = new List<CPAO_Application__c>();

		Boolean returnTerm = true;
		for(CPAO_Application__c app:CPAO_ApplicationSelector.getUserApplicationsByRecordtypeName(recordTypes)){
			if(app.RecordType.Name == AMD_CPD){
				if(dateAppsAppearInPortal <= Date.today()){
					applicationsToReturn.add(app);
				}
			} else {
				applicationsToReturn.add(app);
			}
		}
		results.renewalApplications = applicationsToReturn;
		
		List<OrderApi__Renewal__c> termsToReturn = CPAO_FontevaObjectSelector.getRenewalTermsToShow(currentYear);
		if(!termsToReturn.isEmpty()){
			results.relevantTerms = termsToReturn;
		}

		List<OrderApi__Renewal__c> palTerms=CPAO_FontevaObjectSelector.getRenewalTermsToShow(currentYear,PAL_SUBSC_TYPE_NAME);
		if(!palTerms.isEmpty()){
			results.PalRenewalTerms=palTerms;
		}

		return results;
	}

	@AuraEnabled
	public static List<CPAO_Application__c> getDeclarations() {

		return CPAO_ApplicationSelector.getUserPoaDeclarations();
	}

	@AuraEnabled
	public static startApplicationWrapper startApplication(OrderApi__Renewal__c term) {
		startApplicationWrapper result = new startApplicationWrapper();
		Id amdCPDRecordTypeid = CPAO_ApplicationHandler.getApplicationRecordTypeId(CPAO_ApplicationHandler.AMD_CPD);
		CPAO_Application__c application = new CPAO_Application__c(
			recordTypeId = amdCPDRecordTypeid,
			CPAO_Status__c = 'Open',
			CPAO_Contact__c = term.OrderApi__Subscription__r.OrderApi__Contact__c,
			CPAO_Subscription__c = term.OrderApi__Subscription__c);
		
		term.CPAO_Hide_Term__c = true;
		try{
			insert application;
			update term;
		} catch(Exception e) {
			result.status = e.getMessage();
			return result;
		}

		result.status = 'Success';
		result.application = [SELECT Id, CPAO_Form_URL__c FROM CPAO_Application__c WHERE Id = :application.Id];
		return result;
	}

	@AuraEnabled
	public static void cancelApplication(Id applicationId) {
		CPAO_Application__c application = new CPAO_Application__c(Id = applicationId);
		application.CPAO_Status__c = 'Cancelled';
		update application;
	}



	@AuraEnabled
	public static String withdrawApplication(Id applicationId) {
		CPAO_Application__c application = new CPAO_Application__c(Id = applicationId);
		application.CPAO_Status__c = 'Withdrawal Requested';
		List<Group> roQueue = CPAO_GroupSelector.getROStaffQueue();
		if(roQueue.size() == 1){
			application.OwnerId = roQueue[0].Id;
		}

		try{
			update application;
		} catch(Exception e) {
			return e.getMessage();
		}
		return 'Success';
	}

	@AuraEnabled
	public static CPAO_AMDRenewalSalesOrderprocessing.attestationWrapper finishDeferredApp(CPAO_Application__c application) {
		System.debug('Marco test: ');
		return CPAO_AMDRenewalSalesOrderprocessing.saveApplicationAttestation(application);

	}

	@AuraEnabled
	public static String clickOnRenewal() {
		return null;
	}

	public class renewalPageWrapper{
		@AuraEnabled public List<CPAO_Application__c> renewalApplications{get;set;}
		@AuraEnabled public List<OrderApi__Renewal__c> relevantTerms{get;set;}
		@AuraEnabled public List<OrderApi__Renewal__c> PalRenewalTerms{get;set;}
	}

	public class startApplicationWrapper{
		@AuraEnabled public CPAO_Application__c application{get;set;}
		@AuraEnabled public String status{get;set;}
	}

}