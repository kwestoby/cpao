<?xml version="1.0" encoding="UTF-8"?>
<CustomMetadata xmlns="http://soap.sforce.com/2006/04/metadata" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xmlns:xsd="http://www.w3.org/2001/XMLSchema">
    <label>Parenting/Family Care</label>
    <protected>false</protected>
    <values>
        <field>AMD_or_CPD__c</field>
        <value xsi:type="xsd:string">AMD</value>
    </values>
    <values>
        <field>CPAO_Explanation__c</field>
        <value xsi:type="xsd:string">Members who leave full-time employment for the purpose of parenting or providing full-time are to an elderly or ill family member may apply for a 50% reduction if the member’s:
 - Net equity is less than $200,000; 
 - Gross income from all sources (including employment insurance benefits, investments, etc.) is less than $32,364; and
 - Full-time care is being provided to a child who is six years of age or less as of April 1, or who is over six and has a disability or illness; or full-time care is being provided for an elderly or ill family member who is unable to care for themselves.
Two free Professional development days are granted to every member who qualifies for a parenting or family care reduction.  Courses must be taken within the membership year that the reduction is granted.</value>
    </values>
    <values>
        <field>CPAO_Percent_Off__c</field>
        <value xsi:type="xsd:double">50.0</value>
    </values>
    <values>
        <field>Explanation_Third_Year__c</field>
        <value xsi:nil="true"/>
    </values>
    <values>
        <field>Full_Name__c</field>
        <value xsi:type="xsd:string">Parenting/Family Care</value>
    </values>
</CustomMetadata>
