@isTest
private class CPAO_RosterManagementTest {

	@TestSetup
	private static void setupData() {
		CPAO_TestFactory.automationControlSetup();
		Account acc = CPAO_TestFactory.createAccounts(1) [0];
		insert acc;
		Contact con = CPAO_TestFactory.createContacts(acc, 1) [0];
		con.CPAO_CPA_Contact_Type__c = 'Member';
		con.CPAO_CPA_Status__c = 'Active';
		con.CPAO_CPA_Id_Display__c='1234';
		insert con;
		List<OrderApi__Badge_Type__c> badgeTypes = new List<OrderApi__Badge_Type__c> ();
		badgeTypes.add(new OrderApi__Badge_Type__c(Name = 'PAL'));
		badgeTypes.add(new OrderApi__Badge_Type__c(Name = 'LSO'));
		badgeTypes.add(new OrderApi__Badge_Type__c(Name = 'Primary Contact'));
		insert badgeTypes;
		List<OrderApi__Badge__c> badges = new List<OrderApi__Badge__c> ();
		badges.add(new OrderApi__Badge__c(OrderApi__Badge_Type__c = badgeTypes[0].Id, OrderApi__Contact__c = con.Id));
		badges.add(new OrderApi__Badge__c(OrderApi__Badge_Type__c = badgeTypes[1].Id, OrderApi__Contact__c = con.Id));
		insert badges;
		CPAO_Account_Contact_Relationship__c acRelation = CPAO_TestFactory.createAccountContactRelationships(acc, con, 1) [0];
		acRelation.CPAO_Primary_Employer__c = 'Yes';
		insert acRelation;
		acc.CPAO_LSO__c = con.Id;
		acc.OrderApi__Primary_Contact__c = con.Id;
		update acc;
		CPAO_Application__c app = CPAO_TestFactory.createPALApplication(1, con) [0];
		app.CPAO_Status__c = 'Submitted';
		insert app;
	}

	@isTest
	private static void testGetUser() {
		Contact con = [SELECT Id FROM Contact LIMIT 1];
		User communityUser = CPAO_TestFactory.createCommunityUser(con.Id);
		System.runAs(communityUser) {
			Test.startTest();
			User usr = CPAO_RosterManagement.getUser();
			System.assertEquals(con.Id, usr.ContactId);
			Test.stopTest();
		}
	}

	@isTest
	private static void testGetAccountNames() {
		Contact con = [SELECT Id, Name FROM Contact LIMIT 1];
		User communityUser = CPAO_TestFactory.createCommunityUser(con.Id);
		System.runAs(communityUser) {
			Test.startTest();
			List<Account> accounts = CPAO_RosterManagement.getAccountNames();
			System.assert(accounts.size() > 0);
			Test.stopTest();
		}
	}

	@isTest
	private static void testGetAccountDetails() {
		Contact con = [SELECT Id, Name FROM Contact LIMIT 1];
		User communityUser = CPAO_TestFactory.createCommunityUser(con.Id);
		Account acc = [SELECT Id FROM Account LIMIT 1];
		System.runAs(communityUser) {
			Test.startTest();
			List<Account> accounts = CPAO_RosterManagement.getAccountDetails('["' + acc.Id + '"]');
			System.assert(accounts.size() > 0);
			Test.stopTest();
		}
	}

	@IsTest
	private static void testQueryMDT() {
		Test.startTest();
		System.assert(CPAO_RosterManagement.queryAllPaymentTypes() != null);
		System.assert(CPAO_RosterManagement.queryInstructionScreen() != null);
		Test.stopTest();
	}

	@isTest
	private static void testGetContactNames() {
		Account acc = [SELECT Id FROM Account LIMIT 1];
		Test.startTest();
		List<Contact> contacts = CPAO_RosterManagement.getContactNames('PAL', '["' + acc.Id + '"]');
		System.assert(contacts.size() > 0);
		Test.stopTest();
	}

	@isTest
	private static void testSearchMethod() {
		Test.startTest();
		System.assert(CPAO_RosterManagement.searchmethod('1234').size() > 0);
		Test.stopTest();
	}

	@IsTest
	private static void testSavePayment() {
		Contact con = [SELECT Id, Name FROM Contact LIMIT 1];
		OrderApi__Receipt__c receipt = new OrderApi__Receipt__c(OrderApi__Contact__c = con.id,
		                                                        RecordTypeId = Schema.SObjectType.OrderApi__Receipt__c.getRecordTypeInfosByName().get('Payment').getRecordTypeId());
		Test.startTest();
		CPAO_RosterManagement.savePayment(new List<OrderApi__Receipt__c> { receipt });
		System.assert(receipt.Id != null);
		Test.stopTest();
	}

	@isTest
	private static void testSaveMarkedContact() {
		Contact con = [SELECT Id FROM Contact LIMIT 1];
		Test.startTest();
		System.assert(CPAO_RosterManagement.saveMarkedContacts('["' + con.Id + '"]').size() > 0);
		Test.stopTest();
	}
}