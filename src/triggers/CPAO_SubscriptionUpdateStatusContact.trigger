trigger CPAO_SubscriptionUpdateStatusContact on OrderApi__Subscription__c (before update) {

    CPAO_Automation_Control__c control = CPAO_Automation_Control__c.getOrgDefaults();
    if(!control.OrderApi_Subscription_c__c){
        return;
    }
    
    Map<Id, OrderApi__Subscription__c > subToEvaluate = new Map<Id, OrderApi__Subscription__c>();
    if(Trigger.isBefore&& Trigger.isUpdate){
        for(OrderApi__Subscription__c subObj: Trigger.New){
                if(subObj.OrderApi__Status__c == 'Active')
                    subToEvaluate.put(subObj.Id,subObj);
            }
    }       
    CPAO_orderSubUpdateStatusInContactHelper.updatesubOrderContact(subToEvaluate);
}