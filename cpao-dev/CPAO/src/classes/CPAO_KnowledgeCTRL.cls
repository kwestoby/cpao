public class CPAO_KnowledgeCTRL {
	@AuraEnabled
	public static KnowledgeQueryResponse getKnowledgeArticles(String query) {
		try {
			KnowledgeQuery kQuery = (KnowledgeQuery) Json.deserialize(query, KnowledgeQuery.class);
			ApexPages.StandardSetController stdSetCtrl = new ApexPages.StandardSetController(kQuery.getQueryLocator());
			stdSetCtrl.setPageSize(kQuery.getPageSize());
			stdSetCtrl.setPageNumber(kQuery.getPageNumber());
			return new KnowledgeQueryResponse((List<Knowledge__kav>) stdSetCtrl.getRecords(), stdSetCtrl.getResultSize());
		} catch(Exception e) {
			throw new AuraHandledException('Error! ' + e.getMessage());
		}
	}

	public class KnowledgeQuery {
		private String searchKey;
		private Integer pageSize;
		private Integer pageNumber;
		private String sortBy;
		private String sortOrder;

		public Database.QueryLocator getQueryLocator() {
			String sql = 'Select Id, Title, Summary, ArticleCreatedDate, UrlName, Question__c, Answer__c FROM Knowledge__kav ';
			sql += 'WHERE PublishStatus=\'Online\' AND IsVisibleInCsp=true ';
			Set<Id> searchIdSet = new Set<Id> ();
			if (!String.isEmpty(searchKey)) {
				searchKey = String.escapeSingleQuotes(searchKey);
				List<List<SObject>> searchResults = [FIND :searchKey IN ALL FIELDS RETURNING Knowledge__kav(Id)];
				for (SObject result : searchResults[0]) {
					searchIdSet.add(result.Id);
				}
				sql += 'AND ID IN :searchIdSet ';
			}
			if (!String.isEmpty(sortBy)) {
				sql += 'ORDER BY ' + String.escapeSingleQuotes(sortBy);
			}
			if (!String.isEmpty(sortBy) && !String.isEmpty(sortOrder)) {
				sql += ' ' + String.escapeSingleQuotes(sortOrder);
			}
			return Database.getQueryLocator(sql);
		}

		public Integer getPageNumber() {
			return pageNumber;
		}

		public Integer getPageSize() {
			return pageSize;
		}
	}

	public class KnowledgeQueryResponse {
		@AuraEnabled public List<Knowledge__kav> articles;
		@AuraEnabled public Integer totalRecords;

		public KnowledgeQueryResponse(List<Knowledge__kav> articles, Integer totalRecords) {
			this.articles = articles;
			this.totalRecords = totalRecords;
		}
	}
}