public class CPAO_MilestoneAccountHelper {

    public static void historyTracker(Map<Id, Account>  accObjMap){


        Map<String,String> triggerPlusCopyFieldMap = new Map<String,String>();
        List<String> triggerfields = new List<String>();
        List<String> copyfields = new List<String>();

        for(CPAO_History_Tracker_Controller__c historytrackerController : [Select Id, name,CPAO_Fields_to_Copy__c, CPAO_ObjectType__c,CPAO_Trigger_Fields__c,CPAO_Active__c from CPAO_History_Tracker_Controller__c where CPAO_ObjectType__c =: 'Account' and CPAO_Active__c =: True]){
            triggerfields = historytrackerController.CPAO_Trigger_Fields__c.split(',');
            copyfields  = historytrackerController.CPAO_Fields_to_Copy__c.split(',');
            system.debug('triggerfields'+triggerfields );
            //triggerPlusCopyFieldMap.put(globalhistoryController.CPAO_Trigger_Fields__c,globalhistoryController.CPAO_Fields_to_Copy__c);

        }
        
        List<CPAO_MilestoneAndHistory__c> listHistory = new List<CPAO_MilestoneAndHistory__c>();


        for(Account accObj: accObjMap.values()){

            for(integer i=0; i<triggerfields.size(); i++){
                string fieldAPI = triggerfields[i];
                


                String oldNameValue     = (String)trigger.oldMap.get(accObj.Id).get('Name');
                String currentNameValue = (String)accObj.Name;

                if(!oldNameValue.equals(currentNameValue) ){

                    id recordTypeId = Schema.SObjectType.CPAO_MilestoneAndHistory__c.getRecordTypeInfosByName().get('Account Name').getRecordTypeId();
                    CPAO_MilestoneAndHistory__c history = copyFieldsHistory(accObj, copyfields);
                    history.CPAO_Account_Firm_Name__c=oldNameValue;
                    history.RecordTypeId= recordTypeId;
                    listHistory.add(history);

                }
                if(trigger.oldMap.get(accObj.Id).get(fieldAPI)!=null){

                    if( trigger.oldMap.get(accObj.Id).get(fieldAPI) != accObj.get(fieldAPI) ){
                        CPAO_MilestoneAndHistory__c history = copyFieldsHistory(accObj, copyfields);
                        listHistory.add(history);
                        break;
                    }
                }
            }           

        }
        if(listHistory.size()>0){
            insert listHistory;
        }
    }  



    public static CPAO_MilestoneAndHistory__c copyFieldsHistory(Account accObj, List<String> listFields){

        id recordTypeId = Schema.SObjectType.CPAO_MilestoneAndHistory__c.getRecordTypeInfosByName().get('Account Milestone').getRecordTypeId();
        CPAO_MilestoneAndHistory__c history = new CPAO_MilestoneAndHistory__c();
        history.CPAO_Account__c = accObj.Id;
        history.RecordTypeId= recordTypeId;
        for(integer i=0; i<listFields.size(); i++){
            string fieldAPI = listFields[i];
                //system.debug('fieldAPI'+fieldAPI);
                //String oldFieldValue = trigger.oldMap.get(accObj.Id)+'.'+fieldAPI;
                history.CPAO_Account_Firm_Name__c = accObj.Name;

                history.put(fieldAPI,trigger.oldMap.get(accObj.Id).get(fieldAPI) );


            }
            return history;
        }

    }