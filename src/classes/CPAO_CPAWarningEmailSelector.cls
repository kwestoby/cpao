public with sharing class CPAO_CPAWarningEmailSelector {
	
	public static List<CPA_Id_Warning_Email__c> getCurrentEmailDetails() {
		return [SELECT Warning_Email_send_to_address__c, Warning_email_threshold__c
		,CPA_Warning_Email_Subject__c,CPA_Warning_Email_Body__c FROM CPA_Id_Warning_Email__c ];
	}
}