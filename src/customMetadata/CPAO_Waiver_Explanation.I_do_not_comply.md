<?xml version="1.0" encoding="UTF-8"?>
<CustomMetadata xmlns="http://soap.sforce.com/2006/04/metadata" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xmlns:xsd="http://www.w3.org/2001/XMLSchema">
    <label>I do not comply</label>
    <protected>false</protected>
    <values>
        <field>AMD_or_CPD__c</field>
        <value xsi:type="xsd:string">CPD</value>
    </values>
    <values>
        <field>CPAO_Explanation__c</field>
        <value xsi:type="xsd:string">You have not completed the minimum CPD hours as required in CPA Ontario CPD Regulations. You agree to complete the remaining annual CPD requirements within 120 days of submission of your plan of action in accordance with CPA Ontario CPD Regulations.</value>
    </values>
    <values>
        <field>CPAO_Percent_Off__c</field>
        <value xsi:nil="true"/>
    </values>
    <values>
        <field>Explanation_Third_Year__c</field>
        <value xsi:type="xsd:string">You have not completed the minimum CPD hours as required in CPA Ontario CPD Regulations. You agree to complete the remaining annual CPD requirements within 120 days of submission of your plan of action in accordance with CPA Ontario CPD Regulations.</value>
    </values>
    <values>
        <field>Full_Name__c</field>
        <value xsi:type="xsd:string">I do not comply</value>
    </values>
</CustomMetadata>
