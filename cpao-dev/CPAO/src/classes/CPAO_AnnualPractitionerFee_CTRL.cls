public without sharing class CPAO_AnnualPractitionerFee_CTRL {
    @TestVisible private static final String CPAO_APF = 'CPAO_APF';
    @TestVisible private static final String OPEN = 'Open';
    @TestVisible private static final String ACTIVE = 'Active';
    @TestVisible private static final String CANNOT_SAVE = 'Unable to save record';
    @TestVisible private static final String APF_INSTRUCTIONS = 'AnnualPractitionerFee';
    @TestVisible private static final String DEFAULT_STEP = 'step1';
    @TestVisible private static final List<RecordType> APF_APPLICATION_RECORD_TYPE = [SELECT Id FROM RecordType WHERE DeveloperName =: CPAO_APF];
    
    @TestVisible public static User LOGGED_USER {
        get {
            if(LOGGED_USER == null) {
                LOGGED_USER = CPAO_UserSelector.getCurrentUser();
            }
            return LOGGED_USER;
        }
        set;
    }

    // 
    //@AuraEnabled
    //public static FirmStructure getFirmStructure() {
    //    FirmStructure struct = new FirmStructure();
    //    struct.operatingStatusOptions.add(new CPAO_PicklistWrapper('Full-time', 'Full-time practice'));
    //    struct.operatingStatusOptions.add(new CPAO_PicklistWrapper('Regional', 'Regional (more than one office in Ontario and up to two other provinces)'));

    //    struct.firmLocalityOptions.add(new CPAO_PicklistWrapper('Local', 'Local (one office only)'));
    //    struct.firmLocalityOptions.add(new CPAO_PicklistWrapper('Regional', 'Regional (more than one office in Ontario; or office in Ontario and up to two other provinces)'));
    //    struct.firmLocalityOptions.add(new CPAO_PicklistWrapper('National', 'National (offices in more than three provinces or territories)'));

    //    struct.firmServicesOptions.add(new CPAO_PicklistWrapper('Accounting', 'Providing accounting services to the public in Ontario'));
    //    struct.firmServicesOptions.add(new CPAO_PicklistWrapper('Engages', 'Engages in the practice of public accounting in Ontario (PAL required for all Firm structures)'));
    //    struct.firmServicesOptions.add(new CPAO_PicklistWrapper('National', 'National (offices in more than three provinces or territories)'));
    //    struct.firmServicesOptions.add(new CPAO_PicklistWrapper('Agreement', 'Providing services to a Firm (service agreement or per diem / contract work)'));
    //    struct.firmServicesOptions.add(new CPAO_PicklistWrapper('Partner', 'Parter in a Firm (providing services through another Firm)'));
    //    struct.firmServicesOptions.add(new CPAO_PicklistWrapper('Service', 'Service agreement or per diem / contract work (providing services to a Firm)'));

    //    return struct;
    //}

    @AuraEnabled
    public static Map<String, Question> getQuestionnaire(CPAO_Application__c currentAPF) {
        Map<String, Question> questionMap = new Map<String, Question>();
        for(APF_Question__mdt question : [SELECT Field_API_Name__c, Field_Type__c, Question__c, Options__c FROM APF_Question__mdt]) {
            String field = question.Field_API_Name__c;
            String label = question.Question__c;

            List<CPAO_PicklistWrapper> options;
            if(String.isNotBlank(question.Options__c)) {
                options = parseOptions(question.Options__c);
            }
            String fieldType = question.Field_Type__c;

            questionMap.put(field, new Question(field, label, options));
        }
        return questionMap;
    }

    private static List<CPAO_PicklistWrapper> parseOptions(String optionLongText) {
        List<String> results = optionLongText.split('\n');
        List<CPAO_PicklistWrapper> wrappers = new List<CPAO_PicklistWrapper>();
        for(String option : results) {
            wrappers.add(new CPAO_PicklistWrapper(option.trim(), option.trim()));
        }
        return wrappers;
    }

    @AuraEnabled
    public static void updateFirmStructure() {
        // TODO: fill in related businesses
        
    }

    @AuraEnabled
    public static Account updateAccount(String accountJSON, String currentAPFJSON) {
        Account account = (Account)JSON.deserialize(accountJSON, Account.class);
        try {
            update account;
        } catch(DmlException ex) {
            System.debug(ex.getMessage() + '\n\n' + ex.getStackTraceString());
            throw new AuraHandledException(CANNOT_SAVE);
        }
        return getAccount(account.Id);
    }

    @AuraEnabled
    public static CPAO_Application__c updateAPF(String currentAPFJSON) {
        CPAO_Application__c currentAPF = (CPAO_Application__c) JSON.deserialize(currentAPFJSON, CPAO_Application__c.class);
        try {
            update currentAPF;
        } catch(DmlException ex) {
            System.debug(ex.getMessage() + '\n\n' + ex.getStackTraceString());
            throw new AuraHandledException(ex.getMessage() + '\n\n' + ex.getStackTraceString());
        }
        return getOpenAPFApplication();
    }

    @AuraEnabled
    public static void updateStep(Id recordId, String step) {
        CPAO_Application__c currentAPF = new CPAO_Application__c(
            Id = recordId, 
            CPAO_Current_Step__c = step
        );
        update currentAPF;
    }

    @AuraEnabled
    public static InitWrapper getCurrentAPF(String recordId) {
        InitWrapper response = new InitWrapper();

        response.currentAPF = getOpenAPFApplication();
        response.contactStatus = getContactStatus();
        response.account = getAccount(recordId);
        response.instructions = getInstructions();
        response.step = response.currentAPF.CPAO_Current_Step__c;
        response.mailingPicklist = getMailingPicklist();

        return response;
    }

    @AuraEnabled
    public static String getInstructions() {
        List<CPAO_RosterInstructionScreen__mdt> instructions = [SELECT CPAO_RosterInstruction__c FROM CPAO_RosterInstructionScreen__mdt WHERE DeveloperName =: APF_INSTRUCTIONS];
        return instructions[0].CPAO_RosterInstruction__c;
    }

    public static List<CPAO_PicklistWrapper> getMailingPicklist() {
        List<Schema.PicklistEntry> entries = Account.CPAO_Preferred_Mailing_Address__c.getDescribe().getPicklistValues();
        List<CPAO_PicklistWrapper> wrappers = new List<CPAO_PicklistWrapper>();
        for(Schema.PicklistEntry entry : entries) {
            wrappers.add(new CPAO_PicklistWrapper(entry));
        }
        return wrappers;
    }

    public static Account getAccount(String recordId) {
        List<Account> accounts = [
            SELECT 
                Name,
                OrderApi__Account_Email__c,
                Phone,
                Website,
                CPAO_Preferred_Mailing_Address__c,
                BillingStreet,
                BillingCity,
                BillingStateCode,
                BillingPostalCode,
                ShippingStreet,
                ShippingCity,
                ShippingStateCode,
                ShippingPostalCode,
                CPAO_LSO__c,
                CPAO_LSO__r.Name,
                CPAO_Designated_Administrator__c,
                CPAO_Practice_Inspection_Contact__r.Name,
                CPAO_Practice_Inspection_Contact__c,
                CPAO_Discipline_Contact__r.Name,
                CPAO_Discipline_Contact__c,
                CPAO_Discipline_Contact_Alternative__r.Name,
                CPAO_Discipline_Contact_Alternative__c
            FROM Account
            WHERE Id =: recordId
        ];
        return accounts.isEmpty() ? null : accounts[0];
    }

    public static String getContactStatus() {
        List<Contact> contacts = [SELECT CPAO_CPA_Status__c FROM Contact WHERE Id=:LOGGED_USER.ContactId LIMIT 1];
        return !contacts.isEmpty() ? contacts[0].CPAO_CPA_Status__c : null;
    }

    public static CPAO_Application__c getOpenAPFApplication() {
        // Get the Application
        List<CPAO_Application__c> applications = CPAO_ApplicationSelector.getUserApplicationsByRecordTypeNameAndStatus(OPEN, CPAO_APF);
        if(applications.isEmpty()) {
            // Create the APF Application if it doesn't exist already for the current user
            CPAO_Application__c apfApplication = new CPAO_Application__c();
            apfApplication.CPAO_Contact__c = LOGGED_USER.ContactId;
            apfApplication.RecordTypeId = APF_APPLICATION_RECORD_TYPE[0].Id;
            apfApplication.CPAO_Status__c = OPEN;
            apfApplication.CPAO_Current_Step__c = DEFAULT_STEP;

            // Attempt to insert our Application
            try {
                insert apfApplication;
                applications.add(apfApplication);

            } catch(DmlException ex) {
                if (ex.getMessage().containsIgnoreCase(CPAO_ApplicationServiceUtils.APPLICATION_ALREADY_IN_PROGRESS)) {
                    throw new AuraHandledException(CPAO_ApplicationServiceUtils.APPLICATION_ALREADY_IN_PROGRESS);
                } else if (ex.getMessage().containsIgnoreCase(CPAO_ApplicationServiceUtils.APPLICATION_DUPLICATE_PORTAL) || ex.getMessage().equalsIgnoreCase(CPAO_ApplicationServiceUtils.APPLICATION_DUPLICATE)) {
                    throw new AuraHandledException(CPAO_ApplicationServiceUtils.APPLICATION_DUPLICATE_PORTAL);
                } else {
                    throw new AuraHandledException('An error occured while creating new application, please contact CPA Ontario\'s Customer Service team.');
                }
            }
        }
        return applications[0];
    }

    public class InitWrapper {
        @AuraEnabled public CPAO_Application__c currentAPF {get; set;}
        @AuraEnabled public String contactStatus {get; set;}
        @AuraEnabled public Account account {get; set;}
        @AuraEnabled public String instructions {get; set;}
        @AuraEnabled public String step {get;set;}
        @AuraEnabled public List<CPAO_PicklistWrapper> mailingPicklist = new List<CPAO_PicklistWrapper>();
    }

    //public class FirmStructure {
    //    @AuraEnabled public List<CPAO_PicklistWrapper> operatingStatusOptions = new List<CPAO_PicklistWrapper>();
    //    @AuraEnabled public List<CPAO_PicklistWrapper> firmLocalityOptions = new List<CPAO_PicklistWrapper>();
    //    @AuraEnabled public List<CPAO_PicklistWrapper> firmServicesOptions = new List<CPAO_PicklistWrapper>();
    //}

    public class Question {
        @AuraEnabled public String fieldApi;
        @AuraEnabled public String label;
        @AuraEnabled public List<CPAO_PicklistWrapper> options;
        @AuraEnabled public Object value;

        public Question(String fieldApi, String label, List<CPAO_PicklistWrapper> options) {
            this.fieldApi = fieldApi;
            this.label = label;
            this.options = options;
        }
    }
}