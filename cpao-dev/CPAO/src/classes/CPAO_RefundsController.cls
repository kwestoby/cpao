public class CPAO_RefundsController {
    
    public static User getLoggedUser(){
		return CPAO_UserSelector.getCurrentUser();
	}

    @AuraEnabled
    public static List<OrderApi__Receipt__c> getReceipts(){
        user usr = getLoggedUser();
        System.debug(usr);
        List<OrderApi__Receipt__c> receiptList = [SELECT Id, Name, OrderApi__Receipt__c, OrderApi__Process_Refund__c, OrderApi__Date__c, OrderApi__Memo__c, OrderApi__Payment_Type__c, OrderApi__Total__c, OrderApi__Type__c, CPAO_First_Approver__c, CPAO_First_Approval__c, CPAO_Additional_Approval__c, CPAO_Additional_Approver__c, CPAO_Additional_Approval_Second__c, CPAO_Additional_Approver_Second__c, CPAO_Finance_Approval__c FROM OrderApi__Receipt__c WHERE OrderApi__Contact__c=: usr.ContactId];
        
        Map<Id, OrderApi__Receipt__c> paymentMap = new Map<Id, OrderApi__Receipt__c>();
        
        for(OrderApi__Receipt__c r:receiptList){
            if(r.OrderApi__Type__c.equals('Payment')){
                paymentMap.put(r.Id,r);
            }
        }      
        
        for(OrderApi__Receipt__c r:receiptList){
            if(r.OrderApi__Type__c.equals('Refund')){
                //System.debug(r.Id);
                //System.debug(r.OrderApi__Receipt__c);
                if(r.OrderApi__Receipt__c != null){
                    if(paymentMap.containsKey(r.OrderApi__Receipt__c)){
                        OrderApi__Receipt__c temp = paymentMap.get(r.OrderApi__Receipt__c);
                        temp.OrderApi__Receipt__c = r.Id;
                        temp.OrderApi__Is_Refund__c = true;
                        temp.OrderApi__Process_Refund__c = r.OrderApi__Process_Refund__c;
                        temp.CPAO_First_Approver__c = r.CPAO_First_Approver__c;
                        temp.CPAO_First_Approval__c = r.CPAO_First_Approval__c;
                        
                        temp.CPAO_Additional_Approver__c = r.CPAO_Additional_Approver__c;
                        temp.CPAO_Additional_Approval__c = r.CPAO_Additional_Approval__c;
                        
                        temp.CPAO_Additional_Approver_Second__c = r.CPAO_Additional_Approver_Second__c;
                        temp.CPAO_Additional_Approval_Second__c = r.CPAO_Additional_Approval_Second__c;
                        
                        
                        temp.CPAO_Finance_Approval__c = r.CPAO_Finance_Approval__c;
                        
                        
                        
                        
                        
                    }  
                }
            }
        }

        //return paymentMap.values();
        //
        
        List<OrderApi__Receipt__c> paymentReceipts = paymentMap.values();
        
        processUpdate(paymentReceipts);
        
        return paymentReceipts;
        
    }
    
    public static void processUpdate(List<OrderApi__Receipt__c> paymentReceipts){
        
        for(OrderApi__Receipt__c r: paymentReceipts){
            
            if(r.OrderApi__Receipt__c != null){
                
                if(r.CPAO_First_Approval__c == 'No' || r.CPAO_Additional_Approval__c == 'No' || r.CPAO_Additional_Approval_Second__c == 'No' || r.CPAO_Finance_Approval__c == 'No'){
                    r.CPAO_Approval_Notes__c = 'Rejected';
                }else if (r.CPAO_First_Approval__c == 'Yes' || r.CPAO_Additional_Approval__c == 'Yes' || r.CPAO_Additional_Approval_Second__c == 'Yes' || r.CPAO_Finance_Approval__c == 'Yes'){
                    r.CPAO_Approval_Notes__c = 'Approved';
                }
                
               
            }
        }
        
        
    }
    
    @AuraEnabled
    public static void createRefund(String id, String reason){
        
        List<OrderApi__Receipt__c> contactReceipt = [SELECT Id, OrderApi__Contact__c, OrderApi__Refund__c FROM OrderApi__Receipt__c WHERE Id=:id limit 1];
        contactReceipt[0].CPAO_Refund_Reason__c = reason;
        contactReceipt[0].OrderApi__Refund__c = True;
        contactReceipt[0].CPAO_Date_Time_of_Refund_Request__c = datetime.now();
        
        sendEmail(contactReceipt[0]);
        
        update contactReceipt[0];
        
    }
    
    public static void sendEmail(OrderApi__Receipt__c rec){
        List<OrgWideEmailAddress> owa = [select id, DisplayName, Address from OrgWideEmailAddress limit 1];
        List<EmailTemplate> template = [select id from EmailTemplate where developername=:'Refund_Requested_Email_Template' limit 1];
        
        List<Messaging.SingleEmailMessage> allMsg = new List<Messaging.SingleEmailMessage>();
        
        Messaging.SingleEmailMessage email = new Messaging.SingleEmailMessage();
        email.setTemplateId(template[0].id);
        email.setWhatId(rec.Id);
        email.setTargetObjectId(rec.OrderApi__Contact__c);
        email.setSaveAsActivity(false);
        email.setOrgWideEmailAddressId(owa[0].id);
        
        allMsg.add(email);
        
        Messaging.sendEmail(allMsg, false);
    }
    
    
    
}