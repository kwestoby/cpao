public with sharing class CPAO_CommunicationPreferenceSelector {
    
    /*Those methods are deprecated
    //Based on enhancement user story 1583, Communication Preference Object should be deleted and functionalities
    //should be moved to Contact object.
    //For future tracking purposes, this class has not been deleted
    //Halima Dosso - 02/28/2018
    public static List<CPAO_Communications_Preferences__c> getCommunicationPreferences() {
        return [SELECT Id,CPAO_Consent_type__c,Associated_Contact__c FROM CPAO_Communications_Preferences__c];
    }

    public static List<CPAO_Communications_Preferences__c> getCommunicationPreferencesByContactId(Set<Id> idsContact) {
        return [SELECT Id,CPAO_Consent_type__c,Associated_Contact__c FROM CPAO_Communications_Preferences__c WHERE
        Associated_Contact__c =: idsContact];
    }*/

    public static List<Contact> getCommunicationPreferences() {
        return [SELECT Id,CPAO_Consent_type__c,CPAO_Implied_Consent_Expiry_Date__c FROM Contact];
    }

    public static List<Contact> getCommunicationPreferencesByContactId(Set<Id> idsContact) {
        return [SELECT Id,CPAO_Consent_type__c FROM Contact WHERE id =: idsContact];
    }
}