@isTest
public class CPAO_LateDeferralLockBatchTest {
    
    public static List<Account> factoryAccounts;
    public static List<Contact> factoryContacts;
    public static List<CPAO_Infraction__c> infractions;
    public static CPAO_Infraction_Type__c infractionType;
    public static final String AMD_INFRACTION_TYPE = 'Annual Membership Dues (AMD)';
    @testSetup
    static void setup(){
        CPAO_TestFactory.automationControlSetup();
        List<CPAO_Infraction_Type__c> infractionTypes = CPAO_TestFactory.createInfractionTypes(1, AMD_INFRACTION_TYPE);
        insert infractionTypes;
        factoryAccounts = CPAO_TestFactory.createAccounts(2);
        insert factoryAccounts;
        factoryContacts= CPAO_TestFactory.createContacts(factoryAccounts[0],2);
        insert factoryContacts;        
        List<CPAO_Application__c> listApplication =  CPAO_TestFactory.createAMDCPDpplication(10, factoryContacts[0]);       
        insert listApplication;
    }
    
    @isTest        
    static void test() {    
        Test.startTest();
        CPAO_LateDeferralLockBatch lateDeferralLockBatch = new CPAO_LateDeferralLockBatch();
        Id batchId = Database.executeBatch(lateDeferralLockBatch);
        Test.stopTest();
        List<CPAO_Application__c> listApplication = [SELECT Id,CPAO_Status__c, CPAO_Contact__c FROM CPAO_Application__c];
        for(CPAO_Application__c app: listApplication)
        {
            System.assert(app.CPAO_Status__c == 'Completed');
            
        }
        
        
    }
    
    
    
    
}