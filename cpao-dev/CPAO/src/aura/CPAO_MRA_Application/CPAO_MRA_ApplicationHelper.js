({
    notifyClickOnNextButton : function(componentToNotify,palApp,nextStep) {
        var nextButtonClick = $A.get("e.c:CPAO_Next_Button_Has_Been_Pressed");
        nextButtonClick.setParams({
            "componentName" : componentToNotify,
            "PALApplication": palApp,
            "nextStepName":nextStep
        } );
        nextButtonClick.fire();
        console.log('fired button click event');
    },
    verifyIfSkippingCPDPage: function(component,buttonPressed){
        var basisOfApplication=component.get('v.conditionToNotRedirectToCDP');
        if(component.get('v.currentPAL').CPAO_Basis_Of_Application__c!=basisOfApplication){
            //if basis of application is not "hold an active PAL with another body",show cpd page
            if(buttonPressed=='Next'){
                component.set("v.selectedStep", "step5");
            }else if(buttonPressed=='Previous'){
                component.set("v.selectedStep", "step7");
                
            }
            
        }else{
            //else skip cpd page and skip practise inspection page 
            //and professional page and  show schedule B page
            if(buttonPressed=='Next'){
                component.set("v.selectedStep", "step8");
            }else if(buttonPressed=='Previous'){
                component.set("v.selectedStep", "step4");
                
            }
        }
        
    },
    upsertPAL: function(component){
        
        var action = component.get("c.upsertPalApplication");
        action.setParams({ "PALappToAdd" :component.get('v.currentPAL') });
        action.setCallback(this, function(response){
            var state = response.getState();
            if (state === "SUCCESS") {
                alert('Thank you for submitting your PAL app. \n This is a Placeholder for payment.\n Application now has status "submitted"');
                //window.open('CPAO_ApplicationDashboard');
                window.location.href='/';
                
            }else{
                console.log('error while submitting Pal');
                
            }
        });
        
        $A.enqueueAction(action);
    },
    
    checkProfessionalExperienceCheckboxes: function(component){
        var checkValuesTable=[];
        var totalHoursDeficiency = component.get('v.currentPAL.CPAO_Total_Hours_Deficiency__c');
        var PA_ServiceDeficiency = component.get('v.currentPAL.CPAO_PA_Services_Deficiency__c');
        if(totalHoursDeficiency=='Yes'){
            checkValuesTable.push(
                'Completing a total of 2,500 eligible hours (combination of public accounting services and designated services)');
        }
        if(PA_ServiceDeficiency=='Yes'){
            checkValuesTable.push(
                'Completing a minimum of 1,250 eligible hours in public accounting services');
        }
        component.set('v.deficiencyValues',checkValuesTable);
        
    },
    formatJavaScriptDates: function(component){
        
        
        
        if(component.get('v.currentPAL.CPAO_Effective_Date__c')!=null){
            var effectiveDateFormatted  = component.get('v.currentPAL.CPAO_Effective_Date__c').substring(0,10);
            component.set('v.currentPAL.CPAO_Effective_Date__c',effectiveDateFormatted);
        }
        
        if(component.get('v.currentPAL.CPAO_Practice_Inspection_Date_1__c')!=null){
            var inspectiondate1Formatted= component.get('v.currentPAL.CPAO_Practice_Inspection_Date_1__c').substring(0,10);
            component.set('v.currentPAL.CPAO_Practice_Inspection_Date_1__c',inspectiondate1Formatted);
        }
        
        
        if(component.get('v.currentPAL.CPAO_Practice_Inspection_Date_2__c')!=null){
            var inspectiondate2Formatted= component.get('v.currentPAL.CPAO_Practice_Inspection_Date_2__c').substring(0,10);
            component.set('v.currentPAL.CPAO_Practice_Inspection_Date_2__c',inspectiondate2Formatted);
        }
        
        if(component.get('v.currentPAL.CPAO_Practice_Inspection_Date_3__c')!=null){
            var inspectiondate3Formatted= component.get('v.currentPAL.CPAO_Practice_Inspection_Date_3__c').substring(0,10);
            component.set('v.currentPAL.CPAO_Practice_Inspection_Date_3__c',inspectiondate3Formatted);
        }
        if(component.get('v.currentPAL.CPAO_Exceptional_Circumstances_SD__c')!=null){
            var exceptionalCircumstancesSD= component.get('v.currentPAL.CPAO_Exceptional_Circumstances_SD__c').substring(0,10);
            component.set('v.currentPAL.CPAO_Exceptional_Circumstances_SD__c',exceptionalCircumstancesSD);
        }
        
        if(component.get('v.currentPAL.CPAO_Exceptional_Circumstances_End_Date__c')!=null){
            var exceptionalCircumstancesED= component.get('v.currentPAL.CPAO_Exceptional_Circumstances_End_Date__c').substring(0,10);
            component.set('v.currentPAL.CPAO_Exceptional_Circumstances_End_Date__c',exceptionalCircumstancesED);
        }
        
        
    },
    showSpinner: function (component, event, helper) {
        var spinner = component.find("mySpinner");
        $A.util.removeClass(spinner, "slds-hide");
    },
    
    hideSpinner: function (component, event, helper) {
        var spinner = component.find("mySpinner");
        $A.util.addClass(spinner, "slds-hide");
    }
})