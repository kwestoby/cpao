({
	doInit : function(component, event, helper) {
		var today = new Date();
		var dd = today.getDate();
		var mm = today.getMonth()+1; 
		var yyyy = today.getFullYear();

		if(dd<10) {
		    dd = '0'+dd
		}
		if(mm<10) {
		    mm = '0'+mm
		}
		today = yyyy + '-' + mm + '-' + dd;

		var rel = component.get("v.accountContactRel");
		rel.CPAO_Start_Date__c = today;
		component.set("v.accountContactRel", rel);
        var action = component.get("c.NewEmploymentDoInit");
   	    action.setCallback(this, function(response){
	      	var state = response.getState();
	      	if (state === "SUCCESS") {
	        	component.set("v.businessUnitOptions", response.getReturnValue().businessUnits);
	        	component.set("v.positionCategoryOptions", response.getReturnValue().positionCategories);
	        	component.set("v.loggedUser", response.getReturnValue().loggedUser);
	        	component.set("v.newEmployerWrapper", response.getReturnValue().newEmploymentWrapper);
	        	console.log('marco test !!!!!!!!!!!!!!!');
	        	console.log(response.getReturnValue());
	        	component.set("v.country", response.getReturnValue().country);
	        	component.set("v.state", response.getReturnValue().state);
	      	}
        });

	    $A.enqueueAction(action);
	},
	addEmployer: function(component, event, helper) {
		var validForm = component.find('newEmployerform').reduce(function (validSoFar, inputCmp) {
            // Displays error messages for invalid fields
            inputCmp.showHelpMessageIfInvalid();
            return validSoFar && inputCmp.get('v.validity').valid;
        }, true);
        // If we pass error checking, do some real work
        if(validForm){
        	var rel = component.get("v.accountContactRel");
        	if(!rel.CPAO_Primary_Employer__c){
        		component.set('v.severity','error');
	           	component.set("v.confirmationMessage"," Please choose a value for Primary Employer"); 
        	}
        	if(!component.get('v.selectedAccount.val')){
        		component.set('v.severity','error');
	           	component.set("v.confirmationMessage"," Please choose an employer from the list");
        	}
			//var primaryEmployerIsChecked = component.find('primaryEmployer').get('v.checked');
			component.set("v.accountContactRel.CPAO_Contact__c",component.get("v.loggedUser")[0].ContactId);
			var action= component.get("c.isPrimaryEmployer");
		    action.setParams({
	            "contactId": component.get('v.accountContactRel.CPAO_Contact__c')
	        });
		    action.setCallback(this, function(response){
	            var state = response.getState();
	            if (state === "SUCCESS") {
	            	var rel = component.get("v.accountContactRel");
	      			if(response.getReturnValue().length>0 && rel.CPAO_Primary_Employer__c == 'Yes'){ //&& primaryEmployerIsChecked
	      				console.log(response.getReturnValue()[0]);
	    			    var action3= component.get("c.changePrimaryEmployer");
						action3.setParams({
	      				  "relationshipId": response.getReturnValue()[0].Id,
	      				  "doInsert" : true
	   				    });
						action3.setCallback(this, function(response){
	     				   var state = response.getState();
	                       if (state === "SUCCESS") {
	     				      helper.creationOfNewEmployer(component,event);

	      					}
	  					});
	 				   $A.enqueueAction(action3);	      	
	                }else if(response.getReturnValue().length==0 && rel.CPAO_Primary_Employer__c != 'Yes'){ //&& (!primaryEmployerIsChecked)
	      				component.set('v.severity','error');
	      	            component.set("v.confirmationMessage"," Error: You don't have any primary employer. Please make this one your primary");
	      	        }else{
	          			 helper.creationOfNewEmployer(component,event);
					}
	            }else{
	      		  console.log('errOR');
	      	    }
            });
		    $A.enqueueAction(action);
	    }
    },
	handleAddedEmployer:function(component, event, helper) {
	  	// var addedEmployerEvent = component.getEvent("newEmployerAdded");
      	// var listOfRelationships=addedEmployerEvent.getParam("listOfRelationships");
    	console.log('fired ');

    },

    closeModel: function(component, event, helper) {
      	// for Hide/Close Model,set the "endDateModalOpen" attribute to "Fasle"  
    	component.set("v.endDateModalOpen", false);
    },

    handleClick: function(component, event, helper) {
      	// for Hide/Close Model,set the "endDateModalOpen" attribute to "Fasle"  
    	component.set("v.endDateModalOpen", true);
    },

    newEmployer: function(component, event, helper) {
      	
      	var newEmploymentWrapper = component.get("v.newEmployerWrapper");
      	console.log('marco 1');
      	var validForm = component.find('wrapperForm').reduce(function (validSoFar, inputCmp) {
            // Displays error messages for invalid fields
            inputCmp.showHelpMessageIfInvalid();
            return validSoFar && inputCmp.get('v.validity').valid;
        }, true);
      	console.log('marco 2');
        if(!newEmploymentWrapper.isPrimaryEmployer || !newEmploymentWrapper.isAccountingFirm){
        	validForm = false;
        }
        console.log('marco 3');
        if(validForm){
        	var JSONnewEmploymentWrapper = JSON.stringify(newEmploymentWrapper);
        	var action = component.get("c.NewEmployerCreate");
		    action.setParams({
	            "JSONuserInputData": JSONnewEmploymentWrapper
	        });
	        console.log('marco 4');
		    action.setCallback(this, function(response){
		    	var state = response.getState();
		    	console.log('state');
		    	console.log(state);
		    	if(state == "SUCCESS"){
		    		console.log(response.getReturnValue());
		    		if(response.getReturnValue() == 'Success'){
		    			console.log('marco 5');
				    	var addedEmployerEvent = component.getEvent("newEmployerAdded");
				    	addedEmployerEvent.fire();
		    		}			    		
		    	}		    	
		    });
		    $A.enqueueAction(action);
        } else {
        	console.log('marco 6');
        	component.set('v.severityModal','error');
	        component.set("v.confirmationMessageModal"," Please fill in all fields");
        }
    },
    
})