/* File: gulpfile.js */
'use strict'
// grab our gulp packages
var gulp 		= require('gulp'),
    changed 	= require('gulp-changed'),
    runSequence = require('run-sequence'),
    newer 		= require('gulp-newer'),
    browserSync = require('browser-sync').create(),
    sass        = require('gulp-sass'),
    sourcemaps  = require('gulp-sourcemaps'),
    cssmin      = require('gulp-cssmin'),
    autoprefixer = require('gulp-autoprefixer'),
    del         = require('del'),
    pug         = require('gulp-pug');

var autoprefixerOptions = {
    browsers: ['last 5 Chrome versions', 'last 5 Firefox versions', 'ie >= 8']
};

gulp.task('browserSync', function () {
    browserSync.init(['./build/**.*'], {
        port: 3000,
        server: {
            baseDir: './',
            directory: true,
            browser: 'google chrome',
            logLevel: 'debug'
        },
        files: ['build/**/*.{html,js,css}']
        // reloadDebounce: 15000
    });
});
// run this task by typing in gulp pug in CLI
gulp.task('pug', function () {
    return gulp.src('src/pug/pages/*.pug')
      .pipe(pug()) // pipe to pug plugin
      .pipe(gulp.dest('build')); // tell gulp our output folder
});
gulp.task('html', function (doneTemplates) {
    return gulp.src('./src/pug/pages/*.pug')
        .pipe(newer('build'))
        .pipe(pug({
            locals: './src/pug/',
            pretty: true
        }))
        .pipe(gulp.dest('build'));
});
gulp.task('css', function () {
    return gulp.src('./src/scss/*.scss')
        // Write the maps only for development
        // .pipe(changed('./build/css', { extension: '.css' }))
        // .pipe(sourcemaps.init())
        .pipe(sass({
            outputStyle: 'expanded',
            omitSourceMapUrl: true,
            precision: 2,
            indentType: 'tab',
            indentWidth: 1
        })
        .on('error', sass.logError))
        // Write the maps only for development
        // .pipe(autoprefixer(autoprefixerOptions))
        // .pipe(sourcemaps.write('.'))
        .pipe(gulp.dest('./build/css'))
        .pipe(browserSync.reload({ stream: true }));
});
gulp.task('vendor', function () {
    return gulp.src('./src/vendor/**/*')
        .pipe(changed('./build/vendor'))
        .pipe(gulp.dest('./build/vendor'));
});
gulp.task("fonts", function() {
  return gulp
    .src("./src/fonts/**/*")
    .pipe(changed("./build/fonts"))
    .pipe(gulp.dest("./build/fonts"));
});
gulp.task("images", function() {
    return gulp
      .src("./src/images/**/*")
      .pipe(changed("./build/images"))
      .pipe(gulp.dest("./build/images"));
  });
gulp.task("scripts", function() {
  return gulp
    .src("./src/js/**/*")
    .pipe(changed("./build/js"))
    .pipe(gulp.dest("./build/js"));
});
// Clean dev folder and prod folder
gulp.task('clean', function () {
    return del(['./build/']);
});
// Build new dev folder and prod folder
gulp.task('build', function (cb) {
    return runSequence(
        'clean',
        ['vendor', 'css', 'html', 'fonts', 'images', 'scripts'],
        cb
    );
});
gulp.task('watch', function () {
    gulp.watch('./src/pug/**/*.pug', ['html', browserSync.reload]);
    gulp.watch('./src/scss/**/*.scss', ['css', browserSync.reload]);
    gulp.watch("./app/js/**/*.js", ["scripts", browserSync.reload]);
    gulp.watch('./app/fonts/**/*', ['fonts']);
    gulp.watch('./app/images/**/*', ['images']);

});

gulp.task('default', ['browserSync', 'watch']);