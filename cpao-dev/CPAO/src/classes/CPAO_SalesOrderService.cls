public without sharing class CPAO_SalesOrderService {
	
	public static void markApplicationsSubmitted(List<OrderApi__Sales_Order__c> newSalesOrders) {
		List<CPAO_Application__c> appsToUpdate = new List<CPAO_Application__c>();
		Set<Id> currentIds = new Set<Id>();
		for(OrderApi__Sales_Order__c so:newSalesOrders){			
			if(!currentIds.contains(so.CPAO_Application__c)){
				appsToUpdate.add(new CPAO_Application__c(Id = so.CPAO_Application__c, CPAO_Status__c = 'Submitted', CPAO_Submission_Date__c=System.today(), CPAO_Contact__c = so.OrderApi__Contact__c));
				currentIds.add(so.CPAO_Application__c);
			}
		}
		update appsToUpdate;

		List<EmailTemplate> lstEmailTemplates = getTemplate('CPAO_Successful_Application_Submission');
        if(lstEmailTemplates.size() != 1){
            return;
        }
        Messaging.SingleEmailMessage[] messages = new List<Messaging.SingleEmailMessage>();

        for(CPAO_Application__c app:appsToUpdate){
            Messaging.SingleEmailMessage mail = new Messaging.SingleEmailMessage();
            mail.setTemplateId(lstEmailTemplates[0].Id);
            mail.setSaveAsActivity(false);
            mail.setTargetObjectId(app.CPAO_Contact__c);

            mail.setWhatId(app.id);
            messages.add(mail);
        }           
        
        Messaging.SendEmailResult[] resultMail = Messaging.sendEmail(messages);
	}

	public static List<EmailTemplate> getTemplate(String templateName) {
        return [SELECT Id, Body, Subject from EmailTemplate where DeveloperName = :templateName];
    }
}