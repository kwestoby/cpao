({
	doAction : function(component, event, helper) {
		var apexAction = component.get('c.loadReviewInfo');
        apexAction.setCallback(this, function(response) {
        	var state = response.getState();
            console.log('state');
            console.log(state);
        	if(state === "SUCCESS"){
                var result = response.getReturnValue();
                component.set("v.userContact", result.userContact);
                component.set("v.contactEmploymentRecords", result.contactEmploymentRecords);
                component.set("v.contactEmploymentRecordsSize", result.contactEmploymentRecords.length);
                component.set("v.application", result.application);
                
            }
        });
        $A.enqueueAction(apexAction);
	},
    clickOnNextButton : function(component, event, helper){
        var talkingToComponent = event.getParam("componentName");
        if(talkingToComponent == 'step5'){
            var app = component.get("v.application");
            console.log('app.CPAO_Attestation__c');
            console.log(app.CPAO_Attestation__c);
            if(app.CPAO_Attestation__c != true){
                helper.methodFailure(component, event, helper, 'You must comply with the attestation', 'Error');
                return;
            }
            var apexAction = component.get('c.saveAttestation');
            apexAction.setParams({
                "currentReinstateApp": app
            });
            apexAction.setCallback(this, function(response) {
                var state = response.getState();
                console.log('state');
                console.log(state);
                if(state === "SUCCESS"){
                    var result = response.getReturnValue();
                    console.log('result');
                    console.log(result);
                    if(result.status == 'Success'){
                        if(result.status == 'Success'){
							console.log("Application Submitted");
                            helper.methodFailure(component, event, helper);    
                            //window.location.href = result.checkOutScreenURL;
                        } else {
                            helper.methodFailure(component, event, helper, result.msg, result.status);    
                        }
                        // var actionToTake = event.getParam("nextStepName");
                        // var applicationHasChanged = component.getEvent("applicationHasChanged");
                        // applicationHasChanged.setParams({
                        //     "nextButtonClickeable" : true,
                        //     "action" : actionToTake});
                        // applicationHasChanged.fire();
                    } else {
                        helper.methodFailure(component, event, helper);
                    }                
                } else {
                    helper.methodFailure(component, event, helper);
                }
            });
            $A.enqueueAction(apexAction);
        }
    }
})