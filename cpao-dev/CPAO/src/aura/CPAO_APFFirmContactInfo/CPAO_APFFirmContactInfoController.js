({
	doInit : function(component, event, helper) {
        var localOfficer         = {text:component.get("v.account.CPAO_LSO__r.Name")};
        var inspectionClient     = {text:component.get("v.account.CPAO_Practice_Inspection_Contact__r.Name")};
        var disciplineContact    = {text:component.get("v.account.CPAO_Discipline_Contact__r.Name")};
        var disciplineContactAlt = {text:component.get("v.account.CPAO_Discipline_Contact_Alternative__r.Name")};

        if(localOfficer.text)         { component.set("v.localOfficer", localOfficer); }
        if(inspectionClient.text)     { component.set("v.inspectionClient", inspectionClient); }
        if(disciplineContact.text)    { component.set("v.disciplineContact", disciplineContact); }
        if(disciplineContactAlt.text) { component.set("v.disciplineContactAlt", disciplineContactAlt); }
    },

    handlePrev : function(component, event, helper) {
        helper.firePrevEvent(component);
    },

    handleNext : function(component, event, helper) {
        var isEdit = component.get("v.isEdit");
        if(isEdit) {
            helper.updateAPF(component, helper, false); 
        }
        else {
            helper.handleNext(component, helper, false);
        }
    },

    handleSaveAndQuit : function(component, event, helper) {
        var isEdit = component.get("v.isEdit");
        if(isEdit) {
            helper.updateAPF(component, helper, true); 
        }
        else {
            helper.handleNext(component, helper, true);
        }
    },

    onEdit : function(component, event, helper) {
        let isEdit = component.get("v.isEdit");
        component.set("v.isEdit", !isEdit);
    },

    handleLocalOfficer : function(component, event, helper) {
        helper.handleLookupChange(component, helper, 'localOfficer');
    },

    handleInspectionClient : function(component, event, helper) {
        helper.handleLookupChange(component, helper, 'inspectionClient');
    },

    handleDisciplineContact : function(component, event, helper) {
        helper.handleLookupChange(component, helper, 'disciplineContact');
    },

    handleDisciplineContactAlt : function(component, event, helper) {
        helper.handleLookupChange(component, helper, 'disciplineContactAlt');
    },

})