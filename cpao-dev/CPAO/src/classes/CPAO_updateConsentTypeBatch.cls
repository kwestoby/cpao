global class CPAO_updateConsentTypeBatch implements Database.Batchable<sObject> {
// This batch job class looks for CPAO_Communications_Preferences__c records
// if the consent type of the record is "Implied" and the Implied Consent Expiry Date 
// is less or equals to today, the  batch job updates the consent type field to unknown 	
	
	
	global Database.QueryLocator start(Database.BatchableContext BC) {

		String query='SELECT Id,CPAO_Consent_type__c,CPAO_Implied_Consent_Expiry_Date__c FROM Contact WHERE CPAO_Implied_Consent_Expiry_Date__c <=TODAY AND CPAO_Consent_type__c=\'Implied Consent\'';
		return Database.getQueryLocator(query);
	}

   	global void execute(Database.BatchableContext BC, List<Contact> contacts) {
		for(Contact contact:contacts){
   			contact.CPAO_Consent_type__c = 'Unknown';
   		}
   		update contacts;
	}
	
	global void finish(Database.BatchableContext BC) {
		
	}
	
}