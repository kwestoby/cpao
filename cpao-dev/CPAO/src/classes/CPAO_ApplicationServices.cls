public without sharing class CPAO_ApplicationServices {

    public static final String MEMBER = 'Member';
    public static final String ACTIVE = 'Active';
    public static final String CLOSED = 'Closed';
    public static final String COMPLETED = 'Completed';
    //public static final String NULL = 'Reinstated following suspension';
    public static final String CANADA = 'Canada';
    public static final String BERMUDA = 'Bermuda';
    public static final String ONTARIO = 'Ontario';
    public static final String AMD = 'AMD';
    public static final String CPD = 'CPD';
    public static final String PUBLIC_ACCOUNTING_TYPE = 'Public Accounting Licence';
    public static final String CERTIFICATE_OF_AUTHORIZATION = 'Certificate Of Authorization';
    public static final String ORIGINAL = 'Original';
    public static final String READMIT_APPROVAL_LETTER = 'CPAO_Readmission_Approval_Letter';
    public static final String APP_RECOMMEND_APPROVE = 'Approve';
    public static final String APP_RECOMMEND_DENY = 'Deny';
    public static final String REGISTRAR_QUEUE = 'Registrar';
    
    public static  String emailTemplate_PAL='CPAO_Reinstatement_Letter_WITH_PAL';
    public static  String emailTemplate_NO_PAL='CPAO_Reinstatement_Letter_NO_PAL'; 

    //marks applications with the current users contact Id
    public static void stampUserContactOnApplication(List<CPAO_Application__c> applications) {
        Id currentUserContactId = CPAO_UserSelector.getCurrentUser().ContactId;
        if (currentUserContactId == null) {
            return;
        }
        for (CPAO_Application__c application : applications) {
            application.CPAO_Contact__c = currentUserContactId;
        }
    }

    //when an application has an RO staff recomendation of Reinstate, this checks to see if there are any open infractions
    //if there are open infractions, it adds an error, if not, its send the application to the Registrar queue
    //can this be applied to readmission as well?
    public static void roReinstateApplications(List<CPAO_Application__c> applications) {
        List<CPAO_Application__c> noInfractionApplications = CPAO_ApplicationServiceUtils.findOpenInfractions(applications);

        if (!noInfractionApplications.isEmpty()) {
            CPAO_ApplicationServiceUtils.sendToRegistrarQueue(noInfractionApplications);
        }
    }
    
    

    public static void inReviewApplications(List<CPAO_Application__c> applications) {
        if (!applications.isEmpty()) {
            for (CPAO_Application__c application : applications) {
                application.CPAO_Reviewer_Name__c = UserInfo.getName();
                application.CPAO_UserIdOfReviewer__c = UserInfo.getUserId();
                application.CPAO_Recommendation_Date_Hidden__c = System.today();
            }
        }
    }

    //when a Registrar decision is reinstate, then this method checks if there are any open infractions
    //if there are open infractions, it adds an error, if not, it marks the application status as Completed, The contacts CPA
    // status as Active, and sends an email to the applicant
    public static void registrarReinstateApplications(List<CPAO_Application__c> applications) {
        List<CPAO_Application__c> noInfractionApplications = CPAO_ApplicationServiceUtils.findOpenInfractions(applications);
        List<Contact> contactsToUpdate = new List<Contact> ();
        if (!noInfractionApplications.isEmpty()) {
            for (CPAO_Application__c application : noInfractionApplications) {
                application.CPAO_Status__c = COMPLETED;
                contactsToUpdate.add(new Contact(Id = application.CPAO_Contact__c,
                                                 CPAO_CPA_Status__c = ACTIVE, CPAO_CPA_Sub_Status__c = null));
            }
            update contactsToUpdate;
            //sendRegistrarReinstatementEmail(noInfractionApplications);
        }
    }
    
    //when a Registrar decision is approve for readmission, then this method checks if there are any open infractions
    public static void registrarReadmitApplications(List<CPAO_Application__c> applications) {
        List<CPAO_Application__c> noInfractionApplications = CPAO_ApplicationServiceUtils.findOpenInfractions(applications);
        List<Contact> contactsToUpdate = new List<Contact> ();
        if (!noInfractionApplications.isEmpty()) {
            for (CPAO_Application__c application : noInfractionApplications) {
                //application.CPAO_Status__c = COMPLETED;
                contactsToUpdate.add(new Contact(Id = application.CPAO_Contact__c,
                                                 CPAO_CPA_Status__c = ACTIVE, CPAO_CPA_Sub_Status__c = null));
            }
            update contactsToUpdate;
            sendEmails(noInfractionApplications,READMIT_APPROVAL_LETTER);
        }
    }

    //marks an application status as completed when a registrar denies an application
    public static void registrarDenyApplications(List<CPAO_Application__c> applications) {
        for (CPAO_Application__c application : applications) {
            application.CPAO_Status__c = COMPLETED;
        }
    }
    
    
    
    //Do not let the recommendation be set to Approve unless all the Breaches are closed
    public static void checkBreachesBeforeApplApprovoed(List<CPAO_Application__c> applications)
    {
        List <CPAO_Application__c> lstApprovedApps = new List <CPAO_Application__c>();
        Map<Id, CPAO_Application__c> applicationContactIds = new Map<Id, CPAO_Application__c> ();
        Group registrarQueue = [SELECT Id FROM Group WHERE Name =:REGISTRAR_QUEUE and Type='Queue'];
        
        for(CPAO_Application__c app : applications)
        {
            if(app.CPAO_Recommendation__c == APP_RECOMMEND_APPROVE)
                lstApprovedApps.add(app);
            
            if(app.CPAO_Recommendation__c == APP_RECOMMEND_DENY)
                app.OwnerID = registrarQueue.ID;
        }
        
        if(lstApprovedApps.size()>0)
        {
            for (CPAO_Application__c application : lstApprovedApps) {
                applicationContactIds.put(application.CPAO_Contact__c, application);
            }
            Map<Id, List<CPAO_Infraction__c>> contactIdToInfractionMap = new Map<Id, List<CPAO_Infraction__c>> ();
            
            for (CPAO_Infraction__c infraction : CPAO_InfractionSelector.getContactOpenInfractions(applicationContactIds.keySet())) {
                if (!contactIdToInfractionMap.containsKey(infraction.CPAO_Contact__c)) {
                    contactIdToInfractionMap.put(infraction.CPAO_Contact__c, new List<CPAO_Infraction__c> { infraction });
                } else {
                    List<CPAO_Infraction__c> tempList = contactIdToInfractionMap.get(infraction.CPAO_Contact__c);
                    tempList.add(infraction);
                    contactIdToInfractionMap.put(infraction.CPAO_Contact__c, tempList);
                }
            }
            
            for(CPAO_Application__c appl : lstApprovedApps)
            {
                if (contactIdToInfractionMap.get(appl.CPAO_Contact__c) != null)
                    appl.addError(System.Label.CPAO_OPEN_BREACHES);
                else
                    appl.OwnerID = registrarQueue.ID;
            }
        }
    }
        
        
        
        
    //this accepts reinstatement applications that have been submitted
    //this first checks if an applcation can be automatically approved, the criteria being that the open infractions are of type 
    //AMD, APF, and PIF. there must be a good character declaration that has all answers as no.
    //if its set for automatic aproval, the infracions are closed, the contact is reinstated, and the applciation record is set to
    // completed. for manual approval, the record is sent to the RO staff queue
    public static void reinstatementPaidApplications(List<CPAO_Application__c> applications) {
        Map<Id, CPAO_Application__c> applicationContactIds = new Map<Id, CPAO_Application__c> ();
        for (CPAO_Application__c application : applications) {
            applicationContactIds.put(application.CPAO_Contact__c, application);
        }
        Map<Id, List<CPAO_Infraction__c>> contactIdToInfractionMap = new Map<Id, List<CPAO_Infraction__c>> ();
        for (CPAO_Infraction__c infraction : CPAO_InfractionSelector.getContactOpenInfractions(applicationContactIds.keySet())) {
            if (!contactIdToInfractionMap.containsKey(infraction.CPAO_Contact__c)) {
                contactIdToInfractionMap.put(infraction.CPAO_Contact__c, new List<CPAO_Infraction__c> { infraction });
            } else {
                List<CPAO_Infraction__c> tempList = contactIdToInfractionMap.get(infraction.CPAO_Contact__c);
                tempList.add(infraction);
                contactIdToInfractionMap.put(infraction.CPAO_Contact__c, tempList);
            }
        }

        //removing references to good character references
        //Map<Id, CPAO_Declaration__c> goodDeclarations = new Map<Id, CPAO_Declaration__c> ();
        //for (CPAO_Declaration__c declaration : CPAO_DeclarationSelector.getCleanReinstatementDecs(applicationContactIds.values())) {
        //    goodDeclarations.put(declaration.CPAO_Application__c, declaration);
        //}

        List<CPAO_Application__c> automaticApplications = new List<CPAO_Application__c> ();
        List<CPAO_Application__c> manualApplications = new List<CPAO_Application__c> ();

        for (Id contactId : applicationContactIds.keySet()) {
            Boolean isAutomatic = true;
            //if (!goodDeclarations.containsKey(applicationContactIds.get(contactId).Id)) {
            //    isAutomatic = false;
            //}
            if (contactIdToInfractionMap.get(contactId) != null) {
                for (CPAO_Infraction__c infraction : contactIdToInfractionMap.get(contactId)) {
                    if (!(infraction.CPAO_Infraction_Type__r.Name.contains(CPAO_InfractionAssignBatch.AMD) ||
                          infraction.CPAO_Infraction_Type__r.Name.contains(CPAO_InfractionAssignBatch.APF) ||
                          infraction.CPAO_Infraction_Type__r.Name.contains(CPAO_InfractionAssignBatch.PIF))) {
                        isAutomatic = false;
                    }
                }
            }

            if (isAutomatic) {
                automaticApplications.add(applicationContactIds.get(contactId));
            } else {
                manualApplications.add(applicationContactIds.get(contactId));
            }

        }
        //List<CPAO_Infraction__c> infractionsToUpdate = new List<CPAO_Infraction__c>();

        List<CPAO_Application__c> applicationsToUpdate = new List<CPAO_Application__c> ();
        if (!automaticApplications.isEmpty()) {

            List<Contact> contactsToUpdate = new List<Contact> ();
            for (CPAO_Application__c application : automaticApplications) {
                applicationsToUpdate.add(new CPAO_Application__c(Id = application.Id, CPAO_Status__c = COMPLETED));
                contactsToUpdate.add(new Contact(Id = application.CPAO_Contact__c,
                                                 CPAO_CPA_Status__c = ACTIVE, CPAO_CPA_Sub_Status__c = null));
            }

            if (!contactsToUpdate.isEmpty()) {
                update contactsToUpdate;
            }
            //sendRegistrarReinstatementEmail(automaticApplications);
        }

        if (!manualApplications.isEmpty()) {
            List<Group> roStaffQueue = CPAO_GroupSelector.getROStaffQueue();
            if (roStaffQueue.size() == 1) {
                for (CPAO_Application__c application : manualApplications) {
                    if (application.OwnerId != roStaffQueue[0].Id) {
                        applicationsToUpdate.add(new CPAO_Application__c(Id = application.Id, OwnerId = roStaffQueue[0].Id));
                    }
                }
            }
        }
        if (!applicationsToUpdate.isEmpty()) {
            update applicationsToUpdate;
        }

    }

    public static void sendEmails(List<CPAO_Application__c> applications, String emailTemplateDeveloperName) {
        List<OrgWideEmailAddress> owa = [select id, DisplayName, Address from OrgWideEmailAddress limit 1];
        List<EmailTemplate> emailTemplate = [SELECT Id, Body, Subject from EmailTemplate where DeveloperName =:emailTemplateDeveloperName limit 1];
        if (emailTemplate.size() == 1) {
            List<Messaging.SingleEmailMessage> messages = new List<Messaging.SingleEmailMessage> ();
            for (CPAO_Application__c application : applications) {
                
                Messaging.SingleEmailMessage mail = new Messaging.SingleEmailMessage();
                mail.setTemplateId(emailTemplate[0].Id);
                mail.setSaveAsActivity(false);
                mail.setTargetObjectId(application.CPAO_Contact__c);

                mail.setWhatId(application.Id); // Enter your record Id whose merge field you want to add in template
                mail.setOrgWideEmailAddressId(owa[0].id);
                messages.add(mail);
            }

            Messaging.SendEmailResult[] resultMail = Messaging.sendEmail(messages);
        }
    }

    public static void amdANDcpdInitializer(List<CPAO_Application__c> applications) {
        Set<Id> contactIds = new Set<Id> ();
        for (CPAO_Application__c app : applications) {
            contactIds.add(app.CPAO_Contact__c);
        }
        Map<Id, Contact> contactMap = new Map<Id, Contact> (CPAO_ContactSelector.getContactToUpdate(contactIds));

        for (CPAO_Application__c app : applications) {
            Contact applicationContact = contactMap.get(app.CPAO_Contact__c);
            if (applicationContact != null) {
                if (applicationContact.CPAO_AMD_Continuous_Waiver__c != null && applicationContact.CPAO_CPD_Continuous_Exemption__c == null) {
                    //mark application to skip AMD waiver process since there is already a continous AMD waiver
                    app.CPAO_Sub_Type__c = 'CPD Only';
                } else if (applicationContact.CPAO_AMD_Continuous_Waiver__c == null && applicationContact.CPAO_CPD_Continuous_Exemption__c != null) {
                    //mark application to skip CPD exemption process since there is already a continous CPD Exemption
                    app.CPAO_Sub_Type__c = 'AMD Only';
                } else if (applicationContact.CPAO_AMD_Continuous_Waiver__c == null && applicationContact.CPAO_CPD_Continuous_Exemption__c == null) {
                    //mark application to skip nothing
                    app.CPAO_Sub_Type__c = 'AMD / CPD';
                } else if (applicationContact.CPAO_AMD_Continuous_Waiver__c != null && applicationContact.CPAO_CPD_Continuous_Exemption__c != null) {
                    //mark application to skip nothing
                    app.CPAO_Sub_Type__c = 'Review Only';
                }

            }
        }
    }
    
    /********Akshay Changes Start*****/
    public static void Find_PAL_License(List <CPAO_Application__c>  Applications){
    List <Id> ContactID = new List<Id>();
    List <ID> PAL_License = new List<Id>();
    List <CPAO_Application__c> lstApprovedApps = [SELECT ID,CPAO_Contact__c FROM CPAO_Application__c where ID in :Applications];
    Set <Id> conID= new Set <Id>();
    Map<Id, List<CPAO_Licence__c>> mapContactsWithLicense= new Map<Id, List<CPAO_Licence__c>>();
    string LicenseName='';
     //get the contact related to application
    for(CPAO_Application__c app : Applications)
        {
                ContactID.add(app.CPAO_Contact__c);                
                app.OwnerId='0053B000001mNNd';
        }      
    //get the license related to contact
    for(Contact objContact: [SELECT id, name,(SELECT ID,NAME,CPAO_Licence_Type__c,CPAO_Status__c from Licenses__r) FROM CONTACT where id in:ContactID]) 
        {
       mapContactsWithLicense.put(objContact.Id,objContact.Licenses__r);
       conID.add(objContact.id);
        }
     for (Id contactId_1 : mapContactsWithLicense.keySet())
        {
     if (mapContactsWithLicense.get(contactId_1) != null)
            {
                for (CPAO_Licence__c lic: mapContactsWithLicense.get(contactId_1)) 
                {
                    If(lic.CPAO_Licence_Type__c=='Public Accounting Licence' && lic.CPAO_Status__c=='Active')
                    {         PAL_License.Add(lic.id);
                              LicenseName=lic.name;
                    }
                }
            }
        }
        //Update contact object
        If (conID.isEmpty() == false)
        {
        List<Contact> listCons = [SELECT id, CPAO_CPA_Status__c from CONTACT WHERE Id in :conID];
        For (Contact oCon : listCons ) 
            {
            oCon.CPAO_CPA_Contact_Type__c='Member';
            oCon.CPAO_CPA_Status__c= 'Active';
            oCon.CPAO_PAL_ID__c=LicenseName;
            }
          Update listCons;
         }
        //Identify the email template to be used based on whether PAL license is available or not.
        if(!PAL_License.IsEmpty()) 
                {
                sendEmails(Applications, emailTemplate_PAL);
                 }
        else if (PAL_License.IsEmpty())
                {
                 sendEmails(Applications, emailTemplate_NO_PAL);
                }           
    }
    
    /********Akshay Changes End*****/
    
    //for applications where the registrar admits (MRA and not reistatement), an email is sent to the application, if tehre are conditions,
    //they are listed
    public static void registrarAdmitedApplications(List<CPAO_Application__c> applications) {
        Set<Contact> contactsToUpdate = new Set<Contact> ();
        Map<Id, String> applicationStatusToContacts = new Map<Id, String> ();
        Map<Id, String> applicationBodyToContacts = new Map<Id, String> ();
        for (CPAO_Application__c application : applications) {
            System.debug('application: ' + application);
            contactsToUpdate.add(new Contact(
                                             Id = application.CPAO_Contact__c,
                                             CPAO_CPA_Contact_Type__c = MEMBER,
                                             CPAO_CPA_Status__c = ACTIVE));
            applicationBodyToContacts.put(application.CPAO_Contact__c, application.CPAO_Accounting_Body__c);
            if (!String.isBlank(application.CPAO_Conditions__c) && application.CPAO_Decision__c == CPAO_ApplicationHandler.ADMIT_CONDITIONS) {
                applicationStatusToContacts.put(application.CPAO_Contact__c, application.CPAO_Conditions__c);
            }
        }
        System.debug('Marco test contactsToUpdate: ' + contactsToUpdate);
        update new List<Contact> (contactsToUpdate);

        if (applications.size() > 1) {
            return;
        }

        Map<String, Id> users2EmailToContacts = new Map<String, Id> ();
        for (User user : CPAO_UserSelector.getUsersWithContactList(contactsToUpdate)) {
            users2EmailToContacts.put(user.Email, user.ContactId);
        }
        Messaging.SingleEmailMessage[] messages = new List<Messaging.SingleEmailMessage> ();
        for (String email : users2EmailToContacts.keySet()) {

            String emailBody = label.Registrar_Admit_1;
            String accountingBody = applicationBodyToContacts.get(users2EmailToContacts.get(email));
            if (accountingBody != null) {
                emailBody = emailBody.replace('<body>', accountingBody);
            }
            if (applicationStatusToContacts.containsKey(users2EmailToContacts.get(email))) {
                emailBody = emailBody.substring(0, emailBody.length() - 1);
                emailBody = emailBody + label.Registrar_Admit_Conditions;
                emailBody = emailBody.replace('<conditions>', applicationStatusToContacts.get(users2EmailToContacts.get(email)));
            }
            emailBody = emailBody + label.Registrar_Admit_2;
            Messaging.SingleEmailMessage message = new Messaging.SingleEmailMessage();
            message.toAddresses = new List<String> { email };
            message.subject = 'Admission Granted';
            message.plainTextBody = emailBody;
            messages.add(message);
        }
        Messaging.SendEmailResult[] results = Messaging.sendEmail(messages);
    }

    public static void poaStamp(List<CPAO_Application__c> applications) {
        for(CPAO_Application__c app:applications){
            app.CPAO_Decision_Date_Hidden__c = Date.today();
            app.CPAO_Decision_Maker_Name_Lookup__c = UserInfo.getUserId();
        }
    }

    public static void poaCompleted(List<CPAO_Application__c> applications) {
        poaStamp(applications);
        List<EmailTemplate> lstEmailTemplates = [SELECT Id, Body, Subject 
                                                FROM EmailTemplate 
                                                WHERE DeveloperName = 'Compliance_Accepted_CPD_Plan_of_Action'];
        if(lstEmailTemplates.size() != 1){
            return;
        }
        Messaging.SingleEmailMessage[] messages = new List<Messaging.SingleEmailMessage>();

        for(CPAO_Application__c app:applications){
            Messaging.SingleEmailMessage mail = new Messaging.SingleEmailMessage();
            mail.setTemplateId(lstEmailTemplates[0].Id);
            mail.setSaveAsActivity(false);
            mail.setTargetObjectId(app.CPAO_Contact__c);
            mail.setWhatId(app.id);
            messages.add(mail);
        }           
        
        Messaging.SendEmailResult[] resultMail = Messaging.sendEmail(messages);
    }

    // This method calculates a PAL license expiry date.
    // If current date is between July 1 and December 31, 
    //Expiry Date should be October 31 of the current year + 1 year. Else if current date 
    //is between January 1 and june 30, Expiry date is October 31 of the current year.
    public static Date calculatePALExpiryDate(Date effectiveDate) {
        Integer currentYear = Date.today().year();
        Integer nextYear = (Date.today().addYears(1)).year();
        Date july1st = Date.newinstance(currentYear, 7, 1);
        Date december31st = Date.newinstance(currentYear, 12, 31);
        Date january1st = Date.newinstance(currentYear, 1, 1);
        Date june30th = Date.newinstance(currentYear, 6, 30);
        Date expiryDate = null;

        if (effectiveDate >= january1st && effectiveDate <= june30th) {
            expiryDate = Date.newinstance(currentYear, 10, 31);



        } else if (effectiveDate >= july1st && effectiveDate <= december31st) {
            expiryDate = Date.newinstance(nextYear, 10, 31);

        }
        return expiryDate;
    }
    public static void sendApprovalEmail(List<CPAO_Licence__c> licences, String licenceType) {
        List<SDOC__SDJob__c> jobList = new List<SDOC__SDJob__c> { };
        String templates = '';
        /*SDOC__SDTemplate__c PAL_Template_1 = new SDOC__SDTemplate__c();
        SDOC__SDTemplate__c PAL_Template_2 = new SDOC__SDTemplate__c();
        SDOC__SDTemplate__c COA_Template_1 = new SDOC__SDTemplate__c();
        SDOC__SDTemplate__c COA_Template_2 = new SDOC__SDTemplate__c();
        PAL_Template_1 =[SELECT ID FROM SDOC__SDTemplate__c WHERE Name='PAL Licence Attachment'];
        PAL_Template_2 =[SELECT ID FROM SDOC__SDTemplate__c WHERE Name='PAL Application Approved'];
        COA_Template_1 =[SELECT ID FROM SDOC__SDTemplate__c WHERE Name='COA Licence Attachment'];
        COA_Template_2 =[SELECT ID FROM SDOC__SDTemplate__c WHERE Name='COA Application Approved'];
        String PAL_templates = '';//'PAL_Template_1,PAL_Template_2';
        String COA_templates = '';//'COA_Template_1,COA_Template_2';*/
        /***** Removed hardcoded record IDs to make the code independent of instance *****/
         //String PAL_templates = '';//'a2K3B000000O1XV,a2K3B000000O1Wc';
         //String COA_templates ='' ;//'a2K3B000000O1YO,a2K3B000000O1YJ';
         List<CPAO_SDOC_Templates_Used_In_Applications__mdt> listOfTemplatesIds=CPAO_MetadataTypeSelector.getSDOCS_templatesIds('Base');
        if(!listOfTemplatesIds.isEmpty()){

            if (licenceType == 'PAL') {
                templates = listOfTemplatesIds[0].PAL_Licence_Attachment_SDOC_Template_Id__c+','+ listOfTemplatesIds[0].CPAO_PAL_Approval_SDOCS_Template_ID__c;

            } else if (licenceType == 'COA') {
                templates = listOfTemplatesIds[0].CPAO_COA_Licence_Attachment_Template_Id__c+','+ listOfTemplatesIds[0].CPAO_COA_Approval_SDOCS_Template_ID__c;

            }else if(licenceType=='PAL-Renewal'){
                templates = listOfTemplatesIds[0].PAL_Licence_Attachment_SDOC_Template_Id__c+','+ listOfTemplatesIds[0].CPAO_PAL_Ren_Approval_SDOCS_Template_ID__c;

            }

            for (CPAO_Licence__c licence : licences) {
                SDOC__SDJob__c job =
                new SDOC__SDJob__c(SDOC__Start__c = true,
                                   SDOC__Oid__c = licence.Id,
                                   SDOC__ObjApiName__c = 'CPAO_Licence__c',
                                   SDOC__SendEmail__c = '1',
                                   SDOC__Doclist__c = templates);
                jobList.add(job);
            }

            insert jobList;
        }
    
    }

    
    //public static CPAO_Licence__c createLicenceRecords(Id applicationId, Id contactId,String firmName) {

    //    CPAO_Licence__c associatedLicence = new CPAO_Licence__c();
    //    associatedLicence.CPAO_Related_Application__c = applicationId;
    //    associatedLicence.CPAO_Status__c = ACTIVE;
    //    associatedLicence.CPAO_Sub_Status__c = ORIGINAL;
    //    associatedLicence.CPAO_Issued_to__c = contactId;
    //    associatedLicence.CPAO_Effective_Date__c = Date.today();
    //    if(firmName==''){
    //        associatedLicence.CPAO_Licence_Type__c = PUBLIC_ACCOUNTING_TYPE;
    //        associatedLicence.CPAO_Expiry_Date__c = calculatePALExpiryDate(associatedLicence.CPAO_Effective_Date__c);

    //    }else{
    //        associatedLicence.CPAO_Licence_Type__c = CERTIFICATE_OF_AUTHORIZATION;
    //        associatedLicence.CPAO_Name_of_Professional_Corporation__c =firmName;
    //    }
        
    //    return associatedLicence;
    //}

    //This method stamps the reinstatement application lookup field on an infraction 
    //specific application
    //public static void stampReinstatementApplication(List<CPAO_Application__c> newApplications) {
    //    List<RecordType> infractionApplicationRecordType = CPAO_ApplicationSelector.getInfractionApplicationRecordType();
    //    System.debug('marco test 3');
    //    for (CPAO_Application__c application : newApplications) {
    //        if (application.RecordTypeId == infractionApplicationRecordType[0].Id) {
    //            application.CPAO_Reinstatement_Application__c = application.CPAO_ReinstatementApplicationFormula__c;
    //        }
    //    }
    //}

}