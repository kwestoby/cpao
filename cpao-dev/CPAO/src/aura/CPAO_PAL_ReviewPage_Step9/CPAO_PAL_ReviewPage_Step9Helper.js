({
    upsertPAL: function(component){
        
        var action = component.get("c.submitPalApplication");
        action.setParams({ "PALappToAdd" :component.get('v.currentPAL') });
        action.setCallback(this, function(response){
            var state = response.getState();
            
            if (state === "SUCCESS") {
                
                var results = response.getReturnValue();
                console.log(results);
                
                var orgID = results.orgID;
                delete results.orgID;
                console.log(orgID);
                console.log(results);
                var cookieValue = encodeURIComponent(JSON.stringify(results));
                console.log(cookieValue);
                
                
                //document.cookie = `apex__${orgID}-fonteva-shopping-cart=${cookieValue}`; 
                //document.cookie = 'test=test'; 
                //console.log("cookie saved");
                //console.log(encodeURIComponent(results));
                //alert('Thank you for submitting your PAL app. \n This is a Placeholder for payment.\n Application now has status "submitted"');
                //window.open('CPAO_ApplicationDashboard');
                //window.location.href='/CPAO_ApplicationDashboard';
                //window.location.href='/apex/orderapi__checkout';
                checkOut(component,'/CPAO_ApplicationDashboard');
                
            }else{
                console.log('error while submitting Pal');
                
            }
        });
        
        $A.enqueueAction(action);
    },
    fireReviewSectionModification:function(component,step){
        var modifyReviewSection = component.getEvent("modifyReviewSection");
        modifyReviewSection.setParams({
            "stepToRedirect": step,
        });
        modifyReviewSection.fire();
        //alert('fired');
    },
    
    checkOut : function(component, url) {
		var checkOutPage = component.getEvent("checkoutPage");
        applicationHasChanged.setParams({"url" : url} );
        applicationHasChanged.fire();
	}

})