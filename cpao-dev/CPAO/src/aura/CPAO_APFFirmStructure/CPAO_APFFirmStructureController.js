({
    doInit : function(component, event, helper) {
        // helper.getInitWrapper(component, helper);
    },

    handlePrev : function(component, event, helper) {
        helper.firePrevEvent(component);
    },

    handleNext : function(component, event, helper) {
        helper.updateAPF(component, helper, false);
    },

    handleSaveAndQuit : function(component, event, helper) {
        helper.updateAPF(component, helper, true);
    },
})