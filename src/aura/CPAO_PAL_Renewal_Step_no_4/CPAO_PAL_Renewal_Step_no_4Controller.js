({
	 clickOnNextButton : function(component,event,helper){
        var componentNameInEvent=event.getParam("componentName");
        if(componentNameInEvent==component.get('v.nameOfThisComponent')){
            if(event.getParam("nextStepName")=='Exit'){ 
                component.set('v.shouldSaveAndQuit',true);
            }

            //var ppad_req= component.find('ppad_req');
            var validForm = component.find('PALForm').reduce(function (validSoFar, inputCmp) {
                var cpdDeclaration =  component.get('v.currentPAL.CPAO_CPD_Declaration__c');  
                var specialConsideration = component.get('v.currentPAL.CPAO_CPD_Special_Consideration__c');  
                var verifiable_hours1 = component.get('v.currentPAL.CPAO_CPD_Year1_VerifiableHours__c');  
                var unverifiable_hours1 = component.get('v.currentPAL.CPAO_CPD_Year_1_Unverifiable_Hours__c');  
                var verifiable_hours2 = component.get('v.currentPAL.CPAO_CPD_Year2_VerifiableHours__c');  
                var unverifiable_hours2 = component.get('v.currentPAL.CPAO_CPD_Year_2_Unverifiable_Hours__c');  
                var verifiable_hours3 = component.get('v.currentPAL.CPAO_CPD_Year3_VerifiableHours__c');  
                var unverifiable_hours3 = component.get('v.currentPAL.CPAO_CPD_Year_3_Unverifiable_Hours__c');  
                var isValid = cpdDeclaration==component.get('v.completeRequirements') ||( cpdDeclaration==component.get('v.incompleteRequirements') && specialConsideration!=undefined &&verifiable_hours1!=undefined &&
                              unverifiable_hours1!=undefined && verifiable_hours2!=undefined &&unverifiable_hours2!=undefined && verifiable_hours3!=undefined &&
                               unverifiable_hours3!=undefined);
                // Displays error messages for invalid fields
                if(cpdDeclaration==undefined){
                    //$A.util.addClass(cpdDeclaration_req, 'slds-show');
                    component.set('v.showErrorMsg',true);
                }else{
                   // $A.util.removeClass(cpdDeclaration_req, 'slds-show');
                     component.set('v.showErrorMsg',false);
                }
            
                inputCmp.showHelpMessageIfInvalid();
                return validSoFar && inputCmp.get('v.validity').valid && isValid;
            }, true);
            
            if(validForm){
                //$A.util.removeClass(ppad_req, 'slds-show');
                component.set('v.showErrorMsg',false);
                var showSpinnerEvent = component.getEvent("showSpinner");
                showSpinnerEvent.fire();
                if(component.get('v.currentPAL.CPAO_CPD_Declaration__c')==component.get('v.completeRequirements')){
                	component.set('v.currentPAL.CPAO_PPAD_Special_Circumstances__c',null);
                    component.set('v.currentPAL.CPAO_CPD_Special_Consideration__c',null);
                    component.set('v.currentPAL.CPAO_CPD_Year1_VerifiableHours__c',null);
                    component.set('v.currentPAL.CPAO_CPD_Year_1_Unverifiable_Hours__c',null);
                    component.set('v.currentPAL.CPAO_CPD_Year2_VerifiableHours__c',null);
                    component.set('v.currentPAL.CPAO_CPD_Year_2_Unverifiable_Hours__c',null);
                    component.set('v.currentPAL.CPAO_CPD_Year3_VerifiableHours__c',null);
                    component.set('v.currentPAL.CPAO_CPD_Year_3_Unverifiable_Hours__c',null);

                }
                helper.upsertPAL(component);
            }
        }  	   
        
    }
})