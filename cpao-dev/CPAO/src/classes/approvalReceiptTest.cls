@isTest
public class approvalReceiptTest {
    public static List<Account> factoryAccounts;
    public static  List<OrderApi__Receipt__c> listReceipt;
    public static User userTest;
    public static User userApprover;
    public static Integer NUMBER_RECEIPTS =2;
    
    @testSetup
    public static void setup(){
        User thisUser = [SELECT Id FROM User WHERE Id = :UserInfo.getUserId()];
        System.runAs (thisUser) {
            /*setup environment*/
            CPAO_TestFactory.automationControlSetup();
            /*Setup Users*/
            
            UserRole userRole = new UserRole(DeveloperName = 'MyCustomRole', Name = 'My Role');
            insert userRole;
            userTest =  CPAO_TestFactory.createUser('userTest@cpaotest.com','System Administrator');
            userApprover =  CPAO_TestFactory.createUser('userapp@cpaotest.com','System Administrator'); 
            userTest.UserRoleId = userRole.id;
            userApprover.UserRoleId = userRole.id;
            insert userTest;
            insert userApprover;
            
            /*Setup Test Data*/
            factoryAccounts = CPAO_TestFactory.createAccounts(NUMBER_RECEIPTS);
            insert factoryAccounts;
            listReceipt =  CPAO_TestFactory.createReceipt(NUMBER_RECEIPTS,'Refund',factoryAccounts);
            for(OrderApi__Receipt__c receipt:listReceipt){
                receipt.OwnerId = userTest.Id; 
            }
            insert  listReceipt;
            
            Group grp =new Group(Name = 'CPAO PD Department',Type = 'Queue');
            insert grp;
            GroupMember groupMember = new GroupMember(UserorGroupId=userTest.Id, GroupId=grp.Id);
            insert groupMember;   
            
        }
    }
    
    @isTest
    public static void assignApprovalReceiptandRecall(){
        Test.startTest();
        listReceipt = new List<OrderApi__Receipt__c>();
        Map<String, OrderApi__Receipt__c> mapReceipt = new Map<String, OrderApi__Receipt__c>([Select ri.Name, ri.Id From OrderApi__Receipt__c ri]);
        userTest = [SELECT Id, Alias FROM User WHERE UserName='userTest@cpaotest.com'];
        System.debug('mapReceipt' + mapReceipt);      
        for(ID receiptID : mapReceipt.keySet()){
            System.debug(receiptID);
            OrderApi__Receipt__c receipt = mapReceipt.get(receiptID);
            receipt.OwnerId = userTest.Id;
            listReceipt.add(receipt);
        }
        update listReceipt;
        Test.stopTest();
        
        /*Check Approval creation*/
        List<ProcessInstance> listProcessInstance = [SELECT TargetObjectId, CreatedDate FROM ProcessInstance];
        for(ProcessInstance pi:listProcessInstance){
            System.assert(mapReceipt.get(pi.TargetObjectId) != null );
        }
        
        for(OrderApi__Receipt__c rec:listReceipt){
            approvalRecall.recallApproval(rec.Id);
        }
        
        /*Check Removal Approval*/
        for(ProcessInstance pi:listProcessInstance){
            System.assert(mapReceipt.get(pi.TargetObjectId) == null );
        }    
    }  
}