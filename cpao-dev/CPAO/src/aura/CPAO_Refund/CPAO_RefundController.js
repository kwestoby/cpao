({
    doInit: function(component, event, helper) {
        // initiates at aura handler in Lightning Component
        
        //calls method from Helper (which calls from Apex Class) - gets lists of receipt
        helper.getReceiptList(component);
    },
    requestRefund: function(component, event, helper){

        var source = component.get('v.selectedButton');
        source.set('v.label','Refund Requested');
        source.set('v.disabled',true);
        source.set('v.iconName',null);
        
        var reason = component.get('v.refundReason');
        var receiptID = component.get('v.selectedReceipt').Id;
 
        console.log(reason);
        
        if(reason == undefined || reason == ''){
            console.log('blank');
            component.find('reasonField').showHelpMessageIfInvalid();
        }else{
            helper.createRefund(component, receiptID, reason);
            component.set("v.isOpen", false);
            component.set("v.isOpen2", true);
            
        }
        
        

        
    },
    openModel: function(component, event, helper) {
     	// for Display Model,set the "isOpen" attribute to "true"
      	
      	component.set("v.isOpen", true);
        
    	// getting info from button in table  
      	var source = event.getSource();
        component.set("v.selectedReceipt",source.get('v.value'));
        component.set("v.selectedButton",source);
        
        //input info into modal
        
      
  	 },
 
   	closeModel: function(component, event, helper) {
      // for Hide/Close Model,set the "isOpen" attribute to "Fasle"  
      component.set("v.isOpen", false);
   	},
    closeModel2: function(component, event, helper) {
      // for Hide/Close Model,set the "isOpen" attribute to "Fasle"  
      component.set("v.isOpen2", false);
   	}
})