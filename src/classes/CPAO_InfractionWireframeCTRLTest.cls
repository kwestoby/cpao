@isTest
private class CPAO_InfractionWireframeCTRLTest {

	@isTest
	static void itShould() {
		CPAO_Infraction_Type__c infractionType = new CPAO_Infraction_Type__c(Name = 'Test');
		insert infractionType;

		CPAO_InfractionWireframeCTRL.componentInit();
		CPAO_InfractionWireframeCTRL.getReinstatementApplication();
		CPAO_InfractionWireframeCTRL.getAllInfractionsType();
		CPAO_InfractionWireframeCTRL.getCurrentInfractions();
		CPAO_InfractionWireframeCTRL.getLoggedUserInfo();

		CPAO_InfractionWireframeCTRL testObject = new CPAO_InfractionWireframeCTRL();
		testObject.nextStep();
		testObject.previousStep();
		testObject.cancelJP();
	}
}