@isTest
public with sharing class CPAO_poaPageCTRLTest {
	public static List<Account> factoryAccounts;
	public static List<Contact> factoryContacts;
	public static User currentUser;
	public static User user;
	public static RecordType amdCPDRecordType;
    //public static UserRole r;

	public static void dataInitialize(Integer numberOfAccounts, Integer numberOfContacts){
		CPAO_TestFactory.automationControlSetup();
		factoryAccounts = CPAO_TestFactory.createAccounts(numberOfAccounts);
		insert factoryAccounts;
		factoryContacts = CPAO_TestFactory.createContacts(factoryAccounts[0],numberOfContacts);

		insert factoryContacts;
		
        
        
		user = CPAO_TestFactory.createCommunityUser(factoryContacts[0].Id);
        insert user;
        

        currentUser = CPAO_UserSelector.getCurrentUser();
        amdCPDRecordType = [SELECT Id FROM RecordType 
							WHERE Name = :CPAO_ApplicationHandler.AMD_CPD 
							AND SobjectType = 'CPAO_Application__c'][0];
    }

    @isTest
	public static void getPOAinfoTest() {
		dataInitialize(1,1); 
		CPAO_Application__c application = new CPAO_Application__c();
		
		application.CPAO_Contact__c = factoryContacts[0].Id;
		application.CPAO_Status__c = 'Open';
		application.RecordTypeId = amdCPDRecordType.Id;
		application.CPAO_Sub_Status__c = 'POA Pending';
		insert application;
		system.runAs(user) {
			CPAO_poaPageCTRL.getPOAinfo();
		}

		Plan_of_Action_Items__c newPOA = new Plan_of_Action_Items__c(
			CPAO_Start_Date__c = Date.today().addDays(-1),
			CPAO_End_Date__c = Date.today().addDays(1),
			CPAO_CPD_Provider_or_Sponsoring__c = 'Test',
			CPAO_Course_Name__c = 'Test',
			CPAO_Research_Area__c = 'Test'
			);

		system.runAs(user) {
			CPAO_poaPageCTRL.saveNewPOA(newPOA, application.Id);
			newPOA.CPAO_Course_Name__c = null;
			CPAO_poaPageCTRL.saveNewPOA(newPOA, application.Id);
		}
	}

	@isTest
	public static void getPOAinfoTest2() {
		dataInitialize(1,1); 
		CPAO_Application__c application = new CPAO_Application__c();
		
		application.CPAO_Contact__c = factoryContacts[0].Id;
		application.CPAO_Status__c = 'Open';
		application.RecordTypeId = amdCPDRecordType.Id;
		application.CPAO_Sub_Status__c = 'POA Pending';
		insert application;

		Plan_of_Action_Items__c newPOA = new Plan_of_Action_Items__c(
			CPAO_Start_Date__c = Date.today().addDays(-1),
			CPAO_End_Date__c = Date.today().addDays(1),
			CPAO_CPD_Provider_or_Sponsoring__c = 'Test',
			CPAO_Course_Name__c = 'Test',
			CPAO_Research_Area__c = 'Test',
			CPAO_Application__c = application.Id);

		insert newPOA;

		system.runAs(user) {
			
			CPAO_poaPageCTRL.saveEditPOA(newPOA, application.Id);
			CPAO_poaPageCTRL.submitPOA(application);
			CPAO_poaPageCTRL.deletePOA(newPOA.Id, application.Id);
			CPAO_poaPageCTRL.saveEditPOA(newPOA, application.Id);

		}
	}
}