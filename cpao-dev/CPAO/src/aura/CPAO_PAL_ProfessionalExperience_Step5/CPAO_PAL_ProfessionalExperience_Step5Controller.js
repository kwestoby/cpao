({
    
   
    clickOnNextButton : function(component,event,helper){
        
        
        var componentNameInEvent=event.getParam("componentName");
        if(componentNameInEvent==component.get('v.nameOfThisComponent')){
            if(event.getParam("nextStepName")=='Exit'){ 
                component.set('v.shouldSaveAndQuit',true);
            }
            var validForm = component.find('PALForm').reduce(function (validSoFar, inputCmp) {
                var deficiencies =component.get('v.deficiencyValues');
                var declaration = component.get('v.currentPAL.CPAO_Professional_Experience_Declaration__c');  
                var special_consideration = component.get('v.currentPAL.CPAO_Special_Consideration__c');
                var special_consideration_req = component.find('special_consideration_req');
                var  startDate=component.get('v.currentPAL.CPAO_Exceptional_Circumstances_SD__c');
                var endDate=component.get('v.currentPAL.CPAO_Exceptional_Circumstances_End_Date__c');
                var invalidDate= component.find('invalidDate');

                // Displays error messages for invalid fields
                if(declaration==undefined){
                    component.set('v.declaration_required',true);

                }else{
                    component.set('v.declaration_required',false);
                }
                
                if(special_consideration==undefined){
                    component.set('v.special_consideration_required',true);
                }else{
                    component.set('v.special_consideration_required',false);
                }
                
                
                if(deficiencies.length==0){
                    component.set('v.deficiency_required',true);
                }else{
                     component.set('v.deficiency_required',false);
                }
                  
                if(startDate>=endDate){
                   component.set('v.invalidDate',true);
                 }else{
                        component.set('v.invalidDate',false);
                }

                
                inputCmp.showHelpMessageIfInvalid();
                return validSoFar && inputCmp.get('v.validity').valid && declaration!=undefined && 
                    special_consideration!=undefined && deficiencies.length!=0 && !(startDate>=endDate);
            }, true);
            
            var declaration = component.get('v.currentPAL.CPAO_Professional_Experience_Declaration__c');  
            var completedExperience = component.get('v.completeProfessionalExperience');
            if(validForm || (declaration==completedExperience && !validForm)){             
                component.set('v.declaration_required',false);
                component.set('v.deficiency_required',false);
                component.set('v.special_consideration_required',false);               
                component.set('v.invalidDate',false);
                var nextStep=event.getParam('nextStepName');
                //component.set('v.currentPAL.CPAO_Current_Step__c',nextStep);
                component.set('v.currentPAL',event.getParam('PALApplication'));
                var declaration= component.get('v.currentPAL.CPAO_Professional_Experience_Declaration__c');
                var incompleteProfessionalExperience =component.get('v.incompleteProfessionalExperience');
                var completeProfessionalExperience =component.get('v.completeProfessionalExperience');
                
                if(declaration==incompleteProfessionalExperience){
                    var startDate = component.get('v.currentPAL').CPAO_Exceptional_Circumstances_SD__c;
                    var endate = component.get('v.currentPAL').CPAO_Exceptional_Circumstances_End_Date__c;
                    var formattedDate1=startDate.substring(0,10);
                    var formattedDate2=endate.substring(0,10);
                    component.set('v.currentPAL.CPAO_Exceptional_Circumstances_SD__c',formattedDate1);
                    component.set('v.currentPAL.CPAO_Exceptional_Circumstances_End_Date__c',formattedDate2);
                    var deficiencyOptions=component.get('v.deficiencyValues');
                    for (var i=0;i<deficiencyOptions.length;i++){
                        if(deficiencyOptions[i]==component.get('v.totalDeficiency')){
                            component.set('v.currentPAL.CPAO_Total_Hours_Deficiency__c','Yes')
                        }else if(deficiencyOptions[i]==component.get('v.PADeficiency')){
                            component.set('v.currentPAL.CPAO_PA_Services_Deficiency__c','Yes')
                        }
                    }
                    
                    if(component.get('v.currentPAL.CPAO_PA_Services_Deficiency__c')==null){
                        component.set('v.currentPAL.CPAO_PA_Services_Deficiency__c','No')
                    }
                    if(component.get('v.currentPAL.CPAO_Total_Hours_Deficiency__c')==null){
                        component.set('v.currentPAL.CPAO_Total_Hours_Deficiency__c','No')
                    }
                    
                }else  if(declaration==completeProfessionalExperience){
                    component.set('v.currentPAL.CPAO_PA_Services_Deficiency__c',null);
                    component.set('v.currentPAL.CPAO_Total_Hours_Deficiency__c',null);
                    component.set('v.currentPAL.CPAO_Exceptional_Circumstances_Note__c',null);
                    component.set('v.currentPAL.CPAO_Exceptional_Circumstances_SD__c',null);
                    component.set('v.currentPAL.CPAO_Exceptional_Circumstances_End_Date__c',null);
                    component.set('v.currentPAL.CPAO_Special_Consideration__c',null);
                    /*var emptyDeficiencies=component.get('v.emptyDeficiency');
                 component.set('v.deficiencyValues',emptyDeficiencies)*/
                }
                
                
                var showSpinnerEvent = component.getEvent("showSpinner");
                showSpinnerEvent.fire();
                helper.upsertPAL(component);
                
                
            }else{
                console.log('form invalid');
            }
        } 
    },processUploadId: function(component,event,helper){
        //var uploadId=event.getParam("IdOfUploadedFile");
        // alert(uploadId);
        alert('processed'); 
        
    },
    handleFilesChange: function (component, event,helper) {
        var fileInput = event.getSource().get("v.files")[0];
        console.log('file input'+fileInput);
        console.log(fileInput.size);
        component.set('v.fileInput',fileInput);
        component.set('v.fileName',fileInput.name);
        component.set('v.SpecialConsiderationName',fileInput.name);
        // component.set('v.NotYetUploaded','true');
        
        //alert( component.get('v.fileName'));
        component.set('v.fileType',fileInput.type);
        
        var fr = new FileReader();
        var self = this;
        
        fr.onload = function() {
            var fileContents = fr.result;
            var base64Mark = 'base64,';
            var dataStart = fileContents.indexOf(base64Mark) + base64Mark.length;
            fileContents = fileContents.substring(dataStart);
            component.set('v.fileContents',fileContents);
            
            // helper.upload(component,fileInput, fileContents);
        };
        
        fr.readAsDataURL(fileInput);
    },/*
    uploadFile:function(component, event,helper,fieldName) {
        // var fileInput=
        // helper.upload(component,fileInput, fileContents);
    var currentPAL=event.getParam("currentPAL");
    component.set('v.currentPAL',currentPAL);
        var fieldName=event.getParam("fieldName");
         ///   alert(JSON.stringify(currentPAL));
                 //       alert(fieldName);
//

        helper.upload(component,component.get('v.fileInput'), component.get('v.fileContents'),fieldName);
       // helper.savePalApplication(component);

    }*/
    
    
    /* handleChange: function (cmp, event) {
        var changeValue = event.getParam("deficiencyValues");
        //alert(changeValue[0]);
    }*/
    
})