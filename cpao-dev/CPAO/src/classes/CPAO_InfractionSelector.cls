public without sharing class CPAO_InfractionSelector {
    public static final String AMD = 'AMD';
    public static final String CPD = 'CPD';

    public static List<CPAO_Infraction__c> getContactInfractions(Set<Id> contactIds) {
        //List<String> itemType = new List<String>{'%'+AMD+'%','%'+CPD+'%'};
        //AND CPAO_Infraction_Type__c LIKE :itemType
        return [SELECT Id, CPAO_Status__c, CPAO_Contact__c FROM CPAO_Infraction__c 
                WHERE CPAO_Contact__c IN:contactIds];
    }

    public static List<CPAO_Infraction__c> getInfractionById(Id infractionId) {
        return [SELECT Id, CPAO_Status__c, CPAO_Contact__c,CPAO_Reinstatement_Application__c
         FROM CPAO_Infraction__c WHERE Id=:infractionId];
    }
    
    public static List<CPAO_Infraction__c> getAccountInfractions(Set<Id> accountIds) {
        //List<String> itemType = new List<String>{'%'+AMD+'%','%'+CPD+'%'};
        return [SELECT Id, CPAO_Status__c, CPAO_Account__c FROM CPAO_Infraction__c 
                WHERE CPAO_Account__c IN:accountIds];
    }

    public static List<CPAO_Infraction__c> getContactOpenInfractions(Set<Id> contactIds) {
        
        return [SELECT Id, CPAO_Infraction_Type_Formula__c,Name,CPAO_Status__c, 
                    CPAO_Contact__c,CPAO_Infraction_Type__c,CPAO_Infraction_Start_Date__c, CPAO_Infraction_Type__r.Name,
                    CPAO_Form_Url__c,CPAO_Reinstatement_Application__c, 
                    CPAO_Infraction_Type__r.CPAO_Automatic_Closure__c, CPAO_Infraction_Type__r.CPAO_Breach_Instructions__c,
                	CPAO_Additional_Information_From_Member__c, CPAO_Marked_as_Resolved__c
                FROM CPAO_Infraction__c  
                WHERE CPAO_Contact__c IN:contactIds AND CPAO_Status__c IN ('Open','Pending Review') ORDER BY Id ASC];
    }

    //public static List<CPAO_Infraction__c> getApplicationInfractions(Id applicationId) {
    //    return [SELECT Id, Name FROM CPAO_Infraction__c WHERE CPAO_Reinstatement_Application__c = :applicationId];
    //}


    // ----------------Infraction Type Selectors--------
    public static List<CPAO_Infraction_Type__c> getInfractionType(String infractionType) {
        return [SELECT id, Name FROM CPAO_Infraction_Type__c WHERE Name = :infractionType];
    }
}