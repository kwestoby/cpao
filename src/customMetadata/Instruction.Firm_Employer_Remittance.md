<?xml version="1.0" encoding="UTF-8"?>
<CustomMetadata xmlns="http://soap.sforce.com/2006/04/metadata" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xmlns:xsd="http://www.w3.org/2001/XMLSchema">
    <label>Firm/Employer Remittance</label>
    <protected>false</protected>
    <values>
        <field>CPAO_Description__c</field>
        <value xsi:type="xsd:string">If your employer is paying on your behalf, please ensure they remit the payment by the due date.</value>
    </values>
    <values>
        <field>CPAO_Order__c</field>
        <value xsi:type="xsd:double">3.0</value>
    </values>
    <values>
        <field>CPAO_Type__c</field>
        <value xsi:type="xsd:string">Offline Payment</value>
    </values>
</CustomMetadata>
