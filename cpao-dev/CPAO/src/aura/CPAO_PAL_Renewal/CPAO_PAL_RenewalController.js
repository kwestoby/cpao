({
	doInit : function(component, event, helper) {
		var apexAction = component.get('c.getCurrentPALRenewalApplication');
        apexAction.setCallback(this, function(response) {
            var state = response.getState();
            if(state === "SUCCESS"){
                var result = response.getReturnValue();
                component.set("v.currentPAL", result.currentRenewal);
                component.set("v.associatedWithCPAOFirm",result.associatedWithCPAOFirm);
                component.set("v.metCPD_requirements",result.metCPD_requirements);
                component.set('v.metPracticeInspectionRequirements',result.metPracticeInspectionRequirements);
                component.set('v.countryOptions',result.countryOptions);
                component.set('v.accountingBodyOptions',result.accountingBodyOptions);
                component.set('v.needToMeetProfessionalRequirements',result.needToMeetProfessionalRequirements);
                //alert(component.get('v.needToMeetProfessionalRequirements'));
                //console.log(JSON.stringify(result));
               // console.log('met metPracticeInspectionRequirements'+JSON.stringify(result.metPracticeInspectionRequirements));

            }else{
            	 var errors=response.getError();
                 var message = errors[0];
                console.log(message);
            }
        });
        $A.enqueueAction(apexAction);
	},
	handleNext : function(component,event,helper){
        
        var getselectedStep = component.get("v.selectedStep");
        var palApp= component.get('v.currentPAL');
        console.log(palApp);
        if(getselectedStep == "step1"){
            component.set("v.selectedStep", "step2");
            //  component.set("v.nextButtonLabel", "Next");
            
        }
        else if(getselectedStep == "step2"){
            component.set("v.selectedStep", "step3");
            //component.set("v.nextButtonLabel", "Next");
        }
            else if(getselectedStep == "step3"){
                helper.notifyClickOnNextButton("CPAO_PAL_Renewal_Step_no_3",palApp,'step4');
                
                //  component.set("v.nextButtonLabel", "Next");
            }else if(getselectedStep == "step4"){
                helper.notifyClickOnNextButton("CPAO_PAL_Renewal_Step_no_4",palApp,'step5');
                
            }else if(getselectedStep == "step5"){
                helper.notifyClickOnNextButton("CPAO_PAL_Renewal_Step_no5",palApp,'step6');
                
            }else if(getselectedStep == "step6"){
                helper.notifyClickOnNextButton("CPAO_PAL_Renewal_Step_no_6",palApp,'step7'); 
            }
    },
    applyResponseFromChildComponent : function(component,event,helper){
        //TO DO: Update PAL APP
        ////console.log('response received from childs');
        // console.log('we can continue?'+(event.getParam('nextButtonClickeable')));
        // console.log(component.get("v.selectedStep"));
        if(event.getParam('nextButtonClickeable')){
            component.set('v.currentPAL',event.getParam('currentApplication'));
            console.log( 'returned application: '+JSON.stringify(component.get('v.currentPAL')));
            
           // helper.formatJavaScriptDates(component);
            if(component.get("v.selectedStep")== "step3"){
                 component.set("v.selectedStep", "step4");
               // helper.verifyIfSkippingCPDPage(component,'Next');
            }else if (component.get("v.selectedStep")== "step4"){
                component.set("v.selectedStep", "step5");
                //   component.set("v.nextButtonLabel", "Next");
            }else if (component.get("v.selectedStep")== "step5"){
                component.set("v.selectedStep", "step6");
                //   component.set("v.nextButtonLabel", "Next");
            }else if (component.get("v.selectedStep")== "step6"){
                component.set("v.selectedStep", "step7");
                
            }
            helper.hideSpinner(component);
            
        }
    },
    handlePrev : function(component,event,helper){
       var getselectedStep = component.get("v.selectedStep");
        if(getselectedStep == "step2"){
            component.set("v.selectedStep", "step1");
            // component.set("v.nextButtonLabel", "Start PAL Application");
        }else if(getselectedStep == "step3"){
            component.set("v.selectedStep", "step2");
        }else if(getselectedStep == "step4"){
                
                component.set("v.selectedStep", "step3");
        }else if(getselectedStep == "step5"){
                    
            component.set("v.selectedStep", "step4");
        } else if(getselectedStep == "step6"){
                    // helper.verifyIfSkippingCPDPage(component,'Previous');
                    component.set("v.selectedStep", "step5");
        }else if(getselectedStep == "step7"){
                    component.set("v.selectedStep", "step6");
        }
    },
    
    handleFinish : function(component,event,helper){
        var palApp= component.get('v.currentPAL');
        helper.notifyClickOnNextButton("CPAO_PAL_Renewal_Step_no_7",palApp,'');
        
    },
    
    saveAndQuit: function(component,event,helper){
        var getselectedStep = component.get("v.selectedStep");
        var palApp= component.get('v.currentPAL');
        if(getselectedStep == "step1" || getselectedStep == "step2"){
            window.location.href='/CPAO_ApplicationDashboard';
            return;
        } else {
            if(getselectedStep == "step3"){
                helper.notifyClickOnNextButton("CPAO_PAL_Renewal_Step_no_3",palApp,'step4');

            }else if(getselectedStep == "step4"){
                helper.notifyClickOnNextButton("CPAO_PAL_Renewal_Step_no_4",palApp,'step5');
                
            }else if(getselectedStep == "step5"){
                helper.notifyClickOnNextButton("CPAO_PAL_Renewal_Step_no5",palApp,'step6');
                
            }else if(getselectedStep == "step6"){
                helper.notifyClickOnNextButton("CPAO_PAL_Renewal_Step_no_6",palApp,'step7');
            }else if(getselectedStep == "step7"){
                helper.notifyClickOnNextButton("CPAO_PAL_Renewal_Step_no_7",palApp,'');
            }
        }


        
    },
    showSpinner: function (component, event, helper) {
        helper.showSpinner(component, event, helper);
    }
})