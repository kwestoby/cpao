({
    upsertCOA : function(component) {
        var currentCOA=component.get('v.currentCOA');
        var action = component.get("c.upsertCOAApplication");
        //alert(component.get('v.currentCOA'));
        action.setParams({ "COAAppToAdd" :currentCOA});
        action.setCallback(this, function(response){
            var state = response.getState();
            
            if (state === "SUCCESS") {
                console.log('COA added');
                if(component.get('v.shouldSaveAndQuit')){
                    window.location.href='/CPAO_ApplicationDashboard';
                    
                }else{
                    component.set('v.currentCOA',response.getReturnValue());
                    var applicationHasChanged = component.getEvent("applicationHasChanged");
                    applicationHasChanged.setParams({
                        "currentApplication": component.get("v.currentCOA"),
                        "nextButtonClickeable": "true"
                    });
                    applicationHasChanged.fire();
                }
                
            }else{
                console.log('error while adding COA');
                var error= response.getError();
                console.log(error[0]);
            }
        });
        
        $A.enqueueAction(action);
    }
})