@isTest
public class CPAO_KnowledgeCTRLTest  {
	@isTest
	static void testGetKnowledgeArticles() {
		String query = '{"searchKey":"CPAO_KnowledgeCTRLTest article","pageNumber":1,"pageSize":10,"sortBy":"Title","sortOrder":"ASC"}';
		Test.startTest();
		CPAO_KnowledgeCTRL.KnowledgeQueryResponse response = CPAO_KnowledgeCTRL.getKnowledgeArticles(query);
		Test.stopTest();
		System.assert(response.articles.size()==0);
	}
}