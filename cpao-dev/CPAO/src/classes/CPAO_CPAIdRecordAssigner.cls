public without sharing class CPAO_CPAIdRecordAssigner {
	//THis method gets called from triggers on account and contact, as well as button clicks on those objects as well
	//the wrapper class contains which type of object the Ids are linked to
	//the wrapper contains 2 different lists structures, at the moment only 1 gets processed, the other gets ignored
	public static String lookupException = 'Id Record does not have any lookup fields populated. In order to continue please fill in on of the lookup fields';
	public static String ACTIVE_STATUS = 'Active';
	public static String INACTIVE_STATUS = 'Inactive';
	public static Boolean hasrun = false;
	public static void assignIds(CPAO_RecordIdAssignmentWrapper wrapper) {
		// if the values are being put in manually, this branch is used
		// the method in the CPAO_IDRecordController class is called to get the Id records.
		// these records are then upserted with the right lookup value
		// upsert is needed since some might have been created, and some might need to be inserted for teh first time  
		hasrun = true;
		if(!wrapper.idsManualAssignment.isEmpty()){
			Set<Decimal> neededIds = new Set<Decimal>();
			for(Decimal neededId:wrapper.idsManualAssignment.values()){
				neededIds.add(neededId);
			}
			Map<Decimal, CPAO_CPA_Id__c> neededIdRecords = CPAO_IDRecordController.getIdsManual(neededIds);
			for(Id targetObjectRecord:wrapper.idsManualAssignment.keySet()){
				Decimal currentidNumber = wrapper.idsManualAssignment.get(targetObjectRecord);
				wrapper.idRecordsToTargets.put(neededIdRecords.get(currentidNumber), targetObjectRecord);
			}
			List<CPAO_CPA_Id__c> idsToRenderInactive = renderoldIdsInvalid(wrapper.idRecordsToRenderInactive);
			upsertIds(wrapper.idRecordsToTargets, wrapper.objectType, idsToRenderInactive);
		} else {
			// if the values are being assigned automatically, then this branch gets used
			// the method in the CPAO_IDRecordController class is called to get the Id records.
			// these records are then upserted with the right lookup value
			// the account/contact records are then updated with the Id values 
			// in a future call since this whole operation might be taking place in a trigger sense
			if(!wrapper.idsAutomaticAssignment.isEmpty()){

				Map<Id, Decimal> targetRecordIdToCPAid = new Map<Id, Decimal>();
				List<CPAO_CPA_Id__c> unusedIdRecords = CPAO_IDRecordController.getIds(wrapper.idsAutomaticAssignment.size());
				for(Integer i=0; i<wrapper.idsAutomaticAssignment.size(); i++){
					wrapper.idRecordsToTargets.put(unusedIdRecords[i], wrapper.idsAutomaticAssignment[i]);
					targetRecordIdToCPAid.put(wrapper.idsAutomaticAssignment[i], unusedIdRecords[i].Id_Name__c);
				}
				List<CPAO_CPA_Id__c> idsToRenderInactive = renderoldIdsInvalid(wrapper.idRecordsToRenderInactive);
				List<CPAO_CPA_Id__c> upsertedIds = upsertIds(wrapper.idRecordsToTargets, wrapper.objectType, idsToRenderInactive);
				updateTargetRecords(CPAO_CPAIdSelector.getExistingIdsFromRecordSet(upsertedIds));
			}
		}
		CPAO_IDRecordController.sendIdWarningEmail();
	}
	//fills in the proper lookup values (for account or contact) and then upserts the Ids
	public static List<CPAO_CPA_Id__c> upsertIds(Map<CPAO_CPA_Id__c, Id> idRecordsToTargets, String objectType, List<CPAO_CPA_Id__c> idsToRenderInactive) {
		if(objectType == 'Account'){
			for(CPAO_CPA_Id__c idRecord:idRecordsToTargets.keySet()){
				idRecord.Account__c = idRecordsToTargets.get(idRecord);
				idRecord.Status__c = ACTIVE_STATUS;
			}
		} else if(objectType == 'Contact'){
			for(CPAO_CPA_Id__c idRecord:idRecordsToTargets.keySet()){
				idRecord.Contact__c = idRecordsToTargets.get(idRecord);
				idRecord.Status__c = ACTIVE_STATUS;
			}
		}

		List<CPAO_CPA_Id__c> listToUpsert = new List<CPAO_CPA_Id__c>();
		listToUpsert.addAll(idRecordsToTargets.keySet());
		listToUpsert.addAll(idsToRenderInactive);
		upsert listToUpsert;
		return listToUpsert;
	}

	//for automatic assignments, the accounts/contacts need to be updated with the proper Id values
	// since this might be happening in a trigger sense, the method is done in the future

	static void updateTargetRecords(List<CPAO_CPA_Id__c> idList) {

		if(idList[0].Contact__c != null){
			Map<Id, Decimal> contactIdsToCPAIdVal = new Map<Id, Decimal>();
			for(CPAO_CPA_Id__c idRecordObj:idList){
				contactIdsToCPAIdVal.put(idRecordObj.Contact__c, idRecordObj.Id_Name__c);
			}
			List<Contact> contactsToUpdate = new List<Contact>();

			for(Contact contactToUpdate: CPAO_ContactSelector.getContactToUpdate(contactIdsToCPAIdVal.keySet())){
				contactToUpdate.CPAO_CPA_Id__c = contactIdsToCPAIdVal.get(contactToUpdate.Id);
				contactsToUpdate.add(contactToUpdate);
			}
			update contactsToUpdate;
		} else if(idList[0].Account__c != null){
			Map<Id, Decimal> accountIdsToCPAIdVal = new Map<Id, Decimal>();
			for(CPAO_CPA_Id__c idRecordObj:idList){
				accountIdsToCPAIdVal.put(idRecordObj.Account__c, idRecordObj.Id_Name__c);
			}
			List<Account> accountsToUpdate = new List<Account>();

			for(Account accountToUpdate: CPAO_AccountSelector.getAccountToUpdate(accountIdsToCPAIdVal.keySet())){
				accountToUpdate.CPAO_CPA_Id__c = accountIdsToCPAIdVal.get(accountToUpdate.Id);
				accountsToUpdate.add(accountToUpdate);
			}
			update accountsToUpdate;
		} else {
			throw new CPAO_CPAIdRecordAssigner_Exception(lookupException);
		}
	}

	//contacts that have Ids which ar no longer used
	public static List<CPAO_CPA_Id__c> renderoldIdsInvalid(List<Decimal> existingIds) {
		Set<Decimal> existingIdSet = new Set<Decimal>();
		existingIdSet.addAll(existingIds);
		List<CPAO_CPA_Id__c> idsToRenderInactive = CPAO_CPAIdSelector.getExistingIdsFromSet(existingIdSet);
		for(CPAO_CPA_Id__c idRecord:idsToRenderInactive){
			idRecord.Status__c = INACTIVE_STATUS;
		}
		return idsToRenderInactive;
	}

	public class CPAO_CPAIdRecordAssigner_Exception extends Exception {}
}