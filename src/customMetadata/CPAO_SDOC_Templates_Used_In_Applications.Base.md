<?xml version="1.0" encoding="UTF-8"?>
<CustomMetadata xmlns="http://soap.sforce.com/2006/04/metadata" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xmlns:xsd="http://www.w3.org/2001/XMLSchema">
    <label>Base</label>
    <protected>false</protected>
    <values>
        <field>CPAO_COA_Approval_SDOCS_Template_ID__c</field>
        <value xsi:type="xsd:string">a2K3B000000O1YJ</value>
    </values>
    <values>
        <field>CPAO_COA_Licence_Attachment_Template_Id__c</field>
        <value xsi:type="xsd:string">a2K3B000000O1YO</value>
    </values>
    <values>
        <field>CPAO_PAL_Approval_SDOCS_Template_ID__c</field>
        <value xsi:type="xsd:string">a2K3B000000O1Wc</value>
    </values>
    <values>
        <field>CPAO_PAL_Ren_Approval_SDOCS_Template_ID__c</field>
        <value xsi:type="xsd:string">a2K3B000000O4SV</value>
    </values>
    <values>
        <field>PAL_Licence_Attachment_SDOC_Template_Id__c</field>
        <value xsi:type="xsd:string">a2K3B000000O1XV</value>
    </values>
</CustomMetadata>
