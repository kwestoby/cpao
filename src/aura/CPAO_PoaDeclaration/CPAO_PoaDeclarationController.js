({
	doInit : function(component, event, helper) {
		var today = new Date();

		var dd = today.getDate();
		var mm = today.getMonth()+1;

		var yyyy = today.getFullYear();
		if(dd<10){
		    dd='0'+dd;
		} 
		if(mm<10){
		    mm='0'+mm;
		} 
		today = yyyy+'-'+mm+'-'+dd;

		component.set("v.today", today);
		var apexAction = component.get('c.loadPoaComplianceDeclaration');
        apexAction.setCallback(this, function(response) {
        	var state = response.getState();
        	if(state === "SUCCESS"){
                var result = response.getReturnValue();
                component.set("v.declarationApplication", result);
            }
        });
        $A.enqueueAction(apexAction);

	},

	handleClick : function(component, event, helper) {
		var apexAction = component.get('c.saveApplicationPoaCompliance');
		var app = component.get("v.declarationApplication");
		apexAction.setParams({
            "currentAMDCPDApplication": app
        });
        apexAction.setCallback(this, function(response) {
        	var state = response.getState();
        	if(state === "SUCCESS"){
                var result = response.getReturnValue();
                if(result == 'Success'){
                	console.log('Success');
                }
            }
        });
        $A.enqueueAction(apexAction);
	} 
})