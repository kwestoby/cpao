trigger CPAO_SalesOrder on OrderApi__Sales_Order__c (before insert, after insert, before update, after update) {

	CPAO_Automation_Control__c control = CPAO_Automation_Control__c.getOrgDefaults();
    if(!control.OrderApi_Sales_Order_c__c){
        return;
    }
    
    if(Trigger.isBefore && Trigger.isInsert){

    }
    if(Trigger.isBefore && Trigger.isUpdate){

    }

    if(Trigger.isAfter && Trigger.isInsert){
    	CPAO_SalesOrderHandler.afterInsertFilter(Trigger.New);
    }

    if(Trigger.isAfter && Trigger.isUpdate){
    	CPAO_SalesOrderHandler.afterUpdateFilter(Trigger.New, Trigger.oldMap);
    }
}