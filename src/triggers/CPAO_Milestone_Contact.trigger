/**
* @File Name    :   CPAO_Milestone_Contact
* @Description  :   Trigger Milestone Contact 
* @Date Created :   2018-07-11
* @Author       :   Michaell Reis Gasparini, Deloitte, mreisg@deloitte.com
* @group        :   Trigger
* @Modification Log:
**************************************************************************************
* Ver       Date        Author              Modification
* 1.0       2018-07-11  Michaell Reis       Created the file/class
**/

trigger CPAO_Milestone_Contact on Contact (before update) {

   Map<Id, Contact> contactToEvaluate = new Map<Id, Contact >();
    if(Trigger.isBefore&& Trigger.isUpdate){
        for(Contact conObj : Trigger.New){
            contactToEvaluate.put(conObj.Id,conObj);
        }
    }  
    
    CPAO_MilestoneContactHelper.historyTracker(contactToEvaluate);

}