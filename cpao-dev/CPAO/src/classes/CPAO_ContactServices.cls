public without sharing class CPAO_ContactServices  {
    
    public static String MEMBER_CONTACT_TYPE = 'Member';
    public static String ACTIVE_STATUS = 'Member Active';
    public static String LSO_BADGE_TYPE='LSO';
    public static String APP_CANCELLED = 'Cancelled';
    //public static String SUSPENDED_STATUS = 'Member Suspended';
    
    public static void assignBadgeToContacts(List<Id> contacts,String badgeType) {
        List<OrderApi__Badge_Type__c> assignedBadgeType =CPAO_BadgeSelector.getBadgeType(badgeType);
        Set<Id> setOfIds = new Set<Id>();
        setOfIds.addAll(contacts); 
        List<OrderApi__Badge__c> badgesToInsert = new List<OrderApi__Badge__c>();
        List<Contact> contactsToModify= CPAO_ContactSelector.getContactToUpdate(setOfIds); 
        System.debug('badhe type'+assignedBadgeType[0].Id);
        List<OrderApi__Badge__c>badgesAlreadyAssignedToContact=CPAO_BadgeSelector.getBadges(setOfIds,assignedBadgeType[0].Id);
        //This map will further be used to determine if a contact already has a similar badge of the same badge type that was assigned 
        //to him manually. 
        Map<Id,OrderApi__Badge__c>existantBadgesMap= new Map<Id,OrderApi__Badge__c>();
        for (OrderApi__Badge__c badge : badgesAlreadyAssignedToContact) {
            existantBadgesMap.put(badge.OrderApi__Contact__c,badge);
        }
        //If  a contact already has a similar badge of the same badge type , the system will not automaticly create it to avoid duplicates.
        for (Contact contact : contactsToModify) {
            if(existantBadgesMap.get(contact.Id)==null){
                OrderApi__Badge__c badgeToInsert = new OrderApi__Badge__c ();
                badgeToInsert.OrderApi__Contact__c=contact.Id;
                badgeToInsert.OrderApi__Badge_Type__c=assignedBadgeType[0].Id;
                badgesToInsert.add(badgeToInsert);
            }
        }
        
        if(!badgesToInsert.isEmpty()){
            insert(badgesToInsert);
        }
    }
    
    public static void suspendAction(List<Contact> contacts){
        User currentUser = CPAO_UserSelector.getCurrentUser();
        for(Contact contact:contacts){
            contact.CPAO_Suspension_Approved_By__c = currentUser.Id;
        }
    }

    public static void revokeDeregisterAction(List<Contact> contacts){
        User currentUser = CPAO_UserSelector.getCurrentUser();
        for(Contact contact:contacts){
            contact.CPAO_Revocation_Approved_By__c = currentUser.Id;
        }
    }

    public static void createReadmissionApplications(List<Contact> contacts){
        List<CPAO_Application__c> appsToInsert = new List<CPAO_Application__c>();
        String recordTypename = CPAO_ApplicationHandler.READMISSION_RECORDTYPENAME;
        Id readmissionRecordTypeId = Schema.SObjectType.CPAO_Application__c.getRecordTypeInfosByName().get(recordTypename).getRecordTypeId();

        for(Contact contact:contacts){
            appsToInsert.add(new CPAO_Application__c(
                RecordTypeId = readmissionRecordTypeId,
                CPAO_Contact__c = contact.Id,
                CPAO_Status__c = 'Open'));
        }
        if(!appsToInsert.isEmpty()){
            insert appsToInsert;
        }
    }
    
    public static void createReinstatementApplications(List<Contact> contacts){
        List<CPAO_Application__c> appsToInsert = new List<CPAO_Application__c>();
        String recordTypename = CPAO_ApplicationHandler.REINSTATEMENT;
        Id reinstatementRecordTypeId = Schema.SObjectType.CPAO_Application__c.getRecordTypeInfosByName().get(recordTypename).getRecordTypeId();

        for(Contact contact:contacts){
            appsToInsert.add(new CPAO_Application__c(
                RecordTypeId = reinstatementRecordTypeId,
                CPAO_Contact__c = contact.Id,
                CPAO_Status__c = 'Open'));
        }
        if(!appsToInsert.isEmpty()){
            insert appsToInsert;
        }
    }
    
    public static void cancelReinstatementApplications(List<Contact> contacts){
        
        String recordTypename = CPAO_ApplicationHandler.REINSTATEMENT;
        Id reinstatementRecordTypeId = Schema.SObjectType.CPAO_Application__c.getRecordTypeInfosByName().get(recordTypename).getRecordTypeId();
        List<CPAO_Application__c> reinstatementApps = [SELECT CPAO_Status__c, CPAO_Contact__c from CPAO_Application__c WHERE RecordTypeId=:reinstatementRecordTypeId];
        List<CPAO_Application__c> appsToUpdate = new List<CPAO_Application__c>();
        
        for(Contact contact: contacts){
            for(CPAO_Application__c app: reinstatementApps){
                if(app.CPAO_Contact__c == contact.Id){
                    app.CPAO_Status__c = APP_CANCELLED;
                    appsToUpdate.add(app);
                }
            }
        }
        
        update appsToUpdate;

    }
    
    public static void removeBadgeFromContact(List<Id> contacts,String badgeType) {
        
        /*if(badgeType==LSO_BADGE_TYPE){
Set<Id> idsContacts = new Set<Id>();
idsContacts.addAll(contacts); 
List<Contact> l=new List<Contact>;
l=CPAO_AccountSelector.getLSOAccounts
}else{*/
        List<OrderApi__Badge_Type__c> assignedBadgeType =CPAO_BadgeSelector.getBadgeType(badgeType);
        Set<Id> idsContacts = new Set<Id>();
        idsContacts.addAll(contacts); 
        List<OrderApi__Badge__c> badgesToDelete= CPAO_BadgeSelector.getBadges(idsContacts,assignedBadgeType[0].Id); 
        delete badgesToDelete;
        //}
        
        
        //TO DO:check if its the only account for which contact is primary , if yes delete 
    }
    public static void removeAllBadgesFromContact(Set<Id> idsContacts) {
        List<OrderApi__Badge__c> badgesToDelete= CPAO_BadgeSelector.getBadges(idsContacts); 
        delete badgesToDelete;
    }
    
    public static Boolean  contact_eligible_To_PAL_Badge(Contact contact){
        /*return (contact.CPAO_CPA_Contact_Type__c==MEMBER_CONTACT_TYPE && (contact.CPAO_CPA_Status__c
==ACTIVE_STATUS	||contact.CPAO_CPA_Status__c==SUSPENDED_STATUS));*/
        return (contact.CPAO_CPA_Contact_Type__c==MEMBER_CONTACT_TYPE && contact.CPAO_CPA_Status__c
                ==ACTIVE_STATUS	);
        
    }
}