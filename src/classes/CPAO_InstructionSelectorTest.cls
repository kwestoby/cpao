@isTest
public class CPAO_InstructionSelectorTest {
	@TestSetup
	static void setupData() {
		List<CPAO_Instruction__c> instructions = new List<CPAO_Instruction__c> ();
		instructions.add(new CPAO_Instruction__c(CPAO_Detail__c = 'MRA Instructions', CPAO_Applicable_To__c = 'MRA Application'));
		instructions.add(new CPAO_Instruction__c(CPAO_Detail__c = 'PT Instructions', CPAO_Applicable_To__c = 'PT Application'));
		instructions.add(new CPAO_Instruction__c(CPAO_Detail__c = 'PAL Instructions', CPAO_Applicable_To__c = 'PAL Application'));
		instructions.add(new CPAO_Instruction__c(CPAO_Detail__c = 'Offline Payment Instructions', CPAO_Applicable_To__c = 'Offline Payment'));
		insert instructions;
	}

	@IsTest
	static void testGetInstrucitons() {
		Test.startTest();
		System.assertEquals('MRA Instructions', CPAO_InstructionSelector.getInstructions(CPAO_InstructionSelector.MRA_APP).CPAO_Detail__c);
		System.assertEquals('PT Instructions', CPAO_InstructionSelector.getInstructions(CPAO_InstructionSelector.PT_APP).CPAO_Detail__c);
		System.assertEquals('PAL Instructions', CPAO_InstructionSelector.getInstructions(CPAO_InstructionSelector.PAL_APP).CPAO_Detail__c);
		System.assertEquals('Offline Payment Instructions', CPAO_InstructionSelector.getInstructions(CPAO_InstructionSelector.OFFLINE_PAYMENT).CPAO_Detail__c);
		Test.stopTest();
	}
}