/**
 * @author Sachet Khanal (sakhanal@deloitte.com)
 * @version 1.0.0
 * @description Batch class for writing off invoices.
 *
 *
 **/ 
global class CPAO_BulkInvoiceWriteOffBatch implements Database.Batchable<SObject> {

	private List<Id> invoiceIDs;

	global CPAO_BulkInvoiceWriteOffBatch(List<Id> invoiceIDs) {
		this.invoiceIDs = invoiceIDs;
	}

	/**
	 * @description gets invoked when the batch job starts
	 * @param context contains the job ID
	 * @returns the record set as a QueryLocator object that will be batched for execution
	 */
	global Database.QueryLocator start(Database.BatchableContext context) {
		return Database.getQueryLocator('SELECT Id, OrderApi__Balance_Due__c, OrderApi__Business_Group__c, OrderApi__Business_Group__r.OrderApi__Invoice_Write_off_Account__c FROM OrderApi__Invoice__c WHERE Id IN :invoiceIDs');
	}

	/**
	 * @description gets invoked when the batch job executes and operates on one batch of records. Contains or calls the main execution logic for the batch job.
	 * @param context contains the job ID
	 * @param scope contains the batch of records to process.
	 */
	global void execute(Database.BatchableContext context, List<OrderApi__Invoice__c> scope) {
		List<OrderApi__Invoice_Line__c> writeOffs = new List<OrderApi__Invoice_Line__c> ();
		for (OrderApi__Invoice__c invoice : scope) {
			writeOffs.add(new OrderApi__Invoice_Line__c(OrderApi__Invoice__c = invoice.Id,
			                                            OrderApi__Sale_Price__c = (invoice.OrderApi__Balance_Due__c * - 1),
			                                            OrderApi__Total__c = (invoice.OrderApi__Balance_Due__c * - 1),
			                                            OrderApi__Line_Description__c = 'Write Off',
			                                            OrderApi__Posted_Date__c = System.today(),
			                                            OrderApi__Is_Posted__c = true,
			                                            OrderApi__Business_Group__c = invoice.OrderApi__Business_Group__c,
			                                            OrderApi__Is_Adjustment__c = true,
			                                            OrderApi__GL_Account__c = invoice.OrderApi__Business_Group__r.OrderApi__Invoice_Write_off_Account__c));
		}
		insert writeOffs;
	}

	/**
	 * @description gets invoked when the batch job finishes. Place any clean up code in this method.
	 * @param context contains the job ID
	 */
	global void finish(Database.BatchableContext context) {

	}
}