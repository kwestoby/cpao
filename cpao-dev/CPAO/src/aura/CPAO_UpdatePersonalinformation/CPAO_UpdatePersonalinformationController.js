({
	doInit : function(component, event, helper) {
		var apexAction = component.get('c.loadReviewInfo');
        apexAction.setCallback(this, function(response) {
            var state = response.getState();
            console.log('state');
            console.log(state);
            if(state === "SUCCESS"){
                var result = response.getReturnValue();
                component.set("v.userContact", result.userContact);
                component.set("v.suffix", result.suffix);
                component.set("v.gender", result.gender);
                component.set("v.preferredPhone", result.preferredPhone);
                component.set("v.preferredMailingAddress", result.preferredMailingAddress);
                component.set("v.country", result.country);
                component.set("v.state", result.state);
                component.set("v.salutation", result.salutation);
            }
        });
        $A.enqueueAction(apexAction);
	},

    clickOnNextButton : function(component,event,helper){
        var talkingToComponent = event.getParam("componentName");
        if(talkingToComponent == 'step2'){
            var apexAction = component.get('c.saveContactInfo');

            var validForm = component.find('personalInfo').reduce(function (validSoFar, inputCmp) {
            // Displays error messages for invalid fields
            inputCmp.showHelpMessageIfInvalid();
            return validSoFar && inputCmp.get('v.validity').valid;
            }, true);
            if(validForm == false){
                helper.methodFailure(component, event, helper);
                return;
            }
            var contact = component.get("v.userContact");
            apexAction.setParams({
                "contact": contact
            });
            apexAction.setCallback(this, function(response) {
                var state = response.getState();
                if(state === "SUCCESS"){
                    var result = response.getReturnValue();
                    if(result == 'Success'){
                        var actionToTake = event.getParam("nextStepName");
                        var applicationHasChanged = component.getEvent("applicationHasChanged");
                        applicationHasChanged.setParams({
                            "nextButtonClickeable" : true,
                            "action" : actionToTake});
                        applicationHasChanged.fire();
                    } else {
                        helper.methodFailure(component, event, helper, result, 'Error');    
                    }
                } else {
                    helper.methodFailure(component, event, helper, state, 'Error');
                }
            });
            $A.enqueueAction(apexAction);
        }        
    }
})