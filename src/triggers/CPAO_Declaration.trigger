trigger CPAO_Declaration on CPAO_Declaration__c (before insert, after insert, before update, after update) {

	CPAO_Automation_Control__c control = CPAO_Automation_Control__c.getOrgDefaults();
	if(!control.CPAO_Declaration_c__c){
		return;
	}
	
	if(Trigger.isBefore && Trigger.isInsert){
		CPAO_DeclarationHandler.markProperType(Trigger.New);
	}

	if(Trigger.isBefore && Trigger.isUpdate){

	}

	if(Trigger.isAfter && Trigger.isInsert){
		CPAO_DeclarationHandler.markGoodCharacReviewRequired(Trigger.New);
	}
	if(Trigger.isAfter && Trigger.isUpdate){
		CPAO_DeclarationHandler.markGoodCharacReviewRequired(Trigger.New);
	}
}