({
	initialize : function(component, helper) {
        var action = component.get("c.getInitWrapper");

        action.setParams({
            parentId : component.get("v.parentId")
        });
        action.setCallback(this, function(response){
            var state = response.getState();
            var result = response.getReturnValue();
            var err = response.getError();
            if(state === "SUCCESS") {
                component.set("v.children", result.children);
                component.set("v.parent", result.parent);
                component.set("v.relationships", result.relationships);
            }
        });
        $A.enqueueAction(action);
    },

    saveNewBusiness : function(component, helper, newAccount) {
        var action = component.get("c.saveAccount");
        var newAccountJSON = JSON.stringify(newAccount);
        action.setParams({
            newAccountJSON : newAccountJSON
        });
        action.setCallback(this, function(response){
            var state = response.getState();
            var result = response.getReturnValue();
            var err = response.getError();
            if(state === "SUCCESS") {
                component.set("v.newAccount", undefined);
            }
        });
        $A.enqueueAction(action);
    },

    getDefault : function(component, children) {
		var parent = component.get("v.parent");
        var wrapper = {
            index        : children.length,
            parentId     : parent.Id,
            child        : null,
            isEdit       : true,
            relationship : 'Related Business',
        };
        return wrapper;
	},

    saveRelationship : function(component, helper, children) {
        var action = component.get("c.saveChildren");
        var childrenJSON = JSON.stringify(children);

        action.setParams({
            parentId : component.get("v.parent.Id"),
            childrenJSON : childrenJSON

        });
        action.setCallback(this, function(response){
            var state = response.getState();
            var result = response.getReturnValue();
            var err = response.getError();
            if(state === "SUCCESS") {
                component.set("v.children", result);
            }
        });
        $A.enqueueAction(action);
    }
})