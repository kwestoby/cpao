public class CPAO_ReinstatementBreachPage_CTRL {
    
    public static User LOGGED_USER = getLoggedUserInfo();
    
    @AuraEnabled
    public static ResultWrapper getUserBreaches(){
        ResultWrapper result = new ResultWrapper();
        result.currentInfractions = getCurrentInfractions();
        result.currentFiles = new List<List<ContentVersion>>();
        
        for(Integer i=0; i<result.currentInfractions.size();i++){
            result.currentFiles.add(getCurrentAttachments(result.currentInfractions.get(i).Id));
        }
        
        //get current attached files
        
        return result;
    }
    
    @AuraEnabled
    public static void updateUserBreaches(CPAO_Infraction__c[] infractionList, String files){

        List<List<fileWrapper>> fileList = (List<List<fileWrapper>>)System.JSON.deserialize(files, List<List<fileWrapper>>.Class);
        //System.debug(fileList);
        //[[{}][{}][{}][]][][][][]
        for(Integer i=0; i<fileList.size();i++){
            for(Integer j=0; j<fileList.get(i).size();j++){
                attachmentWrapper fileResult = handleFileInsert(fileList.get(i).get(j),infractionList[i]);
                if(fileResult.errorMSG != null){
                    //return fileResult.errorMSG; 
                    System.debug('failed to attach file');
                }
            }
        }
        
        update infractionList;
       
        /*for(Integer i=0; i<fileList.size();i++){
            if(fileList.get(i).title != null){
                attachmentWrapper fileResult = handleFileInsert(fileList.get(i),infractionList[i]);
                if(fileResult.errorMSG != null){
                    //return fileResult.errorMSG; 
                    System.debug('failed to attach file');
                }
            }
  
        }*/
 
    }
    
    public static attachmentWrapper handleFileInsert(fileWrapper file, 
	CPAO_Infraction__c currentBreach) {
        
		//delete current attachments
		//delete getCurrentAttachments(currentBreach.Id);       
        
        
		attachmentWrapper result = new attachmentWrapper();
		file.filecontent = EncodingUtil.urlDecode(file.filecontent, 'UTF-8');

		ContentVersion newFile = new ContentVersion();
        newFile.FirstPublishLocationId = currentBreach.Id;
        newFile.versionData = EncodingUtil.base64Decode(file.filecontent);
        newFile.title = file.title;
        newFile.pathOnClient = '/' + file.title;

        try {
			insert newFile;
		} catch(Exception e) {
			result.errorMSG = e.getMessage();
			return result;
		}
        /*
		Map<String,String> cpdMap = new Map<String,String>();
		Id newFileId = [Select Id, ContentDocumentId from ContentVersion Where id = :newFile.id].ContentDocumentId;
		cpdMap.put('Id', newFileId);
		cpdMap.put('Name', fileName);
		Map<String,Map<String,String>> fullMap = 
			(Map<String,Map<String,String>>)JSON.deserialize(currentReadmissionApp.CPAO_JSON_File_Map__c, Map<String,Map<String,String>>.class);
		if(fullMap.containsKey(key)){
			result.idToDelete = fullMap.get(key).get('Id');
		}
		fullMap.put(key, cpdMap);
		currentReadmissionApp.CPAO_JSON_File_Map__c = JSON.serialize(fullMap);
		result.app = currentReadmissionApp;
*/
		return result;
	}
    
    public static User getLoggedUserInfo(){
        return CPAO_UserSelector.getCurrentUser();
    }
    
    public static List<CPAO_Infraction__c>  getCurrentInfractions(){
         Set<Id> contactId=new Set<Id> ();
         contactId.add(LOGGED_USER.ContactId);
         return CPAO_InfractionSelector.getContactOpenInfractions(contactId);
    }
    
    public static List<ContentVersion> getCurrentAttachments(Id infractionId){
        List<ContentVersion> currentFiles = [SELECT Title, pathOnClient, ContentUrl FROM ContentVersion WHERE FirstPublishLocationId=:infractionId];
        
        return currentFiles;
    }
     
    public class ResultWrapper{
        @AuraEnabled public List<CPAO_Infraction__c> currentInfractions{get;set;}
        @AuraEnabled public List<List<ContentVersion>> currentFiles{get;set;}
    }
    
    public class fileWrapper{
        @AuraEnabled public String title {get;set;}
        @AuraEnabled public String filetype {get;set;}
        @AuraEnabled public String filecontent {get;set;}
            
    }
    
    public class attachmentWrapper{
		@AuraEnabled public CPAO_Infraction__c breach{get;set;}
        @AuraEnabled public Id idToDelete{get;set;}
        @AuraEnabled public String errorMSG{get;set;}
    }

}