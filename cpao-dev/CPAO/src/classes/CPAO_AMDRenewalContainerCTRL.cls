public without sharing class CPAO_AMDRenewalContainerCTRL {

	public static final String SUCCESS = 'Success';
	public static final String CANADA = 'Canada';
	public static final String BERMUDA = 'Bermuda';
	public static final String ONTARIO = 'Ontario';
	public static final String OPEN = 'Open';
	public static final String ACTIVE = 'Active';
	public static final String I_DO_NOT_COMPLY = 'I do not comply';
	public static final String OTHER_EXTRAORDINARY_CONSEQUENCES = 'Other extraordinary consequences';
	public static final String COMPLY_WITH_OTHER_BODY = 'Comply with other professional body requirements';
	public static final String FINANCIAL_HARDSHIP = 'Financial Hardship/Unemployment';

	
	@AuraEnabled
	public static instructionPageWrapper getWaivers() {
		InstructionPageWrapper results = new InstructionPageWrapper();
		results.hasAMDWaiver = false;
		results.hasCPDWaiver = false;

		User loggedUser = CPAO_UserSelector.getCurrentUser();
		
		if(!String.isEmpty(loggedUser.Contact.CPAO_AMD_Continuous_Waiver__c)){
			results.hasAMDWaiver = true;
		}
		if(!String.isEmpty(loggedUser.Contact.CPAO_CPD_Continuous_Exemption__c)){
			results.hasCPDWaiver = true;
		}
		if(loggedUser.Contact.MailingCountry != CANADA){
			results.livesInCanada = false;
		}
		List<CPAO_Application__c> currentAMDCPDApplication = 
				CPAO_ApplicationSelector.getUserApplicationsByRecordTypeNameAndStatus(OPEN, CPAO_ApplicationHandler.AMD_CPD);
		if(!currentAMDCPDApplication.isEmpty()){
			results.currentAMDCPDApplication = currentAMDCPDApplication[0];
		} else {
			return null;
		}

		results.capacityAccount = new Account(
			Id = results.currentAMDCPDApplication.CPAO_Capacity_through_Firm__c,
			Name = results.currentAMDCPDApplication.CPAO_Capacity_through_Firm__r.Name);
		results.waiverExplanations = new Map<String, String>();
		for(CPAO_Waiver_Explanation__mdt explanation:CPAO_MetadataTypeSelector.getWaiverExplanations()){
			results.waiverExplanations.put(explanation.Full_Name__c, explanation.CPAO_Explanation__c);
		}

		List<CPAO_Application__c> pastFinancialHardshipApplications = CPAO_ApplicationSelector.getUserPreviousFinancialHardshipAMD();

		if(pastFinancialHardshipApplications.size() >2){
			results.unemployed3TimesAlready = true;
		}

		results.communityURL = System.Label.Community_URL;

		results.serviceTypes = new List<String>();
		Schema.DescribeFieldResult profesionalServiceTypes = CPAO_Application__c.CPAO_Professional_Service_Work_Performed__c.getDescribe();
		for(Schema.PicklistEntry ple:profesionalServiceTypes.getPicklistValues()){
			results.serviceTypes.add(ple.getLabel());
		}

		results.registrantTypes = new Map<String,String>();
		Schema.DescribeFieldResult registrantTypesVals = CPAO_Application__c.CPAO_Registrant_Of__c.getDescribe();
		for(Schema.PicklistEntry ple:registrantTypesVals.getPicklistValues()){
			results.registrantTypes.put(ple.getLabel(), ple.getLabel());
		}

		if(results.currentAMDCPDApplication.CPAO_Professional_Service_Work_Performed__c == null){
			results.applicationServiceTypes = new List<String>();
		} else {
			results.applicationServiceTypes = results.currentAMDCPDApplication.CPAO_Professional_Service_Work_Performed__c.split('\\;');
		}

		return results;

	}

	@AuraEnabled
	public static waiverInfoWrapper getWaiverExplanations() {
		waiverInfoWrapper results = new waiverInfoWrapper();
		results.waiverExplanations = new Map<String, String>();
		results.amdWaiverTypes = new List<String>();

		List<CPAO_Application__c> currentAMDCPDApplication = 
				CPAO_ApplicationSelector.getUserApplicationsByRecordTypeNameAndStatus(OPEN, CPAO_ApplicationHandler.AMD_CPD);
		if(!currentAMDCPDApplication.isEmpty()){
			results.application = currentAMDCPDApplication[0];
		} else {
			return null;
		}

		for(CPAO_Waiver_Explanation__mdt explanation:CPAO_MetadataTypeSelector.getWaiverExplanations()){
			results.waiverExplanations.put(explanation.Full_Name__c, explanation.CPAO_Explanation__c);
		}

		Schema.DescribeFieldResult waiverTypes = CPAO_Application__c.CPAO_AMD_Waiver__c.getDescribe();
		for(Schema.PicklistEntry ple:waiverTypes.getPicklistValues()){
			results.amdWaiverTypes.add(ple.getLabel());
		}

		return results;
	}



	@AuraEnabled
	public static cpdPageWrapper getCPDandPOAinfo() {
		cpdPageWrapper results = new cpdPageWrapper();
		results.waiverExplanations = new Map<String, String>();
		List<CPAO_Application__c> currentAMDCPDApplication = 
				CPAO_ApplicationSelector.getUserApplicationsByRecordTypeNameAndStatus(OPEN, CPAO_ApplicationHandler.AMD_CPD);
		if(!currentAMDCPDApplication.isEmpty()){
			results.currentAMDCPDApplication = currentAMDCPDApplication[0];
		} else {
			return null;
		}

		String currentApplicationYear = String.valueOf(results.currentAMDCPDApplication.CreatedDate.year());
		Triennial_Period__mdt currentPeriod;
		for(Triennial_Period__mdt period:CPAO_MetadataTypeSelector.getTrienialPeriods()){
			if(period.First_Year__c == currentApplicationYear || period.Second_Year__c == currentApplicationYear || period.Third_Year__c == currentApplicationYear){
				currentPeriod = period;
			}
		}

		
		Boolean isThirdYear = false;
		if(currentPeriod != null){
			Integer yearOne = Integer.valueOf(currentPeriod.First_Year__c);
			Integer yearTwo = Integer.valueOf(currentPeriod.Second_Year__c);
			Integer yearThree = Integer.valueOf(currentPeriod.Third_Year__c);
			Integer currentYear = Integer.valueOf(Date.today().year());
			Id currentContactId = currentAMDCPDApplication[0].CPAO_Contact__c;
			List<CPAO_Application__c> pastApplicationsWithCPDNonComplies = [SELECT Id FROM CPAO_Application__c 
				WHERE RecordType.Name = :CPAO_ApplicationHandler.AMD_CPD AND CPAO_Declaration__c = :I_DO_NOT_COMPLY 
				AND CPAO_Contact__c = :currentContactId AND CALENDAR_YEAR(CreatedDate) != :currentYear  AND(
				CALENDAR_YEAR(CreatedDate) = :yearOne OR CALENDAR_YEAR(CreatedDate) = :yearTwo OR CALENDAR_YEAR(CreatedDate) = :yearThree)];
			if(!pastApplicationsWithCPDNonComplies.isEmpty()){
				results.priorNoComply = true;
			} else {
				results.priorNoComply = false;
			}

			if(currentYear == yearThree){
				isThirdYear = true;
			}
		}
		for(CPAO_Waiver_Explanation__mdt explanation:CPAO_MetadataTypeSelector.getCpdWaiverExplanations()){
			if(!isThirdYear){
				results.waiverExplanations.put(explanation.Full_Name__c, explanation.CPAO_Explanation__c);
			} else {
				results.waiverExplanations.put(explanation.Full_Name__c, explanation.Explanation_Third_Year__c);
			}
		}

		results.poaItems = CPAO_PlanOfActionSelector.getPOAsByApplicationId(results.currentAMDCPDApplication.Id);

		return results;
	}

	@AuraEnabled
	public static String extendedProfileSave(CPAO_Application__c currentAMDCPDApplication, List<String> services) {
		if(currentAMDCPDApplication.CPAO_professional_services_in_Ontario__c == 'No'){
			currentAMDCPDApplication.CPAO_Professional_Service_Work_Performed__c = null;
			currentAMDCPDApplication.CPAO_Capacity_through_Non_firm__c = null;
			currentAMDCPDApplication.CPAO_Capacity_through_Firm__c = null;
			currentAMDCPDApplication.CPAO_Registrant_Of__c = null;
			try{
				update currentAMDCPDApplication;
			} catch (Exception e){
				return e.getMessage();
			}
			return 'Success';
		} else if(currentAMDCPDApplication.CPAO_professional_services_in_Ontario__c == 'Yes'){
			if(services != null){
				if(!services.isEmpty()){
					String appServicesValue = '';
					for(String service:services){
						appServicesValue = appServicesValue + service + ';';
					}
					appServicesValue = appServicesValue.substring(0, appServicesValue.length() - 1);
					currentAMDCPDApplication.CPAO_Professional_Service_Work_Performed__c = appServicesValue;
				}
			}				
			try{
				update currentAMDCPDApplication;
			} catch (Exception e){
				return e.getMessage();
			}
			return 'Success';
		}		
		return 'Error, professional service offering is blank';
	}

	@AuraEnabled
	public static String saveApplication(CPAO_Application__c currentAMDCPDApplication) {
		Contact applicationContact;
		
		String payingProvincialThroughOntario = (currentAMDCPDApplication.CPAO_Provincial_dues_paid_to__c == ONTARIO? 'Yes':'No');
		String payingCPACanadaThroughOntario = (currentAMDCPDApplication.CPAO_CPA_Canada_Fee_Paid_to__c == ONTARIO? 'Yes':'No');
		String livingInCanada = 'No';
		User currentUser = CPAO_UserSelector.getCurrentUser();
		if(currentUser.Contact.MailingCountry == CANADA || currentUser.Contact.MailingCountry == BERMUDA){
			livingInCanada = 'Yes';
			currentAMDCPDApplication.CPAO_Not_a_Resident_of_Canada__c = 'No';
			currentAMDCPDApplication.CPAO_Not_Present_in_Canada__c = 'No';
			currentAMDCPDApplication.CPAO_Not_Registered_for_GST_HST__c = 'No';
			currentAMDCPDApplication.CPAO_Do_not_Carry_Business_in_Canada__c = 'No';
			currentAMDCPDApplication.CPAO_Zero_Tax_Attestation__c = false;
		}
		Boolean matchingPricing = false;
		for(Annual_Membership_Due_Line__mdt amdMembershipLine:CPAO_MetadataTypeSelector.getAMDLines()){
			if(payingProvincialThroughOntario == amdMembershipLine.Paying_Provincial_Through_Ontario__c &&
				payingCPACanadaThroughOntario == amdMembershipLine.Paying_CPAC_Through_CPAO__c &&
				livingInCanada == amdMembershipLine.Live_In_Canada_Bermuda__c){
				if(amdMembershipLine.MasterLabel != currentAMDCPDApplication.CPAO_Contact__r.CPAO_AMD_Payment_Category__c){
					applicationContact = new Contact(Id = currentAMDCPDApplication.CPAO_Contact__c, 
						CPAO_AMD_Payment_Category__c = amdMembershipLine.AMD_Location_Based_Reduction__c);
					currentAMDCPDApplication.CPAO_AMD_Location_Based_Reduction__c = amdMembershipLine.AMD_Location_Based_Reduction__c;
					matchingPricing = true;

				}
			}
		}
		//AMD_Deferral_Date__c
		if(!matchingPricing){
			applicationContact = new Contact(Id = currentAMDCPDApplication.CPAO_Contact__c, CPAO_AMD_Payment_Category__c = '');
			currentAMDCPDApplication.CPAO_AMD_Location_Based_Reduction__c = '';
		}

		if(currentAMDCPDApplication.CPAO_Not_a_Resident_of_Canada__c == 'Yes' && currentAMDCPDApplication.CPAO_Not_Present_in_Canada__c == 'Yes' && 
		currentAMDCPDApplication.CPAO_Not_Registered_for_GST_HST__c == 'Yes' && currentAMDCPDApplication.CPAO_Zero_Tax_Attestation__c == true &&
		currentAMDCPDApplication.CPAO_Do_not_Carry_Business_in_Canada__c == 'Yes'){
			currentAMDCPDApplication.CPAO_AMD_Zero_Tax_Certificate__c = 'Yes';
			applicationContact.CPAO_Zero_Tax__c = 'Yes';
		} else {
			currentAMDCPDApplication.CPAO_AMD_Zero_Tax_Certificate__c = 'No';
			applicationContact.CPAO_Zero_Tax__c = 'No';
		}
		List<Application_Date__mdt> cpdDate = CPAO_MetadataTypeSelector.getApplicationDates();
		if(currentAMDCPDApplication.CPAO_AMD_Waiver__c != null && currentAMDCPDApplication.CPAO_AMD_Waiver__c != ''){
			for(CPAO_Waiver_Explanation__mdt explanation:CPAO_MetadataTypeSelector.getWaiverExplanation(currentAMDCPDApplication.CPAO_AMD_Waiver__c)){
				currentAMDCPDApplication.CPAO_Percentage_Discount__c = explanation.CPAO_Percent_Off__c;
				applicationContact.CPAO_AMD_Waiver_Discount_Percentage__c = explanation.CPAO_Percent_Off__c;
			}
			

			Integer currentYear = Integer.valueOf(Date.Today().year());
			Integer expiryMonth = cpdDate[0].CPD_Exemption_Expiry_Date__c.Month();
			Integer expiryDay = cpdDate[0].CPD_Exemption_Expiry_Date__c.Day();
			Integer effectiveMonth = cpdDate[0].CPD_Exemption_Effective_Date__c.Month();
			Integer effectiveDay = cpdDate[0].CPD_Exemption_Effective_Date__c.Day();
			currentAMDCPDApplication.CPAO_Waiver_Expiry_Date__c = Date.newInstance(currentYear+1, expiryMonth, expiryDay);
			currentAMDCPDApplication.CPAO_Waiver_Effective_Date__c = Date.newInstance(currentYear, effectiveMonth, effectiveDay);

		}
		OrderApi__Subscription__c subToUpdate = new OrderApi__Subscription__c();
		if(currentAMDCPDApplication.CPAO_Subscription__c != null){
			subToUpdate.Id = currentAMDCPDApplication.CPAO_Subscription__c;
			if(currentAMDCPDApplication.CPAO_Applied_for_Deferral__c == 'Yes'){
				Integer currentYear = Integer.valueOf(Date.Today().year());
				Integer deferralMonth = cpdDate[0].AMD_Deferral_Date__c.Month();
				Integer deferralDay = cpdDate[0].AMD_Deferral_Date__c.Day();
				subToUpdate.CPAO_Payment_Deferral_Due_Date__c = Date.newInstance(currentYear, deferralMonth, deferralDay);
			} else {
				subToUpdate.CPAO_Payment_Deferral_Due_Date__c = null;
			}
		}
			
		try{
			update currentAMDCPDApplication;
			update applicationContact;
			
			if(subToUpdate.Id != null){
				update subToUpdate;
			}
		} catch(Exception e) {
			return e.getMessage();
		}

		return SUCCESS;
	}

	@AuraEnabled
	public static String deletePOA(Id poaId) {
		try{
			Plan_of_Action_Items__c poaToDelete = new Plan_of_Action_Items__c(Id = poaId);
			delete poaToDelete;
		} catch(Exception e) {
			return e.getMessage();
		}

		return SUCCESS;
	}

	@AuraEnabled
	public static String saveAppAndPOA(List<String> keyList, List<String> fileNameList,  List<String> base64DataList, 
		List<String> contentTypeList, CPAO_Application__c currentAMDCPDApplication, String poaS) {

		List<Id> currentAttachmentIds = new List<Id>();
        List<contentWrapper> newWrappers = new List<contentWrapper>();
        List<ContentVersion> newFiles = new List<ContentVersion>();
      	Map<String,Map<String,String>> fullMap = new Map<String,Map<String,String>> ();
        if(currentAMDCPDApplication.CPAO_JSON_File_Map__c!=null){
        	fullMap=(Map<String,Map<String,String>>)JSON.deserialize(currentAMDCPDApplication.CPAO_JSON_File_Map__c, Map<String,Map<String,String>>.class);
        }
        if(!keyList.isEmpty()){
        	//System.debug('not empty');
          
            for(Integer i=0; i<keyList.size(); i++){
            	System.debug('in for');
                ContentVersion newFile = new ContentVersion();
                newFile.FirstPublishLocationId = currentAMDCPDApplication.Id;
                base64DataList[i] = EncodingUtil.urlDecode(base64DataList[i], 'UTF-8');
                newFile.versionData = EncodingUtil.base64Decode(base64DataList[i]);
                newFile.title = fileNameList[i];
                newFile.pathOnClient = '/' + fileNameList[i];
                newFiles.add(newFile);
                contentWrapper newWrapper = new contentWrapper();
                newWrapper.wrapperFile = newFile;
                newWrapper.key = keyList[i];
                newWrappers.add(newWrapper);
            }
            //System.debug('after for and before insert');
            try{
            	//System.debug('inside try catch');

            	insert newFiles;
            }catch(Exception e){
            	return e.getMessage();
            }
            

            Map<Id,Id> contentDocIdMap = new Map<Id,Id>();
            for(ContentVersion content:[SELECT Id, ContentDocumentId FROM ContentVersion WHERE Id IN :newFiles]){
                contentDocIdMap.put(content.Id, content.ContentDocumentId);
            }
            for(Integer i=0; i<newWrappers.size(); i++){
                if(fullMap.containsKey(newWrappers[i].key)){
                    currentAttachmentIds.add(fullMap.get(newWrappers[i].key).get('Id'));
                }
                Map<String,String> fileMap = new Map<String,String>();
                fileMap.put('Id', contentDocIdMap.get(newWrappers[i].wrapperFile.Id));
                fileMap.put('Name', newWrappers[i].wrapperFile.title);
                fullMap.put(newWrappers[i].key, fileMap);
            }
        	currentAMDCPDApplication.CPAO_JSON_File_Map__c = JSON.serialize(fullMap);
    	}else if(currentAMDCPDApplication.CPAO_Declaration__c != 'Other extraordinary consequences'){ // in this case we must delete file
			Id idFileToDelete= fullMap.get('fileNameExtraordinaryConsequences').get('Id');
			currentAttachmentIds.add(idFileToDelete);
			fullMap.remove('fileNameExtraordinaryConsequences');
			currentAMDCPDApplication.CPAO_JSON_File_Map__c = JSON.serialize(fullMap);
    	}
		if(currentAMDCPDApplication.CPAO_Declaration__c != null){
			List<CPAO_Waiver_Explanation__mdt> cpdExplanation = 
			CPAO_MetadataTypeSelector.getCpdWaiverExplanation(currentAMDCPDApplication.CPAO_Declaration__c);
			if(!cpdExplanation.isEmpty()){
				currentAMDCPDApplication.CPAO_CPD_Declaration_Explanation__c = cpdExplanation[0].CPAO_Explanation__c;
			}
		}
		
		if(currentAMDCPDApplication.CPAO_Declaration__c != OTHER_EXTRAORDINARY_CONSEQUENCES){
			currentAMDCPDApplication.CPAO_Other_Extraordinary_Consequences__c = null;
		}
		if(currentAMDCPDApplication.CPAO_Declaration__c != COMPLY_WITH_OTHER_BODY){
			currentAMDCPDApplication.CPAO_Other_Professional_Body__c = null;
		}
		if(currentAMDCPDApplication.CPAO_Declaration__c != I_DO_NOT_COMPLY || currentAMDCPDApplication.CPAO_Mulitple_POAs_in_Triennial_Period__c){
			currentAMDCPDApplication.CPAO_Number_of_Unverifiable_Hours_Short__c = null;
			currentAMDCPDApplication.CPAO_Number_of_Verifiable_Hours_Short__c = null;
			List<Plan_of_Action_Items__c> appPOAS = CPAO_PlanOfActionSelector.getPOAsByApplicationId(currentAMDCPDApplication.Id);
			try{
				if(!appPOAS.isEmpty()){
					delete appPOAS;
				}
				update currentAMDCPDApplication;
				if(!currentAttachmentIds.isEmpty()){
                List<ContentDocument> docsToDelete = new List<ContentDocument>();
                for(Id id:currentAttachmentIds){
                    docsToDelete.add(new ContentDocument(Id = id));
                }
                delete docsToDelete;
            }
			} catch(Exception e) {
				return e.getMessage();
			}
			return SUCCESS;
		}

		List<Plan_of_Action_Items__c> poaSObjects = (List<Plan_of_Action_Items__c>)JSON.deserialize(poaS, List<Plan_of_Action_Items__c>.class);
		//Decimal sumVerifiedHours = 0;
		//Decimal sumUnverifiedHours = 0;
		//for(Plan_of_Action_Items__c poa:poaSObjects){
		//	poa.CPAO_Application__c = currentAMDCPDApplication.Id;
		//	if(poa.CPAO_Verifiable__c == 'Yes'){
		//		sumVerifiedHours+= poa.CPOA_CPD_Hours__c;
		//	}
		//	if(poa.CPAO_Verifiable__c == 'No'){
		//		sumUnverifiedHours+= poa.CPOA_CPD_Hours__c;
		//	}
		//}
		//if(sumVerifiedHours != currentAMDCPDApplication.CPAO_Number_of_Verifiable_Hours_Short__c || 
		//sumUnverifiedHours != currentAMDCPDApplication.CPAO_Number_of_Unverifiable_Hours_Short__c){
			
		//}
		if(currentAMDCPDApplication.CPAO_Number_of_Unverifiable_Hours_Short__c == currentAMDCPDApplication.CPAO_Number_of_POA_Unverifiable_Hours__c  
		&& currentAMDCPDApplication.CPAO_Number_of_Verifiable_Hours_Short__c == currentAMDCPDApplication.CPAO_Number_of_POA_Verifiable_Hours__c){
			currentAMDCPDApplication.CPAO_Sub_Status__c = 'POA Compliance Pending';
		} else {
			currentAMDCPDApplication.CPAO_Sub_Status__c = 'POA Pending';
		}
		try{
			update currentAMDCPDApplication;
			upsert poaSObjects;
		} catch(Exception e) {
			return e.getMessage();
		}
		return SUCCESS;
	}

	@AuraEnabled
	public static String saveApplicationPoaCompliance(CPAO_Application__c currentAMDCPDApplication) {
		currentAMDCPDApplication.CPAO_Sub_Status__c = 'In POA Review';
		try{
			update currentAMDCPDApplication;
		} catch(Exception e) {
			return e.getMessage();
		}
		Boolean isCpdComplete = false;

		if(currentAMDCPDApplication.CPAO_Compliance_Declaration__c == true && currentAMDCPDApplication.CPAO_Record_Keeping_Declaration__c == true){
			isCpdComplete = true;
		}

		if(isCpdComplete){
			List<EmailTemplate> lstEmailTemplates = [SELECT Id, Body, Subject from EmailTemplate where DeveloperName = 'User_has_complied_with_CPD_POA_requirements'];
			if(lstEmailTemplates.size() == 1){
				Messaging.SingleEmailMessage mail = new Messaging.SingleEmailMessage();
				mail.setTemplateId(lstEmailTemplates[0].Id);
				mail.setSaveAsActivity(false);
				mail.setTargetObjectId(currentAMDCPDApplication.CPAO_Contact__c);

				mail.setWhatId(currentAMDCPDApplication.id); // Enter your record Id whose merge field you want to add in template
				Messaging.SendEmailResult[] resultMail = Messaging.sendEmail(new Messaging.SingleEmailMessage[] { mail });
			}
		}
		return SUCCESS;
	}

	@AuraEnabled
	public static poaLoadingWrapper loadPoaComplianceDeclaration() {
		poaLoadingWrapper results = new poaLoadingWrapper();
		results.currentAMDCPDApplication = CPAO_ApplicationSelector.getUserPoaDeclarations()[0];
		results.communityURL = System.Label.Community_URL + '/CPAO_ApplicationDashboard';
		return results;
	}

	public class poaLoadingWrapper{
		@AuraEnabled public CPAO_Application__c currentAMDCPDApplication{get;set;}
        @AuraEnabled public String communityURL{get;set;}
	}

	public class cpdPageWrapper{
		@AuraEnabled public List<Plan_of_Action_Items__c> poaItems{get;set;}
		@AuraEnabled public CPAO_Application__c currentAMDCPDApplication{get;set;}
        @AuraEnabled public Boolean priorNoComply{get;set;}
        @AuraEnabled public Map<String, String> waiverExplanations{get;set;}
	}

	public class InstructionPageWrapper{
		@AuraEnabled public CPAO_Application__c currentAMDCPDApplication{get;set;}
        @AuraEnabled public Boolean hasAMDWaiver{get;set;}
        @AuraEnabled public Boolean hasCPDWaiver{get;set;}
        @AuraEnabled public Boolean livesInCanada{get;set;}
        @AuraEnabled public Boolean unemployed3TimesAlready{get;set;}
        @AuraEnabled public Map<String, String> waiverExplanations{get;set;}
        @AuraEnabled public String communityURL{get;set;}
        @AuraEnabled public List<String> serviceTypes{get;set;}
        @AuraEnabled public Map<String,String> registrantTypes{get;set;}
        @AuraEnabled public List<String> applicationServiceTypes{get;set;}
        @AuraEnabled public Account capacityAccount{get;set;}
    }

    public class waiverInfoWrapper{
        @AuraEnabled public Map<String, String> waiverExplanations{get;set;}
        @AuraEnabled public List<String> amdWaiverTypes{get;set;}
        @AuraEnabled public CPAO_Application__c application{get;set;}
    }

    public class contentWrapper{
        @AuraEnabled public ContentVersion wrapperFile{get;set;}
        @AuraEnabled public String key{get;set;}
    }

    
}