global class CPAO_InfractionAssignBatch implements Database.Batchable<sObject> {
    public static final String AMD = 'Annual Membership Due';
    public static final String APF = 'Annual Practitioner Fee';
    public static final String PIF = 'Practice Inspection Fee';

    global Database.QueryLocator start(Database.BatchableContext BC) {
        List<String> itemType = new List<String>{'%'+AMD+'%','%'+APF+'%','%'+PIF+'%'};
        String query = 'SELECT Id, OrderApi__Contact__c, OrderApi__Item__r.Name FROM OrderApi__Subscription__c WHERE ';
        query = query + 'OrderApi__Item__r.Name LIKE :itemType AND OrderApi__In_Grace_Period__c = true AND OrderApi__Contact__c != null ';
        query = query + 'AND CPAO_Infraction_Created__c = false';
        return Database.getQueryLocator(query);
    }

    global void execute(Database.BatchableContext BC, List<OrderApi__Subscription__c> subscriptions) {
        List<String> subscriptionNames = new List<String>();
        for(OrderApi__Subscription__c subscription:subscriptions){
            subscriptionNames.add(subscription.OrderApi__Item__r.Name);
        }
        Map<String, Id> infractionTypes = new Map<String, Id>();
        for(CPAO_Infraction_Type__c infractionType:[SELECT id, Name FROM CPAO_Infraction_Type__c WHERE Name IN :subscriptionNames]){
            infractionTypes.put(infractionType.Name, infractionType.Id);
        }

        List<CPAO_Infraction__c> infractionsToCreate = new List<CPAO_Infraction__c>();
        for(OrderApi__Subscription__c subscription:subscriptions){
            infractionsToCreate.add(new CPAO_Infraction__c(
                CPAO_Infraction_Type__c = infractionTypes.get(subscription.OrderApi__Item__r.Name),
                CPAO_Contact__c = subscription.OrderApi__Contact__c));
            subscription.CPAO_Infraction_Created__c = true;
        }
        insert infractionsToCreate;
        update subscriptions;
    }
    
    global void finish(Database.BatchableContext BC) {
        
    }
    
}