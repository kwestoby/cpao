({
    doInit: function(component,event,helper){
        var escapedString="Quebec CPA Order/L'Ordre des CPA du Quebec";
        var CPA_Quebec="Quebec CPA Order/L Ordre des CPA du Quebec";
        var accountingBodies=component.get('v.options');
        //var unescapedString=accountingBodies[11];
        if(accountingBodies[12].label==CPA_Quebec){
            accountingBodies[12].label=escapedString;
        }
    },
    clickOnNextButton :  function(component,event,helper){
        //NICE TO HAVE: VALIDATE THAT REFERENCE 1 IS DIFERENT FROM REF 2

        var componentNameInEvent=event.getParam("componentName");
        var accountingBody1=component.get('v.currentPAL.CPAO_Membership_Body_Reference_1__c');
        var accountingBody2=component.get('v.currentPAL.CPAO_Membership_Body_Reference_2__c');
        var accounting_body_req1=component.find('accounting_body_req1');
        var accounting_body_req2=component.find('accounting_body_req2');
        var accountingBody1Valid=true;
        var accountingBody2Valid=true;
        if(componentNameInEvent==component.get('v.nameOfThisComponent')){
            if(event.getParam("nextStepName")=='Exit'){ 
                component.set('v.shouldSaveAndQuit',true);
            }
            var validForm = component.find('PALForm').reduce(function (validSoFar, inputCmp) {
                if(accountingBody1==undefined || accountingBody1=='--Please select an accounting body--'){
                    $A.util.addClass(accounting_body_req1, 'slds-show');
                    accountingBody1Valid=false;
                }else{
                    $A.util.removeClass(accounting_body_req1, 'slds-show');
                }
                
                if(accountingBody2==undefined || accountingBody2=='--Please select an accounting body--'){
                    $A.util.addClass(accounting_body_req2, 'slds-show');
                    accountingBody2Valid=false;
                }else{
                    $A.util.removeClass(accounting_body_req2, 'slds-show');
                }
                inputCmp.showHelpMessageIfInvalid();
                return validSoFar && inputCmp.get('v.validity').valid && accountingBody1Valid  && accountingBody2Valid}  , true);
            if(validForm){		
                
                $A.util.removeClass(accounting_body_req1, 'slds-show');
                $A.util.removeClass(accounting_body_req2, 'slds-show');
                var nextStep=event.getParam('nextStepName');
                //  component.set('v.currentPAL.CPAO_Current_Step__c',nextStep);
                component.set('v.currentPAL',event.getParam('PALApplication'));
                var showSpinnerEvent = component.getEvent("showSpinner");
                showSpinnerEvent.fire();
                helper.upsertPAL(component);
            }		
            
        }
    }
})