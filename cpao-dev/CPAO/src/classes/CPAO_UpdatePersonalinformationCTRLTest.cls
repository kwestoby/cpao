@isTest
public with sharing class CPAO_UpdatePersonalinformationCTRLTest {

	public static List<Account> factoryAccounts;
	public static List<Contact> factoryContacts;
	public static User user;

	public static void dataInitialize(Integer numberOfAccounts, Integer numberOfContacts) {
		CPAO_TestFactory.automationControlSetup();
		factoryAccounts = CPAO_TestFactory.createAccounts(numberOfAccounts);
		insert factoryAccounts;
		factoryContacts = CPAO_TestFactory.createContacts(factoryAccounts[0], numberOfContacts);
		insert factoryContacts;

		user = CPAO_TestFactory.createCommunityUser(factoryContacts[0].Id);
		insert user;

	}

	@isTest
	public static void standardTest() {
		dataInitialize(1, 1);

		system.runAs(user) {
			CPAO_UpdatePersonalinformationCTRL testObject = new CPAO_UpdatePersonalinformationCTRL();
			testObject.insertNewApplication();
			testObject.attachment = new Attachment(Body = Blob.valueof('myString'), Name = 'test');
			testObject.attachment2 = new Attachment(Body = Blob.valueof('myString'), Name = 'test');
			testObject.changeLegalName = false;
			testObject.contact.OtherCountryCode = 'CM';
			testObject.contact.MailingCountryCode = 'CM';
			testObject.save();
			testObject.cancelJP();

			testObject.contact.OtherCountryCode = 'CA';
			testObject.save();
			testObject.contact.OtherCountryCode = 'CM';
			testObject.contact.MailingCountryCode = 'CA';
			testObject.attachment = new Attachment(Body = Blob.valueof('myString'), Name = 'test');
			testObject.attachment2 = new Attachment(Body = Blob.valueof('myString'), Name = 'test');
			testObject.save();
			testObject.contact.MailingCountryCode = 'CM';

			testObject.attachment = null;
			testObject.changeLegalName = true;
			testObject.save();

			CPAO_UpdatePersonalinformationCTRL.ReviewPageWrapper wrapper = CPAO_UpdatePersonalinformationCTRL.loadReviewInfo();
			CPAO_UpdatePersonalinformationCTRL.saveContactInfo(testObject.contact);
		}

	}
}