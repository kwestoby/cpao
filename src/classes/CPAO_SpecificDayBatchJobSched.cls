global class CPAO_SpecificDayBatchJobSched implements Schedulable {
	global void execute(SchedulableContext sc) {
		Application_Date__mdt dates = CPAO_MetadataTypeSelector.getApplicationDates()[0];

		Integer currentYear = Date.today().year();
		
		if(Date.today() == Date.newinstance(currentYear, dates.Member_Pricing_Reset_Date__c.month(), dates.Member_Pricing_Reset_Date__c.day())){
			CPAO_ResetMemberPricingBatch a = new CPAO_ResetMemberPricingBatch();
        	database.executebatch(a);
		}
		if(Date.today() == Date.newinstance(currentYear, dates.CPD_Infraction_Date__c.month(), dates.CPD_Infraction_Date__c.day())){
			CPAO_CpdInfractionAssignmentBatch b = new CPAO_CpdInfractionAssignmentBatch();
        	database.executebatch(b);
		}
		
		if(Date.today() == Date.newinstance(currentYear, dates.AMD_Deferral_Date__c.month(), dates.AMD_Deferral_Date__c.day())){
			CPAO_CpdDeferredInfracAssigntBatch c = new CPAO_CpdDeferredInfracAssigntBatch();
        	database.executebatch(c);

        	CPAO_LateDeferralLockBatch e = new CPAO_LateDeferralLockBatch();
        	database.executebatch(e);
		}

		if(Date.today() == Date.newinstance(currentYear, dates.AMD_Renewal_Date__c.month(), dates.AMD_Renewal_Date__c.day())){
			CPAO_AutoRenewWaiverAmdSubsBatch d = new CPAO_AutoRenewWaiverAmdSubsBatch();
        	database.executebatch(d);
		}

		if(Date.today() == Date.newinstance(currentYear, dates.CPAO_Date_PAL_Appears_On_Portal__c.month(), dates.CPAO_Date_PAL_Appears_On_Portal__c.day())){
			CPAO_AutoRenewPALSubsBatch PAL_batchJob = new CPAO_AutoRenewPALSubsBatch();
        	database.executebatch(PAL_batchJob);
		}

		if(Date.today() == Date.newinstance(currentYear, dates.CPAO_Reset_Employer_is_paying_PALR__c.month(), dates.CPAO_Reset_Employer_is_paying_PALR__c.day())){
			CPAO_ClearEmployerIsPayingPALR PAL_Renewal_batchJob = new CPAO_ClearEmployerIsPayingPALR();
        	database.executebatch(PAL_Renewal_batchJob);
		}

	}
}