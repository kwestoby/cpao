public class CPAO_ReinstatementReviewPageCTRL {
    
    @AuraEnabled
	public static ReviewPageWrapper loadReviewInfo() {
		ReviewPageWrapper results = new ReviewPageWrapper();
		List<CPAO_Application__c> application = CPAO_ApplicationSelector.getUserApplicationsByRecordTypeNameAndStatus(
			CPAO_AMDRenewalContainerCTRL.OPEN, CPAO_ApplicationHandler.REINSTATEMENT);
		if(!application.isEmpty()){
			results.application = application[0];
		} else {
			return null;
		}

		User current = CPAO_UserSelector.getCurrentUser();
		results.userContact = CPAO_ContactSelector.getContactToUpdate(new Set<Id>{current.ContactId})[0];
		results.contactEmploymentRecords = CPAO_AccountContactRelationSelector.getRelationshipsByContactIdAndStatus(
			results.userContact.ID, CPAO_AMDRenewalContainerCTRL.ACTIVE);

		List<CPAO_Account_Contact_Relationship__c> primaryEmployers = CPAO_AccountContactRelationSelector.getPrimaryRelationshipsByContactId(
			new Set<Id>{results.userContact.Id});
		return results;
	}
    
    @AuraEnabled
	public static ReviewPageReturnWrapper saveAttestation(CPAO_Application__c currentReinstateApp) {
		ReviewPageReturnWrapper results = new ReviewPageReturnWrapper();
        
        //The line below is a placeholder for the payment page
        //currently, the application will go into submitted status after the review, when it actually needs to go into submitted after payment
        
        currentReinstateApp.CPAO_Status__c = 'Submitted';

		try{
			update currentReinstateApp;
            results.status = 'Success';
		} catch(Exception e) {
			results.status = 'Error';
			results.msg = e.getMessage();
			return results;
		}
        
        return results;
    }
    
    public class ReviewPageReturnWrapper{
		@AuraEnabled public String msg{get;set;}
		@AuraEnabled public String status{get;set;}
		@AuraEnabled public String checkOutScreenURL{get;set;}
	}
    
    public class ReviewPageWrapper{
		@AuraEnabled public CPAO_Application__c application{get;set;}
		@AuraEnabled public Contact userContact{get;set;}
		@AuraEnabled public List<CPAO_Account_Contact_Relationship__c> contactEmploymentRecords{get;set;}
	}

}