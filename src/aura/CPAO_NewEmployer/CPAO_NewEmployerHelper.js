({
	
    creationOfNewEmployer: function(component, event) {
    	
	    component.set("v.accountContactRel.CPAO_Relationship_to_Account__c","Employee");
	    component.set("v.accountContactRel.CPAO_Account__c", component.get('v.selectedAccount.val'));

	 	var action2 = component.get("c.SaveAccountContactRelationships");
	    action2.setParams({
	        "relationship": component.get("v.accountContactRel")
	    });  
	    action2.setCallback(this, function(response){
	    	var state = response.getState();
	   	    if (state === "SUCCESS") {
		       component.set("v.accountContactRel", response.getReturnValue());
		       component.set('v.severity','confirm');
		       component.set("v.confirmationMessage","The new employer have been added.");
		       var addedEmployerEvent = component.getEvent("newEmployerAdded");
		       var listOfRelationships=addedEmployerEvent.getParam("listOfRelationships");
		       // listOfRelationships.push(component.get("v.accountContactRel"));
			   //  addedEmployerEvent.setParams({"listOfRelationships" : listOfRelationships });
			   console.log('list'+listOfRelationships);
			   addedEmployerEvent.fire();
		      //$A.get('e.force:refreshView').fire();
	        }else{
	     	   component.set('v.severity','error');
	           component.set("v.confirmationMessage"," Database error"); 
	     	}
	    });
        $A.enqueueAction(action2);
    },

    
})