@isTest
private class CPAO_AnnualPractitionerFee_CTRLTest {
    
    private static final Id APF_APPLICATION_RECORD_TYPE_ID = CPAO_AnnualPractitionerFee_CTRL.APF_APPLICATION_RECORD_TYPE[0].Id;
    private static final String CPAO_APF = CPAO_AnnualPractitionerFee_CTRL.CPAO_APF;
    private static final String OPEN = CPAO_AnnualPractitionerFee_CTRL.OPEN;
    private static final String DEFAULT_STEP = CPAO_AnnualPractitionerFee_CTRL.DEFAULT_STEP;

    private static User currentUser;
    private static List<User> userWithApplication;
    private static List<Account> factoryAccounts;
    private static List<Contact> factoryContacts;
    private static List<User> communityUsers;

    private static Map<Id, Contact> factoryContactMap;

    @testSetup
    public static void dataInitialize(){
        CPAO_TestFactory.automationControlSetup();
        
        List<Account> factoryAccounts = CPAO_TestFactory.createAccounts(3);
        insert factoryAccounts;

        List<Contact> factoryContacts = CPAO_TestFactory.createContacts(factoryAccounts[0], 2);
        insert factoryContacts;
        
        communityUsers = new List<User>();
        for(Integer i = 0; i < factoryContacts.size(); i++) {
            Contact factoryContact = factoryContacts[i];
            User communityUser = CPAO_TestFactory.createCommunityUser(factoryContact.Id);
            communityUsers.add(communityUser);
        }
        insert communityUsers;
    }

    private static void fillStaticData() {
        currentUser = CPAO_UserSelector.getCurrentUser();

        factoryAccounts = [SELECT Id FROM Account];
        System.assertEquals(3, factoryAccounts.size());

        factoryContacts = [SELECT Id, CPAO_CPA_Status__c FROM Contact];
        factoryContactMap = new Map<Id, Contact>(factoryContacts);
        System.assertEquals(2, factoryContacts.size());System.debug(factoryContacts);


        communityUsers = [SELECT Id, ContactId FROM User WHERE ContactId IN: factoryContacts];
        System.assertEquals(factoryContacts.size(), communityUsers.size(), 'We should have used all the contacts to make community users');
    }

    private static CPAO_Application__c createAPFApplication(Id contactId) {
        return new CPAO_Application__c(
            CPAO_Contact__c = contactId,
            CPAO_Status__c = 'Open',
            RecordTypeId = APF_APPLICATION_RECORD_TYPE_ID,
            CPAO_Current_Step__c = 'step1'
        );
    }

    @isTest
    private static void updateStep() {
        fillStaticData();

        CPAO_Application__c application = createAPFApplication(factoryContacts[0].Id);
        insert application;
        
        String newStep = 'step2';
        User user = communityUsers[0];
        System.runAs(user) {
            CPAO_AnnualPractitionerFee_CTRL.updateStep(application.Id, newStep);
        }
        CPAO_Application__c updatedApplication = [SELECT Id, CPAO_Current_Step__c FROM CPAO_Application__c WHERE Id=:application.Id];
        System.assertEquals(newStep, updatedApplication.CPAO_Current_Step__c);
    }
    
    @isTest
    private static void getOpenAPFApplicationTest1() {
        fillStaticData();

        User user = communityUsers[1];
        CPAO_Application__c application;
        System.runAs(user) {
            List<CPAO_Application__c> existingAPFs = CPAO_ApplicationSelector.getUserApplicationsByRecordTypeNameAndStatus(OPEN, CPAO_APF);
            System.assert(existingAPFs.isEmpty(), 'This current user has no open applications');
        
            application = CPAO_AnnualPractitionerFee_CTRL.getOpenAPFApplication();
        }
        System.assertNotEquals(null, application, 'We should have created a new application');
        System.assertEquals(DEFAULT_STEP, application.CPAO_Current_Step__c, 'We should be starting from the beginning.');
        System.assertEquals(OPEN, application.CPAO_Status__c, 'We should have the new application.');
    }
    
    @isTest
    private static void getOpenAPFApplicationTest2() {
        fillStaticData();

        User user = communityUsers[0];
        CPAO_Application__c application = createAPFApplication(factoryContacts[0].Id);
        insert application;

        List<CPAO_Application__c> existingAPFs;
        System.runAs(user) {
            existingAPFs = CPAO_ApplicationSelector.getUserApplicationsByRecordTypeNameAndStatus(OPEN, CPAO_APF);
            System.assert(existingAPFs.size() == 1, 'This current user has open applications');
        
            application = CPAO_AnnualPractitionerFee_CTRL.getOpenAPFApplication();
        }
        System.assertEquals(existingAPFs[0].Id, application.Id, 'We should have not created a new application');
    }
    
    @isTest
    private static void getContactStatusTest() {
        fillStaticData();

        User user = communityUsers[0];
        Contact contact = factoryContactMap.get(user.ContactId);
        System.assertNotEquals(null, contact, 'Our user should have a contact');

        String status;
        System.runAs(user) {
            status = CPAO_AnnualPractitionerFee_CTRL.getContactStatus();
        }
        System.assertEquals(contact.CPAO_CPA_Status__c, status, 'The status fields should match');
    }
    
    @isTest
    private static void getAccountTest() {
        fillStaticData();

        Account account = CPAO_AnnualPractitionerFee_CTRL.getAccount(factoryAccounts[0].Id);
        System.assertNotEquals(null, account, 'We should have returned the account');
    }
    
    @isTest
    private static void getInstructionsTest() {
        List<CPAO_RosterInstructionScreen__mdt> apfInstructions = [
            SELECT CPAO_RosterInstruction__c 
            FROM CPAO_RosterInstructionScreen__mdt 
            WHERE DeveloperName =: CPAO_AnnualPractitionerFee_CTRL.APF_INSTRUCTIONS
        ];
        System.assert(!apfInstructions.isEmpty(), 'We do not have APF_INSTRUCTIONS required for this test method');

        String instructions = CPAO_AnnualPractitionerFee_CTRL.getInstructions();
        System.assert(String.isNotBlank(instructions), 'Our instructions should not be blank');
    }

    @isTest
    private static void getCurrentAPFTest() {
        fillStaticData();

        User user = communityUsers[0];
        CPAO_Application__c application = createAPFApplication(factoryContacts[0].Id);
        insert application;

        CPAO_AnnualPractitionerFee_CTRL.InitWrapper response;
        System.runAs(user) {
            response = CPAO_AnnualPractitionerFee_CTRL.getCurrentAPF(factoryAccounts[0].Id);
        }

        System.assertEquals(application.Id, response.currentAPF.Id, 'Application not same as inserted one');
        System.assertEquals(factoryAccounts[0].Id, response.account.Id, 'Account returned incorrect');
        System.assertEquals(true, String.isNotBlank(response.instructions), 'Instructions are blank or null');
        System.assertEquals(application.CPAO_Current_Step__c, response.step, 'Current application step does not match');
    }

    @isTest
    private static void getQuestionnaireTest() {
        fillStaticData();

        List<APF_Question__mdt> questions = [SELECT Id FROM APF_Question__mdt];
        System.assert(!questions.isEmpty(), 'We need the APF_Question__mdt metadata to complete this test');

        User user = communityUsers[0];
        CPAO_Application__c application = createAPFApplication(factoryContacts[0].Id);
        insert application;

        Map<String, CPAO_AnnualPractitionerFee_CTRL.Question> questionMap;
        System.runAs(user) {
            questionMap = CPAO_AnnualPractitionerFee_CTRL.getQuestionnaire(application);
        }
        System.assertEquals(questions.size(), questionMap.size(), 'We should have the same amount of questions as our metadata');
    }

    @isTest
    private static void updateAccountTest() {
        fillStaticData();

        CPAO_Application__c application = createAPFApplication(factoryContacts[0].Id);
        insert application;

        Account originalAccount = factoryAccounts[0];
        Account updatedAccount;
        Boolean exceptionOccurred = false;
        String errorMessage;
        User user = communityUsers[0];
        System.runAs(user) {
            try {
                originalAccount.CPAO_Designated_Administrator__c = 'steve';
                String accountJSON = JSON.serialize(originalAccount);
                String currentAPFJSON = JSON.serialize(application);
                updatedAccount = CPAO_AnnualPractitionerFee_CTRL.updateAccount(accountJSON, currentAPFJSON);
            }
            catch(Exception ex) {
                errorMessage = ex.getMessage() + '\n\n' + ex.getStackTraceString();
                exceptionOccurred = true;
            }
        }
        System.assertEquals(false, exceptionOccurred, 'Exception occured: '+errorMessage);
        System.assertEquals(originalAccount.Id, updatedAccount.Id, 'Shoud have received the same account');
        
        System.assertEquals(originalAccount.CPAO_Designated_Administrator__c, 
                            updatedAccount.CPAO_Designated_Administrator__c, 
                            'The CPAO_Designated_Administrator__c should have changed'
        );
    }

    @isTest
    private static void updateAPFTest() {
        fillStaticData();

        CPAO_Application__c originalApplication = createAPFApplication(factoryContacts[0].Id);
        insert originalApplication;

        CPAO_Application__c updatedApplication;
        Boolean exceptionOccurred = false;
        String errorMessage;
        User user = communityUsers[0];
        System.runAs(user) {
            try {
                originalApplication.CPAO_Public_Accountng_Accounting_Service__c = 'No';
                originalApplication.CPAO_Other_Services__c = 'Per Diem';
                String currentAPFJSON = JSON.serialize(originalApplication);
                updatedApplication = CPAO_AnnualPractitionerFee_CTRL.updateAPF(currentAPFJSON);
            }
            catch(Exception ex) {
                errorMessage = ex.getMessage() + '\n\n' + ex.getStackTraceString();
                exceptionOccurred = true;
            }
        }
        System.assertEquals(false, exceptionOccurred, 'Exception occured: '+errorMessage);
        System.assertEquals(originalApplication.Id, updatedApplication.Id, 'Shoud have received the same application');
        
        System.assertEquals(originalApplication.CPAO_Public_Accountng_Accounting_Service__c, 
                            updatedApplication.CPAO_Public_Accountng_Accounting_Service__c, 
                            'The CPAO_Designated_Administrator__c should have changed'
        );
    }

    //@isTest
    //private static void getFirmStructureTest() {
    //    fillStaticData();

    //    User user = communityUsers[0];
    //    CPAO_AnnualPractitionerFee_CTRL.FirmStructure struct;
    //    System.runAs(user) {
    //        struct = CPAO_AnnualPractitionerFee_CTRL.getFirmStructure();
    //    }
    //    System.assertNotEquals(null, struct, 'We should have returned the wrapper for the firm structure');
    //}

    @isTest
    private static void updateFirmStructureTest() {
        System.assert(false, 'This method is not completed');
    }
}