@isTest
public with sharing class CPAO_ApplicationHandlerTest {
	
	public static List<Account> factoryAccounts;
	public static List<Contact> factoryContacts;
	public static User currentUser;
	public static User user;

	public static void dataInitialize(Integer numberOfAccounts, Integer numberOfContacts){
		CPAO_TestFactory.automationControlSetup();
		factoryAccounts = CPAO_TestFactory.createAccounts(numberOfAccounts);
		insert factoryAccounts;
		factoryContacts = CPAO_TestFactory.createContacts(factoryAccounts[0],numberOfContacts);
		factoryContacts = CPAO_TestFactory.addContactStudentInfo(factoryContacts);

		insert factoryContacts;
		
		user = CPAO_TestFactory.createCommunityUser(factoryContacts[0].Id);
        insert user;

        currentUser = CPAO_UserSelector.getCurrentUser();
        insert new CPA_Id_Warning_Email__c(
        	SetupOwnerId=UserInfo.getOrganizationId(), 
        	CPA_Warning_Email_Body__c = 'Some Value',
        	CPA_Warning_Email_Subject__c = 'Some Value',
        	Warning_Email_send_to_address__c = 'test@test.com',
        	Warning_email_threshold__c = 10);

        insert new CPAO_CPA_Range_Details__c(
        	Name = 'test',
        	Active__c = true,
        	Next_Range_Id__c = 'Some Value',
        	Range_End__c = 50,
        	Range_Start__c = 10);

	}

	public static CPAO_Declaration__c cleanGoodCharacterDeclaration(CPAO_Application__c application) {
		return new CPAO_Declaration__c(
        	CPAO_Application__c = application.Id,
        	CPAO_Criminal_Charges_or_Convictions__c = 'no',
        	CPAO_Expelled_from_a_Professional_Org__c = 'no',
        	CPAO_Guilty_of_a_Sec_or_Tax_Leg__c = 'no',
        	CPAO_Made_assignment_in_bankruptcy__c = 'no',
        	CPAO_Subject_to_Disciplinary_Decision__c = 'no',
        	CPAO_Susp_Rev_of_Prof_License__c = 'no');
	}

	@isTest
	public static void amdCPDTest() {
		dataInitialize(1,1);
		RecordType amdCPDRecordType = [SELECT Id FROM RecordType 
												WHERE Name = :CPAO_ApplicationHandler.AMD_CPD 
												AND SobjectType = 'CPAO_Application__c'][0];

		List <CPAO_Application__c> applications = new List<CPAO_Application__c>();	

		CPAO_Application__c application1 = new CPAO_Application__c();
		application1.CPAO_Provincial_dues_paid_to__c = 'Ontario';
		application1.CPAO_CPA_Canada_Fee_Paid_to__c = 'Ontario';
		application1.RecordTypeId = amdCPDRecordType.Id;
		application1.CPAO_Applied_for_Deferral__c   = 'Yes';
		

		CPAO_Application__c application2 = new CPAO_Application__c();
		application2.CPAO_Provincial_dues_paid_to__c = 'Ontario';
		application2.CPAO_CPA_Canada_Fee_Paid_to__c = 'Ontario';
		application2.RecordTypeId = amdCPDRecordType.Id;
		application2.CPAO_Employer_is_paying_AMD__c  = 'Yes';

		CPAO_Application__c application3 = new CPAO_Application__c();
		application3.CPAO_Provincial_dues_paid_to__c = 'Ontario';
		application3.CPAO_CPA_Canada_Fee_Paid_to__c = 'Ontario';
		application3.RecordTypeId = amdCPDRecordType.Id;
		application3.CPAO_Employer_is_paying_AMD__c  = 'No';
		application3.CPAO_Applied_for_Deferral__c   = 'No';

		CPAO_Application__c application4 = new CPAO_Application__c();
		application4.CPAO_Provincial_dues_paid_to__c = 'Ontario';
		application4.CPAO_CPA_Canada_Fee_Paid_to__c = 'Ontario';
		application4.RecordTypeId = amdCPDRecordType.Id;
		application4.CPAO_Applied_for_Deferral__c   = 'Yes';
		application4.CPAO_Sub_Type__c = CPAO_ApplicationHandler.CPD_ONLY;
		
		applications.add(application1);
		applications.add(application2);
		applications.add(application3);
		applications.add(application4);

		system.runAs(user) {
			System.debug('marco test -2');
			insert applications;
			System.debug('marco test -1');
        }

        application1.CPAO_Status__c = CPAO_ApplicationHandler.SUBMITTED_DEFERRED;
		application2.CPAO_Status__c = CPAO_ApplicationHandler.SUBMITTED;
		application3.CPAO_Status__c = CPAO_ApplicationHandler.COMPLETED;
		application4.CPAO_Status__c = CPAO_ApplicationHandler.COMPLETED;
        update applications;

	}

	@isTest
	public static void standardTest() {
		dataInitialize(1,1);
		CPAO_Application__c application = new CPAO_Application__c();
		application.CPAO_Country__c = 'Australia';
		application.CPAO_Accounting_Body__c = 'CPA Australia';
		application.CPAO_Conditions__c = 'test';
		
		system.runAs(user) {
			System.debug('marco test -2');
			insert application;
			System.debug('marco test -1');
        }

        application.CPAO_Good_Character_Review_Notes__c = 'test';
        application.CPAO_Good_Standing_Verified__c = 'Yes';
        application.CPAO_Decision__c = 'Admit';

        system.runAs(currentUser) {
			update application;
	    }
	}

 //   @isTest
	//public static void bulkTest() {
	//	dataInitialize(1,1);
	//	List<CPAO_Application__c> applications = new List<CPAO_Application__c>();
	//	for(Integer i=0; i<250; i++){
	//		CPAO_Application__c application = new CPAO_Application__c();
	//		application.CPAO_Country__c = 'Australia';
	//		application.CPAO_Accounting_Body__c = 'CPA Australia';
	//		application.CPAO_Conditions__c = 'test';
	//		applications.add(application);
	//	}
			
	//	system.runAs(user) {
	//		insert applications;
 //       }
 //       for(Integer i=0; i<250; i++){
 //       	applications[i].CPAO_Good_Character_Review_Notes__c = 'test';
	//        applications[i].CPAO_Good_Standing_Verified__c = 'Yes';
	//        applications[i].CPAO_Decision__c = 'Admit';
 //       }
	//    Test.startTest();    
 //       system.runAs(currentUser) {
	//		update applications;
 //       }
 //       Test.stopTest();
	//}

 //   @isTest
	//public static void bulkTestInsert() {
	//	dataInitialize(1,1);
	//	List<CPAO_Application__c> applications = new List<CPAO_Application__c>();
	//	for(Integer i=0; i<250; i++){
	//		CPAO_Application__c application = new CPAO_Application__c();
	//		application.CPAO_Country__c = 'Australia';
	//		application.CPAO_Accounting_Body__c = 'CPA Australia';
	//		application.CPAO_Conditions__c = 'test';
	//		applications.add(application);
	//	}
			
	//	system.runAs(user) {
	//		insert applications;
 //       }
	//}

	@isTest
	public static void reinstatementSubmissionAutomatic() {
		dataInitialize(1,1);
		RecordType reinstatementRecordType = [SELECT Id FROM RecordType WHERE Name = :CPAO_ApplicationHandler.REINSTATEMENT AND SobjectType = 'CPAO_Application__c'][0];
		CPAO_Application__c application = new CPAO_Application__c(
			CPAO_Country__c = 'Australia',
			CPAO_Accounting_Body__c = 'CPA Australia',
			CPAO_Conditions__c = 'test',
			RecordTypeId = reinstatementRecordType.Id);
		
		system.runAs(user) {
			insert application;
        }
        CPAO_Declaration__c applicationDec = cleanGoodCharacterDeclaration(application);
        insert applicationDec;

        CPAO_Infraction_Type__c infractionType = CPAO_TestFactory.createInfractionTypes(1, CPAO_InfractionAssignBatch.AMD)[0];
        insert infractionType;
        CPAO_Infraction__c infraction = CPAO_TestFactory.createInfractions(1, infractionType)[0];
        infraction.CPAO_Contact__c = factoryContacts[0].Id;
        insert infraction;

        application.CPAO_Status__c = 'Submitted';
        system.runAs(currentUser) {
        	factoryContacts[0].CPAO_CPA_Contact_Type__c = 'Member';
        	update factoryContacts;
			update application;
	    }
	    application = [SELECT Id, CPAO_Status__c FROM CPAO_Application__c WHERE Id = :application.Id];
	    System.assert(application.CPAO_Status__c == 'Completed');

	    //infraction = [SELECT CPAO_Status__c, CPAO_Infraction_Close_Date__c FROM CPAO_Infraction__c WHERE Id = :infraction.Id];
	    //System.assert(infraction.CPAO_Status__c == 'Closed' && infraction.CPAO_Infraction_Close_Date__c == Date.today());

	    Contact contact = [SELECT CPAO_CPA_Status__c, CPAO_CPA_Sub_Status__c FROM Contact WHERE Id = :factoryContacts[0].Id][0];
	    //System.assert(contact.CPAO_CPA_Status__c == 'Member Active' && contact.CPAO_CPA_Sub_Status__c == 'Reinstated following suspension');
	}

	@isTest
	public static void reinstatementSubmissionManual() {
		dataInitialize(1,1);
		RecordType reinstatementRecordType = [SELECT Id FROM RecordType WHERE Name = :CPAO_ApplicationHandler.REINSTATEMENT AND SobjectType = 'CPAO_Application__c'][0];
		CPAO_Application__c application = new CPAO_Application__c(
			CPAO_Country__c = 'Australia',
			CPAO_Accounting_Body__c = 'CPA Australia',
			CPAO_Conditions__c = 'test',
			RecordTypeId = reinstatementRecordType.Id);
		
		system.runAs(user) {
			insert application;
        }
        CPAO_Declaration__c applicationDec = cleanGoodCharacterDeclaration(application);
        insert applicationDec;

		CPAO_Infraction_Type__c infractionType = CPAO_TestFactory.createInfractionTypes(1, 'test')[0];
        insert infractionType;
        CPAO_Infraction__c infraction = CPAO_TestFactory.createInfractions(1, infractionType)[0];
        infraction.CPAO_Contact__c = factoryContacts[0].Id;
        insert infraction;
        factoryContacts[0].CPAO_CPA_Contact_Type__c = 'Member';
    	update factoryContacts;

	    application.CPAO_Status__c = 'Submitted';
        system.runAs(currentUser) {
    		factoryContacts[0].CPAO_CPA_Contact_Type__c = 'Member';
    		factoryContacts[0].Email = 'test@test.com';
	       	update factoryContacts;
	       	CPAO_ApplicationHandler.firstRunAfter = true;
	       	CPAO_ApplicationHandler.firstRunBefore = true;
			update application;
	    }

	    application = [SELECT Id, CPAO_Status__c, OwnerId FROM CPAO_Application__c WHERE Id = :application.Id];
    	System.debug('marco test application150: ' + application);

    	infraction.CPAO_Status__c = CPAO_ApplicationServices.CLOSED;
    	update infraction;

    	application = [SELECT Id, CPAO_Status__c FROM CPAO_Application__c WHERE Id = :application.Id];
    	System.debug('marco test application156: ' + application);

    	application.CPAO_Recommendation__c = CPAO_ApplicationHandler.REINSTATE;
    	application.CPAO_Recommendation_Notes__c = 'test';
    	CPAO_ApplicationHandler.firstRunAfter = true;
    	CPAO_ApplicationHandler.firstRunBefore = true;
    	update application;

    	application.CPAO_Decision__c = CPAO_ApplicationHandler.REINSTATE;
    	CPAO_ApplicationHandler.firstRunAfter = true;
    	CPAO_ApplicationHandler.firstRunBefore = true;
    	update application;

    	application = [SELECT Id, CPAO_Status__c FROM CPAO_Application__c WHERE Id = :application.Id];
	    System.assert(application.CPAO_Status__c == 'Completed');

	    Contact contact = [SELECT CPAO_CPA_Status__c, CPAO_CPA_Sub_Status__c FROM Contact WHERE Id = :factoryContacts[0].Id][0];
	    //System.assert(contact.CPAO_CPA_Status__c == 'Member Active' && contact.CPAO_CPA_Sub_Status__c == 'Reinstated following suspension');
	}
}