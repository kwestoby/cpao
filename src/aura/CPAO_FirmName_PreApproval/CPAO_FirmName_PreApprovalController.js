({
    doInit : function(component,event,helper){
        var action =  component.get("c.doInitResp");
        action.setCallback(this,function(response) {            
            var status = response.getState();
            if (status == 'SUCCESS') {               
                var resp = response.getReturnValue(); 
                component.set('v.objApplication',resp) ;
            }            
        });
        $A.enqueueAction(action);
        
        
    },
    clickOnNextButton : function(component,event,helper){
        
        var componentNameInEvent=event.getParam("componentName");
        if(componentNameInEvent==component.get('v.nameOfThisComponent')){
            if(event.getParam("nextStepName")=='Exit'){ 
                component.set('v.shouldSaveAndQuit',true);
            }
            var lso_account_req1=component.find('lso_account_req1');
            var firmName=component.get('v.currentCOA.CPAO_COA_Firm_Name__c');
            var address=component.get('v.currentCOA.Address');
            if(firmName==undefined ||firmName=='--Please select an accounting body--'){
                $A.util.addClass(lso_account_req1, 'slds-show');
            }else{
                $A.util.removeClass(lso_account_req1, 'slds-show');
                var showSpinnerEvent = component.getEvent("showSpinner");
                showSpinnerEvent.fire();
                helper.upsertFMT(component);
                
            }         
            
        }
        
    },
    upsertFirmApproval : function(component,helper,event){
        // create a one-time use instance of the serverEcho action
        // in the server-side controller
        //var action = component.get("c.testClass");
        var action = component.get("c.InsertFirmField");
        console.log('objApplication: ' + component.get("v.objApplication"));
        //action.setParams({ NatureOfService : component.get("v.NatureOfService") });
		action.setParams({ objCPAO :component.get("v.objApplication")});
        // Create a callback that is executed after 
        // the server-side action returns
        action.setCallback(this, function(response) {
            var state = response.getState();
            if (state === "SUCCESS") {
                // Alert the user with the value returned 
                // from the server
                alert("From server: " + JSON.stringify(response.getReturnValue()));

                // You would typically fire a event here to trigger 
                // client-side notification that the server-side 
                // action is complete
            }
            else if (state === "INCOMPLETE") {
                // do something
            }
            else if (state === "ERROR") {
                var errors = response.getError();
                if (errors) {
                    if (errors[0] && errors[0].message) {
                        console.log("Error message: " + 
                                 errors[0].message);
                    }
                } else {
                    console.log("Unknown error");
                }
            }
        });

        // optionally set storable, abortable, background flag here

        // A client-side action could cause multiple events, 
        // which could trigger other events and 
        // other server-side action calls.
        // $A.enqueueAction adds the server-side action to the queue.
        $A.enqueueAction(action);
    }
})  
        /*var action = component.get('c.upsertFirmApproval');
        action.setParams({
            'NatureOfService' : 'test'
        });
        action.setCallback(this, function(response){
            var state = response.getState();
            console.log('state in upsertFirmApproval: ' +state);
            if (state === "SUCCESS") {
                console.log('in success upsertFirmApproval ----------------------');
                //helper.hideSpinner(component); 
                //var palApp= component.get('v.currentPAL');
               // helper.notifyClickOnNextButton("CPAO_FirmRegistration_Application",palApp,'');
            }
        });
        
        $A.enqueueAction(action);
        
    } */
   /* doInit : function(component,helper,event){
        console.log('Entered####');
    }
    */