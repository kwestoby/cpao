public without sharing class CPAO_RelatedBusinesses_CTRL {
	
    @AuraEnabled
    public static void saveAccount(String newAccountJSON) {
        Account newAccount = (Account) JSON.deserialize(newAccountJSON, Account.class);

        // New Accounts created by portal users are not verified and
        // need to be verified by the internal staff
        newAccount.CPAO_Account_Type__c = 'Non-firm';
        newAccount.CPAO_Account_Verified__c = 'Unverified';
        insert newAccount;
    }

    @AuraEnabled
    public static InitWrapper getInitWrapper(String parentId) {
        InitWrapper init = new InitWrapper();

        init.children = getChildren(parentId);
        init.relationships = getRelationshipPicklist();
        init.parent = getParent(parentId);
        
        return init;
    }

    public static Account getParent(String parentId) {
        Id parent = Id.valueOf(parentId);
        List<Account> accounts = CPAO_AccountSelector.getAccountsById(new Set<Id>{parent});
        return !accounts.isEmpty() ? accounts[0] : null;
    }

    public static List<RelatedWrapper> getChildren(String parentId) {
        List<RelatedWrapper> wrappers = new List<RelatedWrapper>();
        List<CPAO_Account_to_Account_Relationship__c> children = getAccountToAccountRelations(parentId);
        for(CPAO_Account_to_Account_Relationship__c child : children) {
            wrappers.add(new RelatedWrapper(child));
        }
        return wrappers;
    }

    public static List<CPAO_PicklistWrapper> getRelationshipPicklist() {
        List<CPAO_PicklistWrapper> options = new List<CPAO_PicklistWrapper>();
        List<Schema.PicklistEntry> entries = CPAO_Account_to_Account_Relationship__c.CPAO_Relationship_to_Associated_Account__c.getDescribe().getPicklistValues();
        for(Schema.PicklistEntry entry : entries) {
            options.add(new CPAO_PicklistWrapper(entry));
        }
        return options;
    }

    @AuraEnabled
    public static List<RelatedWrapper> saveChildren(String childrenJSON, String parentId) {
        List<RelatedWrapper> updatedChildren = (List<RelatedWrapper>)JSON.deserialize(childrenJSON, List<RelatedWrapper>.class);
        List<CPAO_Account_to_Account_Relationship__c> relationships = convertToSObjects(updatedChildren, parentId);
        try {
            upsert relationships;
        }
        catch(DmlException ex) {
            System.debug(ex.getMessage() + '\n\n' + ex.getStackTraceString());
            throw new AuraHandledException(ex.getMessage() + '\n\n' + ex.getStackTraceString());
        }
        return getChildren(parentId);
    }

    public static List<CPAO_Account_to_Account_Relationship__c> convertToSObjects(List<RelatedWrapper> children, String parentId) {
        List<CPAO_Account_to_Account_Relationship__c> relationships = new List<CPAO_Account_to_Account_Relationship__c>();
        for(RelatedWrapper child : children) {
            CPAO_Account_to_Account_Relationship__c relationship = new CPAO_Account_to_Account_Relationship__c();
            relationship.CPAO_Account_Name__c = Id.valueOf(parentId);
            relationship.CPAO_Associated_Account_Name__c = child.child.val;
            relationship.CPAO_Start_Date__c = child.effectiveDate;
            relationship.CPAO_End_Date__c = child.endDate;
            relationship.CPAO_Relationship_to_Associated_Account__c = child.relationship;
            relationship.Id = child.recordId;
            relationships.add(relationship);
        }
        return relationships;
    }

    public static List<CPAO_Account_to_Account_Relationship__c> getAccountToAccountRelations(String parentId) {
        return [
            SELECT 
                CPAO_Account_Name__c,
                CPAO_Account_Name__r.Name, 
                CPAO_Associated_Account_Name__c, 
                CPAO_Associated_Account_Name__r.Name,
                CPAO_Start_Date__c,
                CPAO_End_Date__c,
                CPAO_Relationship_to_Associated_Account__c,
                CPAO_Status__c
            FROM CPAO_Account_to_Account_Relationship__c
            WHERE CPAO_Account_Name__c =: parentId 
        ];
    }

    public class InitWrapper {
        @AuraEnabled public List<RelatedWrapper> children;
        @AuraEnabled public List<CPAO_PicklistWrapper> relationships;
        @AuraEnabled public Account parent;
    }

    public class RelatedWrapper {
        @AuraEnabled public Lookup.ResultWrapper child = new Lookup.ResultWrapper();
        @AuraEnabled public Boolean isEdit;
        @AuraEnabled public Date effectiveDate;
        @AuraEnabled public Date endDate;
        @AuraEnabled public Id parentId;
        @AuraEnabled public Id recordId;
        @AuraEnabled public String relationship;

        public RelatedWrapper(CPAO_Account_to_Account_Relationship__c relationship) {
            this.parentId = relationship.CPAO_Account_Name__c;
            this.child.val = relationship.CPAO_Associated_Account_Name__c;
            this.child.text = relationship.CPAO_Associated_Account_Name__r.Name;
            this.effectiveDate = relationship.CPAO_Start_Date__c;
            this.endDate = relationship.CPAO_End_Date__c;
            this.relationship = relationship.CPAO_Relationship_to_Associated_Account__c;
            this.recordId = relationship.Id;
        }
    }
}