@isTest
private class CPAO_UnmarkPrimaryRelationBatchTest{
	
	public static List<Account> accounts;
	public static List<Contact> contacts;

	static void dataInitialize(Integer contactAmount){
		CPAO_TestFactory.automationControlSetup();
		accounts = CPAO_TestFactory.createAccounts(1);
		insert accounts;
		contacts = CPAO_TestFactory.createContacts(accounts[0], contactAmount);
		insert contacts;
	}

	@isTest
	static void itShould() {
		dataInitialize(1);
		CPAO_Account_Contact_Relationship__c rel = CPAO_TestFactory.createAccountContactRelationships(accounts[0], contacts[0], 1)[0];
		rel.CPAO_Primary_Employer__c = 'Yes';
		rel.CPAO_Text__c = 'TestClass Function';
		rel.CPAO_Position_Category__c = 'COO';
		rel.CPAO_Business_Unit__c = 'Internal Audit';
		rel.CPAO_Start_Date__c = Date.today().addDays(-2);
		rel.CPAO_End_Date__c = Date.today().addDays(-1);
		insert rel;

		Test.startTest();
		CPAO_UnmarkPrimaryRelationBatch b = new CPAO_UnmarkPrimaryRelationBatch();
		database.executebatch(b);
		Test.stopTest();
		rel = [SELECT Id, CPAO_Primary_Employer__c FROM CPAO_Account_Contact_Relationship__c WHERE Id = :rel.Id];
		Contact contact = [SELECT Id, CPAO_Primary_Employer__c FROM Contact WHERE Id =:contacts[0].Id];
		System.assert(rel.CPAO_Primary_Employer__c == 'No');
		System.assert(contact.CPAO_Primary_Employer__c == null);
	}

}