trigger CPAO_Task_Trigger on Task (before insert, after insert, before update, after update) {
    
   /* CPAO_Automation_Control__c control = CPAO_Automation_Control__c.getOrgDefaults();
  	
    //need automation control for task object
    if(!control.Task__c){
        return;
    }*/
    
    if(Trigger.isBefore && Trigger.isInsert){
        CPAO_TaskHandler.afterInsertFilter(Trigger.New);
  	}
  
    if(Trigger.isBefore && Trigger.isUpdate){

    }

  
    if(Trigger.isAfter && Trigger.isInsert){

    }
  
    if(Trigger.isAfter && Trigger.isUpdate){
		CPAO_TaskHandler.afterUpdateFilter(Trigger.New, Trigger.oldMap);
    }

}