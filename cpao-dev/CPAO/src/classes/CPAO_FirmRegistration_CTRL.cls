public without sharing class CPAO_FirmRegistration_CTRL {
    
    public static final String ACTIVE = 'Active';
    public static final String OPEN = 'Open';
    public static final String FMT_APPLICATION = 'CPAO_FirmRegistration_Application';
    public static User LOGGED_USER = getLoggedUserInfo();
    public static List<RecordType> FMT_APPLICATION_RECORD_TYPE= CPAO_ApplicationSelector.getFMTApplicationRecordType();
    
    @AuraEnabled
    public static User getLoggedUserInfo(){
        return CPAO_UserSelector.getCurrentUser();
    }
    
    @AuraEnabled
    public static componentInitWrapper getCurrentCOAApplication() {
        componentInitWrapper response= new componentInitWrapper();
        List<CPAO_Application__c> currentfmtApps = 
            CPAO_ApplicationSelector.getUserApplicationsByRecordTypeNameAndStatus('Open', FMT_APPLICATION);
        if(currentfmtApps.size()>0){
            response.COA_app= currentfmtApps[0];
            //return currentCoaApps[0];
        }else{
            CPAO_Application__c currentCoaApp = new CPAO_Application__c();
            currentCoaApp.CPAO_Contact__c=LOGGED_USER.ContactId;
            currentCoaApp.RecordTypeId=FMT_APPLICATION_RECORD_TYPE[0].Id;
            currentCoaApp.CPAO_Status__c='Open';
            currentCoaApp.CPAO_Current_Step__c='step1';
            insert currentCoaApp;
            response.COA_app=currentCoaApp;
            //return currentCoaApp;
            
        }
        response.LSO_Accounts=getLSOAccounts();
        return response;//JSON.serialize(response);
    }
    
    
    public static List<Account> getLSOAccounts() {
        return [SELECT Id,Name,CPAO_LSO__c FROM Account WHERE CPAO_LSO__c=:LOGGED_USER.ContactId ORDER BY Name ASC];
    }
    
    @AuraEnabled	
    public static void upsertFirmApproval(String test, String Address, String City, String Province, String Code, String Phone, String Email, String Website) {
        CPAO_Application__c app = new CPAO_Application__c();
        Contact c = new Contact();
        c.CPAO_AMD_Category__c = test;
        //c.CPAO_AMD_Tax_Rate__c = Address;
            insert c;
        //upsert FMTAppToAdd;
        //Contact c = [select id from contact where id = '0033B00000K54gv'];
        //c.CPAO_AMD_Category__c = test;
            //update c;
        app.CPAO_Employment_City__c = City;
        app.CPAO_Other_Full_Time_Employer_Address__c = Address;
        app.CPAO_Residential_Province__c = Province;
        app.CPAO_Residential_Postal_Code__c = Code;
        app.CPAO_Reference_1_Phone__c = Phone;
        app.CPAO_Reference_1_Email__c = Email;
        app.Website_URL__c = Website;
        insert app;
        //return FMTAppToAdd;
        
    }
    
    @AuraEnabled
    public static FileWrapper saveTheFile(Id parentId, String fileName, String base64Data, String contentType,String fileTitle,String idFileToReplace) { 
        
        if(idFileToReplace!=null){
            List<ContentDocument> filesToUpdate=[SELECT Id,Title,SharingOption FROM ContentDocument WHERE Id=:idFileToReplace];
            delete filesToUpdate;
        }
        
        base64Data = EncodingUtil.urlDecode(base64Data, 'UTF-8');
        
        ContentVersion a = new ContentVersion();
        a.FirstPublishLocationId = parentId;
        
        a.versionData = EncodingUtil.base64Decode(base64Data);
        a.title = fileName;
        
        a.pathOnClient = '/'+fileName;
        //  a.ContentType = contentType;
        
        insert a;
        
        FileWrapper response= new FileWrapper();
        response.fileId= [Select Id,ContentDocumentId from ContentVersion Where id =:a.id].ContentDocumentId;
        response.fileName=a.title;
        
        return response;
        
    }
    
    
    @AuraEnabled
    public static ReviewPageWrapper loadReviewInfo() {
        ReviewPageWrapper results = new ReviewPageWrapper();
        List<CPAO_Application__c> currentFMTApplication = 
            CPAO_ApplicationSelector.getUserApplicationsByRecordTypeNameAndStatus(OPEN,FMT_APPLICATION);
        if(!currentFMTApplication.isEmpty()){
            results.currentFMTApplication = currentFMTApplication[0];
            results.professionalCoorporation = CPAO_AccountSelector.getAccountToUpdate(new Set<Id>{currentFMTApplication[0].CPAO_COA_Firm_Name__c})[0];
           
        } else {
            return null;
        }
        
        return results;
    }
    
    
    
    
    public class ReviewPageWrapper{
        @AuraEnabled public CPAO_Application__c currentFMTApplication{get;set;}
        @AuraEnabled public Account professionalCoorporation{get;set;}
        
    }
    public class componentInitWrapper{
        @AuraEnabled public CPAO_Application__c COA_app{get;set;}
        @AuraEnabled public List<Account> LSO_Accounts{get;set;}    
    }
    
    public class FileWrapper{
        @AuraEnabled public Id fileId{get;set;}
        //@AuraEnabled public ContentDocumentLink file{get;set;}
        @AuraEnabled public String fileName{get;set;}
        
    }

}