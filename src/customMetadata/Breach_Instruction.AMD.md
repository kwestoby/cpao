<?xml version="1.0" encoding="UTF-8"?>
<CustomMetadata xmlns="http://soap.sforce.com/2006/04/metadata" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xmlns:xsd="http://www.w3.org/2001/XMLSchema">
    <label>AMD</label>
    <protected>false</protected>
    <values>
        <field>CPAO_Breach_Instruction__c</field>
        <value xsi:type="xsd:string">Please go to the &quot;Annual Obligations&quot; tab to pay your AMD. Once your AMD is completed, return to this reinstatement application and mark the obligation as completed.</value>
    </values>
    <values>
        <field>CPAO_Breach_Type__c</field>
        <value xsi:type="xsd:string">Annual Membership Dues (AMD)</value>
    </values>
</CustomMetadata>
