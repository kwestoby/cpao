({
	handleShowModal: function (component, event, helper) {
		var params = event.getParam("arguments");
		if(params) {
			component.set("v.title", params.title);
			component.set("v.body", params.body);
			component.set("v.showModal", true);
		}
	},
	handleHideModal: function(component, event, helper) {
		component.set("v.showModal", false);
	}
})