/**
* @File Name    :   CPAO_MilestoneContactHelper
* @Description  :   Trigger Milestone Contact  Helper
* @Date Created :   2018-07-11
* @Author       :   Michaell Reis Gasparini, Deloitte, mreisg@deloitte.com
* @group        :   Class
* @Modification Log:
**************************************************************************************
* Ver       Date        Author              Modification
* 1.0       2018-07-11  Michaell Reis       Created the file/class
**/
public class CPAO_MilestoneContactHelper {

	public static void historyTracker(Map<Id, Contact>  conObjMap){


		Map<String,String> triggerPlusCopyFieldMap = new Map<String,String>();
		List<String> triggerfields = new List<String>();
		List<String> copyfields = new List<String>();

		for(CPAO_History_Tracker_Controller__c historytrackerController : [Select Id, name,CPAO_Fields_to_Copy__c, CPAO_ObjectType__c,CPAO_Trigger_Fields__c,CPAO_Active__c from CPAO_History_Tracker_Controller__c where CPAO_ObjectType__c =: 'Contact' and CPAO_Active__c =: True]){
			triggerfields = historytrackerController.CPAO_Trigger_Fields__c.split(',');
			copyfields  = historytrackerController.CPAO_Fields_to_Copy__c.split(',');
			system.debug('triggerfields'+triggerfields );
            //triggerPlusCopyFieldMap.put(globalhistoryController.CPAO_Trigger_Fields__c,globalhistoryController.CPAO_Fields_to_Copy__c);

        }
        
        List<CPAO_MilestoneAndHistory__c> listHistory = new List<CPAO_MilestoneAndHistory__c>();


        for(Contact conObj: conObjMap.values()){

        	for(integer i=0; i<triggerfields.size(); i++){
        		string fieldAPI = triggerfields[i];

        		if(trigger.oldMap.get(conObj.Id).get('CPAO_Legal_Given__c') != null){
        			
        			String oldNameValue     = (String)trigger.oldMap.get(conObj.Id).get('CPAO_Legal_Given__c');
        			String currentNameValue = (String)conObj.CPAO_Legal_Given__c;
        			
        			if(trigger.oldMap.get(conObj.Id).get('CPAO_Legal_Surname__c') != null){

        				String oldSurnameValue  = (String)trigger.oldMap.get(conObj.Id).get('CPAO_Legal_Surname__c');
        				String currentSurnameValue = (String)conObj.CPAO_Legal_Surname__c;
        				if(!oldNameValue.equals(currentNameValue) || !oldSurnameValue.equals(currentSurnameValue) ){

        					id recordTypeId = Schema.SObjectType.CPAO_MilestoneAndHistory__c.getRecordTypeInfosByName().get('Contact Previous Name').getRecordTypeId();
        					CPAO_MilestoneAndHistory__c history = copyFieldsHistory(conObj, copyfields);
        					history.CPAO_Legal_Given__c=oldNameValue;
        					history.CPAO_Legal_Surname__c =oldSurnameValue;

        					history.RecordTypeId= recordTypeId;
        					listHistory.add(history);

        				}
        			}
        		}
        		if(trigger.oldMap.get(conObj.Id).get(fieldAPI) != null){
        			if(trigger.oldMap.get(conObj.Id).get(fieldAPI)!= conObj.get(fieldAPI)){
        				CPAO_MilestoneAndHistory__c history = copyFieldsHistory(conObj, copyfields);
        				listHistory.add(history);
        				break;
        			}
        		}
        	}           

        }
        if(listHistory.size()>0){
        	insert listHistory;
        }

    }  



    public static CPAO_MilestoneAndHistory__c copyFieldsHistory(Contact conObj, List<String> listFields){

    	id recordTypeId = Schema.SObjectType.CPAO_MilestoneAndHistory__c.getRecordTypeInfosByName().get('Contact Milestone').getRecordTypeId();
    	CPAO_MilestoneAndHistory__c history = new CPAO_MilestoneAndHistory__c();
    	history.CPAO_Contact__c = conObj.Id;
    	history.RecordTypeId= recordTypeId;
    	for(integer i=0; i<listFields.size(); i++){
    		string fieldAPI = listFields[i];
                //system.debug('fieldAPI'+fieldAPI);
                //String oldFieldValue = trigger.oldMap.get(conObj.Id)+'.'+fieldAPI;
                
                history.CPAO_Legal_Given__c=(String)conObj.CPAO_Legal_Given__c;
                history.CPAO_Legal_Surname__c = (String)conObj.CPAO_Legal_Surname__c;
                history.put(fieldAPI,trigger.oldMap.get(conObj.Id).get(fieldAPI) );


            }
            return history;
        }

    }