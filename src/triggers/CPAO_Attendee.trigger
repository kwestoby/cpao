trigger CPAO_Attendee on EventApi__Attendee__c (before insert, after insert, before update, after update) {

	CPAO_Automation_Control__c control = CPAO_Automation_Control__c.getOrgDefaults();
	if(!control.EventApi_Attendee_c__c){
		return;
	}
	
	if(Trigger.isBefore && Trigger.isInsert){
		CPAO_AttendeeHandler.beforeInsertFilter(Trigger.New);
	}
	if(Trigger.isBefore && Trigger.isUpdate){
		CPAO_AttendeeHandler.beforeUpdateFilter(Trigger.New, Trigger.oldMap);
	}
}