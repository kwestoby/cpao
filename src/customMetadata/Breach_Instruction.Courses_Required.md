<?xml version="1.0" encoding="UTF-8"?>
<CustomMetadata xmlns="http://soap.sforce.com/2006/04/metadata" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xmlns:xsd="http://www.w3.org/2001/XMLSchema">
    <label>Courses Required</label>
    <protected>false</protected>
    <values>
        <field>CPAO_Breach_Instruction__c</field>
        <value xsi:type="xsd:string">Please enter the date (s) of completion of the required courses as detailed by the Registrar (e.g. CARPD) in the &quot;Additional Notes to Reviewer&quot; and upload the certificate of completion.</value>
    </values>
    <values>
        <field>CPAO_Breach_Type__c</field>
        <value xsi:type="xsd:string">Courses Required</value>
    </values>
</CustomMetadata>
