<?xml version="1.0" encoding="UTF-8"?>
<CustomMetadata xmlns="http://soap.sforce.com/2006/04/metadata" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xmlns:xsd="http://www.w3.org/2001/XMLSchema">
    <label>Practice Inspection - Hourly Rate</label>
    <protected>false</protected>
    <values>
        <field>Member_Experience__c</field>
        <value xsi:nil="true"/>
    </values>
    <values>
        <field>PD_Department_Manager_or_higher__c</field>
        <value xsi:nil="true"/>
    </values>
    <values>
        <field>Practice_Inspection__c</field>
        <value xsi:type="xsd:string">Practice Inspection - Hourly Rate</value>
    </values>
    <values>
        <field>Registrar_s_Office_Registrar__c</field>
        <value xsi:nil="true"/>
    </values>
    <values>
        <field>Student_Services_Education__c</field>
        <value xsi:nil="true"/>
    </values>
    <values>
        <field>Varies_depends_on_which_department_it__c</field>
        <value xsi:nil="true"/>
    </values>
</CustomMetadata>
