// javascript-only alternative
//TODO Apply disabled styling for button until an option is selected
// var container = document.getElementById("modal-body");
// var app = container.getElementsByClassName("new-application-record");
// for (var i = 0; i < app.length; i++) {
//   app[i].addEventListener("click", function() {
//     var current = document.getElementsByClassName("active-application");
//     if (current) {
//       current[0].className = current[0].className.replace(" active-application", "");
//     }
//     this.className += " active-application";
//   });
// }

// jquery alternative
//TODO Apply disabled styling for button until an option is selected
$(document).ready(function() {
  $(".new-application-record").click(function () {
      $(".new-application-record").removeClass("active-application");
      $(this).addClass("active-application"); 
  });
});