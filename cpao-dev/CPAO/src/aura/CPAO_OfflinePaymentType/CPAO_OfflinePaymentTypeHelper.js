({
	showMessage : function(component, title, severity, msg) {
		component.get("v.offlinePaymentMsg");
		$A.createComponents([
			["ui:message",{
				"title": title,
				"severity": severity,
			}],
			["ui:outputText",{
				"value": msg
			}]
			],
			function(components,status,errorMessage) {
				var message = components[0];
				var outputText = components[1];
				message.set("v.body", outputText);
				component.find("offlinePaymentMsg").set("v.body", message);
			}
		);
	},
	clearMessage : function(component) {
		component.find("offlinePaymentMsg").set("v.body",[]);
	}
})