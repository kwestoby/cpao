({
	doAction : function(component, event, helper) {
		var app = component.get("v.application");

		var map = JSON.parse(app.CPAO_JSON_File_Map__c);
        if(map['certificateDocument'] != undefined){
            if(map['certificateDocument']['Name'] != undefined){
                component.set('v.fileName',map['certificateDocument']['Name']);
            }
        }
	},

	clickOnNextButton : function(component,event,helper){
    	var talkingToComponent = event.getParam("componentName");
    	if(talkingToComponent == 'step6'){
    		var app = component.get("v.application");
    		console.log('app');
    		console.log(app);
    		console.log(app.CPAO_Lost_Certificate_Declaration__c);
    		var filename = component.get('v.fileName');
    		if(filename == undefined ||  !filename.replace(/\s/g, '').length){
    			helper.methodFailure(component, event, helper, "please provide an attachment", "Error");
    		}
    		var apexAction = component.get('c.saveCertificate');
	        apexAction.setParams({
	        	"fileName" : filename,
                "base64Data" : encodeURIComponent(component.get('v.fileContents')), 
                "contentType" : component.get("v.fileType"),
		    	"currentReadmissionApp": app
		    });
	        apexAction.setCallback(this, function(response) {
	        	var state = response.getState();
	        	console.log('state');
	        	console.log(state);
	        	if(state === "SUCCESS"){
	        		var result = response.getReturnValue();
	        		console.log('result');
	        		console.log(result);
	                if(result == 'Success'){
	                	var actionToTake = event.getParam("nextStepName");
                        var applicationHasChanged = component.getEvent("applicationHasChanged");
                        component.set('v.fileContents',null);
                        component.set('v.fileType',null);
                        applicationHasChanged.setParams({
                            "nextButtonClickeable" : true,
                            "action" : actionToTake});
				        applicationHasChanged.fire();
	                } else {
                        helper.methodFailure(component, event, helper);
	                }                
	            } else {
                    helper.methodFailure(component, event, helper);
                }
	        });
	        $A.enqueueAction(apexAction);
    	}        
    },

    handleFilesChange: function (component, event,helper) {
    	console.log('file input start');
        var fileInput = event.getSource().get("v.files")[0];
        component.set('v.fileInput',fileInput);
        component.set('v.fileName',fileInput.name);
        component.set('v.SpecialConsiderationName',fileInput.name);
        component.set('v.fileType',fileInput.type);
        
        var fr = new FileReader();
        var self = this;
        
        fr.onload = function() {
            var fileContents = fr.result;
            var base64Mark = 'base64,';
            var dataStart = fileContents.indexOf(base64Mark) + base64Mark.length;
            fileContents = fileContents.substring(dataStart);
            component.set('v.fileContents',fileContents);
        };
        
        fr.readAsDataURL(fileInput);
    },
})