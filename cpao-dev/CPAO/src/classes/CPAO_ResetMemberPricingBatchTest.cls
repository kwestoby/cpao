@isTest
public class CPAO_ResetMemberPricingBatchTest {
	public static List<Account> accounts;
	public static List<Contact> contacts;


	static void dataInitialize(Integer contactAmount,String contactType){
		CPAO_TestFactory.automationControlSetup();
		accounts = CPAO_TestFactory.createAccounts(1);
		insert accounts;
		contacts = CPAO_TestFactory.createContacts(accounts[0], contactAmount);

		for (Contact contact: contacts){
			contact.CPAO_AMD_Payment_Category__c = 'Category 0100 – Member is paying full CPA Ontario and Full CPA Canada fees to CPAO';
			contact.CPAO_Resignation_Discount_Percentage__c = 20;
			contact.CPAO_AMD_Waiver_Discount_Percentage__c = 15;
			contact.CPAO_Zero_Tax__c = 'Yes';
			contact.CPAO_Employer_is_paying_AMD__c = 'Yes';
			contact.CPAO_CPA_Contact_Type__c = contactType;
		}
		insert contacts;

	}

	@isTest
	static void itShouldPass() {
		dataInitialize(1,'Member');
		Test.startTest();
		CPAO_ResetMemberPricingBatch b = new CPAO_ResetMemberPricingBatch();
		database.executebatch(b);
		Test.stopTest();
		List<Contact> contactsUpdated = [SELECT Id, CPAO_AMD_Payment_Category__c, CPAO_Resignation_Discount_Percentage__c, 
											CPAO_AMD_Waiver_Discount_Percentage__c, CPAO_Zero_Tax__c, 
											CPAO_Employer_is_paying_AMD__c 
										FROM Contact];
		for (Contact contact: contactsUpdated){
			System.assert(contact.CPAO_AMD_Payment_Category__c == null);
			System.assert(contact.CPAO_Resignation_Discount_Percentage__c == null);
			System.assert(contact.CPAO_AMD_Waiver_Discount_Percentage__c == null);
			System.assert(contact.CPAO_Zero_Tax__c == null);
			System.assert(contact.CPAO_Employer_is_paying_AMD__c == null);
		}
	
	}

	@isTest
	static void itShouldFail() {
		dataInitialize(1,'Lead');
		Test.startTest();
		CPAO_ResetMemberPricingBatch b = new CPAO_ResetMemberPricingBatch();
		database.executebatch(b);
		Test.stopTest();
		List<Contact> contactsUpdated = [SELECT Id, CPAO_AMD_Payment_Category__c, CPAO_Resignation_Discount_Percentage__c, 
											CPAO_AMD_Waiver_Discount_Percentage__c, CPAO_Zero_Tax__c, 
											CPAO_Employer_is_paying_AMD__c 
										FROM Contact];
		for (Contact contact: contactsUpdated){
			System.assert(contact.CPAO_AMD_Payment_Category__c != null);
			System.assert(contact.CPAO_Resignation_Discount_Percentage__c != null);
			System.assert(contact.CPAO_AMD_Waiver_Discount_Percentage__c != null);
			System.assert(contact.CPAO_Zero_Tax__c != null);
			System.assert(contact.CPAO_Employer_is_paying_AMD__c != null);
		}

	}

}