@isTest
public with sharing class CPAO_AMDRenewalContainerCTRLTest {
	
	public static List<Account> factoryAccounts;
	public static List<Contact> factoryContacts;
	public static User currentUser;
	public static User user;
	public static RecordType amdCPDRecordType;
    //public static UserRole r;

	public static void dataInitialize(Integer numberOfAccounts, Integer numberOfContacts){
		CPAO_TestFactory.automationControlSetup();
		factoryAccounts = CPAO_TestFactory.createAccounts(numberOfAccounts);
		insert factoryAccounts;
		factoryContacts = CPAO_TestFactory.createContacts(factoryAccounts[0],numberOfContacts);

		insert factoryContacts;
		
        
        
		user = CPAO_TestFactory.createCommunityUser(factoryContacts[0].Id);
        insert user;
        

        currentUser = CPAO_UserSelector.getCurrentUser();
        amdCPDRecordType = [SELECT Id FROM RecordType 
												WHERE Name = :CPAO_ApplicationHandler.AMD_CPD 
												AND SobjectType = 'CPAO_Application__c'][0];
    }

    @isTest
	public static void getWaiversTest() {
		dataInitialize(1,1); 
		CPAO_Application__c application = new CPAO_Application__c();
		
		application.CPAO_Contact__c = factoryContacts[0].Id;
		application.CPAO_Status__c = 'Open';
		application.RecordTypeId = amdCPDRecordType.Id;
		insert application;
		system.runAs(user) {
			CPAO_AMDRenewalContainerCTRL.getWaivers();
		}
	}

	@isTest
	public static void getWaiverExplanationsTest() {

		CPAO_AMDRenewalContainerCTRL.getWaiverExplanations();
	}

	@isTest
	public static void getCPDandPOAinfoTest() {
		dataInitialize(1,1);
		CPAO_Application__c application = new CPAO_Application__c();
		
		application.CPAO_Contact__c = factoryContacts[0].Id;
		application.CPAO_Status__c = 'Open';
		application.RecordTypeId = amdCPDRecordType.Id;
		insert application;
		system.runAs(user) {
			CPAO_AMDRenewalContainerCTRL.getCPDandPOAinfo();
		}		
	}

	@isTest
	public static void saveApplicationTest() {
		dataInitialize(1,1);
		CPAO_Application__c application = new CPAO_Application__c();
		
		application.CPAO_Contact__c = factoryContacts[0].Id;
		application.CPAO_Status__c = 'Open';
		application.RecordTypeId = amdCPDRecordType.Id;
		application.CPAO_AMD_Waiver__c = 'Parenting/Family Care';
		insert application;
		system.runAs(user) {
			CPAO_AMDRenewalContainerCTRL.saveApplication(application);
        }
		
	}

	@isTest
	public static void deletePOATest() {
		dataInitialize(1,1);
		CPAO_Application__c application = new CPAO_Application__c();
		insert application;
		Plan_of_Action_Items__c planOfActionItem = new Plan_of_Action_Items__c(
			CPAO_Application__c = application.Id,
			CPAO_Course_Name__c = 'test',
			CPOA_CPD_Hours__c = 10,
			CPAO_CPD_Provider_or_Sponsoring__c = 'test',
			CPAO_End_Date__c = Date.today(),
			CPAO_Research_Area__c = 'test',
			CPAO_Start_Date__c = Date.today(),
			CPAO_Verifiable__c = 'No');
		//insert planOfActionItem;
		system.runAs(user) {
			CPAO_AMDRenewalContainerCTRL.deletePOA(planOfActionItem.Id);
        }		
	}

	@isTest
	public static void saveAppAndPOATest1() {
		dataInitialize(1,1);
		CPAO_Application__c application = new CPAO_Application__c();
		
		application.CPAO_Contact__c = factoryContacts[0].Id;
		application.CPAO_Status__c = 'Open';
		application.RecordTypeId = amdCPDRecordType.Id;
		application.CPAO_Declaration__c = 'Comply with full CPD requirements';
		insert application;
		system.runAs(user) {
			CPAO_AMDRenewalContainerCTRL.saveAppAndPOA(application, null);
        }
		
	}

	@isTest
	public static void saveAppAndPOATest2() {
		dataInitialize(1,1);
		CPAO_Application__c application = new CPAO_Application__c();
		
		application.CPAO_Contact__c = factoryContacts[0].Id;
		application.CPAO_Status__c = 'Open';
		application.RecordTypeId = amdCPDRecordType.Id;
		application.CPAO_Declaration__c = 'I do not comply';
        application.CPAO_Number_of_Unverifiable_Hours_Short__c = 4;
		//insert application;
		Plan_of_Action_Items__c planOfActionItem = new Plan_of_Action_Items__c(
			CPAO_Application__c = application.Id,
			CPAO_Course_Name__c = 'test',
			CPOA_CPD_Hours__c = 10,
			CPAO_CPD_Provider_or_Sponsoring__c = 'test',
			CPAO_End_Date__c = Date.today(),
			CPAO_Research_Area__c = 'test',
			CPAO_Start_Date__c = Date.today(),
			CPAO_Verifiable__c = 'No');
		String posJSONString = JSON.serialize(new List<Plan_of_Action_Items__c>{planOfActionItem});
		system.runAs(user) {
			CPAO_AMDRenewalContainerCTRL.saveAppAndPOA(application, posJSONString);
        }		
	}

	//@isTest
	//public static void loadReviewInfoTest() {
	//	dataInitialize(1,1);
	//	CPAO_Application__c application = new CPAO_Application__c();
		
	//	application.CPAO_Contact__c = factoryContacts[0].Id;
	//	application.CPAO_Status__c = 'Open';
	//	application.RecordTypeId = amdCPDRecordType.Id;
	//	application.CPAO_Declaration__c = 'Comply with full CPD requirements';
	//	insert application;
	//	CPAO_Account_Contact_Relationship__c relationshipToInsert = CPAO_TestFactory.createAccountContactRelationships(
	//	factoryAccounts[0], factoryContacts[0], 1)[0];
	//	insert relationshipToInsert;
	//	system.runAs(user) {
	//		CPAO_AMDRenewalContainerCTRL.loadReviewInfo();
 //       }		
	//}

	//@isTest
	//public static void saveReviewTest() {
	//	dataInitialize(1,1);
	//	CPAO_Application__c application = new CPAO_Application__c();
		
	//	application.CPAO_Contact__c = factoryContacts[0].Id;
	//	application.CPAO_Status__c = 'Open';
	//	application.RecordTypeId = amdCPDRecordType.Id;
	//	application.CPAO_Declaration__c = 'Comply with full CPD requirements';
	//	insert application;
	//	system.runAs(user) {
	//		CPAO_AMDRenewalContainerCTRL.saveReview(application);
 //       }		
	//}

	@isTest
	public static void saveApplicationPoaComplianceTest() {
		dataInitialize(1,1);
		CPAO_Application__c application = new CPAO_Application__c();
		
		application.CPAO_Contact__c = factoryContacts[0].Id;
		application.CPAO_Status__c = 'Open';
		application.RecordTypeId = amdCPDRecordType.Id;
		application.CPAO_Declaration__c = 'Comply with full CPD requirements';
		application.CPAO_Record_Keeping_Declaration__c = true;
		application.CPAO_Compliance_Declaration__c = true;
		insert application;
		system.runAs(user) {
			CPAO_AMDRenewalContainerCTRL.saveApplicationPoaCompliance(application);
        }		
	}

	@isTest
	public static void loadPoaComplianceDeclarationTest() {
		dataInitialize(1,1);
		CPAO_Application__c application = new CPAO_Application__c();
		
		application.CPAO_Contact__c = factoryContacts[0].Id;
		application.CPAO_Status__c = 'Completed';
		application.RecordTypeId = amdCPDRecordType.Id;
		application.CPAO_Record_Keeping_Declaration__c = false;
		application.CPAO_Compliance_Declaration__c = false;
		insert application;
		Plan_of_Action_Items__c planOfActionItem = new Plan_of_Action_Items__c(
			CPAO_Application__c = application.Id,
			CPAO_Course_Name__c = 'test',
			CPOA_CPD_Hours__c = 10,
			CPAO_CPD_Provider_or_Sponsoring__c = 'test',
			CPAO_End_Date__c = Date.today(),
			CPAO_Research_Area__c = 'test',
			CPAO_Start_Date__c = Date.today(),
			CPAO_Verifiable__c = 'No');

		
		insert planOfActionItem;

		system.runAs(user) {
			CPAO_AMDRenewalContainerCTRL.loadPoaComplianceDeclaration();
        }		
	}

}