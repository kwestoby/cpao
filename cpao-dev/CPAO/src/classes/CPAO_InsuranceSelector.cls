public without sharing class CPAO_InsuranceSelector {
	
	public static List<CPAO_Insurance__c> getInsuranceByAccount(List<Account> primaryContactAccounts) {
		return [SELECT Id, Name, CPAO_Account_Name__c, CPAO_Coverage_Type__c, CPAO_Insurance_Provider__c, 
				CPAO_Insurance_Effective_Date__c, CPAO_Expiry_Date__c,CPAO_Account_Name_Of_Policy_Holder__c,
				CPAO_Insurance_Status__c, CPAO_Account_Name_of_Policy_Holder_Text__c, CPAO_Account_CPA_ID__c,
				CPAO_Account_Name_Text__c, CPAO_CPA_ID_Of_Policy_Holder__c
				FROM CPAO_Insurance__c 
				WHERE CPAO_Account_Name__c IN :primaryContactAccounts
			 	ORDER BY Name ASC];
	}
}