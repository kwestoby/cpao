({
    doInit : function(component, event, helper) {
        component.set("v.newLog", {});
        component.set("v.logs", []);
        var options = [
            {value:'yes', label:'I comply with the Annual CPD Audit requirement.'},
            {value:'no', label:'I do not comply with Annual CPD Audit requirement.'},
        ];
        component.set("v.declareOptions", options);
        // helper.getInitWrapper(component, helper);
    },

    handlePrev : function(component, event, helper) {
        helper.firePrevEvent(component);
    },

    handleNext : function(component, event, helper) {
        helper.updateAPF(component, helper, false);
    },

    handleSaveAndQuit : function(component, event, helper) {
        helper.updateAPF(component, helper, true);
    },

    onSaveLog : function(component, event, helper) {
        console.log(component.get("v.newLog"));
        helper.handleNewLogSave(component, helper);
    },
    
    mimicSaveAndNext : function(component, event, helper) {
        helper.updateAPF(component, helper, false);
    },
    
    mimicSaveAndQuit : function(component, event, helper) {
        helper.updateAPF(component, helper, true);
    },
})