public with sharing class CPAO_ReadmissionContainerCTRL {
	
	public static final String OPEN = 'Open';

	@AuraEnabled
	public static ContainerWrapper getReadmissionInfo() {
		ContainerWrapper results = new ContainerWrapper();
		results.communityURL = System.Label.Community_URL;
		results.refereeTemplateURL = System.Label.Referee_template;
		results.cpdLogTemplateURL = System.Label.CPD_log_template;

		List<CPAO_Application__c> currentReadmissionApp = 
				CPAO_ApplicationSelector.getUserApplicationsByRecordTypeNameAndStatus(OPEN, CPAO_ApplicationHandler.READMISSION_RECORDTYPENAME);
		if(!currentReadmissionApp.isEmpty()){
			results.currentReadmissionApp = currentReadmissionApp[0];
		} else {
			return null;
		}

		return results;
	}

	@AuraEnabled
	public static String saveCpd(String fileName, String base64Data, String contentType, CPAO_Application__c currentReadmissionApp) {
		Id currentAttachmentId = null;
		if(base64Data != null && contentType != null){
			attachmentWrapper fileResult = handleFileInsert(fileName, base64Data, contentType, currentReadmissionApp, 'cpdDocument');
			if(fileResult.errorMSG != null){
				return fileResult.errorMSG;
			}
			currentReadmissionApp = fileResult.app;
			currentAttachmentId = fileResult.idToDelete;
		}
			
		try {
			update currentReadmissionApp;
			if(currentAttachmentId != null){
				delete new ContentDocument(Id = currentAttachmentId);
			}
		} catch(Exception e) {
			return e.getMessage();
		}
		return 'Success';
	}

	@AuraEnabled
	public static String saveCertificate(String fileName, String base64Data, String contentType, CPAO_Application__c currentReadmissionApp) {
		Id currentAttachmentId = null;
		if(base64Data != null && contentType != null){
			attachmentWrapper fileResult = handleFileInsert(fileName, base64Data, contentType, currentReadmissionApp, 'certificateDocument');
			if(fileResult.errorMSG != null){
				return fileResult.errorMSG;
			}
			currentReadmissionApp = fileResult.app;
			currentAttachmentId = fileResult.idToDelete;
		}

		try {
			update currentReadmissionApp;
			if(currentAttachmentId != null){
				delete new ContentDocument(Id = currentAttachmentId);
			}
		} catch(Exception e) {
			return e.getMessage();
		}
		return 'Success';
	}

	@AuraEnabled
	public static String save5Year(List<String> keyList, List<String> fileNameList, 
    List<String> base64DataList, List<String> contentTypeList, CPAO_Application__c currentReadmissionApp) {
		List<Id> currentAttachmentIds = new List<Id>();
        List<contentWrapper> newWrappers = new List<contentWrapper>();
        List<ContentVersion> newFiles = new List<ContentVersion>();
        if(!keyList.isEmpty()){
            Map<String,Map<String,String>> fullMap = 
            (Map<String,Map<String,String>>)JSON.deserialize(currentReadmissionApp.CPAO_JSON_File_Map__c, Map<String,Map<String,String>>.class);
            for(Integer i=0; i<keyList.size(); i++){
                ContentVersion newFile = new ContentVersion();
                newFile.FirstPublishLocationId = currentReadmissionApp.Id;
                base64DataList[i] = EncodingUtil.urlDecode(base64DataList[i], 'UTF-8');
                newFile.versionData = EncodingUtil.base64Decode(base64DataList[i]);
                newFile.title = fileNameList[i];
                newFile.pathOnClient = '/' + fileNameList[i];
                newFiles.add(newFile);
                contentWrapper newWrapper = new contentWrapper();
                newWrapper.wrapperFile = newFile;
                newWrapper.key = keyList[i];
                newWrappers.add(newWrapper);
            }
            insert newFiles;
            Map<Id,Id> contentDocIdMap = new Map<Id,Id>();
            for(ContentVersion content:[SELECT Id, ContentDocumentId FROM ContentVersion WHERE Id IN :newFiles]){
                contentDocIdMap.put(content.Id, content.ContentDocumentId);
            }
            for(Integer i=0; i<newWrappers.size(); i++){
                if(fullMap.containsKey(newWrappers[i].key)){
                    currentAttachmentIds.add(fullMap.get(newWrappers[i].key).get('Id'));
                }
                Map<String,String> fileMap = new Map<String,String>();
                fileMap.put('Id', contentDocIdMap.get(newWrappers[i].wrapperFile.Id));
                fileMap.put('Name', newWrappers[i].wrapperFile.title);
                fullMap.put(newWrappers[i].key, fileMap);
            }
            currentReadmissionApp.CPAO_JSON_File_Map__c = JSON.serialize(fullMap);
        }
        try {
            update currentReadmissionApp;
            if(!currentAttachmentIds.isEmpty()){
                List<ContentDocument> docsToDelete = new List<ContentDocument>();
                for(Id id:currentAttachmentIds){
                    docsToDelete.add(new ContentDocument(Id = id));
                }
                delete docsToDelete;
            }
        } catch(Exception e) {
            return 'The following error occured ' + e;
        }
        return 'Success';
	}

	public static attachmentWrapper handleFileInsert(String fileName, String base64Data, String contentType, 
	CPAO_Application__c currentReadmissionApp, String key) {
		attachmentWrapper result = new attachmentWrapper();
		base64Data = EncodingUtil.urlDecode(base64Data, 'UTF-8');

		ContentVersion newFile = new ContentVersion();
        newFile.FirstPublishLocationId = currentReadmissionApp.Id;
        newFile.versionData = EncodingUtil.base64Decode(base64Data);
        newFile.title = fileName;
        newFile.pathOnClient = '/' + fileName;

        try {
			insert newFile;
		} catch(Exception e) {
			result.errorMSG = e.getMessage();
			return result;
		}
		Map<String,String> cpdMap = new Map<String,String>();
		Id newFileId = [Select Id, ContentDocumentId from ContentVersion Where id = :newFile.id].ContentDocumentId;
		cpdMap.put('Id', newFileId);
		cpdMap.put('Name', fileName);
		Map<String,Map<String,String>> fullMap = 
			(Map<String,Map<String,String>>)JSON.deserialize(currentReadmissionApp.CPAO_JSON_File_Map__c, Map<String,Map<String,String>>.class);
		if(fullMap.containsKey(key)){
			result.idToDelete = fullMap.get(key).get('Id');
		}
		fullMap.put(key, cpdMap);
		currentReadmissionApp.CPAO_JSON_File_Map__c = JSON.serialize(fullMap);
		result.app = currentReadmissionApp;
		return result;
	}

	@AuraEnabled
	public static ReviewPageWrapper loadReviewInfo() {
		ReviewPageWrapper results = new ReviewPageWrapper();
		List<CPAO_Application__c> application = CPAO_ApplicationSelector.getUserApplicationsByRecordTypeNameAndStatus(
			CPAO_AMDRenewalContainerCTRL.OPEN, CPAO_ApplicationHandler.READMISSION_RECORDTYPENAME);
		if(!application.isEmpty()){
			results.application = application[0];
		} else {
			return null;
		}
		Set<String> attachmentsNeeded = new Set<String>{'fileNamecriminalExplanation','fileNamesuspensionExplanation',
		'fileNamedisciplineExplanation','fileNameviolationExplanation','fileNameexpulsionExplanation','fileNamebankruptcyExplanation',
		'cpdDocument','certificateDocument','courseDocument','accountingBody1File','accountingBody2File','accountingBody3File',
		'accountingBody4File'};
		results.fileMap = new Map<String,String>();
		System.debug('results.application.CPAO_JSON_File_Map__c: ' + results.application.CPAO_JSON_File_Map__c);
		Map<String,Map<String,String>> fullMap = 
            (Map<String,Map<String,String>>)JSON.deserialize(results.application.CPAO_JSON_File_Map__c, Map<String,Map<String,String>>.class);
        for(String fullMapKey:fullMap.keySet()){
        	if(attachmentsNeeded.contains(fullMapKey)){
        		results.fileMap.put(fullMapKey, fullMap.get(fullMapKey).get('Name'));
        	}
        }

		User current = CPAO_UserSelector.getCurrentUser();
		results.userContact = CPAO_ContactSelector.getContactToUpdate(new Set<Id>{current.ContactId})[0];
		results.contactEmploymentRecords = CPAO_AccountContactRelationSelector.getRelationshipsByContactIdAndStatus(
			results.userContact.ID, CPAO_AMDRenewalContainerCTRL.ACTIVE);

		List<CPAO_Account_Contact_Relationship__c> primaryEmployers = CPAO_AccountContactRelationSelector.getPrimaryRelationshipsByContactId(
			new Set<Id>{results.userContact.Id});
		return results;
	}

	@AuraEnabled
	public static ReviewPageReturnWrapper saveAttestation(CPAO_Application__c currentReadmissionApp) {
		ReviewPageReturnWrapper results = new ReviewPageReturnWrapper();

		try{
			update currentReadmissionApp;
		} catch(Exception e) {
			results.status = 'Error';
			results.msg = e.getMessage();
			return results;
		}
		List<OrderApi__Sales_Order__c> existingSalesOrder = CPAO_FontevaObjectSelector.getSalesOrderWithApplication(currentReadmissionApp.Id);
		if(existingSalesOrder.isEmpty()){
			OrderApi__Sales_Order__c salesOrderToInsert = new OrderApi__Sales_Order__c(
				OrderApi__Contact__c = currentReadmissionApp.CPAO_Contact__c,
				OrderApi__Entity__c = 'Contact');
			try{
				insert salesOrderToInsert;
			} catch(Exception e) {
				results.status = 'Error';
				results.msg = e.getMessage();
				return results;
			}
			List<OrderApi__Item__c> readmissionFeeItem = CPAO_FontevaObjectSelector.getItem(new List<String>{'Readmission Fee'});

			OrderApi__Sales_Order_Line__c salesOrderLineItemToInsert = new OrderApi__Sales_Order_Line__c(
				OrderApi__Sales_Order__c = salesOrderToInsert.Id,
				OrderApi__Item__c = readmissionFeeItem[0].Id);

			try{
				insert salesOrderLineItemToInsert;
			} catch(Exception e) {
				results.status = 'Error';
				results.msg = e.getMessage();
				return results;
			}

			results.status = 'Success';
			results.msg = results.checkOutScreenURL = Label.Community_URL + '/' + Label.Checkout_Screen_Name + salesOrderToInsert.Id;
			return results;
		} else {
			results.status = 'Success';
			results.msg = results.checkOutScreenURL = Label.Community_URL + '/' + Label.Checkout_Screen_Name + existingSalesOrder[0].Id;
			return results;
		}
		
	}

	public class ReviewPageReturnWrapper{
		@AuraEnabled public String msg{get;set;}
		@AuraEnabled public String status{get;set;}
		@AuraEnabled public String checkOutScreenURL{get;set;}
	}

	public class ReviewPageWrapper{
		@AuraEnabled public CPAO_Application__c application{get;set;}
		@AuraEnabled public Contact userContact{get;set;}
		@AuraEnabled public List<CPAO_Account_Contact_Relationship__c> contactEmploymentRecords{get;set;}
		@AuraEnabled public Map<String,String> fileMap{get;set;}
	}

	public class ContainerWrapper{
		@AuraEnabled public CPAO_Application__c currentReadmissionApp{get;set;}
        @AuraEnabled public String communityURL{get;set;}
        @AuraEnabled public String refereeTemplateURL{get;set;}
        @AuraEnabled public String cpdLogTemplateURL{get;set;}
    }

    public class attachmentWrapper{
		@AuraEnabled public CPAO_Application__c app{get;set;}
        @AuraEnabled public Id idToDelete{get;set;}
        @AuraEnabled public String errorMSG{get;set;}
    }

    public class contentWrapper{
        @AuraEnabled public ContentVersion wrapperFile{get;set;}
        @AuraEnabled public String key{get;set;}
    }
}