({
	
    creationOfNewEmployer: function(component, event) {
    	var primaryEmployerIsChecked = component.find('primaryEmployer').get('v.checked');
	    var businessUnitValue =component.get("v.businessUnitSelectedValue");  
	    component.set("v.accountContactRel.CPAO_Business_Unit__c", businessUnitValue);
	    var positionCategoryValue=component.get("v.positionCategorySelectedValue");
	    component.set("v.accountContactRel.CPAO_Position_Category__c", positionCategoryValue);
	    component.set("v.accountContactRel.CPAO_Relationship_to_Account__c","Employee");
	    component.set("v.accountContactRel.CPAO_Account__c", component.get('v.selectedAccount.val'));
	    if(primaryEmployerIsChecked){
	    	component.set("v.accountContactRel.CPAO_Primary_Employer__c",'Yes');
	    }else{
	     	component.set("v.accountContactRel.CPAO_Primary_Employer__c",'No');
	    }
	 //    console.log(component.get("v.accountContactRel.CPAO_Business_Unit__c"));
		// console.log(component.get("v.accountContactRel.CPAO_Account__c"));
		// console.log(component.get("v.accountContactRel.CPAO_Contact__c"));
		// console.log(component.get("v.accountContactRel.CPAO_Position_Category__c"));
		// console.log(component.get("v.accountContactRel.CPAO_Relationship_to_Account__c"));
		// console.log(component.get("v.accountContactRel.CPAO_Start_Date__c"));
		// console.log(component.get("v.accountContactRel.CPAO_Text__c"));
		// console.log(component.get("v.accountContactRel.CPAO_Primary_Employer__c"));
	 	var action2 = component.get("c.SaveAccountContactRelationships");
	    action2.setParams({
	        "relationship": component.get("v.accountContactRel")
	    });  
	    action2.setCallback(this, function(response){
	    	var state = response.getState();
	   	    if (state === "SUCCESS") {
		       component.set("v.accountContactRel", response.getReturnValue());
		       component.set('v.severity','confirm');
		       component.set("v.confirmationMessage","The new employer have been added.");
		       var addedEmployerEvent = component.getEvent("newEmployerAdded");
		       var listOfRelationships=addedEmployerEvent.getParam("listOfRelationships");
		       // listOfRelationships.push(component.get("v.accountContactRel"));
			   //  addedEmployerEvent.setParams({"listOfRelationships" : listOfRelationships });
			   console.log('list'+listOfRelationships);
			   addedEmployerEvent.fire();
		      //$A.get('e.force:refreshView').fire();
	        }else{
	     	   component.set('v.severity','error');
	           component.set("v.confirmationMessage"," Database error"); 
	     	}
	     	// console.log('refreshListEvent befoer');
	     	// var refreshListEvent = component.getEvent("UpdateRelationshipEvent");
	     	// console.log('refreshListEvent middle');
	      //   refreshListEvent.fire();
	      //   console.log('refreshListEvent after');
	    });
        $A.enqueueAction(action2);
    }
})