public with sharing class CPAO_CPARangeDetailSelector {
	
	public static List<CPAO_CPA_Range_Details__c> getActiveRange() {
		return [SELECT Next_Range_Id__c, Range_End__c, Range_Start__c, Active__c FROM CPAO_CPA_Range_Details__c WHERE Active__c = true];
	}

	public static List<CPAO_CPA_Range_Details__c> getNextRange(String nextRangeId) {
		return [SELECT Range_End__c, Range_Start__c FROM CPAO_CPA_Range_Details__c WHERE Name = :nextRangeId];
	}
}