public class CPAO_MilestoneAccountHelper {

     public static void historyTracker(List<Account>  accObjMap){
        System.debug('Enter1');
        List<String> triggerfields = new List<String>();
        List<String> copyfields = new List<String>();
        Map<String,String> fieldMap = new Map<String,String>();
        List<CPAO_MilestoneAndHistory__c> listHistory = new List<CPAO_MilestoneAndHistory__c>();
        String recordType;
        
        for(CPAO_History_Tracker_Controller__c historytrackerController : [Select Id, name,CPAO_Fields_to_Copy__c,CPAO_Fields_to_Map__c,Record_Type__c, CPAO_ObjectType__c,CPAO_Trigger_Fields__c,CPAO_Active__c from CPAO_History_Tracker_Controller__c where CPAO_ObjectType__c = 'Account' and CPAO_Active__c = True]){
            triggerfields = new List<String>();
            copyfields  = new List<String>();
            fieldMap = new Map<String,String>();
            recordType = '';         
            triggerfields = historytrackerController.CPAO_Trigger_Fields__c.split(',');
            if(historytrackerController.CPAO_Fields_to_Copy__c != null){
            	copyfields  = historytrackerController.CPAO_Fields_to_Copy__c.split(',');
            }
            if(historytrackerController.CPAO_Fields_to_Map__c != null){
                fieldMap = textToMap(';',',',historytrackerController.CPAO_Fields_to_Map__c);
            }
            recordType = historytrackerController.Record_Type__c;
            fieldMap = mixListAndMap(fieldMap,copyfields);
            for(Account accObj: accObjMap){
                Boolean isTriggerFieldsChanged = isTriggerFieldChanged(triggerfields, accObj);
                if(isTriggerFieldsChanged){
                    CPAO_MilestoneAndHistory__c history = copyFieldsHistory(accObj, fieldMap, recordType);
                    listHistory.add(history);
                }    
            }    
        }
        if(listHistory.size()>0){
            System.debug('Enter4');
            insert listHistory;
        }   
    }
    
    
    public static CPAO_MilestoneAndHistory__c copyFieldsHistory(Account accObj, Map<String,String> mapFieldstoCopy,String recordType){
        
        id recordTypeId = Schema.SObjectType.CPAO_MilestoneAndHistory__c.getRecordTypeInfosByName().get(recordType).getRecordTypeId();
        CPAO_MilestoneAndHistory__c history = new CPAO_MilestoneAndHistory__c();
        history.CPAO_Account__c = accObj.Id;
        history.RecordTypeId= recordTypeId;    
        for(String fieldAPI: mapFieldstoCopy.keySet()){
            history.put(mapFieldstoCopy.get(fieldAPI),trigger.oldMap.get(accObj.Id).get(fieldAPI) );
        }
        return history;
    }
    
    public static Map<String,String> textToMap(String separatorRecord, String separatorValue, String text){
        Map<String,String> fieldMap = new Map<String,String>();
        List<String> listValues = new List<String>();
        listValues = text.split(';');     
        for(String value: listValues){
            fieldMap.put(value.split(',')[0],value.split(',')[1]);               
        }
        return fieldMap;
    }
    
    public static Map<String,String> mixListAndMap(Map<String,String> mapfields, List<String> listFields ){
        for(String field: listFields)
        {
            if(!mapfields.containsKey(field)){
                mapFields.put(field,field);
            }
        }  
        return  mapfields;  
    }
    
    
    public static Boolean isTriggerFieldChanged(List<String> triggerfields,Account accObj){
        Boolean isChanged = false;
        for(String fieldAPI: triggerfields){
            if(trigger.oldMap.get(accObj.Id).get(fieldAPI)!=null){
                if( trigger.oldMap.get(accObj.Id).get(fieldAPI) != accObj.get(fieldAPI) ){
                    isChanged =  isChanged || true; 
                }
            }     
        }
        return isChanged;
    } 
    
}