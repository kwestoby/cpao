@isTest
public class CPAO_PoaComplianceBatchTest {
    public static List<contact>contacts;
    public static List<CPAO_Application__c>applications;
    public static List<CPAO_Licence__c>licences;


    static void dataInitialize(String licenseStatus){

        List<Account>accounts = CPAO_TestFactory.createAccounts(1);
        insert accounts; 
        contacts = CPAO_TestFactory.createContacts(accounts[0],1);
        insert contacts;
        applications=CPAO_TestFactory.createPALApplication(1,contacts[0]);
        insert(applications);
        if(licenseStatus=='Active'){
            licences=CPAO_TestFactory.createActiveLicences(1,contacts[0], applications[0]);
            insert(licences);   
        }else if(licenseStatus=='Inactive'){
            licences=CPAO_TestFactory.createInactiveLicences(1,contacts[0], applications[0]);
            insert(licences);   
            
        }
    }

    
    @isTest static void changeStatusToRevoked() {
        dataInitialize('Inactive');
        Test.startTest();
        CPAO_Update_Licence_Status_Batch batchJob = new CPAO_Update_Licence_Status_Batch();
        database.executebatch(batchJob);
        Test.stopTest();
        List<CPAO_Licence__c> result =   [SELECT Id,CPAO_Expiry_Date__c,CPAO_Issued_to__c,CPAO_Status__c,CPAO_Sub_Status__c FROM CPAO_Licence__c];

        for ( Integer i=0; i<result.size();i++ ){
            System.assert(result[i].CPAO_Issued_to__c == contacts[i].Id );
            System.assert(result[i].CPAO_Status__c== 'Revoked');
            System.assert(result[i].CPAO_Sub_Status__c== 'Licence Expiration');

        }
    
    }

    @isTest static void noChangeOnStatus() {
        dataInitialize('Active');
        Test.startTest();
        CPAO_Update_Licence_Status_Batch batchJob = new CPAO_Update_Licence_Status_Batch();
        database.executebatch(batchJob);
        Test.stopTest();
        List<CPAO_Licence__c> result =   [SELECT Id,CPAO_Expiry_Date__c,CPAO_Issued_to__c,CPAO_Status__c,CPAO_Sub_Status__c FROM CPAO_Licence__c];
            for ( Integer i=0; i<result.size();i++ ){
            System.assert(result[i].CPAO_Issued_to__c == contacts[i].Id );
            System.assert(result[i].CPAO_Status__c== 'Active');
            System.assert(result[i].CPAO_Sub_Status__c== 'Original');
        }
    }
}