({
    // @Author : Halima Dosso
    // This function runs once the component CPAO_Employment_Info is fully loaded on the page 
    // It calls an apex method @getRelationships from class @CPAO_EmployerInfoController
    // It sets two component attributes based on method @getRelationships returned value:
    //  - listOfRelationships: list of all CPAO_Account_Contact_Relationship__c records associated
    //    to end user's contact record
    //  - showRetireUnemployButton: boolean indicating if the user should see button "Confirm Retirement"
	doInit: function(component, event, helper) {
        
    	var action = component.get("c.getRelationships");
       	action.setCallback(this, function(response){
            var state = response.getState();
            if (state === "SUCCESS") {
                console.log(response.getReturnValue());
                component.set("v.listOfRelationships", response.getReturnValue().relationships);
                component.set("v.employmentStatuses", response.getReturnValue().employmentStatuses);
                component.set("v.contact", response.getReturnValue().contact);
                if(response.getReturnValue().canBeRetiredUnemployed == true){
                    component.set('v.showRetireUnemployButton',true);
                }

                helper.orderListOfEmployer(component, event, helper);
            }
        });

        $A.enqueueAction(action);
	},


   

    // @Author: Halima Dosso
    // This function runs once the end user clicks on employer record in order to modify it
    // This function sets @currentRelShowForm to true so the user could see fill up the form
    // corresponding to the CPAO_Account_Contact_Relationship__c the user clicked on.
    showOrHideCurrentEmployerForm : function(component, event, helper) {
        component.set('v.currentRelShowForm','true');
        var positionInlist=event.getSource().get("v.value");
        var relationAtPosition= component.get('v.listOfRelationships')[positionInlist];
        if(relationAtPosition.CPAO_Primary_Employer__c=='Yes'){
            component.set('v.isAlreadyPrimaryEmployer',true);
        }else{
            component.set('v.isAlreadyPrimaryEmployer',false);
        }

        component.set('v.employerPositionInList',relationAtPosition);
    },

    // @Author: Halima Dosso
    // This function runs once the end user clicks on button "Add new Employer"
    // This function sets @newEmployerShowForm to true so the user could see fill up the form
    // corresponding to the new CPAO_Account_Contact_Relationship__c he wish to add.
    showOrHideNewEmployerForm : function(component, event, helper) {
    component.set('v.newEmployerShowForm','true'); 
    },

    // @Author: Halima Dosso
    // This function runs once the end user choose from a list of radio buttons in the main page.
    // Depending on the radio button choice, the function assign them a value.
    // Those values will be further used to show the appropriate content to the user
    radioButtonChange : function(component, event, helper) {
        var changedValue = event.getParam("value");
        if(changedValue=='Retired'){     
            component.set('v.radioValue','Retired');        
        } else if(changedValue=='Current Employer information has changed'){
            component.set('v.radioValue','CurrentEmployer');
        } else if(changedValue=='Unemployed'){
            component.set('v.radioValue','Unemployed');
        } else {
            component.set('v.radioValue','Blank');
        }
    },

     confirmRetirement : function(component, event, helper) {
    helper.confirmRetirementUnemployment(component, event, helper, 'Retired');
    },

    confirmUnemployment : function(component, event, helper) {
    helper.confirmRetirementUnemployment(component, event, helper, 'Unemployed');
    },

    /*test: function(component, event, helper) {
    console.log('fired in main');
    //location.reload();
    component.set('v.body','CPAO_Employment_Info'); 
    },*/

    updatedRelationshipRecord : function(component, event, helper) {
        console.log('relationship: ');
        console.log(event.getParam("msg"));
        console.log(event.getParam("type"));
        console.log(component.get('v.confirmationMessage'));
        console.log(component.get('v.severity'));

        
        var action = component.get("c.getRelationships");
        action.setCallback(this, function(response){
            var state = response.getState();
            if (state === "SUCCESS") {
                component.set("v.listOfRelationships", response.getReturnValue().relationships);
                 //console.log(JSON.stringify(response.getReturnValue().relationships));

                helper.orderListOfEmployer(component,event,helper);
                component.set('v.newEmployerShowForm','false');
                if(response.getReturnValue().canBeRetiredUnemployed  == true){
                    component.set('v.showRetireUnemployButton',true);
                }
            }
        });
        $A.enqueueAction(action);
        if(event.getParam("relationship") != null && event.getParam("relationship") != undefined){
            var deserializedRelationship = event.getParam("relationship");
            console.log('about to set the');
            component.set('v.employerPositionInList',deserializedRelationship);
            console.log('afetr being set');
        }
        if(event.getParam("msg") != null && event.getParam("msg") != undefined && 
                event.getParam("type") != undefined && event.getParam("type") != 'noMSG'){

            component.set("v.severity", event.getParam("type"));
            component.set("v.confirmationMessage", event.getParam("msg"));
        }

    },

    // @Author: Halima Dosso
    // This function is used once the CPAO_Employment_Info is used as part of a join process
    // It runs once the user clicks on the next button to get to the next step of the process
    // In this case, it redirects the user to the infraction wireframe screen
    nextButton:function(component, event, helper) {
        window.location.replace($A.get("$Label.namespace.CPAO_InfractionWireframeControllerReference") + '/CPAO_InfractionWireframe');
    },

    changeStatus : function(component, event, helper) {
        var status = event.getSource().get("v.value");
        console.log(status);
        if(status == 'Unemployed' || status == 'Retired'){
            component.set("v.endDateModalOpen", true);
            component.set('v.showSaveButtonEmployed',false);
        }else if (status=='Employed'){
           component.set('v.showSaveButtonEmployed',true);
        }
    },

    closeModel: function(component, event, helper) {
      // for Hide/Close Model,set the "endDateModalOpen" attribute to "Fasle"  
      component.set("v.endDateModalOpen", false);
    },

    confirmStatusChange : function(component, event, helper) {
        var contact = component.get("v.contact");
        helper.confirmRetirementUnemployment(component, event, helper, contact.CPAO_Employment_Status__c);
    },

    //This method is used only when this component is used as part of an application flow.
    //It allows to trigger validation on the contact employment status field 
    clickOnNextButton: function(component, event, helper) {
        var componentNameInEvent=event.getParam("componentName");
        if(componentNameInEvent==component.get('v.nameOfThisComponent')){
            //                var validForm= component.find('PALForm').reduce(function (validSoFar, inputCmp) {

            var contactEmploymentStatus = component.get("v.contact.CPAO_Employment_Status__c");
            var isPartOfApplicationFlow=component.get('v.isPartOfApplicationFlow');
            if(contactEmploymentStatus!=undefined && isPartOfApplicationFlow){
                var applicationHasChanged = component.getEvent("applicationHasChanged");
                applicationHasChanged.setParams({
                "nextButtonClickeable": "true"
                });
                applicationHasChanged.fire();

            }else if(contactEmploymentStatus==undefined){
                component.set('v.showStatusErrorMsg',true);
            }
        }
    },
    updateContactInformation:function(component, event, helper) {
        //alert('upating contact');
        //console.log('about to update'+JSON.stringify(component.get('v.contact')));
        //console.log('logging'+component.get('v.contact'));
        var contactJson=JSON.stringify(component.get('v.contact'));
        var contact=component.get('v.contact');
       var action = component.get("c.updateContact");
       action.setParams({
            "contactToUpdate": contact
            }); 
        action.setCallback(this, function(response){

            var state = response.getState();
            console.log('state'+state);
            if (state === "SUCCESS") {
                console.log('success updating contact employment status');
               // console.log('response from server'+JSONresponse.getReturnValue());
               component.set('v.showSaveButtonEmployed',false);

            }else{      

                console.log('there was an error updating contact employment status');
                var errors=response.getError();
                var message = errors[0];
                console.log(message);
            }
        });
        $A.enqueueAction(action);
    }
  

})