@isTest
public class CPAO_AutoRenewPALSubsBatchTest {

	public static String member='Member';
	public static String memberActive='Member Active';
	public static List<contact>contacts;
	public static List<CPAO_Application__c>applications;
	public static List<CPAO_Licence__c>licences;
	public static string PAL_SUBSC_TYPE_NAME='Public Accounting Licence';
	public static String PAL_RENEWAL_RECORD_TYPE_NAME='CPAO_PAL_Renewal';
	public static List<RecordType>  PAL_RENEWAL_RECORD_TYPE= CPAO_ApplicationSelector.getRecordType(PAL_RENEWAL_RECORD_TYPE_NAME);
	public static String OPEN='Open';

    
	 @isTest(SeeAllData=true) static void testWithAllDataAccess() {

		OrderApi__Subscription_Plan__c pal_subscription_plan = new OrderApi__Subscription_Plan__c();
 		List<OrderApi__Subscription_Plan__c> pal_subscription_plans = CPAO_FontevaObjectSelector.getSubscriptionPlan(PAL_SUBSC_TYPE_NAME); 
 		if(!pal_subscription_plans.isEmpty()){
 			pal_subscription_plan=pal_subscription_plans[0];
 		}
		Contact c = [SELECT Id, Name, CPAO_CPA_Contact_Type__c, CPAO_CPA_Status__c from Contact WHERE Name = 'Halima Test' LIMIT 1];
		Test.startTest();
		CPAO_AutoRenewPALSubsBatch batchJob = new CPAO_AutoRenewPALSubsBatch();
		database.executebatch(batchJob);
		Test.stopTest();
		OrderApi__Subscription__c testSubscription= [SELECT Id,OrderApi__Contact__c,OrderApi__Subscription_Plan__c 
													FROM OrderApi__Subscription__c 
													WHERE OrderApi__Subscription_Plan__c=:pal_subscription_plan.Id AND OrderApi__Contact__c =:c.Id];
		List<CPAO_Application__c> result =  [SELECT Id,CPAO_Contact__c,CPAO_Status__c,RecordTypeId FROM CPAO_Application__c];

		for ( Integer i=0; i<result.size();i++ ){
			/*System.assert(result[i].CPAO_Contact__c == testSubscription.OrderApi__Contact__c );
			System.assert(result[i].CPAO_Status__c== OPEN);
			System.assert(result[i].RecordTypeId== PAL_RENEWAL_RECORD_TYPE[0].Id);*/
			System.debug(result[i].Name);

		}
	
	}

	@isTest static void testEmptySubscriptions() {

		//List<OrderApi__Subscription__c> testSubscriptions = new List<OrderApi__Subscription__c>();
		Test.startTest();
		CPAO_AutoRenewPALSubsBatch batchJob = new CPAO_AutoRenewPALSubsBatch();
		database.executebatch(batchJob);
		Test.stopTest();
	}

	@isTest static void testEligibleForRenewal() {

		OrderApi__Subscription__c testSubscription = new OrderApi__Subscription__c();
		CPAO_AutoRenewPALSubsBatch.eligibleForRenewal(testSubscription);
	}
}