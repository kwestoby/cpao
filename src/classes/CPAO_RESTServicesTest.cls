/**
* @File Name    :   CPAO_RESTServicesTest
* @Description  :   Rest Services Test
* @Date Created :   2018-07-12
* @Author       :   Michaell Reis Gasparini, Deloitte, mreisg@deloitte.com
* @group        :   Class
* @Modification Log:
**************************************************************************************
* Ver       Date        Author              Modification
* 1.0       2018-07-11  Michaell Reis       Created the file/class
**/
@isTest
private class CPAO_RESTServicesTest {
	
	@testSetup
	static void dataInit(){
		CPAO_TestFactory.automationControlSetup();
		
		List<Account> accounts = CPAO_TestFactory.createAccounts(1);
		
		Database.insert(accounts);

	}

	@isTest static void test_method_one() {
		RestRequest request   = new RestRequest();
		RestResponse response = new RestResponse();
		Account account =[Select id, CPAO_Account_Status__c FROM Account LIMIT 1];
		request.requestURI='/services/apexrest/directories/'+account.Id;

		RestContext.request = request;
		RestContext.response = response;
		Test.startTest();
		List<Account> accounts =CPAO_RESTServices.getAccountById();
		Test.stopTest();
		System.assert(accounts.size()>0);
	}
	
	@isTest static void test_method_two() {
		// Implement test code
	}
	
}