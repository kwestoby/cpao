({
	init : function(cmp, ev) {
		var action = cmp.get("c.getAccountNames");
 
        action.setCallback(this, function(response) {
            var state = response.getState();
            if (state === "SUCCESS") {
				var accs=response.getReturnValue()
				var wrappers=new Array();
                for (var idx=0; idx<accs.length; idx++) {
					var wrapper = { 'acc' : accs[idx], 
									'selected' : false
									};
					wrappers.push(wrapper);
                }
                cmp.set('v.wrappers', wrappers);
            }
            else if (state === "ERROR") {
                alert('Error : ' + JSON.stringify(errors));
            }
        });
        $A.enqueueAction(action);
	},
	selectPaymentType : function(cmp, ev) {
	   
		cmp.set('v.clickedSelectPaymentType',true);
		var action = cmp.get("c.queryAllPaymentTypes");
        action.setCallback(this, function(response){
        cmp.set("v.allPaymentTypes", response.getReturnValue());
        });
        $A.enqueueAction(action); 
	},
	 
	selectedCheckedAccount : function(cmp, ev) {
        var listOfAccounts=[];
		var oldWrapper = cmp.get('v.wrappers');
        
		for (var idx=0; idx<oldWrapper.length; idx++) {
            if(oldWrapper[idx].selected){
				cmp.set('v.showPaymentTypeButton',true);
                 listOfAccounts.push(oldWrapper[idx]);
            } 
       }
       cmp.set('v.selectedAccounts',listOfAccounts);

	}
})