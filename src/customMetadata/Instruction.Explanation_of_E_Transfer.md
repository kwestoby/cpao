<?xml version="1.0" encoding="UTF-8"?>
<CustomMetadata xmlns="http://soap.sforce.com/2006/04/metadata" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xmlns:xsd="http://www.w3.org/2001/XMLSchema">
    <label>Explanation of E-Transfer</label>
    <protected>false</protected>
    <values>
        <field>CPAO_Description__c</field>
        <value xsi:type="xsd:string">An email money transfer that allows users to transfer funds between personal accounts, using email and their online banking services. CPA Ontario does not accept E-Transfers.</value>
    </values>
    <values>
        <field>CPAO_Order__c</field>
        <value xsi:type="xsd:double">5.0</value>
    </values>
    <values>
        <field>CPAO_Type__c</field>
        <value xsi:type="xsd:string">Offline Payment</value>
    </values>
</CustomMetadata>
