({
	closeInfo:function(component,event,helper){    
		var cmpTarget = component.find('Modalbox');
		var cmpBack = component.find('Modalbackdrop');
		$A.util.removeClass(cmpBack,'slds-backdrop--open');
		$A.util.removeClass(cmpTarget, 'slds-fade-in-open');
        component.set('v.instructionRead',true); // InstructionRead set to True after reading the instruction
    },
	openInfo:function(component,event,helper) {
		var cmpTarget = component.find('Modalbox');
		var cmpBack = component.find('Modalbackdrop');
		$A.util.addClass(cmpTarget, 'slds-fade-in-open');
		$A.util.addClass(cmpBack, 'slds-backdrop--open'); 
	},
	
	showinstructionScreen: function(component,event,helper){
		helper.showinstructionScreen(component, event);
	},
    
    getContacts: function(component,event,helper){
		helper.getContacts(component, event);
	},
    
    refreshContacts : function(component, event, helper) {
    	helper.refreshContacts(component, event);
    },
    checkAllCheckboxes : function(component, event, helper) {
    	helper.checkAllCheckboxes(component,event);
    },
    
    saveMarkedContactsaAndOpenPayment : function(component, event, helper) {
		helper.saveMarkedContactsaAndOpenPayment(component, event);
	}
})