public without sharing class CPAO_GoodCharacterDeclarationCTRL {
	

	@AuraEnabled
    public static String saveApplication(List<String> keyList, List<String> fileNameList, 
    List<String> base64DataList, List<String> contentTypeList, CPAO_Application__c application) {
        List<Id> currentAttachmentIds = new List<Id>();
        List<contentWrapper> newWrappers = new List<contentWrapper>();
        List<ContentVersion> newFiles = new List<ContentVersion>();
        if(!keyList.isEmpty()){
            Map<String,Map<String,String>> fullMap = 
            (Map<String,Map<String,String>>)JSON.deserialize(application.CPAO_JSON_File_Map__c, Map<String,Map<String,String>>.class);
            for(Integer i=0; i<keyList.size(); i++){
                ContentVersion newFile = new ContentVersion();
                newFile.FirstPublishLocationId = application.Id;
                base64DataList[i] = EncodingUtil.urlDecode(base64DataList[i], 'UTF-8');
                newFile.versionData = EncodingUtil.base64Decode(base64DataList[i]);
                newFile.title = fileNameList[i];
                newFile.pathOnClient = '/' + fileNameList[i];
                newFiles.add(newFile);
                contentWrapper newWrapper = new contentWrapper();
                newWrapper.wrapperFile = newFile;
                newWrapper.key = keyList[i];
                newWrappers.add(newWrapper);
            }
            insert newFiles;
            Map<Id,Id> contentDocIdMap = new Map<Id,Id>();
            for(ContentVersion content:[SELECT Id, ContentDocumentId FROM ContentVersion WHERE Id IN :newFiles]){
                contentDocIdMap.put(content.Id, content.ContentDocumentId);
            }
            for(Integer i=0; i<newWrappers.size(); i++){
                if(fullMap.containsKey(newWrappers[i].key)){
                    currentAttachmentIds.add(fullMap.get(newWrappers[i].key).get('Id'));
                }
                Map<String,String> fileMap = new Map<String,String>();
                fileMap.put('Id', contentDocIdMap.get(newWrappers[i].wrapperFile.Id));
                fileMap.put('Name', newWrappers[i].wrapperFile.title);
                fullMap.put(newWrappers[i].key, fileMap);
            }
            application.CPAO_JSON_File_Map__c = JSON.serialize(fullMap);
        }
        try {
            update application;
            if(!currentAttachmentIds.isEmpty()){
                List<ContentDocument> docsToDelete = new List<ContentDocument>();
                for(Id id:currentAttachmentIds){
                    docsToDelete.add(new ContentDocument(Id = id));
                }
                delete docsToDelete;
            }
        } catch(Exception e) {
            return 'The following error occured ' + e;
        }
        return 'Success';
    }

    public class contentWrapper{
        @AuraEnabled public ContentVersion wrapperFile{get;set;}
        @AuraEnabled public String key{get;set;}
    }
}