global class CPAO_ResetMemberPricingBatch implements Database.Batchable<sObject> {
	
	global Database.QueryLocator start(Database.BatchableContext BC) {
		String query='SELECT Id FROM Contact WHERE CPAO_CPA_Contact_Type__c = \'Member\'';
        return Database.getQueryLocator(query);
	}

   	global void execute(Database.BatchableContext BC, List<Contact> contacts) {
		for(Contact contact:contacts){
			contact.CPAO_AMD_Payment_Category__c = null;
			contact.CPAO_Resignation_Discount_Percentage__c = null;
			contact.CPAO_AMD_Waiver_Discount_Percentage__c = null;
			contact.CPAO_Zero_Tax__c = null;
			contact.CPAO_Employer_is_paying_AMD__c = null;
		}
		Database.SaveResult[] srList = Database.update(contacts, false);
		//update contacts;
	}	
	global void finish(Database.BatchableContext BC) {}	
}