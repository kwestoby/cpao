@isTest
public class CPAO_TaskHandlerTest {
	@TestSetup
	private static void setupData() {
		Account acc = CPAO_TestFactory.createAccounts(1) [0];
		insert acc;
		Contact con = CPAO_TestFactory.createContacts(acc, 1) [0];
		insert con;
		CPAO_Application__c app = CPAO_TestFactory.createReadmissionApplication(1, con) [0];
		insert app;
	}

	@isTest
	private static void testAfterInsert() {
		CPAO_Application__c app = [SELECT Id, OwnerId FROM CPAO_Application__c LIMIT 1];
		Test.startTest();
		Task t = new Task(Subject = 'Test subject', Type = 'Other', WhatId = app.Id);
		insert t;
		Test.stopTest();
	}

	@isTest
	private static void testAfterUpdate() {
		CPAO_Application__c app = [SELECT Id, CPAO_Payment_Status__c FROM CPAO_Application__c LIMIT 1];
		Task task = new Task(WhatId = app.Id, Subject = 'Test subject', Type = 'Other', Status='Pending');
		insert task;
		task.Status = 'Completed';
		update task;
		app = [SELECT Id, CPAO_Payment_Status__c FROM CPAO_Application__c WHERE Id=:app.Id];
		System.assertEquals('Invoice Created', app.CPAO_Payment_Status__c);
	}
}