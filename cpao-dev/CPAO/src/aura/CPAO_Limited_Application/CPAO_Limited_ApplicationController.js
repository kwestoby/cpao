({
	doInit : function(component, event, helper) {
        var start = component.get("v.contactStatus");
        
        if(start == 'Suspended'){
            component.set("v.options",[{'label': 'Reinstatement', 'value': 'Reinstatement'}]);
        }else if(start == 'Revoked'){
            component.set("v.options",[{'label': 'Readmission', 'value': 'Readmission'}]);
        }
		
	},
    radioButtonChange: function (component, event){
        
            component.set('v.radioValue',event.getParam("value"));
        
    },
    startApplication: function(component, event){
        
        console.log("hi");
        var appType = component.get('v.radioValue');
        
        if(appType == 'Reinstatement'){
            var action = component.get("c.createReinstatementApplication");
            action.setCallback(this, function(response){
               	var State = response.getState();
                console.log(State);
                //console.log(response.getReturnValue().error);
                //console.log(response.getReturnValue().url);
                if(State == 'SUCCESS'){
                    if(response.getReturnValue().error != null){
                        component.set('v.error',response.getReturnValue().error);
                        
                    }else if(response.getReturnValue().url != null){
                        location.replace(response.getReturnValue().url);
                        console.log(response.getReturnValue().url);
                    }
                }
            });
            $A.enqueueAction(action);
        }
        
    }
})