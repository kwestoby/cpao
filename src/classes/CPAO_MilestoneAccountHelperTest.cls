/**
* @File Name    :   CPAO_MilestoneAccountHelperTest
* @Description  :   Trigger Milestone Account Test
* @Date Created :   2018-07-11
* @Author       :   Michaell Reis Gasparini, Deloitte, mreisg@deloitte.com
* @group        :   Class
* @Modification Log:
**************************************************************************************
* Ver       Date        Author              Modification
* 1.0       2018-07-11  Michaell Reis       Created the file/class
**/

@isTest
private class CPAO_MilestoneAccountHelperTest {

	@testSetup
	static void dataInit(){

		CPAO_History_Tracker_Controller__c historytrackerController = new CPAO_History_Tracker_Controller__c();
		historytrackerController.CPAO_Fields_to_Copy__c ='CPAO_Account_Status__c,CPAO_Account_Substatus__c';
		historytrackerController.CPAO_Trigger_Fields__c ='CPAO_Account_Status__c,CPAO_Account_Substatus__c';
		historytrackerController.CPAO_ObjectType__c ='Account';
		historytrackerController.CPAO_Active__c=true;

		Database.insert(historytrackerController);
		

		CPAO_TestFactory.automationControlSetup();
		
		List<Account> accounts = CPAO_TestFactory.createAccounts(1);
		accounts[0].CPAO_Account_Status__c = 'Active';
		accounts[0].CPAO_Account_Substatus__c = 'Registered';
		
		Database.insert(accounts);

	}
	
	@isTest static void testChangeFields() {
		
		id recordTypeId = Schema.SObjectType.CPAO_MilestoneAndHistory__c.getRecordTypeInfosByName().get('Account Milestone').getRecordTypeId();
		
		Account account =[Select id, CPAO_Account_Status__c FROM Account LIMIT 1];
		
		account.CPAO_Account_Status__c='Inactive';
		account.CPAO_Account_Substatus__c='Closed';
		
		Database.update(account);
		

		CPAO_MilestoneAndHistory__c history =[Select id, CPAO_Account__c,CPAO_Account_Status__c,CPAO_Account_Substatus__c, RecordTypeId FROM CPAO_MilestoneAndHistory__c LIMIT 1]; 
		
		System.assertEquals(account.Id,history.CPAO_Account__c);
		System.assertEquals(recordTypeId ,history.RecordTypeId);
		System.assertEquals('Active',history.CPAO_Account_Status__c );
		System.assertEquals('Registered',history.CPAO_Account_Substatus__c );
		
	}
	

	@isTest static void testChangeName() {
		
		id recordTypeId = Schema.SObjectType.CPAO_MilestoneAndHistory__c.getRecordTypeInfosByName().get('Account Name').getRecordTypeId();
		
		Account account =[Select id,Name, CPAO_Account_Status__c FROM Account LIMIT 1];
		String oldName= account.name;
		account.Name='Changed';
		Database.update(account);
		

		CPAO_MilestoneAndHistory__c history =[Select id, CPAO_Account__c,CPAO_Account_Firm_Name__c,CPAO_Account_Status__c,CPAO_Account_Substatus__c, RecordTypeId FROM CPAO_MilestoneAndHistory__c LIMIT 1]; 
		
		System.assertEquals(account.Id,history.CPAO_Account__c);
		System.assertEquals(recordTypeId ,history.RecordTypeId);
		System.assertEquals(oldName,history.CPAO_Account_Firm_Name__c );

	}
	

	}