public class CPAO_ReceiptTriggerHandler {

	public static void beforeUpdateFilter(List<OrderApi__Receipt__c> newReceipts, Map<Id, OrderApi__Receipt__c> oldReceiptMap) {
		List<OrderApi__Receipt__c> refundsForApproval = new List<OrderApi__Receipt__c> ();
		for (OrderApi__Receipt__c receipt : newReceipts) {
			if (receipt.CPAO_First_Approval__c == 'Yes' && receipt.CPAO_First_Approval__c != oldReceiptMap.get(receipt.Id).CPAO_First_Approval__c) {
				receipt.CPAO_Actual_Initial_Approver__c = UserInfo.getUserId();
			}
			if (!receipt.CPAO_Is_Recall__c && receipt.OrderApi__Type__c == 'Refund' && String.isBlank(receipt.CPAO_Initial_Approving_Queue_Name__c)) {
				refundsForApproval.add(receipt);
			}
			if (receipt.CPAO_Is_Recall__c) {
				receipt.CPAO_Initial_Approving_Queue_Name__c = null;
				receipt.CPAO_Actual_Initial_Approver__c = null;
				receipt.CPAO_Is_Recall__c = false;
			}
		}
		if (refundsForApproval.size() > 0) {
			CPAO_ReceiptService.sendToRefundQueueAndApproval(refundsForApproval);
		}
	}
}