public class CPAO_QueueUtils {
	public static final String REGISTRAR_QUEUE = 'CPAO Registrar Office (Registrar)';
	public static final String STUDENT_SERVICE_QUEUE = 'CPAO Student Services (Education)';
	public static final String VARING_QUEUE = 'CPAO Varies - depends on which depart';
	public static final String PD_DEPT_QUEUE = 'CPAO PD Department';
	public static final String PRACTICE_INSPECTION_QUEUE = 'CPAO Practice Inspection';

	public static Map<String, Group> queueMap {
		get {
			if (queueMap == null) {
				queueMap = new Map<String, Group> ();
				for (Group queue :[SELECT Id, Name FROM Group WHERE Type = 'Queue']) {
					queueMap.put(queue.Name, queue);
				}
			}
			return queueMap;
		}
		set;
	}
}