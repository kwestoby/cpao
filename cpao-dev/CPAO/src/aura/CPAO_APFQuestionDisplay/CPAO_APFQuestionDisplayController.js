({
	doInit : function(component, event, helper) {
		console.log(component.get("v.name"), JSON.stringify(component.get("v.question")));
	},

    handleShow : function(component, event, helper) {
        let show = component.get("v.show");
        if(!show) {
            component.set("v.value", undefined);
        }
    },
})