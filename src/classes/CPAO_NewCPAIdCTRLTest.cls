@isTest
public with sharing class CPAO_NewCPAIdCTRLTest {
	
	@isTest
	public static void standardTest() {
		CPAO_TestFactory.automationControlSetup();
		Account account = CPAO_TestFactory.createAccounts(1)[0];
		insert account;
		CPAO_NewCPAIdCTRL.handleNewIdButtonClicks(account.Id);

		Contact contact = CPAO_TestFactory.createContacts(account, 1)[0];
		insert contact;
		CPAO_NewCPAIdCTRL.handleNewIdButtonClicks(contact.Id);

		CPAO_CPA_Id__c idRecord = CPAO_TestFactory.createNotUniqueIdRecords(1)[0];
		insert idRecord;
		CPAO_NewCPAIdCTRL.handleNewIdButtonClicks(idRecord.Id);
	}
}