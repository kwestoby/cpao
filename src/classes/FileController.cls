public class FileController {
    
    @AuraEnabled
 /*   public static Id saveTheFile(Id parentId, String fileName, String base64Data, String contentType) { 
        base64Data = EncodingUtil.urlDecode(base64Data, 'UTF-8');
        
        Attachment a = new Attachment();
        a.parentId = parentId;

        a.Body = EncodingUtil.base64Decode(base64Data);
        a.Name = fileName;
        a.ContentType = contentType;
        
        insert a;
        
        return a.Id;
    }*/
    public static Id saveTheFile(Id parentId, String fileName, String base64Data, String contentType,String fileTitle) { 
        base64Data = EncodingUtil.urlDecode(base64Data, 'UTF-8');
        
        ContentVersion a = new ContentVersion();
        a.FirstPublishLocationId = parentId;

        a.versionData = EncodingUtil.base64Decode(base64Data);
        a.title = fileTitle;

        a.pathOnClient = '/'+fileName;
        //  a.ContentType = contentType;
        
        insert a;
        
        return a.Id;
    }
}