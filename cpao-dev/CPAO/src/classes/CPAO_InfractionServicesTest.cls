@isTest
public class CPAO_InfractionServicesTest {
    
    public static List<Account> factoryAccounts;
    public static List<Contact> factoryContacts;
    public static List<CPAO_Infraction__c> infractions;
    public static CPAO_Infraction_Type__c infractionType;
    
    public static void dataInitialize(Integer numberOfAccounts, Integer numberOfContacts){
        CPAO_TestFactory.automationControlSetup();
        factoryAccounts = CPAO_TestFactory.createAccounts(numberOfAccounts);
        insert factoryAccounts;
        factoryContacts= CPAO_TestFactory.createContacts(factoryAccounts[0],numberOfContacts);
        insert factoryContacts;
        infractionType = CPAO_TestFactory.createInfractionTypes(1, 'test')[0];
        insert infractionType;
        
        infractions = CPAO_TestFactory.createInfractions((numberOfAccounts+numberOfContacts)*2, infractionType);
        insert infractions;
        
    }
    
    @isTest static void ledToSuspension(){
        dataInitialize(2,2);        
        Test.startTest();
        CPAO_InfractionServices.ledToSuspension(infractions);
        
        Test.stopTest();
        System.debug('here1');
        for(CPAO_Infraction__c infraction : infractions) {
            System.debug('here2'+ infraction.CPAO_Date_Led_to_Suspension__c);
            System.assert(infraction.CPAO_Date_Led_to_Suspension__c  == Date.today());
        }              
    }
    
    
    @isTest static void ledToRevocationDeregistration(){
        dataInitialize(2,2);        
        Test.startTest();
        CPAO_InfractionServices.ledToRevocationDeregistration(infractions);
        
        Test.stopTest();
        System.debug('here1');
        for(CPAO_Infraction__c infraction : infractions) {
            System.debug('here2'+ infraction.CPAO_Date_Led_Revocation_Deregistration__c);
            System.assert(infraction.CPAO_Date_Led_Revocation_Deregistration__c  == Date.today());
        }              
    }
    
}