({
    /*upsertFirmApproval : function(component,event,handler) {
        var currentCOA=component.get('v.currentCOA');
        var action = component.get("c.upsertFirmApproval");
        var testField = component.get("v.testName");
        var FirmName = component.get("v.firmName");
        var NatureOfService = component.get("v.NatureOfService");
        var Province = component.get("v.Province");
        var Code = component.get("v.Code");
        var Phone = component.get("v.Phone");
        var Email = component.get("v.Email");
        var Website = component.get("v.Website");
        console.log('FirmName###'+FirmName);
        console.log('NatureOfService###'+NatureOfService);
        //alert(component.get('v.currentCOA'));
        action.setParams({ "COAAppToAdd" :currentCOA});
        action.setParams({ "test" :testField});
        action.setParams({ "FirmName" :FirmName});
        action.setParams({ "NatureOfService" :NatureOfService});
        
        
        action.setCallback(this, function(response){
            var state = response.getState();
            
            if (state === "SUCCESS") {
                console.log('COA added');
                if(component.get('v.shouldSaveAndQuit')){
                    window.location.href='/CPAO_ApplicationDashboard';
                    
                }else{
                    component.set('v.currentCOA',response.getReturnValue());
                    var applicationHasChanged = component.getEvent("applicationHasChanged");
                    applicationHasChanged.setParams({
                        "currentApplication": component.get("v.currentCOA"),
                        "nextButtonClickeable": "true"
                    });
                    applicationHasChanged.fire();
                }
                
            }else{
                console.log('error while adding COA');
                var error= response.getError();
                console.log(error[0]);
            }
        });
        
        $A.enqueueAction(action);
    }*/
    upsertFirmApprovalTest : function(component,event,handler) {
        var action = component.get("c.InsertFirmField");
        console.log('FirmName###'+FirmName);
        console.log('NatureOfService###'+NatureOfService);
        //alert(component.get('v.currentCOA'));
        console.log('objApplication: ' + component.get("v.objApplication"));
        action.setParams({ objCPAO :component.get("v.objApplication")});
       
        action.setCallback(this, function(response){
            var state = response.getState();
            
            if (state === "SUCCESS") {
                console.log('COA added');
                if(component.get('v.shouldSaveAndQuit')){
                    window.location.href='/CPAO_ApplicationDashboard';
                    
                }else{
                    component.set('v.currentCOA',response.getReturnValue());
                    var applicationHasChanged = component.getEvent("applicationHasChanged");
                    applicationHasChanged.setParams({
                        "currentApplication": component.get("v.currentCOA"),
                        "nextButtonClickeable": "true"
                    });
                    applicationHasChanged.fire();
                }
                
            }else{
                console.log('error while adding COA');
                var error= response.getError();
                console.log(error[0]);
            }
        });
        
        $A.enqueueAction(action);
    }
})