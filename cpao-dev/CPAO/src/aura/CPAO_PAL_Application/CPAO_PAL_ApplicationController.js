({
    doInit : function(component,event,helper){
        
        var action = component.get("c.getCurrentPALApplication");
        action.setCallback(this, function(response){
            var state = response.getState();
            if (state === "SUCCESS") {
                //  console.log(JSON.stringify(response.getReturnValue()));
                if( response.getReturnValue().currentPALApplication.CPAO_Effective_Date__c!=null){
                    response.getReturnValue().currentPALApplication.CPAO_Effective_Date__c = response.getReturnValue().currentPALApplication.CPAO_Effective_Date__c.substring(0,10);
                }
                if( response.getReturnValue().currentPALApplication.CPAO_Practice_Inspection_Date_1__c!=null){
                    response.getReturnValue().currentPALApplication.CPAO_Practice_Inspection_Date_1__c = response.getReturnValue().currentPALApplication.CPAO_Practice_Inspection_Date_1__c.substring(0,10);
                }
                if( response.getReturnValue().currentPALApplication.CPAO_Practice_Inspection_Date_2__c!=null){
                    response.getReturnValue().currentPALApplication.CPAO_Practice_Inspection_Date_2__c = response.getReturnValue().currentPALApplication.CPAO_Practice_Inspection_Date_2__c.substring(0,10);
                }
                if( response.getReturnValue().currentPALApplication.CPAO_Practice_Inspection_Date_3__c!=null){
                    response.getReturnValue().currentPALApplication.CPAO_Practice_Inspection_Date_3__c = response.getReturnValue().currentPALApplication.CPAO_Practice_Inspection_Date_3__c.substring(0,10);
                }
                if( response.getReturnValue().currentPALApplication.CPAO_Exceptional_Circumstances_SD__c!=null){
                    response.getReturnValue().currentPALApplication.CPAO_Exceptional_Circumstances_SD__c = response.getReturnValue().currentPALApplication.CPAO_Exceptional_Circumstances_SD__c.substring(0,10);
                }
                if( response.getReturnValue().currentPALApplication.CPAO_Exceptional_Circumstances_End_Date__c!=null){
                    response.getReturnValue().currentPALApplication.CPAO_Exceptional_Circumstances_End_Date__c = response.getReturnValue().currentPALApplication.CPAO_Exceptional_Circumstances_End_Date__c.substring(0,10);
                }
                
                component.set('v.currentPAL',response.getReturnValue().currentPALApplication);
                component.set('v.fileWrapper',response.getReturnValue().ScheduleA_B);
                
                /*  if(component.get('v.currentPAL.CPAO_Current_Step__c')!=null){
                        component.set('v.selectedStep',component.get('v.currentPAL.CPAO_Current_Step__c'));
                    }*/
                  helper.checkProfessionalExperienceCheckboxes(component);
                  // console.log('cur'+component.get('v.currentPAL').Name);
                  //console.log('cur'+component.get('v.currentPAL').CPAO_Contact__c);
                  //console.log('cur'+component.get('v.currentPAL').CPAO_Basis_Of_Application__c);
                  var basisOfApplication=component.get('v.currentPAL').CPAO_Basis_Of_Application__c;
                  if(basisOfApplication==component.get('v.basisOfApplication')){
                      //  component.set('v.confirmationPreviousPAL',true);
                  }
                  // console.log( 'init application: '+JSON.stringify(component.get('v.currentPAL')));
                  
                  
              }else{
                  console.log('error while doinit Main container');
                  var error= response.getError();
                  console.log(error[0]);
                  component.set('v.errorMsg', error[0].message);
              }
        });
        
        $A.enqueueAction(action);
        
        
    },
    handleNext : function(component,event,helper){
        
        var getselectedStep = component.get("v.selectedStep");
        var palApp= component.get('v.currentPAL');
        if(getselectedStep == "step1"){
            component.set("v.selectedStep", "step2");
            //  component.set("v.nextButtonLabel", "Next");
            
        }else if(getselectedStep == "step2"){
            component.set("v.selectedStep", "step3");
            //component.set("v.nextButtonLabel", "Next");
        } else if(getselectedStep == "step3"){ // employment info page
                helper.notifyClickOnNextButton("CPAO_Employment_Info",palApp,'step5');
            }else if(getselectedStep == "step4"){
                helper.notifyClickOnNextButton("CPAO_PAL_BasisOfApplication_Step2",palApp,'step5');
                
            }else if(getselectedStep == "step5"){
                helper.notifyClickOnNextButton("CPAO_PAL_CPDPage_Step3",palApp,'step6');
                
            }else if(getselectedStep == "step6"){
                helper.notifyClickOnNextButton("CPAO_PAL_PracticeInspection_Step4",palApp,'step7'); 
            }else if(getselectedStep == "step7"){
                helper.notifyClickOnNextButton("CPAO_PAL_ProfessionalExperience_Step5",palApp,'step8'); 
            }else if(getselectedStep == "step8"){
                //component.set("v.selectedStep", "step9");
                helper.notifyClickOnNextButton("CPAO_PAL_ScheduleA_B_Step6",palApp,'step9'); 
            }else if(getselectedStep == "step9"){
                helper.notifyClickOnNextButton("CPAO_PAL_GoodDeclaration_Step7",palApp,'step10'); 
                
            }else if(getselectedStep == "step10"){
                helper.notifyClickOnNextButton("CPAO_PAL_References_Step8",palApp,'step11'); 
                
            } 
    },
    applyResponseFromChildComponent : function(component,event,helper){
        //TO DO: Update PAL APP
        ////console.log('response received from childs');
        // console.log('we can continue?'+(event.getParam('nextButtonClickeable')));
        // console.log(component.get("v.selectedStep"));
        if(event.getParam('nextButtonClickeable')){
            if(event.getParam('currentApplication')!=null){ // because on employment component, application is null and we don't want to loose the previous value of application
                component.set('v.currentPAL',event.getParam('currentApplication'));
                console.log( 'returned application: '+JSON.stringify(component.get('v.currentPAL')));
            }
            
            
            helper.formatJavaScriptDates(component);
            if(component.get("v.selectedStep")== "step3"){ // Employment component 
                component.set("v.selectedStep", "step4");

            }else if(component.get("v.selectedStep")== "step4"){
                helper.verifyIfSkippingCPDPage(component,'Next');
            }else if (component.get("v.selectedStep")== "step5"){
                component.set("v.selectedStep", "step6");
                //   component.set("v.nextButtonLabel", "Next");
            }else if (component.get("v.selectedStep")== "step6"){
                component.set("v.selectedStep", "step7");
                //   component.set("v.nextButtonLabel", "Next");
            }else if (component.get("v.selectedStep")== "step7"){
                component.set("v.selectedStep", "step8");
                //component.set("v.selectedStep", "step11");
                
                ///   component.set("v.nextButtonLabel", "Next");
            }else if (component.get("v.selectedStep")== "step8"){
                //component.set("v.selectedStep", "step9");
                component.set("v.selectedStep", "step9");
                
                //   component.set("v.nextButtonLabel", "Next");
            }else if (component.get("v.selectedStep")== "step9"){
                component.set("v.selectedStep", "step10");
                
                // component.set("v.selectedStep", "step10");
                //   component.set("v.nextButtonLabel", "Next");
            }else if (component.get("v.selectedStep")== "step10"){
                component.set("v.selectedStep", "step11");
                //   component.set("v.nextButtonLabel", "Next");
            }
            helper.hideSpinner(component);
            
        }
    },
    handlePrev : function(component,event,helper){
        var getselectedStep = component.get("v.selectedStep");
        if(getselectedStep == "step2"){
            component.set("v.selectedStep", "step1");
            // component.set("v.nextButtonLabel", "Start PAL Application");
        }
        else if(getselectedStep == "step3"){
            component.set("v.selectedStep", "step2");
        }
            else if(getselectedStep == "step4"){
                
                component.set("v.selectedStep", "step3");
            }
        
                else if(getselectedStep == "step5"){
                    
                    component.set("v.selectedStep", "step4");
                } else if(getselectedStep == "step6"){
                    // helper.verifyIfSkippingCPDPage(component,'Previous');
                    component.set("v.selectedStep", "step5");
                }else if(getselectedStep == "step7"){
                    component.set("v.selectedStep", "step6");
                }else if(getselectedStep == "step8"){
                    //component.set("v.selectedStep", "step7");
                    helper.verifyIfSkippingCPDPage(component,'Previous'); 
                }else if(getselectedStep == "step9"){
                    // helper.verifyIfSkippingCPDPage(component,'Previous'); 
                    component.set("v.selectedStep", "step8");
                    
                }else if(getselectedStep == "step10"){
                    component.set("v.selectedStep", "step9");
                }else if(getselectedStep == "step11"){
                    component.set("v.selectedStep", "step10");
                }
    },
    
    handleFinish : function(component,event,helper){
        //helper.showSpinner(component);
        var palApp= component.get('v.currentPAL');
        helper.notifyClickOnNextButton("CPAO_PAL_ReviewPage_Step9",palApp,'');
        
    },
    
    saveAndQuit: function(component,event,helper){
        var getselectedStep = component.get("v.selectedStep");
        var palApp= component.get('v.currentPAL');
        if(getselectedStep == "step1" || getselectedStep == "step2" || getselectedStep == "step3"){
            window.location.href='/CPAO_ApplicationDashboard';
            return;
        } else {
            if(getselectedStep == "step4"){
                helper.notifyClickOnNextButton("CPAO_PAL_BasisOfApplication_Step2",palApp,'Exit');
                
            }else if(getselectedStep == "step5"){
                helper.notifyClickOnNextButton("CPAO_PAL_CPDPage_Step3",palApp,'Exit');
                
            }else if(getselectedStep == "step6"){
                helper.notifyClickOnNextButton("CPAO_PAL_PracticeInspection_Step4",palApp,'Exit'); 
            }else if(getselectedStep == "step7"){
                helper.notifyClickOnNextButton("CPAO_PAL_ProfessionalExperience_Step5",palApp,'Exit'); 
            }else if(getselectedStep == "step8"){
                //component.set("v.selectedStep", "step9");
                helper.notifyClickOnNextButton("CPAO_PAL_ScheduleA_B_Step6",palApp,'Exit'); 
            }else if(getselectedStep == "step9"){
                helper.notifyClickOnNextButton("CPAO_PAL_GoodDeclaration_Step7",palApp,'Exit'); 
                
            }else if(getselectedStep == "step10"){
                helper.notifyClickOnNextButton("CPAO_PAL_References_Step8",palApp,'Exit'); 
            }
        }
        
    },
    clickOnNextButton : function(component,event,helper){
        console.log('click on next event fired in PAL  AND NT CHILDS');
    },
    redirectToReviewPageSection: function(component,event,helper){
        alert('catched');
        var sectionToModify=event.getParam('stepToRedirect');
        component.set('v.selectedStep',sectionToModify);
    },
    showSpinner: function (component, event, helper) {
		console.log("showSpinner()");
        helper.showSpinner(component, event, helper);
    }
    
    
})