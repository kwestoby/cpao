({
	 doInit : function(component, event, helper) {
        var apexAction = component.get('c.loadReviewInfo');
        apexAction.setCallback(this, function(response) {
            var state = response.getState();
            console.log('state');
            console.log(state);
            if(state === "SUCCESS"){
                var result = response.getReturnValue();
                component.set("v.userContact", result.userContact);
                component.set('v.PAL_attestation',result.PAL_attestation);
                
            }
        });
        $A.enqueueAction(apexAction);
    },
    clickOnNextButton : function(component,event,helper){
        
        var componentNameInEvent=event.getParam("componentName");
        
        
        if(componentNameInEvent==component.get('v.nameOfThisComponent')){
            var validForm = component.find('PALForm').reduce(function (validSoFar, inputCmp) {
                
                inputCmp.showHelpMessageIfInvalid();
                return validSoFar && inputCmp.get('v.validity').valid ;
            }, true);
            
            if(validForm){
                component.set('v.currentPAL.CPAO_Status__c','Submitted');
                component.set('v.currentPAL.CPAO_Submission_Date__c',new Date());
                var showSpinnerEvent = component.getEvent("showSpinner");
                showSpinnerEvent.fire();
                helper.upsertPAL(component);
            }
            
        }
    }
})