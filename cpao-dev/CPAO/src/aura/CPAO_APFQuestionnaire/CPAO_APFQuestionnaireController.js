({
    doInit : function(component, event, helper) {
        console.log(JSON.stringify(component.get("v.currentAPF")));
        helper.getInitWrapper(component, helper);
    },

    handlePrev : function(component, event, helper) {
        helper.firePrevEvent(component);
    },

    handleNext : function(component, event, helper) {
        helper.updateAPF(component, helper, false);
    },

    handleSaveAndQuit : function(component, event, helper) {
        helper.updateAPF(component, helper, true);
    },

    clicky: function(component) {
        console.log(JSON.stringify(component.get("v.currentAPF")));
    },
})