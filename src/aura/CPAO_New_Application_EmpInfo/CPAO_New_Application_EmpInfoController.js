({
    // @Author : Halima Dosso
    // This function runs once the component CPAO_Employment_Info is fully loaded on the page 
    // It calls an apex method @getRelationships from class @CPAO_EmployerInfoController
    // It sets two component attributes based on method @getRelationships returned value:
    //  - listOfRelationships: list of all CPAO_Account_Contact_Relationship__c records associated
    //    to end user's contact record
    //  - showRetireUnemployButton: boolean indicating if the user should see button "Confirm Retirement"
	doInit: function(component, event, helper) {
    console.log('doInintforMainComponent');
	var action = component.get("c.getRelationships");
   	action.setCallback(this, function(response){
        var state = response.getState();
        if (state === "SUCCESS") {
            console.log(response.getReturnValue());
            component.set("v.listOfRelationships", response.getReturnValue().relationships);
            if(response.getReturnValue().canBeRetiredUnemployed == true){
                component.set('v.showRetireUnemployButton',true);
            }
        }
    });

    $A.enqueueAction(action);
	},

    // @Author: Halima Dosso
    // This function runs once the end user clicks on employer record in order to modify it
    // This function sets @currentRelShowForm to true so the user could see fill up the form
    // corresponding to the CPAO_Account_Contact_Relationship__c the user clicked on.
    showOrHideCurrentEmployerForm : function(component, event, helper) {
        component.set('v.currentRelShowForm','true');
        var positionInlist=event.getSource().get("v.value");
        var relationAtPosition= component.get('v.listOfRelationships')[positionInlist];
        if(relationAtPosition.CPAO_Primary_Employer__c=='Yes'){
            component.set('v.isAlreadyPrimaryEmployer',true);
        }else{
            component.set('v.isAlreadyPrimaryEmployer',false);
        }

        component.set('v.employerPositionInList',relationAtPosition);
    },

    // @Author: Halima Dosso
    // This function runs once the end user clicks on button "Add new Employer"
    // This function sets @newEmployerShowForm to true so the user could see fill up the form
    // corresponding to the new CPAO_Account_Contact_Relationship__c he wish to add.
    showOrHideNewEmployerForm : function(component, event, helper) {
    component.set('v.newEmployerShowForm','true'); 
    },

    // @Author: Halima Dosso
    // This function runs once the end user choose from a list of radio buttons in the main page.
    // Depending on the radio button choice, the function assign them a value.
    // Those values will be further used to show the appropriate content to the user
    radioButtonChange : function(component, event, helper) {
        var changedValue = event.getParam("value");
        if(changedValue=='MRA'){     
            component.set('v.radioValue','MRA');        
        } else if(changedValue=='FirmReg'){
            component.set('v.radioValue','FirmReg');
        }
        //else (changedValue=='FirmReg'){
            //component.set('v.radioValue','FirmReg');
        //}
        
        // get Radio active label/value and if MAra
        // hide the MAra button clas by using $A.util.addclass("slds-hide") and  
    },

     confirmRetirement : function(component, event, helper) {
    helper.confirmRetirementUnemployment(component, event, helper, 'Retired');
    },

    confirmUnemployment : function(component, event, helper) {
    helper.confirmRetirementUnemployment(component, event, helper, 'Unemployed');
    },

    /*test: function(component, event, helper) {
    console.log('fired in main');
    //location.reload();
    component.set('v.body','CPAO_Employment_Info'); 
    },*/

    updatedRelationshipRecord : function(component, event, helper) {
        console.log('relationship: ');
        console.log(event.getParam("msg"));
        console.log(event.getParam("type"));
        console.log(component.get('v.confirmationMessage'));
        console.log(component.get('v.severity'));

        
        var action = component.get("c.getRelationships");
        action.setCallback(this, function(response){
            var state = response.getState();
            if (state === "SUCCESS") {
              component.set("v.listOfRelationships", response.getReturnValue().relationships);
              component.set('v.newEmployerShowForm','false');
              if(response.getReturnValue().canBeRetiredUnemployed  == true){
                component.set('v.showRetireUnemployButton',true);
              }
            }
        });
        $A.enqueueAction(action);
        if(event.getParam("relationship") != null && event.getParam("relationship") != undefined){
            var deserializedRelationship = event.getParam("relationship");
            console.log('about to set the');
            component.set('v.employerPositionInList',deserializedRelationship);
            console.log('afetr being set');
        }
        if(event.getParam("msg") != null && event.getParam("msg") != undefined && 
                event.getParam("type") != undefined && event.getParam("type") != undefined){

            component.set("v.severity", event.getParam("type"));
            component.set("v.confirmationMessage", event.getParam("msg"));
        }

    },

    // @Author: Halima Dosso
    // This function is used once the CPAO_Employment_Info is used as part of a join process
    // It runs once the user clicks on the next button to get to the next step of the process
    // In this case, it redirects the user to the infraction wireframe screen
    nextButton:function(component, event, helper) {
        
    }
  

})