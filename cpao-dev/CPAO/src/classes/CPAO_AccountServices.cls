public without sharing class CPAO_AccountServices {
    
    public static String FIRM = 'Firm';
    public static String PROFESSIONAL_COORP = 'Professional Corporation';
    
    public static Boolean  account_potentially_eligible_to_LSO_Badge(Account account){
        // make it LSO_PC BECAUSE ONLY THEM HAVE ACCESS TO COA 
        return (account.CPAO_LSO__c!=null && account.CPAO_Account_Type__c==FIRM && account.CPAO_Corporate_Structure__c==PROFESSIONAL_COORP);
        
    }
    
    
    public static void  assign_LSO_badges_to_eligible_contacts(List<Id> potential_LSOContacts){
        
        Set<Id> setOfIds = new Set<Id>();
        setOfIds.addAll( potential_LSOContacts);
        List<Contact> final_LSOContacts=CPAO_ContactSelector.getLSOContacts(setOfIds);
        List<Id>final_LSOContactsId = new List<Id>();  
        for (Contact LSO_contact:final_LSOContacts){
            final_LSOContactsId.add(LSO_contact.Id);
        }
        if(!final_LSOContactsId.isEmpty()){
            CPAO_ContactServices.assignBadgeToContacts(final_LSOContactsId,'LSO');
            ///System.debug('LSO badges added');
        }
        
    }
}