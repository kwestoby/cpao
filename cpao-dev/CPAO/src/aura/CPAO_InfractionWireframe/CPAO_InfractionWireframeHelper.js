({
	//This mehod verify if all the forms related to every infractions are completed
	//If they are, it makes the next button clickeable
	verifyIfAllFormsAreCompleted : function(component,listOfInfractions) {

		var makeButtonClickeable=false;
		for (i = 0; i < listOfInfractions.length; i++) { 
			//console.log(listOfInfractions[0].CPAO_Form_Status__c);
			if(listOfInfractions[i].CPAO_Form_Status__c!=='Open'){
  			  	makeButtonClickeable=true;
			}else{
				makeButtonClickeable=false;
				return;
			}

		}
		if(makeButtonClickeable){
			console.log('ici');
			var nextButtonisCurrentlyNotClickeable= component.get('v.nextButtonIsNotClickable');
			component.set('v.nextButtonIsNotClickable','false');
		}
	}
})