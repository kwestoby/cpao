global class CPAO_PoaComplianceBatch implements Database.Batchable<sObject> {
    //searches for contacts with CPA Contact Type = member and who have no continuous amd waiver    
    global Database.QueryLocator start(Database.BatchableContext BC) {
        //String query='SELECT Id, CPAO_Compliance_Due_Date__c FROM CPAO_Application__c WHERE CPAO_Compliance_Declaration__c = false AND ';
        //query+= 'CPAO_Record_Keeping_Declaration__c = false AND CPAO_Status__c = \'Completed\' AND ';
        //query+= 'RecordType.Name = \'AMD/CPD\'';
        //return Database.getQueryLocator(query);
        //Date todaysDate = Date.today();
        String query='SELECT Id, CPAO_Compliance_Due_Date__c, CPAO_Sub_Status__c, CPAO_POA_Due_Date__c, CPAO_POA_Submission_Date__c, CPAO_Contact__c ';
        query+= 'FROM CPAO_Application__c WHERE RecordType.Name = \'AMD/CPD\' AND CPAO_Status__c != \'Open\' AND (CPAO_Sub_Status__c ';
        query+= '= \'POA Pending\' OR CPAO_Sub_Status__c = \'POA Compliance Pending\') AND CPAO_Contact__r.CPAO_CPD_Continuous_Exemption__c = null';
        System.debug('query: ' + query);
        return Database.getQueryLocator(query);
    }

    global void execute(Database.BatchableContext BC, List<CPAO_Application__c> applications) {
        System.debug('applications: ' + applications);
        List<CPAO_Application__c> breachList = new List<CPAO_Application__c>();
        List<CPAO_Application__c> poaSubmissionDate110List = new List<CPAO_Application__c>();
        List<CPAO_Application__c> poaSubmissionDate119List = new List<CPAO_Application__c>();

        for(CPAO_Application__c app:applications){
            Date today = Date.today();
            Date complianceDueDate = app.CPAO_Compliance_Due_Date__c;
            Date poaDueDate = app.CPAO_POA_Due_Date__c;
            Date poaSubmissionDate = app.CPAO_POA_Submission_Date__c;

            if(poaDueDate.addDays(1) == today && app.CPAO_Sub_Status__c == 'POA Pending'){
                breachList.add(app);
            }
            if(complianceDueDate.addDays(1) == today && app.CPAO_Sub_Status__c == 'POA Compliance Pending'){
                breachList.add(app);
            }
            if(poaSubmissionDate.addDays(110) == today && app.CPAO_Sub_Status__c == 'POA Compliance Pending'){
                poaSubmissionDate110List.add(app);
            }
            if(poaSubmissionDate.addDays(119) == today && app.CPAO_Sub_Status__c == 'POA Compliance Pending'){
                poaSubmissionDate119List.add(app);
            }
        }

        if(!breachList.isEmpty()){            
            List<CPAO_Infraction_Type__c> infractionType = CPAO_InfractionSelector.getInfractionType('CPD Plan of Action Compliance (CPD POA)');
            if(infractionType.size() == 1){
                List<CPAO_Infraction__c> infractionsToCreate = new List<CPAO_Infraction__c>();
                for(CPAO_Application__c app:breachList){
                    infractionsToCreate.add(new CPAO_Infraction__c(
                    CPAO_Infraction_Type__c = infractionType[0].Id,
                    CPAO_Contact__c = app.CPAO_Contact__c));
                }
                if(!infractionsToCreate.isEmpty()){
                    insert infractionsToCreate;
                }
            }                
        }

        if(!poaSubmissionDate110List.isEmpty() || !poaSubmissionDate119List.isEmpty()){
            Messaging.SingleEmailMessage[] messages = new List<Messaging.SingleEmailMessage>();

            if(!poaSubmissionDate110List.isEmpty()){
                messages.addAll(sendWarningEmails(poaSubmissionDate110List, 'CPAO_CPD_POA_compliance_reminder'));
            }
            if(!poaSubmissionDate119List.isEmpty()){
                messages.addAll(sendWarningEmails(poaSubmissionDate119List, 'CPAO_CPD_POA_compliance_final_notice'));
            }
            if(!messages.isEmpty()){
                Messaging.SendEmailResult[] resultMail = Messaging.sendEmail(messages);
            }            
        }
    }

    global void finish(Database.BatchableContext BC) {
        
    }

    public static Messaging.SingleEmailMessage[] sendWarningEmails(List<CPAO_Application__c> warningList, String templateName) {
        List<EmailTemplate> lstEmailTemplates = [SELECT Id, Body, Subject from EmailTemplate where DeveloperName = :templateName];
        if(lstEmailTemplates.size() != 1){
            return null;
        }
        Messaging.SingleEmailMessage[] messages = new List<Messaging.SingleEmailMessage>();

        for(CPAO_Application__c app:warningList){
            Messaging.SingleEmailMessage mail = new Messaging.SingleEmailMessage();
            mail.setTemplateId(lstEmailTemplates[0].Id);
            mail.setSaveAsActivity(false);
            mail.setTargetObjectId(app.CPAO_Contact__c);

            mail.setWhatId(app.id);
            messages.add(mail);
        }
        return messages;
    }
}