trigger CPAO_ReceiptItemUpdate  on OrderApi__Item__c (before update) {

    CPAO_Automation_Control__c control = CPAO_Automation_Control__c.getOrgDefaults();
    system.debug('control control control '+control );
    if(!control.OrderApi_Item_c__c){
        return;
    }
    
    Map<Id, OrderApi__Item__c> itemsToEvaluate = new Map<Id, OrderApi__Item__c>();
    if(Trigger.isBefore&& Trigger.isUpdate){
        for(OrderApi__Item__c itemObj: Trigger.New){
                itemsToEvaluate.put(itemObj.Id,itemObj);
            }
    }       
    CPAO_ReceiptItemUpdateHelper.updateItemReceipts(itemsToEvaluate);
}