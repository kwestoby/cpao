<?xml version="1.0" encoding="UTF-8"?>
<CustomMetadata xmlns="http://soap.sforce.com/2006/04/metadata" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xmlns:xsd="http://www.w3.org/2001/XMLSchema">
    <label>Retired years of service is 70</label>
    <protected>false</protected>
    <values>
        <field>AMD_or_CPD__c</field>
        <value xsi:type="xsd:string">AMD</value>
    </values>
    <values>
        <field>CPAO_Explanation__c</field>
        <value xsi:type="xsd:string">Members who have reached the age of 55 or more an hose gross annual income, excluding pension or other retirement/investment income is not greater than $25,000; AND their age and the total number of years of continuous membership in good standing held in CPA Ontario, CGA Ontario, CMA Ontario or one or more provincial bodies or an accounting body recognized by the Council (or Board), equals or exceeds the sum of 70.</value>
    </values>
    <values>
        <field>CPAO_Percent_Off__c</field>
        <value xsi:type="xsd:double">100.0</value>
    </values>
    <values>
        <field>Explanation_Third_Year__c</field>
        <value xsi:nil="true"/>
    </values>
    <values>
        <field>Full_Name__c</field>
        <value xsi:type="xsd:string">Retired (Age 55 and over, sum of age and years of service is 70 years or over)</value>
    </values>
</CustomMetadata>
