/**
 * @author Sachet Khanal (sakhanal@deloitte.com)
 * @version 1.0.0
 * @description Test class for CPAO_OfflinePaymentTypeCtrl
 *
 *
 **/
@isTest
public class CPAO_OfflinePaymentTypeCtrlTest {

	@TestSetup
	static void setupData() {
		CPAO_Instruction__c instructions = new CPAO_Instruction__c(CPAO_Applicable_To__c = 'Offline Payment',
		                                                           CPAO_Detail__c = 'Offline instructions');
		insert instructions;
		Contact con = new Contact(FirstName = 'Test', LastName = 'Contact');
		insert con;
		OrderApi__Sales_Order__c salesOrder = new OrderApi__Sales_Order__c(OrderApi__Entity__c = 'Contact',
		                                                                   OrderApi__Contact__c = con.Id,
		                                                                   OrderApi__Posting_Entity__c = 'Invoice',
		                                                                   OrderApi__Posting_Status__c = 'Posted');
		insert salesOrder;
		OrderApi__Invoice__c invoice = new OrderApi__Invoice__c(OrderApi__Sales_Order__c = salesOrder.Id,
		                                                        OrderApi__Entity__c = 'Contact',
		                                                        OrderApi__Contact__c = con.Id);
		insert invoice;
		OrderApi__EPayment__c epayment = new OrderApi__EPayment__c(OrderApi__Entity__c = 'Contact',
		                                                           OrderApi__Contact__c = con.Id);
		insert epayment;
		OrderApi__EPayment_Line__c lineItem = new OrderApi__EPayment_Line__c(OrderApi__EPayment__c = epayment.Id,
		                                                                     OrderApi__Invoice__c = invoice.Id);
		insert lineItem;
	}

	@IsTest
	static void testGetInstructions() {
		Test.startTest();
		CPAO_Instruction__c instructions = CPAO_OfflinePaymentTypeCtrl.getOfflineInstructions();
		System.assertEquals('Offline instructions', instructions.CPAO_Detail__c);
		Test.stopTest();
	}

	@IsTest
	static void testConfirmOfflinePaymentWithSalesOrder() {
		Test.startTest();
		OrderApi__Sales_Order__c salesOrder = [SELECT Id, CPAO_Pay_Offline__c FROM OrderApi__Sales_Order__c LIMIT 1];
		System.assertEquals(false, salesOrder.CPAO_Pay_Offline__c);
		CPAO_OfflinePaymentTypeCtrl.confirmOfflinePayment(salesOrder.Id);
		Test.stopTest();
		salesOrder = [SELECT Id, CPAO_Pay_Offline__c FROM OrderApi__Sales_Order__c LIMIT 1];
		System.assertEquals(true, salesOrder.CPAO_Pay_Offline__c);
	}

	@IsTest
	static void testConfirmOfflinePaymentWithEpayment() {
		OrderApi__Sales_Order__c salesOrder = [SELECT Id, CPAO_Pay_Offline__c FROM OrderApi__Sales_Order__c LIMIT 1];
		System.assertEquals(false, salesOrder.CPAO_Pay_Offline__c);
		Test.startTest();
		OrderApi__EPayment__c epayment = [SELECT Id FROM OrderApi__EPayment__c LIMIT 1];
		CPAO_OfflinePaymentTypeCtrl.confirmOfflinePayment(epayment.Id);
		Test.stopTest();
		salesOrder = [SELECT Id, CPAO_Pay_Offline__c FROM OrderApi__Sales_Order__c LIMIT 1];
		System.assertEquals(true, salesOrder.CPAO_Pay_Offline__c);
	}
}