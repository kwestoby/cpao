@isTest
private class CPAO_RefundsControllerTest {

	@TestSetup
	private static void setupData() {
		CPAO_TestFactory.automationControlSetup();
		Account acc = CPAO_TestFactory.createAccounts(1) [0];
		insert acc;
		Contact con = CPAO_TestFactory.createContacts(acc, 1) [0];
		insert con;
		OrderApi__Receipt__c payment = new OrderApi__Receipt__c(OrderApi__Contact__c = con.Id,
		                                                        OrderApi__Entity__c = 'Contact',
		                                                        OrderApi__Is_Posted__c = true,
		                                                        OrderApi__Posted_Date__c = System.today(),
		                                                        OrderApi__Total__c = 100.00,
		                                                        RecordTypeId = Schema.SObjectType.OrderApi__Receipt__c.getRecordTypeInfosByName().get('Payment').getRecordTypeId(),
		                                                        OrderApi__Type__c = 'Payment');
		insert payment;
		OrderApi__Receipt__c refund = new OrderApi__Receipt__c(OrderApi__Contact__c = con.Id,
		                                                       OrderApi__Entity__c = 'Contact',
		                                                       OrderApi__Is_Posted__c = true,
		                                                       OrderApi__Posted_Date__c = System.today(),
		                                                       OrderApi__Total__c = 100.00,
		                                                       OrderApi__Receipt__c = payment.Id,
		                                                       RecordTypeId = Schema.SObjectType.OrderApi__Receipt__c.getRecordTypeInfosByName().get('Refund').getRecordTypeId(),
		                                                       OrderApi__Type__c = 'Refund');
		insert refund;
	}

	@isTest
	private static void testGetReceipts() {
		Contact con = [SELECT Id FROM Contact LIMIT 1];
		User usr = CPAO_TestFactory.createCommunityUser(con.Id);
		insert usr;
		System.runAs(usr) {
			Test.startTest();
			List<OrderApi__Receipt__c> receipts = CPAO_RefundsController.getReceipts();
			Test.stopTest();
		}

	}
}