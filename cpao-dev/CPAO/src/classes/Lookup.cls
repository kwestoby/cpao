/**
 * @Author		:		Jitendra Zaa
 * @Date		:		4-Jul-2017
 * @Desc		:		Controller for Lookup Lightning Component modified
 * @Reference       :       http://www.jitendrazaa.com/blog/salesforce/lookup-component-in-salesforce-lightning/
 * */

public without sharing class Lookup {



    /**
     * Returns JSON of list of ResultWrapper to Lex Components
     * @objectName - Name of SObject
     * @fld_API_Text - API name of field to display to user while searching
     * @fld_API_Val - API name of field to be returned by Lookup COmponent
     * @lim   - Total number of record to be returned
     * @fld_API_Search - API name of field to be searched
     * @searchText - text to be searched
     * */

    @AuraEnabled 

    public static String searchDB(String objectName, String fld_API_Text, String fld_API_Val, 
                                Integer lim,String fld_API_Search,String searchText, String optionalCriteria ){

        searchText='\'%' + String.escapeSingleQuotes(searchText.trim()) + '%\'';
        String query = 'SELECT '+fld_API_Text+' ,'+fld_API_Val;
        if(objectName == 'Account') {
             query += ', BillingStreet, BillingCity, BillingState, BillingPostalCode, BillingCountry ';
        }
        query += ' FROM '+objectName;
        query += ' WHERE '+fld_API_Search+' LIKE '+searchText;
        if(String.isNotBlank(optionalCriteria)){
            query += ' AND ' +optionalCriteria;
        } 
        query += ' LIMIT '+lim;

        List<sObject> sobjList = Database.query(query);
        List<ResultWrapper> lstRet = new List<ResultWrapper>();
       
        for(SObject s : sobjList){

            ResultWrapper obj = new ResultWrapper();

            obj.objName = objectName;
            String billingStreet='';
            String billingCity='';
            String billingState='';
            String billingPostalCode='';
            String billingCountry='';

            if(objectName == 'Account') {
                if(String.valueOf(s.get('BillingStreet'))!=null){
                    billingStreet=' - '+String.valueOf(s.get('BillingStreet'))+',';
                }
                if(String.valueOf(s.get('billingCity'))!=null){
                    billingCity=String.valueOf(s.get('billingCity'))+',';
                }
                if(String.valueOf(s.get('billingState'))!=null){
                    billingState=String.valueOf(s.get('billingState'))+',';
                }
                if(String.valueOf(s.get('billingPostalCode'))!=null){
                    billingPostalCode=String.valueOf(s.get('billingPostalCode'))+',';
                }
                if(String.valueOf(s.get('billingCountry'))!=null){
                    billingCountry= String.valueOf(s.get('billingCountry'));
                }
                system.debug(String.valueOf(s.get('BillingStreet')) );
            }
            obj.text = String.valueOf(s.get(fld_API_Text)) +billingStreet+' ' 
            + billingCity+' '+billingState+' '+billingPostalCode+billingState+' '+billingCountry;
            obj.val = String.valueOf(s.get(fld_API_Val)) ;
            /*obj.text = String.valueOf(s.get(fld_API_Text))+ ' - '+String.valueOf(s.get('BillingStreet'))+', ' 
            + String.valueOf(s.get('BillingCity')) +', '+ String.valueOf(s.get('BillingState'))+', '+
            String.valueOf(s.get('BillingPostalCode')) + ', '+String.valueOf(s.get('BillingCountry'));
            obj.val = String.valueOf(s.get(fld_API_Val)) ;*/

            lstRet.add(obj);
                   


        } 

         return JSON.serialize(lstRet) ;
     }

/* @AuraEnabled 

    public static String searchDB(String objectName, String fld_API_Text, String fld_API_Val, 
                                Integer lim,String fld_API_Search,String searchText ){
        searchText='\'%' + String.escapeSingleQuotes(searchText.trim()) + '%\'';
       

        String query = 'SELECT '+fld_API_Text+' ,'+fld_API_Val+
            			' FROM '+objectName+
            			' WHERE '+fld_API_Search+' LIKE '+searchText+ 
            			' LIMIT '+lim;
       

        List<sObject> sobjList = Database.query(query);
        List<ResultWrapper> lstRet = new List<ResultWrapper>();
       
        for(SObject s : sobjList){

            ResultWrapper obj = new ResultWrapper();

            obj.objName = objectName;

            obj.text = String.valueOf(s.get(fld_API_Text)) ;

            obj.val = String.valueOf(s.get(fld_API_Val))  ;

            lstRet.add(obj);

        } 

         return JSON.serialize(lstRet) ;

    }*/

    

    public class ResultWrapper{

        @AuraEnabled public String objName {get;set;}

        @AuraEnabled public String text{get;set;}

        @AuraEnabled public String val{get;set;}

    }

}