({
    becomeLicensedPA_Redirect : function(component, event, helper) {
        url_to_redirect='https://www.cpaontario.ca/cpa-members/obligations/public-accounting-licence';
        window.open(url_to_redirect,'_blank');
    },
    
    byLaws_Redirect : function(component, event, helper) {
        url_to_redirect='https://media.cpaontario.ca/stewardship-of-the-profession/pdfs/CPA-Ontario-Members-Handbook.pdf';
        window.open(url_to_redirect,'_blank');
    },
    
    FAQ_Redirect : function(component, event, helper) {
        url_to_redirect='https://www.cpaontario.ca/cpa-Members/public-practice/setting-up-a-practice/firm-names-faqs';
        //window.location.href=url_to_redirect;
        window.open(url_to_redirect,'_blank');
    },
})