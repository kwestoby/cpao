<?xml version="1.0" encoding="UTF-8"?>
<CustomMetadata xmlns="http://soap.sforce.com/2006/04/metadata" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xmlns:xsd="http://www.w3.org/2001/XMLSchema">
    <label>Electronic Funds Transfer (EFT)</label>
    <protected>false</protected>
    <values>
        <field>CPAO_Description__c</field>
        <value xsi:type="xsd:string">Payment must be received on or before the due date. Dues may be paid through the Internet or by telephone bill payment with your ﬁnancial institution. The listing is under “Chartered Professional Accountants of Ontario” or “CPA Ontario”. Please use your CPA Ontario ID as the account number. EFT payments will take longer to process than credit card payments. Please allow ﬁve business days for EFT payment to be processed and posted to your account with CPA Ontario. Please note that Electronic Funds Transfers are not Email Transfers (E- Transfers) and that CPA Ontario does not accept E-Transfers.</value>
    </values>
    <values>
        <field>CPAO_Order__c</field>
        <value xsi:type="xsd:double">4.0</value>
    </values>
    <values>
        <field>CPAO_Type__c</field>
        <value xsi:type="xsd:string">Offline Payment</value>
    </values>
</CustomMetadata>
