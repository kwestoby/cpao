public with sharing class CPAO_RosterManagement{
    
    
    public static Map<Id,Account> accountMap = new Map<Id,Account>();
    Public static List<Id> contactlistIds = new List<Id>();
    public static List<Id> ContactIds = new List<Id>();
    public static Set<Contact> confinalSet= new  Set<Contact>();
    
    /* Fetch Users */
    @AuraEnabled 
    public static user getUser(){
        User u = CPAO_UserSelector.getCurrentUser();
        return u;
    } 
    
    
    @AuraEnabled
    public static List<Account> getAccountNames(){
        User u = CPAO_UserSelector.getCurrentUser();
        return CPAO_AccountSelector.getContactSoAccounts(u.ContactId);
    }
    
    @AuraEnabled 
    public static List<Account> getAccountDetails(String idListJSONStr){
        System.debug('idListJSON = ' + idListJSONStr);
        Type idArrType=Type.forName('List<Id>');
        List<Id> ids=(List<Id>) JSON.deserialize(idListJSONStr, idArrType);
        return CPAO_AccountSelector.getAccountToUpdate(new Set<Id>(ids));
    }
    
    /* Roster Payment Type */
    @AuraEnabled
    public static List<CPAO_RosterPaymentType__mdt> queryAllPaymentTypes(){                                                 
        return CPAO_MetadataTypeSelector.getRosterPaymentType();
    }
    
    /* Roster Instruction Screen */
    @AuraEnabled
    public static CPAO_RosterInstructionScreen__mdt queryInstructionScreen(){
        return CPAO_MetadataTypeSelector.getRosterInstruction();
    }
    
    @AuraEnabled
    public static List<Contact> getContactNames(string selectedRosterPaymentType, string selectedAccountIds){
        List<Id> subContactIds = new List<Id>();
        List<Id> itemClassIds = new List<Id>();
        List<Id> contactAppIds = new List<Id>();
        List<Account> accIds= new List<Account>();

        
        System.debug('selectedAccountsIds= ' + selectedAccountIds);
        
        Type idArrType=Type.forName('List<Id>');
        List<Id> ids=(List<Id>) JSON.deserialize(selectedAccountIds, idArrType);
        system.debug('idssssssssss'+ids);
        
        User u = CPAO_UserSelector.getCurrentUser();

        for(Account account:CPAO_AccountSelector.getContactSoAccountsWithIds(u.ContactId, ids)){
            accountMap.put(account.OrderApi__Primary_Contact__c, account);
        }
        
        system.debug(' accountMap'+accountMap);
        String payType = '%'+selectedRosterPaymentType+'%';
        system.debug('payType'+payType);
        
        for(CPAO_Application__c appContact : CPAO_ApplicationSelector.getAppsByStatusAndType('Submitted', payType)){
            contactAppIds.add(appContact.CPAO_Contact__c);
        }
        system.debug('contactAppIds'+contactAppIds);
            
        
        for(CPAO_Account_Contact_Relationship__c accConRelation : 
        CPAO_AccountContactRelationSelector.getPrimaryRelationshipsByContactId(new Set<Id>(contactAppIds))){
            Account acc = accountMap.get(accConRelation.CPAO_Contact__c);
            system.debug('accConRelation.CPAO_Contact__c'+accConRelation.CPAO_Contact__c);
            if(acc != null){
                ContactIds.add(accConRelation.CPAO_Contact__c);
                system.debug('accConRelationaccConRelation '+accConRelation );

            }
        }
        
        
        
        return CPAO_ContactSelector.getContactToUpdate(new Set<Id>(ContactIds));
        
    }
    
    @AuraEnabled
    public static list<contact> searchmethod(string searchKeyword){
        system.debug('--enter --'+searchKeyword);
        String searchKey=searchKeyword + '%';
        system.debug('--enter =--'+searchKey);
        list<Id> conIds =new list<Id>();
        list<Id> contactmembersIds =new list<Id>();

        list<contact> ConList=[select id,name,CPAO_Primary_Employer__r.Name, CPAO_Subscription_Status_of_Comple_AMD__c,CPAO_Subscription_Status_of_Comple_APF__c,CPAO_Subscription_Status_of_Comple_PAL__c, CPAO_CPA_Id_Display__c from contact where CPAO_CPA_Id_Display__c like: searchKey and CPAO_CPA_Contact_Type__c =:'Member'];
        for(contact con:ConList){
            system.debug('connnnnnn'+con.Name);
            conIds.add(con.Id);
        }
        
        for(CPAO_Account_Contact_Relationship__c accConRelation : [Select CPAO_Account__c, CPAO_Contact__c, CPAO_Primary_Employer__c from CPAO_Account_Contact_Relationship__c where CPAO_Contact__c =: conIds]){
            contactmembersIds.add(accConRelation.CPAO_Contact__c);
        }
        
        List<Contact> condisplay = [Select Id, name, CPAO_Primary_Employer__r.Name, CPAO_Subscription_Status_of_Comple_AMD__c,CPAO_Subscription_Status_of_Comple_APF__c,CPAO_Subscription_Status_of_Comple_PAL__c, CPAO_CPA_Id_Display__c  from Contact where Id =: contactmembersIds];
        system.debug('condisplay '+condisplay );
            
        return condisplay;
    }
    
    public static List<Contact> cons = new List<Contact>();
       
    @AuraEnabled
    public static List<OrderApi__Receipt__c> savePayment (List<OrderApi__Receipt__c> acc){
        upsert acc;
        return acc;
    }
    

    @AuraEnabled 
    public static List<Contact> saveMarkedContacts(String idListJSONStr){
        Set<Contact> conSet = new Set<Contact>();
        System.debug('idListJSON = ' + idListJSONStr);
        Type idArrType=Type.forName('List<Id>');
        List<Contact> conList = new List<Contact>();
        List<Id> ids=(List<Id>) JSON.deserialize(idListJSONStr, idArrType);
        List<Contact> condetailsObj = [Select Id, name, CPAO_Subscription_Status_of_Comple_AMD__c,CPAO_Subscription_Status_of_Comple_APF__c,CPAO_Subscription_Status_of_Comple_PAL__c,CPAO_CPA_Contact_Type__c, CPAO_CPA_Id_Display__c from Contact where id in: ids ];
        for(integer i=0; i<condetailsObj.size(); i++){
            conSet.add(condetailsObj[i]);
        }

        conList.addAll(conSet);
        system.debug('conListtttt'+conList);

        return conList;
    }
   
    
}