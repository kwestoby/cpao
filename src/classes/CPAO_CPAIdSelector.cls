public with sharing class CPAO_CPAIdSelector {
	
	public static String UNUSED_STATUS = 'Unused';
	public static String ACTIVE_STATUS = 'Active';

	public static List<CPAO_CPA_Id__c> getUnusedIds() {
		return [SELECT Id, Id_Name__c, Status__c FROM CPAO_CPA_Id__c 
		        WHERE Status__c =:UNUSED_STATUS 
		        ORDER BY Id_Name__c];
	}

	public static List<CPAO_CPA_Id__c> getUnusedIdsExceptOldRange(Set<Decimal> currentIds) {
		return [SELECT Id, Id_Name__c, Status__c FROM CPAO_CPA_Id__c 
				WHERE Status__c =:UNUSED_STATUS AND Id_Name__c NOT IN :currentIds 
				ORDER BY Id_Name__c];
	}

	public static List<CPAO_CPA_Id__c> getExistingIdsInRange(Decimal rangeStart, Decimal rangeEnd) {
		return [SELECT Id, Id_Name__c, Status__c FROM CPAO_CPA_Id__c 
				WHERE Id_Name__c >= :rangeStart 
				AND Id_Name__c <= :rangeEnd];
	}

	public static List<CPAO_CPA_Id__c> getExistingIdsFromSet(Set<Decimal> currentIds) {
		return [SELECT Id, Id_Name__c, Status__c FROM CPAO_CPA_Id__c 
				WHERE Id_Name__c IN :currentIds 
				ORDER BY Id_Name__c];
	}

	public static List<CPAO_CPA_Id__c> getExistingIdsFromRecordSet(List<CPAO_CPA_Id__c> upsertedIds) {
		return [SELECT Id, Account__c, Contact__c ,Id_Name__c, Status__c FROM CPAO_CPA_Id__c
		        WHERE Status__c =:ACTIVE_STATUS AND Id IN :upsertedIds];
	}
}