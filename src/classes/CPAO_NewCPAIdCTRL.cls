public with sharing class CPAO_NewCPAIdCTRL {
	public static String invalidObjectIdType = 'The supplied record Id is not a valid type, please contact an administrator';
	@auraEnabled
	public static String handleNewIdButtonClicks(Id recordId) {
		String idObjectType = recordId.getSobjectType().getDescribe().getName();
		try{
			if(idObjectType == 'Contact'){
				CPAO_ContactHandler.handleNewIdButtonClicks(recordId);
				return null;
			} else if(idObjectType == 'Account'){
				CPAO_AccountHandler.handleNewIdButtonClicks(recordId);
				return null;
			} else {
				throw new CPAO_NewCPAIdCTRL_Exception(invalidObjectIdType);
			}

		} catch(Exception e){
			//throw new CPAO_NewCPAIdCTRL_Exception(e.getMessage());
			return e.getMessage();
		}
	}

	public class CPAO_NewCPAIdCTRL_Exception extends Exception {}
}