trigger CPAO_InvoiceTrigger on OrderApi__Invoice__c(after update) {
	if (!CPAO_Utilities.automation.OrderApi_Invoice_c__c) {
		return;
	}

	if(Trigger.isAfter && Trigger.isUpdate) {
		CPAO_InvoiceTriggerHandler.afterUpdateFilter(Trigger.new, Trigger.oldMap);
	}
}