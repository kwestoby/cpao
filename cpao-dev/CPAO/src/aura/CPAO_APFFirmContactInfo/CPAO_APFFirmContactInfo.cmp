<aura:component controller="CPAO_AnnualPractitionerFee_CTRL">

	<aura:attribute name="recordId" type="String" description="Firm's record id" />
    <aura:attribute name="isEdit" type="Boolean" default="false" />
    <aura:attribute name="account" type="Account" default="{}" />
    <aura:attribute name="currentAPF" type="Object" />
    <aura:attribute name="step" type="String" />
    <aura:attribute name="mailingPicklist" type="List" />

    <!-- Lookup Attributes that require change handlers -->
    <aura:attribute name="localOfficer" type="Object" />
    <aura:attribute name="inspectionClient" type="Object" />
    <aura:attribute name="disciplineContact" type="Object" />
    <aura:attribute name="disciplineContactAlt" type="Object" />

    <aura:registerEvent name="nextButtonClick" type="c:CPAO_Next_Button_Has_Been_Pressed"/>
    <aura:registerEvent name="prevButtonClick" type="c:CPAO_Prev_Button_Has_Been_Pressed"/>

    <aura:handler name="init" value="{!this}" action="{!c.doInit}" />
    <aura:handler name="change" value="{!v.localOfficer}" action="{!c.handleLocalOfficer}"
        description="When the lookup value changes, update the associated field" />
    <aura:handler name="change" value="{!v.inspectionClient}" action="{!c.handleInspectionClient}"
        description="When the lookup value changes, update the associated field" />
    <aura:handler name="change" value="{!v.disciplineContact}" action="{!c.handleDisciplineContact}"
        description="When the lookup value changes, update the associated field" />
    <aura:handler name="change" value="{!v.disciplineContactAlt}" action="{!c.handleDisciplineContactAlt}"
        description="When the lookup value changes, update the associated field" />

    2018 | Profile Information
    <hr></hr>

    <div>
        <h1>Firm Contact Information
            <div class="slds-text-align_right"><button class="slds-button slds-button_brand" onclick="{!c.onEdit}">Edit</button></div>
        </h1>
    </div>

    <div class="form">

        <lightning:input type="text" label="Name of Firm" value="{!v.account.Name}" class="{!not(v.isEdit) ? 'readonly' : ''}" readonly="{!not(v.isEdit)}" />

        <lightning:input type="email" label="Email" value="{!v.account.OrderApi__Account_Email__c}" class="{!not(v.isEdit) ? 'readonly' : ''}" readonly="{!not(v.isEdit)}" />

        <lightning:input type="tel" label="Phone" value="{!v.account.Phone}" class="{!not(v.isEdit) ? 'readonly' : ''}" readonly="{!not(v.isEdit)}" />

        <lightning:input type="url" label="Website" value="{!v.account.Website}" class="{!not(v.isEdit) ? 'readonly' : ''}" readonly="{!not(v.isEdit)}" />
        
        <!-- Preferred Mailing Address START -->
        <div class="pref-address">
            <lightning:select name="pref-address" 
                              label="Preferred Mailing Address"
                              value="{!v.account.CPAO_Preferred_Mailing_Address__c}" 
                              disabled="{!not(v.isEdit)}"
                              readonly="{!not(v.isEdit)}">
                <aura:iteration items="{!v.mailingPicklist}" var="item">
                    <aura:if isTrue="{!v.account.CPAO_Preferred_Mailing_Address__c == item.value}">
                        <option text="{!item.label}" value="{!item.value}" selected="true" />
                        <aura:set attribute="else">
                            <option text="{!item.label}" value="{!item.value}" />
                        </aura:set>
                    </aura:if>
                </aura:iteration>
            </lightning:select>
        </div>
        <!-- Preferred Mailing Address END -->

        <!-- Business Address START -->
        <lightning:inputAddress
            streetLabel="Business Street"
            cityLabel="Business City"
            provinceLabel="Business Province"
            postalCodeLabel="Business Postal Code"
            street="{!v.account.BillingStreet}"
            city="{!v.account.BillingCity}"
            province="{!v.account.BillingStateCode}"
            postalCode="{!v.account.BillingPostalCode}"
            readonly="{!not(v.isEdit)}"
            class="{!not(v.isEdit) ? 'readonly' : ''}"
        />
        <!-- Business Address END -->
        
        <!-- Other Address START -->
        <lightning:inputAddress
            streetLabel="Other Street"
            cityLabel="Other City"
            provinceLabel="Other Province"
            postalCodeLabel="Other Postal Code"
            street="{!v.account.ShippingStreet}"
            city="{!v.account.ShippingCity}"
            province="{!v.account.ShippingStateCode}"
            postalCode="{!v.account.ShippingPostalCode}"
            readonly="{!not(v.isEdit)}"
            class="{!not(v.isEdit) ? 'readonly' : ''}"
        />
        <!-- Other Address END -->

        <!-- Local Senior Officer START -->
        <div class="local-officer">
            <aura:if isTrue="{!not(v.isEdit)}">
                <lightning:input type="text" label="Local Senior Officer" value="{!v.account.CPAO_LSO__r.Name}" 
                        class="{!not(v.isEdit) ? 'readonly' : ''}" readonly="{!not(v.isEdit)}" />

                <aura:set attribute="else">
                    <div>
                        Local Senior Officer
                        <c:Lookup
                            objectName="Contact"
                            field_API_text="Name"
                            field_API_val="Id"
                            limit="6"
                            field_API_search="Name"
                            lookupIcon="standard:contact"
                            selItem="{!v.localOfficer}"
                            placeholder="{!v.account.CPAO_LSO__r.Name}" />
                    </div>
                </aura:set>
            </aura:if>
        </div>
        <!-- Local Senior Officer END -->

        <!-- Designated Admin START -->
        <div class="del-admin">
            <lightning:input type="text" label="Delegated Admin" value="{!v.account.CPAO_Designated_Administrator__c}" 
                class="{!not(v.isEdit) ? 'readonly' : ''}" readonly="{!not(v.isEdit)}" />
        </div>
        <!-- Designated Admin START -->

        <!-- Practice Inspection Client START -->
        <div class="prac-client">
            <aura:if isTrue="{!not(v.isEdit)}">
                <lightning:input type="text" label="Practice Inspection Contact" value="{!v.account.CPAO_Practice_Inspection_Contact__r.Name}" 
                        class="{!not(v.isEdit) ? 'readonly' : ''}" readonly="{!not(v.isEdit)}" />

                <aura:set attribute="else">
                    <div>
                        Practice Inspection Client
                        <c:Lookup
                            objectName="Contact"
                            field_API_text="Name"
                            field_API_val="Id"
                            limit="6"
                            field_API_search="Name"
                            lookupIcon="standard:contact" 
                            selItem="{!v.inspectionClient}" 
                            placeholder="{!v.account.CPAO_Practice_Inspection_Contact__r.Name}" />
                    </div>
                </aura:set>
            </aura:if>
        </div>
        <!-- Practice Inspection Client END -->

        <!-- Discipline Contact START -->
        <div class="discipline-contact">
            <aura:if isTrue="{!not(v.isEdit)}">
                <lightning:input type="text" label="Discipline Contact" value="{!v.account.CPAO_Discipline_Contact__r.Name}" 
                        class="{!not(v.isEdit) ? 'readonly' : ''}" readonly="{!not(v.isEdit)}" />

                <aura:set attribute="else">
                    <div>
                        Discipline Contact
                        <c:Lookup
                            objectName="Contact"
                            field_API_text="Name"
                            field_API_val="Id"
                            limit="6"
                            field_API_search="Name"
                            lookupIcon="standard:contact" 
                            selItem="{!v.disciplineContact}" 
                            placeholder="{!v.account.CPAO_Discipline_Contact__r.Name}" />
                    </div>
                </aura:set>
            </aura:if>
        </div>
        <!-- Discipline Contact END -->

        <!-- Discipline Contact Alternative START -->
        <div class="discipline-contact-alt">
            <aura:if isTrue="{!not(v.isEdit)}">
                <lightning:input type="text" label="Discipline Contact Alternative" value="{!v.account.CPAO_Discipline_Contact_Alternative__r.Name}" 
                        class="{!not(v.isEdit) ? 'readonly' : ''}" readonly="{!not(v.isEdit)}" />

                <aura:set attribute="else">
                    <div>
                        Discipline Contact Alternative
                        <c:Lookup
                            objectName="Contact"
                            field_API_text="Name"
                            field_API_val="Id"
                            limit="6"
                            field_API_search="Name"
                            lookupIcon="standard:contact" 
                            selItem="{!v.disciplineContactAlt}" 
                            placeholder="{!v.account.CPAO_Discipline_Contact_Alternative__r.Name}" />
                    </div>
                </aura:set>
            </aura:if>
        </div>
        <!-- Discipline Contact Alternative END -->
        
    </div>

    <div class="slds-float_right">
        <button class="slds-button slds-button--neutral" onclick="{!c.handlePrev}">Back</button>
        <button class="slds-button slds-button--brand" onclick="{!c.handleNext}">Save and Next</button>
        <button class="slds-button slds-button--destructive" onclick="{!c.handleSaveAndQuit}">Save and Quit</button>
    </div>
</aura:component>