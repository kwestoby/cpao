<?xml version="1.0" encoding="UTF-8"?>
<CustomMetadata xmlns="http://soap.sforce.com/2006/04/metadata" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xmlns:xsd="http://www.w3.org/2001/XMLSchema">
    <label>None</label>
    <protected>false</protected>
    <values>
        <field>AMD_or_CPD__c</field>
        <value xsi:type="xsd:string">AMD</value>
    </values>
    <values>
        <field>CPAO_Explanation__c</field>
        <value xsi:nil="true"/>
    </values>
    <values>
        <field>CPAO_Percent_Off__c</field>
        <value xsi:type="xsd:double">0.0</value>
    </values>
    <values>
        <field>Explanation_Third_Year__c</field>
        <value xsi:nil="true"/>
    </values>
    <values>
        <field>Full_Name__c</field>
        <value xsi:type="xsd:string">None</value>
    </values>
</CustomMetadata>
