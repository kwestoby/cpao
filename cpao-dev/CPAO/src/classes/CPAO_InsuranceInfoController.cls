public with sharing class CPAO_InsuranceInfoController {

	public static User LOGGED_USER = getLoggedUserInfo();
	public static List<Account> primaryContactAccounts = CPAO_AccountSelector.getprimaryContactAccounts(LOGGED_USER.ContactId);
	public static Map<String,Id> returnedIdsAndNames = new Map<String,Id>();

	@AuraEnabled
	public static User getLoggedUserInfo() {
		return CPAO_UserSelector.getCurrentUser();
	}

	//Accounts for which the logged user is a primary contact
	@AuraEnabled
	public static  List<CPAO_Insurance__c>  getCurrentInsurances() {		
		return CPAO_InsuranceSelector.getInsuranceByAccount(primaryContactAccounts);
	}

	//Accounts for which the logged user is a primary contact
	@AuraEnabled
	public static list<String>  getIdOfAccountsPrimary(){
		list<String> returnedAccountsName = new list<String>();
		for (Account account : primaryContactAccounts){
			returnedAccountsName.add(account.Name);
			returnedIdsAndNames.put(account.Name,account.Id);	
		}
		return returnedAccountsName;
	}


	@AuraEnabled
	public static  List<String>  getCoverageTypesOptions(){
		List<String> listOfOptions = new List<String>();
        Schema.DescribeFieldResult fieldResult = CPAO_Insurance__c.CPAO_Coverage_Type__c.getDescribe();
		List<Schema.PicklistEntry> pickListEntries = fieldResult.getPicklistValues();
		for (Schema.Picklistentry picklistEntry : pickListEntries){
			if(picklistEntry.isActive()){
				listOfOptions.add(pickListEntry.getLabel());
			}
		}
		return listOfOptions;
	}

	@AuraEnabled
	public static  List<String>  getStatusOptions(){
		List<String> listOfOptions = new List<String>();
        Schema.DescribeFieldResult fieldResult = CPAO_Insurance__c.CPAO_Insurance_Status__c.getDescribe();
		List<Schema.PicklistEntry> pickListEntries = fieldResult.getPicklistValues();
		for (Schema.Picklistentry picklistEntry : pickListEntries){
			if(picklistEntry.isActive()){
				listOfOptions.add(pickListEntry.getLabel());
			}
		}
		return listOfOptions;
	}

	@AuraEnabled
	public static  List<String>  getInsuranceProviders(){
		List<String> listOfOptions = new List<String>();
        Schema.DescribeFieldResult fieldResult = CPAO_Insurance__c.CPAO_Insurance_Provider__c.getDescribe();
		List<Schema.PicklistEntry> pickListEntries = fieldResult.getPicklistValues();
		for (Schema.Picklistentry picklistEntry : pickListEntries){
			if(picklistEntry.isActive()){
				listOfOptions.add(pickListEntry.getLabel());
			}
		}
		return listOfOptions;
	}

	@AuraEnabled
	public static  String  componentInit(){
		ResultWrapper response = new ResultWrapper();
		response.currentInsurances = getCurrentInsurances();
		response.coverageTypes = getCoverageTypesOptions();
		response.insuranceProviders = getInsuranceProviders();
		response.accountNames = getIdOfAccountsPrimary();
		response.accountNamesAndIds = returnedIdsAndNames;

	     return JSON.serialize(response);
	}

	@AuraEnabled
	public static  Sobject  saveNewInsurance(Sobject newInsurance){
		insert newInsurance;
		return newInsurance;
	}

	public class ResultWrapper{

        public List<CPAO_Insurance__c> currentInsurances {get;set;}
        public List<String> coverageTypes {get;set;}
        public List<String> insuranceProviders {get;set;}
        public list<String> accountNames {get;set;}
        public Map<String,Id> accountNamesAndIds {get;set;}
    }
}