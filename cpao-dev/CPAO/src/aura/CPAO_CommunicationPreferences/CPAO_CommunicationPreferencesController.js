({
	doInit : function(component, event, helper) {
		console.log('comm prefs start');
		var apexAction = component.get('c.getUserInfo');
        apexAction.setCallback(this, function(response) {
        	var state = response.getState();
        	if(state === "SUCCESS"){
        		console.log('donint success');
                var result = response.getReturnValue();

                component.set("v.contact", result);
                if(result.CPAO_Consent_type__c == 'Unsubscribe'){
                	component.set("v.setToYes", false);
                	component.find(result.CPAO_Consent_type__c).set("v.checked", true);
                }
                if(result.CPAO_Consent_type__c == 'Express Consent'){
                	component.set("v.setToYes", true);
                	component.find(result.CPAO_Consent_type__c).set("v.checked", true);
                }
            }
        });
        $A.enqueueAction(apexAction);
	},

	onRadioChange : function(component, event, helper) {
		var newVal = event.getSource().get('v.value');
		var contact = component.get("v.contact");
		contact.CPAO_Consent_type__c = newVal;
		if(newVal == 'Unsubscribe'){
			contact.CPAO_Member_news_and_information__c == false;
			contact.CPAO_Association_news__c == false;
			contact.CPAO_Professional_development_learning__c == false;
			contact.CPAO_Conferences__c == false;
			contact.CPAO_Networking_events__c == false;
			contact.CPAO_Volunteer_opportunities__c == false;
			contact.CPAO_Member_and_student_benefits__c == false;
			contact.CPAO_Exam_and_Course_information__c == false;

			component.find("CPAO_Member_news_and_information__c").set("v.checked", false);
			component.find("CPAO_Association_news__c").set("v.checked", false);
			component.find("CPAO_Professional_development_learning__c").set("v.checked", false);
			component.find("CPAO_Conferences__c").set("v.checked", false);
			component.find("CPAO_Networking_events__c").set("v.checked", false);
			component.find("CPAO_Volunteer_opportunities__c").set("v.checked", false);
			component.find("CPAO_Member_and_student_benefits__c").set("v.checked", false);
			component.find("CPAO_Exam_and_Course_information__c").set("v.checked", false);
			component.set("v.contact", contact);
			component.set("v.setToYes", false);
		} else {
			component.set("v.setToYes", true);
		}
		
	},

	save : function(component, event, helper) {
		var contact = component.get("v.contact");
		var apexAction = component.get('c.saveContactInfo');
		apexAction.setParams({
	    	"contact": contact
	    });
        apexAction.setCallback(this, function(response) {
        	var state = response.getState();
        	if(state === "SUCCESS"){
        		var result = response.getReturnValue();
        		console.log("success!!")
        		if(result == 'Success'){
        			component.set("v.isOpen", true);
		            component.set("v.modalBody", "Information successfully saved");
		            component.set("v.modalHeader", "Success");
        		} else {
        			component.set("v.isOpen", true);
		            component.set("v.modalBody", result);
		            component.set("v.modalHeader", "Error");
        		}        		
        	} else {
        		component.set("v.isOpen", true);
	            component.set("v.modalBody", "There has been an error saving your information, please contact an administrator");
	            component.set("v.modalHeader", "Error");
        	}
        });
        $A.enqueueAction(apexAction);
	},

	closeModel: function(component, event, helper) {
      // for Hide/Close Model,set the "isOpen" attribute to "Fasle"  
      component.set("v.isOpen", false);
    },

})