({
	doInit : function(component, event, helper) {
		var apexAction = component.get('c.getReadmissionInfo');
        apexAction.setCallback(this, function(response) {
            var state = response.getState();
            console.log('state');
            console.log(state);
            if(state === "SUCCESS"){
                var result = response.getReturnValue();
                component.set("v.application", result.currentReadmissionApp);
                component.set("v.communityHomePage", result.communityURL);
                component.set("v.refereeTemplateURL", result.refereeTemplateURL);
                component.set("v.cpdLogTemplateURL", result.cpdLogTemplateURL);
            }
        });
        $A.enqueueAction(apexAction);
	},

	handleNext : function(component,event,helper){
        var getselectedStep = component.get("v.selectedStep");
        console.log('getselectedStep: ');
        console.log(getselectedStep);
        if(getselectedStep == "step1"){
            component.set("v.selectedStep", "step2");
            component.set("v.canBack", true);
            
        } else if(getselectedStep == "step3"){
            console.log('before good character start');
            var goodCharacterLoadMethod = component.find('goodCharacter');
            goodCharacterLoadMethod.loadMethod();
            component.set("v.selectedStep", "step4");
            console.log('should proceed');
        } else {
            helper.showSpinner(component);
            var nextButtonClick = $A.get("e.c:CPAO_Next_Button_Has_Been_Pressed");
            nextButtonClick.setParams({
                "componentName" : getselectedStep,
                "nextStepName" : "Next"});
            nextButtonClick.fire();            
        }
    },

    saveAndExit : function(component,event,helper){
        var getselectedStep = component.get("v.selectedStep");
        if(getselectedStep == "step1" || getselectedStep == "step3"){
            var homepageURL = component.get("v.communityHomePage");
            window.location.href = homepageURL;
            return;
        } else {
            helper.showSpinner(component);
            var nextButtonClick = $A.get("e.c:CPAO_Next_Button_Has_Been_Pressed");
            nextButtonClick.setParams({
                "componentName" : getselectedStep,
                "nextStepName" : "Exit"});
            nextButtonClick.fire();
        }
    },

    applyResponseFromChildComponent : function(component,event,helper){
        var getselectedStep = component.get("v.selectedStep");

        helper.hideSpinner(component);
        var isNextOk = event.getParam("nextButtonClickeable");
        if(isNextOk == false){
            var modalBody = event.getParam("modalBody");
            var modalHeader = event.getParam("modalHeader");
            if(modalBody == undefined || modalBody == ''){
                return;
            }
            component.set("v.isOpen", true);
            component.set("v.modalBody", modalBody);
            component.set("v.modalHeader", modalHeader);
            return;
        }
        var action = event.getParam("action");
        if(action == "Exit"){
            var homepageURL = component.get("v.communityHomePage");
            window.location.href = homepageURL;
            return;
        }
        
        if(getselectedStep == "step2"){
            component.set("v.selectedStep", "step3");
        } else if(getselectedStep == "step4"){
            var cpdLoadMethod = component.find('cpdPage');
            cpdLoadMethod.loadMethod();
            component.set("v.selectedStep", "step5");
        } else if(getselectedStep == "step5"){
            var certificateLoadMethod = component.find('certificatePage');
            certificateLoadMethod.loadMethod();
            component.set("v.selectedStep", "step6");
        } else if(getselectedStep == "step6"){
            var certificateLoadMethod = component.find('5YearsPage');
            certificateLoadMethod.loadMethod();
            component.set("v.selectedStep", "step7");
        } else if(getselectedStep == "step7"){
            var reviewLoadMethod = component.find('reviewPage');
            reviewLoadMethod.loadMethod();
            component.set("v.selectedStep", "step8");
        } else if(getselectedStep == "step8"){
            console.log('you made it to the end');
        }
    },

    handlePrev : function(component,event,helper){
        var getselectedStep = component.get("v.selectedStep");
        if(getselectedStep == "step2"){
            component.set("v.selectedStep", "step1");
            component.set("v.canBack", false);
        } else if(getselectedStep == "step3"){
            component.set("v.selectedStep", "step2");
        } else if(getselectedStep == "step4"){
            component.set("v.selectedStep", "step3");
        } else if(getselectedStep == "step5"){
            component.set("v.selectedStep", "step4");
        } else if(getselectedStep == "step6"){
            component.set("v.selectedStep", "step5");
        } else if(getselectedStep == "step7"){
            component.set("v.selectedStep", "step6");
        } else if(getselectedStep == "step8"){
            component.set("v.selectedStep", "step7");
        }
    },
    closeModel: function(component, event, helper) {
      component.set("v.isOpen", false);
    },
})