<?xml version="1.0" encoding="UTF-8"?>
<CustomMetadata xmlns="http://soap.sforce.com/2006/04/metadata" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xmlns:xsd="http://www.w3.org/2001/XMLSchema">
    <label>Subscription Active Status</label>
    <protected>false</protected>
    <values>
        <field>CPAO_endDate__c</field>
        <value xsi:type="xsd:date">2019-03-31</value>
    </values>
    <values>
        <field>CPAO_startDate__c</field>
        <value xsi:type="xsd:date">2018-04-01</value>
    </values>
</CustomMetadata>
