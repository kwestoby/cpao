trigger CPAO_Venue on EventApi__Venue__c (after insert, after update) {

	CPAO_Automation_Control__c control = CPAO_Automation_Control__c.getOrgDefaults();
	if(!control.EventApi_Venue_c__c){
		return;
	}
	if(Trigger.isAfter && Trigger.isInsert ){
		CPAO_VenueHandler.insertFilter(Trigger.New);
	}

	if(Trigger.isAfter && Trigger.isUpdate){
		CPAO_VenueHandler.updateFilter(Trigger.New,Trigger.oldMap);
	}
	

}