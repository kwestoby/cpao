public class CPAO_ServiceRequest_CTRL {
	public static User LOGGED_USER =  CPAO_UserSelector.getCurrentUser();

	@auraEnabled
	public static Id componentInit (){
		Id contactID = LOGGED_USER.Id;
		return contactID;//CPAO_ContactSelector.getContactRecords(new set<Id>{LOGGED_USER.ContactId})[0];
	}
}