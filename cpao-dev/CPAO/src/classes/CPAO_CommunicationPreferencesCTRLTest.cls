/**
* @File Name    :   CPAO_CommunicationPreferencesCTRLTest
* @Description  :   CPAO Communication Preferences Controller Test
* @Date Created :   2018-07-17
* @Author       :   Michaell Reis Gasparini, Deloitte, mreisg@deloitte.com
* @group        :   Class
* @Modification Log:
**************************************************************************************
* Ver       Date        Author              Modification
* 1.0       2018-07-17  Michaell Reis       Created the file/class
**/
@isTest
private class CPAO_CommunicationPreferencesCTRLTest {
	
	@testSetup
	static void dataInit(){
		
		
		CPAO_TestFactory.automationControlSetup();
		
		List<Account> accounts = CPAO_TestFactory.createAccounts(1);
		Database.insert(accounts);

		List<Contact> contacts = CPAO_TestFactory.createContacts(accounts[0],1);
		Database.insert(contacts);


	}
	
	@isTest static void callGetUserInfo() {
		
		try {
			CPAO_CommunicationPreferencesCTRL.getUserInfo();
			} catch(Exception e) {

			}

		}


		@isTest static void callSaveContactInfo() {

			Contact con = [Select id from Contact];

			CPAO_CommunicationPreferencesCTRL.saveContactInfo(con);
		}

		@isTest static void callWrongSaveContactInfo() {


			try {
				Contact con = new Contact();

				CPAO_CommunicationPreferencesCTRL.saveContactInfo(con);

				} catch( Exception e ) {
					System.assert( e.getMessage().length()>0	);
				}

			}


		}