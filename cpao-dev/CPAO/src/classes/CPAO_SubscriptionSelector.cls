public class CPAO_SubscriptionSelector {

	public static List<OrderApi__Subscription__c> getSubscriptionsWithLatestTerm(Set<Id> subcriptionIds) {
		return[SELECT Id, Name, CPAO_Expiry_Date__c, CPAO_Effective_Date__c, OrderApi__Is_Active__c,
		OrderApi__Contact__c, OrderApi__Activated_Date__c, OrderApi__Cancelled_Date__c, OrderApi__Status__c,
		OrderApi__Current_Term_Start_Date__c, OrderApi__Current_Term_End_Date__c,
		OrderApi__Subscription_Plan__c, OrderApi__Subscription_Plan__r.OrderApi__Grace_Period__c,
		(SELECT Id, OrderApi__Is_Active__c, OrderApi__Term_Start_Date__c, OrderApi__Term_End_Date__c
		 FROM OrderApi__Renewals__r ORDER BY OrderApi__Term_Start_Date__c DESC, OrderApi__Term_End_Date__c DESC LIMIT 1)
		FROM OrderApi__Subscription__c
		WHERE Id IN : subcriptionIds];
	}
}