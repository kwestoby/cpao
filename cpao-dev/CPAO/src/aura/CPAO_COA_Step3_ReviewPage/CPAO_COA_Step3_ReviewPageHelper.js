({
    upsertCOA: function(component){
        
        var currentCOA=component.get('v.currentCOA');
        var action = component.get("c.upsertCOAApplication");
        //alert(component.get('v.currentCOA'));
        action.setParams({ "COAAppToAdd" :currentCOA});
        action.setCallback(this, function(response){
            var state = response.getState();
            
            if (state === "SUCCESS") {
                alert('Thank you for submitting your COA app. \n This is a Placeholder for payment.\n Application now has status "submitted"');
                window.location.href='/CPAO_ApplicationDashboard';
                
            }else{
                console.log('error while adding COA');
                var error= response.getError();
                console.log(error[0]);
            }
        });
        
        $A.enqueueAction(action);
    },
    
    
})