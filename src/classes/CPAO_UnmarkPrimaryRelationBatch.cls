global class CPAO_UnmarkPrimaryRelationBatch implements Database.Batchable<sObject> {
	
	global Database.QueryLocator start(Database.BatchableContext BC) {
		String query = 'SELECT Id FROM CPAO_Account_Contact_Relationship__c WHERE CPAO_End_Date__c<TODAY AND CPAO_Primary_Employer__c = \'Yes\'';
		return Database.getQueryLocator(query);
	}

   	global void execute(Database.BatchableContext BC, List<CPAO_Account_Contact_Relationship__c> relationships) {
   		for(CPAO_Account_Contact_Relationship__c rel:relationships){
   			rel.CPAO_Primary_Employer__c = 'No';
   		}
   		update relationships;
	}
	
	global void finish(Database.BatchableContext BC) {
		
	}
	
}