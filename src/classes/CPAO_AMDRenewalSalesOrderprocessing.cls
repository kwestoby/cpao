public without sharing class CPAO_AMDRenewalSalesOrderprocessing {
	public static final String SUCCESS = 'Success';
	public static final String SUBMITTED = 'Submitted';
	public static final String SUBMITTED_DEFERRED = 'Submitted-Payment Deferred';
	public static final String COMPLETED = 'Completed';
	public static final String AMD_CANADA_ITEM = 'Annual Membership Dues - CPA Canada';
	public static final String AMD_ONTARIO_ITEM = 'Annual Membership Dues - CPA Ontario';
	public static final String CPD_LATE_FEE = 'CPD Late Fee';
	public static final String AMD_LATE_FEE = 'AMD Late Fee';
	
	//Loads up information needed for the AMD/CPD process review page
	//the following items are needed: the amd/cpd application, current user contact, and contact relationships
	//Queries for primary employers as well because there is logic needed to determine whether or not the user can ask
	//	for the employer to pay
	@AuraEnabled
	public static ReviewPageWrapper loadReviewInfo() {
		ReviewPageWrapper results = new ReviewPageWrapper();
		List<CPAO_Application__c> currentAMDCPDApplication = CPAO_ApplicationSelector.getUserApplicationsByRecordTypeNameAndStatus(
			CPAO_AMDRenewalContainerCTRL.OPEN, CPAO_ApplicationHandler.AMD_CPD);
		if(!currentAMDCPDApplication.isEmpty()){
			results.currentAMDCPDApplication = currentAMDCPDApplication[0];
		} else {
			return null;
		}

		User current = CPAO_UserSelector.getCurrentUser();
		results.userContact = CPAO_ContactSelector.getContactToUpdate(new Set<Id>{current.ContactId})[0];
		results.poaItems = CPAO_PlanOfActionSelector.getPOAsByApplicationId(currentAMDCPDApplication[0].Id);
		results.contactEmploymentRecords = CPAO_AccountContactRelationSelector.getRelationshipsByContactIdAndStatus(
			results.userContact.ID, CPAO_AMDRenewalContainerCTRL.ACTIVE);

		List<CPAO_Account_Contact_Relationship__c> primaryEmployers = CPAO_AccountContactRelationSelector.getPrimaryRelationshipsByContactId(
			new Set<Id>{results.userContact.Id});
		results.canEmployerPay = false;
		if(primaryEmployers.size()>0){
			if(results.currentAMDCPDApplication.CPAO_Sub_Type__c.contains('AMD')){
				results.canEmployerPay = true;
			}
		}
		return results;
	}


	//Method is called at the end of the AMD/CPD application flow (or when the user wants to pay a deferred application)

	@AuraEnabled
	public static attestationWrapper saveApplicationAttestation(CPAO_Application__c currentAMDCPDApplication) {
		attestationWrapper results = new attestationWrapper();

		//The following Bool values determine if the user must pay a late fee
		Application_Date__mdt dates = CPAO_MetadataTypeSelector.getApplicationDates()[0];
		Boolean applyCpdLateFee = false;
		Boolean applyAmdLateFee = false;
		
		if(currentAMDCPDApplication.CPAO_Sub_Type__c.contains('CPD') && pastLateFeeDate(dates.CPD_Late_Fee_Date__c)){
			applyCpdLateFee = true;
		}
		if(currentAMDCPDApplication.CPAO_Sub_Type__c.contains('AMD') && pastLateFeeDate(dates.AMD_Late_Fee_Date__c)){
			applyAmdLateFee = true;
		}

		//Most Subscriptions will go through the following logic. It is skipped when the user wants to pay for a 
		//deferred payment appliaction
		if(currentAMDCPDApplication.CPAO_Status__c != SUBMITTED_DEFERRED){
			Contact applicationContact = new Contact(
				Id = currentAMDCPDApplication.CPAO_Contact__c, 
				CPAO_Employer_is_paying_AMD__c = currentAMDCPDApplication.CPAO_Employer_is_paying_AMD__c);
			try{
				update applicationContact;
			} catch(Exception e) {
				results.msg = e.getMessage();
				results.status = 'Error';
				return results;
			}

			//following Bool values determine whether or not the user has payments to make, or gets directed to the thank you page
			// if sendToThankYouPage is false, then user has to pay to continue with the application
			Boolean sendToThankYouPage = false;
			Boolean isComplete = false;
			if(currentAMDCPDApplication.CPAO_Employer_is_paying_AMD__c == 'Yes' || 
			currentAMDCPDApplication.CPAO_Applied_for_Deferral__c == 'Yes'){
				sendToThankYouPage = true;
			}
			if(!currentAMDCPDApplication.CPAO_Sub_Type__c.contains('AMD') && !applyCpdLateFee){
				sendToThankYouPage = true;
				isComplete = true;
			}
			Boolean isDeferred = (currentAMDCPDApplication.CPAO_Applied_for_Deferral__c == 'Yes'? true:false);
			if(sendToThankYouPage){
				if(isDeferred){
					currentAMDCPDApplication.CPAO_Status__c = SUBMITTED_DEFERRED;
				} else if(isComplete){
					currentAMDCPDApplication.CPAO_Status__c = COMPLETED;
				} else {
					currentAMDCPDApplication.CPAO_Status__c = SUBMITTED;
				}
				
				try{
					update currentAMDCPDApplication;
				} catch(Exception e) {
					results.msg = e.getMessage();
					results.status = 'Error';
					return results;
				}
				results.status = SUCCESS;
				results.checkOutScreenURL = Label.Community_URL;
				return results;
			} else {
				try{
					update currentAMDCPDApplication;
				} catch(Exception e) {
					results.msg = e.getMessage();
					results.status = 'Error';
					return results;
				}
			}
		}

		//Checks for the current users AMD Subscription record. if 1 is not found (or if there are more than 1), 
		// then this part is skipped and nothing happens
		List<OrderApi__Subscription__c> subscription = 
		CPAO_FontevaObjectSelector.getContactSubscription('AMD', currentAMDCPDApplication.CPAO_Contact__c);
		system.debug('subscription.size(): ' + subscription.size());
		system.debug('currentAMDCPDApplication.CPAO_Sub_Type__c.: ' + currentAMDCPDApplication.CPAO_Sub_Type__c.contains('AMD'));
		
		if(subscription.size() == 1 && currentAMDCPDApplication.CPAO_Sub_Type__c.contains('AMD')){
			OrderApi__Sales_Order__c salesOrderToInsert = new OrderApi__Sales_Order__c(
				OrderApi__Contact__c = currentAMDCPDApplication.CPAO_Contact__c,
				OrderApi__Entity__c = 'Contact');
			try{
				insert salesOrderToInsert;
			} catch(Exception e) {
				results.msg = e.getMessage();
				results.status = 'Error';
				return results;
			}
			
			List<OrderApi__Sales_Order_Line__c> salesOrderLineItemsToInsert = new List<OrderApi__Sales_Order_Line__c>();
			Decimal waiverDiscount = 0;
			Decimal resignationDiscount = 0;
			if(currentAMDCPDApplication.CPAO_AMD_Waiver__c != null){
				List<CPAO_Waiver_Explanation__mdt> waiverInfo = 
					CPAO_MetadataTypeSelector.getWaiverExplanation(currentAMDCPDApplication.CPAO_AMD_Waiver__c);
				waiverDiscount = waiverInfo[0].CPAO_Percent_Off__c;
			}
			User currentUser = CPAO_UserSelector.getCurrentUser();
			if(currentUser.Contact.CPAO_Resignation_Discount_Percentage__c != null){
				resignationDiscount = currentUser.Contact.CPAO_Resignation_Discount_Percentage__c;
			}

			List<Annual_Membership_Due_Line__mdt> membershipDues = 
				CPAO_MetadataTypeSelector.getAMDLine(currentAMDCPDApplication.CPAO_AMD_Location_Based_Reduction__c);

			Decimal totalPrice = 0;

			for(OrderApi__Item_Subscription_Plan__c item:CPAO_FontevaObjectSelector.getSubscriptionPlanItems('AMD')){
				OrderApi__Sales_Order_Line__c soliToInsert = new OrderApi__Sales_Order_Line__c(
					OrderApi__Sales_Order__c = salesOrderToInsert.Id,
					OrderApi__Item__c = item.OrderApi__Item__r.Id);

				if(item.OrderApi__Item__r.Name == AMD_CANADA_ITEM){
					soliToInsert.OrderApi__Price_Override__c = true;
					Decimal price = getPrice(membershipDues[0].CPA_Canada_Fee__c, waiverDiscount, resignationDiscount);
					soliToInsert.OrderApi__Sale_Price__c = price;
					totalPrice += price;
				}
				if(item.OrderApi__Item__r.Name == AMD_ONTARIO_ITEM){
					soliToInsert.OrderApi__Price_Override__c = true;
					Decimal price = getPrice(membershipDues[0].CPA_Ontario_Fee__c, waiverDiscount, resignationDiscount);
					soliToInsert.OrderApi__Sale_Price__c = price;
					totalPrice += price;

					soliToInsert.OrderApi__Subscription__c = subscription[0].Id;
					soliToInsert.OrderApi__Subscription_Plan__c = subscription[0].OrderApi__Subscription_Plan__c;
					soliToInsert.OrderApi__Is_Renewal__c = true;
				}
				salesOrderLineItemsToInsert.add(soliToInsert);
			}

			if(applyCpdLateFee || applyAmdLateFee){
				integer lateFee = 0;
				if(totalPrice < 100){
					lateFee = 25;
				} else if(totalPrice < 500){
					lateFee = 45;
				} else {
					lateFee = 100;
				}
				for(OrderApi__Item__c item:CPAO_FontevaObjectSelector.getLateFeeItems()){
					if(applyCpdLateFee && item.Name == CPD_LATE_FEE){
						salesOrderLineItemsToInsert.add(new OrderApi__Sales_Order_Line__c(
							OrderApi__Sales_Order__c = salesOrderToInsert.Id,
							OrderApi__Item__c = item.Id,
							OrderApi__Price_Override__c = true,
							OrderApi__Sale_Price__c = lateFee));
					}
					if(applyAmdLateFee && item.Name == AMD_LATE_FEE){
						salesOrderLineItemsToInsert.add(new OrderApi__Sales_Order_Line__c(
							OrderApi__Sales_Order__c = salesOrderToInsert.Id,
							OrderApi__Item__c = item.Id,
							OrderApi__Price_Override__c = true,
							OrderApi__Sale_Price__c = lateFee));
					}
				}
			}
			
			try{
				insert salesOrderLineItemsToInsert;				
			} catch(Exception e) {
				results.msg = e.getMessage();
				results.status = 'Error';
				return results;
			}

			if(currentAMDCPDApplication.CPAO_AMD_Zero_Tax_Certificate__c == 'Yes'){
				
				List<OrderApi__Sales_Order_Line__c> taxToUpdate = new List<OrderApi__Sales_Order_Line__c>();
				for(OrderApi__Sales_Order_Line__c taxItem:CPAO_FontevaObjectSelector.getSalesOrderTaxItems(salesOrderToInsert.Id)){
					taxItem.OrderApi__Tax_Override__c = true;
					taxItem.OrderApi__Tax_Percent__c = 0;
					taxToUpdate.add(taxItem);
				}
				if(!taxToUpdate.isEmpty()){
					update taxToUpdate;
				}
			}
			results.status = SUCCESS;
			results.checkOutScreenURL = Label.Community_URL + '/' + Label.Checkout_Screen_Name + salesOrderToInsert.Id;
			return results;			
		}
		results.status = 'Error';
		results.msg = 'In order to proceed, there must be only 1 AMD subscription under your name';
		return results;

	}

	public static Decimal getPrice(Decimal fee, Decimal waiverDiscount, Decimal resignationDiscount){
		if(waiverDiscount == 1 || resignationDiscount == 1){
			return 0;
		}
		
		fee = fee - (fee*waiverDiscount/100);
		fee = fee - (fee*resignationDiscount/100);
		return fee;
	}

	public static Boolean pastLateFeeDate(Date lateFeeDate) {
		if(Date.today() > Date.newInstance(Date.today().year(), lateFeeDate.month(), lateFeeDate.day())){
			return true;
		} else {
			return false;
		}
	}


	public class attestationWrapper{
		@AuraEnabled public String msg{get;set;}
		@AuraEnabled public String status{get;set;}
		@AuraEnabled public String checkOutScreenURL{get;set;}
	}	

	public class ReviewPageWrapper{
		@AuraEnabled public CPAO_Application__c currentAMDCPDApplication{get;set;}
		@AuraEnabled public Contact userContact{get;set;}
		@AuraEnabled public List<Plan_of_Action_Items__c> poaItems{get;set;}
		@AuraEnabled public List<CPAO_Account_Contact_Relationship__c> contactEmploymentRecords{get;set;}
		@AuraEnabled public Boolean canEmployerPay{get;set;}
	}
}