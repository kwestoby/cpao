@isTest 
public class CPAO_AutoRenewWaiverAmdSubsBatchTest {

    public static OrderApi__Subscription__c subscription;
    public static List<Contact> contacts;
    public static List<Account> accounts;
    public static OrderApi__Renewal__c term;


    public static void dataInitialize() {
        accounts = CPAO_TestFactory.createAccounts(2);
        accounts[1].Name = 'Retirement';
        insert accounts;
        contacts = CPAO_TestFactory.createContacts(accounts[0], 2);
        contacts[0].CPAO_CPD_Continuous_Exemption__c='Medical Leave';
        
        insert contacts;
        subscription = CPAO_FontevaTestFactory.createSubscription();
        subscription.OrderApi__Contact__c = contacts[0].Id;
        subscription.CPAO_Effective_Date__c=Date.today();
        insert subscription;
         term = new OrderApi__Renewal__c();
        term.OrderApi__Subscription__c = subscription.Id;
        term.OrderApi__Term_Start_Date__c=Date.today();
        term.CPAO_Hide_Term__c = false;
        insert term;

    }

    @isTest static void testExecuteMethod() {
        dataInitialize();
        Test.startTest();
        CPAO_AutoRenewWaiverAmdSubsBatch batchJob = new CPAO_AutoRenewWaiverAmdSubsBatch();
        database.executebatch(batchJob);
        Test.stopTest();
        List<OrderApi__Renewal__c> result = [SELECT Id,CPAO_Hide_Term__c FROM OrderApi__Renewal__c ];
        System.assertEquals(result[0].CPAO_Hide_Term__c,false);
    }

    @isTest static void testExecuteMethod2() {
        dataInitialize();
        contacts[1].CPAO_CPD_Continuous_Exemption__c=null;
        subscription.OrderApi__Contact__c = contacts[1].Id;
        update subscription;
        Test.startTest();
        CPAO_AutoRenewWaiverAmdSubsBatch batchJob = new CPAO_AutoRenewWaiverAmdSubsBatch();
        database.executebatch(batchJob);
        Test.stopTest();
        List<OrderApi__Renewal__c> result = [SELECT Id,CPAO_Hide_Term__c FROM OrderApi__Renewal__c ];
        System.assertEquals(result[0].CPAO_Hide_Term__c,false);
    }



}