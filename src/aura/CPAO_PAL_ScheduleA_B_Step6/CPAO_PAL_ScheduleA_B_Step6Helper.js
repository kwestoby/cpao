({
    upsertPAL : function(component) {
        var action = component.get("c.upsertPalApplication");   
        action.setParams({ "PALappToAdd" :component.get('v.currentPAL') });
        action.setCallback(this, function(response){
            console.log('callback');
            var state = response.getState();
            if (state === "SUCCESS") {
                console.log('PAL added');
                if(component.get('v.shouldSaveAndQuit')){
                    window.location.href='/CPAO_ApplicationDashboard';
                    
                }else{
                    component.set('v.currentPAL',response.getReturnValue());
                    var applicationHasChanged = component.getEvent("applicationHasChanged");
                    applicationHasChanged.setParams({
                        "currentApplication": component.get("v.currentPAL"),
                        "nextButtonClickeable": "true"
                    });
                    applicationHasChanged.fire();
                }
                
            }else{
                console.log('error while adding Pal');
                
            }
        });
        
        $A.enqueueAction(action);
        
        
    },
    upload: function(component) {  // refactor this method to create  a method for file wrapper generation (will reduce line of code)
        if(component.get('v.currentPAL.CPAO_Basis_Of_Application__c')==component.get('v.activeLicenceWithOtherBody')){
            var oldUploadId_B=component.get('v.currentPAL.CPAO_Schedule_B_File_Id__c');
            var fileContents_B=component.get('v.fileContents_B');
            var file_B=component.get('v.fileInput_B');
            var oldUploadId_A=component.get('v.currentPAL.CPAO_Schedule_A_File_Id__c');
            var oldUploadId_C=component.get('v.currentPAL.CPAO_Schedule_C_File_Id__c');
            
            if(file_B==null && oldUploadId_B!=null){ 
                //alert('no new B to upload');
                if(oldUploadId_A!= null || oldUploadId_C!=null ){
                    //  alert('remving');
                    
                    
                    this.removeScheduleA_C(component);
                }else{
                    /* component.set('v.fileInput_B',null);
                    component.set('v.fileContents_B',null);*/
                    if(component.get('v.shouldSaveAndQuit')){
                        window.location.href='/CPAO_ApplicationDashboard';
                        
                    }else{
                        var applicationHasChanged = component.getEvent("applicationHasChanged");
                        applicationHasChanged.setParams({
                            "currentApplication": component.get("v.currentPAL"),
                            "nextButtonClickeable": "true"
                        });
                        applicationHasChanged.fire();
                    }
                }
                
            }else if(file_B!=null){ //If new upload of schedule A
                //alert(' new B to upload');
                
                
                var listOfFiles=component.get('v.fileWrapper');
                var fileWrapperB =listOfFiles[0];
                fileWrapperB.parentId=component.get("v.currentPAL.Id");
                fileWrapperB.fileName=component.get("v.fileName_B");
                fileWrapperB.base64Data=encodeURIComponent(fileContents);
                fileWrapperB.contentType=component.get("v.fileType_B");
                fileWrapperB.fileTitle=component.get("v.fileTitle_B");
                fileWrapperB.idFileToReplace=oldUploadId_B;
                component.set('v.fileWrapper',listOfFiles);
                var action = component.get("c.saveMultipleFiles"); 
                action.setParams({
                    "ScheduleA_B_Files": JSON.stringify(listOfFiles)
                });
                
                action.setCallback(this, function(response) {
                    var state = response.getState();
                    if (state === "SUCCESS") {
                        /*var k=response.getReturnValue()[0].base64Data;
                              alert('yesy'+(k==fileContents));*/
                          var idInsertedFile=response.getReturnValue()[0].fileId;
                          var nameInsertedFile=response.getReturnValue()[0].fileName;
                          component.set('v.currentPAL.CPAO_Schedule_B_File_Id__c',idInsertedFile);
                          component.set('v.currentPAL.CPAO_Schedule_B_File_Name__c',nameInsertedFile);
                          
                          
                          
                          if(oldUploadId_A!= null || oldUploadId_C!=null ){
                              
                              this.removeScheduleA_C(component);
                          }else{
                              this.upsertPAL(component);
                          }
                          
                          
                      }else{
                          //alert('eror WHILE ADDING B');
                          var errors=response.getError();
                          var message = errors[0];
                          console.log(message);
                          
                      }
                      
                  });
                  $A.enqueueAction(action); 
              }
            
        }else{
            if (component.get('v.currentPAL.CPAO_Schedule_B_File_Id__c')!=null){
                this.removeScheduleB_or_C(component,'v.currentPAL.CPAO_Schedule_B_File_Id__c','v.currentPAL.CPAO_Schedule_B_File_Name__c','B');  
                
            }
            
            var oldUploadId_A=component.get('v.currentPAL.CPAO_Schedule_A_File_Id__c');
            var fileContents=component.get('v.fileContents_A');
            var fileContents_C=component.get('v.fileContents_C');
            var oldUploadId_C=component.get('v.currentPAL.CPAO_Schedule_C_File_Id__c');
            var file_A=component.get('v.fileInput_A');
            var file_C=component.get('v.fileInput_C');
            var postDesignationCompleted=component.get('v.currentPAL.CPAO_completedPostDesignationPAProgram__c');
            //alert(postDesignationCompleted);
            //if(component.get('v.postDesignationCompleted')=='Yes'){
            if(postDesignationCompleted=='Yes'){
                
                // alert('Have you successfully completed your Post Designation Public Accounting Program? :yes');
                if(file_A==null && oldUploadId_A!=null && file_C==null && oldUploadId_C!=null){ // nothing new to upload
                    // alert(' nothing new to upload');
                    if(component.get('v.shouldSaveAndQuit')){
                        window.location.href='/CPAO_ApplicationDashboard';
                        
                    }else{
                        var applicationHasChanged = component.getEvent("applicationHasChanged");
                        applicationHasChanged.setParams({
                            "currentApplication": component.get("v.currentPAL"),
                            "nextButtonClickeable": "true"
                        });
                        applicationHasChanged.fire();
                    }
                }else if(file_A==null && oldUploadId_A!=null && file_C!=null){   
                    // alert('nothing to reupload for A but change C file');
                    var listOfFiles=component.get('v.fileWrapper');
                    var fileWrapperC =listOfFiles[0];
                    fileWrapperC.parentId=component.get("v.currentPAL.Id");
                    fileWrapperC.fileName=component.get("v.fileName_C");
                    fileWrapperC.base64Data=encodeURIComponent(fileContents_C);
                    fileWrapperC.contentType=component.get("v.fileType_C");
                    fileWrapperC.fileTitle=component.get("v.fileTitle_C");
                    fileWrapperC.idFileToReplace=oldUploadId_C;
                    component.set('v.fileWrapper',listOfFiles);
                    var action = component.get("c.saveMultipleFiles"); 
                    action.setParams({
                        "ScheduleA_B_Files": JSON.stringify(listOfFiles)
                    });
                    
                    action.setCallback(this, function(response) {
                        var state = response.getState();
                        if (state === "SUCCESS") {
                            /*var k=response.getReturnValue()[0].base64Data;
                                      alert('yesy'+(k==fileContents));*/
                          var idInsertedFile=response.getReturnValue()[0].fileId;
                          var nameInsertedFile=response.getReturnValue()[0].fileName;
                          component.set('v.currentPAL.CPAO_Schedule_C_File_Id__c',idInsertedFile);
                          component.set('v.currentPAL.CPAO_Schedule_C_File_Name__c',nameInsertedFile);
                          this.upsertPAL(component);
                          
                          
                          
                      }else{
                          // alert('eror');
                          var errors=response.getError();
                          var message = errors[0];
                          console.log(message);
                          
                      }
                      
                  });
                  $A.enqueueAction(action);
              }else if(file_C==null && oldUploadId_C!=null && file_A!=null){   
                  // alert('nothing to reupload for C but change A file'); 
                  var listOfFiles=component.get('v.fileWrapper');
                  var fileWrapperA =listOfFiles[0];
                  fileWrapperA.parentId=component.get("v.currentPAL.Id");
                  fileWrapperA.fileName=component.get("v.fileName_A");
                  fileWrapperA.base64Data=encodeURIComponent(fileContents);
                  fileWrapperA.contentType=component.get("v.fileType_A");
                  fileWrapperA.fileTitle=component.get("v.fileTitle_A");
                  fileWrapperA.idFileToReplace=oldUploadId_A;
                  component.set('v.fileWrapper',listOfFiles);
                  var action = component.get("c.saveMultipleFiles"); 
                  action.setParams({
                      "ScheduleA_B_Files": JSON.stringify(listOfFiles)
                  });
                  
                  action.setCallback(this, function(response) {
                      var state = response.getState();
                      if (state === "SUCCESS") {
                          /*var k=response.getReturnValue()[0].base64Data;
                                      alert('yesy'+(k==fileContents));*/
                                      var idInsertedFile=response.getReturnValue()[0].fileId;
                                      var nameInsertedFile=response.getReturnValue()[0].fileName;
                                      component.set('v.currentPAL.CPAO_Schedule_A_File_Id__c',idInsertedFile);
                                      component.set('v.currentPAL.CPAO_Schedule_A_File_Name__c',nameInsertedFile);
                                      this.upsertPAL(component);
                                      
                                      
                                  }else{
                                      //  alert('eror');
                                      var errors=response.getError();
                                      var message = errors[0];
                                      console.log(message);
                                      
                                  }
                                  
                              });
                              $A.enqueueAction(action);
                              
                              
                          }else if(file_A!=null &&  file_C!=null){
                              // alert(' new A and B to upload');
                              var listOfFiles=component.get('v.fileWrapper');
                              var fileWrapperA =listOfFiles[0];
                              fileWrapperA.parentId=component.get("v.currentPAL.Id");
                              fileWrapperA.fileName=component.get("v.fileName_A");
                              fileWrapperA.base64Data=encodeURIComponent(fileContents);
                              fileWrapperA.contentType=component.get("v.fileType_A");
                              fileWrapperA.fileTitle=component.get("v.fileTitle_A");
                              fileWrapperA.idFileToReplace=oldUploadId_A;
                              
                              var fileWrapperC={};
                              fileWrapperC.parentId=component.get("v.currentPAL.Id");
                              fileWrapperC.fileName=component.get("v.fileName_C");
                              fileWrapperC.base64Data=encodeURIComponent(fileContents_C);
                              fileWrapperC.contentType=component.get("v.fileType_C");
                              fileWrapperC.fileTitle=component.get("v.fileTitle_C");
                              fileWrapperC.idFileToReplace=oldUploadId_C;
                              listOfFiles.push(fileWrapperC);
                              component.set('v.fileWrapper',listOfFiles);
                              
                              var action = component.get("c.saveMultipleFiles"); 
                              action.setParams({
                                  "ScheduleA_B_Files": JSON.stringify(listOfFiles)
                              });
                              action.setCallback(this, function(response) {
                                  var state = response.getState();
                                  if (state === "SUCCESS") {
                                      var insertedFiles=response.getReturnValue();
                                      component.set('v.currentPAL.CPAO_Schedule_A_File_Id__c',insertedFiles[0].fileId);
                                      component.set('v.currentPAL.CPAO_Schedule_A_File_Name__c',insertedFiles[0].fileName);
                                      component.set('v.currentPAL.CPAO_Schedule_C_File_Id__c',insertedFiles[1].fileId);
                                      component.set('v.currentPAL.CPAO_Schedule_C_File_Name__c',insertedFiles[1].fileName);
                                      console.log(JSON.stringify(component.get('v.currentPAL')));
                                      this.upsertPAL(component);
                                      
                                  }else{
                                      // alert('eror while adding A and B');
                                      var errors=response.getError();
                                      var message = errors[0];
                                      console.log(message);
                                      
                                  }
                                  
                              });
                              $A.enqueueAction(action); 
                              
                          }
              
              
              
          }else if(postDesignationCompleted=='No'){
              
              // }else if(component.get('v.postDesignationCompleted')=='No'){
              // alert('Have you successfully completed your Post Designation Public Accounting Program? : no');
              if(file_A==null && oldUploadId_A!=null){  //If no new upload but previously uploaded a schedule A
                  //  alert('no new A to upload');
                  if(oldUploadId_C!=null){ // TO TEST ONCE I FINISH SCHEDULE C FUNCTIONALITY !!!
                      //  alert('rmove old C');
                      this.removeScheduleB_or_C(component,'v.currentPAL.CPAO_Schedule_C_File_Id__c','v.currentPAL.CPAO_Schedule_C_File_Name__c','C');
                      
                      
                      
                  }else if(oldUploadId_C==null){ // TO TEST ONCE I FINISH SCHEDULE C FUNCTIONALITY !!!
                      if(file_C!=null){
                          component.set('v.fileContents_C',null);
                          component.set('v.fileInput_C',null);
                          component.set('v.fileName_C','');
                          component.set('v.fileType_C','');
                          component.set('v.fileTitle_C','');
                      } //No need to call server so no upsert, return same PAL application to main component
                      // ( Should I call server to update postDesignationCompleted to no or delay till next step to limit server call?  )
                      
                      if(component.get('v.shouldSaveAndQuit')){
                          window.location.href='/CPAO_ApplicationDashboard';
                          
                      }else{
                          var applicationHasChanged = component.getEvent("applicationHasChanged");
                          applicationHasChanged.setParams({
                              "currentApplication": component.get("v.currentPAL"),
                              "nextButtonClickeable": "true"
                          });
                          applicationHasChanged.fire();
                      }
                  }
              }else if(file_A!=null){ //If new upload of schedule A
                  //  alert(' new A to upload');
                  
                  
                  var listOfFiles=component.get('v.fileWrapper');
                  var fileWrapperA =listOfFiles[0];
                  fileWrapperA.parentId=component.get("v.currentPAL.Id");
                  fileWrapperA.fileName=component.get("v.fileName_A");
                  fileWrapperA.base64Data=encodeURIComponent(fileContents);
                  fileWrapperA.contentType=component.get("v.fileType_A");
                  fileWrapperA.fileTitle=component.get("v.fileTitle_A");
                  fileWrapperA.idFileToReplace=oldUploadId_A;
                  component.set('v.fileWrapper',listOfFiles);
                  var action = component.get("c.saveMultipleFiles"); 
                  action.setParams({
                      "ScheduleA_B_Files": JSON.stringify(listOfFiles)
                  });
                  
                  action.setCallback(this, function(response) {
                      var state = response.getState();
                      if (state === "SUCCESS") {
                          /*var k=response.getReturnValue()[0].base64Data;
                                alert('yesy'+(k==fileContents));*/
                                    var idInsertedFile=response.getReturnValue()[0].fileId;
                                    var nameInsertedFile=response.getReturnValue()[0].fileName;
                                    component.set('v.currentPAL.CPAO_Schedule_A_File_Id__c',idInsertedFile);
                                    component.set('v.currentPAL.CPAO_Schedule_A_File_Name__c',nameInsertedFile);
                                    if(oldUploadId_C!=null){ // TO TEST ONCE I FINISH SCHEDULE C FUNCTIONALITY !!!
                                        // alert('rmove old C');
                                        
                                        this.removeScheduleB_or_C(component,'v.currentPAL.CPAO_Schedule_C_File_Id__c','v.currentPAL.CPAO_Schedule_C_File_Name__c','C');
                                    }else{
                                        this.upsertPAL(component);
                                        
                                    }
                                }else{
                                    //alert('eror');
                                    var errors=response.getError();
                                    var message = errors[0];
                                    console.log(message);
                                    
                                }
                                
                            });
                            $A.enqueueAction(action); 
                        }
                        
                        
                    }
          
      } // end else of statement:if(component.get('v.currentPAL.CPAO_Basis_Of_Application__c')==component.get('v.activeLicenceWithOtherBody'))
        
    },
    
    
    
    handleFilesChange: function (component,event,req,fileInputComponent,FileName,componentFileName,fileType,fileContent){
        var fileInput = event.getSource().get("v.files")[0];
        var fileReq=component.find(req);
        
        console.log('file input'+fileInput);
        console.log(fileInput.size);
        console.log(fileInput.name);
        component.set(fileInputComponent,fileInput);
        
        component.set(FileName,fileInput.name);
        
        component.set(componentFileName,fileInput.name);
        //   alert(component.get('v.scheduleAFileName'));
        
        // component.set('v.NotYetUploaded','true');
        
        //alert( component.get('v.fileName'));
        component.set(fileType,fileInput.type);
        
        var fr = new FileReader();
        var self = this;
        
        fr.onload = function() {
            var fileContents = fr.result;
            var base64Mark = 'base64,';
            var dataStart = fileContents.indexOf(base64Mark) + base64Mark.length;
            fileContents = fileContents.substring(dataStart);
            component.set(fileContent,fileContents);
            $A.util.removeClass(req, 'slds-show');
            
            
            // helper.upload(component,fileInput, fileContents);
        };
        
        fr.readAsDataURL(fileInput);
        
    }, 
    removeScheduleB_or_C:function(component, fileId,fileName,typeOfFile){
        var oldUploadId_C=component.get(fileId);
        var listOfIds=[];
        listOfIds.push(oldUploadId_C);
        if(listOfIds.length>0){
            var serverAction = component.get("c.deletePALApplicationFiles");   
            serverAction.setParams({ "filesIdToDelete" : listOfIds});
            serverAction.setCallback(this, function(response){
                console.log('callback');
                var state = response.getState();
                if (state === "SUCCESS") {
                    component.set(fileId,null);
                    component.set(fileName,null);
                    if(typeOfFile=='C'){
                        this.upsertPAL(component);
                    }
                }else{
                    console.log('error while deleting schedule C');
                    
                }
            });
            $A.enqueueAction(serverAction);
        }
        
    },
    
    removeScheduleA_C:function(component){
        // alert('acmethod');
        var oldUploadId_A=component.get('v.currentPAL.CPAO_Schedule_A_File_Id__c');
        var oldUploadId_C=component.get('v.currentPAL.CPAO_Schedule_C_File_Id__c');
        var listOfIds=[];
        listOfIds.push(oldUploadId_A);
        listOfIds.push(oldUploadId_C);
        
        var serverAction = component.get("c.deletePALApplicationFiles");   
        serverAction.setParams({ "filesIdToDelete" : listOfIds});
        serverAction.setCallback(this, function(response){
            console.log('callback');
            var state = response.getState();
            // alert(state);
            if (state === "SUCCESS") {
                component.set('v.currentPAL.CPAO_Schedule_A_File_Name__c',null);
                component.set('v.currentPAL.CPAO_Schedule_A_File_Id__c',null);
                component.set('v.currentPAL.CPAO_Schedule_C_File_Name__c',null);
                component.set('v.currentPAL.CPAO_Schedule_C_File_Id__c',null);
                component.set('v.currentPAL.CPAO_completedPostDesignationPAProgram__c',null);
                this.upsertPAL(component);
            }else{
                console.log('error while deleting schedule a and C');
                
            }
        });
        
        $A.enqueueAction(serverAction);
    },
    addFilesToWrapper:function(component,wrapperToCreate){
        
    }
})