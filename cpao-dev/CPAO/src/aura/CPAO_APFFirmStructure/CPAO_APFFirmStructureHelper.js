({
    getInitWrapper : function(component, helper) {
        var action = component.get("c.getFirmStructure");

        action.setCallback(this, function(response){
            var state = response.getState();
            var result = response.getReturnValue();
            if(state === "SUCCESS") {
                var operatingStatusOptions = result.operatingStatusOptions;
                var firmLocalityOptions = result.firmLocalityOptions;
                var firmServicesOptions = result.firmServicesOptions;
                
                component.set("v.operatingStatusOptions", operatingStatusOptions);
                component.set("v.firmLocalityOptions", firmLocalityOptions);
                component.set("v.firmServicesOptions", firmServicesOptions);
            }
        });
        $A.enqueueAction(action);
    },

    updateAPF : function(component, helper, isQuit) {
        var selOperatingStatus = component.get("v.selOperatingStatus");
        var selFirmLocality = component.get("v.selFirmLocality");
        var selFirmServices = component.get("v.selFirmServices");
        var notes = component.get("v.notes");
        
        var action = component.get("c.updateFirmStructure");
        action.setParams({
            // some stuff here
        });
        action.setCallback(this, function(response){
            var state = response.getState();
            var result = response.getReturnValue();
            var err = response.getError();
            if(state === "SUCCESS") {
                component.set("v.account", result);
                component.set("v.isEdit", false);
                
                if(isQuit) {
                    helper.fireNextEvent(component, "Quit");
                } else {
                    helper.fireNextEvent(component, "Next");
                }
            }
        });
        // $A.enqueueAction(action);
        if(isQuit) {
            helper.fireNextEvent(component, "Quit");
        } else {
            helper.fireNextEvent(component, "Next");
        }
},

    fireNextEvent : function(component, nextStep) {
        var nextButtonClick = $A.get("e.c:CPAO_Next_Button_Has_Been_Pressed");
        nextButtonClick.setParams({
            "componentName" : "CPAO_AnnualPractitionerFee",
            "nextStepName" : nextStep});
        nextButtonClick.fire();
    },

    firePrevEvent : function(component) {
        var prevButtonClick = $A.get("e.c:CPAO_Prev_Button_Has_Been_Pressed");
        prevButtonClick.setParams({
            "componentName" : "CPAO_AnnualPractitionerFee",
            "nextStepName" : "Prev"});
        prevButtonClick.fire();
    },

})