({
	
    clickOnNextButton : function(component,event,helper){
        
        var componentNameInEvent=event.getParam("componentName");
        if(componentNameInEvent==component.get('v.nameOfThisComponent')){
            if(event.getParam("nextStepName")=='Exit'){ 
                component.set('v.shouldSaveAndQuit',true);
            }
            var lso_account_req1=component.find('lso_account_req1');
            var firmName=component.get('v.currentCOA.CPAO_COA_Firm_Name__c');
            var address=component.get('v.currentCOA.Address');
            if(firmName==undefined ||firmName=='--Please select an accounting body--'){
                $A.util.addClass(lso_account_req1, 'slds-show');
            }else{
                $A.util.removeClass(lso_account_req1, 'slds-show');
                var showSpinnerEvent = component.getEvent("showSpinner");
                showSpinnerEvent.fire();
                helper.upsertFMT(component);
                
            }         
            
        }
        
    }
})