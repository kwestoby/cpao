({
    doInit : function(component, event, helper) {
		helper.queryPendingJobs(component);
    },
	handleInvoiceWriteOff: function(component, event, helper) {
		helper.writeOffInvoices(component);
	},
    handleFilterChange: function(component, event, helper) {
        component.set("v.pageMessages", []);
        component.set("v.pageNumber",1);
        component.set("v.selectedInvoices",new Map());
		component.set("v.selectedItemCount",0);        
        helper.queryInvoices(component);
    },
    pageSizeChanged: function(component, event, helper) {
        component.set("v.pageMessages", []);
        component.set("v.pageNumber",1);
        helper.queryInvoices(component);
    },
    first: function(component, event, helper) {
        if(component.get("v.pageNumber")>1) {
            component.set("v.pageNumber",1);
            helper.queryInvoices(component);
        }
    },
    previous: function(component, event, helper) {
        var currentPage = component.get("v.pageNumber");
        if(currentPage>1) {
            currentPage-=1;
            component.set("v.pageNumber",currentPage);
            helper.queryInvoices(component);
        }
    },
    next: function(component, event, helper) {
        var currentPage = component.get("v.pageNumber");
        var lastPageNumber = component.get("v.pages");
        if(currentPage<lastPageNumber) {
            currentPage+=1;
            component.set("v.pageNumber",currentPage);
            helper.queryInvoices(component);
        }
    },
    last: function(component, event, helper) {
        var currentPage = component.get("v.pageNumber");
        var lastPageNumber = component.get("v.pages");
        if(currentPage<lastPageNumber) {
            component.set("v.pageNumber", lastPageNumber);
            helper.queryInvoices(component);
        }
    },
    handleSort : function(component, event, helper) {
        var id = event.currentTarget.dataset.id;
        if(component.get("v.sortBy")===id) {
            if(component.get("v.sortOrder")=="ASC") {
                component.set("v.sortOrder", "DESC");
            } else {
                component.set("v.sortOrder", "ASC");
            }
        } else {
            component.set("v.sortBy", id);
            component.set("v.sortOrder", "ASC");
        }
        helper.queryInvoices(component);
    },
    handleSelect: function(component, event, helper) {
        var index = event.getSource().get("v.value");
        var invoices = component.get("v.invoices");
        var selectedInvoices = component.get("v.selectedInvoices");
        var invoiceId = invoices[index].Id;
        if(invoices[index].isSelected) {
            selectedInvoices[invoiceId] = invoices[index];
        } else if(selectedInvoices.hasOwnProperty(invoiceId)) {
			delete selectedInvoices[invoiceId];
		}
        component.set("v.selectedInvoices",selectedInvoices);
        component.set("v.selectedItemCount",Object.keys(selectedInvoices).length);
    },
    handleSelectAll: function(component, event, helper) {
        var selectAll = component.get("v.selectAll");
		var invoices = component.get("v.invoices");
        var selectedInvoices = component.get("v.selectedInvoices");
		if(selectAll) {
			for(var invoice of invoices) {
				if(!invoice.isSelected) {
					invoice.isSelected = true;
					selectedInvoices[invoice.Id] = invoice;
				}
			}
		} else {
			for(var invoice of invoices) {
				if(invoice.isSelected) {
					invoice.isSelected = false;
					selectedInvoices[invoice.Id] = invoice;
				}
				if(selectedInvoices.hasOwnProperty(invoice.Id)) {
					delete selectedInvoices[invoice.Id];
				}
			}
		}
		component.set("v.invoices",invoices);
		component.set("v.selectedInvoices", selectedInvoices);
		component.set("v.selectedItemCount", Object.keys(selectedInvoices).length);
    }
})