public without sharing class xCPAO_ReviewReinstatementInfoCTRL {
    
  //  public static final String OPEN = 'Open';
  //  public static final String ACTIVE = 'Active';
  //  public static final String SUBMITTED = 'Submitted';

  //  //this loads up a lof of data relevant to the reinstatement application
  //  //data loaded: current user, reinstatement aplication, account contact relationships related to the user's contact, 
  //  //the declaration related to the application, the infractions related to the application (they are not displayed),
  //  //and the applications liked to the infractions which are linked to the reinstatement appliaction (and the attachments on those applications)
  //  public CPAO_ReviewReinstatementInfoCTRL() {
  //      User current = CPAO_UserSelector.getCurrentUser();
  //      showDeclaration = false;
  //      showNewLegalNames = false;
  //      List<Contact> currentContact = CPAO_ContactSelector.getContactToUpdate(new Set<Id>{current.ContactId});
  //      if(currentContact.size() != 1){
  //          return;
  //      }
  //      Id contactId = currentContact[0].Id;

  //      contact = currentContact[0];

  //      List<CPAO_Application__c> currentReinstatementAppliation = 
  //          CPAO_ApplicationSelector.getUserApplicationsByRecordTypeNameAndStatus(OPEN, CPAO_ApplicationHandler.REINSTATEMENT);

  //      application = currentReinstatementAppliation[0];
  //      if(application.CPAO_Legal_Given_Name_s__c != null || application.CPAO_Legal_Surname__c != null){
  //          showNewLegalNames = true;
  //      }

  //      accountContactEmploymentInfo = CPAO_AccountContactRelationSelector.getRelationshipsByContactIdAndStatus(contactId, ACTIVE);

  //      if(!accountContactEmploymentInfo.isEmpty()){
  //          showRelationships = true;
  //      }

  //      List<CPAO_Declaration__c> declarations = 
  //          CPAO_DeclarationSelector.getApplicationDeclaration(application.Id, CPAO_DeclarationSelector.GOOD_CHARACTER);

  //      if(!declarations.isEmpty()){
  //          goodCharacterDeclaration = declarations[0];
  //          showDeclaration = true;
  //      }

  //      List<CPAO_Infraction__c> applicationInfractions = CPAO_InfractionSelector.getApplicationInfractions(application.Id);

  //      applicationInfractionApplications = CPAO_ApplicationSelector.getInfractionApplications(applicationInfractions);
  //      if(!applicationInfractionApplications.isEmpty()){
  //          showInfractionApps = true;
  //      }
  //  }

  //  public PageReference save(){
  //      if(!attested){
  //          ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.FATAL,'Please check off the Attest Above checkbox'));
  //          return null;
  //      }else{
  //          application.CPAO_Status__c = SUBMITTED;
  //          update(application);
  //          PageReference successPage = new PageReference('/CPAO_ReinstatementSuccesPage');
  //          successPage.setRedirect(true);
  //          return successPage;
  //      }
  //      //String stepId = Framework.PageUtils.getParam('id');
  ////      joinApi.JoinUtils joinUtil = new joinApi.JoinUtils(stepId);
  ////      String redirectURL = joinUtil.navStepsMap.get('Next');
  ////      System.debug('joinUtil.navStepsMap: ' + joinUtil.navStepsMap);
  //      //JSONGenerator gen = JSON.createGenerator(true);
  //      //gen.writeStartObject();
  //      //gen.writeStringField('stepId', stepId);
  //      //gen.writeStringField('redirectURL', redirectURL);
  //      //gen.writeEndObject();
  //      //joinUtil.setRedirectURLCookie(gen.getAsString());
  //      return null;
  //  }

  //  public PageReference previousStep() {
  //      if(!Test.isRunningTest()){
  //          String stepId = Framework.PageUtils.getParam('id');
  //          joinApi.JoinUtils joinUtil = new joinApi.JoinUtils(stepId);
  //          return new PageReference(joinUtil.getPreviousStep('{}'));
  //      } else {return null;}

            
  //  }

  //  public Contact contact{get; set;}

  //  public CPAO_Declaration__c goodCharacterDeclaration{get; set;}

  //  public List<CPAO_Account_Contact_Relationship__c> accountContactEmploymentInfo{get; set;}

  //  public Boolean showDeclaration{get; set;}
  //  public Boolean showRelationships{get; set;}
  //  public Boolean showNewLegalNames{get; set;}
  //  public Boolean showInfractionApps{get; set;}

  //  public Boolean attested{get; set;}


  //  public CPAO_Application__c application{get; set;}

  //  public List<CPAO_Application__c> applicationInfractionApplications{get; set;}
}