@isTest
private class CPAO_ContactHandlerTest {
	public static String INACTIVE_STATUS = 'Inactive';
	
	public static List<Account> factoryAccounts;
	public static List<Contact> factoryContacts;

	public static void dataInitialize(Integer numberOfAccounts, Integer numberOfContacts){
		CPAO_TestFactory.automationControlSetup();
		factoryAccounts = CPAO_TestFactory.createAccounts(numberOfAccounts);
		insert factoryAccounts;
		factoryContacts= CPAO_TestFactory.createContacts(factoryAccounts[0],numberOfContacts);
		insert factoryContacts;

		insert new CPA_Id_Warning_Email__c(
        	SetupOwnerId=UserInfo.getOrganizationId(), 
        	CPA_Warning_Email_Body__c = 'Some Value',
        	CPA_Warning_Email_Subject__c = 'Some Value',
        	Warning_Email_send_to_address__c = 'test@test.com',
        	Warning_email_threshold__c = 10);

	}
	
	@isTest static void test_automaticAssignment1() {
		// This tests when we create new contacts and want to autoassign ids.
		// In this scenario, number of available ids is equal to number of asked ids in the active range.
		//the ids are already created so the system grabs those existant ids instead of creating new ones from next range
		dataInitialize(1, 10);
		List<CPAO_CPA_Id__c> idRecordsList = CPAO_TestFactory.createUnusedIdRecords(10);
		insert(idRecordsList);

		CPAO_RecordIdAssignmentWrapper wrapper = new CPAO_RecordIdAssignmentWrapper('Contact');
		for (Contact factoryContact :factoryContacts){
			wrapper.idsAutomaticAssignment.add(factoryContact.Id);
		}

		CPAO_ContactHandler.packageAndPassToIdClass(wrapper.idsAutomaticAssignment,null,null);
		
		List<CPAO_CPA_Id__c> addedIds = [SELECT id,Id_Name__c,Contact__c FROM CPAO_CPA_Id__c WHERE Contact__c IN: factoryContacts];
		for (integer i=0;i<factoryContacts.size();i++){
			System.assertEquals(addedIds[i].Contact__c,factoryContacts[i].Id);
		}
	}

	@isTest static void test_automaticAssignment2() {
		// This tests when we create new contacts and want to autoassign ids.
		// In this scenario,the ids are already created but are active so the system creates new ones from next range and
		//assign it to the contacts
		dataInitialize(1, 10);
		List<CPAO_CPA_Id__c> idRecordsList = CPAO_TestFactory.createActiveIdRecords(10);

		insert CPAO_TestFactory.addNextRange(CPAO_TestFactory.createRange(3));
		insert(idRecordsList);

		CPAO_RecordIdAssignmentWrapper wrapper = new CPAO_RecordIdAssignmentWrapper('Contact');
		for (Contact factoryContact :factoryContacts){
			wrapper.idsAutomaticAssignment.add(factoryContact.Id);
		}

		CPAO_ContactHandler.packageAndPassToIdClass(wrapper.idsAutomaticAssignment,null,null);
		
		List<CPAO_CPA_Id__c> addedIds = [SELECT id,Id_Name__c,Contact__c FROM CPAO_CPA_Id__c WHERE Contact__c IN: factoryContacts];
		
		for (integer i=0;i<factoryContacts.size();i++){
			System.assertEquals(addedIds[i].Contact__c,factoryContacts[i].Id);
		}
	}
	
	@isTest static void test_automaticAssignment3() {
		// This tests when we create new contacts and want to autoassign ids
		// No ids are existent at the moment so we need to create first ids.
		// In this scenario, number of ids in active range is more  than number of asked ids 
		// So the system will only work with one range
		dataInitialize(1, 9);

		insert CPAO_TestFactory.addNextRange(CPAO_TestFactory.createRange(3));

		CPAO_RecordIdAssignmentWrapper wrapper = new CPAO_RecordIdAssignmentWrapper('Contact');
		for (Contact factoryContact :factoryContacts){
			wrapper.idsAutomaticAssignment.add(factoryContact.Id);
		}
		

		CPAO_ContactHandler.packageAndPassToIdClass(wrapper.idsAutomaticAssignment,null,null);
		
		List<CPAO_CPA_Id__c> addedIds = [SELECT id,Id_Name__c,Contact__c FROM CPAO_CPA_Id__c WHERE Contact__c IN: factoryContacts];
		for (integer i=0;i<factoryContacts.size();i++){
			System.assertEquals(addedIds[i].Contact__c,factoryContacts[i].Id);
		}

	}

	@isTest static void test_automaticAssignment4() {
		// This tests when we create new contacts and want to autoassign ids
		// No ids are existent at the moment so we need to create first ids.
		// In this scenario, number of ids in active range is less  than number of asked ids 
		// So the system will  work with active range and next range
		dataInitialize(1, 11);

		insert CPAO_TestFactory.addNextRange(CPAO_TestFactory.createRange(3));

		CPAO_RecordIdAssignmentWrapper wrapper = new CPAO_RecordIdAssignmentWrapper('Contact');
		for (Contact factoryContact :factoryContacts){
			wrapper.idsAutomaticAssignment.add(factoryContact.Id);
		}
		

		CPAO_ContactHandler.packageAndPassToIdClass(wrapper.idsAutomaticAssignment,null,null);
		
		List<CPAO_CPA_Id__c> addedIds = [SELECT id,Id_Name__c,Contact__c FROM CPAO_CPA_Id__c WHERE Contact__c IN: factoryContacts];
		for (integer i=0;i<factoryContacts.size();i++){
			System.assertEquals(addedIds[i].Contact__c,factoryContacts[i].Id);
		}
		
	}

	@isTest static void test_automaticAssignment5() {
		// This tests when we create new contacts and want to autoassign ids
		// In this scenario, number of available ids is less than number of needed ids
		//The system will first assign the available unused ids and will create new ones from the next range

		dataInitialize(1, 11);
		List<CPAO_CPA_Id__c> factoryUsedIds=CPAO_TestFactory.createUnusedIdRecords(5);
		insert(factoryUsedIds);

		insert CPAO_TestFactory.addNextRange(CPAO_TestFactory.createRange(3));

		CPAO_RecordIdAssignmentWrapper wrapper = new CPAO_RecordIdAssignmentWrapper('Contact');
		for (Contact factoryContact :factoryContacts){
			wrapper.idsAutomaticAssignment.add(factoryContact.Id);
		}
		

		CPAO_ContactHandler.packageAndPassToIdClass(wrapper.idsAutomaticAssignment,null,null);
		
		List<CPAO_CPA_Id__c> addedIds = [SELECT id,Id_Name__c,Contact__c FROM CPAO_CPA_Id__c WHERE Contact__c IN: factoryContacts];
		for (integer i=0;i<factoryContacts.size();i++){
			System.assertEquals(addedIds[i].Contact__c,factoryContacts[i].Id);
		}
		
	}
	
	@isTest static void test_automaticAssignment6() {
		// This tests when we create new contacts and want to autoassign ids
		// In this scenario, number of available ids is more  than number of asked ids in active range
		// No ids are existent at the moment so we need to create first ids.
		// However, there is an active id that is contained in the next active range (Id_Name__c=0) 
		//so the system must not recreate it as it is already created
		
		dataInitialize(1, 11);
		List<CPAO_CPA_Id__c> idRecordsList = CPAO_TestFactory.createNotUniqueIdRecords(5);

		insert(idRecordsList);

		insert CPAO_TestFactory.addNextRange(CPAO_TestFactory.createRange(3));


		CPAO_RecordIdAssignmentWrapper wrapper = new CPAO_RecordIdAssignmentWrapper('Contact');
		for (Contact factoryContact :factoryContacts){
			wrapper.idsAutomaticAssignment.add(factoryContact.Id);
		}
		

		CPAO_ContactHandler.packageAndPassToIdClass(wrapper.idsAutomaticAssignment,null,null);
		
		List<CPAO_CPA_Id__c> addedIds = [SELECT id,Id_Name__c,Contact__c FROM CPAO_CPA_Id__c WHERE Contact__c IN: factoryContacts];
		for (integer i=0;i<factoryContacts.size();i++){
			System.assertEquals(addedIds[i].Contact__c,factoryContacts[i].Id);
		}
		
	}

	@isTest static void test_ManualAssignment1() {
		// This tests when we create new contacts and want to manually assign ids
		// In this scenario, the contacts don't have yet a cpa id 
		// the system verifies if the id is valid before adding it 
		
		dataInitialize(1, 5);
		List<CPAO_CPA_Id__c> idRecordsList = CPAO_TestFactory.createUnusedIdRecords(5);

		insert(idRecordsList);

		insert CPAO_TestFactory.addNextRange(CPAO_TestFactory.createRange(3));

		for(integer i=0;i<factoryContacts.size();i++){
			factoryContacts[i].CPAO_CPA_Id__c=idRecordsList[i].Id_Name__c;
		}
		update(factoryContacts);
		Map<Id, Contact> oldMap = new Map<Id, Contact>();
		for(integer i=0;i<factoryContacts.size();i++){
			Contact oldContact = New Contact();
			oldContact.LastName=factoryContacts[i].LastName;
			oldContact.id=factoryContacts[i].id;
			oldMap.put(oldContact.id,oldContact);
		}

		
		CPAO_ContactHandler.beforeUpdateFilters(factoryContacts,oldMap) ;
		CPAO_ContactHandler.packageAndPassToIdClass(null,factoryContacts,null);
		
		List<CPAO_CPA_Id__c> addedIds = [SELECT id,Id_Name__c,Contact__c,status__c FROM CPAO_CPA_Id__c WHERE Contact__c IN: factoryContacts];
		for (integer i=0;i<factoryContacts.size();i++){
			System.assertEquals(addedIds[i].Contact__c,factoryContacts[i].Id);
		}
		
	}

	@isTest static void test_WrongPath1() {
		// This tests when we create new contacts and want to automaticly assign ids
		// In this scenario, there is no available ids and no active range
		//the system must throw an error
		
		try{
			dataInitialize(1, 10);
			List<CPAO_CPA_Id__c> idRecordsList = CPAO_TestFactory.createActiveIdRecords(10);

			insert(idRecordsList);

			CPAO_RecordIdAssignmentWrapper wrapper = new CPAO_RecordIdAssignmentWrapper('Contact');
			for (Contact factoryContact :factoryContacts){
				wrapper.idsAutomaticAssignment.add(factoryContact.Id);
			}

			CPAO_ContactHandler.packageAndPassToIdClass(wrapper.idsAutomaticAssignment,null,null);
			
			
		}catch(CPAO_IDRecordController.CPAO_IDRecordController_Exception e){
			System.assertEquals(e.getMessage(),CPAO_IDRecordController.noCurrentActiveRange);
		}

	}


	@isTest static void test_WrongPath2() {

		// This tests when we create new contacts and want to autoassign ids
		// No ids are existent at the moment so we need to create first ids.
		// In this scenario, number of ids in active range is less  than number of asked ids 
		// So the system will  work with active range and since there is no next range
		//it will throw an error
		try{
			dataInitialize(1, 11);
			
			insert CPAO_TestFactory.addNextRange(CPAO_TestFactory.createRange(3));

			CPAO_RecordIdAssignmentWrapper wrapper = new CPAO_RecordIdAssignmentWrapper('Contact');
			for (Contact factoryContact :factoryContacts){
				wrapper.idsAutomaticAssignment.add(factoryContact.Id);
			}
			CPAO_ContactHandler.packageAndPassToIdClass(wrapper.idsAutomaticAssignment,null,null);

		}catch(CPAO_IDRecordController.CPAO_IDRecordController_Exception e){
			System.assertEquals(e.getMessage(),CPAO_IDRecordController.nextActiveRangeNotSpecified);
		}
	}


	@isTest static void test_ButtonClick1() {
		// This tests simulates when we click the button to automaticly assign an id to a contact
		// In this scenario, there is no Id yet assigned to the contact so the system will assign
		// an id for the first time
		dataInitialize(1, 1);
		List<CPAO_CPA_Id__c> idRecordsList = CPAO_TestFactory.createUnusedIdRecords(1);

		insert(idRecordsList);

		CPAO_ContactHandler.handleNewIdButtonClicks(factoryContacts[0].id);

		List<CPAO_CPA_Id__c> addedIds = [SELECT id,Id_Name__c,Contact__c,status__c FROM CPAO_CPA_Id__c WHERE Contact__c IN: factoryContacts];
		for (integer i=0;i<factoryContacts.size();i++){
			System.assertEquals(addedIds[i].Contact__c,factoryContacts[i].Id);
		}
	}

	@isTest static void suspensionAndRevocationUpdate() {
		dataInitialize(1, 10);

		for(integer i=0;i<factoryContacts.size();i++){
			factoryContacts[i].CPAO_CPA_Contact_Type__c = CPAO_ContactHandler.MEMBER;
			if(i>4){
				factoryContacts[i].CPAO_CPA_Status__c = CPAO_ContactHandler.SUSPENDED;
				factoryContacts[i].CPAO_Anticipated_Revocation_Dereg_Date__c = Date.today();
			} else {
				factoryContacts[i].CPAO_CPA_Status__c = CPAO_ContactHandler.REVOKED;
			}			
		}
		update(factoryContacts);		
	}

	@isTest static void suspensionAndRevocationInsert() {
		CPAO_TestFactory.automationControlSetup();
		factoryAccounts = CPAO_TestFactory.createAccounts(1);
		insert factoryAccounts;
		factoryContacts= CPAO_TestFactory.createContacts(factoryAccounts[0],10);

		for(integer i=0;i<factoryContacts.size();i++){
			factoryContacts[i].CPAO_CPA_Contact_Type__c = CPAO_ContactHandler.MEMBER;
			if(i>4){
				factoryContacts[i].CPAO_CPA_Status__c = CPAO_ContactHandler.SUSPENDED;
				factoryContacts[i].CPAO_Anticipated_Revocation_Dereg_Date__c = Date.today();
			} else {
				factoryContacts[i].CPAO_CPA_Status__c = CPAO_ContactHandler.REVOKED;
			}			
		}
		insert(factoryContacts);		
	}

	/*
	@isTest static void test_AccountContactRelationshipCreation() {
		// This tests verifies that whenever a contact is created and the account lookup field is populated, 
		// the system creates an associated account to contact relationship 
		dataInitialize(1, 1);
		List<Contact> contacts = [SELECT Id, AccountId from Contact];
		List<Account> accounts = [SELECT Id,Name from Account];
		contacts[0].CPAO_Primary_Employer__c=contacts[0].AccountId;
		update newContact;

		List<CPAO_Account_Contact_Relationship__c> relationships=[SELECT CPAO_Account__c,CPAO_Contact__c,CPAO_Start_Date__c,
		CPAO_Relationship_to_Account__c	 FROM CPAO_Account_Contact_Relationship__c];
		System.assertEquals(1,relationships.size());
		system.debug(relationships);
		
		}*/
	
}