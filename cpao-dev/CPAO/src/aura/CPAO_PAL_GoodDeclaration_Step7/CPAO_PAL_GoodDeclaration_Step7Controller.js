({
    clickOnNextButton : function(component,event,helper){
        
        var criminalCharges=component.get('v.currentPAL.CPAO_Criminal_Charges_or_Convictions__c');
        var licenseSuspension=component.get('v.currentPAL.CPAO_Susp_Rev_of_Professional_License__c');
        var disciplinaryDecision=component.get('v.currentPAL.CPAO_Subject_to_disciplinary_decision__c');
        var bankruptcy=component.get('v.currentPAL.CPAO_Made_assignment_in_bankruptcy__c');
        var guiltyOfTaxLegislation=component.get('v.currentPAL.CPAO_Guilty_of_Security_Tax_Legislation__c');
        var expulsion=component.get('v.currentPAL.CPAO_Expelled_from_a_Professional_Org__c');
        
        var componentNameInEvent=event.getParam("componentName");
        
        
        if(componentNameInEvent==component.get('v.nameOfThisComponent')){
            if(event.getParam("nextStepName")=='Exit'){ 
                component.set('v.shouldSaveAndQuit',true);
            }
            
            var validForm = component.find('PALForm').reduce(function (validSoFar, inputCmp) {
                
                if(criminalCharges==undefined){
                    component.set('v.criminal_charges_required',true);
                }else{
                    component.set('v.criminal_charges_required',false);
                }
                
                if(licenseSuspension==undefined){
                    component.set('v.license_suspension_required',true);
                }else{
                    component.set('v.license_suspension_required',false);
                }
                
                if(disciplinaryDecision==undefined){
                    component.set('v.disciplinary_decision_required',true);
                }else{
                    component.set('v.disciplinary_decision_required',false);
                }
                
                if(bankruptcy==undefined){
                    component.set('v.bankruptcy_required',true);
                }else{
                    component.set('v.bankruptcy_required',false);
                }
                
                if(guiltyOfTaxLegislation==undefined){
                    component.set('v.tax_legislation_required',true);
                }else{
                    component.set('v.tax_legislation_required',false);
                }
                
                if(expulsion==undefined){
                    component.set('v.expulsion_required',true);
                }else{
                    component.set('v.expulsion_required',false);
                }
                
                
                inputCmp.showHelpMessageIfInvalid();
                return validSoFar && inputCmp.get('v.validity').valid && criminalCharges!=undefined
                && licenseSuspension!=undefined && disciplinaryDecision!=undefined && bankruptcy!=undefined
                && guiltyOfTaxLegislation!=undefined && expulsion!=undefined
            }, true);
            //}
            if(validForm){ 
                component.set('v.criminal_charges_required',false);
                component.set('v.license_suspension_required',false);
                component.set('v.disciplinary_decision_required',false);
                component.set('v.bankruptcy_required',false);
                component.set('v.tax_legislation_required',false);
                component.set('v.expulsion_required',false);
                
                var nextStep=event.getParam('nextStepName');
                //  component.set('v.currentPAL.CPAO_Current_Step__c',nextStep);
                component.set('v.currentPAL',event.getParam('PALApplication'));
                
                
                
                if(criminalCharges=='No'){
                    component.set('v.currentPAL.CPAO_Explanation_for_criminal_offence__c',null);
                }
                
                if(licenseSuspension=='No'){
                    component.set('v.currentPAL.CPAO_Explanation_for_suspension__c',null);
                }
                
                if(disciplinaryDecision=='No'){
                    component.set('v.currentPAL.CPAO_Explanation_for_discipline_decision__c',null);
                }
                
                if(bankruptcy=='No'){
                    component.set('v.currentPAL.CPAO_Explanation_for_bankruptcy__c',null);
                }
                
                if(guiltyOfTaxLegislation=='No'){
                    component.set('v.currentPAL.CPAO_Explanation_of_Legislation_Guilt__c',null);
                }
                
                if(expulsion=='No'){
                    component.set('v.currentPAL.CPAO_Explanation_for_expulsion__c',null);
                }
                
                
                
                
                if(criminalCharges=='Yes' || licenseSuspension=='Yes' || disciplinaryDecision=='Yes' ||
                   bankruptcy=='Yes' || guiltyOfTaxLegislation=='Yes' || expulsion=='Yes'){
                    component.set('v.currentPAL.CPAO_Good_Character_Review_Required__c','Yes');
                }else{
                    component.set('v.currentPAL.CPAO_Good_Character_Review_Required__c','No');
                }
                var showSpinnerEvent = component.getEvent("showSpinner");
                showSpinnerEvent.fire();
                helper.upsertPAL(component);
                
                //helper.upload(component,component.get('v.fileInput_CriminalOffence'), component.get('v.fileContents_CriminalOffence')) ;
                
                
                //to do: delete values in date nd in firm name if choice isnt good    	
            }else{
                console.log('good declaration not valid');
            }
        }
    }/*,
    handleFilesChange_CriminalOffence: function (component, event,helper) {
        var fileInput = event.getSource().get("v.files")[0];
        console.log('file input'+fileInput);
        console.log(fileInput.size);
        component.set('v.fileInput_CriminalOffence',fileInput);
        component.set('v.fileName_CriminalOffence',fileInput.name);
        component.set('v.CriminalOffenceFileName',fileInput.name);
       // component.set('v.NotYetUploaded','true');

        //alert( component.get('v.fileName'));
        component.set('v.fileType_CriminalOffence',fileInput.type);

        var fr = new FileReader();
        var self = this;

        fr.onload = function() {
            var fileContents = fr.result;
            var base64Mark = 'base64,';
            var dataStart = fileContents.indexOf(base64Mark) + base64Mark.length;
            fileContents = fileContents.substring(dataStart);
            component.set('v.fileContents_CriminalOffence',fileContents);

          // helper.upload(component,fileInput, fileContents);
        };

        fr.readAsDataURL(fileInput);
    },*/
    //
})