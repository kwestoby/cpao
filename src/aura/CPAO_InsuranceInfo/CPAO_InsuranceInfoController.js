({
	doInit : function(component, event, helper) {
		console.log('init');
	    var action = component.get("c.componentInit");
	   	action.setCallback(this, function(response){
		      var state = response.getState();
		      if (state === "SUCCESS") {
		      	component.set("v.listOfInsurances", JSON.parse(response.getReturnValue()).currentInsurances);
		      	component.set("v.coverageTypeOptions",JSON.parse(response.getReturnValue()).coverageTypes);
		      	component.set("v.insuranceProviderOptions",JSON.parse(response.getReturnValue()).insuranceProviders);
		      	component.set("v.accountNamesAndIdsMap",JSON.parse(response.getReturnValue()).accountNamesAndIds);
		      	component.set("v.accountNameOptions",JSON.parse(response.getReturnValue()).accountNames);

		      	console.log(JSON.parse(response.getReturnValue()));
		    	
		      }else{
		      	console.log('error while response.getReturnValue()');

		      }
	    });
	  
	    $A.enqueueAction(action);
	},
	 openModal: function(component, event, helper) {
      // for Display Modal,set the "modalIsOpen" attribute to "true"
      component.set("v.modalIsOpen", true);
   },
 
   closeModal: function(component, event, helper) {
      // for Hide/Close Modal,set the "modalIsOpen" attribute to "Fasle"  
      component.set("v.modalIsOpen", false);
   },
   dateUpdate: function(component, event, helper) {

	   	/*var nowDate = new Date(); 
		var date = nowDate.getFullYear()+'-0'+(nowDate.getMonth()+1)+'-'+nowDate.getDate(); 
		console.log(component.get("v.expiryDate"));
		console.log(date);*/
		var today = new Date();        
        var dd = today.getDate();
        var mm = today.getMonth() + 1; //January is 0!
        var yyyy = today.getFullYear();
     // if date is less then 10, then append 0 before date   
        if(dd < 10){
            dd = '0' + dd;
        } 
    // if month is less then 10, then append 0 before date    
        if(mm < 10){
            mm = '0' + mm;
        }
        
     var todayFormattedDate = yyyy+'-'+mm+'-'+dd;

	    if(component.get("v.effectiveDate")>=component.get("v.expiryDate")){   
	      	 component.set("v.expirydateValidationError", false);
	      	 component.set("v.effectivedateValidationError", true);
	      	 //$A.util.addClass(component.get("v.effectiveDate"), 'errorClass');
			 component.set("v.addButtonIsNotClickable", true);
			 //$A.util.removeClass(cmpTarget, 'errorClass');

		/*}else if(component.get("v.expiryDate")<=todayFormattedDate){
		     component.set("v.effectivedateValidationError", false);
			 component.set("v.expirydateValidationError", true);
			 component.set("v.addButtonIsNotClickable", true);*/
		}else{
		     component.set("v.effectivedateValidationError", false);
			 component.set("v.expirydateValidationError", false);
			 component.set("v.addButtonIsNotClickable", false);

		}
   },
   
  
 
   addNewInsurance: function(component, event, helper) {
      var empty='--Please choose an option--';
      var coverageType =component.get("v.coverageType");
      var insuranceProvider=component.get("v.insuranceProvider");
      var accountNameOption =component.get("v.accountName");
   	  var validationErrors=component.get('v.selectedPolicyHolder.text')==null || coverageType ==empty ||
   	  insuranceProvider==empty  ||accountNameOption==empty
      var validForm = component.find('insuranceForm').reduce(function (validSoFar, inputCmp) {
            // Displays error messages for invalid fields
            inputCmp.showHelpMessageIfInvalid();
            return validSoFar && inputCmp.get('v.validity').valid;
        }, true);
        // If we pass error checking, do some real work
       /* if(component.get('v.selectedPolicyHolder.text')==null){
        	component.set('v.policyHolderEmpty',true);
         }else*/ if(validationErrors){	
         	if(insuranceProvider==empty){	
       		component.set('v.emptyInsuranceProvider',true);
       		}else{
       			component.set('v.emptyInsuranceProvider',false);
       		}

         	if(coverageType==empty){	
       		component.set('v.emptyCoverageType',true);
       		}else{
       		component.set('v.emptyCoverageType',false);
       		}

       		if(accountNameOption==empty){	
       		component.set('v.emptyAccountName',true);
       		}else{
       			       		component.set('v.emptyAccountName',false);

       		}
       		if(component.get('v.selectedPolicyHolder.text')==null){	
       		component.set('v.policyHolderEmpty',true);
       		}else{
       			       		component.set('v.policyHolderEmpty',false);

       		}
     /*  }else if(component.get("v.insuranceProvider") =='--Please choose an option--'){	

       		component.set('v.emptyInsuranceProvider',true);
	   }else if(component.get("v.coverageType") =='--Please choose an option--'){	

			component.set('v.emptyCoverageType',true);
	   }else if(component.get("v.accountName") =='--Please choose an option--'){	

			component.set('v.emptyAccountName',true);
       */
       
        }else if(validForm ){
        	component.set('v.policyHolderEmpty',false);
        	var accountName = component.get("v.accountNamesAndIdsMap")[component.get('v.accountName')];
        	var policyHolder=component.get('v.selectedPolicyHolder.val');
        	var insuranceToAdd ={ "sobjectType": "CPAO_Insurance__c",
        	    "CPAO_Account_Name__c":accountName,
        	    "CPAO_Account_Name_Of_Policy_Holder__c":policyHolder,
        	    "CPAO_Coverage_Type__c":component.get("v.coverageType"),
        	    "CPAO_Insurance_Provider__c":component.get("v.insuranceProvider"),
        	    "CPAO_Insurance_Effective_Date__c":component.get("v.effectiveDate"),
        	    "CPAO_Expiry_Date__c":component.get("v.expiryDate"),
        	    "CPAO_Professional_Liability_Insurance__c":true,
        	    "CPAO_Compliance_with_Requirements__c":true};

        	component.set("v.newInsurance", insuranceToAdd);

        	console.log(insuranceToAdd);

        	var action2 = component.get("c.saveNewInsurance");
	    action2.setParams({
	        "newInsurance": component.get("v.newInsurance")
	    });  
	    action2.setCallback(this, function(response){
	    	var state = response.getState();
	   	    if (state === "SUCCESS") {
	   	    	component.set("v.modalIsOpen", false);
		window.location.reload();	   	    	 

	   	    	console.log('success');
		     
	        }else{
	        	console.log('err');}
	        
	    });
        $A.enqueueAction(action2);
       // component.set("v.modalIsOpen", false);
			
		   
	    }
   }
})