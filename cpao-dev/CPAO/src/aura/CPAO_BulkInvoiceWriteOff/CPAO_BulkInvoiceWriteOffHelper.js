({
	queryPendingJobs: function(component) {
		var helper = this;
		component._interval = setInterval($A.getCallback(function() {
			var action = component.get("c.getPendingJobs");
			action.setCallback(this, function(response) {
				var state = response.getState();
				if(state==="SUCCESS") {
					var pendingJobs = response.getReturnValue();
					if(pendingJobs.length<=0) {
                        if(component.get("v.pendingJobs").length>0){
                            component.set("v.pageMessages",[{
                                title:"Confirmation",
                                severity:"confirm",
                                body:"Bulk invoice write-off completed!"
                            }]);
                        }
						clearInterval(component._interval);
						component.set("v.isLoading",false);
						helper.queryInvoices(component);
					}
                    component.set("v.pendingJobs", pendingJobs);
				} else {
					console.log("Error: "+response.getError()[0].message);
				}
			});
			$A.enqueueAction(action);
		}), 2000);
	},
	queryInvoices : function(component) {
        var action = component.get("c.getInvoices");
        if(component.get("v.dateFrom")=="") {
            component.set("v.dateFrom",null);
        }
        if(component.get("v.dateTo")=="") {
            component.set("v.dateTo",null);
        }
		var invoiceStatus = [];
		var memberStatus = [];
		invoiceStatus.push(component.get("v.invoiceStatus"));
		memberStatus.push(component.get("v.memberStatus"));
        action.setParams({
            query: JSON.stringify({
                searchKey: component.get("v.searchKey"),
				invoiceStatus: invoiceStatus,
				memberStatus: memberStatus,
                postedFrom: component.get("v.dateFrom"),
                postedTo: component.get("v.dateTo"),
                pageNumber: component.get("v.pageNumber"),
            	pageSize: component.get("v.pageSize"),
                sortBy: component.get("v.sortBy"),
                sortOrder: component.get("v.sortOrder")
            })
        });
        action.setCallback(this, function(response){
            var state = response.getState();
            if(state==="SUCCESS") {
                var invoices = response.getReturnValue().invoices;
                var selectedInvoices = component.get("v.selectedInvoices");
				var selectAll = true;
                for(var invoice of invoices) {
                    if(selectedInvoices.hasOwnProperty(invoice.Id)) {
                        invoice.isSelected = true;
                    } else {
						selectAll = false;
                        invoice.isSelected = false;
                    }
                }
				component.set("v.selectAll", selectAll);
                component.set("v.invoices", invoices);
				component.set("v.totalItems", response.getReturnValue().totalRecords); 
                component.set("v.pages", Math.ceil(response.getReturnValue().totalRecords/component.get("v.pageSize")));
            } else {
                console.log("Error: "+response.getError()[0].message);
            }
        });
        $A.enqueueAction(action);
	},
	writeOffInvoices: function(component) {
        component.set("v.pageMessages", []);
		var selectedInvoiceIDs = Object.keys(component.get("v.selectedInvoices"));
		console.log(selectedInvoiceIDs);
		var action = component.get("c.writeOffInvoices");
		action.setParams({
			invoiceIDs: selectedInvoiceIDs
		});
		var helper = this;
		action.setCallback(this, function(response) {
			var state = response.getState();
			if(state==="SUCCESS") {
				component.set("v.isLoading", true);
                component.set("v.selectAll",false);
                component.set("v.selectedInvoices", new Map());
                component.set("v.selectedItemCount", 0);
				helper.queryPendingJobs(component);
			} else {
				console.log("Error: "+response.getError()[0].message);
			}
		});
		$A.enqueueAction(action);
	}
})