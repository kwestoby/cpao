@isTest
public class CPAO_Milestone_ContactTest {
    
    public static List<Account> factoryAccounts;
    public static List<Contact> factoryContacts;
    public static  List<CPAO_Licence__c> factoryActiveLicences;
    public static Integer NUMBER_CONTACTS = 1;
    
    @testSetup
    public static void setup(){
        factoryAccounts = CPAO_TestFactory.createAccounts(2);
        insert factoryAccounts;
        factoryContacts= CPAO_TestFactory.createContacts(factoryAccounts[0],NUMBER_CONTACTS);
        insert factoryContacts;
        
    }
    
    @isTest
    public static void beforeUpdateTest(){
          factoryContacts = [SELECT Id, LastName FROM Contact];
        test.startTest();
            for(Contact contact : factoryContacts){
                contact.LastName = 'change';
            }
       	 update factoryContacts;
        test.stopTest();
        Integer historyTrack = [SELECT COUNT(Id) FROM CPAO_MilestoneAndHistory__c].size();
        System.assert(NUMBER_CONTACTS == historyTrack);
        
    }
    
}