<?xml version="1.0" encoding="UTF-8"?>
<CustomMetadata xmlns="http://soap.sforce.com/2006/04/metadata" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xmlns:xsd="http://www.w3.org/2001/XMLSchema">
    <label>Full-Time University Attendance</label>
    <protected>false</protected>
    <values>
        <field>AMD_or_CPD__c</field>
        <value xsi:type="xsd:string">AMD</value>
    </values>
    <values>
        <field>CPAO_Explanation__c</field>
        <value xsi:type="xsd:string">A 50% reduction is granted to members who are attending a recognized university on a full-time basis.  The program or courses do not need to be accounting related.  Subject to meeting the criteria each year, there are no limits to how many reductions may be granted to a member.  Where the dates of university attendance overlap with the dates marking the end of one CPA Ontario fiscal year and the beginning of the next, a dues reduction will apply only to one year’s annual membership dues.
Your fee payment reduction is subject to audit by CPA Ontario.  You may be contacted by CPA Ontario to provide a letter from the university confirming full-time enrolment in a program for at least seven months in a single, 12-month period.</value>
    </values>
    <values>
        <field>CPAO_Percent_Off__c</field>
        <value xsi:type="xsd:double">50.0</value>
    </values>
    <values>
        <field>Explanation_Third_Year__c</field>
        <value xsi:nil="true"/>
    </values>
    <values>
        <field>Full_Name__c</field>
        <value xsi:type="xsd:string">Full-Time University Attendance</value>
    </values>
</CustomMetadata>
