@isTest
private class CPAO_VenueHandlerTest{

	public static void dataInitialize(Integer numberOfEvents, Integer numberOfVenues,Boolean primaryVenue){
		CPAO_TestFactory.automationControlSetup();
		List<EventApi__Event__c> events=CPAO_TestFactory.createEvents(numberOfEvents);
		insert events;
		List<EventApi__Venue__c> venues = CPAO_TestFactory.createVenues(1,events[0], primaryVenue);
		insert venues;
	}


	//This tests that when a venue is created and is marked as the primary, the associated event's 
	//location lookup gets populated with a reference to the newly created venue
	@isTest public static void test_changeVenueLookup() {
		dataInitialize(1,1,true);
		List<EventApi__Venue__c> venues= [SELECT Id,Name FROM EventApi__Venue__c
		WHERE Name LIKE 'UnitTestVenue%'];
		List<EventApi__Event__c> events = [SELECT Id,CPAO_Location__c FROM EventApi__Event__c 
		WHERE Name LIKE 'UnitTestEvent%'];
		System.assertEquals(events[0].CPAO_Location__c,venues[0].Id);
		
	}

	//This tests that when a venue that was not the primary venue is updated and is now marked 
	//as the primar. The associated event's location lookup gets populated with a reference to 
	//the  updated venue
	@isTest public static void test_changeVenueLookup2() {
		dataInitialize(1,1,false);//the venue is initially not the primary venue
		List<EventApi__Venue__c> venues= [SELECT Id,Name,EventApi__Is_Primary_Venue__c FROM
		EventApi__Venue__c WHERE Name LIKE 'UnitTestVenue%'];
		venues[0].EventApi__Is_Primary_Venue__c=true; // updates the venue to become prmary
		update(venues[0]);
	
		List<EventApi__Event__c> events = [SELECT Id,CPAO_Location__c FROM EventApi__Event__c 
		WHERE Name LIKE 'UnitTestEvent%'];
		System.assertEquals(events[0].CPAO_Location__c,venues[0].Id);
		
	}


    //This tests that when a venue is created and is not marked as the primary, the associated event's 
	//location lookup does not gets populated with a reference to the newly created venue because
	//it is not a primary venue for the event
	@isTest public static void test_UnchangedVenueLookup() {
		dataInitialize(1,1,false);
		List<EventApi__Venue__c> venues= [SELECT Id,Name,EventApi__Is_Primary_Venue__c FROM 
		EventApi__Venue__c WHERE Name LIKE 'UnitTestVenue%'];
		List<EventApi__Event__c> events = [SELECT Id,CPAO_Location__c FROM EventApi__Event__c
		WHERE Name LIKE 'UnitTestEvent%'];
		System.assertNotEquals(events[0].CPAO_Location__c,venues[0].Id);
		System.assertEquals(null,events[0].CPAO_Location__c);
	}

}