({
	doInit : function(component, helper) {
        var action = component.get("c.getCurrentAPF");
        action.setParams({
            recordId : component.get("v.recordId")
        });
        action.setCallback(this, function(response){
            var state = response.getState();
            if(state === "SUCCESS") {
                var result = response.getReturnValue();

                var currentAPF = result.currentAPF;
                var status = result.contactStatus;
                var account = result.account;
                var instructions = result.instructions;
                var step = result.step;
                var mailingPicklist = result.mailingPicklist;
                account.sobjectType = "Account";

                component.set("v.currentAPF", currentAPF);
                component.set("v.instructions", instructions);
                component.set("v.mailingPicklist", mailingPicklist);
                component.set("v.account", account);

                helper.initializeSelectedStep(component, helper, step);
            }
        });
        $A.enqueueAction(action);
    },

    initializeSelectedStep : function(component, helper, curStep) {
        var selectedStep = "step1";
        var steps = helper.getStepStructure();
        if(steps[curStep]) {
            selectedStep = curStep;
        }
        component.set("v.selectedStep", selectedStep);
    },

    handleStep : function(component, helper, direction) {
		var isNext = direction === "Next";
        var isQuit = direction === "Quit";

        var currentStep = component.get("v.selectedStep");
        var selectedStep;
        var steps = this.getStepStructure();
        
        if(isQuit) {
            helper.handleQuit(component);
        }
        else if(isNext && steps[currentStep] && steps[currentStep].next) {
            selectedStep = steps[currentStep].next;
        }
        else if(!isNext && steps[currentStep] && steps[currentStep].prev) {
            selectedStep = steps[currentStep].prev;
        }

        if(selectedStep) {
            component.set("v.selectedStep", selectedStep);
            helper.saveLatestStep(component, helper, selectedStep);
        }
	},

    saveLatestStep : function(component, helper, step) {
        var action = component.get("c.updateStep");
        action.setParams({
            recordId : component.get("v.currentAPF.Id"),
            step : step
        });
        action.setCallback(this, function(response){
            var state = response.getState();
            if(state === "SUCCESS") {
                console.log('hooray');
            }
        });
        $A.enqueueAction(action);
    },

    getStepStructure : function() {
        var steps = { 
            step1: {current:"step1", next : "step2", prev : null},
            step2: {current:"step2", next : "step3", prev : "step1"},
            step3: {current:"step3", next : "step4", prev : "step2"},
            step4: {current:"step4", next : "step5", prev : "step3"},
            step5: {current:"step5", next : "step6", prev : "step4"},
            step6: {current:"step6", next : null,    prev : "step5"},
        };
        return steps;
    },

    handleQuit : function(component) {
        component.set("v.selectedStep", "step1");
    },
})