<?xml version="1.0" encoding="UTF-8"?>
<CustomMetadata xmlns="http://soap.sforce.com/2006/04/metadata" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xmlns:xsd="http://www.w3.org/2001/XMLSchema">
    <label>0400-2</label>
    <protected>false</protected>
    <values>
        <field>AMD_Location_Based_Reduction__c</field>
        <value xsi:type="xsd:string">Category 0400 - Member of another provincial body. Membership Fees are paid to another province and CPA Canada Fee is paid to another province</value>
    </values>
    <values>
        <field>CPA_Canada_Fee__c</field>
        <value xsi:type="xsd:double">0.0</value>
    </values>
    <values>
        <field>CPA_Ontario_Fee__c</field>
        <value xsi:type="xsd:double">290.0</value>
    </values>
    <values>
        <field>Live_In_Canada_Bermuda__c</field>
        <value xsi:type="xsd:string">Yes</value>
    </values>
    <values>
        <field>Paying_CPAC_Through_CPAO__c</field>
        <value xsi:type="xsd:string">No</value>
    </values>
    <values>
        <field>Paying_Provincial_Through_Ontario__c</field>
        <value xsi:type="xsd:string">No</value>
    </values>
</CustomMetadata>
