({
    showToast : function(title, message, type) {
   		console.log('start of showToast method');
	  	var toastEvent = $A.get("e.force:showToast");
	  	console.log('before setparams');
	  	toastEvent.setParams({
	        "title": title,
	        "message": message,
	        "type" : type
	  	});
	  	console.log('before toast fire');
	  	toastEvent.fire();
	  	console.log('after toast fire');
	},

	confirmRetirementUnemployment : function(component, event, helper, type) {
		console.log('retirement method');
		var allRelationships = component.get("v.listOfRelationships");
	    for (var i = 0; i < allRelationships.length; i++) {
	    	console.log('allRelationships[i]:');
	    	console.log(allRelationships[i]);

	        if(allRelationships[i].CPAO_End_Date__c == null || allRelationships[i].CPAO_End_Date__c == undefined){
	        	component.set("v.confirmationMessage"," Error:Please mark an end date for all employment entries");
	          helper.showToast("Error", "please mark an end date for all employment entries", "error");
	          return;
	        }
	    }
	    var action = component.get("c.retireUnemployEmployee");
	    action.setParams({
	    	"employmentType": type
	      	}); 

	    action.setCallback(this, function(response){

	        var state = response.getState();
	        console.log('state'+state);
	        if (state === "SUCCESS") {
	        	if(type == 'Retirement'){
	        		if(response.getReturnValue() == true){
			            helper.showToast("Success", "Retirement set successfully", "success");
			        } else {
			            helper.showToast("Error", "Error setting retirement status", "error");
			        }
	        	} else if(type == 'Unemployment'){
	        		if(response.getReturnValue() == true){
			            helper.showToast("Success", "Unemployment set successfully", "success");
			        } else {
			            helper.showToast("Error", "Error setting unemployment status", "error");
			        }
	        	}
	        	
	        }else{
	        	 console.log('there was an error');
	        }
	      });
	       $A.enqueueAction(action);
	}
})