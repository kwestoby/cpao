<?xml version="1.0" encoding="UTF-8"?>
<CustomObject xmlns="http://soap.sforce.com/2006/04/metadata">
    <actionOverrides>
        <actionName>Accept</actionName>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>CancelEdit</actionName>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>Clone</actionName>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>Delete</actionName>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>Edit</actionName>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>List</actionName>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>New</actionName>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>SaveEdit</actionName>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>Tab</actionName>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>View</actionName>
        <type>Default</type>
    </actionOverrides>
    <allowInChatterGroups>false</allowInChatterGroups>
    <compactLayoutAssignment>SYSTEM</compactLayoutAssignment>
    <deploymentStatus>Deployed</deploymentStatus>
    <description>Object to store and track the Account Contact Relationship</description>
    <enableActivities>true</enableActivities>
    <enableBulkApi>true</enableBulkApi>
    <enableChangeDataCapture>false</enableChangeDataCapture>
    <enableFeeds>false</enableFeeds>
    <enableHistory>true</enableHistory>
    <enableReports>true</enableReports>
    <enableSearch>true</enableSearch>
    <enableSharing>true</enableSharing>
    <enableStreamingApi>true</enableStreamingApi>
    <externalSharingModel>ReadWrite</externalSharingModel>
    <fields>
        <fullName>CPAO_AccountNameFormula__c</fullName>
        <description>This field is used to show the account name directly in the portal(in a lightning component) without querying the account object</description>
        <externalId>false</externalId>
        <formula>CPAO_Account__r.Name</formula>
        <formulaTreatBlanksAs>BlankAsZero</formulaTreatBlanksAs>
        <label>Account Name Formula</label>
        <required>false</required>
        <trackHistory>false</trackHistory>
        <trackTrending>false</trackTrending>
        <type>Text</type>
        <unique>false</unique>
    </fields>
    <fields>
        <fullName>CPAO_Account__c</fullName>
        <deleteConstraint>Restrict</deleteConstraint>
        <externalId>false</externalId>
        <label>Account Name</label>
        <referenceTo>Account</referenceTo>
        <relationshipLabel>Account Contact Relationships</relationshipLabel>
        <relationshipName>Account_Contact_Relationships</relationshipName>
        <required>true</required>
        <trackHistory>true</trackHistory>
        <trackTrending>false</trackTrending>
        <type>Lookup</type>
    </fields>
    <fields>
        <fullName>CPAO_Business_Unit__c</fullName>
        <externalId>false</externalId>
        <label>Business Unit</label>
        <required>false</required>
        <trackHistory>true</trackHistory>
        <trackTrending>false</trackTrending>
        <type>Picklist</type>
        <valueSet>
            <restricted>true</restricted>
            <valueSetDefinition>
                <sorted>false</sorted>
                <value>
                    <fullName>Assurance, Business Valuation</fullName>
                    <default>false</default>
                    <label>Assurance, Business Valuation</label>
                </value>
                <value>
                    <fullName>Finance ( including Controllership &amp; Treasury)</fullName>
                    <default>false</default>
                    <label>Finance ( including Controllership &amp; Treasury)</label>
                </value>
                <value>
                    <fullName>Financial Planning</fullName>
                    <default>false</default>
                    <label>Financial Planning</label>
                </value>
                <value>
                    <fullName>General Management</fullName>
                    <default>false</default>
                    <label>General Management</label>
                </value>
                <value>
                    <fullName>Human Resources</fullName>
                    <default>false</default>
                    <label>Human Resources</label>
                </value>
                <value>
                    <fullName>Information Systems and Control</fullName>
                    <default>false</default>
                    <label>Information Systems and Control</label>
                </value>
                <value>
                    <fullName>Information Technology</fullName>
                    <default>false</default>
                    <label>Information Technology</label>
                </value>
                <value>
                    <fullName>Insolvency/Business Recover Services</fullName>
                    <default>false</default>
                    <label>Insolvency/Business Recover Services</label>
                </value>
                <value>
                    <fullName>Internal Audit</fullName>
                    <default>false</default>
                    <label>Internal Audit</label>
                </value>
                <value>
                    <fullName>Investigative &amp; Forensic Accounting</fullName>
                    <default>false</default>
                    <label>Investigative &amp; Forensic Accounting</label>
                </value>
                <value>
                    <fullName>Management Consulting</fullName>
                    <default>false</default>
                    <label>Management Consulting</label>
                </value>
                <value>
                    <fullName>Operations</fullName>
                    <default>false</default>
                    <label>Operations</label>
                </value>
                <value>
                    <fullName>Organizational Effectiveness Control and Risk Management</fullName>
                    <default>false</default>
                    <label>Organizational Effectiveness Control and Risk Management</label>
                </value>
                <value>
                    <fullName>Other (Specify in Notes)</fullName>
                    <default>false</default>
                    <label>Other (Specify in Notes)</label>
                </value>
                <value>
                    <fullName>Performance Measurement (including Accounting)</fullName>
                    <default>false</default>
                    <label>Performance Measurement (including Accounting)</label>
                </value>
                <value>
                    <fullName>Regulatory/Compliance</fullName>
                    <default>false</default>
                    <label>Regulatory/Compliance</label>
                </value>
                <value>
                    <fullName>Sales &amp; Marketing</fullName>
                    <default>false</default>
                    <label>Sales &amp; Marketing</label>
                </value>
                <value>
                    <fullName>Taxation</fullName>
                    <default>false</default>
                    <label>Taxation</label>
                </value>
            </valueSetDefinition>
        </valueSet>
    </fields>
    <fields>
        <fullName>CPAO_Contact__c</fullName>
        <deleteConstraint>Restrict</deleteConstraint>
        <externalId>false</externalId>
        <label>Contact Name</label>
        <referenceTo>Contact</referenceTo>
        <relationshipLabel>Account Contact Relationships</relationshipLabel>
        <relationshipName>Account_Contact_Relationships</relationshipName>
        <required>true</required>
        <trackHistory>true</trackHistory>
        <trackTrending>false</trackTrending>
        <type>Lookup</type>
    </fields>
    <fields>
        <fullName>CPAO_End_Date__c</fullName>
        <description>End Date of the Relationship</description>
        <externalId>false</externalId>
        <label>End Date</label>
        <required>false</required>
        <trackHistory>true</trackHistory>
        <trackTrending>false</trackTrending>
        <type>Date</type>
    </fields>
    <fields>
        <fullName>CPAO_Local_Senior_Officer__c</fullName>
        <deleteConstraint>Restrict</deleteConstraint>
        <externalId>false</externalId>
        <label>Local Senior Officer</label>
        <referenceTo>Contact</referenceTo>
        <relationshipLabel>Account Contact Relationships (Local Senior Officer)</relationshipLabel>
        <relationshipName>Account_Contact_Relationships1</relationshipName>
        <required>false</required>
        <trackHistory>false</trackHistory>
        <trackTrending>false</trackTrending>
        <type>Lookup</type>
    </fields>
    <fields>
        <fullName>CPAO_Position_Category__c</fullName>
        <externalId>false</externalId>
        <label>Position Category</label>
        <required>false</required>
        <trackHistory>true</trackHistory>
        <trackTrending>false</trackTrending>
        <type>Picklist</type>
        <valueSet>
            <restricted>true</restricted>
            <valueSetDefinition>
                <sorted>false</sorted>
                <value>
                    <fullName>Accountant/Senior Accountant/Analyst</fullName>
                    <default>false</default>
                    <label>Accountant/Senior Accountant/Analyst</label>
                </value>
                <value>
                    <fullName>Assistant Deputy Minister</fullName>
                    <default>false</default>
                    <label>Assistant Deputy Minister</label>
                </value>
                <value>
                    <fullName>Associate Partner</fullName>
                    <default>false</default>
                    <label>Associate Partner</label>
                </value>
                <value>
                    <fullName>Auditor/Internal Auditor</fullName>
                    <default>false</default>
                    <label>Auditor/Internal Auditor</label>
                </value>
                <value>
                    <fullName>CFO/VP Finance</fullName>
                    <default>false</default>
                    <label>CFO/VP Finance</label>
                </value>
                <value>
                    <fullName>CIO</fullName>
                    <default>false</default>
                    <label>CIO</label>
                </value>
                <value>
                    <fullName>Consultant</fullName>
                    <default>false</default>
                    <label>Consultant</label>
                </value>
                <value>
                    <fullName>Controller</fullName>
                    <default>false</default>
                    <label>Controller</label>
                </value>
                <value>
                    <fullName>COO</fullName>
                    <default>false</default>
                    <label>COO</label>
                </value>
                <value>
                    <fullName>Director/Assistant Director</fullName>
                    <default>false</default>
                    <label>Director/Assistant Director</label>
                </value>
                <value>
                    <fullName>Employee</fullName>
                    <default>false</default>
                    <label>Employee</label>
                </value>
                <value>
                    <fullName>Financial Advisor</fullName>
                    <default>false</default>
                    <label>Financial Advisor</label>
                </value>
                <value>
                    <fullName>Manager</fullName>
                    <default>false</default>
                    <label>Manager</label>
                </value>
                <value>
                    <fullName>National Managing Partner</fullName>
                    <default>false</default>
                    <label>National Managing Partner</label>
                </value>
                <value>
                    <fullName>Office Managing Partner</fullName>
                    <default>false</default>
                    <label>Office Managing Partner</label>
                </value>
                <value>
                    <fullName>Other</fullName>
                    <default>false</default>
                    <label>Other</label>
                </value>
                <value>
                    <fullName>Part-time Partner</fullName>
                    <default>false</default>
                    <label>Part-time Partner</label>
                </value>
                <value>
                    <fullName>Part-time Practice exclusively</fullName>
                    <default>false</default>
                    <label>Part-time Practice exclusively</label>
                </value>
                <value>
                    <fullName>Part-time practice in addition to other employment</fullName>
                    <default>false</default>
                    <label>Part-time practice in addition to other employment</label>
                </value>
                <value>
                    <fullName>Partner</fullName>
                    <default>false</default>
                    <label>Partner</label>
                </value>
                <value>
                    <fullName>Partner registered as a professional corporation</fullName>
                    <default>false</default>
                    <label>Partner registered as a professional corporation</label>
                </value>
                <value>
                    <fullName>President/CEO/Owner</fullName>
                    <default>false</default>
                    <label>President/CEO/Owner</label>
                </value>
                <value>
                    <fullName>Principal</fullName>
                    <default>false</default>
                    <label>Principal</label>
                </value>
                <value>
                    <fullName>Professor/Instructor</fullName>
                    <default>false</default>
                    <label>Professor/Instructor</label>
                </value>
                <value>
                    <fullName>Regional Managing Partner</fullName>
                    <default>false</default>
                    <label>Regional Managing Partner</label>
                </value>
                <value>
                    <fullName>Senior Manager</fullName>
                    <default>false</default>
                    <label>Senior Manager</label>
                </value>
                <value>
                    <fullName>Senior Partner</fullName>
                    <default>false</default>
                    <label>Senior Partner</label>
                </value>
                <value>
                    <fullName>Sole Practitioner in association with one or more other practices</fullName>
                    <default>false</default>
                    <label>Sole Practitioner in association with one or more other practices</label>
                </value>
                <value>
                    <fullName>Sole Practitioner not associated with any other practice</fullName>
                    <default>false</default>
                    <label>Sole Practitioner not associated with any other practice</label>
                </value>
                <value>
                    <fullName>Supervisor</fullName>
                    <default>false</default>
                    <label>Supervisor</label>
                </value>
                <value>
                    <fullName>Team Leader</fullName>
                    <default>false</default>
                    <label>Team Leader</label>
                </value>
                <value>
                    <fullName>Treasurer</fullName>
                    <default>false</default>
                    <label>Treasurer</label>
                </value>
                <value>
                    <fullName>Vice-President</fullName>
                    <default>false</default>
                    <label>Vice-President</label>
                </value>
            </valueSetDefinition>
        </valueSet>
    </fields>
    <fields>
        <fullName>CPAO_Primary_Employer__c</fullName>
        <externalId>false</externalId>
        <label>Primary Employer</label>
        <required>false</required>
        <trackHistory>false</trackHistory>
        <trackTrending>false</trackTrending>
        <type>Picklist</type>
        <valueSet>
            <restricted>true</restricted>
            <valueSetName>CPAO_Yes_No</valueSetName>
        </valueSet>
    </fields>
    <fields>
        <fullName>CPAO_Relationship_to_Account__c</fullName>
        <description>Relationship to the Account</description>
        <externalId>false</externalId>
        <label>Relationship to Account</label>
        <required>true</required>
        <trackHistory>true</trackHistory>
        <trackTrending>false</trackTrending>
        <type>Picklist</type>
        <valueSet>
            <restricted>true</restricted>
            <valueSetDefinition>
                <sorted>false</sorted>
                <value>
                    <fullName>Ambassador</fullName>
                    <default>false</default>
                    <label>Ambassador</label>
                </value>
                <value>
                    <fullName>Board Member</fullName>
                    <default>false</default>
                    <label>Board Member</label>
                </value>
                <value>
                    <fullName>Employee</fullName>
                    <default>false</default>
                    <label>Employee</label>
                </value>
                <value>
                    <fullName>Instructor</fullName>
                    <default>false</default>
                    <label>Instructor</label>
                </value>
                <value>
                    <fullName>LSO</fullName>
                    <default>false</default>
                    <label>Local Senior Officer</label>
                </value>
                <value>
                    <fullName>Member</fullName>
                    <default>false</default>
                    <label>Member</label>
                </value>
                <value>
                    <fullName>Mentor</fullName>
                    <default>false</default>
                    <label>Mentor</label>
                </value>
                <value>
                    <fullName>Prospect/Lead</fullName>
                    <default>false</default>
                    <label>Prospect/Lead</label>
                </value>
                <value>
                    <fullName>Shareholder</fullName>
                    <default>false</default>
                    <label>Shareholder</label>
                </value>
                <value>
                    <fullName>Sponsor</fullName>
                    <default>false</default>
                    <label>Sponsor</label>
                </value>
                <value>
                    <fullName>Student</fullName>
                    <default>false</default>
                    <label>Student</label>
                </value>
                <value>
                    <fullName>Supplier</fullName>
                    <default>false</default>
                    <label>Supplier</label>
                </value>
                <value>
                    <fullName>Training Officer</fullName>
                    <default>false</default>
                    <label>Training Officer</label>
                </value>
                <value>
                    <fullName>Volunteer</fullName>
                    <default>false</default>
                    <label>Volunteer</label>
                </value>
                <value>
                    <fullName>Retirement</fullName>
                    <default>false</default>
                    <label>Retirement</label>
                </value>
                <value>
                    <fullName>Unemployment</fullName>
                    <default>false</default>
                    <label>Unemployment</label>
                </value>
                <value>
                    <fullName>Director</fullName>
                    <default>false</default>
                    <label>Director</label>
                </value>
                <value>
                    <fullName>Officer</fullName>
                    <default>false</default>
                    <label>Officer</label>
                </value>
                <value>
                    <fullName>Sole Proprietor</fullName>
                    <default>false</default>
                    <label>Sole Proprietor</label>
                </value>
                <value>
                    <fullName>Partner</fullName>
                    <default>false</default>
                    <label>Partner</label>
                </value>
                <value>
                    <fullName>Designated Administrator</fullName>
                    <default>false</default>
                    <label>Designated Administrator</label>
                </value>
                <value>
                    <fullName>Practice Inspection Contact</fullName>
                    <default>false</default>
                    <label>Practice Inspection Contact</label>
                </value>
                <value>
                    <fullName>Discipline Contact</fullName>
                    <default>false</default>
                    <label>Discipline Contact</label>
                </value>
                <value>
                    <fullName>Discipline Contact Alternative</fullName>
                    <default>false</default>
                    <label>Discipline Contact Alternative</label>
                </value>
            </valueSetDefinition>
        </valueSet>
    </fields>
    <fields>
        <fullName>CPAO_Start_Date__c</fullName>
        <externalId>false</externalId>
        <label>Start Date</label>
        <required>false</required>
        <trackHistory>true</trackHistory>
        <trackTrending>false</trackTrending>
        <type>Date</type>
    </fields>
    <fields>
        <fullName>CPAO_Status__c</fullName>
        <description>Status of the Relationship, if it is Inactive or Active</description>
        <externalId>false</externalId>
        <formula>IF( CPAO_End_Date__c  &lt;=   TODAY() , &quot;Inactive&quot;, &quot;Active&quot;)</formula>
        <formulaTreatBlanksAs>BlankAsZero</formulaTreatBlanksAs>
        <label>Status</label>
        <required>false</required>
        <trackHistory>false</trackHistory>
        <trackTrending>false</trackTrending>
        <type>Text</type>
        <unique>false</unique>
    </fields>
    <fields>
        <fullName>CPAO_Text__c</fullName>
        <externalId>false</externalId>
        <label>Position Title</label>
        <required>false</required>
        <trackHistory>true</trackHistory>
        <trackTrending>false</trackTrending>
        <type>TextArea</type>
    </fields>
    <label>Account Contact Relationship</label>
    <listViews>
        <fullName>All</fullName>
        <columns>NAME</columns>
        <columns>CPAO_Relationship_to_Account__c</columns>
        <filterScope>Everything</filterScope>
        <label>All</label>
    </listViews>
    <nameField>
        <displayFormat>{000000}</displayFormat>
        <label>Relationship Number</label>
        <trackHistory>false</trackHistory>
        <type>AutoNumber</type>
    </nameField>
    <pluralLabel>Account Contact Relationships</pluralLabel>
    <searchLayouts/>
    <sharingModel>ReadWrite</sharingModel>
    <startsWith>Vowel</startsWith>
    <validationRules>
        <fullName>CPAO_AccCont_Relationship_Employee</fullName>
        <active>true</active>
        <description>When &quot;Relationship to Account&quot; = &quot;Employee&quot;, then the following fields are required: &quot;Title&quot;, &quot;Position Category&quot; &quot;Business Unit&quot;</description>
        <errorConditionFormula>ISPICKVAL( CPAO_Relationship_to_Account__c , &apos;Employee&apos;)&amp;&amp;  OR( ISBLANK( CPAO_Text__c ),  ISBLANK(TEXT (CPAO_Position_Category__c )), ISBLANK(TEXT( CPAO_Business_Unit__c )))
&amp;&amp;
$Setup.CPAO_Automation_Control__c.CPAO_Account_Contact_Relationship_c__c</errorConditionFormula>
        <errorMessage>When &apos;Relationship to Account&apos; = &quot;Employee&quot; then the folllowing fields are required: Title, Position Category, Business Unit</errorMessage>
    </validationRules>
    <validationRules>
        <fullName>CPAO_End_Date</fullName>
        <active>true</active>
        <description>End Date should always be greater than or equal to the Start Date</description>
        <errorConditionFormula>CPAO_End_Date__c  &lt;  CPAO_Start_Date__c
&amp;&amp; 
$Setup.CPAO_Automation_Control__c.CPAO_Account_Contact_Relationship_c__c</errorConditionFormula>
        <errorMessage>End Date should be greater than or equal to the Start Date</errorMessage>
    </validationRules>
    <visibility>Public</visibility>
</CustomObject>
