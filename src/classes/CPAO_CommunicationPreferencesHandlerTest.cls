@isTest
public with sharing class CPAO_CommunicationPreferencesHandlerTest {
	public static List<Contact> communicationPreferences;
	public static List<contact>contacts;

	public static void dataInitialize(Integer amount,String consentType,Date consentDate){
		CPAO_TestFactory.automationControlSetup();
		List<Account>accounts = CPAO_TestFactory.createAccounts(1);
		insert accounts; 
		contacts = CPAO_TestFactory.createContacts(accounts[0],1);
		insert contacts;
		communicationPreferences=CPAO_TestFactory.createCommPreferenceImplied(1,consentType,contacts,consentDate);
		update(communicationPreferences);
	}
	
	 // This test that a user has a communication preference record an try to create
	 //a second one, the system shows an error and doesnt create the second one
	 @isTest
	 public static void restrictToOnePreference(){
		dataInitialize(2,'Implied', Date.newInstance(2017, 6, 9));
		Set<Id> contactid= new Set<Id> ();
		contactid.add(contacts[0].id);
		List<Contact> result = CPAO_CommunicationPreferenceSelector.getCommunicationPreferencesByContactId(contactid);
		System.assert(result.size()== 1);
		
	
	}
}