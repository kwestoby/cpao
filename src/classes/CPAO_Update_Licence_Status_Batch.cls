global class CPAO_Update_Licence_Status_Batch implements Database.Batchable<sObject> {
	  
	 // This batch job class looks for CPAO_Licence__c records
	// if theExpiry Date is less or equals to today, the  batch job
	// updates the status to revoked and the substatus to licence expiration
  
  global Database.QueryLocator start(Database.BatchableContext BC) {

    String query='SELECT Id,CPAO_Effective_Date__c,CPAO_Expiry_Date__c,CPAO_Status__c,CPAO_Sub_Status__c FROM CPAO_Licence__c WHERE CPAO_Expiry_Date__c <=TODAY';
    return Database.getQueryLocator(query);
  }

     global void execute(Database.BatchableContext BC, List<CPAO_Licence__c>  licences) {
    	for(CPAO_Licence__c licence:licences){
     		licence.CPAO_Status__c = 'Revoked';
         	licence.CPAO_Sub_Status__c = 'Licence Expiration';
       }
       update licences;
  }
  
  global void finish(Database.BatchableContext BC) {
    
  }
}