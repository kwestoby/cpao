public without sharing class CPAO_UserSelector {
	
	public static User currentUser;

	public static User getCurrentUser() {
		if(Test.isRunningTest()){
			currentUser = null;
		}
		if(currentUser == null){
			currentUser = [SELECT ContactId,Name, Id, Contact.AccountId, Contact.CPAO_Currently_acting_as__c, Contact.CPAO_AMD_Continuous_Waiver__c,
								 Contact.CPAO_CPD_Continuous_Exemption__c, Contact.MailingCountry, Contact.CPAO_Resignation_Discount_Percentage__c
							FROM User WHERE Id = : UserInfo.getUserId()];
		}
		return currentUser;
	}

	public static List<User> getUsersWithContactList(Set<Contact> contacts) {
		return [SELECT Id, Email, ContactId FROM User WHERE ContactId IN :contacts];
	}
}