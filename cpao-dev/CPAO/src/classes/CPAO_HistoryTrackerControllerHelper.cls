public class CPAO_HistoryTrackerControllerHelper {
    
    
    public static void historyTrackerControllerValidation(List<CPAO_History_Tracker_Controller__c>  histTrackObjList){
        List<String> triggerfields = new List<String>();
        List<String> copyfields = new List<String>();
        Map<String,String> fieldMap = new Map<String,String>();
        String recordType;
        String objectType;
        Map<String, Schema.SObjectField> objectFieldsTargetObject = Schema.getGlobalDescribe().get('CPAO_MilestoneAndHistory__c').getDescribe().fields.getMap();
        
        for(CPAO_History_Tracker_Controller__c historytrackerController: histTrackObjList){
            triggerfields = historytrackerController.CPAO_Trigger_Fields__c.split(',');
            if(historytrackerController.CPAO_Fields_to_Copy__c == null && historytrackerController.CPAO_Fields_to_Map__c == null){
                historytrackerController.addError('"Fields to Copy" or "Fields to Map" must be filled.');
                break;
            }
            
            if(historytrackerController.CPAO_Fields_to_Copy__c != null){
           	   copyfields  = historytrackerController.CPAO_Fields_to_Copy__c.split(',');
            }
            if(historytrackerController.CPAO_Fields_to_Map__c != null){
          		  fieldMap = textToMap(';',',',historytrackerController.CPAO_Fields_to_Map__c);
            }
            objectType = historytrackerController.CPAO_ObjectType__c;
            recordType = historytrackerController.Record_Type__c;
			            
            if(fieldMap == null){
                historytrackerController.CPAO_Fields_to_Map__c.addError('The fields are in the wrong format.');
                break;
            }
            
            Map<String, Schema.SObjectField> objectFieldsSourceObject = Schema.getGlobalDescribe().get(objectType).getDescribe().fields.getMap();
            for(String APIName: fieldMap.keySet()){
                if(!objectFieldsSourceObject.containsKey(APIName)){
                    historytrackerController.CPAO_Fields_to_Map__c.addError('The field ' + APIName + ' does not exists in the object ' + objectType + '.');
                } 
                if(!objectFieldsTargetObject.containsKey(fieldMap.get(APIName))){
                    historytrackerController.CPAO_Fields_to_Map__c.addError('The field ' + fieldMap.get(APIName) + ' does not exists in the object CPAO_MilestoneAndHistory__c.');
                }
            }
            for(String copyField : copyfields){
                if(!objectFieldsSourceObject.containsKey(copyField)){
                    historytrackerController.CPAO_Fields_to_Copy__c.addError('The field ' + copyField + ' does not exists in the object ' + objectType + '.');
                } 
                if(!objectFieldsTargetObject.containsKey(copyField)){
                    historytrackerController.CPAO_Fields_to_Copy__c.addError('The field '+ copyField+' does not exists in the object CPAO_MilestoneAndHistory__c.');          
                }
            }
            
            for(String triggerField : triggerfields){
                if(!objectFieldsTargetObject.containsKey(triggerField)){
                    historytrackerController.CPAO_Trigger_Fields__c.addError('The field ' + triggerField + ' does not exists in the object ' + objectType + '.');
                }
            }
        }
    }

    private static Map<String,String> textToMap(String separatorRecord, String separatorValue, String text){
        Map<String,String> fieldMap = new Map<String,String>();
        List<String> listValues = new List<String>();
        listValues = text.split(';');     
        for(String value: listValues){
            if(value.split(',').size() != 2){
               return null; 
            }
            fieldMap.put(value.split(',')[0],value.split(',')[1]);               
        }
        return fieldMap;
    }
    private static Map<String,String> mixListAndMap(Map<String,String> mapfields, List<String> listFields ){
        for(String field: listFields)
        {
            if(!mapfields.containsKey(field)){
                mapFields.put(field,field);
            }
        }  
        return  mapfields;  
    }
    
}