({
	 clickOnNextButton : function(component,event,helper){
        var componentNameInEvent=event.getParam("componentName");
        if(componentNameInEvent==component.get('v.nameOfThisComponent')){
            if(event.getParam("nextStepName")=='Exit'){ 
                component.set('v.shouldSaveAndQuit',true);
            }

            //var ppad_req= component.find('ppad_req');
            var validForm = component.find('PALForm').reduce(function (validSoFar, inputCmp) {
            	  var ppad=  component.get('v.currentPAL.CPAO_Practising_Public_Accounting_Dec__c');  
                // Displays error messages for invalid fields
                if(ppad==undefined){
                    //$A.util.addClass(ppad_req, 'slds-show');
                    component.set('v.showErrorMsg',true);
                }else{
                   // $A.util.removeClass(ppad_req, 'slds-show');
                     component.set('v.showErrorMsg',false);
                }
            
                inputCmp.showHelpMessageIfInvalid();
                return validSoFar && inputCmp.get('v.validity').valid && ppad!=undefined;
            }, true);
            
            if(validForm){
                //$A.util.removeClass(ppad_req, 'slds-show');
                component.set('v.showErrorMsg',false);
                var showSpinnerEvent = component.getEvent("showSpinner");
                showSpinnerEvent.fire();
                if(component.get('v.associatedWithCPAOFirm')){
                	component.set('v.currentPAL.CPAO_PPAD_Special_Circumstances__c',null);
                }
                helper.upsertPAL(component);
                
            }
        }  	   
        
    }
})