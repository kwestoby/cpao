/**
 * @author Sachet Khanal (sakhanal@deloitte.com)
 * @version 1.0.0
 * @description Test class for CPAO_BulkInvoiceWriteOffBatch class.
 *
 *
 **/ 
@isTest
public class CPAO_BulkInvoiceWriteOffBatchTest {
	@TestSetup
	static void setupData() {
		Contact con = new Contact(FirstName = 'Test', LastName = 'Contact', CPAO_CPA_Contact_Type__c = 'Member', CPAO_CPA_Status__c = 'Active');
		insert con;
		List<OrderApi__Sales_Order__c> salesOrders = new List<OrderApi__Sales_Order__c> ();
		for (Integer i = 0; i< 10; i++) {
			salesOrders.add(new OrderApi__Sales_Order__c(OrderApi__Entity__c = 'Contact',
			                                             OrderApi__Contact__c = con.Id,
			                                             OrderApi__Posted_Date__c = System.today() - 5,
			                                             OrderApi__Posting_Entity__c = 'Invoice',
			                                             OrderApi__Posting_Status__c = 'Posted'));
		}
		insert salesOrders;
		List<OrderApi__Sales_Order_Line__c> salesOrderLines = new List<OrderApi__Sales_Order_Line__c> ();
		List<OrderApi__Invoice__c> invoices = new List<OrderApi__Invoice__c> ();
		for (OrderApi__Sales_Order__c s : salesOrders) {
			salesOrderLines.add(new OrderApi__Sales_Order_Line__c(OrderApi__Sales_Order__c = s.Id,
			                                                      OrderApi__Sale_Price__c = 10.00,
			                                                      OrderApi__Total__c = 10.00));
			invoices.add(new OrderApi__Invoice__c(OrderApi__Sales_Order__c = s.Id,
			                                      OrderApi__Entity__c = 'Contact',
			                                      OrderApi__Status__c = 'Overdue',
			                                      OrderApi__Posted_Date__c = System.today() - 5,
			                                      OrderApi__Contact__c = con.Id));
		}
		insert salesOrderLines;
		insert invoices;
		List<OrderApi__Invoice_Line__c> invoiceLines = new List<OrderApi__Invoice_Line__c> ();
		for (Integer i = 0; i<invoices.size(); i++) {
			invoiceLines.add(new OrderApi__Invoice_Line__c(OrderApi__Invoice__c = invoices[i].Id,
			                                               OrderApi__Sale_Price__c = salesOrderLines[i].OrderApi__Sale_Price__c,
			                                               OrderApi__Total__c = salesOrderLines[i].OrderApi__Total__c,
			                                               OrderApi__Sales_Order_Line__c = salesOrderLines[i].Id));
		}
		insert invoiceLines;
	}

	@isTest
	static void testBulkInvoiceWriteOff() {
		List<ID> invoiceIDs = new List<Id> ();
		for (OrderApi__Invoice__c inv :[SELECT Id FROM OrderApi__Invoice__c]) {
			invoiceIDs.add(inv.Id);
		}
		List<OrderApi__Invoice_Line__c> writeOffLines = [SELECT Id FROM OrderApi__Invoice_Line__c WHERE OrderApi__Is_Adjustment__c = true];
		System.assertEquals(writeOffLines.size(), 0);
		Test.startTest();
		CPAO_BulkInvoiceWriteOffBatch batch = new CPAO_BulkInvoiceWriteOffBatch(invoiceIDs);
		Database.executeBatch(batch);
		Test.stopTest();
		writeOffLines = [SELECT Id FROM OrderApi__Invoice_Line__c WHERE OrderApi__Is_Adjustment__c = true];
		System.assertEquals(writeOffLines.size(), 10);
	}
}