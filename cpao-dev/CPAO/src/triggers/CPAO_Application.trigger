trigger CPAO_Application on CPAO_Application__c (before insert, after insert, before update, after update) {
    
    if(Trigger.isBefore && (Trigger.isInsert || Trigger.isUpdate)){
        CPAO_ApplicationServices.checkBreachesBeforeApplApprovoed(Trigger.New);
    }
    

    CPAO_Automation_Control__c control = CPAO_Automation_Control__c.getOrgDefaults();
    if(!control.CPAO_Application_c__c){
        return;
    }
    
    if(Trigger.isBefore && Trigger.isInsert){
        CPAO_ApplicationHandler.beforeInsertFilter(Trigger.New);
        
        //dont think is is needed
        //CPAO_ApplicationServices.stampReinstatementApplication(Trigger.New);
    }
    if(Trigger.isBefore && Trigger.isUpdate){
        CPAO_ApplicationHandler.beforeUpdateFilter(Trigger.New, Trigger.oldMap);
        //MAKE SURE TO PUT THIS METHOD IN THE FILTER ! 
        //CPAO_ApplicationServices.stampReinstatementApplication(Trigger.New);
    }

    if(Trigger.isAfter && Trigger.isInsert){
        CPAO_ApplicationHandler.afterInsertFilter(Trigger.New);
    }
    if(Trigger.isAfter && Trigger.isUpdate){
        CPAO_ApplicationHandler.afterUpdateFilter(Trigger.New, Trigger.oldMap);
        
    }
}