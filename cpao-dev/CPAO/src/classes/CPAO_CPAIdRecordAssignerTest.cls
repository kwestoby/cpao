@isTest
private class CPAO_CPAIdRecordAssignerTest {

	public static List<Account> factoryAccounts;
	public static List<Contact> factoryContacts;

	public static void dataInitialize(Integer numberOfAccounts, Integer numberOfContacts){
		CPAO_TestFactory.automationControlSetup();
		factoryAccounts = CPAO_TestFactory.createAccounts(numberOfAccounts);
		insert factoryAccounts;
		factoryContacts= CPAO_TestFactory.createContacts(factoryAccounts[0],numberOfContacts);
		insert factoryContacts;

	}
/*
	@isTest static void test_automaticAssignment1() {
		// This tests when we create new contacts and want to autoassign ids.
		// In this scenario, number of available ids is equal to number of asked ids in the active range.
		//the ids are already created so the system grabs those existant ids instead of creating new ones from next range
	
		dataInitialize(1, 10);
		List<CPAO_CPA_Id__c> idRecordsList = CPAO_TestFactory.createUnusedIdRecords(11);

		insert(idRecordsList);

		CPAO_RecordIdAssignmentWrapper wrapper = new CPAO_RecordIdAssignmentWrapper('Contact');
		for (Contact factoryContact :factoryContacts){
			wrapper.idsAutomaticAssignment.add(factoryContact.Id);
		}

		CPAO_ContactHandler.packageAndPassToIdClass(wrapper.idsAutomaticAssignment,null,null);
		
		List<CPAO_CPA_Id__c> addedIds = [SELECT id,Id_Name__c,Contact__c FROM CPAO_CPA_Id__c WHERE Contact__c IN: factoryContacts];
		for (integer i=0;i<factoryContacts.size();i++){
			System.assertEquals(addedIds[i].Contact__c,factoryContacts[i].Id);

		}

	}

	@isTest static void test_automaticAssignment1b() {
		dataInitialize(1, 10);
		List<CPAO_CPA_Id__c> idRecordsList = CPAO_TestFactory.createUnusedIdRecords(11);

		insert(idRecordsList);
		CPAO_RecordIdAssignmentWrapper wrapperAccount = new CPAO_RecordIdAssignmentWrapper('Account');
		wrapperAccount.idsAutomaticAssignment.add(factoryAccounts[0].Id);
		CPAO_AccountHandler.packageAndPassToIdClass(wrapperAccount.idsAutomaticAssignment,null,null);
	}

	@isTest static void test_automaticAssignment2() {
		// This tests when we create new contacts and want to autoassign ids.
		// In this scenario, number of available ids is equal to number of asked ids in the active range.
		//the ids are already created but are active so the system creates new ones from next range and
		//assign it to the contacts
	
		dataInitialize(1, 10);
		List<CPAO_CPA_Id__c> idRecordsList = CPAO_TestFactory.createActiveIdRecords(10);
		insert(idRecordsList);

		insert CPAO_TestFactory.addNextRange(CPAO_TestFactory.createRange(3));
		CPAO_RecordIdAssignmentWrapper wrapper = new CPAO_RecordIdAssignmentWrapper('Contact');
		for (Contact factoryContact :factoryContacts){
			wrapper.idsAutomaticAssignment.add(factoryContact.Id);
		}

		CPAO_ContactHandler.packageAndPassToIdClass(wrapper.idsAutomaticAssignment,null,null);
		
		List<CPAO_CPA_Id__c> addedIds = [SELECT id,Id_Name__c,Contact__c FROM CPAO_CPA_Id__c WHERE Contact__c IN: factoryContacts];
		
		for (integer i=0;i<factoryContacts.size();i++){
			System.assertEquals(addedIds[i].Contact__c,factoryContacts[i].Id);

		}

	}
	
	@isTest static void test_automaticAssignment3() {
		// This tests when we create new contacts and want to autoassign ids
		// In this scenario, number of available ids is more  than number of asked ids in active range
		// No ids are existent at the moment so we need to create first ids.

		dataInitialize(1, 9);

		insert CPAO_TestFactory.addNextRange(CPAO_TestFactory.createRange(3));

		CPAO_RecordIdAssignmentWrapper wrapper = new CPAO_RecordIdAssignmentWrapper('Contact');
		for (Contact factoryContact :factoryContacts){
			wrapper.idsAutomaticAssignment.add(factoryContact.Id);
		}
		

		CPAO_ContactHandler.packageAndPassToIdClass(wrapper.idsAutomaticAssignment,null,null);
		
		List<CPAO_CPA_Id__c> addedIds = [SELECT id,Id_Name__c,Contact__c FROM CPAO_CPA_Id__c WHERE Contact__c IN: factoryContacts];
		for (integer i=0;i<factoryContacts.size();i++){
			System.assertEquals(addedIds[i].Contact__c,factoryContacts[i].Id);
		}
		
	}

	@isTest static void test_automaticAssignment4() {
		// This tests when we create new contacts and want to autoassign ids
		// In this scenario, number of available ids is less  than number of asked ids in active range
		// In this case, new ids must be created from next range

		dataInitialize(1, 11);

		insert CPAO_TestFactory.addNextRange(CPAO_TestFactory.createRange(3));

		CPAO_RecordIdAssignmentWrapper wrapper = new CPAO_RecordIdAssignmentWrapper('Contact');
		for (Contact factoryContact :factoryContacts){
			wrapper.idsAutomaticAssignment.add(factoryContact.Id);
		}
		

		CPAO_ContactHandler.packageAndPassToIdClass(wrapper.idsAutomaticAssignment,null,null);
		
		List<CPAO_CPA_Id__c> addedIds = [SELECT id,Id_Name__c,Contact__c FROM CPAO_CPA_Id__c WHERE Contact__c IN: factoryContacts];
		for (integer i=0;i<factoryContacts.size();i++){
			System.assertEquals(addedIds[i].Contact__c,factoryContacts[i].Id);
		}
		
	}

	@isTest static void test_automaticAssignment5() {
		// This tests when we create new contacts and want to autoassign ids
		// In this scenario, number of available ids is less than number of needed ids
		//The system will first assign the available unused ids and will create new ones from the next range

		dataInitialize(1, 11);
		List<CPAO_CPA_Id__c> factoryUsedIds=CPAO_TestFactory.createUnusedIdRecords(5);
		insert(factoryUsedIds);

		insert CPAO_TestFactory.addNextRange(CPAO_TestFactory.createRange(3));

		CPAO_RecordIdAssignmentWrapper wrapper = new CPAO_RecordIdAssignmentWrapper('Contact');
		for (Contact factoryContact :factoryContacts){
			wrapper.idsAutomaticAssignment.add(factoryContact.Id);
		}
		

		CPAO_ContactHandler.packageAndPassToIdClass(wrapper.idsAutomaticAssignment,null,null);
		
		List<CPAO_CPA_Id__c> addedIds = [SELECT id,Id_Name__c,Contact__c FROM CPAO_CPA_Id__c WHERE Contact__c IN: factoryContacts];
		for (integer i=0;i<factoryContacts.size();i++){
			System.assertEquals(addedIds[i].Contact__c,factoryContacts[i].Id);
		}
		
	}
	
	@isTest static void test_automaticAssignment6() {
		// This tests when we create new contacts and want to autoassign ids
		// In this scenario, number of available ids is more  than number of asked ids in active range
		// No ids are existent at the moment so we need to create first ids.
		// However, there is an active id that is contained in the next active range (Id_Name__c=0) 
		//so the system must not recreate it as it is already created
		
		dataInitialize(1, 11);
		List<CPAO_CPA_Id__c> idRecordsList = CPAO_TestFactory.createNotUniqueIdRecords(5);
		insert(idRecordsList);

		insert CPAO_TestFactory.addNextRange(CPAO_TestFactory.createRange(3));

		CPAO_RecordIdAssignmentWrapper wrapper = new CPAO_RecordIdAssignmentWrapper('Contact');
		for (Contact factoryContact :factoryContacts){
			wrapper.idsAutomaticAssignment.add(factoryContact.Id);
		}
		
		CPAO_ContactHandler.packageAndPassToIdClass(wrapper.idsAutomaticAssignment,null,null);
		
		List<CPAO_CPA_Id__c> addedIds = [SELECT id,Id_Name__c,Contact__c FROM CPAO_CPA_Id__c WHERE Contact__c IN: factoryContacts];
		for (integer i=0;i<factoryContacts.size();i++){
			System.assertEquals(addedIds[i].Contact__c,factoryContacts[i].Id);
		}
		
	}

	@isTest static void test_manualAssignment() {
		// This tests when we create new contacts and want to autoassign ids
		// In this scenario, number of available ids is more  than number of asked ids in active range
		// No ids are existent at the moment so we need to create first ids.
		// However, there is an active id that is contained in the next active range (Id_Name__c=0) 
		//so the system must not recreate it as it is already created
		
		dataInitialize(1, 11);
		List<CPAO_CPA_Id__c> idRecordsList = CPAO_TestFactory.createNotUniqueIdRecords(11);
		insert(idRecordsList);

		for(Integer i = 0; i<idRecordsList.size(); i++){
			factoryContacts[i].CPAO_CPA_Id__c=idRecordsList[i].Id_Name__c;
		}
		update factoryContacts;
		
		List<CPAO_CPA_Id__c> addedIds = [SELECT id,Id_Name__c,Contact__c FROM CPAO_CPA_Id__c WHERE Contact__c IN: factoryContacts];
		for (integer i=0;i<factoryContacts.size();i++){
			System.assertEquals(addedIds[i].Contact__c,factoryContacts[i].Id);
		}
		
	}*/
	@isTest static void emailtest() {
		// This tests that when there is not enough unused cpa ids records, the system
		// sends an email to the appropriate user so that he can acquire more
		
		List<CPAO_CPA_Id__c> idRecordsList = CPAO_TestFactory.createUnusedIdRecords(8);
		insert(idRecordsList);
		insert CPAO_TestFactory.addNextRange(CPAO_TestFactory.createRange(1));
		CPA_Id_Warning_Email__c emailSettings=CPAO_TestFactory.createEmailDetails();
		insert(emailSettings);
		CPAO_IDRecordController.sendIdWarningEmail();
	}
}