({
    clickOnNextButton : function(component,event,helper){
        
        var componentNameInEvent=event.getParam("componentName");
        if(componentNameInEvent==component.get('v.nameOfThisComponent')){
            if(event.getParam("nextStepName")=='Exit'){ 
                component.set('v.shouldSaveAndQuit',true);
            }
            var validForm = component.find('COAForm').reduce(function (validSoFar, inputCmp) {
                inputCmp.showHelpMessageIfInvalid();
                return validSoFar && inputCmp.get('v.validity').valid ;
            }, true);
            
            if(validForm){
                var showSpinnerEvent = component.getEvent("showSpinner");
                showSpinnerEvent.fire();
                helper.upsertCOA(component);
            }
            // helper.upload(component,component.get('v.fileInput'), component.get('v.fileContents'),false) ;
        }
        
        
        
        
    }/*,
    handleFilesChange: function (component, event,helper) {
        var fileInput = event.getSource().get("v.files")[0];
        var coa_decl_req=component.find('coa_decl_req');

        console.log('file input'+fileInput);
        console.log(fileInput.size);
        component.set('v.fileInput',fileInput);
        component.set('v.fileName',fileInput.name);
        component.set('v.coaDeclarationFileName',fileInput.name);
       // component.set('v.NotYetUploaded','true');

        //alert( component.get('v.fileName'));
        component.set('v.fileType',fileInput.type);

        var fr = new FileReader();
        var self = this;

        fr.onload = function() {
            var fileContents = fr.result;
            var base64Mark = 'base64,';
            var dataStart = fileContents.indexOf(base64Mark) + base64Mark.length;
            fileContents = fileContents.substring(dataStart);
            component.set('v.fileContents',fileContents);

          // helper.upload(component,fileInput, fileContents);
        };

        fr.readAsDataURL(fileInput);
        $A.util.removeClass(coa_decl_req, 'slds-show');

    },
    clearFile: function (component, event,helper) {
      component.set('v.currentCOA.CPAO_COA_Declaration_File_Name__c',null);
      component.set('v.currentCOA.CPAO_COA_Declaration_File_Id__c',null);
            component.set('v.coaDeclarationFileName',null);

            helper.upload(component,null,null,true) ;


    }*/
})