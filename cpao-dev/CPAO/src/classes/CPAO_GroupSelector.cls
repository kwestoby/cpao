public without sharing class CPAO_GroupSelector {
	
	public static List<Group> getROStaffQueue() {
		return [SELECT Id FROM Group WHERE  Type = 'Queue' AND NAME = 'RO Staff Queue'];
	}

	public static List<Group> getRegistrarQueue() {
		return [SELECT Id FROM Group WHERE  Type = 'Queue' AND NAME = 'Registrar'];
	}
}