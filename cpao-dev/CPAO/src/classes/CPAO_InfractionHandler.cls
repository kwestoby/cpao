public with sharing class CPAO_InfractionHandler {
	public static final String OPEN = 'Open';
	public static final String PENDING = 'Pending Review';
	public static final String CLEAR = 'Clear';

	public static void beforeInsertFilter(List<CPAO_Infraction__c> infractions) {
		List<CPAO_Infraction__c> ledToSuspension = new List<CPAO_Infraction__c>();
		List<CPAO_Infraction__c> ledToRevocationDeregistration = new List<CPAO_Infraction__c>();
		for(CPAO_Infraction__c infraction:infractions){
			if(infraction.CPAO_Led_to_Suspension__c){
				ledToSuspension.add(infraction);
			}
			if(infraction.CPAO_Led_to_Revocation_Deregistration__c){
				ledToRevocationDeregistration.add(infraction);
			}
		}

		if(!ledToSuspension.isEmpty()){
			CPAO_InfractionServices.ledToSuspension(ledToSuspension);
		}
		if(!ledToRevocationDeregistration.isEmpty()){
			CPAO_InfractionServices.ledToRevocationDeregistration(ledToRevocationDeregistration);
		}
	}

	public static void beforeUpdateFilter(List<CPAO_Infraction__c> infractions, Map<Id, CPAO_Infraction__c> oldMap) {
		List<CPAO_Infraction__c> ledToSuspension = new List<CPAO_Infraction__c>();
		List<CPAO_Infraction__c> ledToRevocationDeregistration = new List<CPAO_Infraction__c>();
		for(CPAO_Infraction__c infraction:infractions){
			if(infraction.CPAO_Led_to_Suspension__c && !oldMap.get(infraction.Id).CPAO_Led_to_Suspension__c){
				ledToSuspension.add(infraction);
			}
			if(infraction.CPAO_Led_to_Revocation_Deregistration__c && !oldMap.get(infraction.Id).CPAO_Led_to_Revocation_Deregistration__c){
				ledToRevocationDeregistration.add(infraction);
			}
		}

		if(!ledToSuspension.isEmpty()){
			CPAO_InfractionServices.ledToSuspension(ledToSuspension);
		}
		if(!ledToRevocationDeregistration.isEmpty()){
			CPAO_InfractionServices.ledToRevocationDeregistration(ledToRevocationDeregistration);
		}

	}

	public static void afterInsertFilter(List<CPAO_Infraction__c> infractions) {
		List<CPAO_Infraction__c> activeInfractions = new List<CPAO_Infraction__c>();
		for(CPAO_Infraction__c infraction:infractions){
			if((infraction.CPAO_Status__c == OPEN || infraction.CPAO_Status__c == PENDING)
				&&(infraction.CPAO_Contact__c != null || infraction.CPAO_Account__c != null)){
				activeInfractions.add(infraction);
			}
		}
		if(!activeInfractions.isEmpty()){
			rollUpActiveInfractions(activeInfractions);
		}
	}

	public static void afterUpdateFilter(List<CPAO_Infraction__c> infractions, Map<Id, CPAO_Infraction__c> oldMap) {
		List<CPAO_Infraction__c> statusChangeInfractions = new List<CPAO_Infraction__c>();
		for(CPAO_Infraction__c infraction:infractions){
			if(infraction.CPAO_Status__c != oldMap.get(infraction.Id).CPAO_Status__c 
			&&(infraction.CPAO_Contact__c != null || infraction.CPAO_Account__c != null)){
				statusChangeInfractions.add(infraction);
			}
		}
		if(!statusChangeInfractions.isEmpty()){
			rollUpActiveInfractions(statusChangeInfractions);
		}
	}

	public static void afterDeleteFilter(Map<Id, CPAO_Infraction__c> oldMap) {
		List<CPAO_Infraction__c> deletedActiveInfractions = new List<CPAO_Infraction__c>();
		for(CPAO_Infraction__c infraction:oldMap.values()){
			if((infraction.CPAO_Status__c == OPEN || infraction.CPAO_Status__c == PENDING) 
				&&(infraction.CPAO_Contact__c != null || infraction.CPAO_Account__c != null)){
				deletedActiveInfractions.add(infraction);
			}
		}
		if(!deletedActiveInfractions.isEmpty()){
			rollUpActiveInfractions(deletedActiveInfractions);
		}
	}

	public static void rollUpActiveInfractions(List<CPAO_Infraction__c> filteredInfractions) {
		List<Contact> contactsToUpdate = new List<Contact>();
		List<Account> accountsToUpdate = new List<Account>();
		Set<Id> contactIds = new Set<Id>();
		Set<Id> accountIds = new Set<Id>();
		for(CPAO_Infraction__c infraction:filteredInfractions){
			if(infraction.CPAO_Contact__c != null) contactIds.add(infraction.CPAO_Contact__c);
			if(infraction.CPAO_Account__c != null) accountIds.add(infraction.CPAO_Account__c);
		}
		if(!contactIds.isEmpty()){
			Map<Id, List<CPAO_Infraction__c>> contactInfractionMap = new Map<Id, List<CPAO_Infraction__c>>();

			List<CPAO_Infraction__c> contactInfractions = CPAO_InfractionSelector.getContactInfractions(contactIds);

			for(CPAO_Infraction__c infraction:contactInfractions){
				if(!contactInfractionMap.containsKey(infraction.CPAO_Contact__c)){
					contactInfractionMap.put(infraction.CPAO_Contact__c, new List<CPAO_Infraction__c>{infraction});
				} else {
					List<CPAO_Infraction__c> templist = contactInfractionMap.get(infraction.CPAO_Contact__c);
					templist.add(infraction);
					contactInfractionMap.put(infraction.CPAO_Contact__c, templist);
				}
			}
			for(Id contactId:contactIds){
				if(contactInfractionMap.containsKey(contactId)){
					Integer numberOfActiveInfractions = 0;
					for(CPAO_Infraction__c contactInfraction:contactInfractionMap.get(contactId)){
						if(contactInfraction.CPAO_Status__c == OPEN || contactInfraction.CPAO_Status__c == PENDING){
							numberOfActiveInfractions = numberOfActiveInfractions+1;
						}
					}
					contactsToUpdate.add(new Contact(Id = contactId, CPAO_Number_of_Active_Infractions__c = numberOfActiveInfractions));
				} else {
					contactsToUpdate.add(new Contact(Id = contactId, CPAO_Number_of_Active_Infractions__c = 0));
				}
			}
		}
		if(!accountIds.isEmpty()){
			Map<Id, List<CPAO_Infraction__c>> accountInfractionMap = new Map<Id, List<CPAO_Infraction__c>>();

			List<CPAO_Infraction__c> accountInfractions = CPAO_InfractionSelector.getAccountInfractions(accountIds);
			for(CPAO_Infraction__c infraction:accountInfractions){
				if(!accountInfractionMap.containsKey(infraction.CPAO_Account__c)){
					accountInfractionMap.put(infraction.CPAO_Account__c, new List<CPAO_Infraction__c>{infraction});
				} else {
					List<CPAO_Infraction__c> templist = accountInfractionMap.get(infraction.CPAO_Account__c);
					templist.add(infraction);
					accountInfractionMap.put(infraction.CPAO_Account__c, templist);
				}
			}
			for(Id accountId:accountIds){
				if(accountInfractionMap.containsKey(accountId)){
					Integer numberOfActiveInfractions = 0;
					for(CPAO_Infraction__c accountInfraction:accountInfractionMap.get(accountId)){
						if(accountInfraction.CPAO_Status__c == OPEN || accountInfraction.CPAO_Status__c == PENDING){
							numberOfActiveInfractions= numberOfActiveInfractions+1;
						}
					}
					accountsToUpdate.add(new Account(Id = accountId, CPAO_Number_of_Active_Infractions__c = numberOfActiveInfractions));
				} else {
					accountsToUpdate.add(new Account(Id = accountId, CPAO_Number_of_Active_Infractions__c = 0));
				}
			}
		}
		if(!contactsToUpdate.isEmpty()){
			update contactsToUpdate;
		}
		if(!accountsToUpdate.isEmpty()){
			update accountsToUpdate;
		}
	}
}