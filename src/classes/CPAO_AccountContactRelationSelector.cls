public without sharing class CPAO_AccountContactRelationSelector {
	
	public static List<CPAO_Account_Contact_Relationship__c> getPrimaryRelationshipsByContactId(Set<Id> contactIds) {
		return [SELECT Id, CPAO_Contact__c, CPAO_Primary_Employer__c, CPAO_AccountNameFormula__c  
				FROM CPAO_Account_Contact_Relationship__c 
				WHERE CPAO_Contact__c IN :contactIds AND CPAO_Primary_Employer__c = 'Yes'];
	}

	public static List<CPAO_Account_Contact_Relationship__c> getRelationshipsByContactIdAndRelationToAccount(Id contactId, String relationship,String status) {
		return [SELECT Id, CPAO_Account__c,CPAO_AccountNameFormula__c,CPAO_Business_Unit__c,CPAO_Status__c,
					CPAO_Position_Category__c, CPAO_Relationship_to_Account__c, CPAO_Text__c,CPAO_Start_Date__c,
					CPAO_End_Date__c, CPAO_Primary_Employer__c, CPAO_Contact__c,CPAO_Account__r.BillingStreet,
					CPAO_Account__r.BillingCity, CPAO_Account__r.BillingState, CPAO_Account__r.BillingPostalCode,
					CPAO_Account__r.BillingCountry,CPAO_Contact__r.CPAO_Employment_Status__c,CPAO_Contact__r.Name
				FROM CPAO_Account_Contact_Relationship__c
				WHERE CPAO_Contact__c = :contactId AND CPAO_Relationship_to_Account__c = :relationship AND  CPAO_Status__c =:status
				ORDER BY CPAO_Start_Date__c desc];
	}

	public static List<CPAO_Account_Contact_Relationship__c> getPrimaryRelationshipsById(Id relationshipId) {
		return [SELECT Id, CPAO_Primary_Employer__c 
				FROM CPAO_Account_Contact_Relationship__c
			    WHERE  CPAO_Primary_Employer__c = 'Yes' AND Id =:relationshipId];
	}

	public static List<CPAO_Account_Contact_Relationship__c> getRelationshipsByContactIdAndStatus(Id contactId, String status) {
		return [SELECT Id, CPAO_Account__r.Name, CPAO_Relationship_to_Account__c, CPAO_Text__c,CPAO_Account__r.CPAO_Account_Type__c,CPAO_Account__r.CPAO_Account_Status__c
				FROM CPAO_Account_Contact_Relationship__c 
				WHERE CPAO_Contact__c = :contactId AND CPAO_Status__c = :status];
	}

	public static List<CPAO_Account_Contact_Relationship__c> getRelationshipsByDateAndAccountType(Id contactId,String relationship, Date startDate,String accountType) {
		return [SELECT Id, CPAO_Account__r.Name, CPAO_Start_Date__c,CPAO_End_Date__c,CPAO_Relationship_to_Account__c, 
					   CPAO_Text__c,CPAO_Account__r.CPAO_Account_Type__c,CPAO_Account__r.CPAO_Account_Status__c
				FROM CPAO_Account_Contact_Relationship__c 
				WHERE CPAO_Contact__c = :contactId AND CPAO_Relationship_to_Account__c = :relationship AND CPAO_Account__r.CPAO_Account_Type__c = :accountType AND CPAO_Start_Date__c >= :startDate];
	}

}//