<?xml version="1.0" encoding="UTF-8"?>
<CustomMetadata xmlns="http://soap.sforce.com/2006/04/metadata" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xmlns:xsd="http://www.w3.org/2001/XMLSchema">
    <label>Financial Hardship/Unemployment</label>
    <protected>false</protected>
    <values>
        <field>AMD_or_CPD__c</field>
        <value xsi:type="xsd:string">AMD</value>
    </values>
    <values>
        <field>CPAO_Explanation__c</field>
        <value xsi:type="xsd:string">Member’s projected business and employment gross earnings (excluding investments, pension, disability, employment insurance, etc.) for the current calendar year will be less than $32,364 and net equity is less than $200,000.  A financial waiver can be granted a maximum of three times in a member’s lifetime.</value>
    </values>
    <values>
        <field>CPAO_Percent_Off__c</field>
        <value xsi:type="xsd:double">100.0</value>
    </values>
    <values>
        <field>Explanation_Third_Year__c</field>
        <value xsi:nil="true"/>
    </values>
    <values>
        <field>Full_Name__c</field>
        <value xsi:type="xsd:string">Financial Hardship/Unemployment</value>
    </values>
</CustomMetadata>
