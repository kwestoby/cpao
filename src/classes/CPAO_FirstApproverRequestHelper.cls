public class CPAO_FirstApproverRequestHelper {

    public static void updateOrderReceipts(Map<Id, OrderApi__Receipt__c >  receiptObjs){
        
        List<Id> groupRegistersIds = new List<Id>();
        List<Id> groupStudentIds = new List<Id>();
        List<Id> groupVariesIds = new List<Id>();
        List<Id> groupMemberIds = new List<Id>();
        List<Id> groupPdDepartmentIds = new List<Id>();
        List<Id> groupPracticeInsepectionIds = new List<Id>();


        Map<Id,OrderApi__Business_Group__c> businessMap = New Map<Id,OrderApi__Business_Group__c>([Select Id, name from OrderApi__Business_Group__c]);

        for(Group assignRegistersQueue : [select Id from Group where  Type = 'Queue' AND NAME = 'CPAO Registrar Office (Registrar)']){
            groupRegistersIds.add(assignRegistersQueue.Id);
        }
        
        for(Group assignStudentQueue : [select Id from Group where  Type = 'Queue' AND NAME = 'CPAO Student Services (Education)']){
            groupStudentIds.add(assignStudentQueue.Id);
        }
        
        for(Group assignVariesDependsQueue : [select Id from Group where  Type = 'Queue' AND NAME = 'CPAO Varies - depends on which depart']){
            groupVariesIds.add(assignVariesDependsQueue.Id);
        }
        
        for(Group assignMemberDependsQueue : [select Id from Group where  Type = 'Queue' AND NAME = 'CPAO Member Experience']){
            groupMemberIds.add(assignMemberDependsQueue.Id);
        }
        
        for(Group assignPdDepartmentDependsQueue : [select Id from Group where  Type = 'Queue' AND NAME = 'CPAO PD Department']){
            groupPdDepartmentIds.add(assignPDDepartmentDependsQueue.Id);
        }
        
        for(Group assignPracticeDependsQueue : [select Id from Group where  Type = 'Queue' AND NAME = 'CPAO Practice Inspection']){
            groupPracticeInsepectionIds.add(assignPracticeDependsQueue.Id);
        }
        
        List<GroupMember> grpRegistersMember = [Select UserOrGroupId From GroupMember where GroupId =:groupRegistersIds];
        List<GroupMember> grpStudentsMember = [Select UserOrGroupId From GroupMember where GroupId =:groupStudentIds];
        List<GroupMember> grpVariesMember = [Select UserOrGroupId From GroupMember where GroupId =:groupVariesIds];
        List<GroupMember> grpPdMember = [Select UserOrGroupId From GroupMember where GroupId =:groupPdDepartmentIds];
        List<GroupMember> grpPracticeMember = [Select UserOrGroupId From GroupMember where GroupId =:groupPracticeInsepectionIds];

        
        if(grpRegistersMember.size() > 0){
            Map<String,CPAO_Department__mdt > allFeeDepartmentMap = new Map<String,CPAO_Department__mdt>();
            for(CPAO_Department__mdt allFeeDepartment : [SELECT Member_Experience__c,MasterLabel,PD_Department_Manager_or_higher__c,Practice_Inspection__c,Registrar_s_Office_Registrar__c,Student_Services_Education__c  FROM CPAO_Department__mdt]){
                allFeeDepartmentMap.put(allFeeDepartment.Registrar_s_Office_Registrar__c, allFeeDepartment);
            }                                                    
        
            for(OrderApi__Receipt__c orderReceipt: receiptObjs.values()){
                CPAO_Department__mdt allFreeDepartment = allFeeDepartmentMap.get(orderReceipt.CPAO_Item_Name__c);
                if(allFreeDepartment != null){
                    orderReceipt.CPAO_First_Approver__c = grpRegistersMember[0].UserOrGroupId;
                }
            }
        }  
        
                
        if(grpStudentsMember.size() > 0){
            Map<String,CPAO_Department__mdt > allFeeDepartmentMap = new Map<String,CPAO_Department__mdt>();
            for(CPAO_Department__mdt allFeeDepartment : [SELECT Member_Experience__c,MasterLabel,PD_Department_Manager_or_higher__c,Practice_Inspection__c,Registrar_s_Office_Registrar__c,Student_Services_Education__c  FROM CPAO_Department__mdt]){
                allFeeDepartmentMap.put(allFeeDepartment.Student_Services_Education__c, allFeeDepartment);
            }                                                    
        
            for(OrderApi__Receipt__c orderReceipt: receiptObjs.values()){
                CPAO_Department__mdt allFreeDepartment = allFeeDepartmentMap.get(orderReceipt.CPAO_Item_Name__c);
                if(allFreeDepartment != null){
                    orderReceipt.CPAO_First_Approver__c = grpStudentsMember[0].UserOrGroupId;
                }
            }
        }  
        
        if(grpVariesMember.size() > 0){
            Map<String,CPAO_Department__mdt > allFeeDepartmentMap = new Map<String,CPAO_Department__mdt>();
            for(CPAO_Department__mdt allFeeDepartment : [SELECT Member_Experience__c,MasterLabel,PD_Department_Manager_or_higher__c,Varies_depends_on_which_department_it__c,Practice_Inspection__c,Registrar_s_Office_Registrar__c,Student_Services_Education__c  FROM CPAO_Department__mdt]){
                allFeeDepartmentMap.put(allFeeDepartment.Varies_depends_on_which_department_it__c, allFeeDepartment);
            }                                                    
        
            for(OrderApi__Receipt__c orderReceipt: receiptObjs.values()){
                CPAO_Department__mdt allFreeDepartment = allFeeDepartmentMap.get(orderReceipt.CPAO_Item_Name__c);
                if(allFreeDepartment != null){
                    orderReceipt.CPAO_First_Approver__c = grpVariesMember[0].UserOrGroupId;
                }
            }
        }  
        
        
        if(grpPdMember.size() > 0){
            Map<String,CPAO_Department__mdt > allFeeDepartmentMap = new Map<String,CPAO_Department__mdt>();
            for(CPAO_Department__mdt allFeeDepartment : [SELECT Member_Experience__c,MasterLabel,PD_Department_Manager_or_higher__c,Practice_Inspection__c,Registrar_s_Office_Registrar__c,Student_Services_Education__c  FROM CPAO_Department__mdt]){
                allFeeDepartmentMap.put(allFeeDepartment.PD_Department_Manager_or_higher__c, allFeeDepartment);
            }                                                    
        
            for(OrderApi__Receipt__c orderReceipt: receiptObjs.values()){
                CPAO_Department__mdt allFreeDepartment = allFeeDepartmentMap.get(orderReceipt.CPAO_Item_Name__c);
                if(allFreeDepartment != null){
                    orderReceipt.CPAO_First_Approver__c = grpPdMember[0].UserOrGroupId;
                }
            }
        }  
        
        
        if(grpPracticeMember.size() > 0){
            Map<String,CPAO_Department__mdt > allFeeDepartmentMap = new Map<String,CPAO_Department__mdt>();
            for(CPAO_Department__mdt allFeeDepartment : [SELECT Member_Experience__c,MasterLabel,PD_Department_Manager_or_higher__c,Practice_Inspection__c,Registrar_s_Office_Registrar__c,Student_Services_Education__c  FROM CPAO_Department__mdt]){
                allFeeDepartmentMap.put(allFeeDepartment.Practice_Inspection__c, allFeeDepartment);
            }                                                    
        
            for(OrderApi__Receipt__c orderReceipt: receiptObjs.values()){
                system.debug('orderReceiptorderReceipt'+orderReceipt);
                CPAO_Department__mdt allFreeDepartment = allFeeDepartmentMap.get(orderReceipt.CPAO_Item_Name__c);
                if(allFreeDepartment != null){
                    orderReceipt.CPAO_First_Approver__c = grpPdMember[0].UserOrGroupId;
                }
            }
        }  
         
    }
}