@isTest
private class CPAO_SelectorTests {
	
	@isTest
	static void CPAO_AccountSelector() {
		Account account = CPAO_TestFactory.createAccounts(1)[0];
		CPAO_AccountSelector.getAccountToUpdate(new Set<Id>{account.Id});
	}

	@isTest
	static void CPAO_AccountSelector2() {
		Account account = CPAO_TestFactory.createAccounts(1)[0];
		String accountName = account.Name;
		CPAO_AccountSelector.getAccountToUpdate(accountName);
	}

	@isTest
	static void CPAO_AccountSelector3() {
		Account account = CPAO_TestFactory.createAccounts(1)[0];
		Contact contact = CPAO_TestFactory.createContacts(account,1)[0];
		CPAO_AccountSelector.getprimaryContactAccounts(contact.id);
		CPAO_AccountSelector.getLSOAccounts(contact.id);
		CPAO_AccountSelector.getContactSoAccounts(contact.id);
		CPAO_AccountSelector.getContactSoAccountsWithIds(contact.id, new List<Id>{account.id});
	}
	
	@isTest
	static void CPAO_ContactSelector() {
		Account account = CPAO_TestFactory.createAccounts(1)[0];
		Contact contact = CPAO_TestFactory.createContacts(account, 1)[0];
		CPAO_ContactSelector.getContactToUpdate(new Set<Id>{contact.Id});
		CPAO_ContactSelector.getLSOContacts(new Set<Id>{contact.Id});
		CPAO_ContactSelector.getContactRecords(new Set<Id>{contact.Id});
	}

	@isTest
	static void CPAO_InsuranceSelector() {
		Account account = CPAO_TestFactory.createAccounts(1)[0];
	 	CPAO_InsuranceSelector.getInsuranceByAccount(new List<Account>{account});
	}

	@isTest
	static void CPAO_InfractionSelector() {
		Account account = CPAO_TestFactory.createAccounts(1)[0];
		Contact contact = CPAO_TestFactory.createContacts(account,1)[0];
		CPAO_Application__c application = CPAO_TestFactory.createPALApplication(1, contact)[0];
		CPAO_Infraction_Type__c infractionType	= new CPAO_Infraction_Type__c(Name='test type');
		CPAO_Infraction__c infraction = new CPAO_Infraction__c();
		infraction.CPAO_Infraction_Type__c=infractionType.id;
		infraction.CPAO_Status__c='Open';
	 	CPAO_InfractionSelector.getInfractionById(infraction.id);
	 	CPAO_InfractionSelector.getAccountInfractions(new Set<Id>{account.id});
//	 	CPAO_InfractionSelector.getApplicationInfractions(application.id);
	 	CPAO_InfractionSelector.getInfractionType(infractionType.Name);
	 	CPAO_InfractionSelector.getContactInfractions(new Set<Id>{contact.id});
	 	CPAO_InfractionSelector.getContactOpenInfractions(new Set<Id>{contact.id});

	}


	@isTest
	static void CPAO_CommunicationPreferenceSelector() {
		Account account = CPAO_TestFactory.createAccounts(1)[0];
		Contact contact = CPAO_TestFactory.createContacts(account, 1)[0];
		CPAO_CommunicationPreferenceSelector.getCommunicationPreferences();
		CPAO_CommunicationPreferenceSelector.getCommunicationPreferencesByContactId(new Set<Id>{contact.Id});
	}

	@isTest
	static void CPAO_CPAIdSelector() {
		List<CPAO_CPA_Id__c> testIdRecords = CPAO_TestFactory.createUnusedIdRecords(10);
		insert testIdRecords;

		CPAO_CPAIdSelector.getUnusedIds();
		CPAO_CPAIdSelector.getUnusedIdsExceptOldRange(new Set<Decimal>{1,2,3});
		CPAO_CPAIdSelector.getExistingIdsInRange(1,2);
		CPAO_CPAIdSelector.getExistingIdsFromSet(new Set<Decimal>{1,2,3});
		CPAO_CPAIdSelector.getExistingIdsFromRecordSet(testIdRecords);
	}

	@isTest
	static void CPAO_CPARangeDetailSelector() {
		Account account = CPAO_TestFactory.createAccounts(1)[0];
		Contact contact = CPAO_TestFactory.createContacts(account, 1)[0];
		CPAO_CPARangeDetailSelector.getActiveRange();
		CPAO_CPARangeDetailSelector.getNextRange('test');
	}

	@isTest
	static void CPAO_UserSelector() {
		Account account = CPAO_TestFactory.createAccounts(1)[0];
		Contact contact = CPAO_TestFactory.createContacts(account, 1)[0];
		CPAO_UserSelector.getCurrentUser();
		CPAO_UserSelector.getUsersWithContactList(new Set<Contact>{contact});
	}

	@isTest
	static void CPAO_BadgeSelector() {
		List<Account> accountsToInsert = CPAO_AccountHandlerTest.dataInitializeBadges(1);
		insert accountsToInsert;
		List<Account>accounts = [SELECT Id,Name,OrderApi__Primary_Contact__c FROM Account];
		List<Contact>contacts = [SELECT Id FROM Contact WHERE id=:accounts[0].OrderApi__Primary_Contact__c];
		List<OrderApi__Badge_Type__c> primaryContactBadgeType = CPAO_BadgeSelector.getBadgeType('Primary Contact');
		CPAO_BadgeSelector.getBadges(new Set<Id>{contacts[0].id}, primaryContactBadgeType[0].id);
		CPAO_BadgeSelector.getActiveBadges(new Set<Id>{contacts[0].id});
	}

	@isTest
	static void CPAO_GroupSelector() {
		CPAO_GroupSelector.getROStaffQueue();
		CPAO_GroupSelector.getRegistrarQueue();
	}

	@isTest
	static void CPAO_WaiverSelector() {
		Account account = CPAO_TestFactory.createAccounts(1)[0];
		Contact contact = CPAO_TestFactory.createContacts(account, 1)[0];
		CPAO_WaiverSelector.getContinuousContactWaivers(new Set<Id>{contact.id});
		CPAO_WaiverSelector.getContactWaivers(new Set<Id>{contact.id});
	}

	@isTest
	static void CPAO_PracticeInspectionSelector() {
		Account account = CPAO_TestFactory.createAccounts(1)[0];
		CPAO_PracticeInspectionSelector.getInspectionByReportDate(new Set<Id>{account.id}, Date.today());
		CPAO_PracticeInspectionSelector.getInspectionByAccountId(new Set<Id>{account.id});
	}

	@isTest
	static void CPAO_AccountContactRelationshipSelector() {
		Account account = CPAO_TestFactory.createAccounts(1)[0];
		Contact contact = CPAO_TestFactory.createContacts(account, 1)[0];
		Date today=Date.today();
		CPAO_AccountContactRelationSelector.getRelationshipsByDateAndAccountType(contact.id, 'Employee', Date.today(), 'Firm');
		CPAO_AccountContactRelationSelector.getRelationshipsByContactIdAndRelationToAccount(contact.id, 'Employee','Active');
		//CPAO_AccountContactRelationSelector.getPrimaryRelationshipsById()
		CPAO_AccountContactRelationSelector.getRelationshipsByContactIdAndStatus(contact.id,'Active');
		CPAO_AccountContactRelationSelector.getRelationshipsByDateAndAccountType(contact.id,'Employee',today,'Firm');

	}

	@isTest
	static void CPAO_FontevaObjectSelector() {
		final String PLAN = 'Public Accounting Licence';
		Account account = CPAO_TestFactory.createAccounts(1)[0];
		Contact contact = CPAO_TestFactory.createContacts(account, 1)[0];
		EventApi__Event__c event = CPAO_FontevaTestFactory.createEventApiEvent();
		OrderApi__Sales_Order_Line__c sol = CPAO_FontevaTestFactory.createSalesOrderLine(event);
		CPAO_FontevaObjectSelector.getSubscriptionPlanItems(PLAN);
		CPAO_FontevaObjectSelector.getSubscriptionPlanInfo(PLAN);
		CPAO_FontevaObjectSelector.getSubscriptionPlan(PLAN);
		CPAO_FontevaObjectSelector.getContactSubscription(PLAN, contact.id);
		CPAO_FontevaObjectSelector.getContactsSubscriptions(PLAN, new Set<Id>{contact.id});
		CPAO_FontevaObjectSelector.getSalesOrderTaxItems(sol.OrderApi__Sales_Order__c);
		//CPAO_FontevaObjectSelector.getSalesOrderWithApplication();
		CPAO_FontevaObjectSelector.getSalesOrdersWithSpecifiedAppRecordTypes(new List<OrderApi__Sales_Order__c>{new OrderApi__Sales_Order__c()}, 'Readmission');
		CPAO_FontevaObjectSelector.getPayTypeItemClass('payType');
		CPAO_FontevaObjectSelector.getLateFeeItems();
		CPAO_FontevaObjectSelector.getItem(new List<String>{'items'});
		CPAO_FontevaObjectSelector.getRenewalTermsToShow(2018);
		//CPAO_FontevaObjectSelector.getCurrentRenewal(Id subscriptionId);
	}

	@isTest
	static void CPAO_ApplicationSelector() {
		CPAO_Infraction__c infraction = new CPAO_Infraction__c();
		CPAO_Application__c application = new CPAO_Application__c();
		insert application;
		CPAO_ApplicationSelector.getAllUserApplications();
		CPAO_ApplicationSelector.getUserApplicationsWithStatuses(new List<String>{'Open'});
		CPAO_ApplicationSelector.getAllNonAMDApplication(new List<String>{'Open','Submitted'});
		CPAO_ApplicationSelector.getUserApplicationsByRecordtypeName(new List<String>{'AMD/CPD'});
		CPAO_ApplicationSelector.getUserApplicationsByRecordTypeNameAndStatus('Open','CPAO_PAL_Application');
		CPAO_ApplicationSelector.getUserPoaDeclarations();
		CPAO_ApplicationSelector.getUserPreviousFinancialHardshipAMD();
		CPAO_ApplicationSelector.getUserPreviousApplicationsByRecordTypeName('AMD/CPD');
		CPAO_ApplicationSelector.getInfractionApplications(new List<CPAO_Infraction__c>{infraction});
		CPAO_ApplicationSelector.getAppsByStatusAndType('Open', 'AMD/CPD');
		CPAO_ApplicationSelector.getReinstatementApplicationRecordType();
		CPAO_ApplicationSelector.getFMTApplicationRecordType();
		CPAO_ApplicationSelector.getMRAApplicationRecordType();
		CPAO_ApplicationSelector.getCOAApplicationRecordType();
		CPAO_ApplicationSelector.getAMDCPDApplicationRecordType();
		CPAO_ApplicationSelector.getInfractionApplicationRecordType();
		CPAO_ApplicationSelector.getApplicationById(new set<Id> {application.Id});
		CPAO_ApplicationSelector.getRecordType('CPAO_PAL_Application');
		CPAO_ApplicationSelector.getUserApplications('AMD/CPD', new Set<Integer>{10}, 'Open');
		CPAO_ApplicationSelector.getApplicationByRecordTypeAndDecisionDate('AMD/CPD', Date.today(), 'No Previous Licence');
		
		

	}

	@isTest
	static void CPAO_MetadataTypeSelector() {
		CPAO_MetadataTypeSelector.getApplicationDates();
		CPAO_MetadataTypeSelector.getWaiverExplanations();
		CPAO_MetadataTypeSelector.getWaiverExplanation('appWaiverType');
		CPAO_MetadataTypeSelector.getBreachInstructions();
		CPAO_MetadataTypeSelector.getCpdWaiverExplanations();
		CPAO_MetadataTypeSelector.getCpdWaiverExplanation('appWaiverType');
		CPAO_MetadataTypeSelector.getTrienialPeriods();
		CPAO_MetadataTypeSelector.getAMDLines();
		CPAO_MetadataTypeSelector.getAMDLine('locationInfo');
		CPAO_MetadataTypeSelector.getSDOCS_templatesIds('developerName');
		CPAO_MetadataTypeSelector.getRosterPaymentType();
		CPAO_MetadataTypeSelector.getRosterInstruction();
		CPAO_MetadataTypeSelector.getPALAppAttestation();
		CPAO_MetadataTypeSelector.getPALRenewalAttestation();
	}
}