public class CPAO_orderSubUpdateStatusInContactHelper {

	public static void updatesubOrderContact(Map<Id, OrderApi__Subscription__c> subOrderContact) {

		List<Id> contactIds = new List<Id> ();
		List<OrderApi__Subscription__c> subOrderContactObj = new List<OrderApi__Subscription__c> ();
		String mdtName = (Test.isRunningTest()) ? 'Test' : 'Default';
		List<CPAO_Subscription_Status__mdt> subscriptionStatusObj = [SELECT CPAO_startDate__c, CPAO_endDate__c FROM CPAO_Subscription_Status__mdt WHERE DeveloperName = :mdtName limit 1];
		List<Contact> updateContactList = new List<Contact> ();
		Set<Contact> updateContactSet = new Set<Contact> ();

		Map<Id, OrderApi__Subscription__c> contactSubscriptionMap = new Map<Id, OrderApi__Subscription__c> ();

		for (OrderApi__Subscription__c ordersubObj : subOrderContact.values()) {
			if (ordersubObj.OrderApi__Contact__c != null) {
				contactIds.add(ordersubObj.OrderApi__Contact__c);
				contactSubscriptionMap.put(ordersubObj.OrderApi__Contact__c, ordersubObj);
			}
		}

		Map<Id, OrderApi__Subscription_Plan__c> subscriptionPlanMap = new Map<Id, OrderApi__Subscription_Plan__c> ([Select Id, name from OrderApi__Subscription_Plan__c where(Name = :'AMD' or Name = :'PAL' or Name = :'APF')]);
		for (Contact con :[Select Id, name from Contact where Id = :contactIds]) {
			OrderApi__Subscription__c orderSubscription = contactSubscriptionMap.get(con.Id);
			OrderApi__Subscription_Plan__c subplanObj = subscriptionPlanMap.get(orderSubscription.OrderApi__Subscription_Plan__c);

			if (orderSubscription.OrderApi__Activated_Date__c != null && orderSubscription.OrderApi__Expired_Date__c != null) {
				if (subplanObj.Name == 'AMD' && subscriptionStatusObj[0].CPAO_startDate__c <= orderSubscription.OrderApi__Activated_Date__c && orderSubscription.OrderApi__Expired_Date__c <= subscriptionStatusObj[0].CPAO_endDate__c)
				con.CPAO_Subscription_Status_of_Comple_AMD__c = 'Paid';



				else if (subplanObj.Name == 'PAL' && subscriptionStatusObj[0].CPAO_startDate__c <= orderSubscription.OrderApi__Activated_Date__c && orderSubscription.OrderApi__Expired_Date__c <= subscriptionStatusObj[0].CPAO_endDate__c)
				con.CPAO_Subscription_Status_of_Comple_PAL__c = 'Paid';

				else if (subplanObj.Name == 'APF' && subscriptionStatusObj[0].CPAO_startDate__c <= orderSubscription.OrderApi__Activated_Date__c && orderSubscription.OrderApi__Expired_Date__c <= subscriptionStatusObj[0].CPAO_endDate__c)
				con.CPAO_Subscription_Status_of_Comple_APF__c = 'Paid';

				else
				con.CPAO_Subscription_Status_of_Comple_APF__c = 'Submission Outstanding';


				updateContactSet.add(con);
			}
		}

		updateContactList.addALL(updateContactSet);
		if (updateContactList.size() > 0) {
			update updateContactList;
		}

	}

}