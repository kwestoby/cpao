public class CPAO_Application_reinstatement_Email{
    
    public static  String emailTemplate_PAL='CPAO_Reinstatement_Letter_WITH_PAL';
    public static  String emailTemplate_NO_PAL='CPAO_Reinstatement_Letter_NO_PAL'; 
    public static void Find_PAL_License(List <CPAO_Application__c>  Applications){
    List <Id> ContactID = new List<Id>();
    List <ID> PAL_License = new List<Id>();
    List <CPAO_Application__c> lstApprovedApps = [SELECT ID,CPAO_Contact__c FROM CPAO_Application__c where ID in :Applications];
    Set <Id> conID= new Set <Id>();
    Map<Id, List<CPAO_Licence__c>> mapContactsWithLicense= new Map<Id, List<CPAO_Licence__c>>();
    string LicenseName='';
     //get the contact related to application
    for(CPAO_Application__c app : Applications)
        {
                ContactID.add(app.CPAO_Contact__c);                
                app.OwnerId='0053B000001mNNd';
        }      
    //get the license related to contact
    for(Contact objContact: [SELECT id, name,(SELECT ID,NAME,CPAO_Licence_Type__c,CPAO_Status__c from Licenses__r) FROM CONTACT where id in:ContactID]) 
        {
       mapContactsWithLicense.put(objContact.Id,objContact.Licenses__r);
       conID.add(objContact.id);
        }
     for (Id contactId_1 : mapContactsWithLicense.keySet())
        {
     if (mapContactsWithLicense.get(contactId_1) != null)
            {
                for (CPAO_Licence__c lic: mapContactsWithLicense.get(contactId_1)) 
                {
                    If(lic.CPAO_Licence_Type__c=='Public Accounting Licence' && lic.CPAO_Status__c=='Active')
                    {         PAL_License.Add(lic.id);
                              LicenseName=lic.name;
                    }
                }
            }
        }
        //Update contact object
        If (conID.isEmpty() == false)
        {
        List<Contact> listCons = [SELECT id, CPAO_CPA_Status__c from CONTACT WHERE Id in :conID];
        For (Contact oCon : listCons ) 
            {
            oCon.CPAO_CPA_Contact_Type__c='Member';
            oCon.CPAO_CPA_Status__c= 'Active';
            oCon.CPAO_PAL_ID__c=LicenseName;
            }
          Update listCons;
         }
        //Identify the email template to be used based on whether PAL license is available or not.
        if(!PAL_License.IsEmpty()) 
                {
                sendEmails(Applications, emailTemplate_PAL);
                 }
        else if (PAL_License.IsEmpty())
                {
                 sendEmails(Applications, emailTemplate_NO_PAL);
                }           
    }
   public static void sendEmails(List<CPAO_Application__c> applications, String emailTemplateDeveloperName) {
   System.debug('Email:'+emailTemplateDeveloperName );
   
        List<OrgWideEmailAddress> owa = [select id, DisplayName, Address from OrgWideEmailAddress limit 1];
        List<EmailTemplate> emailTemplate = [SELECT Id, Body, Subject from EmailTemplate where DeveloperName =:emailTemplateDeveloperName limit 1];
          if (emailTemplate.size() == 1) {
                List<Messaging.SingleEmailMessage> messages = new List<Messaging.SingleEmailMessage> ();
                for (CPAO_Application__c application : applications) {
                    Messaging.SingleEmailMessage mail = new Messaging.SingleEmailMessage();
                    mail.setTemplateId(emailTemplate[0].Id);
                    mail.setSaveAsActivity(false);
                    mail.setTargetObjectId(application.CPAO_Contact__c);
                    mail.setWhatId(application.Id); // Enter your record Id whose merge field you want to add in template
                    mail.setOrgWideEmailAddressId(owa[0].id);
                    messages.add(mail);
            }
                Messaging.SendEmailResult[] resultMail = Messaging.sendEmail(messages);
        }
    }
}