({
    upsertPAL: function(component){
        
        var action = component.get("c.upsertPalApplication");
        action.setParams({ "PALappToAdd" :component.get('v.currentPAL') });
        action.setCallback(this, function(response){
            var state = response.getState();
            if (state === "SUCCESS") {
                alert('Thank you for submitting your PAL app. \n This is a Placeholder for payment.\n Application now has status "submitted"');
                //window.open('CPAO_ApplicationDashboard');
                window.location.href='/CPAO_ApplicationDashboard';
                
            }else{
                console.log('error while submitting Pal');
                
            }
        });
        
        $A.enqueueAction(action);
    },
    fireReviewSectionModification:function(component,step){
        var modifyReviewSection = component.getEvent("modifyReviewSection");
        modifyReviewSection.setParams({
            "stepToRedirect": step,
        });
        modifyReviewSection.fire();
        //alert('fired');
    }
})