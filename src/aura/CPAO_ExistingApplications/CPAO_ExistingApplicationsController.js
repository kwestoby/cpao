({
	doInit : function(component, event, helper) {
		var apexAction = component.get('c.getApplications');
        apexAction.setCallback(this, function(response) {
        	var state = response.getState();
        	console.log(state);
        	if(state === "SUCCESS"){
                var eventList = response.getReturnValue();
                var openApplicaions = [];
                var submittedNotPaid = [];
                var additionalInfo = [];
                var submitted = [];
                var inReview = [];
                var completed = [];
                var withdrawRequest = [];
                var withdrawn = [];
                var closed = [];
                var cancelled = [];
                if(eventList.length > 0){
                	for(var i = 0; i<eventList.length; i++){
	                	if(eventList[i].CPAO_Status__c == 'Open'){
	                		openApplicaions.push(eventList[i]);
	                	}
	                	if(eventList[i].CPAO_Status__c == 'Submitted not paid'){
	                		submittedNotPaid.push(eventList[i]);
	                	}
	                	if(eventList[i].CPAO_Status__c == 'Additional Information Required'){
	                		additionalInfo.push(eventList[i]);
	                	}
	                	if(eventList[i].CPAO_Status__c == 'Submitted'){
	                		submitted.push(eventList[i]);
	                	}
	                	if(eventList[i].CPAO_Status__c == 'In Review'){
	                		inReview.push(eventList[i]);
	                	}
	                	if(eventList[i].CPAO_Status__c == 'Completed'){
	                		completed.push(eventList[i]);
	                	}
	                	if(eventList[i].CPAO_Status__c == 'Withdrawn'){
	                		withdrawn.push(eventList[i]);
	                	}
	                	if(eventList[i].CPAO_Status__c == 'Closed'){
	                		closed.push(eventList[i]);
	                	}
	                	if(eventList[i].CPAO_Status__c == 'Cancelled'){
	                		cancelled.push(eventList[i]);
	                	}
	                	if(eventList[i].CPAO_Status__c == 'Withdrawal Requested'){
	                		withdrawRequest.push(eventList[i]);
	                	}
	                }
	                if(openApplicaions.length != 0){
	                	component.set("v.openApps", openApplicaions);
	                }
	                if(submittedNotPaid.length != 0){
	                	component.set("v.submittedNotPaid", submittedNotPaid);
	                }
	                if(additionalInfo.length != 0){
	                	component.set("v.additionalInfo", additionalInfo);
	                }
	                if(submitted.length != 0){
	                	component.set("v.submittedApps", submitted);
	                }
	                if(inReview.length != 0){
	                	component.set("v.inReviewApps", inReview);
	                }
	                if(completed.length != 0){
	                	component.set("v.completedApps", completed);
	                }
	                if(withdrawn.length != 0){
	                	component.set("v.withdrawnApps", withdrawn);
	                }
	                if(closed.length != 0){
	                	component.set("v.closedApps", closed);
	                }
	                if(cancelled.length != 0){
	                	component.set("v.cancelledApps", cancelled);
	                }
	                if(withdrawRequest.length != 0){
	                	component.set("v.withdrawRequestApps", withdrawRequest);
	                }
                } else {
                	component.set("v.noApplications", true);
                }
	                
            }
                

        });
        $A.enqueueAction(apexAction);
        var reviewPDFURL = $A.get("$Label.c.Community_URL") + '/FirmRegistrationFields?id=';
        component.set("v.reviewPDFURL", reviewPDFURL);
	},
	cancelApp: function(component, event, helper) {
		if(confirm("Are you sure you want to cancel?")) {
			var action = component.get("c.cancelApplication");
			var appID = event.currentTarget.dataset.value;
			action.setParams({applicationId: appID});
			action.setCallback(this, function(response) {
				var state = response.getState();
				if(state==="SUCCESS") {
					console.log("success");
					var openApplications = component.get("v.openApps");
					var cancelledApps = component.get("v.cancelledApps");
					if(cancelledApps===undefined || cancelledApps===null) {
						cancelledApps = [];
					}
					for(var i=0;i<openApplications.length; i++) {
						if(openApplications[i].Id===appID) {
							openApplications[i].CPAO_Status__c='Cancelled';
							cancelledApps.push(openApplications[i]);
							openApplications.splice(i,1);
							break;
						}
					}
					component.set("v.openApps",openApplications);
					component.set("v.cancelledApps",cancelledApps);
				} else {
					var errors = response.getError();
					if(errors[0] && errors[0].message) {
						console.log(errors[0].message);
					} else {
						console.log("Unknown error occured!");
					}
				}
			});
			$A.enqueueAction(action);
		}
	},

	withdrawApp: function(component, event, helper) {
		if(confirm("Are you sure you want to withdraw?")) {
			var action = component.get("c.withdrawApplication");
			var appID = event.currentTarget.dataset.value;
			console.log('appID');
			console.log(appID);
			action.setParams({applicationId: appID});
			action.setCallback(this, function(response) {
				var state = response.getState();
				console.log(state);
				console.log('state');
				if(state==="SUCCESS") {
					var result = response.getReturnValue();
					console.log("result");
					console.log(result);
					if(result == 'Success'){
						console.log("success");
						var submitedApplications = component.get("v.submittedApps");
						console.log("marco 1");
						var inReviewApplications = component.get("v.inReviewApps");
						console.log("marco 2");
						var withdrawRequestApps = component.get("v.withdrawRequestApps");
						console.log("marco 3");
						if(withdrawRequestApps===undefined || withdrawRequestApps===null) {
							console.log("marco 3b");
							withdrawRequestApps = [];
						}
						console.log("marco 4");
						if(submitedApplications != null || submitedApplications != undefined){
							for(var i=0;i<submitedApplications.length; i++) {
								if(submitedApplications[i].Id===appID) {
									submitedApplications[i].CPAO_Status__c='Withdrawal Requested';
									withdrawRequestApps.push(submitedApplications[i]);
									submitedApplications.splice(i,1);
									break;
								}
							}
						}
							
						console.log("marco 5");
						if(inReviewApplications != null || inReviewApplications != undefined){
							for(var i=0;i<inReviewApplications.length; i++) {
								if(inReviewApplications[i].Id===appID) {
									inReviewApplications[i].CPAO_Status__c='Withdrawal Requested';
									withdrawRequestApps.push(inReviewApplications[i]);
									inReviewApplications.splice(i,1);
									break;
								}
							}
						}
							
						console.log("marco 6");
						component.set("v.submittedApps",submitedApplications);
						component.set("v.inReviewApps",inReviewApplications);
						component.set("v.withdrawRequestApps",withdrawRequestApps);
					}						
				} else {
					var errors = response.getError();
					if(errors[0] && errors[0].message) {
						console.log(errors[0].message);
					} else {
						console.log("Unknown error occured!");
					}
				}
			});
			$A.enqueueAction(action);
		}
	}
})