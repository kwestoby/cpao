public with sharing class CPAO_RecordIdAssignmentWrapper {
	//Wrapper class used for CPA Record Id handling
	//object type string needed since ids can be applid to contacts and Accounts
	//idsAutomaticAssignment is for records which are being assigned numbers according to the current range
	//idsManualAssignment is for records which are being assigned Ids manually
	//idRecordsToTargets list of id records and corresponding target records. 
	//the keys are Id records which are getting their lookup to account/contact filled in
	//and the values are the ids of the Accounts/contacts
	public String objectType;
	public List<Id> idsAutomaticAssignment;
	public Map<Id, Decimal> idsManualAssignment;
	public Map<CPAO_CPA_Id__c, Id> idRecordsToTargets;
	public List<Decimal> idRecordsToRenderInactive;

	public CPAO_RecordIdAssignmentWrapper(String objectType) {
		this.objectType = objectType;
		idsAutomaticAssignment = new List<Id>();
		idsManualAssignment = new Map<Id, Decimal>();
		idRecordsToTargets = new Map<CPAO_CPA_Id__c, Id>();
		idRecordsToRenderInactive = new List<Decimal>();
	}
}