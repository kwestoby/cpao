<?xml version="1.0" encoding="UTF-8"?>
<CustomMetadata xmlns="http://soap.sforce.com/2006/04/metadata" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xmlns:xsd="http://www.w3.org/2001/XMLSchema">
    <label>CPD Audit 1</label>
    <protected>false</protected>
    <values>
        <field>CPAO_Breach_Instruction__c</field>
        <value xsi:type="xsd:string">Please upload your response to the Registrar’s request for CPD Audit</value>
    </values>
    <values>
        <field>CPAO_Breach_Type__c</field>
        <value xsi:type="xsd:string">CPD Audit (failure to comply with request)</value>
    </values>
</CustomMetadata>
