/**
* @File Name    :   CPAO_MilestoneLicenceHelper
* @Description  :   Trigger Milestone Licence Helper
* @Date Created :   2018-07-12
* @Author       :   Michaell Reis Gasparini, Deloitte, mreisg@deloitte.com
* @group        :   Class
* @Modification Log:
**************************************************************************************
* Ver       Date        Author              Modification
* 1.0       2018-07-12  Michaell Reis       Created the file/class
**/
public with sharing class CPAO_MilestoneLicenceHelper {

	public static void historyTracker(Map<Id, CPAO_Licence__c>  licObjMap){


		Map<String,String> triggerPlusCopyFieldMap = new Map<String,String>();
		List<String> triggerfields = new List<String>();
		List<String> copyfields = new List<String>();

		for(CPAO_History_Tracker_Controller__c historytrackerController : [Select Id, name,CPAO_Fields_to_Copy__c, CPAO_ObjectType__c,CPAO_Trigger_Fields__c,CPAO_Active__c from CPAO_History_Tracker_Controller__c where CPAO_ObjectType__c =: 'Licence' and CPAO_Active__c =: True]){
			triggerfields = historytrackerController.CPAO_Trigger_Fields__c.split(',');
			copyfields  = historytrackerController.CPAO_Fields_to_Copy__c.split(',');
			system.debug('triggerfields'+triggerfields );
            //triggerPlusCopyFieldMap.put(globalhistoryController.CPAO_Trigger_Fields__c,globalhistoryController.CPAO_Fields_to_Copy__c);

        }
        
        List<CPAO_MilestoneAndHistory__c> listHistory = new List<CPAO_MilestoneAndHistory__c>();


        for(CPAO_Licence__c licObj: licObjMap.values()){

        	for(integer i=0; i<triggerfields.size(); i++){
        		string fieldAPI = triggerfields[i];
        		String oldFieldValue;

        		if(trigger.oldMap.get(licObj.Id).get(fieldAPI)!= null){
        		
        			if(trigger.oldMap.get(licObj.Id).get(fieldAPI)!= licObj.get(fieldAPI) ){
        				CPAO_MilestoneAndHistory__c history = copyFieldsHistory(licObj, copyfields);
        				listHistory.add(history);
        				break;
        			}
        			}
        	}           

        }
        if(listHistory.size()>0){
        	insert listHistory;
        }
    }  



    public static CPAO_MilestoneAndHistory__c copyFieldsHistory(CPAO_Licence__c licObj, List<String> listFields){

    	id recordTypeId = Schema.SObjectType.CPAO_MilestoneAndHistory__c.getRecordTypeInfosByName().get('License Status').getRecordTypeId();
    	CPAO_MilestoneAndHistory__c history = new CPAO_MilestoneAndHistory__c();
    	history.CPAO_Licence__c = licObj.Id;
    	history.RecordTypeId= recordTypeId;
    	for(integer i=0; i<listFields.size(); i++){
    		string fieldAPI = listFields[i];
                //system.debug('fieldAPI'+fieldAPI);
                //String oldFieldValue = trigger.oldMap.get(licObj.Id)+'.'+fieldAPI;
                

                history.put(fieldAPI,trigger.oldMap.get(licObj.Id).get(fieldAPI) );


            }
            return history;
        }
    }