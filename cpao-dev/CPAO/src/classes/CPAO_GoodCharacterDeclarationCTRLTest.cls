@isTest
private class CPAO_GoodCharacterDeclarationCTRLTest {
	
	public static List<Account> factoryAccounts;
	public static List<Contact> factoryContacts;
	public static User currentUser;
	public static User user;
	public static RecordType amdCPDRecordType;

	public static void dataInitialize(Integer numberOfAccounts, Integer numberOfContacts){
		CPAO_TestFactory.automationControlSetup();
		factoryAccounts = CPAO_TestFactory.createAccounts(numberOfAccounts);
		insert factoryAccounts;
		factoryContacts = CPAO_TestFactory.createContacts(factoryAccounts[0],numberOfContacts);

		insert factoryContacts;
		
        
        
		user = CPAO_TestFactory.createCommunityUser(factoryContacts[0].Id);
        insert user;
        

        currentUser = CPAO_UserSelector.getCurrentUser();
        amdCPDRecordType = [SELECT Id FROM RecordType 
												WHERE Name = :CPAO_ApplicationHandler.AMD_CPD 
												AND SobjectType = 'CPAO_Application__c'][0];
    }

    @isTest
	public static void basicTest() {
		dataInitialize(1,1); 
		CPAO_Application__c application = new CPAO_Application__c();
		
		application.CPAO_Contact__c = factoryContacts[0].Id;
		application.CPAO_Status__c = 'Open';
		application.RecordTypeId = amdCPDRecordType.Id;
		application.CPAO_JSON_File_Map__c = '{}';
		insert application;
		application = [SELECT Id, CPAO_JSON_File_Map__c FROM CPAO_Application__c WHERE Id = :application.Id];
		List<String> keyList = new List<String>{'test'};
		List<String> fileNameList = new List<String>{'test'};
    	List<String> base64DataList = new List<String>{'1010101'};
    	List<String> contentTypeList = new List<String>{'html'};

		CPAO_GoodCharacterDeclarationCTRL.saveApplication(keyList, fileNameList, base64DataList, contentTypeList, application);
		CPAO_GoodCharacterDeclarationCTRL.saveApplication(keyList, fileNameList, base64DataList, contentTypeList, application);

	}
	
}