global class CPAO_AutoRenewWaiverAmdSubsBatch implements Database.Batchable<sObject> {
	
	global Database.QueryLocator start(Database.BatchableContext BC) {
		Integer currentYear = date.today().year(); 
		String query='SELECT Id, OrderApi__Subscription__r.OrderApi__Contact__r.CPAO_CPD_Continuous_Exemption__c FROM OrderApi__Renewal__c WHERE CALENDAR_YEAR(OrderApi__Term_Start_Date__c) = :currentYear ';
        query+= 'AND OrderApi__Subscription__r.OrderApi__Contact__r.CPAO_AMD_Continuous_Waiver__c != null';
		return Database.getQueryLocator(query);
	}

   	global void execute(Database.BatchableContext BC, List<OrderApi__Renewal__c> terms) {
		if(terms.isEmpty()){
			return;
		}
    	
    	for(OrderApi__Renewal__c term:terms){
    		term.OrderApi__Renewed_Date__c = Date.today();
    		if(term.OrderApi__Subscription__r.OrderApi__Contact__r.CPAO_CPD_Continuous_Exemption__c != null){
    			term.CPAO_Hide_Term__c = true;
    		}
    	}
    	update terms;
	}
	
	global void finish(Database.BatchableContext BC) {
		
	}
	
}