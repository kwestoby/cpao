/**
* @File Name    :   CPAO_Milestone_Licence
* @Description  :   Trigger Milestone Licence
* @Date Created :   2018-07-12
* @Author       :   Michaell Reis Gasparini, Deloitte, mreisg@deloitte.com
* @group        :   Trigger
* @Modification Log:
**************************************************************************************
* Ver       Date        Author              Modification
* 1.0       2018-07-12  Michaell Reis       Created the file/class
*  2.0      2018-07-26  Adriano Silva       Changed to the standard
**/
trigger CPAO_Licence on CPAO_Licence__c (before insert,before update) {

 CPAO_Automation_Control__c control = CPAO_Automation_Control__c.getOrgDefaults();
    if(!control.CPAO_Licence__c){
        return;
    }
    
    if(Trigger.isBefore && Trigger.isInsert){
   
    }
    if(Trigger.isBefore && Trigger.isUpdate){  
        CPAO_MilestoneHelper.historyTracker(Trigger.New);
    }

   /* if(Trigger.isAfter && Trigger.isInsert){
       
    }

    if(Trigger.isAfter && Trigger.isUpdate){
       
    }

    if(Trigger.isAfter && Trigger.isDelete){
       
    }*/
   
}