@isTest
private class CPAO_AttendeeHandlerTest{

    public static List<Account> accounts;
    public static List<Contact> contacts;
    public static User communityUser;
    
    public static void dataInitialize(Integer contactAmount) {
        CPAO_TestFactory.automationControlSetup();
        accounts = CPAO_TestFactory.createAccounts(2);
        accounts[1].Name = 'Retirement';
        insert accounts;
        contacts = CPAO_TestFactory.createContacts(accounts[0], contactAmount);
        insert contacts;
        
    }
    
    @isTest
    static void itShould() {
        dataInitialize(1);
        EventApi__Attendee__c  eventAttendee = CPAO_FontevaTestFactory.createAttendee();
        EventApi__Event__c event = [SELECT Id FROM EventApi__Event__c WHERE Id = :eventAttendee.EventApi__Event__c];
        event.EventApi__Start_Date_Time__c  = Date.today().addDays(5);
        event.CPAO_Hours_to_Cancel_Registration__c = 1;
        update event;
        eventAttendee.EventApi__Refund_Requested__c = true;
        insert eventAttendee;
        eventAttendee.EventApi__Refund_Requested__c = false;
        update eventAttendee;
        eventAttendee.EventApi__Refund_Requested__c = true;
        update eventAttendee;
    }
}