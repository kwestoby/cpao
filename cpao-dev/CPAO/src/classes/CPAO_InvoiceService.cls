public class CPAO_InvoiceService {
	public static void handleSubscriptionRenewal(List<OrderApi__Invoice__c> invoices) {
		Set<Id> paidInvoiceIds = new Set<Id> ();
		for (OrderApi__Invoice__c invoice : invoices) {
			if (invoice.OrderApi__Is_Paid__c) {
				paidInvoiceIds.add(invoice.Id);
			}
		}
		if (paidInvoiceIds.size() > 0) {
			Map<Id, Set<Id>> invoiceSubscriptionMap = CPAO_InvoiceSelector.getInvoiceSubscriptionSet(paidInvoiceIds);
			Set<Id> subscriptionRenewalSet = new Set<Id> ();
			for (Set<Id> invSubscriptionSet : invoiceSubscriptionMap.values()) {
				subscriptionRenewalSet.addAll(invSubscriptionSet);
			}
			if (subscriptionRenewalSet.size() > 0) {
				CPAO_SubscriptionService.renewSubscription(subscriptionRenewalSet);
			}
		}
	}
}