@isTest

public class CPAO_ClearEmployerIsPayingPALRTest {
	public static List<Account> accounts;
	public static List<Contact> contacts;
	//public static List<Contact> n


	static void dataInitialize(Integer contactAmount,String contactType){
		CPAO_TestFactory.automationControlSetup();
		accounts = CPAO_TestFactory.createAccounts(1);
		insert accounts;
		contacts = CPAO_TestFactory.createContacts(accounts[0], contactAmount);

		for (Contact contact: contacts){
			contact.CPAO_Employer_is_paying_PALR__c='Yes';
			contact.CPAO_CPA_Contact_Type__c = contactType;
		}
		insert contacts;

	}

	@isTest
	static void itShouldPass() {
		dataInitialize(1,'Member');
		Test.startTest();
		CPAO_ClearEmployerIsPayingPALR b = new CPAO_ClearEmployerIsPayingPALR();
		database.executebatch(b);
		Test.stopTest();
		List<Contact> contactsUpdated = [SELECT Id, CPAO_Employer_is_paying_PALR__c FROM Contact];
		for (Contact contact: contactsUpdated){
			System.assert(contact.CPAO_Employer_is_paying_PALR__c == null);
		}
	
	}


	@isTest
	static void itShouldFail() {
		dataInitialize(1,'Lead');
		Test.startTest();
		CPAO_ClearEmployerIsPayingPALR b = new CPAO_ClearEmployerIsPayingPALR();
		database.executebatch(b);
		Test.stopTest();
		List<Contact> contactsUpdated = [SELECT Id, CPAO_Employer_is_paying_PALR__c FROM Contact];
		for (Contact contact: contactsUpdated){
			System.assert(contact.CPAO_Employer_is_paying_PALR__c != null);
		}
	
	}

}