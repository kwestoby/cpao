({
    upsertPAL : function(component) {
        console.log( 'returned application: '+JSON.stringify(component.get('v.currentMRA')));
        var action = component.get("c.upsertPalApplication");		
        action.setParams({ "PALappToAdd" :component.get('v.currentMRA') });
        action.setCallback(this, function(response){
            console.log('callback');
            var state = response.getState();
            if (state === "SUCCESS") {
                console.log('PAL added');
                if(component.get('v.shouldSaveAndQuit')){
                    window.location.href='/CPAO_ApplicationDashboard';
                    
                }else{
                    component.set('v.currentMRA',response.getReturnValue());
                    var applicationHasChanged = component.getEvent("applicationHasChanged");
                    applicationHasChanged.setParams({
                        "currentApplication": component.get("v.currentMRA"),
                        "nextButtonClickeable": "true"
                    });
                    applicationHasChanged.fire();
                }
                
            }else{
                console.log('error while adding Pal');
                
            }
        });
        
        $A.enqueueAction(action);
        
    }/*,
  	upload: function(component, file, fileContents) {
       
        	var uploadName=component.get('v.currentPAL.CPAO_Special_Consideration_File_Name__c');
        	var oldUploadId=component.get('v.currentPAL.CPAO_Special_Consideration_File_Id__c');
        	var oldFileId;
        	var newFileId;
        	var currentUpload=component.get('v.CriminalOffenceFileName');
        


         if(component.get('v.fileInput_CriminalOffence')==null){
         	       	alert('nothing new to upload,, keep old file');

         		this.savePalApplication(component);

         }else{

         	alert('notnull, thats a new file');
         	if(oldUploadId!=null){
         		  var action = component.get("c.saveTheFile"); 
  	            action.setParams({
  	            parentId: component.get("v.currentPAL.Id"),
  	            fileName: component.get("v.fileName_CriminalOffence"),
  	            base64Data: encodeURIComponent(fileContents), 
  	            contentType: component.get("v.fileType_CriminalOffence"),
  	            fileTitle: component.get('v.fileTitle_CriminalOffence'),
  	            idFileToReplace:oldUploadId
  	        });
         	}else{
         		  var action = component.get("c.saveTheFile"); 
  	        action.setParams({
  	            parentId: component.get("v.currentPAL.Id"),
  	            fileName: component.get("v.fileName_CriminalOffence"),
  	            base64Data: encodeURIComponent(fileContents), 
  	            contentType: component.get("v.fileType_CriminalOffence"),
  	            fileTitle: component.get('v.fileTitle_CriminalOffence'),
  	            idFileToReplace:null
  	        });
         	}
  	      

  	        action.setCallback(this, function(response) {
  	              var state = response.getState();
  	              if (state === "SUCCESS") {
  	              var attachId = response.getReturnValue().fileId;
  	              alert('retuned'+attachId);
  	                component.set('v.currentPAL.CriminalOffenceFileId',attachId);
  	               var fileName=response.getReturnValue().fileName;
  	               component.set('v.currentPAL.CPAO_Criminal_Offence_File_Name__c',fileName);
  	                            

  	                this.savePalApplication(component);
  	                

  	              }else{
  	                console.log('error while adding attachment'+ component.get("v.fileName_CriminalOffence") +  component.get("v.fileTitle_CriminalOffence"));
  	                var errors=response.getError();
  	   		      	var message = errors[0];
  	   
  	   		      			      console.log(message);

  	              }
  	           
  	        });
  	            
  	       
  	        $A.enqueueAction(action); 
  	    }
    //  }
    },
    savePalApplication:function(component) {
        var action = component.get("c.upsertPalApplication"); 
    
         action.setParams({ "PALappToAdd":component.get('v.currentPAL') });
          action.setCallback(this, function(response){
          console.log('callback file upload'  );
            var state = response.getState();
           // alert(state);
            if (state === "SUCCESS") {
              component.set('v.currentPAL',response.getReturnValue());
            // component.set('v.NotYetUploaded','false');

              var applicationHasChanged = component.getEvent("applicationHasChanged");
              applicationHasChanged.setParams({
                "currentApplication": component.get("v.currentPAL"),
                "comingFromFileUpload": "true",
                "nextButtonClickeable": "true"
              });
              applicationHasChanged.fire();
            
            
            }else{
              console.log('error while adding id of file step professional experience');
            }
        });
      
        $A.enqueueAction(action);


    }*/
})