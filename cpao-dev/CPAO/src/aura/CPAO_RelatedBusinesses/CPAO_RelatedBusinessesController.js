({
    doInit : function(component, event, helper) {
        helper.initialize(component, helper);
    },

    addNewRelationship : function(component, event, helper) {
        var children = component.get("v.children");
        var newChild = helper.getDefault(component, children);
        children.push(newChild);
        component.set("v.children", children);
    },

    addNewBusiness : function(component, event, helper) {
        var newAccount = component.get("v.newAccount");
        if(!newAccount){
            newAccount = {};
            newAccount.sobjectType = 'Account';
            newAccount.BillingCountry = 'Canada';
            newAccount.BillingCountryCode = 'CA';
            component.set("v.newAccount", newAccount);
        }
    },

    onSaveBusiness : function(component, event, helper) {
        var newAccount = component.get("v.newAccount");
        helper.saveNewBusiness(component, helper, newAccount);
    },

    onEdit : function(component, event, helper) {
        var index = event.currentTarget.dataset.index;
        var children = component.get("v.children");
        var child = children[index];

        if(child.isEdit) {
            helper.saveRelationship(component, helper, [child]);
        }
        else {
            child.isEdit = true;
            component.set("v.children", children);
        }
    },

    onRemove : function(component, event, helper) {
        var index = event.currentTarget.dataset.index;
        var children = component.get("v.children");
        var child = children[index];

        if(child) {
            children.splice(index, 1);
        }
        component.set("v.children", children);
    }
})