public class CPAO_ConcatDesignations {
    
    
    public static void concatDesignations(Set<Id> DesignationconIds, map<id, CPAO_Designation_Award__c> oldDesig, map<id, CPAO_Designation_Award__c> newDesig  ){
    
        List<contact> cont = new List<contact>();
        Set<Id> setFellow = new Set<Id>();
        
        //To store associated contact records
        cont = [Select Id, Name, CPAO_Designation__c, (Select Id, CPAO_Country__c, CPAO_Accounting_Designations_Awards__c from Designations_Awards__r) from contact where Id in :DesignationconIds];
        
        if(cont.size()>0){
            //To store all Fellow records
            for(Contact con: cont){
                for(CPAO_Designation_Award__c s: con.Designations_Awards__r){
                    if(s.CPAO_Accounting_Designations_Awards__c.equalsIgnoreCase('fellow') && s.CPAO_Country__c == 'Canada'){
                        setFellow.add(con.Id);
                    }
                }
            }
            
            //Loop through all the associated records to concatenate designations to contact record
            for(contact o : cont){
                o.CPAO_Designation__c = '';
                if(o.Designations_Awards__r != null && o.Designations_Awards__r.size()>0){
                    for(CPAO_Designation_Award__c s: o.Designations_Awards__r){
                        if(s.CPAO_Country__c == 'Canada'){
                            if(!o.CPAO_Designation__c.containsIgnoreCase(s.CPAO_Accounting_Designations_Awards__c)){ 
                                o.CPAO_Designation__c += ((setFellow.contains(o.Id) && !s.CPAO_Accounting_Designations_Awards__c.equalsIgnoreCase('fellow')) ? 'F' + s.CPAO_Accounting_Designations_Awards__c + ';' : s.CPAO_Accounting_Designations_Awards__c + ';');
                            }
                        }else
                        {
                             if(o.CPAO_Designation__c.contains(s.CPAO_Accounting_Designations_Awards__c) && oldDesig.get(s.Id).CPAO_Country__c != newDesig.get(s.Id).CPAO_Country__c){
                                 o.CPAO_Designation__c = o.CPAO_Designation__c.remove(s.CPAO_Accounting_Designations_Awards__c);
                             }
                        }    
                    }
                    if(o.CPAO_Designation__c.containsonly('CPA')){
                        o.CPAO_Designation__c= 'CPA' + ';' + o.CPAO_Designation__c.remove('CPA');
                    }else if (o.CPAO_Designation__c.containsonly('FCPA')){
                        o.CPAO_Designation__c = 'CPA' + ';' + o.CPAO_Designation__c.remove('FCPA');
                    }
                } 
            }
            update cont;
        }
    }
    
}