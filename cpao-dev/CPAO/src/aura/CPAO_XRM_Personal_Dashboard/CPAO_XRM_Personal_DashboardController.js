({
	handleViewAllClick : function(component, event, helper) {
		var title = event.getSource().get("v.title");
        var currentTab = "personal_dashboard";
        if(title=="Applications") {
            currentTab = "applications";
        } else if(title=="Annual Obligations") {
            currentTab = "annual_obligations";
        } else if(title=="Transaction History") {
            currentTab = "transaction_history";
        }
        component.set("v.currentTab", currentTab);
	},
    doInit : function(component, event, helper) {
        console.log('loading Application Dashboard');
        
    },

    showSpinner : function(component, event, helper) {
        helper.showSpinner(component);
    }

})