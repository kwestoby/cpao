<?xml version="1.0" encoding="UTF-8"?>
<CustomMetadata xmlns="http://soap.sforce.com/2006/04/metadata" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xmlns:xsd="http://www.w3.org/2001/XMLSchema">
    <label>Base</label>
    <protected>false</protected>
    <values>
        <field>AMD_Deferral_Date__c</field>
        <value xsi:type="xsd:date">2018-06-11</value>
    </values>
    <values>
        <field>AMD_Late_Fee_Date__c</field>
        <value xsi:type="xsd:date">2018-05-21</value>
    </values>
    <values>
        <field>AMD_Renewal_Date__c</field>
        <value xsi:type="xsd:date">2018-04-01</value>
    </values>
    <values>
        <field>CPAO_Date_PAL_Appears_On_Portal__c</field>
        <value xsi:type="xsd:date">2018-07-19</value>
    </values>
    <values>
        <field>CPAO_PAL_Renewal_Effective_Date__c</field>
        <value xsi:type="xsd:date">2017-11-01</value>
    </values>
    <values>
        <field>CPAO_PAL_Renewal_Expiry_Date__c</field>
        <value xsi:type="xsd:date">2017-10-31</value>
    </values>
    <values>
        <field>CPAO_Reset_Employer_is_paying_PALR__c</field>
        <value xsi:type="xsd:date">2018-01-01</value>
    </values>
    <values>
        <field>CPD_Due_Date__c</field>
        <value xsi:type="xsd:date">2018-04-01</value>
    </values>
    <values>
        <field>CPD_End_Date__c</field>
        <value xsi:nil="true"/>
    </values>
    <values>
        <field>CPD_Exemption_Effective_Date__c</field>
        <value xsi:type="xsd:date">2018-04-01</value>
    </values>
    <values>
        <field>CPD_Exemption_Expiry_Date__c</field>
        <value xsi:type="xsd:date">2018-03-31</value>
    </values>
    <values>
        <field>CPD_Infraction_Date__c</field>
        <value xsi:type="xsd:date">2018-07-02</value>
    </values>
    <values>
        <field>CPD_Late_Fee_Date__c</field>
        <value xsi:type="xsd:date">2018-06-02</value>
    </values>
    <values>
        <field>CPD_Start_Date__c</field>
        <value xsi:nil="true"/>
    </values>
    <values>
        <field>Date_AMD_CPD_Appears_in_Portal__c</field>
        <value xsi:type="xsd:date">2018-04-01</value>
    </values>
    <values>
        <field>Member_Pricing_Reset_Date__c</field>
        <value xsi:type="xsd:date">2018-04-01</value>
    </values>
</CustomMetadata>
