({
	doInit : function(component, event, helper) {
        var action = component.get("c.getBusinessUnitOptions");
   	    action.setCallback(this, function(response){
	      var state = response.getState();
	      if (state === "SUCCESS") {
	         component.set("v.businessUnitOptions", response.getReturnValue());
	      }
         });

        var action2=component.get("c.getPositionCategoryOptions");
        action2.setCallback(this, function(response){
	        var state = response.getState();
	        if (state === "SUCCESS") {
	            component.set("v.positionCategoryOptions", response.getReturnValue());
	         }
      	});

        var action3=component.get("c.getLoggedUser");
        action3.setCallback(this, function(response){
	        var state = response.getState();
	        if (state === "SUCCESS") {
	          component.set("v.loggedUser", response.getReturnValue());
	        }
        });
	    $A.enqueueAction(action);
        $A.enqueueAction(action2);
	    $A.enqueueAction(action3);

	},
	addEmployer: function(component, event, helper) {
		var validForm = component.find('newEmployerform').reduce(function (validSoFar, inputCmp) {
            // Displays error messages for invalid fields
            inputCmp.showHelpMessageIfInvalid();
            return validSoFar && inputCmp.get('v.validity').valid;
        }, true);
        // If we pass error checking, do some real work
        if(validForm){
			var primaryEmployerIsChecked = component.find('primaryEmployer').get('v.checked');
			component.set("v.accountContactRel.CPAO_Contact__c",component.get("v.loggedUser")[0].ContactId);
			var action= component.get("c.isPrimaryEmployer");
		    action.setParams({
	            "contactId": component.get('v.accountContactRel.CPAO_Contact__c')
	        });
		    action.setCallback(this, function(response){
	            var state = response.getState();
	            if (state === "SUCCESS") {
	      	
	      			if(response.getReturnValue().length>0 && primaryEmployerIsChecked){
	      				console.log(response.getReturnValue()[0]);
	    			    var action3= component.get("c.changePrimaryEmployer");
						action3.setParams({
	      				  "relationshipId": response.getReturnValue()[0].Id,
	      				  "doInsert" : true
	   				    });
						action3.setCallback(this, function(response){
	     				   var state = response.getState();
	                       if (state === "SUCCESS") {
	     				      helper.creationOfNewEmployer(component,event);

	      					}
	  					});
	 				   $A.enqueueAction(action3);	      	
	                }else if(response.getReturnValue().length==0 && (!primaryEmployerIsChecked)){
	      				component.set('v.severity','error');
	      	            component.set("v.confirmationMessage"," Error: You don't have any primary employer. Please make this one your primary");
	      	        }else{
	          			 helper.creationOfNewEmployer(component,event);
					}
	            }else{
	      		  console.log('errOR');
	      	    }
            });
		    $A.enqueueAction(action);
	    }
    },
	handleAddedEmployer:function(component, event, helper) {
	  // var addedEmployerEvent = component.getEvent("newEmployerAdded");
      // var listOfRelationships=addedEmployerEvent.getParam("listOfRelationships");
       console.log('fired ');

    }
})