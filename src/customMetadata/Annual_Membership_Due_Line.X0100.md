<?xml version="1.0" encoding="UTF-8"?>
<CustomMetadata xmlns="http://soap.sforce.com/2006/04/metadata" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xmlns:xsd="http://www.w3.org/2001/XMLSchema">
    <label>0100</label>
    <protected>false</protected>
    <values>
        <field>AMD_Location_Based_Reduction__c</field>
        <value xsi:type="xsd:string">Category 0100 – Member is paying full CPA Ontario and Full CPA Canada fees to CPAO</value>
    </values>
    <values>
        <field>CPA_Canada_Fee__c</field>
        <value xsi:type="xsd:double">400.0</value>
    </values>
    <values>
        <field>CPA_Ontario_Fee__c</field>
        <value xsi:type="xsd:double">580.0</value>
    </values>
    <values>
        <field>Live_In_Canada_Bermuda__c</field>
        <value xsi:type="xsd:string">Yes</value>
    </values>
    <values>
        <field>Paying_CPAC_Through_CPAO__c</field>
        <value xsi:type="xsd:string">Yes</value>
    </values>
    <values>
        <field>Paying_Provincial_Through_Ontario__c</field>
        <value xsi:type="xsd:string">Yes</value>
    </values>
</CustomMetadata>
