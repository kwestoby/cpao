({
	doInit : function(component, event, helper) {
		console.log('doinit');
		var action = component.get("c.componentInit");
	   	action.setCallback(this, function(response){
		      var state = response.getState();
		      if (state === "SUCCESS") {
		      	console.log(JSON.parse(response.getReturnValue()));
		      	var currentInfractions=JSON.parse(response.getReturnValue()).currentInfractions;
                  var breachInstructions=JSON.parse(response.getReturnValue()).breachInstructions;
		      	var infractionTypes = JSON.parse(response.getReturnValue()).infractionTypes;
		      	var reinstatementApplication = JSON.parse(response.getReturnValue()).reinstatementApplication;
		      	var currentInfractionsMap = JSON.parse(response.getReturnValue()).currentInfractionsMap;
		      	//this var is a reference to where we should redirect the user once he clicks on "Link to complete Form"
		      	var controllerReference=JSON.parse(response.getReturnValue()).infractionWireframeControllerReference;
		      	component.set('v.infractionWireframeControllerReference',controllerReference);
		      	component.set('v.listOfInfractions',currentInfractions);
                  component.set('v.listOfInstructions',breachInstructions);
		      	component.set('v.relatedReinstatementApplication',reinstatementApplication);
		      	component.set('v.InfractionsMap',currentInfractionsMap);
                console.log('reinstatement application id: '+reinstatementApplication.Id);
                 console.log('reinstatement application number: '+reinstatementApplication.Name);
		      	helper.verifyIfAllFormsAreCompleted(component,currentInfractions);

                
		    	
		      }else{
		      	console.log('error while doinit');
		      }
	    });
	  
	    $A.enqueueAction(action);
	},
	redirectToInfractionForm : function(component, event, helper) {
		//var urlForm='https://dev-fontevacustomer-15ecea91a46.cs50.force.com/cpbase__form?ID=a0S3B000000xVXd&a0O3B000000dTsj_SOQL='
      	//URL https://dev-fontevacustomer-15ecea91a46.cs50.force.com/cpbase__form?ID=a0S3B000000xVXd&a0O3B000000dTsj_SOQL=
      	var urlForm =component.get('v.infractionWireframeControllerReference');
      	var idOfInfraction=event.target.value;
      	console.log('INFRACTION IF'+idOfInfraction);
      	var application=component.get('v.relatedReinstatementApplication').Id;
      	console.log('Application Id'+application);
      	var soqlfilter ="Id='"+idOfInfraction+"'";
      	var encodedSoql=encodeURI(soqlfilter);
      	var encodedUrl=urlForm+encodedSoql;	
      	console.log(encodedUrl);
      	var action = component.get("c.linkInfractionToReinstatementApplication");
      	action.setParams({
      	'infraction':idOfInfraction,
      	'application':application
      	});
      	action.setCallback(this, function(response){
        var state = response.getState();
		      if (state === "SUCCESS") {
		      	console.log('success');
		      	console.log(response.getReturnValue());
		      	window.location.href=encodedUrl;
		      }else{
		      	console.log('error while linking infraction to reinstaement app');
		      }
		});
        $A.enqueueAction(action);
		
	},

	nextButton : function(component, event, helper) {
		console.log("marco test Next");
		var externalEvent = $A.get("e.c:CPAO_ExternalEventInfractionFormsCompleted");
	    externalEvent.setParams({'type':'Next'});
	   	externalEvent.fire(); 
	},

	previousButton : function(component, event, helper) {
		console.log("marco test previous");
		var externalEvent = $A.get("e.c:CPAO_ExternalEventInfractionFormsCompleted");
	    externalEvent.setParams({'type':'Previous'});
	   	externalEvent.fire(); 
	},

	cancelButton : function(component, event, helper) {
		console.log("marco test cancel");
		var externalEvent = $A.get("e.c:CPAO_ExternalEventInfractionFormsCompleted");
	    externalEvent.setParams({'type':'Cancel'});
	   	externalEvent.fire(); 
	},


})