public without sharing class CPAO_PAL_Application_CTRL {
    
    public static final String ACTIVE = 'Active';
    public static final String OPEN = 'Open';
    public static final String PAL_APPLICATION = 'CPAO_PAL_Application';
    public static User LOGGED_USER = getLoggedUserInfo();
    public static List<RecordType> PAL_APPLICATION_RECORD_TYPE = CPAO_ApplicationSelector.getPALApplicationRecordType();
    
    @AuraEnabled
    public static User getLoggedUserInfo() {
        return CPAO_UserSelector.getCurrentUser();
    }
    
   @AuraEnabled	
    public static void upsertFirmApproval(String NatureOfService) {
        
        CPAO_Application__c app = new CPAO_Application__c();
        Contact c = new Contact();
        //c.CPAO_AMD_Category__c = test;
        //c.CPAO_AMD_Tax_Rate__c = Address;
            insert c;
        //upsert FMTAppToAdd;
        //Contact c = [select id from contact where id = '0033B00000K54gv'];
        //c.CPAO_AMD_Category__c = test;
            //update c;
        app.CPAO_Nature_of_Services_Provided__c = NatureOfService;
        
        insert app;
        //return FMTAppToAdd;
        
    }
    
    @AuraEnabled
    public static initWrapper getCurrentPALApplication() {
        System.debug('CPAO_PAL_Application_CTRL::getCurrentPALApplication()');
        initWrapper response = new initWrapper();
        Id contactID = LOGGED_USER.ContactId;
        List<Contact> contactStatus = [Select CPAO_CPA_Status__c from Contact Where Id=:contactID limit 1];
        response.contactStatus = contactStatus[0].CPAO_CPA_Status__c;
        List<CPAO_Application__c> currentPalApps =
            CPAO_ApplicationSelector.getUserApplicationsByRecordTypeNameAndStatus('Open', PAL_APPLICATION);
        if (currentPalApps.size() > 0) {
            //return currentPalApps[0];
            response.currentPALApplication = currentPalApps[0];
        } else {
            try {
                CPAO_Application__c currentPalApp = new CPAO_Application__c();
                currentPalApp.CPAO_Contact__c = LOGGED_USER.ContactId;
                currentPalApp.RecordTypeId = PAL_APPLICATION_RECORD_TYPE[0].Id;
                currentPalApp.CPAO_Status__c = 'Open';
                currentPalApp.CPAO_Current_Step__c = 'step1';
                insert currentPalApp;
                //return currentPalApp;
                response.currentPALApplication = currentPalApp;
            } catch(DmlException ex) {
                if (ex.getMessage().containsIgnoreCase(CPAO_ApplicationServiceUtils.APPLICATION_ALREADY_IN_PROGRESS)) {
                    throw new AuraHandledException(CPAO_ApplicationServiceUtils.APPLICATION_ALREADY_IN_PROGRESS);
                } else if (ex.getMessage().containsIgnoreCase(CPAO_ApplicationServiceUtils.APPLICATION_DUPLICATE_PORTAL) || ex.getMessage().equalsIgnoreCase(CPAO_ApplicationServiceUtils.APPLICATION_DUPLICATE)) {
                    throw new AuraHandledException(CPAO_ApplicationServiceUtils.APPLICATION_DUPLICATE_PORTAL);
                } else {
                    throw new AuraHandledException('An error occured while creating new application, please contact CPA Ontario\'s Customer Service team.');
                }
            }
        }
        List<FilesWrapper> files = new List<FilesWrapper> ();
        FilesWrapper file = new FilesWrapper();
        file.parentId = response.currentPALApplication.Id;
        file.fileName = 'empty';
        file.base64Data = 'empty';
        file.contentType = 'empty';
        file.idFileToReplace = 'empty';
        file.fileTitle = 'empty';
        //file.fileId=null; 
        files.add(file);
        response.ScheduleA_B = files;
        return response;
    }
    
    /*@AuraEnabled
public static String componentInit(){
ResultWrapper response = new ResultWrapper();
response.LoggedUserId=LOGGED_USER.ContactId;
response.PALRecordTypeId=PAL_APPLICATION_RECORD_TYPE[0].Id;
response.userHasPreviousPAL=userHasPreviousPALApplications();
return JSON.serialize(response) ;
}*/
    
    
    @AuraEnabled
    public static Boolean userHasPreviousPALApplications() {
        List<CPAO_Application__c> oldPalApps = CPAO_ApplicationSelector.getUserPreviousApplicationsByRecordTypeName('CPAO_PAL_Application');
        return(oldPalApps.size() > 0);
    }
    
    @AuraEnabled
    public static CPAO_Application__c upsertPalApplication(CPAO_Application__c PALappToAdd) {
        
        //generateSalesOrder(PALappToAdd);
        
        upsert PALappToAdd;
        return PALappToAdd;
        
    }
    
    @AuraEnabled
    public static PaymentPageWrapper submitPalApplication(CPAO_Application__c PALappToAdd) {
        
        PaymentPageWrapper results = new PaymentPageWrapper();
        
        generateSalesOrder(PALappToAdd);
        
        results.salesOrderID = PALappToAdd.CPAO_Sales_Order__c;
        //results.userID = LOGGED_USER.Id;
        results.userID = UserInfo.getUserId();
        results.orgID = UserInfo.getOrganizationId();
        //cookieController();
        
        upsert PALappToAdd;
        
        //return results;
        
        return results;
        
    }
    
    public static void generateSalesOrder(CPAO_Application__c app){
        //List<OrderApi__Sales_Order__c> salesOrderList = new List<OrderApi__Sales_Order__c>();
        List<OrderApi__Sales_Order_Line__c> salesOrderLines;
        
        if(app.CPAO_Sales_Order__c == null){
            OrderApi__Sales_Order__c applicationSalesOrder = new OrderApi__Sales_Order__c();
            applicationSalesOrder.OrderApi__Contact__c = app.CPAO_Contact__c;
            applicationSalesOrder.OrderApi__Entity__c = 'Contact';
            insert applicationSalesOrder;
            
            salesOrderLines = generateSalesOrderLine(app.CPAO_Application_Type__c,applicationSalesOrder);
            app.CPAO_Sales_Order__c = applicationSalesOrder.Id;
            //salesOrderList.add(applicationSalesOrder);
            
            System.debug('im here dan');
            
           // upsert salesOrderList;
            
        	insert salesOrderLines;
            
        }else{
            System.debug('im outside dan');
            //if sales order already exists
            //List<OrderApi__Sales_Order__c> 
            //subscriptionSalesOrder = app.CPAO_Sales_Order__c;
        }
        
        

        
    }
    
    public static List<OrderApi__Sales_Order_Line__c> generateSalesOrderLine(String name, OrderApi__Sales_Order__c salesOrder){
        
        List<OrderApi__Item__c> items;
        List<OrderApi__Sales_Order_Line__c> salesOrderLines = new List<OrderApi__Sales_Order_Line__c>();
        
        List<CPAO_Application_Item__mdt> applicationItems = CPAO_MetadataTypeSelector.getApplicationItems(name); 
        
        //get all the items needed
        if(applicationItems.size() != 0){
            //for(CPAO_Application_Item__mdt appItem: applicationItems){
			items = [SELECT Id FROM OrderApi__Item__c WHERE (name=:applicationItems[0].CPAO_Item_Name__c OR name=:applicationItems[0].CPAO_Item_Name_Late_Fee__c)];
            //}
        }
        
        //make all the sales order lines
        if(items != null){
            for(OrderApi__Item__c item: items){
                OrderApi__Sales_Order_Line__c temp = new OrderApi__Sales_Order_Line__c();
                temp.OrderApi__Item__c = item.Id;
                temp.OrderApi__Sales_Order__c = salesOrder.Id;
                temp.OrderApi__Quantity__c = 1;
                temp.OrderApi__Price_Override__c = false;
                salesOrderLines.add(temp);
        	}
        }
        
        
        return salesOrderLines;
   
    }
   /* 
    public static void CookieController() {
        Cookie counter = ApexPages.currentPage().getCookies().get('counter');
    
        // If this is the first time the user is accessing the page, 
        // create a new cookie with name 'counter', an initial value of '1', 
        // path 'null', maxAge '-1', and isSecure 'false'. 
        if (counter == null) {
            counter = new Cookie('counter','1',null,-1,false);
        } else {
        // If this isn't the first time the user is accessing the page
        // create a new cookie, incrementing the value of the original count by 1
            Integer count = Integer.valueOf(counter.getValue());
            counter = new Cookie('counter', String.valueOf(count+1),null,-1,false);
        }
    
        // Set the new cookie for the page
        ApexPages.currentPage().setCookies(new Cookie[]{counter});
    }
*/
    
    
    
    
    @AuraEnabled
    public static FileWrapper saveTheFile(Id parentId, String fileName, String base64Data, String contentType, String fileTitle, String idFileToReplace) {
        
        if (idFileToReplace != null) {
            List<ContentDocument> filesToUpdate = [SELECT Id, Title, SharingOption FROM ContentDocument WHERE Id = :idFileToReplace];
            delete filesToUpdate;
        }
        
        base64Data = EncodingUtil.urlDecode(base64Data, 'UTF-8');
        
        ContentVersion newFile = new ContentVersion();
        newFile.FirstPublishLocationId = parentId;
        
        newFile.versionData = EncodingUtil.base64Decode(base64Data);
        newFile.title = fileName;
        
        newFile.pathOnClient = '/' + fileName;
        //  a.ContentType = contentType;
        
        insert newFile;
        
        FileWrapper response = new FileWrapper();
        response.fileId = [Select Id, ContentDocumentId from ContentVersion Where id = :newFile.id].ContentDocumentId;
        response.fileName = newFile.title;
        
        return response;
        
    }
    
    @AuraEnabled
    public static List<FilesWrapper> saveMultipleFiles(String ScheduleA_B_Files) { //add delete logic
        List<ContentVersion> listOfFiles = new List<ContentVersion> ();
        List<FilesWrapper> files = (List<FilesWrapper>) JSON.deserialize(ScheduleA_B_Files, List<FilesWrapper>.class);
        List<Id> filesIdToDelete = new List<Id> ();
        for (FilesWrapper file : files) {
            file.base64Data = EncodingUtil.urlDecode(file.base64Data, 'UTF-8');
            ContentVersion newFile = new ContentVersion();
            newFile.FirstPublishLocationId = file.parentId;
            newFile.versionData = EncodingUtil.base64Decode(file.base64Data);
            newFile.title = file.fileName;
            newFile.pathOnClient = '/' + file.fileName;
            listOfFiles.add(newFile);
            if (file.idFileToReplace != null) {
                filesIdToDelete.add(file.idFileToReplace);
            }
            
        }
        if (!filesIdToDelete.isEmpty()) {
            Set<Id> setOfFilesIdToDelete = new Set<Id> ();
            setOfFilesIdToDelete.addAll(filesIdToDelete);
            List<ContentDocument> filesToUpdate = [SELECT Id, Title, SharingOption FROM ContentDocument WHERE Id = :setOfFilesIdToDelete];
            delete filesToUpdate;
        }
        if (!listOfFiles.isEmpty()) {
            insert listOfFiles;
        }
        Set<Id> idsOfFiles = new Set<id> ();
        for (Integer i = 0; i<listOfFiles.size(); i++) {
            idsOfFiles.add(listOfFiles[i].Id);
            files[i].fileId = listOfFiles[i].Id;
        }
        
        List<ContentVersion> listOfFilesistOfInsertedFiles = [Select Id, ContentDocumentId from ContentVersion Where id IN :idsOfFiles];
        Map<Id, Id> mapOfIdsAndContentVersionId = new Map<Id, Id> ();
        for (ContentVersion insertedFile : listOfFilesistOfInsertedFiles) {
            mapOfIdsAndContentVersionId.put(insertedFile.Id, insertedFile.ContentDocumentId);
        }
        
        for (FilesWrapper file : files) {
            file.fileId = mapOfIdsAndContentVersionId.get(file.fileId);
        }
        
        
        return files;
        // files[0].fileName;//JSON.deserializeUntyped(ScheduleA_B);
    }
    
    @AuraEnabled
    public static Boolean deletePALApplicationFiles(List<Id> filesIdToDelete) {
        
        if (filesIdToDelete != null) {
            Set<Id> setOfFilesIdToDelete = new Set<Id> ();
            setOfFilesIdToDelete.addAll(filesIdToDelete);
            List<ContentDocument> filesToUpdate = [SELECT Id, Title, SharingOption FROM ContentDocument WHERE Id = :setOfFilesIdToDelete];
            delete filesToUpdate;
            return true;
        }
        return false;
        
    }
    @AuraEnabled
    public static ReviewPageWrapper loadReviewInfo() {
        ReviewPageWrapper results = new ReviewPageWrapper();
        List<CPAO_Application__c> currentPALApplication =
            CPAO_ApplicationSelector.getUserApplicationsByRecordTypeNameAndStatus(OPEN, PAL_APPLICATION);
        if (!currentPALApplication.isEmpty()) {
            results.currentPALApplication = currentPALApplication[0];
        } else {
            return null;
        }
        
        User currentUser = LOGGED_USER; // CPAO_UserSelector.getCurrentUser();
        results.userContact = CPAO_ContactSelector.getContactToUpdate(new Set<Id> { currentUser.ContactId }) [0];
        //results.poaItems = CPAO_PlanOfActionSelector.getPOAsByApplicationId(currentAMDCPDApplication[0].Id);
        results.contactEmploymentRecords = CPAO_AccountContactRelationSelector.getRelationshipsByContactIdAndStatus(results.userContact.ID, ACTIVE);
        
        List<CPAO_Account_Contact_Relationship__c> primaryEmployers = CPAO_AccountContactRelationSelector.getPrimaryRelationshipsByContactId(new Set<Id> { results.userContact.Id });
        
        
        return results;
        
    }
    
    
    public class ReviewPageWrapper {
        @AuraEnabled public CPAO_Application__c currentPALApplication { get; set; }
        @AuraEnabled public Contact userContact { get; set; }
        //  @AuraEnabled public ContentDocumentLink  poaItems{get;set;}
        @AuraEnabled public List<CPAO_Account_Contact_Relationship__c> contactEmploymentRecords { get; set; }
    }
    
    public class PaymentPageWrapper {
        @AuraEnabled public Id salesOrderID {get;set;}
        @AuraEnabled public Id userID {get;set;}
        @AuraEnabled public Id orgID {get;set;}
    }
    
    public class FileWrapper {
        @AuraEnabled public Id fileId { get; set; }
        //@AuraEnabled public ContentDocumentLink file{get;set;}
        @AuraEnabled public String fileName { get; set; }
        
    }
    public class FilesWrapper {
        @AuraEnabled public Id parentId { get; set; }
        @AuraEnabled public String fileName { get; set; }
        @AuraEnabled public String base64Data { get; set; }
        @AuraEnabled public String contentType { get; set; }
        @AuraEnabled public String fileTitle { get; set; }
        @AuraEnabled public String idFileToReplace { get; set; }
        @AuraEnabled public Id fileId { get; set; }
        
        
    }
    public class initWrapper {
        @AuraEnabled public CPAO_Application__c currentPALApplication { get; set; }
        @AuraEnabled public String contactStatus {get; set;}
        @AuraEnabled public List<FilesWrapper> ScheduleA_B { get; set; }
        
    }

}