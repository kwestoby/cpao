({
    upsertFMT : function(component,event,handler) {
        var currentCOA=component.get('v.currentCOA');
        var action = component.get("c.upsertFMTApplication");
        var testField = component.get("v.testName");
        var AddressField = component.get("v.Address");
        var City = component.get("v.City");
        var Province = component.get("v.Province");
        var Code = component.get("v.Code");
        var Phone = component.get("v.Phone");
        var Email = component.get("v.Email");
        var Website = component.get("v.Website");
        console.log('testField###'+testField);
        console.log('Address###'+AddressField);
        //alert(component.get('v.currentCOA'));
        action.setParams({ "COAAppToAdd" :currentCOA});
        action.setParams({ "test" :testField});
        action.setParams({ "Address" :AddressField});
        action.setParams({ "City" :City});
        action.setParams({ "Province" :Province});
        action.setParams({ "Code" :Code});
        action.setParams({ "Phone" :Phone});
        action.setParams({ "Email" :Email});
        action.setParams({ "Website" :Website});
        
        action.setCallback(this, function(response){
            var state = response.getState();
            
            if (state === "SUCCESS") {
                console.log('COA added');
                if(component.get('v.shouldSaveAndQuit')){
                    window.location.href='/CPAO_ApplicationDashboard';
                    
                }else{
                    component.set('v.currentCOA',response.getReturnValue());
                    var applicationHasChanged = component.getEvent("applicationHasChanged");
                    applicationHasChanged.setParams({
                        "currentApplication": component.get("v.currentCOA"),
                        "nextButtonClickeable": "true"
                    });
                    applicationHasChanged.fire();
                }
                
            }else{
                console.log('error while adding COA');
                var error= response.getError();
                console.log(error[0]);
            }
        });
        
        $A.enqueueAction(action);
    }
})