public without sharing class CPAO_AccountSelector {
	
	public static List<Account> getAccountsById(Set<Id> ids) {
		return [SELECT Id, Name, BillingStreet, BillingCity, BillingState, BillingPostalCode, BillingCountry,
				        ShippingStreet, ShippingCity, ShippingState, ShippingPostalCode, ShippingCountry,
				        CPAO_LSO__c, CPAO_NSO__c, CPAO_RSO__c 
			    FROM Account
		        WHERE Id IN :ids];
	}

	public static List<Account> getAccountToUpdate(Set<Id> accountIdsToCPAIdVal ) {
		return [SELECT Id, Name, BillingStreet, BillingCity, BillingState, BillingPostalCode, BillingCountry,
				        ShippingStreet, ShippingCity, ShippingState, ShippingPostalCode, ShippingCountry,
				        CPAO_LSO__c, CPAO_NSO__c, CPAO_RSO__c 
			    FROM Account
		        WHERE Id IN :accountIdsToCPAIdVal];
	}

	public static List<Account> getAccountToUpdate(String accountName) {
		return [SELECT Id FROM Account WHERE Name = :accountName];
	}

	public static List<Account> getprimaryContactAccounts(Id contactId) {
		return [SELECT Id, Name, OrderApi__Primary_Contact__c 
				FROM Account 
				WHERE OrderApi__Primary_Contact__c =:contactId ORDER BY Name ASC];
	}

	public static List<Account> getLSOAccounts(Id contactId) {
		return [SELECT Id, Name 
				FROM Account 
				WHERE CPAO_LSO__c =:contactId];
	}

	public static List<Account> getContactSoAccounts(Id contactId ) {
		return [SELECT Id, Name, BillingStreet, BillingCity, BillingState, BillingPostalCode,
        				BillingCountry, CPAO_LSO__c, CPAO_NSO__c, CPAO_RSO__c 
        		FROM Account 
        		WHERE (CPAO_LSO__c = :contactId OR CPAO_NSO__c = :contactId OR CPAO_RSO__c = :contactId)];
	}

	public static List<Account> getContactSoAccountsWithIds(Id contactId, List<Id> accountIds) {
		return [SELECT Id, Name, BillingCity, BillingCountry, ShippingAddress, OrderApi__Primary_Contact__c,
					CPAO_LSO__c, CPAO_NSO__c, CPAO_RSO__c 
        		FROM Account 
        		WHERE (CPAO_LSO__c = :contactId OR CPAO_NSO__c = :contactId OR CPAO_RSO__c = :contactId)
        		AND Id =: accountIds];
	}
}