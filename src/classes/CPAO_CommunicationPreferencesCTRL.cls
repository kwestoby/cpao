public without sharing class CPAO_CommunicationPreferencesCTRL {

	@AuraEnabled
	public static Contact getUserInfo() {
		Id contactId = CPAO_UserSelector.getCurrentUser().ContactId;
		return CPAO_ContactSelector.getContactToUpdate(new Set<Id>{contactId})[0];
	}

	@AuraEnabled
	public static String saveContactInfo(Contact contact) {
		try{
			update contact;
		} catch(Exception e) {
			return e.getMessage();
		}
		return 'Success';
	}
}