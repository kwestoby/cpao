({
	upsertPAL: function(component){
        
        var action = component.get("c.upsertPalApplication");
        action.setParams({ "PALRenewalToAdd" :component.get('v.currentPAL') });
        action.setCallback(this, function(response){
            var state = response.getState();
            if (state === "SUCCESS") {
                alert('Thank you for submitting your PAL renewal application. \n This is a placeholder for payment.\n Application  has status "submitted" now');
                //window.open('CPAO_ApplicationDashboard');
                window.location.href='/CPAO_ApplicationDashboard';
                
            }else{
                console.log('error while submitting Pal');
                var errors=response.getError();
                var message = errors[0];
                console.log(message);
                
            }
        });
        
        $A.enqueueAction(action);
    }
})