/**
* @File Name    :   CPAO_Limited_Application_CTRLTest
* @Description  :   Limited Application Controller Test
* @Date Created :   2018-07-13
* @Author       :   Michaell Reis Gasparini, Deloitte, mreisg@deloitte.com
* @group        :   Class
* @Modification Log:
**************************************************************************************
* Ver       Date        Author              Modification
* 1.0       2018-07-12  Michaell Reis       Created the file/class
**/
@isTest
private class CPAO_Limited_Application_CTRLTest {
	
	@testSetup
	static void dataInit(){

		CPAO_TestFactory.automationControlSetup();
		
		List<Account> accounts = CPAO_TestFactory.createAccounts(1);
		Database.insert(accounts);

		List<Contact> contacts = CPAO_TestFactory.createContacts(accounts[0],1);
		Database.insert(contacts);

		List<CPAO_Application__c> applications =CPAO_TestFactory.createPALApplication(1,contacts[0]);
		Database.insert(applications);

	}

	@isTest static void callCorrect() {
		// Implement test code

		
		CPAO_Limited_Application_CTRL.createReinstatementApplication();

	}
	

	@isTest static void callWrong() {
		// Implement test code

		
		CPAO_Limited_Application_CTRL.createReinstatementApplication();
		try {
			CPAO_Limited_Application_CTRL.createReinstatementApplication();
			} catch(Exception e) {
				System.assert(e.getMessage().length()>0);
			}

		}

	}