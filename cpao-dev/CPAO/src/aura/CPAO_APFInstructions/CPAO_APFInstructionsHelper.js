({
    fireNextEvent : function(component) {
        var nextButtonClick = $A.get("e.c:CPAO_Next_Button_Has_Been_Pressed");
        nextButtonClick.setParams({
            "componentName" : "CPAO_AnnualPractitionerFee",
            "nextStepName" : "Next"});
        nextButtonClick.fire();
    },

    firePrevEvent : function(component) {
        var prevButtonClick = $A.get("e.c:CPAO_Prev_Button_Has_Been_Pressed");
        prevButtonClick.setParams({
            "componentName" : "CPAO_AnnualPractitionerFee",
            "nextStepName" : "Prev"});
        prevButtonClick.fire();
    },
})