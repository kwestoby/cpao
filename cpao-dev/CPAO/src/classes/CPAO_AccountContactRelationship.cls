public without sharing class CPAO_AccountContactRelationship {
    public static String DUPLICATE_IN_DATABASE = 'A duplicate primary employer was found in the database. These records should be fixed before proceeding: ';
    public static String DUPLICATE_IN_NEW_RECORDS = 'A duplicate primary employer was found in the dataset. These records should be fixed before proceeding';
    
    //handles calls from trigger and filters incoming records to determine which code they shoud run through
    public static void filterAfterInsert(List<CPAO_Account_Contact_Relationship__c> relationships) {
        List<CPAO_Account_Contact_Relationship__c> markedAsPrimary = new List<CPAO_Account_Contact_Relationship__c>();
        for(CPAO_Account_Contact_Relationship__c relationship:relationships){
            if(relationship.CPAO_Primary_Employer__c == 'Yes'){
                markedAsPrimary.add(relationship);
            }
        }
        if(!markedAsPrimary.isEmpty()){
            handlePrimaryEmployee(markedAsPrimary, null);
        }
    }

    //handles calls from trigger and filters incoming records to determine which code they shoud run through
    public static void filterAfterUpdates(List<CPAO_Account_Contact_Relationship__c> relationships, 
    Map<Id, CPAO_Account_Contact_Relationship__c> oldMap) {
        
        List<CPAO_Account_Contact_Relationship__c> markedAsPrimary = new List<CPAO_Account_Contact_Relationship__c>();
        List<CPAO_Account_Contact_Relationship__c> unmarkedAsPrimary = new List<CPAO_Account_Contact_Relationship__c>();

        for(CPAO_Account_Contact_Relationship__c rel:relationships){
            if(rel.CPAO_Primary_Employer__c == 'Yes' && rel.CPAO_Primary_Employer__c != oldMap.get(rel.Id).CPAO_Primary_Employer__c){
                markedAsPrimary.add(rel);
            } else if(rel.CPAO_Primary_Employer__c == 'No' && rel.CPAO_Primary_Employer__c != oldMap.get(rel.Id).CPAO_Primary_Employer__c){
                unmarkedAsPrimary.add(rel);
            }
        }
        if(!markedAsPrimary.isEmpty() || !unmarkedAsPrimary.isEmpty()){
            handlePrimaryEmployee(markedAsPrimary, unmarkedAsPrimary);
        }
    }

    //handles relationships wich have been marked as a primary employer, as well as relationships which have been undesignated as primary employer 
    public static void handlePrimaryEmployee(List<CPAO_Account_Contact_Relationship__c> relationships, 
    List<CPAO_Account_Contact_Relationship__c> unmarkedAsPrimaryRels) {

        Map<Id, Contact> contactsToUpdate = new Map<Id, Contact>();
        if(unmarkedAsPrimaryRels != null){
            for(CPAO_Account_Contact_Relationship__c unmarkedRel:unmarkedAsPrimaryRels){
                contactsToUpdate.put(unmarkedRel.CPAO_Contact__c, new Contact(
                    Id = unmarkedRel.CPAO_Contact__c, CPAO_Primary_Employer__c = null, Title = null));
            }
            if(relationships.isEmpty()){
            system.debug('contactsToUpdate.values()'+contactsToUpdate.values());
                update contactsToUpdate.values();
                return;
            }
        }
            
        Set<Id> contactIds = new Set<Id>();
        Map<Id, List<CPAO_Account_Contact_Relationship__c>> contactRels = new Map<Id, List<CPAO_Account_Contact_Relationship__c>>();

        for(CPAO_Account_Contact_Relationship__c rel:relationships){
            if(contactRels.containsKey(rel.CPAO_Contact__c)){
                String finalError = DUPLICATE_IN_DATABASE +contactRels.get(rel.CPAO_Contact__c)[0].Id+ ', '+ rel.Id;
                rel.addError(finalError);
            } else {
                contactRels.put(rel.CPAO_Contact__c, new List<CPAO_Account_Contact_Relationship__c>{rel});
            }
            contactIds.add(rel.CPAO_Contact__c);
        }
        
        List<CPAO_Account_Contact_Relationship__c> soqlContactRels = CPAO_AccountContactRelationSelector.getPrimaryRelationshipsByContactId(contactIds);
        
        for(CPAO_Account_Contact_Relationship__c rel:soqlContactRels){
            if(contactRels.containsKey(rel.CPAO_Contact__c)){
                if(contactRels.get(rel.CPAO_Contact__c)[0].Id != rel.Id){
                    contactRels.get(rel.CPAO_Contact__c)[0].addError(DUPLICATE_IN_NEW_RECORDS);
                }
                
            } else {
                contactRels.put(rel.CPAO_Contact__c, new List<CPAO_Account_Contact_Relationship__c>{rel});
            }
        }
        for(CPAO_Account_Contact_Relationship__c relationship:relationships){
            contactsToUpdate.put(relationship.CPAO_Contact__c, new Contact(
                Id = relationship.CPAO_Contact__c, 
                CPAO_Primary_Employer__c = relationship.CPAO_Account__c,
                Title = relationship.CPAO_Text__c));
        }
        if(!contactsToUpdate.values().isEmpty()){
            update contactsToUpdate.values();
        }

    }

    public class CPAO_AccountContactRelationship_Exception extends Exception {}

}