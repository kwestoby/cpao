@isTest
public with sharing class CPAO_EmployerInfoControllerTest {

    public static List<Account> accounts;
    public static List<Contact> contacts;
    public static User communityUser;

    public static void dataInitialize(Integer contactAmount) {
        accounts = CPAO_TestFactory.createAccounts(2);
        accounts[1].Name = 'Retirement';
        insert accounts;
        contacts = CPAO_TestFactory.createContacts(accounts[0], contactAmount);
        insert contacts;
        communityUser = CPAO_TestFactory.createCommunityUser(contacts[1].Id);
        insert communityUser;
    }

    static CPAO_Account_Contact_Relationship__c simpleRelationshipSetup(CPAO_Account_Contact_Relationship__c rel){
        rel.CPAO_Primary_Employer__c = 'Yes';
        rel.CPAO_Text__c = 'TestClass Function';
        rel.CPAO_Position_Category__c = 'COO';
        rel.CPAO_Business_Unit__c = 'Internal Audit';
        rel.CPAO_Contact__c = contacts[0].ID;
        return rel;
    }

    @isTest
    public static void basicTest() {
        dataInitialize(2);
        System.RunAs(communityUser){
            CPAO_EmployerInfoController.getRelationships();
            CPAO_EmployerInfoController.getBusinessUnitOptions();
            CPAO_EmployerInfoController.getPositionCategoryOptions();
            CPAO_Account_Contact_Relationship__c rel = simpleRelationshipSetup(
                CPAO_TestFactory.createAccountContactRelationships(accounts[0], contacts[0], 1)[0]);

            CPAO_EmployerInfoController.SaveAccountContactRelationships(rel);
            CPAO_EmployerInfoController.SaveAccountContactRelationships(rel);
            CPAO_EmployerInfoController.getAccountLookup(accounts[0].Name);
            CPAO_EmployerInfoController.isPrimaryEmployer(contacts[0].Id);
            //CPAO_EmployerInfoController.retireUnemployEmployee(accounts[1].Name);
            CPAO_EmployerInfoController.retireUnemployEmployee('Unemployed');
            CPAO_EmployerInfoController.changePrimaryEmployer(rel.Id,true);
            //rel.CPAO_Primary_Employer__c = null;
            //CPAO_EmployerInfoController.updateEmployerAndChangePrimary(rel);
            CPAO_EmployerInfoController.updateContact(contacts[0]);
            CPAO_EmployerInfoController.NewEmploymentDoInit();
            CPAO_EmployerInfoController.NewEmployerCreate(JSON.serialize(accounts[0]));
            CPAO_EmployerInfoController.componentInit();
        }           
    }
}