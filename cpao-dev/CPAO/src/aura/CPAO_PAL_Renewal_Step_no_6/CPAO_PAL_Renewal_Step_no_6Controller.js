({
   doInit: function(component, event, helper) {
        
        var supervisoryOptions=component.get('v.currentPAL.CPAO_Supervisory__c');
        if(supervisoryOptions!=undefined){
            var newSupervisoryValues=supervisoryOptions.split(';')
            for(i=0;i<supervisoryOptions.length;i++){
                //alert(supervisoryOptions[i]);
                newSupervisoryValues.push(supervisoryOptions[i]);
            }
        component.set('v.supervisoryValue',newSupervisoryValues);
        }
       
    },
	 clickOnNextButton : function(component,event,helper){
        var componentNameInEvent=event.getParam("componentName");
        if(componentNameInEvent==component.get('v.nameOfThisComponent')){
            if(event.getParam("nextStepName")=='Exit'){ 
                component.set('v.shouldSaveAndQuit',true);
            }

            if(component.get('v.needToMeetProfessionalRequirements')==true){
                var validForm= component.find('PALForm').reduce(function (validSoFar, inputCmp) {
                    var professionalExperienceRequirement = component.get('v.currentPAL.CPAO_Professional_Experience_Requirement__c');
                    var specialConsideration = component.get('v.currentPAL.CPAO_Prof_Experience_Spec_Consideration__c');   
                    var supervisoryValue = component.get('v.supervisoryValue');
                    var otherFieldsValid=false;
                    var startDate = component.get('v.currentPAL.CPAO_Exceptional_Circumstances_SD__c');
                    var endDate = component.get('v.currentPAL.CPAO_Exceptional_Circumstances_End_Date__c');
                    var oldUploadId=component.get('v.currentPAL.CPAO_Special_Consideration_File_Id__c');

     
                    // Displays error messages for invalid fields
                    if(professionalExperienceRequirement==undefined){
                        component.set('v.showProfessionalExperienceError',true);
                    }else{//CHECK END DATE > STARTD DATE
                         component.set('v.showProfessionalExperienceError',false);
                        if(professionalExperienceRequirement==component.get('v.ProfessionalRequirementsNotMet')){
                                 //VALIDATE OTHER DEPENDENT FIELDS
                            if(specialConsideration==undefined){
                                 component.set('v.showSpecialConsiderationError',true);
                                 otherFieldsValid=false
                            }else{
                                 component.set('v.showSpecialConsiderationError',false);
                                 otherFieldsValid=true;
                            }

                            if(specialConsideration==component.get('v.discretionSupervisory') && supervisoryValue.length== 0){
                                component.set('v.showSupervisoryError',true);
                                otherFieldsValid=false;
                            }else{
                                component.set('v.showSupervisoryError',false);
                                otherFieldsValid=true;
                            }

                            if(specialConsideration==component.get('v.doNotConsider') && component.get('v.fileInput')==null && oldUploadId==null){
                                component.set('v.showFileError',true);
                                otherFieldsValid=false;
                            }else{
                                component.set('v.showFileError',false);
                                otherFieldsValid=true;
                            }

                            if(specialConsideration==component.get('v.discretion2Years') && startDate>=endDate){
                                component.set('v.showDateError',true);
                                otherFieldsValid=false;
                            }else{
                                component.set('v.showDateError',false);
                                otherFieldsValid=true;
                            }

                        } 
                        
                    }
                    otherFieldsValid= specialConsideration!=undefined && (
                            (specialConsideration == component.get('v.discretion2Years')&& startDate<endDate)||
                            (specialConsideration == component.get('v.discretionSupervisory') && supervisoryValue.length != 0) ||
                            (specialConsideration == component.get('v.doNotConsider')  && (component.get('v.fileInput')!=null || oldUploadId!=null))
                            );
                  
                    inputCmp.showHelpMessageIfInvalid();
                    return validSoFar && inputCmp.get('v.validity').valid && (professionalExperienceRequirement==component.get('v.ProfessionalRequirementsMet') || (professionalExperienceRequirement==component.get('v.ProfessionalRequirementsNotMet') && otherFieldsValid));
                }, true);
            }
            if(validForm || component.get('v.needToMeetProfessionalRequirements')==false){

                if(component.get('v.needToMeetProfessionalRequirements')==false){
                    component.set('v.currentPAL.CPAO_Professional_Experience_Requirement__c',component.get('v.lessThan5Years'));
                }else if(component.get('v.currentPAL.CPAO_Prof_Experience_Spec_Consideration__c')==component.get('v.discretionSupervisory')){
                    var supervisory=component.get('v.supervisoryValue');
                    component.set('v.currentPAL.CPAO_Supervisory__c',supervisory);
                    component.set('v.currentPAL.CPAO_Exceptional_Circumstances_SD__c',null);
                    component.set('v.currentPAL.CPAO_Exceptional_Circumstances_End_Date__c',null);
                }else if(component.get('v.currentPAL.CPAO_Prof_Experience_Spec_Consideration__c')==component.get('v.discretion2Years')){
                    var startDate = component.get('v.currentPAL.CPAO_Exceptional_Circumstances_SD__c');
                    var endate = component.get('v.currentPAL.CPAO_Exceptional_Circumstances_End_Date__c');
                    var formattedDate1=startDate.substring(0,10);
                    var formattedDate2=endate.substring(0,10);
                    component.set('v.currentPAL.CPAO_Exceptional_Circumstances_SD__c',formattedDate1);
                    component.set('v.currentPAL.CPAO_Exceptional_Circumstances_End_Date__c',formattedDate2);
                    component.set('v.currentPAL.CPAO_Supervisory__c',null);
                }else if(component.get('v.currentPAL.CPAO_Prof_Experience_Spec_Consideration__c')==component.get('v.doNotConsider')){
                    component.set('v.currentPAL.CPAO_Supervisory__c',null);
                    component.set('v.currentPAL.CPAO_Exceptional_Circumstances_SD__c',null);
                    component.set('v.currentPAL.CPAO_Exceptional_Circumstances_End_Date__c',null);
                }
                
                var showSpinnerEvent = component.getEvent("showSpinner");
                showSpinnerEvent.fire();
                helper.upload(component,component.get('v.fileInput'), component.get('v.fileContents')) ;

                //helper.upsertPAL(component);
            }

        }  	   
        
    },
    handleFilesChange: function (component, event,helper) {
        var fileInput = event.getSource().get("v.files")[0];
        console.log('file input'+fileInput);
        console.log(fileInput.size);
        component.set('v.fileInput',fileInput);
        component.set('v.fileName',fileInput.name);
        component.set('v.SpecialConsiderationName',fileInput.name);
        // component.set('v.NotYetUploaded','true');
        
        //alert( component.get('v.fileName'));
        component.set('v.fileType',fileInput.type);
        
        var fr = new FileReader();
        var self = this;
        
        fr.onload = function() {
            var fileContents = fr.result;
            var base64Mark = 'base64,';
            var dataStart = fileContents.indexOf(base64Mark) + base64Mark.length;
            fileContents = fileContents.substring(dataStart);
            component.set('v.fileContents',fileContents);
            
            // helper.upload(component,fileInput, fileContents);
        };
        
        fr.readAsDataURL(fileInput);
    }
})