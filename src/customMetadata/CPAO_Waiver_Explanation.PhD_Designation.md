<?xml version="1.0" encoding="UTF-8"?>
<CustomMetadata xmlns="http://soap.sforce.com/2006/04/metadata" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xmlns:xsd="http://www.w3.org/2001/XMLSchema">
    <label>PhD Designation</label>
    <protected>false</protected>
    <values>
        <field>AMD_or_CPD__c</field>
        <value xsi:type="xsd:string">AMD</value>
    </values>
    <values>
        <field>CPAO_Explanation__c</field>
        <value xsi:type="xsd:string">Members enrolled I a full-time accounting PhD program at a recognized university may apply for a fee payment reduction. Subject to meeting the criteria each year, there are no limits to how many waivers may granted to a member.
Your fee payment reduction request is subject to audit by CPA Ontario.  You may be contacted by CPA Ontario to provide a letter from the university confirming full-time enrolment in a program for at least seven months in a single, 12-month period.</value>
    </values>
    <values>
        <field>CPAO_Percent_Off__c</field>
        <value xsi:type="xsd:double">100.0</value>
    </values>
    <values>
        <field>Explanation_Third_Year__c</field>
        <value xsi:nil="true"/>
    </values>
    <values>
        <field>Full_Name__c</field>
        <value xsi:type="xsd:string">PhD Designation</value>
    </values>
</CustomMetadata>
