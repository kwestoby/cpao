<?xml version="1.0" encoding="UTF-8"?>
<CustomMetadata xmlns="http://soap.sforce.com/2006/04/metadata" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xmlns:xsd="http://www.w3.org/2001/XMLSchema">
    <label>Medical leave for 6 months or more</label>
    <protected>false</protected>
    <values>
        <field>AMD_or_CPD__c</field>
        <value xsi:type="xsd:string">CPD</value>
    </values>
    <values>
        <field>CPAO_Explanation__c</field>
        <value xsi:type="xsd:string">You did not work and were not involved in any activity in which it was reasonable to believe that another party relied upon my skills as a member for a total of six months or more between January 1 to December 31. 
The CPD exemption is subject to audit by CPA Ontario.  You may be contacted by CPA Ontario to provide a letter from a physician confirming the diagnosis and prognosis, or other such confirmation that you were not employed due to medical reasons during the time period (e.g., letter from employer or disability claim form).</value>
    </values>
    <values>
        <field>CPAO_Percent_Off__c</field>
        <value xsi:nil="true"/>
    </values>
    <values>
        <field>Explanation_Third_Year__c</field>
        <value xsi:type="xsd:string">You did not work and were not involved in any activity in which it was reasonable to believe that another party relied upon my skills as a member for a total of six months or more between January 1 to December 31. 
The CPD exemption is subject to audit by CPA Ontario.  You may be contacted by CPA Ontario to provide a letter from a physician confirming the diagnosis and prognosis, or other such confirmation that you were not employed due to medical reasons during the time period (e.g., letter from employer or disability claim form).</value>
    </values>
    <values>
        <field>Full_Name__c</field>
        <value xsi:type="xsd:string">Medical leave for 6 months or more</value>
    </values>
</CustomMetadata>
