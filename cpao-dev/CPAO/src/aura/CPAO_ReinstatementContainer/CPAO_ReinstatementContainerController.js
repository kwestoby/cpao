({
	doInit : function(component, event, helper) {
		var apexAction = component.get('c.getReinstatementInfo');
        apexAction.setCallback(this, function(response) {
            var state = response.getState();
            console.log('state');
            console.log(state);
            if(state === "SUCCESS"){
                var result = response.getReturnValue();
                component.set("v.application", result.currentReinstatementApp);
                component.set("v.communityHomePage", result.communityURL);
                component.set('v.listOfInfractions',result.currentInfractions);
                console.log(result.currentInfractions);
            }
        });
        $A.enqueueAction(apexAction);
        
        
        
	},

	handleNext : function(component,event,helper){
        var getselectedStep = component.get("v.selectedStep");

        /*if(getselectedStep == "step1"){
            component.set("v.selectedStep", "step2");
            component.set("v.canBack", true);
        } else */if(getselectedStep == "step2"){
            var breachPageSave = component.find("breachPage");
            if(breachPageSave.saveMethod()){
                component.set("v.selectedStep", "step3")
            }
            
        
            
        } else if(getselectedStep == "step4"){
            var reviewLoadMethod = component.find('reviewPage');
            reviewLoadMethod.loadMethod();
            component.set("v.selectedStep", "step5");
        } else {
            helper.showSpinner(component);
            if(getselectedStep == "step3"){
                getselectedStep = "step2";
            }
            var nextButtonClick = $A.get("e.c:CPAO_Next_Button_Has_Been_Pressed");
            nextButtonClick.setParams({
                "componentName" : getselectedStep,
                "nextStepName" : "Next"});
            nextButtonClick.fire();            
        }
    },

    saveAndExit : function(component,event,helper){
        var getselectedStep = component.get("v.selectedStep");
        if(getselectedStep == "step1"){
            var homepageURL = component.get("v.communityHomePage");
            window.location.href = homepageURL;
            return;
        } else if(getselectedStep == "step2"){
            var breachPageSave = component.find("breachPage");
            
            breachPageSave.exitMethod()
            var homepageURL = component.get("v.communityHomePage");
            window.location.href = homepageURL;
            
            return;
        }else {
            helper.showSpinner(component);
            var nextButtonClick = $A.get("e.c:CPAO_Next_Button_Has_Been_Pressed");
            nextButtonClick.setParams({
                "componentName" : getselectedStep,
                "nextStepName" : "Exit"});
            nextButtonClick.fire();
        }
    },

    applyResponseFromChildComponent : function(component,event,helper){
        var getselectedStep = component.get("v.selectedStep");
        console.log('hey dan, event caught.');
        console.log(getselectedStep);

        helper.hideSpinner(component);
        var isNextOk = event.getParam("nextButtonClickeable");
        if(isNextOk == false){
            var modalBody = event.getParam("modalBody");
            var modalHeader = event.getParam("modalHeader");
            if(modalBody == undefined || modalBody == ''){
                return;
            }
            component.set("v.isOpen", true);
            component.set("v.modalBody", modalBody);
            component.set("v.modalHeader", modalHeader);
            return;
        }
        var action = event.getParam("action");
        if(action == "Exit"){
            var homepageURL = component.get("v.communityHomePage");
            window.location.href = homepageURL;
            return;
        }
        
        if(getselectedStep == "step2"){
            component.set("v.selectedStep", "step3");
        } else if(getselectedStep == "step3"){
            component.set("v.selectedStep", "step4");
        } else if(getselectedStep == "step4"){
            //var cpdLoadMethod = component.find('cpdPage');
            //cpdLoadMethod.loadMethod();
            
        } else if(getselectedStep == "step5"){
            console.log("dunzo");
            //this is temporary to complete US, this should be after the payment page.
            var homepageURL = component.get("v.communityHomePage");
            window.location.href = homepageURL;
 			
        }
    },

    handlePrev : function(component,event,helper){
        var getselectedStep = component.get("v.selectedStep");
        if(getselectedStep == "step2"){
            component.set("v.selectedStep", "step1");
            component.set("v.canBack", false);
        } else if(getselectedStep == "step3"){
            component.set("v.selectedStep", "step2");
        } else if(getselectedStep == "step4"){
            component.set("v.selectedStep", "step3");
        }
        
    },
    closeModel: function(component, event, helper) {
      	component.set("v.isOpen", false);
        
        var getselectedStep = component.get("v.selectedStep");
        if(getselectedStep == "step1"){
         	component.set("v.selectedStep", "step2");
            component.set("v.canBack", true);
        }
      
    },
})