<?xml version="1.0" encoding="UTF-8"?>
<CustomMetadata xmlns="http://soap.sforce.com/2006/04/metadata" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xmlns:xsd="http://www.w3.org/2001/XMLSchema">
    <label>Offline</label>
    <protected>false</protected>
    <values>
        <field>OrderApi__Apex_Class__c</field>
        <value xsi:type="xsd:string">OrderApi__CustomPaymentTypeTemplate</value>
    </values>
    <values>
        <field>OrderApi__Auto_Post_Sales_Order__c</field>
        <value xsi:type="xsd:boolean">false</value>
    </values>
    <values>
        <field>OrderApi__Available_for_Recurring_Payments__c</field>
        <value xsi:type="xsd:boolean">false</value>
    </values>
    <values>
        <field>OrderApi__Is_Enabled__c</field>
        <value xsi:type="xsd:boolean">true</value>
    </values>
    <values>
        <field>OrderApi__Lightning_Component_for_Checkout__c</field>
        <value xsi:type="xsd:string">CPAO_OfflinePaymentType</value>
    </values>
    <values>
        <field>OrderApi__Lightning_Component_for_Payment_Method__c</field>
        <value xsi:type="xsd:string">CPAO_OfflinePaymentMethod</value>
    </values>
</CustomMetadata>
