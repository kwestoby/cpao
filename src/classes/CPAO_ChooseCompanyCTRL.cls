public with sharing class CPAO_ChooseCompanyCTRL {
	public static String NOT_A_COMMUNITY_USER = 
			'You are not a community user, this component only works for community users';
	public static String SUCCESS = 'Success';

	@AuraEnabled
	public static String getAccounts() {
		ResultWrapper response = new ResultWrapper();
		User loggedUser = CPAO_UserSelector.getCurrentUser();
		if(loggedUser.ContactId == null){
			response.resultMessage = NOT_A_COMMUNITY_USER;
			return JSON.serialize(response);
		}
		response.accountsLSO = CPAO_AccountSelector.getLSOAccounts(loggedUser.ContactId);
		if(loggedUser.Contact.CPAO_Currently_acting_as__c == null && !response.accountsLSO.isEmpty()){
			update new Contact(Id = loggedUser.ContactId, CPAO_Currently_acting_as__c = response.accountsLSO[0].Id);
		}
		response.resultMessage = SUCCESS;
		return JSON.serialize(response);
	}

	@AuraEnabled
	public static String updateLSOActingAs(Id accountId, Id contactId) {
		ResultWrapper response = new ResultWrapper();
		try{
			update new Contact(Id = contactId, CPAO_Currently_acting_as__c = accountId);
		} catch(Exception e){
			response.resultMessage = e.getMessage();
			return JSON.serialize(response);
		}
		
		response.resultMessage = SUCCESS;
		return JSON.serialize(response);
	}

	public class ResultWrapper{

        @AuraEnabled public List<Account> accountsLSO{get;set;}
        @AuraEnabled public String resultMessage{get;set;}

    }
}