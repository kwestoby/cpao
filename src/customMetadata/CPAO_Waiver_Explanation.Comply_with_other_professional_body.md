<?xml version="1.0" encoding="UTF-8"?>
<CustomMetadata xmlns="http://soap.sforce.com/2006/04/metadata" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xmlns:xsd="http://www.w3.org/2001/XMLSchema">
    <label>Comply with other professional body</label>
    <protected>false</protected>
    <values>
        <field>AMD_or_CPD__c</field>
        <value xsi:type="xsd:string">CPD</value>
    </values>
    <values>
        <field>CPAO_Explanation__c</field>
        <value xsi:type="xsd:string">You comply with the annual CPD requirements as you hold membership in another provincial accounting body, or you are not residing in Canada or Bermuda and you are a member of a recognized professional accounting body, and have met the professional development requirements of that body.</value>
    </values>
    <values>
        <field>CPAO_Percent_Off__c</field>
        <value xsi:nil="true"/>
    </values>
    <values>
        <field>Explanation_Third_Year__c</field>
        <value xsi:type="xsd:string">You comply with the annual CPD requirements as you hold membership in another provincial accounting body, or you are not residing in Canada or Bermuda and you are a member of a recognized professional accounting body, and have met the professional development requirements of that body.</value>
    </values>
    <values>
        <field>Full_Name__c</field>
        <value xsi:type="xsd:string">Comply with other professional body requirements</value>
    </values>
</CustomMetadata>
