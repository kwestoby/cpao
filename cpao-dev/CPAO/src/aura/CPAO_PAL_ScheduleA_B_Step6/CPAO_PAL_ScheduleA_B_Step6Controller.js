({
    schedA_Redirect : function(component, event, helper) {
        window.open('https://media.cpaontario.ca/cpa-members/pdfs/Form-9-1A.pdf','_blank');
        
    },
    schedB_Redirect : function(component, event, helper) {
        window.open('https://media.cpaontario.ca/cpa-members/pdfs/Form-9-1A.pdf','_blank');
    },
    schedC_Redirect : function(component, event, helper) {
        window.open('https://media.cpaontario.ca/cpa-members/pdfs/Form-9-1A.pdf','_blank');
        
    },
    
    clickOnNextButton: function (component, event, helper) {
      
        var componentNameInEvent=event.getParam("componentName");
        if(componentNameInEvent==component.get('v.nameOfThisComponent')){
            if(event.getParam("nextStepName")=='Exit'){ 
                component.set('v.shouldSaveAndQuit',true);
            }
            var validForm = component.find('PALForm').reduce(function (validSoFar, inputCmp) {
                var uploadName=component.get('v.currentPAL.CPAO_Schedule_A_File_Name__c');
                var oldUploadId=component.get('v.currentPAL.CPAO_Schedule_A_File_Id__c');
                var oldUploadId_B=component.get('v.currentPAL.CPAO_Schedule_B_File_Id__c');
                var oldUploadId_C=component.get('v.currentPAL.CPAO_Schedule_C_File_Id__c');
                var currentUpload=component.get('v.scheduleAFileName');
                var emptyScheduleA=false;
                var emptyScheduleC=false;
                var validEntries=false;
                
                if(component.get('v.currentPAL.CPAO_Basis_Of_Application__c')==component.get('v.activeLicenceWithOtherBody')){
                    //alert('here')
                    if(component.get('v.fileInput_B')==null &&oldUploadId_B==null){
                        
                       component.set('v.schedule_B_required',true);
                    }else{
                        component.set('v.schedule_B_required',false);
                        validEntries=true;
                    }
                    
                }else{
                    
                    
                    if(component.get('v.fileInput_A')==null &&oldUploadId==null){
                        component.set('v.schedule_A_required',true);
                        emptyScheduleA=true;
                    }else{
                        component.set('v.schedule_A_required',false);
                        
                    }
                    var postDesignationCompleted= component.get('v.currentPAL.CPAO_completedPostDesignationPAProgram__c');//component.get('v.postDesignationCompleted');
                    if (postDesignationCompleted==undefined){
                        component.set('v.postDesignationCompleted_required',true);
                    }else{
                        component.set('v.postDesignationCompleted_required',false);
                        
                    }
                    
                    if(postDesignationCompleted=='Yes' && component.get('v.fileInput_C')==null &&oldUploadId_C==null ){
                        component.set('v.schedule_C_required',true);
                        emptyScheduleC=true;
                    }else{
                        component.set('v.schedule_C_required',false);
                    }
                    validEntries=postDesignationCompleted!=undefined && !emptyScheduleA && !emptyScheduleC;
                    
                }
                return validSoFar && inputCmp.get('v.validity').valid &&validEntries}, true);
            
            if(validForm){
                component.set('v.postDesignationCompleted_required',false);
                component.set('v.schedule_A_required',false);
                component.set('v.schedule_B_required',false);
                component.set('v.schedule_C_required',false);
                //var postDesignationCompleted_req= component.find('postDesignationCompleted_req');
                
                // 	alert(postDesignationCompleted);
                //helper.upsertPAL(component);
                var showSpinnerEvent = component.getEvent("showSpinner");
                showSpinnerEvent.fire();	
                helper.upload(component)//,component.get('v.fileInput_A'), component.get('v.fileContents_A'),false) ;
                
                
            }
        }
    },
    handleFilesChange_ORIGINAL: function (component, event,helper) {
        var fileInput = event.getSource().get("v.files")[0];
        var schedule_A_req=component.find('schedule_A_req');
        
        console.log('file input'+fileInput);
        console.log(fileInput.size);
        component.set('v.fileInput_A',fileInput);
        component.set('v.fileName_A',fileInput.name);
        component.set('v.scheduleAFileName',fileInput.name);
        // component.set('v.NotYetUploaded','true');
        
        //alert( component.get('v.fileName'));
        component.set('v.fileType_A',fileInput.type);
        
        var fr = new FileReader();
        var self = this;
        
        fr.onload = function() {
            var fileContents = fr.result;
            var base64Mark = 'base64,';
            var dataStart = fileContents.indexOf(base64Mark) + base64Mark.length;
            fileContents = fileContents.substring(dataStart);
            component.set('v.fileContents_A',fileContents);
            
            // helper.upload(component,fileInput, fileContents);
        };
        
        fr.readAsDataURL(fileInput);
        //  $A.util.removeClass(schedule_A_req, 'slds-show');
        
    },
    handleFilesChange_A: function (component, event,helper) {
        helper.handleFilesChange(component,event,'schedule_A_req','v.fileInput_A','v.fileName_A','v.scheduleAFileName','v.fileType_A','v.fileContents_A');
        //this.handleFilesChange(component,event,'schedule_A_req','v.fileInput_A','v.fileName_A','v.scheduleAFileName','v.fileType_A','v.fileContents_A');
    },
    handleFilesChange_B: function (component, event,helper) {
        component.set('v.currentPAL.completedPostDesignationPAProgram',null);
        helper.handleFilesChange(component,event,'schedule_B_req','v.fileInput_B','v.fileName_B','v.scheduleBFileName','v.fileType_B','v.fileContents_B');
        //this.handleFilesChange(component,event,'schedule_A_req','v.fileInput_A','v.fileName_A','v.scheduleAFileName','v.fileType_A','v.fileContents_A');
    },
    handleFilesChange_C: function (component, event,helper) {
        helper.handleFilesChange(component,event,'schedule_C_req','v.fileInput_C','v.fileName_C','v.scheduleCFileName','v.fileType_C','v.fileContents_C');
    }
    
    /*,
    clearFile: function (component, event,helper) {
      component.set('v.currentCOA.CPAO_COA_Declaration_File_Name__c',null);
      component.set('v.currentCOA.CPAO_COA_Declaration_File_Id__c',null);
            component.set('v.coaDeclarationFileName',null);

            helper.upload(component,null,null,true) ;


    }*/
    
})