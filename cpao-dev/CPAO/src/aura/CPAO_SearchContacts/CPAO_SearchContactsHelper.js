({
    searchHelper: function(component, event) {
        var action = component.get("c.searchmethod");
        action.setParams({
            'searchKeyword': component.get("v.searchKeyword")         
        }); 
        action.setCallback(this, function(response) {
            var state = response.getState();
            if (state === "SUCCESS") {
                var storeResponse = response.getReturnValue();
                // if storeResponse size is 0 ,display no record found message on screen.
                if (storeResponse.length == 0) {
                    component.set("v.Message", true);
                } else {
                    component.set("v.Message", false);
                
                }
                var wrappers=new Array();
                for (var idx=0; idx<storeResponse.length; idx++) {
                   var wrapper = { 'con' : storeResponse[idx],
                   'selected' : false
                };
                    
                wrappers.push(wrapper);
            	} 
                component.set('v.wrappers', wrappers);
                
                var nextButtonClick = $A.get("e.c:CPAORefreshedContactResult");
        			nextButtonClick.setParams({
        			"Final_wrappers" : component.get('v.wrappers')
        		} );
        		nextButtonClick.fire();
                //alert('fired button click');
                
                
            } 
            
        });
        $A.enqueueAction(action);		
     },
     /*
     AddtotheContactList: function(component, event) {
        
     	var nextButtonClick = $A.get("e.c:CPAORefreshedContactResult");
      		nextButtonClick.setParams({
        		"Final_wrappers" : component.get('v.wrappers')
        } );
        	
        nextButtonClick.fire();
        alert('fired button click');
   
       // $A.enqueueAction(action);		
     },
    */
    addSelectedCheckboxes: function(cmp,event){
    	//var masterCheckboxes = cmp.find("masterCheckbox");
        
        var childCheckboxes = cmp.find("DependentCheckbox");

        var oldWrapper = cmp.get('v.wrappers');

        //var val = masterCheckboxes.get("v.value");

  		if (!Array.isArray(childCheckboxes)) {
   			childCheckboxes = [childCheckboxes];
  		} 
  		for (var i = 0; i < childCheckboxes.length; i++) {
        	var nextButtonClick = $A.get("e.c:CPAORefreshedContactResult");
      		nextButtonClick.setParams({
        	"Final_wrappers" : cmp.get('v.wrappers')
        	} );
        	
        	nextButtonClick.fire();
        	//alert('fired button click');
            	
  		}
        
	 }
    
})