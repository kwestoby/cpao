public class CPAO_ReceiptService {

	private static final String initialRefundApprovalProcessName = 'First_Approver_Refund_Processes';

	public static void sendToRefundQueueAndApproval(List<OrderApi__Receipt__c> refunds) {
		List<CPAO_Department__mdt> departmentMDTs = [SELECT MasterLabel, Queue__c FROM CPAO_Department__mdt];
		Map<String, String> itemQueueMap = new Map<String, String> ();
		for (CPAO_Department__mdt mdt : departmentMDTs) {
			itemQueueMap.put(mdt.MasterLabel, mdt.Queue__c);
		}
		List<Approval.ProcessSubmitRequest> approvalRequests = new List<Approval.ProcessSubmitRequest> ();
		for (OrderApi__Receipt__c refund : refunds) {
			if (itemQueueMap.containsKey(refund.CPAO_Item_Name__c) && CPAO_QueueUtils.queueMap.containsKey(itemQueueMap.get(refund.CPAO_Item_Name__c))) {
				Group queue = CPAO_QueueUtils.queueMap.get(itemQueueMap.get(refund.CPAO_Item_Name__c));
				refund.CPAO_Initial_Approving_Queue_Name__c = queue.Name;
				Approval.ProcessSubmitRequest request = new Approval.ProcessSubmitRequest();
				request.setComments('Submitting to first approver');
				request.setObjectId(refund.Id);
				request.setNextApproverIds(new List<Id> { queue.Id });
				request.setProcessDefinitionNameOrId(initialRefundApprovalProcessName);
				request.setSkipEntryCriteria(false);
				approvalRequests.add(request);
			} else {
				refund.addError('Queue not assigned for item to refund');
			}
		}
		List<Approval.ProcessResult> approvalSubmitResults = Approval.process(approvalRequests, true);
	}
}