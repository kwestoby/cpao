trigger CPAO_ReceiptTrigger on OrderApi__Receipt__c(before update) {
	if (!CPAO_Utilities.automation.OrderApi_Receipt_c__c) {
		return;
	}
	if (Trigger.isBefore && Trigger.isUpdate) {
		CPAO_ReceiptTriggerHandler.beforeUpdateFilter(Trigger.new, Trigger.oldMap);
	}
}