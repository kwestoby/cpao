public without sharing class CPAO_FontevaObjectSelector {   
	public static final String LATE_FEE_CLASS = 'Late Fee Class';

	public static List<OrderApi__Item_Subscription_Plan__c> getSubscriptionPlanItems(String plan) {
		return [SELECT Id, OrderApi__Item__r.Id, OrderApi__Subscription_Plan__c, OrderApi__Item__r.Name
				FROM OrderApi__Item_Subscription_Plan__c WHERE OrderApi__Subscription_Plan__r.Name = :plan];
	}

	public static List<OrderApi__Subscription_Plan__c> getSubscriptionPlanInfo(String plan) {
		return [SELECT Id,Name
				FROM OrderApi__Subscription_Plan__c WHERE Name = :plan];
	}

	

		

	public static List<OrderApi__Subscription__c> getContactSubscription(String plan, Id contactId) {
		return [SELECT Id, OrderApi__Subscription_Plan__c, OrderApi__Contact__c 
				FROM OrderApi__Subscription__c 
				WHERE OrderApi__Subscription_Plan__r.Name = :plan AND OrderApi__Contact__c = :contactId];
	}

	public static List<OrderApi__Subscription__c> getContactsSubscriptions(String plan, Set<Id> contactIds) {
		return [SELECT Id, OrderApi__Subscription_Plan__c, OrderApi__Contact__c 
				FROM OrderApi__Subscription__c 
				WHERE OrderApi__Subscription_Plan__r.Name = :plan AND OrderApi__Contact__c IN :contactIds];
	}

	public static List<OrderApi__Sales_Order_Line__c> getSalesOrderTaxItems(Id salesOrderId) {
		return [SELECT Id
				FROM OrderApi__Sales_Order_Line__c 
				WHERE OrderApi__Sales_Order__c = :salesOrderId AND OrderApi__Is_Tax__c = true];
	}

	public static List<OrderApi__Sales_Order__c> getSalesOrdersWithSpecifiedAppRecordTypes(List<OrderApi__Sales_Order__c> salesOrders, String recordTypeName) {
		return [SELECT Id, CPAO_Application__c, OrderApi__Contact__c
				FROM OrderApi__Sales_Order__c 
				WHERE CPAO_Application__r.RecordType.Name = :recordTypeName AND Id IN :salesOrders];
	}
	public static List<OrderApi__Sales_Order__c> getSalesOrderWithApplication(Id applicationId) {
		return [SELECT Id
				FROM OrderApi__Sales_Order__c 
				WHERE CPAO_Application__c = :applicationId];
	}

	public static List<OrderApi__Item_Class__c> getPayTypeItemClass(String payType) {
		return [SELECT Id, Name
				FROM OrderApi__Item_Class__c 
				WHERE Name LIKE: payType];
	}

	public static List<OrderApi__Item__c> getLateFeeItems() {
		return [SELECT Id, Name
				FROM OrderApi__Item__c 
				WHERE OrderApi__Item_Class__r.Name = :LATE_FEE_CLASS];
	}

	public static List<OrderApi__Item__c> getItem(List<String> items) {
		return [SELECT Id, Name
				FROM OrderApi__Item__c 
				WHERE Name IN :items];
	}

	public static List<OrderApi__Renewal__c> getRenewalTermsToShow(Integer currentYear) {
		User loggedUser = CPAO_UserSelector.getCurrentUser();
		return [SELECT Id, OrderApi__Subscription__r.OrderApi__Contact__c, OrderApi__Subscription__c 
				FROM OrderApi__Renewal__c 
				WHERE CALENDAR_YEAR(CreatedDate) = :currentYear AND CPAO_Hide_Term__c = false AND
				OrderApi__Subscription__r.OrderApi__Contact__c = :loggedUser.ContactId AND
				OrderApi__Subscription__r.OrderApi__Subscription_Plan__r.Name = 'AMD'];
	}

	public static List<OrderApi__Renewal__c> getRenewalTermsToShow(Integer currentYear,String subcriptionPlanName) {
		User loggedUser = CPAO_UserSelector.getCurrentUser();
		return [SELECT Id, OrderApi__Subscription__r.OrderApi__Contact__c, OrderApi__Subscription__c, OrderApi__Is_Active__c 
				FROM OrderApi__Renewal__c 
				WHERE CALENDAR_YEAR(CreatedDate) = :currentYear AND OrderApi__Is_Active__c=false AND
				OrderApi__Subscription__r.OrderApi__Contact__c = :loggedUser.ContactId AND
				OrderApi__Subscription__r.OrderApi__Subscription_Plan__r.Name = :subcriptionPlanName];
	}
	
    public static List<OrderApi__Subscription_Plan__c> getSubscriptionPlan(String planName) {
    	return [SELECT Id, Name 
               	FROM OrderApi__Subscription_Plan__c
               	WHERE Name =:planName]; 
    }

    public static List<OrderApi__Renewal__c> getCurrentRenewal(Id subscriptionId) {
    	return [SELECT Id,OrderApi__Subscription__c,OrderApi__Term_Start_Date__c,
					OrderApi__Term_End_Date__c,OrderApi__Renewed_Date__c  
			    FROM OrderApi__Renewal__c
				WHERE OrderApi__Term_Start_Date__c <= TODAY AND OrderApi__Renewed_Date__c = NULL
                AND OrderApi__Subscription__c=:subscriptionId];
    }

     public static List<OrderApi__Subscription_Plan__c> getApplicationsToRenew(String planName,String contactType, String cpaStatus,String licenceType,String licenceStatus) {
    	return [SELECT Id, Name 
               	FROM OrderApi__Subscription_Plan__c
               	WHERE Name =:planName]; 
    }

}