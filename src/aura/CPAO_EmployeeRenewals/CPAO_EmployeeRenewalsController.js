({
	doInit : function(component, event, helper) {
		console.log('Marco test renewals342r3');
		var apexAction = component.get('c.getRenewals');
        apexAction.setCallback(this, function(response) {
        	var state = response.getState();
        	if(state === "SUCCESS"){
                var applicationList = response.getReturnValue().renewalApplications;
                console.log(applicationList);
                var termList = [];
                if(!(response.getReturnValue().relevantTerms == null && response.getReturnValue().relevantTerms == undefined)){
                    termList = response.getReturnValue().relevantTerms;
                }
                var terms = [];
                var openApplicaions = [];
                var closedApps = [];
                var deferredApps = [];
                var additionalInfo = [];
                if(applicationList.length > 0){
                	for(var i = 0; i<applicationList.length; i++){
	                	if(applicationList[i].CPAO_Status__c == 'Open'){
	                		openApplicaions.push(applicationList[i]);
	                	}
	                	if(applicationList[i].CPAO_Status__c == 'Completed' || applicationList[i].CPAO_Status__c == 'Submitted'){
	                		closedApps.push(applicationList[i]);
	                	}
                        if(applicationList[i].CPAO_Status__c == 'Submitted-Payment Deferred'){
                            deferredApps.push(applicationList[i]);
                        }
	                }
	                if(openApplicaions.length != 0){
	                	component.set("v.openApps", openApplicaions);
	                }
	                if(closedApps.length != 0){
	                	component.set("v.closedApps", closedApps);
	                }
                    if(deferredApps.length != 0){
                        component.set("v.deferredApps", deferredApps);
                    }
                } 
                if(termList.length > 0) {
                	component.set("v.terms", termList);
	            }    

                var palRenewalTerms =response.getReturnValue().PalRenewalTerms;
                console.log(palRenewalTerms);
                if(palRenewalTerms.length > 0){
                    component.set("v.showPalRenewal", true);
                }
                console.log(p);
            }
                
                

        });
        $A.enqueueAction(apexAction);
	},
	clickOnTerm : function(component, event, helper) {
		var index = event.getSource().get("v.name");
        var terms = component.get("v.terms");
        var term = terms[index];
        console.log('term');
        console.log(term);

        var apexAction = component.get('c.startApplication');
        apexAction.setParams({
	    	"term": term
	    });
        apexAction.setCallback(this, function(response) {
        	var state = response.getState();
        	if(state === "SUCCESS"){
                var result = response.getReturnValue();
                if(result.status == 'Success'){
                	console.log(result.application);
                	window.location.href = result.application.CPAO_Form_URL__c;
                }
            }
        });
        $A.enqueueAction(apexAction);
	},

    finishDeferredAppJS : function(component, event, helper) {
        var index = event.getSource().get("v.name");
        var deferredApps = component.get("v.deferredApps");
        var deferredApp = deferredApps[index];
        console.log('deferredApp');
        console.log(deferredApp);

        var apexAction = component.get('c.finishDeferredApp');
        console.log('marco 1');
        apexAction.setParams({
            "application": deferredApp
        });
        console.log('marco 2');
        var applicationHasChanged = component.getEvent("applicationHasChanged");
        applicationHasChanged.setParams({
            "nextButtonClickeable" : true,
            "action" : "showSpinner"});
        applicationHasChanged.fire();
        apexAction.setCallback(this, function(response) {
            console.log('marco 3');
            var state = response.getState();
            console.log('state');
            console.log(state);
            if(state === "SUCCESS"){
                var result = response.getReturnValue();
                console.log('result');
                console.log(result);
                if(result.status == 'Success'){
                    
                    //console.log(result.application);
                    window.location.href = result.checkOutScreenURL;
                } else {
                    helper.methodFailure(component, event, helper);
                }
            } else {
                helper.methodFailure(component, event, helper);
            }
        });
        $A.enqueueAction(apexAction);
    },

	clickOnOpenApp : function(component, event, helper) {
		var index = event.getSource().get("v.name");
        var apps = component.get("v.openApps");
        var app = apps[index];
        window.location.href = app.CPAO_Form_URL__c;
	},
    startPalRenewal : function(component, event, helper) {
      
        window.location.href = './CPAO_PAL_Renewal';
    },
    
})