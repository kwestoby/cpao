({
	doInit : function(component, event, helper) {
		var apexAction = component.get('c.getCPDandPOAinfo');
        apexAction.setCallback(this, function(response) {
        	var state = response.getState();
        	if(state === "SUCCESS"){
                var result = response.getReturnValue();

                component.set("v.priorNoComply", result.priorNoComply);
                component.set("v.application", result.currentAMDCPDApplication);
                if(result.currentAMDCPDApplication.CPAO_Declaration__c != undefined || result.currentAMDCPDApplication.CPAO_Declaration__c != null){
                	var checkedComp = component.find(result.currentAMDCPDApplication.CPAO_Declaration__c).set("v.checked", true);
                	// checkedComp.checked = true;
                }
                component.set("v.waiverExplanations", result.waiverExplanations);
                
				var poaS = [];
				for(var i=0;i<result.poaItems.length;i++) {
					poaS.push({poa:result.poaItems[i], error:""});
				}
                component.set("v.poaS", poaS);
                
            }
        });
        $A.enqueueAction(apexAction);
	},

	onRadioChange : function(component, event, helper) {
		var newVal = event.getSource().get('v.label');
		var app = component.get("v.application");
		app.CPAO_Declaration__c = newVal;
		component.set("v.application", app);

        var waiverValueMap = component.get("v.waiverExplanations");
        var newWaiverExplanation = waiverValueMap[newVal];
        component.set("v.waiverExplanation", newWaiverExplanation);
	},

	clickOnNextButton : function(component,event,helper){
    	var talkingToComponent = event.getParam("componentName");
    	if(talkingToComponent == 'step5'){
    		var app = component.get("v.application");
    		if(app.CPAO_Declaration__c == 'I do not comply'){
                var PreviousPOAs = component.get("v.priorNoComply");
                if(PreviousPOAs == true){
                    app.CPAO_Mulitple_POAs_in_Triennial_Period__c = true;
                } else {
                    var poaS = component.get("v.poaS");
                    if(poaS.length == 0){
                        helper.methodFailure(component, event, helper);
                        return;
                    }
                    var log = component.find('CPDFormOtherBody');
                    log = component.find('CPDFormNoComply');

                    var validForm = component.find('CPDFormNoComply').reduce(function (validSoFar, inputCmp) {
                    // Displays error messages for invalid fields
                    inputCmp.showHelpMessageIfInvalid();
                    return validSoFar && inputCmp.get('v.validity').valid;

                    }, true);
                    if(validForm == false){
                        helper.methodFailure(component, event, helper);
                        return;
                    }
                }        			
    		}

    		if(app.CPAO_Declaration__c == 'Other extraordinary consequences'){
    			var log = component.find('CPDFormOther');

    			var validForm = component.find('CPDFormOther').reduce(function (validSoFar, inputCmp) {
		        // Displays error messages for invalid fields
		        inputCmp.showHelpMessageIfInvalid();
		        return validSoFar && inputCmp.get('v.validity').valid;
		    	}, true);
		    	if(validForm == false){
                    helper.methodFailure(component, event, helper);
		    		return;
		    	}
    		}
    		
    		if(app.CPAO_Declaration__c == 'Comply with other professional body requirements'){
    			var log = component.find('CPDFormOtherBody');
    			var validForm = component.find('CPDFormOtherBody').reduce(function (validSoFar, inputCmp) {
		        // Displays error messages for invalid fields
		        inputCmp.showHelpMessageIfInvalid();
		        return validSoFar && inputCmp.get('v.validity').valid;
		    	}, true);
		    	if(validForm == false){
                    helper.methodFailure(component, event, helper);
		    		return;
		    	}
    		}
            if(app.CPAO_Declaration__c == '' || app.CPAO_Declaration__c == undefined ){
                helper.methodFailure(component, event, helper, "please choose a value", "Error");
                return;
            }
    		var poaS = component.get("v.poaS");
			var poaList = []
			for(var i=0;i<poaS.length;i++) {
				poaS[i].error = "";
				poaList.push(poaS[i].poa);
				if(poaS[i].poa.CPAO_Start_Date__c>poaS[i].poa.CPAO_End_Date__c) {
					poaS[i].error += "<p>End Date cannot occur before Start Date.</p>";
				}
			}
			component.set("v.poaS",poaS);
    		poaList = JSON.stringify(poaList);
    		var apexAction = component.get('c.saveAppAndPOA');
	        apexAction.setParams({
		    	"currentAMDCPDApplication": app,
		    	"poaS": poaList
		    });
	        apexAction.setCallback(this, function(response) {
	        	var state = response.getState();
	        	if(state === "SUCCESS"){
	        		var result = response.getReturnValue();
	                if(result == 'Success'){
	                	var actionToTake = event.getParam("nextStepName");
                        var applicationHasChanged = component.getEvent("applicationHasChanged");
                        applicationHasChanged.setParams({
                            "nextButtonClickeable" : true,
                            "action" : actionToTake});
				        applicationHasChanged.fire();
	                } else {
                        helper.methodFailure(component, event, helper);
	                }                
	            } else {
                    helper.methodFailure(component, event, helper);
                }
	        });
	        $A.enqueueAction(apexAction);
    	}        
    },

    removeItem : function(component, event, helper) {
        var index = event.getSource().get("v.name");
        var poaS = component.get("v.poaS");
        var poa = poaS[index].poa;
        if (poa.Id == undefined || poa.Id == null) { 
            poaS.splice(index, 1);
            component.set("v.poaS", poaS);
            return;
        } else {
        	var apexAction = component.get("c.deletePOA");
            var recordId = poa.Id;
            apexAction.setParams({
                "poaId" : recordId
            });
            apexAction.setCallback(this, function(response) {
                var state = response.getState();
                if (state === "SUCCESS") {
                    var result =  response.getReturnValue();
                    if (result == 'Success') {
                        poaS.splice(index, 1);
                        component.set("v.poaS", poaS);
                    }
                }
            });

            $A.enqueueAction(apexAction);
        }

    },

	addLine : function(component, event, helper) {
		var poaSItem = component.get("v.poaS");
        poaSItem.push({poa:{},error:""});
        component.set("v.poaS", poaSItem);
	},

})