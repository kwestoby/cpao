@isTest
private class CPAO_ExistingApplicationsCTRLTest {
	@isTest
	static void itShould() {
		CPAO_TestFactory.automationControlSetup();
		CPAO_ExistingApplicationsCTRL.getApplications();

	}

	@testSetup
	static void dataInit(){

		CPAO_TestFactory.automationControlSetup();
		
		List<Account> accounts = CPAO_TestFactory.createAccounts(1);
		Database.insert(accounts);

		List<Contact> contacts = CPAO_TestFactory.createContacts(accounts[0],1);
		Database.insert(contacts);

		List<CPAO_Application__c> applications =CPAO_TestFactory.createPALApplication(1,contacts[0]);
		Database.insert(applications);

	}


	@isTest
	static void callAllApplications() {


		List<Account> applications =[Select id, CPAO_Account_Status__c FROM Account];
		List<CPAO_Application__c>getApplication = CPAO_ExistingApplicationsCTRL.getAllApplications();

		System.assertEquals(applications.size(),getApplication.size());
	}



	@isTest
	static void callRenewal() {
		id recordTypeId = Schema.SObjectType.CPAO_Application__c.getRecordTypeInfosByName().get('AMD/CPD').getRecordTypeId();

		CPAO_Application__c application =[Select id FROM CPAO_Application__c LIMIT 1];
		application.RecordTypeId = recordTypeId;
		CPAO_ExistingApplicationsCTRL.getRenewals();

	}


	@isTest
	static void callDeclarations() {


		CPAO_ExistingApplicationsCTRL.getDeclarations();

	}

	@isTest
	static void callCancelApplication() {

		CPAO_Application__c application =[Select id FROM CPAO_Application__c LIMIT 1];
		CPAO_ExistingApplicationsCTRL.cancelApplication(application.id);

	}


	@isTest
	static void callWithdrawApplication() {

		CPAO_Application__c application =[Select id FROM CPAO_Application__c LIMIT 1];
		CPAO_ExistingApplicationsCTRL.withdrawApplication(application.id);

	}


}