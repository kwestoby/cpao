public without sharing class CPAO_InfractionServices {
	
	public static void ledToSuspension(List<CPAO_Infraction__c> infractions) {
		for (CPAO_Infraction__c infraction:infractions) {
			infraction.CPAO_Date_Led_to_Suspension__c = Date.today();
		}
	}

	public static void ledToRevocationDeregistration(List<CPAO_Infraction__c> infractions) {
		for (CPAO_Infraction__c infraction:infractions) {
			infraction.CPAO_Date_Led_Revocation_Deregistration__c = Date.today();
		}
	}
}