@isTest
private class CPAO_FileControllerTest
{
    public static List<Account> accounts;
    public static List<Contact> contacts;
    public static CPAO_Application__c application1;
    
    @isTest
    static void createAccAttachFiles()
    {
        CPAO_TestFactory.automationControlSetup();
        accounts = CPAO_TestFactory.createAccounts(1);
        insert accounts;
        
        contacts = CPAO_TestFactory.createContacts(accounts[0], 1);
        insert contacts;
        
        RecordType amdCPDRecordType = [SELECT Id FROM RecordType WHERE DeveloperName = 'CPAO_Reinstatement_Application' AND SobjectType = 'CPAO_Application__c'];
        application1 = new CPAO_Application__c();
        application1.RecordTypeId = amdCPDRecordType.Id;
        application1.CPAO_Contact__c=contacts[0].Id;
        application1.CPAO_Status__c = 'Completed';
        application1.CPAO_Recommendation__c= 'Approve';
        application1.CPAO_Recommendation_Notes__c= 'Test Rec';
        insert application1;
    
        string keystring = 'theyKEYrandomstringhereendswith\n';
        Test.startTest();
        CPAO_FileController.saveTheFile(accounts[0].ID, 'test file', keystring, 'test','test');
        CPAO_FileController.upsertPalApplication(application1);
        Test.stopTest();
    }
}