({
    getInitWrapper : function(component, helper) {
        var action = component.get("c.getQuestionnaire");
        action.setParams({
            currentAPF : component.get("v.currentAPF")
        });

        action.setCallback(this, function(response){
            var state = response.getState();
            var result = response.getReturnValue();
            if(state === "SUCCESS") {

                helper.prepareDisplayForAPF(component, helper, result);
                component.set("v.isReady", true);
            }
        });
        $A.enqueueAction(action);
    },

    getFields : function() {
        var fields = [
            'CPAO_Public_Accountng_Accounting_Service__c',
            'CPAO_Other_Services__c',
            'CPAO_Ceased_Providing_Date__c',
            'CPAO_Firm_Structure_Change__c',
            'CPAO_Audit_Engagements__c',
            'CPAO_Review_Engagements__c',
            'CPAO_Compilation_Engagements__c',
            'CPAO_Personal_Tax_Engagements__c',
            'CPAO_Corporate_Tax_Engagements__c',
            'CPAO_Audit_Clients__c',
            'CPAO_Audit_Clients_Reporting_Issuers__c',
            'CPAO_Audit_Clients_Non__c',
            'CPAO_Review_Engagement_Clients__c',
            'CPAO_Compilation_Engagement_Clients__c',
            'CPAO_Corporate_Tax_Engagement_Clients__c',
            'CPAO_Personal_Tax_Engagement_Clients__c',
            'CPAO_CPAB_Registrant__c',
            'CPAO_CPAB_Registration_Date__c',
            'CPAO_PCAOB_Registrant__c',
            'CPAO_PCAOB_Registration_Date__c',
            'CPAO_Annual_Practitioner_Fee_Declaration__c'
        ];
        return fields;
    },

    prepareDisplayForAPF : function(component, helper, result) {
        var currentAPF = component.get("v.currentAPF");
        var fields = helper.getFields();
        
        // If we have a pre-existing value, pre-populate it
        for(var i = 0; i < fields.length; i++) {
            var field = fields[i];
            if(currentAPF[field]) {
                result[field].value = currentAPF[field]; 
            }
            component.set("v."+field, result[field]);
        }
    },

    saveAPF : function(component, helper, isQuit) {
        var currentAPF = component.get("v.currentAPF");
        var currentAPFJSON = JSON.stringify(currentAPF);
        
        var action = component.get("c.updateAPF");
        action.setParams({
            currentAPFJSON : currentAPFJSON
        });
        action.setCallback(this, function(response){
            var state = response.getState();
            var result = response.getReturnValue();
            var err = response.getError();
            if(state === "SUCCESS") {
                component.set("v.currentAPF", result);
                
                if(isQuit) {
                    helper.fireNextEvent(component, "Quit");
                } else {
                    helper.fireNextEvent(component, "Next");
                }
            }
        });
        $A.enqueueAction(action);
    },

    updateAPF : function(component, helper, isQuit) {
        var questions = component.find("question");

        var isValid = true;
        for(var i = 0; i < questions.length; i++) {
            var q = questions[i];
            var show = q.get("v.show");
            var value = q.get("v.value");

            if(show && !value) {
                isValid = false;
                break;
            }
        }

        if(!isValid && !isQuit) {
            component.set("v.errorMsg", "Please fill in all the fields.")
            return;
        }

        if(isQuit || (isValid && !isQuit)) {
            helper.saveAPF(component, helper, isQuit);
        }

    },

    fireNextEvent : function(component, nextStep) {
        var nextButtonClick = $A.get("e.c:CPAO_Next_Button_Has_Been_Pressed");
        nextButtonClick.setParams({
            "componentName" : "CPAO_AnnualPractitionerFee",
            "nextStepName" : nextStep});
        nextButtonClick.fire();
    },

    firePrevEvent : function(component) {
        var prevButtonClick = $A.get("e.c:CPAO_Prev_Button_Has_Been_Pressed");
        prevButtonClick.setParams({
            "componentName" : "CPAO_AnnualPractitionerFee",
            "nextStepName" : "Prev"});
        prevButtonClick.fire();
    },

})