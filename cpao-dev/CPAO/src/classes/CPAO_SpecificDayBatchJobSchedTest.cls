@isTest
private class CPAO_SpecificDayBatchJobSchedTest {

	@isTest
	private static void testResetMemberPricingh() {
		Test.startTest();
		CPAO_SpecificDayBatchJobSched batch = new CPAO_SpecificDayBatchJobSched();
		batch.today = Date.newInstance(2018, 1, 9);
		System.schedule('Test', '0 0 0 * * ? *', batch);
		Test.stopTest();
	}

	@isTest
	private static void testCpdInfractionAssignment() {
		Test.startTest();
		CPAO_SpecificDayBatchJobSched batch = new CPAO_SpecificDayBatchJobSched();
		batch.today = Date.newInstance(2018, 1, 3);
		System.schedule('Test', '0 0 0 * * ? *', batch);
		Test.stopTest();
	}

	@isTest
	private static void testCpdDeferredInfracAssignt() {
		Test.startTest();
		CPAO_SpecificDayBatchJobSched batch = new CPAO_SpecificDayBatchJobSched();
		batch.today = Date.newInstance(2018, 1, 11);
		System.schedule('Test', '0 0 0 * * ? *', batch);
		Test.stopTest();
	}

	@isTest
	private static void testAutoRenewWaiverAmdSubs() {
		Test.startTest();
		CPAO_SpecificDayBatchJobSched batch = new CPAO_SpecificDayBatchJobSched();
		batch.today = Date.newInstance(2018, 1, 12);
		System.schedule('Test', '0 0 0 * * ? *', batch);
		Test.stopTest();
	}

	@isTest
	private static void testAutoRenewPALSubs() {
		Test.startTest();
		CPAO_SpecificDayBatchJobSched batch = new CPAO_SpecificDayBatchJobSched();
		batch.today = Date.newInstance(2018, 1, 16);
		System.schedule('Test', '0 0 0 * * ? *', batch);
		Test.stopTest();
	}

	@isTest
	private static void testClearEmployerIsPayingPALR() {
		Test.startTest();
		CPAO_SpecificDayBatchJobSched batch = new CPAO_SpecificDayBatchJobSched();
		batch.today = Date.newInstance(2018, 1, 15);
		System.schedule('Test', '0 0 0 * * ? *', batch);
		Test.stopTest();
	}
}