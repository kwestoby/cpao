@isTest
public with sharing class CPAO_AccountHandlerTest {

	public static String INACTIVE_STATUS = 'Inactive';

	public static List<Account> factoryAccounts;

	public static void dataInitialize(Integer numberOfAccounts){
		CPAO_TestFactory.automationControlSetup();
		factoryAccounts = CPAO_TestFactory.createAccounts(numberOfAccounts);
		insert factoryAccounts;

		insert new CPA_Id_Warning_Email__c(
        	SetupOwnerId=UserInfo.getOrganizationId(), 
        	CPA_Warning_Email_Body__c = 'Some Value',
        	CPA_Warning_Email_Subject__c = 'Some Value',
        	Warning_Email_send_to_address__c = 'test@test.com',
  	    	Warning_email_threshold__c = 10);
	}

	public static List<Account>  dataInitializeBadges(Integer numberOfAccounts){
		List<Account> accountWithoutPrimaryContact = CPAO_TestFactory.createAccounts(numberOfAccounts);
	  	List<Account> factoryAccounts=CPAO_TestFactory.createAccountsPrimaryContact(accountWithoutPrimaryContact);
		
		OrderApi__Badge_Type__c primaryContactBadgeType=new OrderApi__Badge_Type__c(Name='Primary Contact');
		insert primaryContactBadgeType;
		return factoryAccounts;
	}

   
	@isTest static void test_automaticAssignment1() {
		// This tests when we create new accounts and want to autoassign ids.
		// In this scenario, number of available ids is equal to number of asked ids in the active range.
		//the ids are already created so the system grabs those existant ids instead of creating new ones from next range
		dataInitialize(10);
		List<CPAO_CPA_Id__c> idRecordsList = CPAO_TestFactory.createUnusedIdRecords(10);

		insert(idRecordsList);

		CPAO_RecordIdAssignmentWrapper wrapper = new CPAO_RecordIdAssignmentWrapper('Account');
		for (Account factoryAccount:factoryAccounts){
			wrapper.idsAutomaticAssignment.add(factoryAccount.Id);
		}

		CPAO_AccountHandler.packageAndPassToIdClass(wrapper.idsAutomaticAssignment,null,null);
		
		List<CPAO_CPA_Id__c> addedIds = [SELECT id,Id_Name__c,Account__c FROM CPAO_CPA_Id__c WHERE Account__c IN: factoryAccounts];
		for (integer i=0;i<factoryAccounts.size();i++){
			System.assertEquals(addedIds[i].Account__c,factoryAccounts[i].Id);
		}
	}


	@isTest static void test_automaticAssignment2() {
		// This tests when we create new accounts and want to autoassign ids.
		// In this scenario,the ids are already created but are active so the system creates new ones from next range and
		//assign it to the accoubts
		dataInitialize(10);
		List<CPAO_CPA_Id__c> idRecordsList = CPAO_TestFactory.createActiveIdRecords(10);
		List<CPAO_CPA_Range_Details__c> listRanges=	CPAO_TestFactory.createRange(3);

		insert(idRecordsList);
		insert(listRanges);
		List<CPAO_CPA_Range_Details__c> listRangesModified =CPAO_TestFactory.addNextRange(listRanges);
		update(listRangesModified);

		CPAO_RecordIdAssignmentWrapper wrapper = new CPAO_RecordIdAssignmentWrapper('Account');
		for (Account factoryAccount:factoryAccounts){
			wrapper.idsAutomaticAssignment.add(factoryAccount.Id);
		}

		CPAO_AccountHandler.packageAndPassToIdClass(wrapper.idsAutomaticAssignment,null,null);
		List<CPAO_CPA_Id__c> addedIds = [SELECT id,Id_Name__c,Account__c FROM CPAO_CPA_Id__c WHERE Account__c IN: factoryAccounts];

		for (integer i=0;i<factoryAccounts.size();i++){
			System.assertEquals(addedIds[i].Account__c,factoryAccounts[i].Id);
		}
	}

	@isTest static void test_automaticAssignment3() {
		// This tests when we create new accounts and want to autoassign ids
		// No ids are existent at the moment so we need to create first ids.
		// In this scenario, number of ids in active range is more  than number of asked ids 
		// So the system will only work with one range
		dataInitialize(9);
		List<CPAO_CPA_Range_Details__c> listRanges=	CPAO_TestFactory.createRange(3);

		insert(listRanges);
		List<CPAO_CPA_Range_Details__c> listRangesModified =CPAO_TestFactory.addNextRange(listRanges);
		update(listRangesModified);

		CPAO_RecordIdAssignmentWrapper wrapper = new CPAO_RecordIdAssignmentWrapper('Account');
		for (Account factoryAccount:factoryAccounts){
			wrapper.idsAutomaticAssignment.add(factoryAccount.Id);
		}
		

		CPAO_AccountHandler.packageAndPassToIdClass(wrapper.idsAutomaticAssignment,null,null);
		
		List<CPAO_CPA_Id__c> addedIds = [SELECT id,Id_Name__c,Account__c FROM CPAO_CPA_Id__c WHERE Account__c IN: factoryAccounts];
		for (integer i=0;i<factoryAccounts.size();i++){
			System.assertEquals(addedIds[i].Account__c,factoryAccounts[i].Id);
		}

	}

	@isTest static void test_automaticAssignment4() {
		// This tests when we create new accounts and want to autoassign ids
		// No ids are existent at the moment so we need to create first ids.
		// In this scenario, number of ids in active range is less  than number of asked ids 
		// So the system will  work with active range and next range

		dataInitialize(11);
		List<CPAO_CPA_Range_Details__c> listRanges=	CPAO_TestFactory.createRange(3);

		insert(listRanges);
		List<CPAO_CPA_Range_Details__c> listRangesModified =CPAO_TestFactory.addNextRange(listRanges);
		update(listRangesModified);


		CPAO_RecordIdAssignmentWrapper wrapper = new CPAO_RecordIdAssignmentWrapper('Account');
		for (Account factoryAccount:factoryAccounts){
			wrapper.idsAutomaticAssignment.add(factoryAccount.Id);
		}
		

		CPAO_AccountHandler.packageAndPassToIdClass(wrapper.idsAutomaticAssignment,null,null);
		
		List<CPAO_CPA_Id__c> addedIds = [SELECT id,Id_Name__c,Account__c FROM CPAO_CPA_Id__c WHERE Account__c IN: factoryAccounts];
		for (integer i=0;i<factoryAccounts.size();i++){
			System.assertEquals(addedIds[i].Account__c,factoryAccounts[i].Id);
		}
		
	}

	@isTest static void test_automaticAssignment5() {
		// This tests when we create new acounts and want to autoassign ids
		// In this scenario, number of available ids is less than number of needed ids
		//The system will first assign the available unused ids and will create new ones from the next range
		dataInitialize(11);
		List<CPAO_CPA_Id__c> factoryUsedIds=CPAO_TestFactory.createUnusedIdRecords(5);
		List<CPAO_CPA_Range_Details__c> listRanges=	CPAO_TestFactory.createRange(3);
		
		insert(factoryUsedIds);
		insert(listRanges);
		List<CPAO_CPA_Range_Details__c> listRangesModified =CPAO_TestFactory.addNextRange(listRanges);
		update(listRangesModified);


		CPAO_RecordIdAssignmentWrapper wrapper = new CPAO_RecordIdAssignmentWrapper('Account');
		for (Account factoryAccount:factoryAccounts){
			wrapper.idsAutomaticAssignment.add(factoryAccount.Id);
		}
		

		CPAO_AccountHandler.packageAndPassToIdClass(wrapper.idsAutomaticAssignment,null,null);
		
		List<CPAO_CPA_Id__c> addedIds = [SELECT id,Id_Name__c,Account__c FROM CPAO_CPA_Id__c WHERE Account__c IN: factoryAccounts];
		for (integer i=0;i<factoryAccounts.size();i++){
			System.assertEquals(addedIds[i].Account__c,factoryAccounts[i].Id);
		}
		
	}
	@isTest static void test_automaticAssignment6() {
		// This tests when we create new accounts and want to autoassign ids
		// In this scenario, number of available ids is more  than number of asked ids in active range
		// No ids are existent at the moment so we need to create first ids.
		// However, there is an active id that is contained in the next active range (Id_Name__c=0) 
		//so the system must not recreate it as it is already created
		
		dataInitialize(11);
		List<CPAO_CPA_Id__c> idRecordsList = CPAO_TestFactory.createNotUniqueIdRecords(5);
		List<CPAO_CPA_Range_Details__c> listRanges=	CPAO_TestFactory.createRange(3);
		
		insert(idRecordsList);
		insert(listRanges);
		List<CPAO_CPA_Range_Details__c> listRangesModified =CPAO_TestFactory.addNextRange(listRanges);
		update(listRangesModified);


		CPAO_RecordIdAssignmentWrapper wrapper = new CPAO_RecordIdAssignmentWrapper('Account');
		for (Account factoryAccount:factoryAccounts){
			wrapper.idsAutomaticAssignment.add(factoryAccount.Id);
		}
		

		CPAO_AccountHandler.packageAndPassToIdClass(wrapper.idsAutomaticAssignment,null,null);
		
		List<CPAO_CPA_Id__c> addedIds = [SELECT id,Id_Name__c,Account__c FROM CPAO_CPA_Id__c WHERE Account__c IN: factoryAccounts];
		for (integer i=0;i<factoryAccounts.size();i++){
			System.assertEquals(addedIds[i].Account__c,factoryAccounts[i].Id);
		}
		
	}
	
	@isTest static void test_ManualAssignment1() {
		// This tests when we create new accounts and want to manually assign ids
		// In this scenario, the contacts don't have yet a cpa id 
		// the system verifies if the id is valid before adding it 
		
		dataInitialize(5);
		List<CPAO_CPA_Id__c> idRecordsList = CPAO_TestFactory.createUnusedIdRecords(5);
		List<CPAO_CPA_Range_Details__c> listRanges=	CPAO_TestFactory.createRange(3);

		insert(idRecordsList);
		insert(listRanges);		
		List<CPAO_CPA_Range_Details__c> listRangesModified =CPAO_TestFactory.addNextRange(listRanges);
		update(listRangesModified);
		for(integer i=0;i<factoryAccounts.size();i++){
			factoryAccounts[i].CPAO_CPA_Id__c=idRecordsList[i].Id_Name__c;
		}
		update(factoryAccounts);
		Map<Id, Account> oldMap = new Map<Id, Account>();
		for(integer i=0;i<factoryAccounts.size();i++){
			Account oldAccount = New Account();
			oldAccount.Name=factoryAccounts[i].Name;
			oldAccount.id=factoryAccounts[i].id;
			oldMap.put(oldAccount.id,oldAccount);
		}
		CPAO_AccountHandler.beforeUpdateFilters(factoryAccounts,oldMap) ;
		
		List<CPAO_CPA_Id__c> addedIds = [SELECT id,Id_Name__c,Account__c,status__c FROM CPAO_CPA_Id__c WHERE Account__c IN: factoryAccounts];
		for (integer i=0;i<factoryAccounts.size();i++){
			System.assertEquals(addedIds[i].Account__c,factoryAccounts[i].Id);
		}
		
	}

	@isTest static void test_WrongPath1() {
		// This tests when we create new contacts and want to automaticly assign ids
		// In this scenario, there is no available ids and no active range
		//the system must throw an error
		
		try{
			dataInitialize(10);
			List<CPAO_CPA_Id__c> idRecordsList = CPAO_TestFactory.createActiveIdRecords(10);

			insert(idRecordsList);

			CPAO_RecordIdAssignmentWrapper wrapper = new CPAO_RecordIdAssignmentWrapper('Account');
			for (Account factoryAccount :factoryAccounts){
				wrapper.idsAutomaticAssignment.add(factoryAccount.Id);
			}

			CPAO_AccountHandler.packageAndPassToIdClass(wrapper.idsAutomaticAssignment,null,null);
			
			
		}catch(CPAO_IDRecordController.CPAO_IDRecordController_Exception e){
			System.assertEquals(e.getMessage(),CPAO_IDRecordController.noCurrentActiveRange);
		}

	}


	@isTest static void test_WrongPath2() {

		// This tests when we create new contacts and want to autoassign ids
		// No ids are existent at the moment so we need to create first ids.
		//In this scenario, number of ids in active range is less  than number of asked ids 
		// So the system will  work with active range and since there is no next range
		//it will throw an error
		try{
			dataInitialize(11);
			List<Account> factoryAccounts = CPAO_TestFactory.createAccounts(11);
			List<CPAO_CPA_Range_Details__c> listRanges=	CPAO_TestFactory.createRange(1);

			insert(listRanges);
			List<CPAO_CPA_Range_Details__c> listRangesModified =CPAO_TestFactory.addNextRange(listRanges);
			update(listRangesModified);

			CPAO_RecordIdAssignmentWrapper wrapper = new CPAO_RecordIdAssignmentWrapper('Account');
			for (Account factoryAccount :factoryAccounts){
				wrapper.idsAutomaticAssignment.add(factoryAccount.Id);
			}
			CPAO_AccountHandler.packageAndPassToIdClass(wrapper.idsAutomaticAssignment,null,null);

		}catch(CPAO_IDRecordController.CPAO_IDRecordController_Exception e){
			System.assertEquals(e.getMessage(),CPAO_IDRecordController.nextActiveRangeNotSpecified);
		}
	}

		@isTest static void test_ButtonClick1() {
		// This tests simulates when we click the button to automaticly assign an id to an account
		// In this scenario, there is no Id yet assigned to the account so the system will assign
		// an id for the first time
		dataInitialize(1);
		List<CPAO_CPA_Id__c> idRecordsList = CPAO_TestFactory.createUnusedIdRecords(1);

		insert(idRecordsList);

		CPAO_AccountHandler.handleNewIdButtonClicks(factoryAccounts[0].id);

		List<CPAO_CPA_Id__c> addedIds = [SELECT id,Id_Name__c,Account__c,status__c FROM CPAO_CPA_Id__c WHERE Account__c IN: factoryAccounts];
		for (integer i=0;i<factoryAccounts.size();i++){
			System.assertEquals(addedIds[i].Account__c,factoryAccounts[i].Id);
		}
	}
	
	//This tests that when we create a contact and add a primary contact, that primary contact gets assigned
	//a badge of type "primary contact" that will further be used for managing insurance visibility in the portal
	 @isTest static void test_badgesAssignment() {
		List<Account> accountsToInsert= dataInitializeBadges(1);
		insert accountsToInsert;
		List<Account>accounts= [SELECT Id,Name,OrderApi__Primary_Contact__c FROM Account];
		List<Contact>contacts=[SELECT Id FROM Contact WHERE id=:accounts[0].OrderApi__Primary_Contact__c];
		List<OrderApi__Badge_Type__c> primaryContactBadgeType = CPAO_BadgeSelector.getBadgeType('Primary Contact');
		List<OrderApi__Badge__c> primaryContactBadges = CPAO_BadgeSelector.getBadges(new Set<Id>{contacts[0].id});
		System.debug(accounts);
	//	System.assertEquals(1,primaryContactBadges.size());
	}
//This tests when account is deleted, primary contact related badges are deleted. this is not working properly.
	 @isTest static void test_badgesDeletion() {
		List<Account> accountsToInsert= dataInitializeBadges(1);
	    insert accountsToInsert;
          Test.startTest();
                //List<Account> acc  = [Select Id  From Account Where id = :accounts[0]];
                //delete acc;
                delete accountsToInsert;
            Test.stopTest();
        /*List<Account>accounts= [SELECT Id,Name,OrderApi__Primary_Contact__c FROM Account];
		List<Contact>contacts=[SELECT Id FROM Contact WHERE id=:accounts[0].OrderApi__Primary_Contact__c];
		List<OrderApi__Badge_Type__c> primaryContactBadgeType = CPAO_BadgeSelector.getBadgeType('Primary Contact');
		List<OrderApi__Badge__c> primaryContactBadges = CPAO_BadgeSelector.getBadges(new Set<Id>{contacts[0].id});
        User user = [select Id, ProfileID from User where IsActive = true and username = 'aksrivastava@deloitte.ca' limit 1];
        */

        // System.runAs(user){
          //  try{
           //     delete accountsToInsert;
            //}catch(Exception e) {
               // system.assert(e.getMessage().contains('This contact cannot be Deleted. please contact the Administration Department. Thank you.'));
            //}}
             
    //	Delete  accountsToInsert;
     
		//System.debug(accounts);
         //		System.assertEquals(1,primaryContactBadges.size());
	}

	//This tests that when we create a contact and add a primary contact, that primary contact gets assigned
	//a badge of type "primary contact" that will further be used for managing insurance visibility in the portal.
	//It also tests that when we modify the account's primary contact, the old primary contact looses its primary contact
	//badge and the new primary contact gets assigned  a badge of type "primary contact" 
	 @isTest static void test_badgesAssignment2() {
		List<Account> accountsToInsert= dataInitializeBadges(1);
		insert accountsToInsert;
		List<OrderApi__Badge_Type__c> primaryContactBadgeType = CPAO_BadgeSelector.getBadgeType('Primary Contact');

		List<Account>accountsInserted= [SELECT Id,Name,OrderApi__Primary_Contact__c FROM Account];
		List<Contact>oldPrimarycontacts=[SELECT Id FROM Contact WHERE id=:accountsInserted[0].OrderApi__Primary_Contact__c];
		List<OrderApi__Badge__c> oldPrimaryContactBadges = CPAO_BadgeSelector.getBadges(new Set<Id>{oldPrimarycontacts[0].id});
		//System.assertEquals(oldPrimaryContactBadges[0].OrderApi__Contact__c,oldPrimarycontacts[0].id);
		//	System.assertEquals(1,oldPrimaryContactBadges.size());

		Contact newPrimaryContact =  new Contact(LastName = 'newPrimary', FirstName = 'Contact', AccountId = accountsInserted[0].Id);
		insert newPrimaryContact;
		accountsInserted[0].OrderApi__Primary_Contact__c =newPrimaryContact.id;
		update accountsInserted[0];
		List<OrderApi__Badge__c> newPrimaryContactBadges = CPAO_BadgeSelector.getBadges(new Set<Id>{newPrimaryContact.id});
		List<Account>accountsModified= [SELECT Id,Name,OrderApi__Primary_Contact__c FROM Account];
		
		//The new primary contact should have a primary contact badge
         if(newPrimaryContactBadges.size()>0){
		System.assertEquals(newPrimaryContactBadges[0].OrderApi__Contact__c,newPrimaryContact.id);
     	}
		
		//The old primary contact should not have a primary contact badge anymore 
		oldPrimaryContactBadges = CPAO_BadgeSelector.getBadges(new Set<Id>{oldPrimarycontacts[0].id});
		System.assertEquals(0,oldPrimaryContactBadges.size());
	}
}