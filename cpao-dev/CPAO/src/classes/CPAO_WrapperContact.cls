public class CPAO_WrapperContact{
       
    @AuraEnabled
    public Contact con {get; set;}
    @AuraEnabled
    public Boolean selected {get; set;}
    @AuraEnabled
    public String statusOfCompletion {get; set;}
    @AuraEnabled
    public OrderApi__Subscription__c orderOfSubscription{get; set;}
    
}