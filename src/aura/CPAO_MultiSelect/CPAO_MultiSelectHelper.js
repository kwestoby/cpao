({
    getSelectedValues: function(component) {
        var options = component.get("v.selectOptions");
        var selectedValues = [];
        options.forEach(function(element){
            if(element.selected) {
                selectedValues.push(element.value);
            }
        });
        return selectedValues;
    },
    getSelectedLabels: function(component) {
        var options = component.get("v.selectOptions");
        var selectedLabels = [];
        options.forEach(function(element){
            if(element.selected) {
                selectedLabels.push(element.label);
            }
        });
        return selectedLabels;
    },
    setInfoText : function(component, labels) {
        if(labels.length>1) {
            component.set("v.infoText", labels.length+' options selected');
        } else if(labels.length==1) {
            component.set("v.infoText", labels[0]);
        } else {
            component.set("v.infoText", 'Select an Option');
        }
	},
    despatchSelectChangeEvent: function(component, values) {
        var selectChangeEvent = component.getEvent("selectChange");
        selectChangeEvent.setParams({
            values: values
        });
        selectChangeEvent.fire();
    }
})