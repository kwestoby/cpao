@isTest
private class CPAO_ReceiptItemUpdateHelperTest {

	@TestSetup
	private static void setupData() {
		CPAO_TestFactory.automationControlSetup();
		OrderApi__Business_Group__c businessGroup = new OrderApi__Business_Group__c(Name = 'Test BG');
		insert businessGroup;
		OrderApi__Item_Class__c itemClass = new OrderApi__Item_Class__c(Name = 'Test item class',
		                                                                OrderApi__Business_Group__c = businessGroup.Id);
		insert itemClass;
		OrderApi__Item__c item = new OrderApi__Item__c(Name = 'Test item',
		                                               OrderApi__Business_Group__c = businessGroup.Id,
		                                               OrderApi__Item_Class__c = itemClass.Id);
		insert item;
	}

	@isTest
	private static void testUpdateItemReceipt() {
		OrderApi__Business_Group__c businessGroup = [SELECT Id, OrderApi_Item_Name__c FROM OrderApi__Business_Group__c LIMIT 1];
		System.assert(businessGroup.OrderApi_Item_Name__c == null);
		Test.startTest();
		OrderApi__Item__c item = [SELECT Id, Name FROM OrderApi__Item__c LIMIT 1];
		update item;
		Test.stopTest();
		businessGroup = [SELECT Id, OrderApi_Item_Name__c FROM OrderApi__Business_Group__c LIMIT 1];
		System.assertEquals(item.Name, businessGroup.OrderApi_Item_Name__c);
	}
}