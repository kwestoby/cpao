/**
 * @author Sachet Khanal (sakhanal@deloitte.com)
 * @version 1.0.0
 * @description Test class for CPAO_BulkInvoiceWriteOffCTRL apex class.
 *
 *
 **/ 
@isTest
public class CPAO_BulkInvoiceWriteOffCTRLTest {
	@TestSetup
	static void setupData() {
		Contact con = new Contact(FirstName = 'Test', LastName = 'Contact', CPAO_CPA_Contact_Type__c = 'Member', CPAO_CPA_Status__c = 'Active');
		insert con;
		List<OrderApi__Sales_Order__c> salesOrders = new List<OrderApi__Sales_Order__c> ();
		for (Integer i = 0; i< 10; i++) {
			salesOrders.add(new OrderApi__Sales_Order__c(OrderApi__Entity__c = 'Contact',
			                                             OrderApi__Contact__c = con.Id,
			                                             OrderApi__Posted_Date__c = System.today() - 5,
			                                             OrderApi__Posting_Entity__c = 'Invoice',
			                                             OrderApi__Posting_Status__c = 'Posted'));
		}
		insert salesOrders;
		List<OrderApi__Sales_Order_Line__c> salesOrderLines = new List<OrderApi__Sales_Order_Line__c> ();
		List<OrderApi__Invoice__c> invoices = new List<OrderApi__Invoice__c> ();
		for (OrderApi__Sales_Order__c s : salesOrders) {
			salesOrderLines.add(new OrderApi__Sales_Order_Line__c(OrderApi__Sales_Order__c = s.Id,
			                                                      OrderApi__Sale_Price__c = 10.00,
			                                                      OrderApi__Total__c = 10.00));
			invoices.add(new OrderApi__Invoice__c(OrderApi__Sales_Order__c = s.Id,
			                                      OrderApi__Entity__c = 'Contact',
			                                      OrderApi__Status__c = 'Overdue',
			                                      OrderApi__Posted_Date__c = System.today() - 5,
			                                      OrderApi__Contact__c = con.Id));
		}
		insert salesOrderLines;
		insert invoices;
		List<OrderApi__Invoice_Line__c> invoiceLines = new List<OrderApi__Invoice_Line__c> ();
		for (Integer i = 0; i<invoices.size(); i++) {
			invoiceLines.add(new OrderApi__Invoice_Line__c(OrderApi__Invoice__c = invoices[i].Id,
			                                               OrderApi__Sale_Price__c = salesOrderLines[i].OrderApi__Sale_Price__c,
			                                               OrderApi__Total__c = salesOrderLines[i].OrderApi__Total__c,
			                                               OrderApi__Sales_Order_Line__c = salesOrderLines[i].Id));
		}
		insert invoiceLines;
	}

	@IsTest
	static void testBulkInvoiceWriteOffCTRL() {
		Test.startTest();
		String postedFrom = Datetime.now().addDays(-10).format('yyyy-MM-dd');
		String postedTo = Datetime.now().format('yyyy-MM-dd');
		String query = '{"memberStatus":["Active","Inactive"],"invoiceStatus":["Posted","Overdue"],"searchKey":"Test Contact","postedFrom":"'+postedFrom+'","postedTo":"'+postedTo+'","pageNumber":1,"pageSize":5,"sortBy":"Name","sortOrder":"ASC"}';
		CPAO_BulkInvoiceWriteOffCTRL.InvoiceQueryResponse response = CPAO_BulkInvoiceWriteOffCTRL.getInvoices(query);
		System.assertEquals(response.invoices.size(), 5);
		System.assertEquals(response.totalRecords, 10);
		List<Id> invoiceIDs = new List<Id>();
		for(OrderApi__Invoice__c invoice:response.invoices) {
			invoiceIDs.add(invoice.Id);
		}
		CPAO_BulkInvoiceWriteOffCTRL.writeOffInvoices(invoiceIDs);
		List<AsyncApexJob> jobs = CPAO_BulkInvoiceWriteOffCTRL.getPendingJobs();
		Test.stopTest();
		System.assertEquals(jobs.size(), 1);
	}
}