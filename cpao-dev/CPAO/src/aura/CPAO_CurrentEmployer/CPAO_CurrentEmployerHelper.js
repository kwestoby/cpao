({
	showToast : function(title, message, type) {
	    var toastEvent = $A.get("e.force:showToast");
	    toastEvent.setParams({
	        "title": title,
	        "message": message,
	        "type" : type
	    });
	    toastEvent.fire();
	},
	
	refreshListOfEvents : function(component, event, response){
		if(response.getReturnValue().CPAO_End_Date__c!=undefined){
	      			response.getReturnValue().CPAO_End_Date__c = response.getReturnValue().CPAO_End_Date__c.substring(0,10);

	    }
	    if(response.getReturnValue().CPAO_Start_Date__c!=undefined){
	      			response.getReturnValue().CPAO_Start_Date__c = response.getReturnValue().CPAO_Start_Date__c.substring(0,10);

	    }    
	    var relationshipJSON = JSON.stringify(response.getReturnValue());
        var refreshListEvent = component.getEvent("UpdateRelationshipEvent");
        //console.log(relationshipJSON);
        refreshListEvent.setParams({
		    relationship: relationshipJSON
		}).fire();
	},

	// @Author : Halima Dosso
	// This method updates a CPAO_Account_Contact_Relationship__c record by calling  server side action
	// @updateEmployerAndChangePrimary from class @CPAO_EmployerInfoController
	//This method updates a non primary employer that is now updated to become primary
	// If the apex method updates succesfully, the method shows a success message to the end user.
	// Otherwise, it shows an error message
	updatePrimaryEmployer: function(component,event){
		var action= component.get("c.updateEmployerAndChangePrimary");
			action.setParams({
				"relationship": component.get('v.accountContactRel')
			});
			action.setCallback(this, function(response){
				var state = response.getState();
				if (state === "SUCCESS") {
					component.set("v.accountContactRel.CPAO_Primary_Employer__c",'Yes');
				    component.set('v.severity','confirm');
	        		component.set("v.confirmationMessage","The employer have been updated.");
	        		component.set("v.showEditableForm",false);
	        		var editButton=component.find('editButton');
					$A.util.removeClass(editButton, 'slds-hide'); 
					component.set("v.listOfRelationships", response.getReturnValue().relationships); 
	        		this.refreshListOfEvents(component, event, response);					
				}else if(state == "ERROR"){
					component.set('v.severity','error');
	       		    component.set("v.confirmationMessage","There was an error here.");
	       		     var errors=response.getError();
                      var message = errors[0];
                      
                      console.log(message);
				}
			});
			$A.enqueueAction(action);
	},
	
	// @Author : Halima Dosso
	// This method updates a CPAO_Account_Contact_Relationship__c record by calling  server side action
	// @updateEmployerAndChangePrimary in class CPAO_EmployerInfoController
	// This method updates some information on a non primary employer
	// If the apex method updates succesfully, the method shows a success message to the end user.
	// Otherwise, it shows an error message
	updateSecondaryEmployer : function(component, event){
  	    console.log(component.get("v.accountContactRel"));
	    var action = component.get("c.SaveAccountContactRelationships");
	    action.setParams({
	      "relationship": component.get("v.accountContactRel")
	      });  
	    action.setCallback(this, function(response){
	      	var state = response.getState();
	      	console.log(state);
	        if (state === "SUCCESS") {
	      		console.log(response.getReturnValue());
	      		if(response.getReturnValue().CPAO_End_Date__c!=undefined){
	      			response.getReturnValue().CPAO_End_Date__c = response.getReturnValue().CPAO_End_Date__c.substring(0,10);

	      		}
	      		if(response.getReturnValue().CPAO_Start_Date__c!=undefined){
	      			response.getReturnValue().CPAO_Start_Date__c = response.getReturnValue().CPAO_Start_Date__c.substring(0,10);

	      		}
	       		component.set("v.accountContactRel", response.getReturnValue());
	            component.set('v.severity','confirm');
	      	    component.set("v.confirmationMessage","The employer have been updated.");
	      	    component.set("v.showEditableForm",false);	
	      	    var editButton=component.find('editButton');
				$A.util.removeClass(editButton, 'slds-hide');        		
				component.set("v.listOfRelationships", response.getReturnValue().relationships);
	      	    this.refreshListOfEvents(component, event, response);
	        }else{
	      	 	var errors = action.getError();
				console.error (errors);
	            component.set('v.severity','error');
	            component.set("v.confirmationMessage","There was an error.");
	             var errors=response.getError();
                      var message = errors[0];
                      
                      console.log(message);
	       }

	    });
	    $A.enqueueAction(action);
    }

})