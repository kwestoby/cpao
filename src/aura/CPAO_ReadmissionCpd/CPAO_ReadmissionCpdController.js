({
	doAction : function(component, event, helper) {
		console.log('doAction starts');
		var app = component.get("v.application");

		if(app.CPAO_CPD_Compliance__c != undefined || app.CPAO_CPD_Compliance__c != null){
        	var checkedComp = component.find(app.CPAO_CPD_Compliance__c).set("v.checked", "checked");
        }

        var map = JSON.parse(app.CPAO_JSON_File_Map__c);
        if(map['cpdDocument'] != undefined){
            if(map['cpdDocument']['Name'] != undefined){
                component.set('v.fileName',map['cpdDocument']['Name']);
            }
        }        
	},

	onRadioChange : function(component, event, helper) {
		var newVal = event.getSource().get('v.value');
		var app = component.get("v.application");
		app.CPAO_CPD_Compliance__c = newVal;
		component.set("v.application", app);
	},

	clickOnNextButton : function(component,event,helper){
    	var talkingToComponent = event.getParam("componentName");
    	if(talkingToComponent == 'step5'){
    		var app = component.get("v.application");
    		if(app.CPAO_CPD_Compliance__c == 'I do not comply with CPD Requirements' && (
    			app.CPAO_Explanation_for_deficiency__c == '' || app.CPAO_Explanation_for_deficiency__c == undefined 
    			|| !app.CPAO_Explanation_for_deficiency__c.replace(/\s/g, '').length)){
    			helper.methodFailure(component, event, helper, "please fill in Additional Notes section", "Error");
                return;
    		}
    		var filename = component.get('v.fileName');
    		if(filename == undefined ||  !filename.replace(/\s/g, '').length){
    			helper.methodFailure(component, event, helper, "please provide an attachment", "Error");
    		}
    		var apexAction = component.get('c.saveCpd');
	        apexAction.setParams({
                "fileName" : filename,
                "base64Data" : encodeURIComponent(component.get('v.fileContents')), 
                "contentType" : component.get("v.fileType"),
		    	"currentReadmissionApp": app
		    });
	        apexAction.setCallback(this, function(response) {
	        	var state = response.getState();
	        	console.log('state');
	        	console.log(state);
	        	if(state === "SUCCESS"){
	        		var result = response.getReturnValue();
	        		console.log('result');
	        		console.log(result);
	                if(result.includes('Success')){
	                	var actionToTake = event.getParam("nextStepName");
                        var applicationHasChanged = component.getEvent("applicationHasChanged");
                        component.set('v.fileContents',null);
                        component.set('v.fileType',null);
                        applicationHasChanged.setParams({
                            "nextButtonClickeable" : true,
                            "action" : actionToTake});
				        applicationHasChanged.fire();
	                } else {
                        helper.methodFailure(component, event, helper);
	                }                
	            } else {
                    helper.methodFailure(component, event, helper);
                }
	        });
	        $A.enqueueAction(apexAction);
    	}        
    },

    handleFilesChange: function (component, event,helper) {
    	console.log('file input start');
        var fileInput = event.getSource().get("v.files")[0];
        component.set('v.fileInput',fileInput);
        component.set('v.fileName',fileInput.name);
        component.set('v.SpecialConsiderationName',fileInput.name);
        component.set('v.fileType',fileInput.type);
        
        var fr = new FileReader();
        var self = this;
        
        fr.onload = function() {
            var fileContents = fr.result;
            var base64Mark = 'base64,';
            var dataStart = fileContents.indexOf(base64Mark) + base64Mark.length;
            fileContents = fileContents.substring(dataStart);
            component.set('v.fileContents',fileContents);
        };
        
        fr.readAsDataURL(fileInput);
    },
})