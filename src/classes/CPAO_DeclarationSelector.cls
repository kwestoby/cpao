public without sharing class CPAO_DeclarationSelector {
	
	public static final String GOOD_CHARACTER = 'Good Character';

	public static List<CPAO_Declaration__c> getCleanReinstatementDecs(List<CPAO_Application__c> applicationIds) {
		return [SELECT Id, CPAO_Application__c FROM CPAO_Declaration__c 
				WHERE CPAO_Application__c IN :applicationIds AND RecordType.Name = :GOOD_CHARACTER
				AND CPAO_Criminal_Charges_or_Convictions__c = 'No' AND CPAO_Expelled_from_a_Professional_Org__c = 'No'
				AND CPAO_Guilty_of_a_Sec_or_Tax_Leg__c = 'No' AND CPAO_Made_assignment_in_bankruptcy__c = 'No'
				AND CPAO_Subject_to_Disciplinary_Decision__c = 'No' AND CPAO_Susp_Rev_of_Prof_License__c = 'No'];
	}

	public static List<CPAO_Declaration__c> getApplicationDeclaration(Id applicationId, String recordTypeName) {
		return [SELECT Id, CPAO_Criminal_Charges_or_Convictions__c, CPAO_Expelled_from_a_Professional_Org__c, 
					CPAO_Guilty_of_a_Sec_or_Tax_Leg__c, CPAO_Made_assignment_in_bankruptcy__c, 
					CPAO_Subject_to_Disciplinary_Decision__c, CPAO_Susp_Rev_of_Prof_License__c
				FROM CPAO_Declaration__c
				WHERE CPAO_Application__c = :applicationId AND RecordType.Name = :recordTypeName];
	}
}