@isTest
public class CPAO_AccountServicesTest {
    
    public static List<Account> factoryAccounts;
    public static List<Contact> factoryContacts;
    public static String LSO_BADGE_TYPE='LSO';



	public static void dataInitialize(Integer numberOfAccounts,Integer numberOfContacts,Boolean addMemberInfo){
		factoryAccounts = CPAO_TestFactory.createAccounts(numberOfAccounts);
        
        if(numberOfContacts>0){
            factoryContacts=CPAO_TestFactory.createContacts(factoryAccounts[0],numberOfContacts);
            
        }
        if(addMemberInfo){
            factoryContacts=CPAO_TestFactory.addMemberInfo(factoryContacts);
            insert factoryContacts; 

        }
		insert factoryAccounts;
	}

	@isTest static void test_EligibleToPal() {

		dataInitialize(1,0,false);
       // System.debug('taille'+factoryAccounts[0]);
        Boolean returnedValue= CPAO_AccountServices.account_potentially_eligible_to_LSO_Badge(factoryAccounts[0]);
		System.assertEquals(returnedValue,false);
		factoryAccounts[0].CPAO_Corporate_Structure__c='Professional Corporation';
        update factoryAccounts[0];
        returnedValue= CPAO_AccountServices.account_potentially_eligible_to_LSO_Badge(factoryAccounts[0]);
		System.assertEquals(returnedValue,false);
	}
    
    @isTest static void test_addLSO_badge() {

		dataInitialize(1,3,false);
        List<Id> idsOfContacts= new List<Id>();
        Set<Id> setOfContactIds= new Set<Id>();

        for (Contact contact:factoryContacts){
            idsOfContacts.add(contact.Id);
            setOfContactIds.add(contact.Id);
        }
        CPAO_AccountServices.assign_LSO_badges_to_eligible_contacts(idsOfContacts);
        List<OrderApi__Badge__c> lsoBadges = [SELECT Id,OrderApi__Contact__c,OrderApi__Badge_Type__c,OrderApi__Badge_Type__r.Name 
                                              FROM OrderApi__Badge__c
                                              WHERE OrderApi__Badge_Type__r.Name =:LSO_BADGE_TYPE AND OrderApi__Contact__c IN :setOfContactIds
                                             ];
        List<Contact> contacts = [SELECT Id,Name FROM Contact];
        for(Integer i=0;i<lsoBadges.size();i++){
            System.assertNotEquals(lsoBadges[i].OrderApi__Contact__c,contacts[i].Id);

        }
		
	}
    
    @isTest static void test_addLSO_badge2() {

		dataInitialize(1,3,true);
        List<Id> idsOfContacts= new List<Id>();
        Set<Id> setOfContactIds= new Set<Id>();

        for (Contact contact:factoryContacts){
            idsOfContacts.add(contact.Id);
            setOfContactIds.add(contact.Id);
        }
        CPAO_AccountServices.assign_LSO_badges_to_eligible_contacts(idsOfContacts);
        List<OrderApi__Badge__c> lsoBadges = [SELECT Id,OrderApi__Contact__c,OrderApi__Badge_Type__c,OrderApi__Badge_Type__r.Name 
                                              FROM OrderApi__Badge__c
                                              WHERE OrderApi__Badge_Type__r.Name =:LSO_BADGE_TYPE AND OrderApi__Contact__c IN :setOfContactIds
                                             ];
        List<Contact> contacts = [SELECT Id,Name FROM Contact];
        for(Integer i=0;i<lsoBadges.size();i++){
            System.assertEquals(lsoBadges[i].OrderApi__Contact__c,contacts[i].Id);

        }
		
	}
}