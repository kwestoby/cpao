({
	    
   upload: function(component, file, fileContents,fieldName) {
     /* var validForm = component.find('uploadField').reduce(function (validSoFar, inputCmp) {
        inputCmp.showHelpMessageIfInvalid();
        return validSoFar && inputCmp.get('v.validity').valid }, true);

      if(validForm){*/
        var action = component.get("c.saveTheFile"); 
        action.setParams({
            parentId: component.get("v.parentId"),
            fileName: component.get("v.fileName"),
            base64Data: encodeURIComponent(fileContents), 
            contentType: component.get("v.fileType"),
            fileTitle: component.get('v.fileTitle')
        });

        action.setCallback(this, function(response) {
              var state = response.getState();
              if (state === "SUCCESS") {
                
                var attachId = response.getReturnValue();
                component.set('v.currentPAL.'+fieldName,attachId);
                this.savePalApplication(component);
                /*var returnUploadIdEvent = component.getEvent("returnUploadId");
                returnUploadIdEvent.setParams({
                  "IdOfUploadedFile":response.getReturnValue()
                });
               returnUploadIdEvent.fire();
               alert('fired');*/

              }else{
                console.log('error while adding attachment'+ component.get("v.fileName") +  component.get("v.fileTitle"));

              }
           
        });
            
       
        $A.enqueueAction(action); 
  //  }
  },

  savePalApplication:function(component) {
      var action = component.get("c.upsertPalApplication"); 
  
       action.setParams({ "PALappToAdd":component.get('v.currentPAL') });
        action.setCallback(this, function(response){
        console.log('callback file upload'  );
          var state = response.getState();
         // alert(state);
          if (state === "SUCCESS") {
            component.set('v.currentPAL',response.getReturnValue());
            var applicationHasChanged = component.getEvent("applicationHasChanged");
            applicationHasChanged.setParams({
              "currentApplication": component.get("v.currentPAL"),
              "comingFromFileUpload": "true",
              "nextButtonClickeable": "true"
            });
            applicationHasChanged.fire();
          
          
          }else{
            console.log('error while adding id of file step professional experience');
          }
      });
    
      $A.enqueueAction(action);


  }
})