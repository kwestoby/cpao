public class CPAO_Limited_Application_CTRL {
    
    public static User LOGGED_USER=getLoggedUserInfo();
    public static String OPEN = 'Open';
    public static String SUBMITTED = 'Submitted';
    
    
    public static User getLoggedUserInfo() {
        return CPAO_UserSelector.getCurrentUser();
    }
    
    @AuraEnabled
    public static Wrapper createReinstatementApplication(){
        String recordTypename = CPAO_ApplicationHandler.REINSTATEMENT;
        Id reinstatementRecordTypeId = Schema.SObjectType.CPAO_Application__c.getRecordTypeInfosByName().get(recordTypename).getRecordTypeId();
        
        List<CPAO_Application__c> reinstatementApp = [Select Id FROM CPAO_Application__c WHERE RecordTypeId=:reinstatementRecordTypeId AND CPAO_Contact__c=:LOGGED_USER.ContactId AND (CPAO_Status__c=:OPEN OR CPAO_Status__c=:SUBMITTED)];
        Wrapper response = new Wrapper();
        
        if(reinstatementApp.size()<1){
            List<CPAO_Application__c> appsToInsert = new List<CPAO_Application__c>();
            CPAO_Application__c temp = new CPAO_Application__c(
            	RecordTypeId = reinstatementRecordTypeId,
                CPAO_Contact__c = LOGGED_USER.ContactId,
                CPAO_Status__c = 'Open'
            );
            response.url = temp.CPAO_Form_URL__c;
            appsToInsert.add(temp);
            
            insert appsToInsert;
            
            List<CPAO_Application__c> newApp = [Select Id, CPAO_Form_URL__c FROM CPAO_Application__c WHERE RecordTypeId=:reinstatementRecordTypeId AND CPAO_Contact__c=:LOGGED_USER.ContactId AND CPAO_Status__c=:OPEN];
            response.url = newApp[0].CPAO_Form_URL__c;
            
        }else{
            response.error = 'Application already in progress';
            
        }
        
        return response;
    }
    
    public class Wrapper{
        @AuraEnabled public String url {get; set;} 
        @AuraEnabled public String error {get; set;}
    }


}