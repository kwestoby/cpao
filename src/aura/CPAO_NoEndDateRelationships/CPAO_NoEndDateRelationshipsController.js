({
	doInit : function(component, event, helper) {

	},


	updateEmployer : function(component, event, helper) {
		console.log('here');
		console.log(component.get('v.endDate'));
			

		var startDate = component.get('v.accountContactRel.CPAO_Start_Date__c');
		var endDate = component.get('v.endDate');

	    //var endDate = component.get('v.accountContactRel.CPAO_End_Date__c');
	    if(startDate>endDate){
			var error = "Error: The start date is after the end date. Start date is: " + startDate;
			component.set("v.confirmationMessage", error);
			// var refreshListEvent = component.getEvent("UpdateRelationshipEvent");
			// refreshListEvent.setParams({
			//     type: "error",
			//     msg: error
			// }).fire();
		} else {
			component.set('v.accountContactRel.CPAO_End_Date__c',endDate);
			var action = component.get("c.SaveAccountContactRelationships");
			action.setParams({
				"relationship": component.get("v.accountContactRel")
				});  
			action.setCallback(this, function(response){
				var state = response.getState();
				var refreshListEvent = component.getEvent("UpdateRelationshipEvent");
				if (state === "SUCCESS") {
					response.getReturnValue().CPAO_End_Date__c = response.getReturnValue().CPAO_End_Date__c.substring(0,10);
					component.set("v.accountContactRel", response.getReturnValue());
					// component.set('v.severity','confirm');
					
					refreshListEvent.setParams({
					    type: "confirm",
					    msg: "Updated Successful"
					}).fire();
				}else{
					// component.set('v.severity','error');
					component.set("v.confirmationMessage", "Error updating file, please contact administrator");
					// refreshListEvent.setParams({
					//     type: "error",
					//     relationship: "Error updating file, please contact administrator"
					// }).fire();
				}

			});
				
			$A.enqueueAction(action);
		}
			
	 
	}
})