@RestResource(urlMapping='/directories/*')
global with sharing class CPAO_RESTServices {
    @HttpGet
       
    global static List<Account> getAccountById() {
        RestRequest request = RestContext.request;
        // grab the caseId from the end of the URL
        String caseId = request.requestURI.substring(
          request.requestURI.lastIndexOf('/')+1);
        List<Account> result =  [SELECT Id,Name
                        FROM Account
                        WHERE Id = :caseId];
        return result;
    
    }
}