public class CPAO_ReceiptItemUpdateHelper{

    public static void updateItemReceipts(Map<Id, OrderApi__Item__c>  itemsObjs){
        
        List<OrderApi__Business_Group__c> businessObjs = new List<OrderApi__Business_Group__c>();
   
        Map<Id,OrderApi__Business_Group__c> businessMap = New Map<Id,OrderApi__Business_Group__c>([Select Id, name from OrderApi__Business_Group__c]);
        List<OrderApi__Item_Class__c> itemclassObj = [Select Id, name from OrderApi__Item_Class__c where name =: 'Fees'];
        
        for(OrderApi__Item__c orderitem: itemsObjs.values()){
            if( orderitem.OrderApi__Item_Class__c == itemclassObj[0].Id){
                OrderApi__Business_Group__c businessGroupObj = businessMap.get(orderitem.OrderApi__Business_Group__c);
                businessGroupObj.OrderApi_Item_Name__c = orderitem.Name;
                businessObjs.add(businessGroupObj);
            }
        }
        
        if(businessObjs.size() > 0)
            update businessObjs;
        
    }
}