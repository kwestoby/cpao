<?xml version="1.0" encoding="UTF-8"?>
<CustomMetadata xmlns="http://soap.sforce.com/2006/04/metadata" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xmlns:xsd="http://www.w3.org/2001/XMLSchema">
    <label>Base</label>
    <protected>false</protected>
    <values>
        <field>CPAO_PAL_Application_Attestation__c</field>
        <value xsi:type="xsd:string">I declare that the responses and information I have provided in this application are accurate, true and complete. I understand that my responses and information are essential to CPA Ontario in considering my application and evaluating my eligibility. I also understand that any false or misleading statement contained in my application may result in my application for reinstatement being denied and be used by CPA Ontario in any proceedings respecting the validity of my application or of my status, as applicable with CPA Ontario. I agree to advise CPA Ontario of any changes to my responses and information in a timely manner. I authorize CPA Ontario to contact any professional or regulatory body to make enquires or obtain information in relation to this application. I further authorize any professional or regulatory body to release such information to CPA Ontario. I understand that my responses and information may be used or shared by CPA Ontario for regulatory purposes.
                                                                  
                                                                  I agree to comply with the Chartered Professional Accountants of Ontario Act, 2017, the Public Accounting Act, 2004, and with the bylaws, CPA Code of Professional Conduct, regulations and policies of CPA Ontario, as each of them may be amended or replaced from time to time.</value>
    </values>
</CustomMetadata>
