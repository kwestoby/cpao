trigger CPAO_AccountContactRelationship on CPAO_Account_Contact_Relationship__c (after insert, after update) {
	
	CPAO_Automation_Control__c control = CPAO_Automation_Control__c.getOrgDefaults();
	if(!control.CPAO_Account_Contact_Relationship_c__c){
		return;
	}
	
	//if(Trigger.isBefore && Trigger.isInsert){
	//}
	//if(Trigger.isBefore && Trigger.isUpdate){
	//}

	if(Trigger.isAfter && Trigger.isInsert){
		CPAO_AccountContactRelationship.filterAfterInsert(Trigger.New);
	}
	if(Trigger.isAfter && Trigger.isUpdate){
		CPAO_AccountContactRelationship.filterAfterUpdates(Trigger.New, Trigger.oldMap);
	}
}