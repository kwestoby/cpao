<?xml version="1.0" encoding="UTF-8"?>
<CustomMetadata xmlns="http://soap.sforce.com/2006/04/metadata" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xmlns:xsd="http://www.w3.org/2001/XMLSchema">
    <label>Second</label>
    <protected>false</protected>
    <values>
        <field>First_Year__c</field>
        <value xsi:type="xsd:string">2019</value>
    </values>
    <values>
        <field>Second_Year__c</field>
        <value xsi:type="xsd:string">2020</value>
    </values>
    <values>
        <field>Third_Year__c</field>
        <value xsi:type="xsd:string">2021</value>
    </values>
</CustomMetadata>
