public class CPAO_InvoiceSelector {

	public static Map<Id, Set<Id>> getInvoiceSubscriptionSet(Set<Id> invoiceIds) {
		System.debug('CPAO_InvoiceSelector::getInvoiceSubscriptionSet()');
		List<OrderApi__Invoice_Line__c> invoiceLines = [SELECT Id, OrderApi__Invoice__c, OrderApi__Subscription__c
		                                                FROM OrderApi__Invoice_Line__c
		                                                WHERE OrderApi__Invoice__c IN :invoiceIds];
		Map<Id, Set<Id>> resultMap = new Map<Id, Set<Id>> ();
		// Initialize the result map.
		for (Id invoiceId : invoiceIds) {
			resultMap.put(invoiceId, new Set<Id> ());
		}
		for (OrderApi__Invoice_Line__c invLine : invoiceLines) {
			if (invLine.OrderApi__Subscription__c != null) {
				resultMap.get(invLine.OrderApi__Invoice__c).add(invLine.OrderApi__Subscription__c);
			}
		}
		return resultMap;
	}
}