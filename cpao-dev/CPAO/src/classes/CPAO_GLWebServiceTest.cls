@isTest
public class CPAO_GLWebServiceTest {
	@TestSetup
	static void setupData() {
		CPAO_Integration_Settings__c intSettings = CPAO_Integration_Settings__c.getOrgDefaults();
		intSettings.CPAO_Transaction_Line_Batch_Number__c = 1;
		upsert intSettings;
		OrderApi__Business_Group__c businessGroup = new OrderApi__Business_Group__c(Name = 'Test Business Group');
		insert businessGroup;
		OrderApi__GL_Account__c glAcc = new OrderApi__GL_Account__c(Name = 'Test', CPAO_GL_Code__c = '000-000-0000', OrderApi__Business_Group__c = businessGroup.Id);
		insert glAcc;
		OrderApi__Transaction__c tx = new OrderApi__Transaction__c(OrderApi__Business_Group__c = businessGroup.Id);
		insert tx;
		List<OrderApi__Transaction_Line__c> lines = new List<OrderApi__Transaction_Line__c> ();
		for (Integer i = 0; i< 10; i++) {
			lines.add(new OrderApi__Transaction_Line__c(OrderApi__Transaction__c = tx.Id,
			                                            OrderApi__GL_Account__c = glAcc.Id,
			                                            CPAO_Batch_Number__c = 1,
			                                            OrderApi__Debit__c = 25 + i,
			                                            OrderApi__Credit__c = 10 + i));
		}
		insert lines;
	}

	@isTest
	static void getGLTransactionsTest() {
		Test.startTest();
		List<CPAO_GLWebService.GLTxDetail> txList = CPAO_GLWebService.getGLTransactions();
		Test.stopTest();
	}
}