public without sharing class CPAO_ApplicationSelector {
	public static List<CPAO_Application__c> getAllUserApplications() {
		User loggedUser = CPAO_UserSelector.getCurrentUser();
		return [SELECT Id, Name, CPAO_Application_Type__c, CPAO_Status__c
				FROM CPAO_Application__c
				WHERE CPAO_Contact__c=:loggedUser.ContactId];
	}
	
	public static List<CPAO_Application__c> getUserApplicationsWithStatuses(List<String> statuses) {
		User loggedUser = CPAO_UserSelector.getCurrentUser();
		return [SELECT Id, Name, CPAO_Application_Type__c, CPAO_Status__c, CPAO_Form_URL__c, RecordType.Name
				FROM CPAO_Application__c
				WHERE CPAO_Contact__c=:loggedUser.ContactId AND CPAO_Status__c IN :statuses];
	}
	public static List<CPAO_Application__c> getAllNonAMDApplication(List<String> statuses) {
		User loggedUser = CPAO_UserSelector.getCurrentUser();
		return [SELECT Id, Name, CPAO_Application_Type__c, CPAO_Status__c, CPAO_Form_URL__c, RecordType.Name
				FROM CPAO_Application__c
				WHERE CPAO_Contact__c=:loggedUser.ContactId AND CPAO_Status__c IN :statuses AND RecordType.Name!='AMD/CPD'];
	}
	public static List<CPAO_Application__c> getUserApplicationsByRecordtypeName(List<String> recordTypeNames){
		User loggedUser = CPAO_UserSelector.getCurrentUser();
		return [SELECT Id, Name, CPAO_Application_Type__c, CPAO_Status__c, CPAO_Form_URL__c, RecordType.Name, CPAO_Late_Fee_Passed__c,
					CPAO_Sub_Type__c, CPAO_Date_Application_Appears__c, CreatedDate, CPAO_Contact__c, CPAO_AMD_Zero_Tax_Certificate__c,
					CPAO_AMD_Location_Based_Reduction__c, CPAO_AMD_Waiver__c, CPAO_Sub_Status__c
				FROM CPAO_Application__c
				WHERE CPAO_Contact__c=:loggedUser.ContactId AND 
					(CPAO_RecordTypeDeveloperName__c IN :recordTypeNames OR RecordType.Name IN :recordTypeNames)];
	}

	public static List<CPAO_Application__c> getUserApplicationsByRecordTypeNameAndSubStatus(String subStatus, String recordTypeName) {
		User loggedUser = CPAO_UserSelector.getCurrentUser();
		return [SELECT Id, CPAO_Number_of_Unverifiable_Hours_Short__c, CPAO_Number_of_Verifiable_Hours_Short__c,
						CPAO_Sub_Status__c, CPAO_Status__c, CPAO_Form_URL__c
				FROM CPAO_Application__c
				WHERE CPAO_Sub_Status__c = :subStatus 
				AND (CPAO_RecordTypeDeveloperName__c = :recordTypeName OR RecordType.Name = :recordTypeName)
				AND	CPAO_Contact__c = :loggedUser.ContactId ];
	}

	public static List<CPAO_Application__c> getUserApplicationsByRecordTypeNameAndStatus(String status, String recordTypeName) {
		User loggedUser = CPAO_UserSelector.getCurrentUser();
		return [SELECT Id, Name, CPAO_Contact__c, CPAO_Status__c, CPAO_RecordTypeDeveloperName__c, CPAO_Form_URL__c,
					CPAO_Legal_Given_Name_s__c, CPAO_Legal_Surname__c,CPAO_Effective_Date__c,
					CPAO_Role_In_Public_Accounting__c,CPAO_Firm_Professional_Corporation_BOA__c,CPAO_Firm_Professional_Corporation_Pi__c,
					CPAO_Basis_Of_Application__c,CPAO_CPD_Declaration__c,CPAO_CPD_Year1_VerifiableHours__c,
					CPAO_CPD_Year_1_Unverifiable_Hours__c,CPAO_CPD_Year2_VerifiableHours__c,CPAO_CPD_Year3_VerifiableHours__c,
					CPAO_CPD_Year_2_Unverifiable_Hours__c,CPAO_CPD_Year_3_Unverifiable_Hours__c,
					CPAO_Provincial_dues_paid_to__c, CPAO_CPA_Canada_Fee_Paid_to__c, CPAO_Employer_is_paying_AMD__c,
					CPAO_Not_a_Resident_of_Canada__c, CPAO_Not_Registered_for_GST_HST__c, CPAO_Not_Present_in_Canada__c, 
					CPAO_Do_not_Carry_Business_in_Canada__c, CPAO_AMD_Waiver__c, CPAO_Applied_for_Deferral__c,CPAO_Current_Step__c,
					CPAO_Practice_Inspection_Declaration__c,CPAO_Practice_Inspection_Date_1__c,CPAO_Practice_Inspection_Date_2__c,
					CPAO_Practice_Inspection_Date_3__c,CPAO_Practicing_Office_Address_1__c,CPAO_Practicing_Office_Address_2__c,
					CPAO_Practicing_Office_Address_3__c,CPAO_Practicing_Office_Name_1__c,CPAO_Practicing_Office_Name_2__c,
					CPAO_Practicing_Office_Name_3__c,CPAO_Professional_Experience_Declaration__c,CPAO_Special_Consideration__c,
					CPAO_Exceptional_Circumstances_Note__c,CPAO_Exceptional_Circumstances_End_Date__c,CPAO_Exceptional_Circumstances_SD__c,
					CreatedDate,CPAO_Total_Hours_Deficiency__c,CPAO_PA_Services_Deficiency__c, CPAO_Declaration__c, 
					CPAO_Other_Professional_Body__c, CPAO_Other_Extraordinary_Consequences__c,CPAO_Criminal_Charges_or_Convictions__c,
					CPAO_Explanation_for_criminal_offence__c,CPAO_Susp_Rev_of_Professional_License__c,CPAO_Explanation_for_suspension__c,
					CPAO_Subject_to_disciplinary_decision__c,CPAO_Explanation_for_discipline_decision__c,CPAO_Made_assignment_in_bankruptcy__c,
					CPAO_Explanation_for_bankruptcy__c,CPAO_Guilty_of_Security_Tax_Legislation__c,CPAO_Explanation_of_Legislation_Guilt__c,
					CPAO_Expelled_from_a_Professional_Org__c,CPAO_Explanation_for_expulsion__c, CPAO_Number_of_Unverifiable_Hours_Short__c,
					CPAO_Number_of_Verifiable_Hours_Short__c,CPAO_Good_Character_Review_Required__c,CPAO_Reference_1_CPA_Provincial_ID__c,
					CPAO_Reference_1_Name__c,CPAO_Reference_1_Address__c,CPAO_Reference_1_Phone__c,CPAO_Reference_1_Email__c, CPAO_Sub_Type__c,
					CPAO_Reference_2_CPA_Provincial_ID__c,CPAO_Reference_2_Name__c,CPAO_Reference_2_Address__c,CPAO_Reference_2_Phone__c,
					CPAO_Reference_2_Email__c,CPAO_Membership_Body_Reference_2__c,CPAO_Membership_Body_Reference_1__c, 
					CPAO_Mulitple_POAs_in_Triennial_Period__c,CPAO_Special_Consideration_File_Id__c,CPAO_Special_Consideration_File_Name__c,
					CPAO_Schedule_A_File_Id__c,CPAO_Schedule_A_File_Name__c,CPAO_Schedule_B_File_Id__c, CPAO_Registrant_Of__c,
					CPAO_Schedule_B_File_Name__c,CPAO_Schedule_C_File_Id__c,CPAO_Schedule_C_File_Name__c,CPAO_Attestation__c,CPAO_COA_Firm_Name__c,
					CPAO_COA_Firm_Name_Formula__c,CPAO_COA_Declaration__c, CPAO_Professional_Service_Work_Performed__c,
					CPAO_AMD_Zero_Tax_Certificate__c,CPAO_completedPostDesignationPAProgram__c, CPAO_AMD_Location_Based_Reduction__c,
					CPAO_Capacity_through_Firm__c, CPAO_Capacity_through_Non_firm__c, CPAO_professional_services_in_Ontario__c,
					CPAO_Capacity_through_Firm__r.Name,CPAO_PPAD_Special_Circumstances__c,CPAO_Practising_Public_Accounting_Dec__c,
					CPAO_CPD_Compliance__c, CPAO_Lost_Certificate_Declaration__c, CPAO_Revoked_for_5_years__c, CPAO_Completed_Courses_Required__c,
					CPAO_Course_Type__c, CPAO_Course_Completion_Date__c,CPAO_CPD_Special_Consideration__c, CPAO_JSON_File_Map__c,
					CPAO_Professional_Experience_Requirement__c,CPAO_Prof_Experience_Spec_Consideration__c,CPAO_Practice_Inspection_Requirement__c,
					CPAO_Name_of_Accounting_Body_1__c, CPAO_Date_Membership_Awarded_1__c, CPAO_Date_Membership_Ended_1__c,
					CPAO_Name_of_Accounting_Body_2__c, CPAO_Date_Membership_Awarded_2__c, CPAO_Date_Membership_Ended_2__c,
					CPAO_Name_of_Accounting_Body_3__c, CPAO_Date_Membership_Awarded_3__c, CPAO_Date_Membership_Ended_3__c,
					CPAO_Name_of_Accounting_Body_4__c, CPAO_Date_Membership_Awarded_4__c, CPAO_Date_Membership_Ended_4__c,CPAO_Country__c,
					CPAO_Accounting_Body__c,CPAO_Date_of_Inspection__c,CPAO_Authorization_to_contact_Reg_Body__c,RecordTypeId,CPAO_Supervisory__c,
					CPAO_Public_Accountng_Accounting_Service__c,CPAO_Other_Services__c,CPAO_Ceased_Providing_Date__c,CPAO_Firm_Structure_Change__c,
					CPAO_Audit_Engagements__c,CPAO_Review_Engagements__c,CPAO_Compilation_Engagements__c,
					CPAO_Personal_Tax_Engagements__c,CPAO_Corporate_Tax_Engagements__c,CPAO_Audit_Clients__c,
					CPAO_Audit_Clients_Reporting_Issuers__c,CPAO_Audit_Clients_Non__c,CPAO_Review_Engagement_Clients__c,
					CPAO_Compilation_Engagement_Clients__c,CPAO_Corporate_Tax_Engagement_Clients__c,CPAO_Personal_Tax_Engagement_Clients__c,
					CPAO_CPAB_Registrant__c,CPAO_CPAB_Registration_Date__c,CPAO_PCAOB_Registrant__c, CPAO_PCAOB_Registration_Date__c,
					CPAO_Annual_Practitioner_Fee_Declaration__c, CPAO_Sales_Order__c, CPAO_Application_Type__c
				FROM CPAO_Application__c 
				WHERE CPAO_Status__c = :status 
				AND (CPAO_RecordTypeDeveloperName__c = :recordTypeName OR RecordType.Name = :recordTypeName)
				AND	CPAO_Contact__c = :loggedUser.ContactId];
	}

	public static List<CPAO_Application__c> getUserPoaDeclarations() {
		User loggedUser = CPAO_UserSelector.getCurrentUser();
		return [SELECT Id, CPAO_Status__c, CPAO_Form_URL__c, CPAO_Compliance_Due_Date__c, RecordType.Name, 
						CPAO_Year_of_Registration__c
				FROM CPAO_Application__c 
				WHERE CPAO_Status__c != 'Open' AND CPAO_Sub_Status__c = 'POA Compliance Pending' AND CPAO_RecordTypeDeveloperName__c = 'CPAO_AMD_CPD'
				AND	CPAO_Contact__c = :loggedUser.ContactId ];
	}	

	public static List<CPAO_Application__c> getUserPreviousFinancialHardshipAMD() {
		User loggedUser = CPAO_UserSelector.getCurrentUser();
		return [SELECT Id
				FROM CPAO_Application__c 
				WHERE CPAO_Status__c != 'Open' AND CPAO_RecordTypeDeveloperName__c = 'CPAO__CPD'
				AND	CPAO_Contact__c = :loggedUser.ContactId AND CPAO_AMD_Waiver__c = :CPAO_AMDRenewalContainerCTRL.FINANCIAL_HARDSHIP];
	}

	public static List<CPAO_Application__c> getUserPreviousApplicationsByRecordTypeName(String recordTypeName) {
		User loggedUser = CPAO_UserSelector.getCurrentUser();
		return [SELECT Id, Name, CPAO_Contact__c, CPAO_Status__c, CPAO_RecordTypeDeveloperName__c,
					CPAO_Legal_Given_Name_s__c, CPAO_Legal_Surname__c
				FROM CPAO_Application__c 
				WHERE CPAO_Status__c != :'Open' 
				AND (CPAO_RecordTypeDeveloperName__c = :recordTypeName OR RecordType.Name = :recordTypeName)
				AND	CPAO_Contact__c = :loggedUser.ContactId];
	}

	public static List<CPAO_Application__c> getInfractionApplications(List<CPAO_Infraction__c> applicationInfractions) {
		return [SELECT Id, CPAO_Infraction__r.CPAO_Infraction_Type__r.Name, CPAO_Infraction__r.Name,
					(SELECT Id, Name FROM Attachments)
				FROM CPAO_Application__c 
				WHERE CPAO_Infraction__c IN :applicationInfractions];
	}

	public static List<CPAO_Application__c> getAppsByStatusAndType(String status, String appType) {
		return [SELECT Id, Name, CPAO_Contact__c
				FROM CPAO_Application__c 
				WHERE CPAO_Status__c = :status 
				AND	CPAO_Application_Type__c LIKE :appType];
	}

	//public static List<CPAO_Application__c> getApplicationsWithContactInfo(List<CPAO_Application__c> applications) {
	//	return [SELECT Id, CPAO_CPA_Canada_Fee_Paid_to__c, CPAO_Provincial_dues_paid_to__c, CPAO_Contact__r.CPAO_AMD_Category__c,
	//			CPAO_Contact__r.MailingCountry
	//			FROM CPAO_Application__c
	//			WHERE Id IN :applications];
	//} 

	public static List<RecordType> getReinstatementApplicationRecordType() {
		return [SELECT Id,DeveloperName FROM RecordType 
		WHERE DeveloperName= 'CPAO_Reinstatement_Application'];
	}
    public static List<RecordType> getFMTApplicationRecordType() {
		return [SELECT Id,DeveloperName FROM RecordType 
		WHERE DeveloperName= 'CPAO_Firm_Registration'];
    }
    public static List<RecordType> getMRAApplicationRecordType() {
		return [SELECT Id,DeveloperName FROM RecordType 
		WHERE DeveloperName= 'MRA_Application'];
	}
	public static List<RecordType> getPALApplicationRecordType() {
		return [SELECT Id,DeveloperName FROM RecordType 
		WHERE DeveloperName= 'CPAO_PAL_Application'];
	}
	
	public static List<RecordType> getCOAApplicationRecordType() {
		return [SELECT Id,DeveloperName FROM RecordType 
		WHERE DeveloperName= 'CPAO_COA_Application'];
	}
    
    public static List<RecordType> getAMDCPDApplicationRecordType() {
		return [SELECT Id,DeveloperName FROM RecordType 
		WHERE DeveloperName= 'CPAO_AMD_CPD'];
	}

	public static List<RecordType> getInfractionApplicationRecordType() {
		return [SELECT Id,DeveloperName FROM RecordType 
		WHERE DeveloperName= 'CPAO_Infraction_Dummy_Application'];
	}

	public static List<CPAO_Application__c> getApplicationById(set<Id> applicationsIds){
		return [SELECT Id FROM CPAO_Application__c 
		WHERE Id In: applicationsIds ];
	}

	public static List<RecordType> getRecordType(String developerName){
		return [SELECT Id,DeveloperName FROM RecordType 
				WHERE DeveloperName =: developerName];
	}

	public static List<CPAO_Application__c> getUserApplications(String recordTypeName, set<Integer> setOfYears,String status) {
		User loggedUser = CPAO_UserSelector.getCurrentUser();
		return [SELECT Id,Name, CPAO_Contact__c, CPAO_Status__c, CPAO_RecordTypeDeveloperName__c, CPAO_Form_URL__c,
					CPAO_Legal_Given_Name_s__c, CPAO_Legal_Surname__c,CPAO_Effective_Date__c,
					CPAO_Role_In_Public_Accounting__c,CPAO_Firm_Professional_Corporation_BOA__c,CPAO_Firm_Professional_Corporation_Pi__c,
					CPAO_Basis_Of_Application__c,CPAO_CPD_Declaration__c,CPAO_CPD_Year1_VerifiableHours__c,
					CPAO_CPD_Year_1_Unverifiable_Hours__c,CPAO_CPD_Year2_VerifiableHours__c,CPAO_CPD_Year3_VerifiableHours__c,
					CPAO_CPD_Year_2_Unverifiable_Hours__c,CPAO_CPD_Year_3_Unverifiable_Hours__c,
					CPAO_Provincial_dues_paid_to__c, CPAO_CPA_Canada_Fee_Paid_to__c, CPAO_Employer_is_paying_AMD__c,
					CPAO_Not_a_Resident_of_Canada__c, CPAO_Not_Registered_for_GST_HST__c, CPAO_Not_Present_in_Canada__c, 
					CPAO_Do_not_Carry_Business_in_Canada__c, CPAO_AMD_Waiver__c, CPAO_Applied_for_Deferral__c,CPAO_Current_Step__c,
					CPAO_Practice_Inspection_Declaration__c,CPAO_Practice_Inspection_Date_1__c,CPAO_Practice_Inspection_Date_2__c,
					CPAO_Practice_Inspection_Date_3__c,CPAO_Practicing_Office_Address_1__c,CPAO_Practicing_Office_Address_2__c,
					CPAO_Practicing_Office_Address_3__c,CPAO_Practicing_Office_Name_1__c,CPAO_Practicing_Office_Name_2__c,
					CPAO_Practicing_Office_Name_3__c,CPAO_Professional_Experience_Declaration__c,CPAO_Special_Consideration__c,
					CPAO_Exceptional_Circumstances_Note__c,CPAO_Exceptional_Circumstances_End_Date__c,CPAO_Exceptional_Circumstances_SD__c,
					CreatedDate,CPAO_Total_Hours_Deficiency__c,CPAO_PA_Services_Deficiency__c, CPAO_Declaration__c, 
					CPAO_Other_Professional_Body__c, CPAO_Other_Extraordinary_Consequences__c,CPAO_Criminal_Charges_or_Convictions__c,
					CPAO_Explanation_for_criminal_offence__c,CPAO_Susp_Rev_of_Professional_License__c,CPAO_Explanation_for_suspension__c,
					CPAO_Subject_to_disciplinary_decision__c,CPAO_Explanation_for_discipline_decision__c,CPAO_Made_assignment_in_bankruptcy__c,
					CPAO_Explanation_for_bankruptcy__c,CPAO_Guilty_of_Security_Tax_Legislation__c,CPAO_Explanation_of_Legislation_Guilt__c,
					CPAO_Expelled_from_a_Professional_Org__c,CPAO_Explanation_for_expulsion__c, CPAO_Number_of_Unverifiable_Hours_Short__c,
					CPAO_Number_of_Verifiable_Hours_Short__c,CPAO_Good_Character_Review_Required__c,CPAO_Reference_1_CPA_Provincial_ID__c,
					CPAO_Reference_1_Name__c,CPAO_Reference_1_Address__c,CPAO_Reference_1_Phone__c,CPAO_Reference_1_Email__c, CPAO_Sub_Type__c,
					CPAO_Reference_2_CPA_Provincial_ID__c,CPAO_Reference_2_Name__c,CPAO_Reference_2_Address__c,CPAO_Reference_2_Phone__c,
					CPAO_Reference_2_Email__c,CPAO_Membership_Body_Reference_2__c,CPAO_Membership_Body_Reference_1__c, 
					CPAO_Mulitple_POAs_in_Triennial_Period__c,CPAO_Special_Consideration_File_Id__c,CPAO_Special_Consideration_File_Name__c,
					CPAO_Schedule_A_File_Id__c,CPAO_Schedule_A_File_Name__c,CPAO_Schedule_B_File_Id__c, CPAO_Registrant_Of__c,
					CPAO_Schedule_B_File_Name__c,CPAO_Schedule_C_File_Id__c,CPAO_Schedule_C_File_Name__c,CPAO_Attestation__c,CPAO_COA_Firm_Name__c,
					CPAO_COA_Firm_Name_Formula__c,CPAO_COA_Declaration__c, CPAO_Professional_Service_Work_Performed__c,
					CPAO_AMD_Zero_Tax_Certificate__c,CPAO_completedPostDesignationPAProgram__c, CPAO_AMD_Location_Based_Reduction__c,
					CPAO_Capacity_through_Firm__c, CPAO_Capacity_through_Non_firm__c, CPAO_professional_services_in_Ontario__c,
					CPAO_Capacity_through_Firm__r.Name,CPAO_PPAD_Special_Circumstances__c,CPAO_Practising_Public_Accounting_Dec__c,
					CPAO_CPD_Compliance__c, CPAO_Lost_Certificate_Declaration__c, CPAO_Revoked_for_5_years__c,CPAO_CPD_Special_Consideration__c,
					CPAO_Professional_Experience_Requirement__c,CPAO_Prof_Experience_Spec_Consideration__c
				FROM CPAO_Application__c 
				WHERE CPAO_RecordTypeDeveloperName__c = :recordTypeName AND CPAO_Contact__c =: loggedUser.ContactId 
						AND CALENDAR_YEAR(CPAO_Submission_Date__c)  IN :setOfYears AND CPAO_Status__c=:status];
	}
	public static List<CPAO_Application__c> getApplicationByRecordTypeAndDecisionDate(String recordTypeName,Date decisionDate,String basisOfApp){
		User loggedUser = CPAO_UserSelector.getCurrentUser();
		return [SELECT Id,CPAO_RecordTypeDeveloperName__c,CPAO_Basis_Of_Application__c,CPAO_PAL_COA_Decision_Date__c,CPAO_Decision__c
				FROM CPAO_Application__c 
				WHERE CPAO_RecordTypeDeveloperName__c = :recordTypeName AND CPAO_Contact__c =: loggedUser.ContactId 
					  AND CPAO_Basis_Of_Application__c=:basisOfApp AND CPAO_PAL_COA_Decision_Date__c>= :decisionDate];
	}

	
}