/**
 * @author Sachet Khanal (sakhanal@deloitte.com)
 * @version 1.0.0
 * @description Controller class for CPAO_OfflinePaymentType lightning component
 *
 *
 **/
public class CPAO_OfflinePaymentTypeCtrl {

	/**
	 * @description Queries and returns Instruction records for offline payments.
	 * @return CPAO_Instruction__c Instruction record containing details about instructions for offline payment.
	 **/
	@AuraEnabled
	public static CPAO_Instruction__c getOfflineInstructions() {
		return CPAO_InstructionSelector.getInstructions(CPAO_InstructionSelector.OFFLINE_PAYMENT);
	}

	/**
	 * @description Updates sales order records for offline payment confirmation.
	 * @param paymentId Id EPayment ID or SalesOrder ID used to identify sales order records to update.
	 **/
	@AuraEnabled
	public static void confirmOfflinePayment(Id paymentId) {
		System.debug('CPAO_OfflinePaymentTypeCtrl::confirmOfflinePayment()');
		Set<String> validStatuses = new Set<String> { 'Open', 'In Progress' };
		if (paymentId.getSobjectType() == OrderApi__EPayment__c.SObjectType) {
			Map<Id, OrderApi__Sales_Order__c> salesOrderToUpdate = new Map<Id, OrderApi__Sales_Order__c> ();
			Map<Id, CPAO_Application__c> appsToUpdate = new Map<Id, CPAO_Application__c> ();
			List<OrderApi__EPayment_Line__c> paymentLines = [SELECT Id, OrderApi__Invoice__r.OrderApi__Sales_Order__c,
			                                                 OrderApi__Invoice__r.OrderApi__Sales_Order__r.CPAO_Application__c,
			                                                 OrderApi__Invoice__r.OrderApi__Sales_Order__r.CPAO_Application__r.CPAO_Status__c
			                                                 FROM OrderApi__EPayment_Line__c
			                                                 WHERE OrderApi__EPayment__c = :paymentId
			                                                 AND OrderApi__Invoice__r.OrderApi__Sales_Order__c != null];

			for (OrderApi__EPayment_Line__c line : paymentLines) {
				salesOrderToUpdate.put(line.OrderApi__Invoice__r.OrderApi__Sales_Order__c, new OrderApi__Sales_Order__c(Id = line.OrderApi__Invoice__r.OrderApi__Sales_Order__c, CPAO_Pay_Offline__c = true));
				if (line.OrderApi__Invoice__r.OrderApi__Sales_Order__r.CPAO_Application__c != null && validStatuses.contains(line.OrderApi__Invoice__r.OrderApi__Sales_Order__r.CPAO_Application__r.CPAO_Status__c)) {
					appsToUpdate.put(line.OrderApi__Invoice__r.OrderApi__Sales_Order__r.CPAO_Application__c, new CPAO_Application__c(Id = line.OrderApi__Invoice__r.OrderApi__Sales_Order__r.CPAO_Application__c, CPAO_Status__c = 'Submitted not paid'));
				}
			}
			update salesOrderToUpdate.values();
			update appsToUpdate.values();
		} else if (paymentId.getSobjectType() == OrderApi__Sales_Order__c.SObjectType) {
			OrderApi__Sales_Order__c salesOrder = [SELECT Id, CPAO_Application__c, CPAO_Application__r.CPAO_Status__c
			                                       FROM OrderApi__Sales_Order__c
			                                       WHERE Id = :paymentId];
			salesOrder.CPAO_Pay_Offline__c = true;
			update salesOrder;
			if (salesOrder.CPAO_Application__c != null && validStatuses.contains(salesOrder.CPAO_Application__r.CPAO_Status__c)) {
				CPAO_Application__c app = new CPAO_Application__c(Id = salesOrder.CPAO_Application__c, CPAO_Status__c = 'Submitted not paid');
				update app;
			}
		}
	}
}