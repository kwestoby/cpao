public without sharing class CPAO_poaPageCTRL {
	
	@AuraEnabled
	public static poaInitWrapper getPOAinfo() {
		poaInitWrapper results = new poaInitWrapper();
		List<CPAO_Application__c> currentApplication = 
				CPAO_ApplicationSelector.getUserApplicationsByRecordTypeNameAndSubStatus('POA Pending', CPAO_ApplicationHandler.AMD_CPD);
		if(!currentApplication.isEmpty()){
			results.application = currentApplication[0];
		} else {
			return null;
		}

		results.poaItems = CPAO_PlanOfActionSelector.getPOAsByApplicationId(results.application.Id);
		results.poa = new Plan_of_Action_Items__c();
		results.communityURL = System.Label.Community_URL;
		return results;
	}

	@AuraEnabled
	public static newPoareturnWrapper saveNewPOA(Plan_of_Action_Items__c newPOA, Id appId) {
		newPoareturnWrapper results = new newPoareturnWrapper();
		try{
			newPOA.CPAO_Application__c = appId;
			newPOA.CPAO_Type__c = 'Plan of Action Item';
			insert newPOA;
		} catch(Exception e){
			results.msg = e.getMessage();
			return results;
		}
		results.poaItems = CPAO_PlanOfActionSelector.getPOAsByApplicationId(appId);

		results.msg = 'Success';
		results.poa = new Plan_of_Action_Items__c();
		return results;
	}

	@AuraEnabled
	public static newPoareturnWrapper saveEditPOA(Plan_of_Action_Items__c editPOA, Id appId) {
		newPoareturnWrapper results = new newPoareturnWrapper();
		try{
			update editPOA;
		} catch(Exception e){
			results.msg = e.getMessage();
			return results;
		}
		results.poaItems = CPAO_PlanOfActionSelector.getPOAsByApplicationId(appId);

		results.msg = 'Success';
		results.poa = new Plan_of_Action_Items__c();
		return results;
	}

	@AuraEnabled
	public static newPoareturnWrapper deletePOA(Id poaId, Id appId) {
		newPoareturnWrapper results = new newPoareturnWrapper();
		try{
			Plan_of_Action_Items__c poaToDelete = new Plan_of_Action_Items__c(Id = poaId);
			delete poaToDelete;
		} catch(Exception e) {
			results.msg = e.getMessage();
			return results;
		}
		results.poaItems = CPAO_PlanOfActionSelector.getPOAsByApplicationId(appId);

		results.msg = 'Success';
		return results;
	}
	

	@AuraEnabled
	public static String submitPOA(CPAO_Application__c app) {

		try{
			app.CPAO_Sub_Status__c = 'POA Compliance Pending';
			app.CPAO_POA_Submission_Date__c = Date.today();
			
			update app;
		} catch(Exception e) {
			return e.getMessage();
		}
		return 'Success';
	}

	public class poaInitWrapper{
		@AuraEnabled public List<Plan_of_Action_Items__c> poaItems{get;set;}
		@AuraEnabled public CPAO_Application__c application{get;set;}
		@AuraEnabled public Plan_of_Action_Items__c poa{get;set;}
		@AuraEnabled public String communityURL{get;set;}
	}

	public class newPoareturnWrapper{
		@AuraEnabled public List<Plan_of_Action_Items__c> poaItems{get;set;}
		@AuraEnabled public String msg{get;set;}
		@AuraEnabled public Plan_of_Action_Items__c poa{get;set;}
	}
}