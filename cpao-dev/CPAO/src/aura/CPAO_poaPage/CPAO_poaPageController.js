({
	doInit : function(component, event, helper) {
		console.log('marco test');
		var apexAction = component.get('c.getPOAinfo');
        apexAction.setCallback(this, function(response) {
        	var state = response.getState();
        	console.log('state');
            console.log(state);
        	if(state === "SUCCESS"){
                var result = response.getReturnValue();
                console.log('result');
              	console.log(result);
                component.set("v.application", result.application);
                var sumVerifiedHours = 0;
                var sumUnverifiedHours = 0;
                var totalHours = 0;
				//var poaS = [];
				for(var i=0;i<result.poaItems.length;i++) {
					//poaS.push({poa:result.poaItems[i], error:""});
					sumVerifiedHours+=result.poaItems[i].CPAO_CPD_Verifiable_Hours__c;
					sumUnverifiedHours+=result.poaItems[i].CPAO_CPD_Unverifiable_Hours__c;
					totalHours+=result.poaItems[i].CPAO_Sum__c;
				}
                component.set("v.poaS", result.poaItems);
                component.set("v.sumVerifiedHours", sumVerifiedHours);
                component.set("v.sumUnverifiedHours", sumUnverifiedHours);
                component.set("v.totalHours", totalHours);
                component.set("v.newPoa", result.poa);
                component.set("v.communityHomePage", result.communityURL);

                
            }
        });
        $A.enqueueAction(apexAction);
	},

	closeModel: function(component, event, helper) {
        // for Hide/Close Model,set the "warningModalOpen" attribute to "Fasle"  
        component.set("v.warningModalOpen", false);
    },

    addLine : function(component, event, helper) {
        component.set("v.addNewLogItem", true);
    },

    cancel : function(component, event, helper) {
        var homepageURL = component.get("v.communityHomePage");
        window.location.href = homepageURL;
    },


    editLine : function(component, event, helper) {
        var index = event.getSource().get("v.name");
        var poaS = component.get("v.poaS");
        var poa = poaS[index];

        component.set("v.editLogItem", true);
        component.set("v.editPoa", poa);
    },

    saveEditItem : function(component, event, helper) {
        console.log('marco test savenew');
        var validForm = component.find('editPoaInfo').reduce(function (validSoFar, inputCmp) {
        // Displays error messages for invalid fields
        inputCmp.showHelpMessageIfInvalid();
        return validSoFar && inputCmp.get('v.validity').valid;

        }, true);
        if(validForm == false){
            helper.methodFailure(component, event, helper);
            return;
        }
        var app = component.get("v.application");
        var editPoa = component.get("v.editPoa");
        console.log(editPoa);
        console.log(editPoa.CPAO_CPD_Unverifiable_Hours__c);

        var apexAction = component.get('c.saveEditPOA');
        apexAction.setParams({
            "editPOA": editPoa,
            "appId": app.Id
        });
        helper.showSpinner(component);
        apexAction.setCallback(this, function(response) {
            var state = response.getState();
            console.log('state');
            console.log(state);
            if(state === "SUCCESS"){
                var result = response.getReturnValue();
                console.log('result');
                console.log(result);
                if(result.msg == 'Success'){
                    var sumVerifiedHours = 0;
                    var sumUnverifiedHours = 0;
                    var totalHours = 0;
                    //var poaS = [];
                    for(var i=0;i<result.poaItems.length;i++) {
                        //poaS.push({poa:result.poaItems[i], error:""});
                        sumVerifiedHours+=result.poaItems[i].CPAO_CPD_Verifiable_Hours__c;
                        sumUnverifiedHours+=result.poaItems[i].CPAO_CPD_Unverifiable_Hours__c;
                        totalHours+=result.poaItems[i].CPAO_Sum__c;
                    }
                    component.set("v.poaS", result.poaItems);
                    component.set("v.sumVerifiedHours", sumVerifiedHours);
                    component.set("v.sumUnverifiedHours", sumUnverifiedHours);
                    component.set("v.totalHours", totalHours);
                    component.set("v.editPoa", result.poa);
                    component.set("v.editLogItem", false);
                } else {
                    component.set("v.modalText", result.msg);
                    component.set("v.warningModalOpen", true);
                }
            }
            helper.hideSpinner(component);
        });
        $A.enqueueAction(apexAction);
    },

    removeItem : function(component, event, helper) {
        var index = event.getSource().get("v.name");
        var poaS = component.get("v.poaS");
        var app = component.get("v.application");
        var poa = poaS[index];
        var apexAction = component.get("c.deletePOA");
        var recordId = poa.Id;
        apexAction.setParams({
            "poaId" : recordId,
            "appId": app.Id
        });
        helper.showSpinner(component);
        apexAction.setCallback(this, function(response) {
            var state = response.getState();
            if (state === "SUCCESS") {
                var result =  response.getReturnValue();
                if (result.msg == 'Success') {
                    var sumVerifiedHours = 0;
                    var sumUnverifiedHours = 0;
                    var totalHours = 0;
                    //var poaS = [];
                    for(var i=0;i<result.poaItems.length;i++) {
                        //poaS.push({poa:result.poaItems[i], error:""});
                        sumVerifiedHours+=result.poaItems[i].CPAO_CPD_Verifiable_Hours__c;
                        sumUnverifiedHours+=result.poaItems[i].CPAO_CPD_Unverifiable_Hours__c;
                        totalHours+=result.poaItems[i].CPAO_Sum__c;
                    }
                    component.set("v.poaS", result.poaItems);
                    component.set("v.sumVerifiedHours", sumVerifiedHours);
                    component.set("v.sumUnverifiedHours", sumUnverifiedHours);
                    component.set("v.totalHours", totalHours);
                    component.set("v.editLogItem", false);
                }
            }
            helper.hideSpinner(component);
        });

        $A.enqueueAction(apexAction);

        // if (poa.Id == undefined || poa.Id == null) { 
        //     poaS.splice(index, 1);
        //     component.set("v.poaS", poaS);
        //     return;
        // } else {
        
        // }

    },

    saveNewItem : function(component, event, helper) {
        console.log('marco test savenew');
        var validForm = component.find('newPoaInfo').reduce(function (validSoFar, inputCmp) {
        // Displays error messages for invalid fields
        inputCmp.showHelpMessageIfInvalid();
        return validSoFar && inputCmp.get('v.validity').valid;

        }, true);
        if(validForm == false){
            helper.methodFailure(component, event, helper);
            return;
        }
        var app = component.get("v.application");
        var newPoa = component.get("v.newPoa");
        console.log(newPoa);
        console.log(newPoa.CPAO_CPD_Unverifiable_Hours__c);

        var apexAction = component.get('c.saveNewPOA');
        apexAction.setParams({
            "newPOA": newPoa,
            "appId": app.Id
        });
        helper.showSpinner(component);
        apexAction.setCallback(this, function(response) {
            var state = response.getState();
            console.log('state');
            console.log(state);
            if(state === "SUCCESS"){
                var result = response.getReturnValue();
                console.log('result');
                console.log(result);
                if(result.msg == 'Success'){
                    var sumVerifiedHours = 0;
                    var sumUnverifiedHours = 0;
                    var totalHours = 0;
                    //var poaS = [];
                    for(var i=0;i<result.poaItems.length;i++) {
                        //poaS.push({poa:result.poaItems[i], error:""});
                        sumVerifiedHours+=result.poaItems[i].CPAO_CPD_Verifiable_Hours__c;
                        sumUnverifiedHours+=result.poaItems[i].CPAO_CPD_Unverifiable_Hours__c;
                        totalHours+=result.poaItems[i].CPAO_Sum__c;
                    }
                    component.set("v.poaS", result.poaItems);
                    component.set("v.sumVerifiedHours", sumVerifiedHours);
                    component.set("v.sumUnverifiedHours", sumUnverifiedHours);
                    component.set("v.totalHours", totalHours);
                    component.set("v.newPoa", result.poa);
                    component.set("v.addNewLogItem", false);
                } else {
                    component.set("v.modalText", result.msg);
                    component.set("v.warningModalOpen", true);
                }
            }
            helper.hideSpinner(component);
        });
        $A.enqueueAction(apexAction);
    },


	clickOnNextButton : function(component,event,helper){

		var app = component.get("v.application");
		if(app.CPAO_Number_of_Verifiable_Hours_Short__c + app.CPAO_Number_of_Unverifiable_Hours_Short__c < 1){
            component.set("v.modalText", "Total number of hours short must be greater than 0.");
			component.set("v.warningModalOpen", true);
			return;
		}
		return;
        var poaS = component.get("v.poaS");
        var sumVerifiedHours = 0;
        var sumUnverifiedHours = 0;
        for(var i=0;i<poaS.length;i++) {
            sumVerifiedHours+=poaS[i].CPAO_CPD_Verifiable_Hours__c;
            sumUnverifiedHours+=poaS[i].CPAO_CPD_Unverifiable_Hours__c;
        }
        if(sumVerifiedHours != app.CPAO_Number_of_Verifiable_Hours_Short__c || sumUnverifiedHours != app.CPAO_Number_of_Unverifiable_Hours_Short__c){
            var msg = "Please ensure that the sum of the verifiable/unverifiable hours entered in the log equals the number of verifiable/unverifiable";
            msg = msg + " hours short that you have indicated. Please ensure that the sum of verifiable/unverifiable hours entered in the log";
            msg = msg + " matches the hours short mentioned in the application";
            component.set("v.modalText", msg);
            component.set("v.warningModalOpen", true);
            return;
        }

		var apexAction = component.get('c.submitPOA');
        apexAction.setParams({
	    	"app": app
	    });
        apexAction.setCallback(this, function(response) {
        	var state = response.getState();
        	if(state === "SUCCESS"){
        		var result = response.getReturnValue();
                if(result == 'Success'){
                    component.set("v.isComplete", true);
                	//var homepageURL = component.get("v.communityHomePage");
                    //window.location.href = homepageURL;
                } else {
                    component.set("v.modalText", result);
                    component.set("v.warningModalOpen", true);
                }                    
            } else {
                component.set("v.modalText", "System ran into error, please contact administrator");
                component.set("v.warningModalOpen", true);
            }

        });
        $A.enqueueAction(apexAction);
    },
})