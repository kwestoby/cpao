public without sharing class CPAO_UpdatePersonalinformationCTRL {

    //used in the reinstatement application process
    //this loads up the following data relevant to the user:
    //user/contact record and the application record
    // application record is only used when the user wants to their their legal name; they must then input a new legal name
    // and this gets recorded on the application
    public static final String CA = 'CA';
    public static final String US = 'US';
    public static final String SUCCESS = 'Success';
    public static Boolean insertError { get; set; }

    @AuraEnabled
    public static ReviewPageWrapper loadReviewInfo() {
        ReviewPageWrapper results = new ReviewPageWrapper();
        List<String> contactFieldsToGather = new List<String> {
            'DonorApi__Suffix__c', 'CPAO_Gender__c', 'CPAO_Preferred_Phone__c', 'CPAO_Preferred_Mailing_Address__c', 'Salutation' };
        List<String> userFieldsToGather = new List<String> { 'Countrycode', 'statecode' };

        User current = CPAO_UserSelector.getCurrentUser();
        results.userContact = CPAO_ContactSelector.getContactToUpdate(new Set<Id> { current.ContactId }) [0];

        Schema.SObjectType contactSStype = Schema.getGlobalDescribe().get('Contact');
        Schema.DescribeSObjectResult contactDescribe = contactSStype.getDescribe();

        for (String fieldName : contactFieldsToGather) {
            List<String> pickListValuesList = new List<String> ();

            for (Schema.PicklistEntry pickListVal : contactDescribe.fields.getMap().get(fieldName).getDescribe().getPicklistValues()) {
                pickListValuesList.add(pickListVal.getLabel());
            }
            if (fieldName == 'DonorApi__Suffix__c') {
                results.suffix = pickListValuesList;
            } else if (fieldName == 'CPAO_Gender__c') {
                results.gender = pickListValuesList;
            } else if (fieldName == 'CPAO_Preferred_Phone__c') {
                results.preferredPhone = pickListValuesList;
            } else if (fieldName == 'CPAO_Preferred_Mailing_Address__c') {
                results.preferredMailingAddress = pickListValuesList;
            } else if (fieldName == 'Salutation') {
                results.salutation = pickListValuesList;
            }
        }

        Schema.SObjectType userSStype = Schema.getGlobalDescribe().get('User');
        Schema.DescribeSObjectResult userDescribe = userSStype.getDescribe();

        for (String fieldName : userFieldsToGather) {
            List<String> pickListValuesList = new List<String> ();

            for (Schema.PicklistEntry pickListVal : userDescribe.fields.getMap().get(fieldName).getDescribe().getPicklistValues()) {
                pickListValuesList.add(pickListVal.getLabel());
            }
            if (fieldName == 'Countrycode') {
                results.country = pickListValuesList;
            } else if (fieldName == 'statecode') {
                results.state = pickListValuesList;
            }
        }

        return results;
    }

    @AuraEnabled
    public static String saveContactInfo(Contact contact) {
        try {
            update contact;
        } catch(Exception e) {
            return 'The following error occured ' + e;
        }
        return SUCCESS;
    }

    public class ReviewPageWrapper {
        @AuraEnabled public Contact userContact { get; set; }
        @AuraEnabled public List<String> suffix { get; set; }
        @AuraEnabled public List<String> gender { get; set; }
        @AuraEnabled public List<String> preferredPhone { get; set; }
        @AuraEnabled public List<String> preferredMailingAddress { get; set; }
        @AuraEnabled public List<String> country { get; set; }
        @AuraEnabled public List<String> state { get; set; }
        @AuraEnabled public List<String> salutation { get; set; }
    }


    //The following pieces are used in the VF Page
    public static final String OPEN = 'Open';
    public static Boolean insertNewAppli;
    public static Id contactId;
    public CPAO_UpdatePersonalinformationCTRL() {
        mailingInfoRequired = false;
        otherInfoRequired = false;
        otherAddressRequired = false;
        insertError = false;
        User current = CPAO_UserSelector.getCurrentUser();
        List<Contact> currentContact = CPAO_ContactSelector.getContactToUpdate(new Set<Id> { current.ContactId });
        changeLegalName = false;
        if (currentContact.size() != 1) {
            return;
        }
        contact = currentContact[0];
        contactId = currentContact[0].Id;

        List<CPAO_Application__c> currentReinstatementAppliation =
        CPAO_ApplicationSelector.getUserApplicationsByRecordTypeNameAndStatus(OPEN, CPAO_ApplicationHandler.REINSTATEMENT);

        if (currentReinstatementAppliation.isEmpty()) {
            insertNewAppli = true;
        } else {
            insertNewAppli = false;
            application = currentReinstatementAppliation[0];
        }
    }

    public void insertNewApplication() {
        if (insertNewAppli) {
            try {
                Id reinstatementrecordTypeId =
                Schema.SObjectType.CPAO_Application__c.getRecordTypeInfosByName().get(CPAO_ApplicationHandler.REINSTATEMENT).getRecordTypeId();
                CPAO_Application__c newReinstatementApplication = new CPAO_Application__c(
                                                                                          CPAO_Status__c = OPEN,
                                                                                          CPAO_Contact__c = contact.Id,
                                                                                          RecordTypeId = reinstatementrecordTypeId
                );
                insert newReinstatementApplication;
                application = newReinstatementApplication;
            } catch(DmlException ex) {
                System.debug(ex.getMessage());
                insertError = true;
            }
        }
    }

    public PageReference save() {
        if ((contact.OtherCountryCode == CA || contact.OtherCountryCode == US) &&
        (contact.OtherStateCode == '' || contact.OtherStateCode == null)) {
            ApexPages.addMessage(new ApexPages.Message(
                                                       ApexPages.Severity.FATAL, Label.Other_State_Province_cannot_be_left_blank_when_country_is_CA_or_US));
            return null;
        }
        if ((contact.MailingCountryCode == CA || contact.MailingCountryCode == US) &&
        (contact.MailingStateCode == '' || contact.MailingStateCode == null)) {
            ApexPages.addMessage(new ApexPages.Message(
                                                       ApexPages.Severity.FATAL, Label.Other_State_Province_cannot_be_left_blank_when_country_is_CA_or_US));
            return null;
        }
        try {
            update contact;
        } catch(exception e) {
            ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.FATAL, e.getMessage()));
            return null;
        }


        update application;

        if (attachment.body != null) {
            List<Attachment> attachmentsToInsert = new List<Attachment> ();
            attachment.OwnerId = UserInfo.getUserId();
            attachment.ParentId = application.Id;
            attachment.IsPrivate = false;
            attachmentsToInsert.add(attachment);
            if (attachment2.body != null) {
                attachment2.OwnerId = UserInfo.getUserId();
                attachment2.ParentId = application.Id;
                attachment2.IsPrivate = false;
                attachmentsToInsert.add(attachment2);
            }
            insert attachmentsToInsert;
        } else {
            if (changeLegalName) {
                ApexPages.addMessage(new ApexPages.Message(
                                                           ApexPages.Severity.FATAL, Label.Please_add_an_attachment_if_you_would_like_to_change_your_legal_name));
                return null;
            }

        }
        String stepId = Framework.PageUtils.getParam('id');
        if (!Test.isRunningTest()) {
            joinApi.JoinUtils joinUtil = new joinApi.JoinUtils(stepId);
            String redirectURL = joinUtil.navStepsMap.get('Next');
            JSONGenerator gen = JSON.createGenerator(true);
            gen.writeStartObject();
            gen.writeStringField('stepId', stepId);
            gen.writeStringField('redirectURL', redirectURL);
            gen.writeEndObject();
            joinUtil.setRedirectURLCookie(gen.getAsString());
            return new PageReference(redirectURL);
        }
        return null;
    }

    public PageReference cancelJP() {
        String stepId = Framework.PageUtils.getParam('id');
        if (!Test.isRunningTest()) {
            joinApi.JoinUtils joinUtil = new joinApi.JoinUtils(stepId);
            joinUtil.deleteCookies();
        }
        return new PageReference('/');
    }

    public Contact contact { get; set; }
    public Boolean changeLegalName { get; set; }
    public Boolean mailingInfoRequired { get; set; }
    public Boolean otherInfoRequired { get; set; }
    public Boolean otherAddressRequired { get; set; }
    public CPAO_Application__c application { get; set; }

    public Attachment attachment {
        get {
            if (attachment == null)
            attachment = new Attachment();
            return attachment;
        }
        set;
    }

    public Attachment attachment2 {
        get {
            if (attachment2 == null)
            attachment2 = new Attachment();
            return attachment2;
        }
        set;
    }

}