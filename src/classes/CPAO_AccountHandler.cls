public with sharing class CPAO_AccountHandler {
    
    public static String INACTIVE_STATUS = 'Inactive';
    public static String accountListSizeError = 'Account list size error, please contact an administrator';
    public static String idCurrentlyExists = Label.idCurrentlyExists;
    
    public static void beforeInsertFilters(List<Account> accounts) {
        list<Account> filteredAccounts = new List<Account>();
        for(Account account:accounts){
            if(account.CPAO_CPA_Id__c != null){
                filteredAccounts.add(account);
            }
        }
        if(!filteredAccounts.isEmpty()){
            modifyCPAIdDisplay(filteredAccounts);
        }
    }
    public static void beforeUpdateFilters(List<Account> accounts, Map<Id, Account> oldMap) {
        list<Account> filteredAccounts = new List<Account>();
        for(Account account:accounts){
            if(oldMap.get(account.Id).CPAO_CPA_Id__c != account.CPAO_CPA_Id__c){
                if(account.CPAO_CPA_Id__c != null){
                    filteredAccounts.add(account);
                }
            }
        }
        if(!filteredAccounts.isEmpty()){
            modifyCPAIdDisplay(filteredAccounts);
        }
    }
    
    //handles calls from insert trigger, filters and then calls methods where relevant
    public static void afterInsertFilters(List<Account> accounts) {
        List<Account> filteredAccountss = new List<Account>();
        List<Id> potential_LSOContacts = new List<Id>();
        List<Id> primaryContacts = new List<Id>();
        
        for(Account account:accounts){
            if(account.CPAO_CPA_Id__c != null){
                filteredAccountss.add(account);
            }
            
            if(CPAO_AccountServices.account_potentially_eligible_to_LSO_Badge(account)){
                potential_LSOContacts.add(account.CPAO_LSO__c);
            }
            if(account.OrderApi__Primary_Contact__c!=null ){
                primaryContacts.add(account.OrderApi__Primary_Contact__c);
            }
            
            
        }
        if(!filteredAccountss.isEmpty()){
            packageAndPassToIdClass(null, filteredAccountss, null);
        }
        if(!potential_LSOContacts.isEmpty()){
            CPAO_AccountServices.assign_LSO_badges_to_eligible_contacts(potential_LSOContacts);
            
        }
        if(!primaryContacts.isEmpty()){
            CPAO_ContactServices.assignBadgeToContacts(primaryContacts,'Primary Contact');
        }
        
    }
    
    //handles calls from update trigger, filters and then calls methods where relevant
    public static void afterUpdateFilters(List<Account> accounts, Map<Id, Account> oldMap) {
        List<Decimal> idsToBeInactivated = new List<Decimal>();
        List<Account> manualAssignAccounts = new List<Account>();
        List<Id> potential_LSOContacts= new List<Id>();
        List<Id> LSOContactsToRemove= new List<Id>();
        List<Id> primaryContacts= new List<Id>();
        List<Id> primaryContactsToRemove= new List<Id>();
        for(Account account:accounts){						
            
            if(oldMap.get(account.Id).CPAO_CPA_Id__c != account.CPAO_CPA_Id__c){
                if(account.CPAO_CPA_Id__c != null){
                    manualAssignAccounts.add(account);
                }
                if(oldMap.get(account.Id).CPAO_CPA_Id__c != null){
                    idsToBeInactivated.add(oldMap.get(account.Id).CPAO_CPA_Id__c);
                }
                
            }
            if((oldMap.get(account.Id).CPAO_LSO__c!=account.CPAO_LSO__c || 
                !CPAO_AccountServices.account_potentially_eligible_to_LSO_Badge(oldMap.get(account.Id)))&& 
               CPAO_AccountServices.account_potentially_eligible_to_LSO_Badge(account)){/*account.CPAO_LSO__c!=null && account.CPAO_Account_Type__c=='Firm'&& account.CPAO_Corporate_Structure__c=='Professional Corporation'){
*/potential_LSOContacts.add(account.CPAO_LSO__c);
               }
            
            if(	CPAO_AccountServices.account_potentially_eligible_to_LSO_Badge(oldMap.get(account.Id)) &&
               (!CPAO_AccountServices.account_potentially_eligible_to_LSO_Badge(account))){
                   LSOContactsToRemove.add(oldMap.get(account.Id).CPAO_LSO__c);
               }
            
            if((oldMap.get(account.Id).OrderApi__Primary_Contact__c==null ||oldMap.get(account.Id).OrderApi__Primary_Contact__c!=account.OrderApi__Primary_Contact__c) 
               && account.OrderApi__Primary_Contact__c!=null){
                   primaryContacts.add(account.OrderApi__Primary_Contact__c);
               }
            
            if(oldMap.get(account.Id).OrderApi__Primary_Contact__c!=null && account.OrderApi__Primary_Contact__c!=oldMap.get(account.Id).OrderApi__Primary_Contact__c){
                primaryContactsToRemove.add(oldMap.get(account.Id).OrderApi__Primary_Contact__c);
            }
            
            
        }
        
        if(!manualAssignAccounts.isEmpty()){
            packageAndPassToIdClass(null, manualAssignAccounts, idsToBeInactivated);
        }
        if(!potential_LSOContacts.isEmpty()){
            CPAO_AccountServices.assign_LSO_badges_to_eligible_contacts(potential_LSOContacts);
            
        }
        if(!LSOContactsToRemove.isEmpty()){
            CPAO_ContactServices.removeBadgeFromContact(LSOContactsToRemove,'LSO');
            System.debug('LSO badges deleted');
        }
        if(!primaryContacts.isEmpty()){
            CPAO_ContactServices.assignBadgeToContacts(primaryContacts,'Primary Contact');
            System.debug('primary badges added');
        }
        if(!primaryContactsToRemove.isEmpty()){
            CPAO_ContactServices.removeBadgeFromContact(primaryContactsToRemove,'Primary Contact');
        }
        
    }
    
    //handles calls from after delete trigger, filters and then calls methods where relevant
    //
    public static void afterDeleteFilters(List<Account> accounts) {
        List<Id> primaryContactsToRemove= new List<Id>();
        List<Id> LSOContactsToRemove= new List<Id>();
        
        for(Account account:accounts){ 
            if(account.OrderApi__Primary_Contact__c!=null){
                primaryContactsToRemove.add(account.OrderApi__Primary_Contact__c);
            }
            
            if(account.CPAO_LSO__c!=null){
                LSOContactsToRemove.add(account.CPAO_LSO__c);
            }
        }
        
        if(!LSOContactsToRemove.isEmpty()){
            CPAO_ContactServices.removeBadgeFromContact(LSOContactsToRemove,'LSO');
        }
        if(!primaryContactsToRemove.isEmpty()){
            CPAO_ContactServices.removeBadgeFromContact(primaryContactsToRemove,'Primary Contact');
        }
    }
    
    
    //this will handle new Id button clicks on the contact page
    public static void handleNewIdButtonClicks(Id accountId) {
        
        List<Account> account = [SELECT Id, CPAO_CPA_Id__c FROM Account WHERE Id =:accountId];
        if(account.size() != 1){
            throw new CPAO_AccountHandler_Exception(accountListSizeError);
        }
        List<Decimal> idsToRenderInactive = new List<Decimal>();
        if(account[0].CPAO_CPA_Id__c != null){
            throw new CPAO_AccountHandler_Exception(idCurrentlyExists);
            //idsToRenderInactive.add(account[0].CPAO_CPA_Id__c);
        }
        
        packageAndPassToIdClass(new List<Id>{account[0].Id}, null, idsToRenderInactive);
    }
    
    public static void packageAndPassToIdClass(List<Id> autoAssignAccounts, List<Account> manualAssignAccounts, List<Decimal> idsToRenderInactive) {
        if(CPAO_CPAIdRecordAssigner.hasrun){
            return;
        }
        CPAO_RecordIdAssignmentWrapper wrapper = new CPAO_RecordIdAssignmentWrapper('Account');
        if(autoAssignAccounts != null){
            for(Id accountId:autoAssignAccounts){
                wrapper.idsAutomaticAssignment.add(accountId);
            }
        }
        if(manualAssignAccounts != null){
            for(Account account:manualAssignAccounts){
                wrapper.idsManualAssignment.put(account.Id, account.CPAO_CPA_Id__c);
            }
        }
        
        if(idsToRenderInactive != null){
            for(Decimal idRecord:idsToRenderInactive){
                wrapper.idRecordsToRenderInactive.add(idRecord);
            }
        }
        CPAO_CPAIdRecordAssigner.assignIds(wrapper);
    }
    
    public static void modifyCPAIdDisplay(List<Account> accounts) {
        for(Account account:accounts){
            String cpaIdDisplay = String.valueOf(account.CPAO_CPA_Id__c);
            //while(cpaIdDisplay.length()<9){
            //	cpaIdDisplay = '0' + cpaIdDisplay;
            //}
            account.CPAO_CPA_Id_Display__c = cpaIdDisplay;
        }
    }
    
    
    
    
    
    public class CPAO_AccountHandler_Exception extends Exception {}
}