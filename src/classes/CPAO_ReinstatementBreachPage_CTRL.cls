public class CPAO_ReinstatementBreachPage_CTRL {
    
    public static User LOGGED_USER = getLoggedUserInfo();
    
    @AuraEnabled
    public static ResultWrapper getUserBreaches(){
        ResultWrapper result = new ResultWrapper();
        result.currentInfractions = getCurrentInfractions();
        
        return result;
    }
    
    @AuraEnabled
    public static void updateUserBreaches(CPAO_Infraction__c[] infractionList, String files){
        
        System.debug('here now');
        //System.debug(files);
        
        
        List<fileWrapper> fileList = (List<fileWrapper>)System.JSON.deserialize(files, List<fileWrapper>.Class);
        System.debug(fileList);
       /*
        for(Integer i=0; i<infractionList.size();i++){
            try{
                System.debug('here now');
                System.debug(fileList[i].name);
            }catch(UnexpectedException e){
                System.debug('error now');
            }
        }*/
        
        update infractionList;
        
    }
    
    public static User getLoggedUserInfo(){
        return CPAO_UserSelector.getCurrentUser();
    }
    
    public static  List<CPAO_Infraction__c>  getCurrentInfractions(){
         Set<Id> contactId=new Set<Id> ();
         contactId.add(LOGGED_USER.ContactId);
         return CPAO_InfractionSelector.getContactOpenInfractions(contactId);
    }
    
    public class ResultWrapper{
        @AuraEnabled public List<CPAO_Infraction__c> currentInfractions{get;set;}
    }
    
    public class fileWrapper{
        @AuraEnabled public String filename {get;set;}
        @AuraEnabled public String filetype {get;set;}
        @AuraEnabled public String filecontent {get;set;}
            
    }

}