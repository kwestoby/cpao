({
    upsertPAL : function(component) {
        console.log('in helper'+component.get('v.currentPAL') );
        
        if(component.get('v.currentPAL').CPAO_CPD_Declaration__c==component.get('v.completeRequirements')){//=='Completed annual and triennial requirements'){
            
            component.set('v.currentPAL.CPAO_CPD_Year1_VerifiableHours__c',null);
            component.set('v.currentPAL.CPAO_CPD_Year_1_Unverifiable_Hours__c',null);
            component.set('v.currentPAL.CPAO_CPD_Year2_VerifiableHours__c',null);
            component.set('v.currentPAL.CPAO_CPD_Year_2_Unverifiable_Hours__c',null);
            component.set('v.currentPAL.CPAO_CPD_Year3_VerifiableHours__c',null);
            component.set('v.currentPAL.CPAO_CPD_Year_3_Unverifiable_Hours__c',null);
            
        }
        
        var action = component.get("c.upsertPalApplication");		
        action.setParams({ "PALappToAdd" :component.get('v.currentPAL') });
        action.setCallback(this, function(response){
            console.log('callback');
            var state = response.getState();
            if (state === "SUCCESS") {
                console.log('PAL added');
                if(component.get('v.shouldSaveAndQuit')){
                    window.location.href='/CPAO_ApplicationDashboard';
                    
                }else{
                    component.set('v.currentPAL',response.getReturnValue());
                    var applicationHasChanged = component.getEvent("applicationHasChanged");
                    applicationHasChanged.setParams({
                        "currentApplication": component.get("v.currentPAL"),
                        "nextButtonClickeable": "true"
                    });
                    applicationHasChanged.fire();
                }
                
            }else{
                console.log('error while adding Pal');
                
            }
        });
        
        $A.enqueueAction(action);
    }
    

})