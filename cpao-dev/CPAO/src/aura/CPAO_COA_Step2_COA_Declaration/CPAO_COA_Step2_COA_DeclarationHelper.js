({
    upsertCOA : function(component) {
        var currentCOA=component.get('v.currentCOA');
        var action = component.get("c.upsertCOAApplication");
        //alert(component.get('v.currentCOA'));
        action.setParams({ "COAAppToAdd" :currentCOA});
        action.setCallback(this, function(response){
            var state = response.getState();
            
            if (state === "SUCCESS") {
                console.log('coa added');
                if(component.get('v.shouldSaveAndQuit')){
                    window.location.href='/CPAO_ApplicationDashboard';
                    
                }else{
                    component.set('v.currentCOA',response.getReturnValue());
                    var applicationHasChanged = component.getEvent("applicationHasChanged");
                    applicationHasChanged.setParams({
                        "currentApplication": component.get("v.currentCOA"),
                        "nextButtonClickeable": "true"
                    });
                    applicationHasChanged.fire();
                }
            }else{
                console.log('error while adding COA');
                var error= response.getError();
                console.log(error[0]);
            }
        });
        
        $A.enqueueAction(action);
    }/*,
	 upload: function(component, file, fileContents,ignoreValidation) {
      	var uploadName=component.get('v.currentCOA.CPAO_COA_Declaration_File_Name__c');
      	var oldUploadId=component.get('v.currentCOA.CPAO_COA_Declaration_File_Id__c');
      	var currentUpload=component.get('v.coaDeclarationFileName');
        var coa_decl_req=component.find('coa_decl_req');



       if(component.get('v.fileInput')==null &&!ignoreValidation &&oldUploadId==null){

       		$A.util.addClass(coa_decl_req, 'slds-show');
	     

	   }else if(component.get('v.fileInput')==null && oldUploadId!=null){     
	   	this.upsertCOA(component);
       }else{
         $A.util.removeClass(coa_decl_req, 'slds-show');
       //	alert('notnull, thats a new file');
       	if(oldUploadId!=null){
       		  var action = component.get("c.saveTheFile"); 
	            action.setParams({
	            parentId: component.get("v.currentCOA.Id"),
	            fileName: component.get("v.fileName"),
	            base64Data: encodeURIComponent(fileContents), 
	            contentType: component.get("v.fileType"),
	            fileTitle: component.get('v.fileTitle'),
	            idFileToReplace:oldUploadId
	        });
       	}else{
       		  var action = component.get("c.saveTheFile"); 
	        action.setParams({
	            parentId: component.get("v.currentCOA.Id"),
	            fileName: component.get("v.fileName"),
	            base64Data: encodeURIComponent(fileContents), 
	            contentType: component.get("v.fileType"),
	            fileTitle: component.get('v.fileTitle'),
	            idFileToReplace:null
	        });
       	}
	      

	        action.setCallback(this, function(response) {
	              var state = response.getState();
	              if (state === "SUCCESS") {
	              var attachId = response.getReturnValue().fileId;
	               component.set('v.currentCOA.CPAO_COA_Declaration_File_Id__c',attachId);
	               var fileName=response.getReturnValue().fileName;
	               component.set('v.currentCOA.CPAO_COA_Declaration_File_Name__c',fileName);
	                              //  alert(componen);

	                this.upsertCOA(component);
	               

	              }else{
	                console.log('error while adding attachment'+ component.get("v.fileName") +  component.get("v.fileTitle"));
	                var errors=response.getError();
	   		      	var message = errors[0];
	   
	   		      console.log(message);

	              }
	           
	        });
	            
	       
	        $A.enqueueAction(action); 
	    }
  
  }*/
})