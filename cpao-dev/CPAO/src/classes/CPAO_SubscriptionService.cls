public class CPAO_SubscriptionService {
	public static void renewSubscription(Set<Id> subscriptionIds) {
		List<OrderApi__Subscription__c> subscriptions = CPAO_SubscriptionSelector.getSubscriptionsWithLatestTerm(subscriptionIds);
		for (OrderApi__Subscription__c sub : subscriptions) {
			if (sub.OrderApi__Renewals__r.size() > 0) {
				OrderApi__Renewal__c term = sub.OrderApi__Renewals__r[0];
				//sub.OrderApi__Current_Term_Start_Date__c = term.OrderApi__Term_Start_Date__c; [Removed as this causes subscription to be inactive if renew before new term start date]
				sub.OrderApi__Last_Renewed_Date__c = System.today();
				sub.OrderApi__Current_Term_End_Date__c = term.OrderApi__Term_End_Date__c;
				Integer graceDays = (Integer) sub.OrderApi__Subscription_Plan__r.OrderApi__Grace_Period__c;
				sub.OrderApi__Grace_Period_End_Date__c = term.OrderApi__Term_End_Date__c.addDays(graceDays);
			}
		}
		update subscriptions;
	}
}