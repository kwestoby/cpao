/**
* @File Name    :   CPAO_MRA_Application_CTRLTest
* @Description  :   CPAO MRA  Controller Test
* @Date Created :   2018-07-17
* @Author       :   Michaell Reis Gasparini, Deloitte, mreisg@deloitte.com
* @group        :   Class
* @Modification Log:
**************************************************************************************
* Ver       Date        Author              Modification
* 1.0       2018-07-17  Michaell Reis       Created the file/class
**/
@isTest
private class CPAO_MRA_Application_CTRLTest {
	
	@testSetup
	static void dataInit(){
		
		
		CPAO_TestFactory.automationControlSetup();
		
		List<Account> accounts = CPAO_TestFactory.createAccounts(1);
		Database.insert(accounts);

		List<Contact> contacts = CPAO_TestFactory.createContacts(accounts[0],1);
		Database.insert(contacts);

		User user = CPAO_TestFactory.createCommunityUser(contacts[0].id);
		Database.insert(user);



		List<CPAO_Application__c> applications =CPAO_TestFactory.createPALApplication(1,contacts[0]);
		id recordTypeId = Schema.SObjectType.CPAO_Application__c.getRecordTypeInfosByName().get('MRA Application').getRecordTypeId();
		applications[0].RecordTypeId=recordTypeId;
		applications[0].CPAO_Status__c ='Open';
		applications[0].CPAO_Current_Step__c = 'step1';
		Database.insert(applications);

		ContentVersion contentVersion_1 = new ContentVersion(
			Title = 'Penguins',
			PathOnClient = 'Penguins.jpg',
			VersionData = Blob.valueOf('Test Content'),
			IsMajorVersion = true
			);
		insert contentVersion_1;

		List<ContentDocument> documents = [SELECT Id, Title, LatestPublishedVersionId FROM ContentDocument];

		ContentDocumentLink contentlink=new ContentDocumentLink();
		contentlink.ShareType= 'V';
		contentlink.ContentDocumentId=documents[0].Id;
		contentlink.Visibility = 'AllUsers'; 
		contentlink.LinkedEntityId = accounts[0].id;
		insert contentlink;

	}


	@isTest static void getApplication() {
		// Implement test code

		CPAO_MRA_Application_CTRL.getCurrentMRAApplication();
	}
	
	@isTest static void callUpsertApp() {
		// Implement test code

		CPAO_Application__c app =[Select id from CPAO_Application__c Limit 1];
		
		CPAO_MRA_Application_CTRL.upsertPalApplication(app);
	}
	

	@isTest static void getPreviousApplications() {
		// Implement test code

		CPAO_MRA_Application_CTRL.getCurrentMRAApplication();

		CPAO_MRA_Application_CTRL.userHasPreviousPALApplications();
	}

	@isTest static void getReview() {
		// Implement test code

		CPAO_MRA_Application_CTRL.loadReviewInfo();

		
	}


	@isTest static void callSaveFiles() {
		try {
			id content =[Select id from ContentVersion Limit 1].Id;
			CPAO_MRA_Application_CTRL.saveMultipleFiles('[{"parentId":"01I3B0000009kfK","fileName" :"Test","base64Data":"YTM0NZomIzI2OTsmIzM0NTueYQ==" ,"contentType": "text/html" , "fileTitle" : "Test" ,"FileToReplace": "Test" ,"fileId" :"'+content+'"}]');
			} catch(Exception e) {

			}

		}

		@isTest static void callSaveTheFile() {
			try {
				ContentVersion testContent = [SELECT id, ContentDocumentId FROM ContentVersion LIMIT 1];
				id content =[Select id from ContentDocumentLink WHERE ContentDocumentId =: testcontent.ContentDocumentId  Limit 1].Id;
				CPAO_MRA_Application_CTRL.saveTheFile(content, 'Another Test', 'R0lGODlhPQBEAPeoAJosM//AwO/AwHVYZ/z595kzAP/s7P+goOXMv8+fhw/v739/f+8PD98fH/8mJl+fn/9ZWb8/PzWlwv///6wWGbImAPgTEMImIN9gUFCEm/gDALULDN8PAD6atYdCTX9gUNKlj8wZAKUsAOzZz+UMAOsJAP/Z2ccMDA8PD/95eX5NWvsJCOVNQPtfX/8zM8+QePLl38MGBr8JCP+zs9myn/8GBqwpAP/GxgwJCPny78lzYLgjAJ8vAP9fX/+MjMUcAN8zM/9wcM8ZGcATEL+QePdZWf/29uc/P9cmJu9MTDImIN+/r7+/vz8/P8VNQGNugV8AAF9fX8swMNgTAFlDOICAgPNSUnNWSMQ5MBAQEJE3QPIGAM9AQMqGcG9vb6MhJsEdGM8vLx8fH98AANIWAMuQeL8fABkTEPPQ0OM5OSYdGFl5jo+Pj/+pqcsTE78wMFNGQLYmID4dGPvd3UBAQJmTkP+8vH9QUK+vr8ZWSHpzcJMmILdwcLOGcHRQUHxwcK9PT9DQ0O/v70w5MLypoG8wKOuwsP/g4P/Q0IcwKEswKMl8aJ9fX2xjdOtGRs/Pz+Dg4GImIP8gIH0sKEAwKKmTiKZ8aB/f39Wsl+LFt8dgUE9PT5x5aHBwcP+AgP+WltdgYMyZfyywz78AAAAAAAD///8AAP9mZv///wAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAACH5BAEAAKgALAAAAAA9AEQAAAj/AFEJHEiwoMGDCBMqXMiwocAbBww4nEhxoYkUpzJGrMixogkfGUNqlNixJEIDB0SqHGmyJSojM1bKZOmyop0gM3Oe2liTISKMOoPy7GnwY9CjIYcSRYm0aVKSLmE6nfq05QycVLPuhDrxBlCtYJUqNAq2bNWEBj6ZXRuyxZyDRtqwnXvkhACDV+euTeJm1Ki7A73qNWtFiF+/gA95Gly2CJLDhwEHMOUAAuOpLYDEgBxZ4GRTlC1fDnpkM+fOqD6DDj1aZpITp0dtGCDhr+fVuCu3zlg49ijaokTZTo27uG7Gjn2P+hI8+PDPERoUB318bWbfAJ5sUNFcuGRTYUqV/3ogfXp1rWlMc6awJjiAAd2fm4ogXjz56aypOoIde4OE5u/F9x199dlXnnGiHZWEYbGpsAEA3QXYnHwEFliKAgswgJ8LPeiUXGwedCAKABACCN+EA1pYIIYaFlcDhytd51sGAJbo3onOpajiihlO92KHGaUXGwWjUBChjSPiWJuOO/LYIm4v1tXfE6J4gCSJEZ7YgRYUNrkji9P55sF/ogxw5ZkSqIDaZBV6aSGYq/lGZplndkckZ98xoICbTcIJGQAZcNmdmUc210hs35nCyJ58fgmIKX5RQGOZowxaZwYA+JaoKQwswGijBV4C6SiTUmpphMspJx9unX4KaimjDv9aaXOEBteBqmuuxgEHoLX6Kqx+yXqqBANsgCtit4FWQAEkrNbpq7HSOmtwag5w57GrmlJBASEU18ADjUYb3ADTinIttsgSB1oJFfA63bduimuqKB1keqwUhoCSK374wbujvOSu4QG6UvxBRydcpKsav++Ca6G8A6Pr1x2kVMyHwsVxUALDq/krnrhPSOzXG1lUTIoffqGR7Goi2MAxbv6O2kEG56I7CSlRsEFKFVyovDJoIRTg7sugNRDGqCJzJgcKE0ywc0ELm6KBCCJo8DIPFeCWNGcyqNFE06ToAfV0HBRgxsvLThHn1oddQMrXj5DyAQgjEHSAJMWZwS3HPxT/QMbabI/iBCliMLEJKX2EEkomBAUCxRi42VDADxyTYDVogV+wSChqmKxEKCDAYFDFj4OmwbY7bDGdBhtrnTQYOigeChUmc1K3QTnAUfEgGFgAWt88hKA6aCRIXhxnQ1yg3BCayK44EWdkUQcBByEQChFXfCB776aQsG0BIlQgQgE8qO26X1h8cEUep8ngRBnOy74E9QgRgEAC8SvOfQkh7FDBDmS43PmGoIiKUUEGkMEC/PJHgxw0xH74yx/3XnaYRJgMB8obxQW6kL9QYEJ0FIFgByfIL7/IQAlvQwEpnAC7DtLNJCKUoO/w45c44GwCXiAFB/OXAATQryUxdN4LfFiwgjCNYg+kYMIEFkCKDs6PKAIJouyGWMS1FSKJOMRB/BoIxYJIUXFUxNwoIkEKPAgCBZSQHQ1A2EWDfDEUVLyADj5AChSIQW6gu10bE/JG2VnCZGfo4R4d0sdQoBAHhPjhIB94v/wRoRKQWGRHgrhGSQJxCS+0pCZbEhAAOw==', 'data:image/gif','Another',content);
				} catch(Exception e) {

				}

			}

			@isTest static void callDeleteApplication() {
		// Implement test code
		try{
			
			//Call with some Id
			ContentVersion testContent = [SELECT id, ContentDocumentId FROM ContentVersion LIMIT 1];
			List<String> idFiles= new List<String>();
			String idFile = testContent.ContentDocumentId; 
			idFiles.add(idFile);
			CPAO_MRA_Application_CTRL.deletePALApplicationFiles(idFiles);
			//Call without id
			idFiles= new List<String>();
			CPAO_MRA_Application_CTRL.deletePALApplicationFiles(null);
			}catch(Exception e){

			}

		}



		@isTest static void callConstructors() {

			List<Contact> contacts =[SELECT id FROM Contact];
			List<CPAO_Application__c> applications=[SELECT id FROM CPAO_Application__c];
			CPAO_MRA_Application_CTRL.ReviewPageWrapper  review = new CPAO_MRA_Application_CTRL.ReviewPageWrapper();
			review.currentPALApplication = applications[0];
			review.userContact = contacts[0];
			review.contactEmploymentRecords = new List<CPAO_Account_Contact_Relationship__c>();

			CPAO_MRA_Application_CTRL.FileWrapper wrapper = new CPAO_MRA_Application_CTRL.FileWrapper();
			wrapper.fileId = '01I3B0000009kfK';
			wrapper.fileName  = 'TEST';

			CPAO_MRA_Application_CTRL.FilesWrapper wrappers = new CPAO_MRA_Application_CTRL.FilesWrapper();
			wrappers.fileId  = '01I3B0000009kfK';
		}

	}