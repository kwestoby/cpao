/**
* @File Name    :   CPAO_MilestoneContactHelperTest
* @Description  :   Trigger Milestone Contact  Helper Test
* @Date Created :   2018-07-12
* @Author       :   Michaell Reis Gasparini, Deloitte, mreisg@deloitte.com
* @group        :   Class
* @Modification Log:
**************************************************************************************
* Ver       Date        Author              Modification
* 1.0       2018-07-12  Michaell Reis       Created the file/class
**/
@isTest
private class CPAO_MilestoneContactHelperTest {
	/*@testSetup
	static void dataInit(){

		CPAO_History_Tracker_Controller__c historytrackerController = new CPAO_History_Tracker_Controller__c();
		historytrackerController.CPAO_Fields_to_Copy__c ='CPAO_CPA_Status__c,CPAO_CPA_Sub_Status__c,CPAO_Legal_Given__c,CPAO_Legal_Surname__c';
		historytrackerController.CPAO_Trigger_Fields__c ='CPAO_CPA_Status__c,CPAO_CPA_Sub_Status__c,CPAO_Legal_Given__c,CPAO_Legal_Surname__c';
		historytrackerController.CPAO_ObjectType__c ='Contact';
		historytrackerController.CPAO_Active__c=true;

		Database.insert(historytrackerController);
		

		CPAO_TestFactory.automationControlSetup();
		
		List<Account> accounts = CPAO_TestFactory.createAccounts(1);
		accounts[0].CPAO_Account_Status__c = 'Active';
				
		Database.insert(accounts);

		List<Contact> contact = CPAO_TestFactory.createContacts(accounts[0],1);
		contact[0].CPAO_CPA_Status__c  = 'Active';
		Database.insert(contact);

	}

	@isTest static void testChangeFields() {
		
		id recordTypeId = Schema.SObjectType.CPAO_MilestoneAndHistory__c.getRecordTypeInfosByName().get('Contact Milestone').getRecordTypeId();
		
		Contact contact =[Select id, CPAO_CPA_Status__c,CPAO_CPA_Sub_Status__c FROM Contact LIMIT 1];
		
		contact.CPAO_CPA_Status__c ='Resigned';
		contact.CPAO_CPA_Sub_Status__c='Resigned Before Discipline';
		
		Database.update(contact);
		

		CPAO_MilestoneAndHistory__c history =[Select id,CPAO_Contact__c, CPAO_CPA_Status__c,CPAO_CPA_Sub_Status__c, RecordTypeId FROM CPAO_MilestoneAndHistory__c LIMIT 1]; 
		
		System.assertEquals(contact.Id,history.CPAO_Contact__c);
		System.assertEquals(recordTypeId ,history.RecordTypeId);
		System.assertEquals('Active',history.CPAO_CPA_Status__c );

	}
	

	@isTest static void testDelete() {
		
		id recordTypeId = Schema.SObjectType.CPAO_MilestoneAndHistory__c.getRecordTypeInfosByName().get('Contact Previous Name').getRecordTypeId();
		
		Contact contact =[Select id, CPAO_CPA_Status__c,CPAO_CPA_Sub_Status__c,CPAO_Legal_Given__c,CPAO_Legal_Surname__c FROM Contact LIMIT 1];
		String oldFirstName = contact.CPAO_Legal_Given__c;
		String oldLastName = contact.CPAO_Legal_Surname__c;
		contact.CPAO_Legal_Given__c='Changed';
		contact.CPAO_Legal_Surname__c ='Surname';
		contact.CPAO_CPA_Status__c ='Resigned';
		contact.CPAO_CPA_Sub_Status__c='Resigned Before Discipline';
		Database.update(contact);
		

		CPAO_MilestoneAndHistory__c history =[Select id,CPAO_Contact__c, CPAO_Legal_Given__c,CPAO_Legal_Surname__c ,RecordTypeId FROM CPAO_MilestoneAndHistory__c LIMIT 1]; 
		
		System.assertEquals(contact.Id,history.CPAO_Contact__c);
		System.assertEquals(recordTypeId ,history.RecordTypeId);
		System.assertEquals(oldFirstName,history.CPAO_Legal_Given__c );
		System.assertEquals(oldLastName,history.CPAO_Legal_Surname__c );

	}

	@isTest static void testChangeName() {
		List<Contact>contacts=	[Select id, CPAO_CPA_Status__c,CPAO_CPA_Sub_Status__c,CPAO_Legal_Given__c,CPAO_Legal_Surname__c FROM Contact];
	
		Database.delete(contacts);
	}
	*/
}