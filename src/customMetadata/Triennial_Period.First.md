<?xml version="1.0" encoding="UTF-8"?>
<CustomMetadata xmlns="http://soap.sforce.com/2006/04/metadata" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xmlns:xsd="http://www.w3.org/2001/XMLSchema">
    <label>First</label>
    <protected>false</protected>
    <values>
        <field>First_Year__c</field>
        <value xsi:type="xsd:string">2016</value>
    </values>
    <values>
        <field>Second_Year__c</field>
        <value xsi:type="xsd:string">2017</value>
    </values>
    <values>
        <field>Third_Year__c</field>
        <value xsi:type="xsd:string">2018</value>
    </values>
</CustomMetadata>
