({
	doInit : function(component, event, helper) {
		var apexAction = component.get('c.getEvents');
        apexAction.setCallback(this, function(response) {
        	var state = response.getState();
        	if(state === "SUCCESS"){
                var eventList = response.getReturnValue();
                if(eventList == null & eventList == undefined){

                } else {
                    component.set("v.Events", eventList);
                }
            }
        });
        $A.enqueueAction(apexAction);
	}
})