<?xml version="1.0" encoding="UTF-8"?>
<CustomObject xmlns="http://soap.sforce.com/2006/04/metadata">
    <actionOverrides>
        <actionName>Accept</actionName>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>CancelEdit</actionName>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>Clone</actionName>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>Delete</actionName>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>Edit</actionName>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>List</actionName>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>New</actionName>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>SaveEdit</actionName>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>Tab</actionName>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>View</actionName>
        <type>Default</type>
    </actionOverrides>
    <allowInChatterGroups>false</allowInChatterGroups>
    <compactLayoutAssignment>SYSTEM</compactLayoutAssignment>
    <deploymentStatus>Deployed</deploymentStatus>
    <description>Describes the relationship between accounts</description>
    <enableActivities>true</enableActivities>
    <enableBulkApi>true</enableBulkApi>
    <enableChangeDataCapture>false</enableChangeDataCapture>
    <enableEnhancedLookup>true</enableEnhancedLookup>
    <enableFeeds>false</enableFeeds>
    <enableHistory>true</enableHistory>
    <enableReports>true</enableReports>
    <enableSearch>true</enableSearch>
    <enableSharing>true</enableSharing>
    <enableStreamingApi>true</enableStreamingApi>
    <fields>
        <fullName>CPAO_Account_Name__c</fullName>
        <deleteConstraint>Restrict</deleteConstraint>
        <description>Name of the Account</description>
        <externalId>false</externalId>
        <label>Account Name</label>
        <referenceTo>Account</referenceTo>
        <relationshipLabel>Account to Account Relationships</relationshipLabel>
        <relationshipName>Account_to_Account_Relationships2</relationshipName>
        <required>true</required>
        <trackHistory>true</trackHistory>
        <trackTrending>false</trackTrending>
        <type>Lookup</type>
    </fields>
    <fields>
        <fullName>CPAO_Associated_Account_Name__c</fullName>
        <deleteConstraint>Restrict</deleteConstraint>
        <externalId>false</externalId>
        <label>Associated Account Name</label>
        <referenceTo>Account</referenceTo>
        <relationshipLabel>Account to Account Relationships (Associated Account)</relationshipLabel>
        <relationshipName>Account_to_Account_Relationships3</relationshipName>
        <required>true</required>
        <trackHistory>true</trackHistory>
        <trackTrending>false</trackTrending>
        <type>Lookup</type>
    </fields>
    <fields>
        <fullName>CPAO_End_Date__c</fullName>
        <description>End Date of the relationship</description>
        <externalId>false</externalId>
        <label>End Date</label>
        <required>false</required>
        <trackHistory>true</trackHistory>
        <trackTrending>false</trackTrending>
        <type>Date</type>
    </fields>
    <fields>
        <fullName>CPAO_Relationship_to_Associated_Account__c</fullName>
        <description>Relationship to the Main Account</description>
        <externalId>false</externalId>
        <label>Relationship to Associated Account</label>
        <required>true</required>
        <trackHistory>false</trackHistory>
        <trackTrending>false</trackTrending>
        <type>Picklist</type>
        <valueSet>
            <restricted>true</restricted>
            <valueSetDefinition>
                <sorted>false</sorted>
                <value>
                    <fullName>Parent</fullName>
                    <default>false</default>
                    <label>Parent</label>
                </value>
                <value>
                    <fullName>Professional Corporation Partner</fullName>
                    <default>false</default>
                    <label>Professional Corporation Partner</label>
                </value>
                <value>
                    <fullName>Related Business</fullName>
                    <default>false</default>
                    <label>Related Business</label>
                </value>
                <value>
                    <fullName>Employee</fullName>
                    <default>false</default>
                    <isActive>false</isActive>
                    <label>Employee</label>
                </value>
                <value>
                    <fullName>Instructor</fullName>
                    <default>false</default>
                    <isActive>false</isActive>
                    <label>Instructor</label>
                </value>
                <value>
                    <fullName>LSO</fullName>
                    <default>false</default>
                    <isActive>false</isActive>
                    <label>LSO</label>
                </value>
                <value>
                    <fullName>Member</fullName>
                    <default>false</default>
                    <isActive>false</isActive>
                    <label>Member</label>
                </value>
                <value>
                    <fullName>Partner</fullName>
                    <default>false</default>
                    <isActive>false</isActive>
                    <label>Partner</label>
                </value>
                <value>
                    <fullName>Shareholder</fullName>
                    <default>false</default>
                    <isActive>false</isActive>
                    <label>Shareholder</label>
                </value>
                <value>
                    <fullName>Sole Proprietor</fullName>
                    <default>false</default>
                    <isActive>false</isActive>
                    <label>Sole Proprietor</label>
                </value>
                <value>
                    <fullName>Sponsor</fullName>
                    <default>false</default>
                    <isActive>false</isActive>
                    <label>Sponsor</label>
                </value>
                <value>
                    <fullName>Student</fullName>
                    <default>false</default>
                    <isActive>false</isActive>
                    <label>Student</label>
                </value>
                <value>
                    <fullName>Training Officer</fullName>
                    <default>false</default>
                    <isActive>false</isActive>
                    <label>Training Officer</label>
                </value>
            </valueSetDefinition>
        </valueSet>
    </fields>
    <fields>
        <fullName>CPAO_Start_Date__c</fullName>
        <description>Start Date of the Relationship</description>
        <externalId>false</externalId>
        <label>Start Date</label>
        <required>true</required>
        <trackHistory>true</trackHistory>
        <trackTrending>false</trackTrending>
        <type>Date</type>
    </fields>
    <fields>
        <fullName>CPAO_Status__c</fullName>
        <externalId>false</externalId>
        <formula>IF( CPAO_End_Date__c  &lt;=  TODAY() , &quot;Inactive&quot;, &quot;Active&quot;)</formula>
        <formulaTreatBlanksAs>BlankAsZero</formulaTreatBlanksAs>
        <label>Status</label>
        <required>false</required>
        <trackHistory>false</trackHistory>
        <trackTrending>false</trackTrending>
        <type>Text</type>
        <unique>false</unique>
    </fields>
    <label>Account to Account Relationship</label>
    <nameField>
        <displayFormat>{000000}</displayFormat>
        <label>Relationship Number</label>
        <trackHistory>true</trackHistory>
        <type>AutoNumber</type>
    </nameField>
    <pluralLabel>Account to Account Relationships</pluralLabel>
    <searchLayouts/>
    <sharingModel>ReadWrite</sharingModel>
    <validationRules>
        <fullName>CPAO_End_Date</fullName>
        <active>true</active>
        <description>End Date should always be greater than or equal to the Start Date</description>
        <errorConditionFormula>CPAO_End_Date__c &lt;  CPAO_Start_Date__c
&amp;&amp; 
$Setup.CPAO_Automation_Control__c.CPAO_Account_to_Account_Relationship_c__c</errorConditionFormula>
        <errorMessage>End Date should be greater than or equal to  the Start Date</errorMessage>
    </validationRules>
    <visibility>Public</visibility>
</CustomObject>
