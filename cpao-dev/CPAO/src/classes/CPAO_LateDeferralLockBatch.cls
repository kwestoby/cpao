global class CPAO_LateDeferralLockBatch implements Database.Batchable<sObject> {
	public static final String AMD_INFRACTION_TYPE = 'Annual Membership Dues (AMD)';

	global Database.QueryLocator start(Database.BatchableContext BC) {
		String query = 'SELECT Id, CPAO_Contact__c FROM CPAO_Application__c WHERE ';
        query = query + 'RecordType.Name = \'AMD/CPD\' AND CPAO_Status__c = \'Submitted-Payment Deferred\'';
        //query = query + 'AND CPAO_Infraction_Created__c = false';
        return Database.getQueryLocator(query);
	}

   	global void execute(Database.BatchableContext BC, List<CPAO_Application__c> applications) {
		Set<Id> contactIds = new Set<Id>();
		for(CPAO_Application__c application:applications){
			application.CPAO_Status__c = 'Completed';
			if(application.CPAO_Contact__c != null){
				contactIds.add(application.CPAO_Contact__c);
			}
		}
		Database.SaveResult[] srList = Database.update(applications, false);
		List<CPAO_Infraction_Type__c> infractionType = CPAO_InfractionSelector.getInfractionType(AMD_INFRACTION_TYPE);
		List<CPAO_Infraction__c> infractionsToCreate = new List<CPAO_Infraction__c>();
		if(infractionType.size() == 1 && !contactIds.isEmpty()){
			
			for(Id contactId:contactIds){
				infractionsToCreate.add(new CPAO_Infraction__c(
					CPAO_Infraction_Type__c = infractionType[0].Id,
	                CPAO_Contact__c = contactId));
			}				
		}
		if(!infractionsToCreate.isEmpty()){
			insert infractionsToCreate;
		}
	}
	
	global void finish(Database.BatchableContext BC) {
		
	}
}