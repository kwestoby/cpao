({
    
    clickOnNextButton : function(component,event,helper){
        var componentNameInEvent=event.getParam("componentName");
        if(componentNameInEvent==component.get('v.nameOfThisComponent')){
            if(event.getParam("nextStepName")=='Exit'){ 
                component.set('v.shouldSaveAndQuit',true);
            }
            var validForm = component.find('PALForm').reduce(function (validSoFar, inputCmp) {
                var role_in_PA=  component.get('v.currentPAL.CPAO_Role_In_Public_Accounting__c');  
                var error_role_in_PA= component.find('role_in_PA_Req');
                var basis_of_app=   component.get('v.currentPAL.CPAO_Basis_Of_Application__c');
                var error_basis_of_app= component.find('basis_of_app_req');
                // Displays error messages for invalid fields
                if(role_in_PA==undefined){
                    $A.util.addClass(error_role_in_PA, 'slds-show');
                }else{
                    // $A.util.addClass(error_role_in_PA, 'slds-hide');
                    $A.util.removeClass(error_role_in_PA, 'slds-show');
                }
                if(basis_of_app==undefined){
                    $A.util.addClass(error_basis_of_app, 'slds-show');
                }else {
                    $A.util.removeClass(error_basis_of_app, 'slds-show');
                    
                }
                
                inputCmp.showHelpMessageIfInvalid();
                return validSoFar && inputCmp.get('v.validity').valid &&
                    (role_in_PA!=undefined) && (basis_of_app!=undefined);
            }, true);
            
            if(validForm){
                var error_role_in_PA= component.find('role_in_PA_Req');
                var error_basis_of_app= component.find('basis_of_app_req');
                $A.util.removeClass(error_basis_of_app, 'slds-show');
                $A.util.removeClass(error_role_in_PA, 'slds-show');
                //console.log('form validdddd');
                
                
                var effectiveDate= component.get('v.currentPAL').CPAO_Effective_Date__c;
                var formattedDate=effectiveDate.substring(0,10);
               /* alert(effectiveDate);
                alert(formattedDate);*/
                component.set('v.currentPAL.CPAO_Effective_Date__c',formattedDate);
                
                
                var showSpinnerEvent = component.getEvent("showSpinner");
                showSpinnerEvent.fire();
                console.log(JSON.stringify(component.get('v.currentPAL')));

                helper.upsertPAL(component);
                
                
                
            }
        }
    },
    
     
})