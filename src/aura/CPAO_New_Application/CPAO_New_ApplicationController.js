({
    doInit : function(component,event,helper){
        var action = component.get("c.getCurrentPALApplication");
        action.setCallback(this, function(response){
            var state = response.getState();
            if (state === "SUCCESS") {
                //  console.log(JSON.stringify(response.getReturnValue()));
                if( response.getReturnValue().currentPALApplication.CPAO_Effective_Date__c!=null){
                    response.getReturnValue().currentPALApplication.CPAO_Effective_Date__c = response.getReturnValue().currentPALApplication.CPAO_Effective_Date__c.substring(0,10);
                }
                if( response.getReturnValue().currentPALApplication.CPAO_Practice_Inspection_Date_1__c!=null){
                    response.getReturnValue().currentPALApplication.CPAO_Practice_Inspection_Date_1__c = response.getReturnValue().currentPALApplication.CPAO_Practice_Inspection_Date_1__c.substring(0,10);
                }
                if( response.getReturnValue().currentPALApplication.CPAO_Practice_Inspection_Date_2__c!=null){
                    response.getReturnValue().currentPALApplication.CPAO_Practice_Inspection_Date_2__c = response.getReturnValue().currentPALApplication.CPAO_Practice_Inspection_Date_2__c.substring(0,10);
                }
                if( response.getReturnValue().currentPALApplication.CPAO_Practice_Inspection_Date_3__c!=null){
                    response.getReturnValue().currentPALApplication.CPAO_Practice_Inspection_Date_3__c = response.getReturnValue().currentPALApplication.CPAO_Practice_Inspection_Date_3__c.substring(0,10);
                }
                if( response.getReturnValue().currentPALApplication.CPAO_Exceptional_Circumstances_SD__c!=null){
                    response.getReturnValue().currentPALApplication.CPAO_Exceptional_Circumstances_SD__c = response.getReturnValue().currentPALApplication.CPAO_Exceptional_Circumstances_SD__c.substring(0,10);
                }
                if( response.getReturnValue().currentPALApplication.CPAO_Exceptional_Circumstances_End_Date__c!=null){
                    response.getReturnValue().currentPALApplication.CPAO_Exceptional_Circumstances_End_Date__c = response.getReturnValue().currentPALApplication.CPAO_Exceptional_Circumstances_End_Date__c.substring(0,10);
                }
                
                component.set('v.currentPAL',response.getReturnValue().currentPALApplication);
                component.set('v.fileWrapper',response.getReturnValue().ScheduleA_B);
                component.set('v.contactStatus',response.getReturnValue().contactStatus);
                
                /*  if(component.get('v.currentPAL.CPAO_Current_Step__c')!=null){
                        component.set('v.selectedStep',component.get('v.currentPAL.CPAO_Current_Step__c'));
                    }*/
                  helper.checkProfessionalExperienceCheckboxes(component);
                  // console.log('cur'+component.get('v.currentPAL').Name);
                  //console.log('cur'+component.get('v.currentPAL').CPAO_Contact__c);
                  //console.log('cur'+component.get('v.currentPAL').CPAO_Basis_Of_Application__c);
                  var basisOfApplication=component.get('v.currentPAL').CPAO_Basis_Of_Application__c;
                  if(basisOfApplication==component.get('v.basisOfApplication')){
                      //  component.set('v.confirmationPreviousPAL',true);
                  }
                  // console.log( 'init application: '+JSON.stringify(component.get('v.currentPAL')));
                  
                  
              }else{
                  console.log('error while doinit Main container');
                  var error= response.getError();
                  console.log(error[0]);
                  component.set('v.errorMsg', error[0].message);
              }
        });
        
        $A.enqueueAction(action);
        
        
    }
    
    
})