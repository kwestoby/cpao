({
	 upsertPAL : function(component) {
        console.log( 'returned application: '+JSON.stringify(component.get('v.currentPAL')));
        var action = component.get("c.upsertPalApplication");		
        action.setParams({ "PALRenewalToAdd" :component.get('v.currentPAL') });
        action.setCallback(this, function(response){
            console.log('callback');
            var state = response.getState();
            if (state === "SUCCESS") {
                console.log('PAL added');
                if(component.get('v.shouldSaveAndQuit')){
                    window.location.href='/CPAO_ApplicationDashboard';
                    
                }else{
                    component.set('v.currentPAL',response.getReturnValue());
                    var applicationHasChanged = component.getEvent("applicationHasChanged");
                    applicationHasChanged.setParams({
                        "currentApplication": component.get("v.currentPAL"),
                        "nextButtonClickeable": "true"
                    });
                    applicationHasChanged.fire();
                }
                
            }else{
                console.log('error while adding Pal renewal');
                 var errors=response.getError();
                var message = errors[0];
                
                console.log(message);
                
            }
        });
        
        $A.enqueueAction(action);
        
    }
})