<?xml version="1.0" encoding="UTF-8"?>
<CustomMetadata xmlns="http://soap.sforce.com/2006/04/metadata" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xmlns:xsd="http://www.w3.org/2001/XMLSchema">
    <label>RosterInstructionScreen</label>
    <protected>false</protected>
    <values>
        <field>CPAO_RosterInstruction__c</field>
        <value xsi:type="xsd:string">Please select the contacts you wish to pay for by putting a checkmark next to their name. If you wish to add additional contacts to your roster please enter a CPA Ontario ID in the search bar at the top of the screen.

Once you have selected the contacts you wish to pay for, please select ‘Next’ to move to the payment screen.


Some things to note:
- Rosters will be dynamically generated each time you visit. This means employment information is pulled directly from CPA Ontario’s system in real-time and rosters are no longer saved by the firm
- Only individuals who have completed their required obligations (i.e. In the case of Annual Membership Dues, only those who have submitted their CPD declaration) beforehand can be paid for.
- Roster payments can be made in batches at multiple points in time if desired; simply return to the roster screen and select the members you haven’t paid for yet.</value>
    </values>
</CustomMetadata>
