({
    upsertPAL : function(component) {
        /*//alert ('helper professional experience ');
		//console.log( 'returned application: '+JSON.stringify(component.get('v.currentPAL')));
		var action = component.get("c.upsertPalApplication");		
		action.setParams({ "PALappToAdd" :component.get('v.currentPAL') });
	   	action.setCallback(this, function(response){
	   		console.log('callback');
		      var state = response.getState();
		     // alert(state);
		      if (state === "SUCCESS") {
		      	console.log('PAL added');
		      	component.set('v.currentPAL',response.getReturnValue());
		      	var applicationHasChanged = component.getEvent("applicationHasChanged");
        		applicationHasChanged.setParams({
            	"currentApplication": component.get("v.currentPAL"),
            	"nextButtonClickeable": "true"
      			});
        		applicationHasChanged.fire();
		      
		    	
		      }else{
		      	console.log('error while adding Pal');
		      }
	    });
	  
	    $A.enqueueAction(action);*/
        
        
        /* var uploadFileEvent = $A.get("e.c:CPAO_Upload_File_Event");
                uploadFileEvent.setParams({
                  "currentPAL":component.get('v.currentPAL'),
                  "fieldName":component.get('v.special_consideration_file_field')

                });
                uploadFileEvent.fire();*/
        this.upload(component,component.get('v.fileInput'), component.get('v.fileContents')) ;
        
    },
    upload: function(component, file, fileContents) {
        /* var validForm = component.find('uploadField').reduce(function (validSoFar, inputCmp) {
        inputCmp.showHelpMessageIfInvalid();
        return validSoFar && inputCmp.get('v.validity').valid }, true);

      if(validForm){*/
         var uploadName=component.get('v.currentPAL.CPAO_Special_Consideration_File_Name__c');
         var oldUploadId=component.get('v.currentPAL.CPAO_Special_Consideration_File_Id__c');
         var oldFileId;
         var newFileId;
         var currentUpload=component.get('v.SpecialConsiderationName');
         
         
         
         if(component.get('v.fileInput')==null){
             //alert('nothing new to upload,, keep old file');
             
             this.savePalApplication(component);
             
         }else{
             
             //alert('notnull, thats a new file');
             if(oldUploadId!=null){
                 var action = component.get("c.saveTheFile"); 
                 action.setParams({
                     parentId: component.get("v.currentPAL.Id"),
                     fileName: component.get("v.fileName"),
                     base64Data: encodeURIComponent(fileContents), 
                     contentType: component.get("v.fileType"),
                     fileTitle: component.get('v.fileTitle'),
                     idFileToReplace:oldUploadId
                 });
             }else{
                 var action = component.get("c.saveTheFile"); 
                 action.setParams({
                     parentId: component.get("v.currentPAL.Id"),
                     fileName: component.get("v.fileName"),
                     base64Data: encodeURIComponent(fileContents), 
                     contentType: component.get("v.fileType"),
                     fileTitle: component.get('v.fileTitle'),
                     idFileToReplace:null
                 });
             }
             
             
             action.setCallback(this, function(response) {
                 var state = response.getState();
                 if (state === "SUCCESS") {
                     var attachId = response.getReturnValue().fileId;
                     component.set('v.currentPAL.CPAO_Special_Consideration_File_Id__c',attachId);
                     var fileName=response.getReturnValue().fileName;
                     component.set('v.currentPAL.CPAO_Special_Consideration_File_Name__c',fileName);
                     //  alert(componen);
                     
                     this.savePalApplication(component);
                     /*var returnUploadIdEvent = component.getEvent("returnUploadId");
	                returnUploadIdEvent.setParams({
	                  "IdOfUploadedFile":response.getReturnValue()
	                });
	               returnUploadIdEvent.fire();
	               alert('fired');*/
                      
                  }else{
                      console.log('error while adding attachment'+ component.get("v.fileName") +  component.get("v.fileTitle"));
                      var errors=response.getError();
                      var message = errors[0];
                      
                      console.log(message);
                      
                  }
                
            });
           
           
           $A.enqueueAction(action); 
       }
         //  }
     },
    savePalApplication:function(component) {
        var action = component.get("c.upsertPalApplication"); 
        
        action.setParams({ "PALappToAdd":component.get('v.currentPAL') });
        action.setCallback(this, function(response){
            console.log('callback file upload'  );
            var state = response.getState();
            // alert(state);
            if (state === "SUCCESS") {
                if(component.get('v.shouldSaveAndQuit')){
                    window.location.href='/CPAO_ApplicationDashboard';
                    
                }else{
                    component.set('v.currentPAL',response.getReturnValue());
                    // component.set('v.NotYetUploaded','false');
                    
                    var applicationHasChanged = component.getEvent("applicationHasChanged");
                    applicationHasChanged.setParams({
                        "currentApplication": component.get("v.currentPAL"),
                        "comingFromFileUpload": "true",
                        "nextButtonClickeable": "true"
                    });
                    applicationHasChanged.fire();
                }
                
            }else{
                console.log('error while adding id of file step professional experience');
            }
        });
        
        $A.enqueueAction(action);
        
        
    }
})