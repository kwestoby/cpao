public without sharing class CPAO_PracticeInspectionSelector {

	public static List<CPAO_Practice_Inspection__c> getInspectionByReportDate(Set<Id> accountIds,Date reportDate) {
		return [SELECT Id, CPAO_ID__c, CPAO_Inspection_Report_Date__c
				FROM CPAO_Practice_Inspection__c 
				WHERE CPAO_ID__c =: accountIds AND CPAO_Inspection_Report_Date__c >= :reportDate];
	}

	public static List<CPAO_Practice_Inspection__c> getInspectionByAccountId(Set<Id> accountIds) {
		return [SELECT Id, CPAO_ID__c, CPAO_Inspection_Report_Date__c
				FROM CPAO_Practice_Inspection__c 
				WHERE CPAO_ID__c =: accountIds];

	}
}