public with sharing class xCPAO_DeclarationHandler {


	//when Declarations are inserted, this filters for records which have the CPAO_Declaration_Type__c field filled in. (at the moment ths is
	//happening with forms). the dynamic soql looks for application records which are open, have the same contact and have the same 
	//recordtype name as the CPAO_Declaration_Type__c field. This is needed because the Fonteva form has no way of understanding
	//context, so for example, when they fill in a form for good character declaration for a Reinstatement Application, this code will find the open
	//reinstatement application 
	//public static void markProperType(List<CPAO_Declaration__c> newDeclarations) {
	//	String open = 'Open';
	//	String query = 'SELECT Id, RecordType.Name, CPAO_Contact__c FROM CPAO_Application__c WHERE CPAO_Status__c = :open';
	//	Integer count = 0;
	//	for(Integer i = 0; i<newDeclarations.size(); i++){
	//		if(newDeclarations[i].CPAO_Contact__c != null && newDeclarations[i].CPAO_Declaration_Type__c != null){
	//			Id contactId = newDeclarations[i].CPAO_Contact__c;
	//			String decType = newDeclarations[i].CPAO_Declaration_Type__c;
	//			query = query + ' AND CPAO_Contact__c = :contactId';// + newDeclarations[i].CPAO_Contact__c;
	//			query = query + ' AND ';
	//			query = query + 'RecordType.Name = :decType';// + newDeclarations[i].CPAO_Declaration_Type__c;
	//			count = count +1;
	//		}

	//	}
	//	if(count == 0){
	//		return;
	//	}
	//	List<CPAO_Application__c> applications = Database.query(query);
	//	for(CPAO_Declaration__c declaration:newDeclarations){
	//		for(CPAO_Application__c application:applications){
	//			if(declaration.CPAO_Declaration_Type__c == application.RecordType.Name && 
	//				declaration.CPAO_Contact__c == application.CPAO_Contact__c){
	//				declaration.CPAO_Application__c = application.Id;
	//			}
	//		}
	//	}
	//}
	////This method updates the field CPAO_Good_Character_Review_Required__c	on its parent application
	////depending on the value on the declaration field CPAO_Good_Character_Review_Required__c. T

	//public static void markGoodCharacReviewRequired(List<CPAO_Declaration__c> newDeclarations) {

	//	Map<Id,CPAO_Declaration__c> applicationsAndRelatedDeclarations= new Map<Id,CPAO_Declaration__c>();
	//	Set<Id> applicationsIdsToUpdate= new Set<Id>();

	//	for(CPAO_Declaration__c declaration:newDeclarations){
	//		if(declaration.CPAO_Good_Character_Review_Required__c=='Yes'){
	//			//applicationsIdsToUpdate.add(declaration.CPAO_Application__c);
	//			applicationsAndRelatedDeclarations.put(declaration.CPAO_Application__c,declaration);
	//		}
	//	}
	//	applicationsIdsToUpdate=applicationsAndRelatedDeclarations.keySet();


	//	List<CPAO_Application__c> applicationsToUpdate= [SELECT Id,CPAO_Good_Character_Review_Required__c FROM CPAO_Application__c
	//	 WHERE Id =: applicationsIdsToUpdate];

	//	for (CPAO_Application__c application:applicationsToUpdate){
	//		application.CPAO_Good_Character_Review_Required__c='Yes';
	//		CPAO_Declaration__c relatedDeclaration = applicationsAndRelatedDeclarations.get(application.Id);
			
	//	}
	//	update applicationsToUpdate;
	//}

	
}