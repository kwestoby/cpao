global class CPAO_ClearEmployerIsPayingPALR implements Database.Batchable<sObject> {

	
	public static String MEMBER='Member';
	
	global Database.QueryLocator start(Database.BatchableContext BC) {
	
		return Database.getQueryLocator([
			SELECT Id,Name,CPAO_CPA_Contact_Type__c,CPAO_Employer_is_paying_PALR__c
			FROM Contact
			WHERE CPAO_CPA_Contact_Type__c =:MEMBER 
			]);
	}

   	global void execute(Database.BatchableContext BC, List<Contact> contactsToUpdate) {
		if(contactsToUpdate.isEmpty()){
			return;
		}
    	for(Contact contact:contactsToUpdate){
    		contact.CPAO_Employer_is_paying_PALR__c=null;
    	}
    	update contactsToUpdate;
	}
	
	global void finish(Database.BatchableContext BC) {
		
	}

	
	
}