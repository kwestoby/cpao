/**
* @File Name    :   CPAO_Milestone_Licence
* @Description  :   Trigger Milestone Licence
* @Date Created :   2018-07-12
* @Author       :   Michaell Reis Gasparini, Deloitte, mreisg@deloitte.com
* @group        :   Trigger
* @Modification Log:
**************************************************************************************
* Ver       Date        Author              Modification
* 1.0       2018-07-12  Michaell Reis       Created the file/class
**/
trigger CPAO_Milestone_Licence on CPAO_Licence__c (before update) {

 Map<Id, CPAO_Licence__c> licenceToEvaluate = new Map<Id, CPAO_Licence__c >();
    if(Trigger.isBefore&& Trigger.isUpdate){
        for(CPAO_Licence__c licObj : Trigger.New){
            licenceToEvaluate.put(licObj.Id,licObj);
        }
    }  
    
    CPAO_MilestoneLicenceHelper.historyTracker(licenceToEvaluate);
   
   
}