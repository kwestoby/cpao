<?xml version="1.0" encoding="UTF-8"?>
<CustomMetadata xmlns="http://soap.sforce.com/2006/04/metadata" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xmlns:xsd="http://www.w3.org/2001/XMLSchema">
    <label>Other extraordinary consequences</label>
    <protected>false</protected>
    <values>
        <field>AMD_or_CPD__c</field>
        <value xsi:type="xsd:string">CPD</value>
    </values>
    <values>
        <field>CPAO_Explanation__c</field>
        <value xsi:type="xsd:string">You had extraordinary circumstances between January 1 and December 31 and you are requesting an exemption on compassionate grounds.  The Registrar may grant an exemption, reduction or special consideration of the CPD requirements.  The Registrar shall not exercise the discretion to exempt a member from any or all of the required hours of the CPD Requirement unless the member’s ability to practice or earn a livelihood has been interrupted or significantly impaired.
The CPD exemption is subject to audit by CPA Ontario.  You may be contacted by CPA Ontario to provide a letter of explanation.</value>
    </values>
    <values>
        <field>CPAO_Percent_Off__c</field>
        <value xsi:nil="true"/>
    </values>
    <values>
        <field>Explanation_Third_Year__c</field>
        <value xsi:type="xsd:string">You had extraordinary circumstances between January 1 and December 31 and you are requesting an exemption on compassionate grounds.  The Registrar may grant an exemption, reduction or special consideration of the CPD requirements.  The Registrar shall not exercise the discretion to exempt a member from any or all of the required hours of the CPD Requirement unless the member’s ability to practice or earn a livelihood has been interrupted or significantly impaired.
The CPD exemption is subject to audit by CPA Ontario.  You may be contacted by CPA Ontario to provide a letter of explanation.</value>
    </values>
    <values>
        <field>Full_Name__c</field>
        <value xsi:type="xsd:string">Other extraordinary consequences</value>
    </values>
</CustomMetadata>
