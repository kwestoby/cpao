({
    
	doInit: function(component, event, helper) {
    console.log('doInintforMainComponent');
	var action = component.get("c.getRelationships");
   	action.setCallback(this, function(response){
        var state = response.getState();
        if (state === "SUCCESS") {
            console.log(response.getReturnValue());
            component.set("v.listOfRelationships", response.getReturnValue().relationships);
            if(response.getReturnValue().canBeRetiredUnemployed == true){
                component.set('v.showRetireUnemployButton',true);
            }
        }
    });

    $A.enqueueAction(action);
	},

    
    showOrHideCurrentEmployerForm : function(component, event, helper) {
        component.set('v.currentRelShowForm','true');
        var positionInlist=event.getSource().get("v.value");
        var relationAtPosition= component.get('v.listOfRelationships')[positionInlist];
        if(relationAtPosition.CPAO_Primary_Employer__c=='Yes'){
            component.set('v.isAlreadyPrimaryEmployer',true);
        }else{
            component.set('v.isAlreadyPrimaryEmployer',false);
        }

        component.set('v.employerPositionInList',relationAtPosition);
    },

    
    showOrHideNewEmployerForm : function(component, event, helper) {
    component.set('v.newEmployerShowForm','true'); 
    },

    
    radioButtonChange : function(component, event, helper) {
        var changedValue = event.getParam("value");
        if(changedValue=='Retired'){     
            component.set('v.radioValue','Retired');        
        } else if(changedValue=='Current Employer information has changed'){
            component.set('v.radioValue','CurrentEmployer');
        } else if(changedValue=='Unemployed'){
            component.set('v.radioValue','Unemployed');
        } else {
            component.set('v.radioValue','Blank');
        }
    },

     confirmRetirement : function(component, event, helper) {
    helper.confirmRetirementUnemployment(component, event, helper, 'Retired');
    },

    confirmUnemployment : function(component, event, helper) {
    helper.confirmRetirementUnemployment(component, event, helper, 'Unemployed');
    },

    /*test: function(component, event, helper) {
    console.log('fired in main');
    //location.reload();
    component.set('v.body','CPAO_Employment_Info'); 
    },*/

    updatedRelationshipRecord : function(component, event, helper) {
        console.log('relationship: ');
        console.log(event.getParam("msg"));
        console.log(event.getParam("type"));
        console.log(component.get('v.confirmationMessage'));
        console.log(component.get('v.severity'));

        
        var action = component.get("c.getRelationships");
        action.setCallback(this, function(response){
            var state = response.getState();
            if (state === "SUCCESS") {
              component.set("v.listOfRelationships", response.getReturnValue().relationships);
              component.set('v.newEmployerShowForm','false');
              if(response.getReturnValue().canBeRetiredUnemployed  == true){
                component.set('v.showRetireUnemployButton',true);
              }
            }
        });
        $A.enqueueAction(action);
        if(event.getParam("relationship") != null && event.getParam("relationship") != undefined){
            var deserializedRelationship = event.getParam("relationship");
            console.log('about to set the');
            component.set('v.employerPositionInList',deserializedRelationship);
            console.log('afetr being set');
        }
        if(event.getParam("msg") != null && event.getParam("msg") != undefined && 
                event.getParam("type") != undefined && event.getParam("type") != undefined){

            component.set("v.severity", event.getParam("type"));
            component.set("v.confirmationMessage", event.getParam("msg"));
        }

    },

    
    nextButton:function(component, event, helper) {
        
    }
  

})